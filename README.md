# README #

### || Tensor Network Classes || ###

Based on Uni10 library:  http://uni10.org/



### What is this repository for? ###

* Tensor Network numerical methods for 1D/2D many-body quantum states

### How do I get set up? ###

* Requirements:
    * C++ compiler (c++11 compatible)
    * blas + lapack or Intel MKL
    * Arpack-ng

* Clone this repository
* Set environment variables in tns_install.sh
* Run tns_install.sh

### Who do I talk to? ###

* Author: @cylo
* Affiliation: NTHU Physics @pcchen group
