#!/bin/bash

export COMPILER=g++
export CFLAGS="-O2 -std=c++11"
export UNI10MKL=FALSE
export UNI10ROOT=${HOME}/usr/local/uni10
#export ARPACKLIB=${HOME}/usr/lib

MPSROOT=${PWD}

cd ${MPSROOT}/src/tns-classes
make
make install

#cd ${MPSROOT}/src/tools
#make
#make install

#cd ${MPSROOT}/src/operations
#make
#make install

#cd ${MPSROOT}/src/measurements
#make
#make install

#cd ${MPSROOT}/src/apps
#make
#make install

#cd ${MPSROOT}/experiments
#make
#make install

cd ${MPSROOT}/
