#include <iostream>
#include <sstream>
// #include <complex>
#include <mps.hpp>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-w <(str) Wavefunction mps directory>" << std::endl;
	std::cerr << "-b <(str) Bulk wavefunction imps directory>" << std::endl;
	std::cerr << "-l0 <(int) initial system Length>" << std::endl;
	std::cerr << "-l <(int) system Length>" << std::endl;
	std::cerr << "-uc <(int) Unit Cell size>" << std::endl;
	std::cerr << "-m <(int) Max bond dimension>" << std::endl;
	std::cerr << "-df <(str) Destination Folder for final mps>" << std::endl;
}

//======================================

int main( int argc, char* argv[] ) {

	if (argc < 2) {
		errMsg( argv[0] );
		return 1;
	}

	/// main function body
	// wavefunction directory
	std::string wf_dir = "mps-inf";
	std::string bk_dir = "mps-inf";
	// final mps directory
	std::string mps_dir = "mps-ibc-yj";
	// length of each wire
	int l0 = 16;
	int len = 16;
	// unit-cell size
	int uc = 4;
	// bond dimension
	int chi = 8;

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-w") {
			if (i + 1 < argc) {
				wf_dir = std::string(argv[i+1]);
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-b") {
			if (i + 1 < argc)
				bk_dir = std::string(argv[i+1]);
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l0") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> l0;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-uc") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> uc;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> chi;
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
	}

	// initialize YJ
	uni10::CUniTensor gam0;
	try {
		gam0 = uni10::CUniTensor(wf_dir + "/wire1/gamma_0");
	}
	catch(const std::exception& e) {
		try {
			gam0 = uni10::CUniTensor(wf_dir + "/gamma_0");
		}
		catch(const std::exception& e) {
			errMsg(argv[0]);
			return 1;
		}
	}
	std::vector<uni10::Qnum> qphys = gam0.bond()[1].Qlist();
	std::vector<uni10::Qnum> qvirt = gam0.bond()[0].Qlist();
	chi = std::max(chi, gam0.bond()[0].dim());
	YJuncQnIBC mpsYJ(l0, chi, qphys, qvirt);

	// import MPS
	mpsYJ.importMPS(wf_dir, uc, false);

	// expand
	mpsYJ.expand( (len-l0), bk_dir, uc );

	mpsYJ.exportMPS( mps_dir );

	return 0;
}
