#include <peps.hpp>

//======================================

int main(int argc, char* argv[]) {

	NetInf itfNet(2, 3);
	itfNet.randomize();

	uni10::CUniTensor ham("ham_itf");
	uni10::CUniTensor sz("sz");

	std::vector<monitor> mon;
	mon.push_back( monitor("energy") );
	mon.push_back( monitor("sz") );
	mon.push_back( monitor("entropy") );

	Complex dt(0.0, 0.1);
	itfNet.itebd( ham, dt, 1000 );
	dt = Complex(0.0, 0.01);
	itfNet.itebd( ham, dt, 1000 );
	dt = Complex(0.0, 0.001);
	itfNet.itebd( ham, dt, 1000 );
	dt = Complex(0.0, 0.0001);
	itfNet.itebd( ham, dt, 1000 );
	dt = Complex(0.0, 0.00001);
	itfNet.itebd( ham, dt, 1000 );

	std::cout << std::setprecision(10) << itfNet.expVal( ham ).real() << "\t" 
		<< std::fabs( itfNet.expVal( sz ).real() ) << "\t"
		<< std::fabs( itfNet.expValTRG( sz, 20, 8 ).real() ) << "\t";

	return 0;
}

