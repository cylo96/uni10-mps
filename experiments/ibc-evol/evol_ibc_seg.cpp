#include <sstream>
#include <tuple>
#include <mps.hpp>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H arg  load Hamiltonian from file arg" << std::endl;
	std::cerr << "-H2 arg1 arg2  load Hamiltonians from files" << std::endl;
	std::cerr << "-H3 <Ham-1> <Ham-2> <Ham-3>" << std::endl;
	std::cerr << "-imp arg1 arg2  put local IMPurity Hamiltonian from file arg1 at arg2" << std::endl;
	std::cerr << "-op arg  OPerator to measure" << std::endl;
	std::cerr << "-opx arg1 arg2  apply local excitation OPerator arg1 at arg2" << std::endl;
	std::cerr << "-bloc arg  Boundary/interface Location" << std::endl;
	std::cerr << "-bloc2 <Boundary/interface-LOC-1> <Boundary/interface-LOC-2>" << std::endl;
	std::cerr << "-w arg  load Wavefunction (gamma and lambda tensors) from folder arg" << std::endl;
	std::cerr << "-w2 arg1 arg2  load inf Wavefunction (gamma and lambda tensors) from folder args" << std::endl;
	std::cerr << "-df arg  save final mps to Destination Folder arg" << std::endl;
	std::cerr << "-n arg  Network file folder arg" << std::endl;
	std::cerr << "-b arg  load Boundary operators from folder arg" << std::endl;
	std::cerr << "-b2 arg1 arg2  load Boundary operators from folders" << std::endl;
	std::cerr << "-l arg  system Length" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of tebd Steps" << std::endl;
	std::cerr << "-rt arg  tebd Real Timestep" << std::endl;
	std::cerr << "-it arg  tebd Imaginary Timestep" << std::endl;
	std::cerr << "-mes arg  Monitor expectation value every Evolution Steps" << std::endl;
	std::cerr << "-intm  save INTerMediate state every mes" << std::endl;
	std::cerr << "-V  Verbose. show information during dmrg update" << std::endl;
	std::cerr << "-offset  OFFSET the output data" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// Hamiltonian tensor
	std::string ham_str_1 = "";
	std::string ham_str_2 = "";
	std::string ham_str_3 = "";
	// impurity Hamiltonian tensor
	std::vector<std::tuple<std::string, int>> imps;
	// operator to measure
	std::vector<std::string> op_str_vec;
	std::vector<uni10::CUniTensor> op_vec;
	// local excitation operator
	std::string opx_str = "id";
	int idx_opx = -1;
	// wavefunction directory
	std::string wf_dir_1 = "mps-inf";
	std::string wf_dir_2 = "mps-inf";
	std::string wf_dir_3 = "mps-inf";
	// final mps directory
	std::string mps_dir = "mps-ibc-" + ham_str_1;
	// Network file directory
	std::string net_dir = ".";
	// inf boundary hamiltonian directory
	std::string hb_dir_1 = ".";
	std::string hb_dir_2 = ".";
	// load mps from file?
	bool load_file = true;
	bool load_2file = false;
	bool load_3file = false;
	// system length
	int len = 20;
	// boundary location
	int bloc1 = -1;
	int bloc2 = -1;
	// bond dimension
	int bd_dim = 5;
	// initial imag dt
	double dT = 0.0;
	// initial real dt
	double dt = 0.1;
	// number of steps
	int steps_max = -1;
	// step interval to monitor
	int mes = -1;
	// save intermediate state?
	bool save_intm = false;
	// show truncation error
	bool verbose = false;
	// plot stack offset
	bool offset = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc) {
				ham_str_1 = std::string(argv[i+1]);
			}
			else {
				std::cerr << "-H <(str)Hamiltonian>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-H2") {
			if (i + 2 < argc) {
				ham_str_1 = std::string(argv[i+1]);
				ham_str_2 = std::string(argv[i+2]);
				load_2file = true;
			}
			else {
				std::cerr << "-H2 <(str)Hamiltonian> <(str)Hamiltonian>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-H3") {
			if (i + 3 < argc) {
				ham_str_1 = std::string(argv[i+1]);
				ham_str_2 = std::string(argv[i+2]);
				ham_str_3 = std::string(argv[i+3]);
				load_2file = true;
				load_3file = true;
			}
			else {
				std::cerr << "-H3 <(str)Hamiltonian> <(str)Hamiltonian> <(str)Hamiltonian>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-imp") {
			if (i + 2 < argc) {
				std::string himp_str;
				int idx_imp;
				himp_str = std::string(argv[i+1]);
				std::istringstream(argv[i+2]) >> idx_imp;
				imps.push_back( std::make_tuple(himp_str, idx_imp) );
			}
			else {
				std::cerr << "-imp <(str)impurity Hamiltonain> <(int)location of impurity>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-op") {
			if (i + 1 < argc) {
				op_str_vec.push_back(std::string(argv[i+1]));
			}
			else {
				std::cerr << "-op <(str)operator>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-opx") {
			if (i + 2 < argc) {
				opx_str = std::string(argv[i+1]);
				std::istringstream(argv[i+2]) >> idx_opx;
			}
			else {
				std::cerr << "-opx <(str)operator> <(int)location of operator>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-bloc") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bloc1;
			else {
				std::cerr << "-bloc <(int) boundary location>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-bloc2") {
			if (i + 2 < argc) {
				std::istringstream(argv[i+1]) >> bloc1;
				std::istringstream(argv[i+2]) >> bloc2;
			}
			else {
				std::cerr << "-bloc2 <(int) boundary location> <(int) boundary location>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc) {
				wf_dir_1 = std::string(argv[i+1]);
			}
			else {
				std::cerr << "-w <(str)wavefunction directory>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w2") {
			load_2file = true;
			if (i + 2 < argc &&
				((std::string)argv[i+1]).find("-") != std::string::npos &&
				((std::string)argv[i+2]).find("-") != std::string::npos) {
					wf_dir_1 = std::string(argv[i+1]);
					wf_dir_2 = std::string(argv[i+2]);
			}
			else {
				std::cerr << "-w2 option requires directories of the Wavefunction MPS." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-df option requires a string of Destination Folder's name for final mps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-n") {
			if (i + 1 < argc) {
				net_dir = std::string(argv[i+1]);
			}
			else {
				std::cerr << "-n <(str)network files directory>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-b") {
			if (i + 1 < argc)
				hb_dir_1 = std::string(argv[i+1]);
			else {
				std::cerr << "-b <(str)boundary Hamiltonians directory>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-b2") {
			if (i + 2 < argc) {
				hb_dir_1 = std::string(argv[i+1]);
				hb_dir_2 = std::string(argv[i+2]);
			}
			else {
				std::cerr << "-b2 <(str)left boundary Ham dir> <(str)right boundary Ham dir>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l <(int)lattice length>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> bd_dim;
			}
			else {
				std::cerr << "-m <(int)bond dimension>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-it") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> dT;
				dt = 0.0;
			}
			else {
				std::cerr << "-it <(double)imaginary time step>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-rt") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> dt;
				dT = 0.0;
			}
			else {
				std::cerr << "-rt <(double)real time step>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> steps_max;
			}
			else {
				std::cerr << "-s <(int)max tebd steps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-mes") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> mes;
			}
			else {
				std::cerr << "-mes <(int)monitor evolution steps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (mes > 0) {
				save_intm = true;
			}
		}
		else if (std::string(argv[i]) == "-offset") {
			if (mes > 0) {
				offset = true;
			}
		}
	}

	// import Hamiltonians
	uni10::CUniTensor hl, hlw, hw, hwr, hr;
	try {
		hl = uni10::CUniTensor(hb_dir_1 + "/HL");
		hlw = uni10::CUniTensor(hb_dir_1 + "/HLW");
		hwr = uni10::CUniTensor(hb_dir_2 + "/HWR");
		hr = uni10::CUniTensor(hb_dir_2 + "/HR");
	}
	catch(const std::exception& e) {
		hl = uni10::CUniTensor(hb_dir_1 + "/HL");
		hlw = uni10::CUniTensor(hb_dir_1 + "/HLW");
		hwr = uni10::CUniTensor(hb_dir_1 + "/HWR");
		hr = uni10::CUniTensor(hb_dir_1 + "/HR");
	}
	// Hamiltonian array
	std::vector<uni10::CUniTensor> ham;
	if (ham_str_2 == "") {
		hw = uni10::CUniTensor(ham_str_1);
		for (int i = 0; i < len; ++i)
			ham.push_back(hw);
	}
	else {
		if (load_2file && !load_3file) {
			bloc2 = len;
		}
		hw = uni10::CUniTensor(ham_str_1);
		for (int i = 0; i < bloc1; ++i)
			ham.push_back(hw);
		hw = uni10::CUniTensor(ham_str_2);
		for (int i = bloc1; i < bloc2; ++i)
			ham.push_back(hw);
		if (load_3file) {
			hw = uni10::CUniTensor(ham_str_3);
			for (int i = bloc2; i < len; ++i)
				ham.push_back(hw);
		}
	}
	int num_imps = imps.size();
	if (num_imps > 0) {
		for (int i = 0; i < num_imps; ++i)
			ham[std::get<1>(imps[i])] = uni10::CUniTensor(std::get<0>(imps[i]));
	}

	// import operators
	uni10::CUniTensor opx;
	if (op_str_vec.empty())
		op_str_vec.push_back("id");
	int opn = op_str_vec.size();
	for (int j = 0; j < opn; ++j) {
		try {
			op_vec.push_back(uni10::CUniTensor(op_str_vec[j]));
		}
		catch(const std::exception& e) {
			op_vec.push_back(OP(op_str_vec[j]));
		}
	}
	try {
		opx = uni10::CUniTensor(opx_str);
	}
	catch(const std::exception& e) {
		opx = OP(opx_str);
	}
	uni10::CUniTensor id(op_vec[0].bond());
	id.identity();

	// initialize Ising chain
	int d = ham[0].bond()[0].dim();
	ChainIBC mpsIBC(len, d, bd_dim);
	mpsIBC.importMPS(wf_dir_1);
	mpsIBC.loadNetIBC(net_dir);

	// excitation
	if (idx_opx > 0) {
		mpsIBC.oneSiteOP( opx, idx_opx );
		// mpsIBC.tebdImp( hl, hlw, hw, hwr, hr, himp, idx_imp, Complex(0.0, 0.0), 1);
	}

	// initial measurement
	int bn = op_vec[0].bondNum();
	double expV;
	double norm;

	std::cout << "#time" << '\t' << "idx";
	for (int j = 0; j < opn; ++j)
		std::cout << '\t' << "< " + op_str_vec[j] + " >";
	std::cout << std::endl;

	for (int i = 0; i <= len-(bn/2); ++i) {
		norm = mpsIBC.expVal( id, i )[0].real();
		std::cout << 0 << '\t' << i;
		for (int j = 0; j < opn; ++j) {
			expV = ( mpsIBC.expVal( op_vec[j], i )[0].real() / norm );
			std::cout << std::setprecision(10) << '\t' << expV;
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;

	// evolve and measure
	Complex time_step(dt, dT);
	int max_ite = steps_max/mes;
	for (int ite = 0; ite < max_ite; ++ite) {

		mpsIBC.tebd( hl, hlw, ham, hwr, hr, time_step, mes, 2);

		for (int i = 0; i <= len-(bn/2); ++i) {
			norm = mpsIBC.expVal( id, i )[0].real();
			std::cout << (ite+1)*mes*time_step.real() << '\t' << i;
			for (int j = 0; j < opn; ++j) {
				expV = ( mpsIBC.expVal( op_vec[j], i )[0].real() / norm );
				if (offset)
					std::cout << std::setprecision(10) << '\t' << expV + (ite+1)*0.75;
				else
					std::cout << std::setprecision(10) << '\t' << expV;
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;

		if (save_intm) {
			double tt;
			if (dt > 0) {
				tt = dt * (ite+1) * mes;
				mpsIBC.exportMPS( mps_dir + "-rt" + std::to_string((double) tt).substr(0,5) );
			}
			else if (dT > 0) {
				tt = dT * (ite+1) * mes;
				mpsIBC.exportMPS( mps_dir + "-it" + std::to_string((double) tt).substr(0,5) );
			}
		}
	}

	mpsIBC.clear();

	return 0;
}
