#include <map>
#include <string>
#include <mps.hpp>

void updateHbXXZ( ChainInf& mps_inf, std::map<std::string, uni10::CNetwork>& nets,
	uni10::CUniTensor& hl, uni10::CUniTensor& hlw, uni10::CUniTensor& hwr, uni10::CUniTensor& hr,
	double J, double Jz, double hz, char bdry = 'b', int ar_iter = -1, double tol = 1e-15 );

void updateBdry( ChainIBC& mps_ibc, uni10::CUniTensor lam, char bdry = 'b' );

void importNetIBC( std::map<std::string, uni10::CNetwork>& nets, std::string net_dir );
