#include <sstream>

#include <mps.hpp>

//======================================

int main(int argc, char* argv[]) {

	// import operators
	uni10::CUniTensor hl = uni10::CUniTensor("HL");
	uni10::CUniTensor hlw = uni10::CUniTensor("HLW");
	uni10::CUniTensor hw = uni10::CUniTensor("ham_xxz");
	uni10::CUniTensor hwr = uni10::CUniTensor("HWR");
	uni10::CUniTensor hr = uni10::CUniTensor("HR");
	uni10::CUniTensor op = uni10::CUniTensor("sz");
	uni10::CUniTensor flip = uni10::CUniTensor("sp");

	// initialize heisenberg chain
	int d = hw.bond()[0].dim();
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;

	ChainIBC xxzChain(L, d, X);
	xxzChain.importMPS( "mps-inf" );

	// excitation
	xxzChain.oneSiteOP( flip, L/2 - 1 );

	// initial measurement
	std::vector<uni10::UniTensor> expV;

	for (int i = 0; i < L; ++i) {
		expV.push_back( xxzChain.expVal( op, i ) );
		std::cout << i << "\t" << std::setprecision(10) << expV[i][0] << std::endl;
	}
	expV.clear();
	std::cout << std::endl;

	// evolve and measure
	Complex dt (0.05, 0.0);
	for (int ite = 0; ite < 10; ++ite) {

		xxzChain.tebd2( hl, hlw, hw, hwr, hr, dt, 20);

		for (int i = 0; i < 100; ++i) {
			expV.push_back( xxzChain.expVal( op, i ) );
			std::cout << i << "\t" << std::setprecision(10) << expV[i][0] + (ite+1)*0.75 << std::endl;
			//std::cout << i << "\t" << std::setprecision(10) << expV[i][0] << std::endl;
		}
		expV.clear();
		std::cout << std::endl;
	}

	//xxzChain.exportMPS( "mps-ibc-xxz" );
	xxzChain.clear();

	return 0;
}

