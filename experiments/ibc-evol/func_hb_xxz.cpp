#include <iostream>
#include "func_hb_xxz.hpp"

//============================================

uni10::CUniTensor netContract( uni10::CNetwork net,
	uni10::CUniTensor T1, uni10::CUniTensor T2,
	uni10::CUniTensor T1d, uni10::CUniTensor T2d,
	uni10::CUniTensor op ) {

	net.putTensor( "T1", T1 );
	net.putTensor( "T2", T2 );
	net.putTensor( "op", op );
	net.putTensor( "T1dag", T1d );
	net.putTensor( "T2dag", T2d );

	return net.launch();
}

//============================================

uni10::CUniTensor netContract( uni10::CNetwork net,
	uni10::CUniTensor T1, uni10::CUniTensor T2, uni10::CUniTensor T3, uni10::CUniTensor T4,
	uni10::CUniTensor T1d, uni10::CUniTensor T2d, uni10::CUniTensor T3d, uni10::CUniTensor T4d,
	uni10::CUniTensor op ) {

	net.putTensor( "T1", T1 );
	net.putTensor( "T2", T2 );
	net.putTensor( "T3", T3 );
	net.putTensor( "T4", T4 );
	net.putTensor( "op", op );
	net.putTensor( "T1dag", T1d );
	net.putTensor( "T2dag", T2d );
	net.putTensor( "T3dag", T3d );
	net.putTensor( "T4dag", T4d );

	return net.launch();
}

//============================================

void updateHbXXZ( ChainInf& mps_inf, std::map<std::string, uni10::CNetwork>& nets,
	uni10::CUniTensor& hl, uni10::CUniTensor& hlw, uni10::CUniTensor& hwr, uni10::CUniTensor& hr,
	double J, double Jz, double hz, char bdry, int ar_iter, double tol ) {
	///
	const std::vector<uni10::CUniTensor>& gams = mps_inf.getGamVec();
	const std::vector<uni10::CUniTensor>& lams = mps_inf.getLamVec();
	uni10::CUniTensor A1 = netLG( lams[0], gams[0] );
	uni10::CUniTensor A2 = netLG( lams[1], gams[1] );
	uni10::CUniTensor B1 = netGL( gams[1], lams[0] );
	uni10::CUniTensor B2 = netGL( gams[0], lams[1] );
	uni10::CUniTensor A1d = dag(A1);
	uni10::CUniTensor A2d = dag(A2);
	uni10::CUniTensor B1d = dag(B1);
	uni10::CUniTensor B2d = dag(B2);

	// define operators
	uni10::CUniTensor sx = OP( "sx" );
	uni10::CUniTensor sy = OP( "sy" );
	uni10::CUniTensor sz = OP( "sz" );
	uni10::CUniTensor Id = OP( "id" );
	uni10::CUniTensor ham = 
		0.25 * ( J * ( uni10::otimes(sx, sx) + uni10::otimes(sy, sy) ) + Jz * uni10::otimes(sz, sz) );
	uni10::CUniTensor hml = ham;
	uni10::CUniTensor hmr = ham;
	if (hz != 0.0) {
		hml += 0.5 * hz * uni10::otimes(sz, Id);
		hmr += 0.5 * hz * uni10::otimes(Id, sz);
	}
	
	// define E5 and E5r
	uni10::CUniTensor E5( lams[0].bond(), "E5" );
	E5.identity();
	uni10::CUniTensor E5r = bondInv(E5);

	// define E4 and E4r
	uni10::CUniTensor E4 = netContract( nets["l1"], A1, A2, A1d, A2d, sz );
	uni10::CUniTensor E4r = netContract( nets["r1"], B1, B2, B1d, B2d, sz );

	// define E3 and E3r
	uni10::CUniTensor E3 = netContract( nets["l1"], A1, A2, A1d, A2d, sy );
	uni10::CUniTensor E3r = netContract( nets["r1"], B1, B2, B1d, B2d, sy );

	// define E2 and E2r
	uni10::CUniTensor E2 = netContract( nets["l1"], A1, A2, A1d, A2d, sx );
	uni10::CUniTensor E2r = netContract( nets["r1"], B1, B2, B1d, B2d, sx );

	// define C = Tsx_E2 + Tsy_E3 + Tsz_E4;
	uni10::CUniTensor C1 = netContract( nets["l2o"], A1, A2, A1d, A2d, hml );
	uni10::CUniTensor C2 = netContract( nets["l2e"], A1, A2, A1, A2, A1d, A2d, A1d, A2d, hml );
	uni10::CUniTensor C = C1 + C2;	// lvec

	// define Cr
	uni10::CUniTensor C1r = netContract( nets["r2o"], B1, B2, B1d, B2d, hmr );
	uni10::CUniTensor C2r = netContract( nets["r2e"], B1, B2, B1, B2, B1d, B2d, B1d, B2d, hmr );
	uni10::CUniTensor Cr = C1r + C2r;	// rvec

	// calculate e0
	uni10::CUniTensor lam0 = lams[0];
	uni10::CUniTensor traceRhoC = traceRhoVecQn( true, lam0, C );
	double e0 = traceRhoC[0].real();
	
	// find HL
	uni10::CUniTensor I2 = E5;
	uni10::CUniTensor rhs_l = C + (-1.0 * e0) * I2;
	uni10::CUniTensor trial = rhs_l;

	uni10::CUniTensor HL = findHbQn( true, netAA(A1, A2), lam0, trial, rhs_l, ar_iter, tol );
	HL.permute(1);
	HL.setName("HL");

	// find HR
	traceRhoC = traceRhoVecQn( false, lam0, Cr );
	e0 = traceRhoC[0].real();
	I2 = E5r;
	uni10::CUniTensor rhs_r = Cr + (-1.0 * e0) * I2;
	trial = bondInv(HL);

	uni10::CUniTensor HR;
	HR = findHbQn( false, netBB(B1, B2), lam0, trial, rhs_r, ar_iter, tol );
	HR.permute(1);
	HR.setName("HR");

	if (bdry != 'r') {
		// update hl
		hl = HL;
		// update hlw
		uni10::CUniTensor HLW =
			0.25 * ( J * ( otimes( E2, sx ) + otimes( E3, sy ) )
				+ Jz * uni10::otimes( E4, sz ) )
				+ 0.5 * hz * uni10::otimes( E4, Id );
		hlw = HLW;
	}

	if (bdry != 'l') {
		// update hr
		hr = HR;
		// update hwr
		uni10::CUniTensor HWR =
			0.25 * ( J * ( uni10::otimes( sx, E2r ) + uni10::otimes( sy, E3r ) )
				+ Jz * uni10::otimes( sz, E4r ) )
				+ 0.5 * hz * uni10::otimes( Id, E4r );
		hwr = HWR;
	}
}

//============================================

void updateBdry( ChainIBC& mps_ibc, uni10::CUniTensor lam, char bdry ) {
	///
	if (bdry != 'r') {
		uni10::CUniTensor A0 = netLG(mps_ibc.getLambda(0), mps_ibc.getGamma(0));
		mps_ibc.putGamma(extractGamma2(A0, lam), 0);
		mps_ibc.putLambda(lam, 0);
	}
	if (bdry != 'l') {
		int L = mps_ibc.getSize();
		uni10::CUniTensor B0 = netGL(mps_ibc.getGamma(L-1), mps_ibc.getLambda(L));
		mps_ibc.putGamma(extractGamma(B0, lam), L-1);
		mps_ibc.putLambda(lam, L);
	}
}

//============================================

void importNetIBC( std::map<std::string, uni10::CNetwork>& nets, std::string net_dir ) {
	/// import network files
	nets["l1"] = uni10::CNetwork( net_dir + "/ibc_lb_op1.net" );
	nets["r1"] = uni10::CNetwork( net_dir + "/ibc_rb_op1.net" );
	nets["l2o"] = uni10::CNetwork( net_dir + "/ibc_lb_op2_odd.net" );
	nets["l2e"] = uni10::CNetwork( net_dir + "/ibc_lb_op2_even.net" );
	nets["r2o"] = uni10::CNetwork( net_dir + "/ibc_rb_op2_odd.net" );
	nets["r2e"] = uni10::CNetwork( net_dir + "/ibc_rb_op2_even.net" );
}
