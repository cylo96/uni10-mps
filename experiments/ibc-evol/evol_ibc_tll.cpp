#include <sstream>
#include <stdexcept>
#include <tuple>
#include <mps.hpp>

//======================================

void errMsg( char* arg ) {
	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H [ham_file]                          :" << std::endl;
	std::cerr << "-H2 [ham_file_1] [ham_file_2]          :" << std::endl;
	std::cerr << "-imp [ham_imp_file] [loc_idx]          : local IMPurity Hamiltonian" << std::endl;
	std::cerr << "-op [op_file]                          : OPerator to measure" << std::endl;
	std::cerr << "-opx [op_file] [loc_idx]               : local excitation OPerator" << std::endl;
	std::cerr << "-bloc [loc]                            : Boundary/interface Location" << std::endl;
	std::cerr << "-w [wave_fn_dir]                       : load Wavefunction mps" << std::endl;
	std::cerr << "-wb [bulk_wf_dir]                      : load Bulk Wavefunction mps" << std::endl;
	std::cerr << "-wb2 [bulk_dir_1] [bulk_dir_2]         : load L/R Bulk Wavefunction mps" << std::endl;
	std::cerr << "-df [mps_dir]                          : save mps to Destination Folder" << std::endl;
	std::cerr << "-n [net_dir]                           : Network file directory" << std::endl;
	std::cerr << "-l [length]                            : system Length" << std::endl;
	std::cerr << "-m [bond_dim]                          : Max bond dimension" << std::endl;
	std::cerr << "-s [steps]                             : number of tebd Steps" << std::endl;
	std::cerr << "-rt [time_step]                        : tebd Real Timestep" << std::endl;
	std::cerr << "-it [time_step]                        : tebd Imaginary Timestep" << std::endl;
	std::cerr << "-mes [steps]                           : Monitor in how many Evolution Steps" << std::endl;
	std::cerr << "-intm                                  : save INTerMediate state every mes" << std::endl;
	std::cerr << "-V                                     : Verbose" << std::endl;
	std::cerr << "-offset                                : OFFSET the monitor output" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {
		errMsg( argv[0] );
		return 1;
	}

	// Hamiltonian tensor
	std::string ham_str_1 = "";
	std::string ham_str_2 = "";
	// impurity Hamiltonian tensor
	std::vector<std::tuple<std::string, int>> imps;
	// operator to measure
	// std::string op_str = "id";
	std::vector<std::string> op_str_vec;
	std::vector<uni10::CUniTensor> op_vec;
	// local excitation operator
	std::string opx_str = "id";
	int idx_opx = -1;
	// wavefunction directory
	std::string wf_dir = "mps-inf";
	std::string wb_dir_1 = "mps-inf";
	std::string wb_dir_2 = "";
	// final mps directory
	std::string mps_dir = "mps-ibc-" + ham_str_1;
	// Network file directory
	std::string net_dir = ".";
	// inf boundary hamiltonian directory
	std::string hb_dir_1 = ".";
	std::string hb_dir_2 = ".";
	// load mps from file?
	bool load_file = true;
	bool load_2bulk = false;
	// system length
	int len = 20;
	// boundary location
	int bloc1 = -1;
	// bond dimension
	int bd_dim = 5;
	// initial imag dt
	double dT = 0.0;
	// initial real dt
	double dt = 0.1;
	// number of steps
	int steps_max = -1;
	// step interval to monitor
	int mes = -1;
	// save intermediate state?
	bool save_intm = false;
	// show truncation error
	bool verbose = false;
	// plot stack offset
	bool offset = false;
	
	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc) {
				ham_str_1 = std::string(argv[i+1]);
			}
			else {
				std::cerr << "-H <(str) ham_file>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-H2") {
			if (i + 2 < argc) {
				ham_str_1 = std::string(argv[i+1]);
				ham_str_2 = std::string(argv[i+2]);
			}
			else {
				std::cerr << "-H2 <(str) ham_file_1> <(str) ham_file_2>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-imp") {
			if (i + 2 < argc) {
				std::string himp_str;
				int idx_imp;
				himp_str = std::string(argv[i+1]);
				std::istringstream(argv[i+2]) >> idx_imp;
				imps.push_back( std::make_tuple(himp_str, idx_imp) );
			}
			else {
				std::cerr << "-imp <(str)impurity Hamiltonain> <(int)location of impurity>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-op") {
			if (i + 1 < argc) {
				// op_str = std::string(argv[i+1]);
				op_str_vec.push_back(std::string(argv[i+1]));
			}
			else {
				std::cerr << "-op <(str)operator>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-opx") {
			if (i + 2 < argc) {
				opx_str = std::string(argv[i+1]);
				std::istringstream(argv[i+2]) >> idx_opx;
			}
			else {
				std::cerr << "-opx <(str)operator> <(int)location of operator>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-bloc") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bloc1;
			else {
				std::cerr << "-bloc <(int) boundary location>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc) {
				wf_dir = std::string(argv[i+1]);
			}
			else {
				std::cerr << "-w <(str)wavefunction directory>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-wb") {
			if (i + 1 < argc &&
				((std::string)argv[i+1]).find("-") != std::string::npos) {
					wb_dir_1 = std::string(argv[i+1]);
			}
			else {
				std::cerr << "-wb <(str)bulk wavefunction directory>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-wb2") {
			load_2bulk = true;
			if (i + 2 < argc &&
				((std::string)argv[i+1]).find("-") != std::string::npos &&
				((std::string)argv[i+2]).find("-") != std::string::npos) {
					wb_dir_1 = std::string(argv[i+1]);
					wb_dir_2 = std::string(argv[i+2]);
			}
			else {
				std::cerr << "-wb2 <(str)bulk1 directory> <(str)bulk2 directory>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-df option requires a string of Destination Folder's name for final mps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-n") {
			if (i + 1 < argc) {
				net_dir = std::string(argv[i+1]);
			}
			else {
				std::cerr << "-n <(str)network files directory>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l <(int)lattice length>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> bd_dim;
			}
			else {
				std::cerr << "-m <(int)bond dimension>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-it") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> dT;
				dt = 0.0;
			}
			else {
				std::cerr << "-it <(double)imaginary time step>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-rt") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> dt;
				dT = 0.0;
			}
			else {
				std::cerr << "-rt <(double)real time step>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> steps_max;
			}
			else {
				std::cerr << "-s <(int)max tebd steps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-mes") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> mes;
			}
			else {
				std::cerr << "-mes <(int)monitor evolution steps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (mes > 0) {
				save_intm = true;
			}
		}
		else if (std::string(argv[i]) == "-offset") {
			if (mes > 0) {
				offset = true;
			}
		}
	}

	// Hamiltonian array
	uni10::CUniTensor hbl, hbr, hbb;
	std::vector<uni10::CUniTensor> ham;
	if (ham_str_2 == "") {
		hbb = uni10::CUniTensor(ham_str_1);
		for (int i = 0; i < len; ++i)
			ham.push_back(hbb);
	}
	else {
		if (bloc1 < 0) {
			bloc1 = len/2;
		}
		hbl = uni10::CUniTensor(ham_str_1);
		for (int i = 0; i < bloc1; ++i)
			ham.push_back(hbl);
		hbr = uni10::CUniTensor(ham_str_2);
		for (int i = bloc1; i < len; ++i)
			ham.push_back(hbr);
	}
	int num_imps = imps.size();
	if (num_imps > 0) {
		for (int i = 0; i < num_imps; ++i)
			ham[std::get<1>(imps[i])] = uni10::CUniTensor(std::get<0>(imps[i]));
	}

	// import operators
	uni10::CUniTensor opx;
	if (op_str_vec.empty())
		op_str_vec.push_back("id");
	int opn = op_str_vec.size();
	for (int j = 0; j < opn; ++j) {
		try {
			op_vec.push_back(uni10::CUniTensor(op_str_vec[j]));
		}
		catch(const std::exception& e) {
			op_vec.push_back(OP(op_str_vec[j]));
		}
	}
	try {
		opx = uni10::CUniTensor(opx_str);
	}
	catch(const std::exception& e) {
		opx = OP(opx_str);
	}
	uni10::CUniTensor id(op_vec[0].bond());
	id.identity();

	// initialize IBC chain
	int d = ham[0].bond()[0].dim();
	ChainIBC mpsIBC(len, d, bd_dim);
	mpsIBC.importMPS(wf_dir);
	
	// import bulk states
	ChainInf mpsInf1(d, bd_dim);
	mpsInf1.importMPS(wb_dir_1);
	ChainInf mpsInf2(d, bd_dim);
	if (load_2bulk)
		mpsInf2.importMPS(wb_dir_2);
	else
		mpsInf2.importMPS(wb_dir_1);
	
	// import network files
	mpsIBC.loadNetIBC(net_dir);

	// excitation
	if (idx_opx > 0) {
		mpsIBC.oneSiteOP( opx, idx_opx );
		// mpsIBC.tebdImp( hl, hlw, hw, hwr, hr, himp, idx_imp, Complex(0.0, 0.0), 1);
	}

	// initial measurement
	int bn = op_vec[0].bondNum();
	double expV;
	double norm;

	std::cout << "#time" << '\t' << "idx";
	for (int j = 0; j < opn; ++j)
		std::cout << '\t' << "< " + op_str_vec[j] + " >";
	std::cout << std::endl;

	for (int i = 0; i <= len-(bn/2); ++i) {
		norm = mpsIBC.expVal( id, i )[0].real();
		std::cout << 0 << '\t' << i;
		for (int j = 0; j < opn; ++j) {
			expV = ( mpsIBC.expVal( op_vec[j], i )[0].real() / norm );
			std::cout << std::setprecision(10) << '\t' << expV;
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;

	// evolve and measure
	Complex time_step(dt, dT);
	int max_ite = steps_max/mes;

	for (int ite = 0; ite < max_ite; ++ite) {
		if (load_2bulk)
			mpsIBC.tebd(ham, hbl, hbr, mpsInf1, mpsInf2, time_step, mes, 2 );
		else
			mpsIBC.tebd(ham, hbb, hbb, mpsInf1, mpsInf2, time_step, mes, 2 );

		for (int i = 0; i <= len-(bn/2); ++i) {
			norm = mpsIBC.expVal( id, i )[0].real();
			std::cout << (ite+1)*mes*time_step.real() << '\t' << i;
			for (int j = 0; j < opn; ++j) {
				expV = ( mpsIBC.expVal( op_vec[j], i )[0].real() / norm );
				if (offset)
					std::cout << std::setprecision(10) << '\t' << expV + (ite+1)*0.75;
				else
					std::cout << std::setprecision(10) << '\t' << expV;
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;

		if (save_intm) {
			double tt;
			if (dt > 0) {
				tt = dt * (ite+1) * mes;
				mpsIBC.exportMPS( mps_dir + "-rt" + std::to_string((double) tt).substr(0,5) );
			}
			else if (dT > 0) {
				tt = dT * (ite+1) * mes;
				mpsIBC.exportMPS( mps_dir + "-it" + std::to_string((double) tt).substr(0,5) );
			}
		}
	}

	mpsIBC.clear();
	mpsInf1.clear();
	mpsInf2.clear();

	return 0;
}
