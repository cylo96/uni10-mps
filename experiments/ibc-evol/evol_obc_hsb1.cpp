#include <sstream>

#include <mps.hpp>

//======================================

int main(int argc, char* argv[]) {

	// import H
	uni10::CUniTensor hw = uni10::CUniTensor("ham_hsb1");
	uni10::CUniTensor op = uni10::CUniTensor("sz1");
	uni10::CUniTensor flip = uni10::CUniTensor("sp1");

	int d = hw.bond()[0].dim();
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;

	// initialize heisenberg chain
	ChainOBC hsbChain(L, d, X);
	hsbChain.randomize();

	// imaginary time evolve to ~ g.s.
	Complex dT (0.0, 0.02);
	hsbChain.tebd( hw, hw, hw, dT, 5000);

	// excitation
	hsbChain.oneSiteOP( flip, L/2 - 1 );

	// initial measure
	std::vector<uni10::UniTensor> expV;

	for (int i = 0; i < L; ++i) {
		expV.push_back( hsbChain.expVal( op, i ) );
		std::cout << i << "\t" << std::setprecision(10) << expV[i][0] << std::endl;
	}
	expV.clear();
	std::cout << std::endl;

	// evolve and measure
	Complex dt (0.05, 0.0);

	for (int ite = 0; ite < 10; ++ite) {

		hsbChain.tebd( hw, hw, hw, dt, 20 );

		for (int i = 0; i < L; ++i) {
			expV.push_back( hsbChain.expVal( op, i ) );
			std::cout << i << "\t" << std::setprecision(10) << expV[i][0] + (ite+1)*0.75 << std::endl;
			//std::cout << i << "\t" << std::setprecision(10) << expV[i][0] << std::endl;
		}
		expV.clear();
		std::cout << std::endl;
	}

	return 0;
}


