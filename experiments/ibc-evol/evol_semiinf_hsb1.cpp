#include <sstream>

#include <mps.hpp>

//=================================

int main(int argc, char* argv[]) {

	// import H
	uni10::CUniTensor hlw = uni10::CUniTensor("ham_hsb1");
	uni10::CUniTensor hw = uni10::CUniTensor("ham_hsb1");
	uni10::CUniTensor hwr = uni10::CUniTensor("HWR");
	uni10::CUniTensor hr = uni10::CUniTensor("HR");
	uni10::CUniTensor op = uni10::CUniTensor("sz1");
	uni10::CUniTensor flip = uni10::CUniTensor("sp1");

	// initialize heisenberg chain
	int d = hw.bond()[0].dim();
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;

	ChainSemiInf hsbChain(L, d, X);
/*
	// It seems a boundary forms between the randomized last site and eff-inf hamiltonian
	// as long as using traditional -l-g-l-g-l- updating method
	// todo: try Ian's updating method for semi-inf chain
	hsbChain.randomize();
	hsbChain.putLambda( uni10::CUniTensor("lambdaB"), L );
	// end of this block
*/
	hsbChain.importMPS( "mps-inf" );

	// imaginary time evolve to ~ g.s.
	Complex dT (0.0, 0.1);
	hsbChain.tebd( hlw, hw, hwr, hr, dT, 1000);
	dT = Complex(0.0, 0.01);
	hsbChain.tebd( hlw, hw, hwr, hr, dT, 1000);
	dT = Complex(0.0, 0.001);
	hsbChain.tebd( hlw, hw, hwr, hr, dT, 1000);
	dT = Complex(0.0, 0.0001);
	hsbChain.tebd( hlw, hw, hwr, hr, dT, 1000);

	// excitation
	hsbChain.oneSiteOP( flip, L/2 - 1 );

	// measure
	std::vector<uni10::UniTensor> expV;

	for (int i = 0; i < L; ++i) {
		expV.push_back( hsbChain.expVal( op, i ) );
		std::cout << i << "\t" << std::setprecision(10) << expV[i][0] << std::endl;
	}
	expV.clear();
	std::cout << std::endl;

	// evolve and measure
	Complex dt (0.05, 0.0);

	for (int ite = 0; ite < 10; ++ite) {

		hsbChain.tebd( hlw, hw, hwr, hr, dt, 20);
		for (int i = 0; i < L; ++i) {
			expV.push_back( hsbChain.expVal( op, i ) );
			std::cout << i << "\t" << std::setprecision(10) << expV[i][0] + (ite+1)*0.75 << std::endl;
		}
		expV.clear();
		std::cout << std::endl;
	}

	return 0;
}


