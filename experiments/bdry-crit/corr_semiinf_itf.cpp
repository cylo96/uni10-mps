#include <sstream>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

int main(int argc, char* argv[]) {

	// import H
	uni10::CUniTensor hlw = uni10::CUniTensor("ham_itfb");
	uni10::CUniTensor hw = uni10::CUniTensor("ham_itf");
	uni10::CUniTensor hwr = uni10::CUniTensor("HWR");
	uni10::CUniTensor hr = uni10::CUniTensor("HR");

	// initialize heisenberg chain
	int d = hw.bond()[0].dim();
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;

	ChainSemiInf itfChain(L, d, X);
/*
	itfChain.randomize();
*/
	itfChain.importMPS( "mps-inf" );

	Complex dT (0.0, 0.1);
	itfChain.tebd( hlw, hw, hwr, hr, dT, 2000);
	dT = Complex(0.0, 0.01);
	itfChain.tebd( hlw, hw, hwr, hr, dT, 2000);
	dT = Complex(0.0, 0.001);
	itfChain.tebd( hlw, hw, hwr, hr, dT, 2000);
	dT = Complex(0.0, 0.0001);
	itfChain.tebd( hlw, hw, hwr, hr, dT, 2000);


	uni10::CUniTensor sx = uni10::CUniTensor("sx");
	uni10::CUniTensor sz = uni10::CUniTensor("sz");
	uni10::CUniTensor id = uni10::CUniTensor("id");
	uni10::CUniTensor norm = itfChain.expVal(id, 0);


	/// calculate Sx(r)
	double Xr = 0.0;
	double base = 2./M_PI;

	for (int r = 1; r < L; ++r) {

		Xr = itfChain.correlation( id, sx, 0, 0+r )[0].real()/norm[0].real();
		std::cout << r << "\t" << std::setprecision(10) << std::fabs(Xr)-base << "\n";
	}
	/// end of block

/*
	/// calculate < Sx(0) Sx(r) > - |< Sx(?) >|^2
	double corr = 0.0;
	uni10::CUniTensor exp0 = itfChain.expVal(sx, 0);
	//double base = std::pow( exp0[0].real()/norm[0].real(), 2 );
	//double base = std::pow( 2./M_PI, 2 );
	uni10::CUniTensor expr;
	double base;

	std::cout << "#r\t<Sx(0)Sx(r)>\t<Sx(0)Sx(r)>-<Sx(0)><Sx(r)>\n";
	for (int r = 1; r < L; ++r) {

		expr = itfChain.expVal(sx, r);
		base = (exp0[0].real()/norm[0].real()) * (expr[0].real()/norm[0].real());
		corr = itfChain.correlation( sx, sx, 0, 0+r )[0].real()/norm[0].real();
		std::cout << r << "\t" << std::setprecision(10) 
					<< corr << "\t" << corr-base << "\n";
	}
	/// end of block
*/
/*
	/// calculate < Sz(0) Sz(r) >
	double corr = 0.0;
	//uni10::CUniTensor exp0 = itfChain.expVal(sx, 0);
	//double base = std::pow( exp0[0].real()/norm[0].real(), 2 );
	//double base = 0.0;

	std::cout << "#r\t<Sz(0)Sz(r)>\n";
	for (int r = 1; r < L; ++r) {

		corr = itfChain.correlation( sz, sz, 0, 0+r )[0].real()/norm[0].real();
		std::cout << r << "\t" << std::setprecision(10) << corr << "\n";
	}
	/// end of block
*/

	itfChain.exportMPS( "mps-semiinf-itf" );
	return 0;
}

