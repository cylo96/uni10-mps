#include <sstream>
#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

int main(int argc, char* argv[]) {

	// import H
	uni10::CUniTensor hw = uni10::CUniTensor("ham_itf");
	uni10::CUniTensor hwr = uni10::CUniTensor("HWR");
	uni10::CUniTensor hr = uni10::CUniTensor("HR");

	// import operators
	uni10::CUniTensor sx = uni10::CUniTensor("sx");
	uni10::CUniTensor sz = uni10::CUniTensor("sz");
	uni10::CUniTensor id = uni10::CUniTensor("id");

	double h = 1.0;
	double A = 1000;
	double M0 = 1.0;

	if ( argc > 4 ) {
		if ( argc > 5 ) {
			std::stringstream(argv[3]) >> h;
			std::stringstream(argv[4]) >> A;
			std::stringstream(argv[5]) >> M0;
		}
		else {
			std::stringstream(argv[3]) >> A;
			std::stringstream(argv[4]) >> M0;
		}
	}

	uni10::CUniTensor hlw = 
		(-1.0) * uni10::otimes(sz, sz) + h * uni10::otimes(sx, id) + 0.5 * h * uni10::otimes(id, sx)
		+ (-1.0) * A * ( M0 * uni10::otimes(sz, id) + sqrt(1.0 - M0*M0) * uni10::otimes(sx, id) );


	// initialize heisenberg chain
	int d = hw.bond()[0].dim();
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;

	ChainSemiInf itfChain(L, d, X);
	itfChain.importMPS( "mps-inf" );

	Complex dT (0.0, 0.1);
	itfChain.tebd( hlw, hw, hwr, hr, dT, 5000 );
	dT = Complex(0.0, 0.01);
	itfChain.tebd( hlw, hw, hwr, hr, dT, 5000 );
	dT = Complex(0.0, 0.001);
	itfChain.tebd( hlw, hw, hwr, hr, dT, 5000 );
	dT = Complex(0.0, 0.0001);
	itfChain.tebd( hlw, hw, hwr, hr, dT, 5000 );
	dT = Complex(0.0, 0.00001);
	itfChain.tebd( hlw, hw, hwr, hr, dT, 5000 );

	double norm = itfChain.expVal(id, 0)[0].real();
	double expv, ent;

	std::cout << "#D\t|<Sz(D)>|\tentropy(D|D+1)\n";
	for (int D = 0; D < L; ++D) {

		expv = itfChain.expVal( sz, D )[0].real()/norm;
		ent = entanglementEntropy( itfChain.getLambda(D+1) );
		std::cout << D << "\t" << std::setprecision(10) << expv << "\t" << ent << "\n";
	}

	itfChain.exportMPS( "mps-semiinf-itf" );
	return 0;
}

