#include <sstream>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

std::vector<uni10::CUniTensor> lrVec(
	const std::vector<uni10::CUniTensor>& gams,
	const std::vector<uni10::CUniTensor>& lams,
	bool left = true ) {
    ///
	std::vector<uni10::CUniTensor> blk;

	int L = gams.size();
	for (int i = 0; i < L; ++i)
		blk.push_back( uni10::CUniTensor() );

	uni10::CUniTensor lvec;
	uni10::CUniTensor rvec;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	if (left) {

		int lab_lvec[]= {0, 1};
		int lab_bra1[]= {0, 100, 2};
		int lab_ket1[]= {1, 100, 3};

		lvec.assign( lams[0].bond() );
		lvec.identity();
		lvec.permute(0);
		lvec.setLabel( lab_lvec );

		for (int i = 0; i < L; ++i) {

			ket = netLG( lams[i], gams[i] );
			bra = ket;
			bra.conj();

			bra.setLabel( lab_bra1 );
			ket.setLabel( lab_ket1 );

			lvec = uni10::contract( bra, lvec );
			lvec = uni10::contract( lvec, ket );
			lvec.setLabel( lab_lvec );

			blk[i] = lvec;
			blk[i].permute(0);
		}
	}

	else {
		int lab_rvec[]= {2, 3};
		int lab_bra2[]= {0, 100, 2};
		int lab_ket2[]= {1, 100, 3};

		rvec.assign( lams[L].bond() );
		rvec.identity();
		rvec.permute(2);
		rvec.setLabel( lab_rvec );

		for (int i = L-1; i >= 0; --i) {

			ket = netGL( gams[i], lams[i+1] );
			bra = ket;
			bra.conj();

			bra.setLabel( lab_bra2 );
			ket.setLabel( lab_ket2 );

			rvec = uni10::contract( bra, rvec );
			rvec = uni10::contract( rvec, ket );
			rvec.setLabel( lab_rvec );

			blk[i] = rvec;
			blk[i].permute(2);
		}
	}

    return blk;
}

//======================================

uni10::CUniTensor calcExpV( ChainIBC& mpsIBC, uni10::CUniTensor& O1, int loc,
	const std::vector<uni10::CUniTensor>& lvecs, const std::vector<uni10::CUniTensor>& rvecs ) {
	///
	uni10::CUniTensor expv;
	uni10::CUniTensor lv, rv, bra, ket;
	int L = mpsIBC.getSize();
	int obn = O1.bondNum();
	int skip = 0;

	int lab_lvec[]= {0, 1};
	int lab_rvec[]= {2, 3};
	int lab_bra1[]= {0, 100, 2};
	int lab_op1[] = {100, 200};
	int lab_ket1[]= {1, 200, 3};
	int lab_bra2[]= {0, 100, 101, 2};
	int lab_op2[] = {100, 101, 200, 201};
	int lab_ket2[]= {1, 200, 201, 3};

	if (obn == 2) {
		ket = netLGL( mpsIBC.getLambda(loc), mpsIBC.getGamma(loc), mpsIBC.getLambda(loc+1) );
		bra = ket;
		bra.setLabel( lab_bra1 );
		O1.setLabel( lab_op1 );
		ket.setLabel( lab_ket1 );
		skip = 0;
	}
	else if (obn == 4) {
		ket = netLGLGL( mpsIBC.getLambda(loc), mpsIBC.getGamma(loc), 
			mpsIBC.getLambda(loc+1), mpsIBC.getGamma(loc+1), mpsIBC.getLambda(loc+2) );
		bra = ket;
		bra.setLabel( lab_bra2 );
		O1.setLabel( lab_op2 );
		ket.setLabel( lab_ket2 );
		skip = 1;
	}
	bra.conj();

	if (loc == 0) {
		lv.assign( lvecs[0].bond() );
		lv.permute(1);
		lv.identity();
		rv = rvecs[1+skip];
	}
	else if (loc == L-1-skip) {
		lv = lvecs[L-2-skip];
		rv.assign( rvecs[0].bond() );
		rv.permute(1);
		rv.identity();
	}
	else {
		lv = lvecs[loc-1];
		rv = rvecs[loc+1+skip];
	}

	lv.setLabel( lab_lvec );
	rv.setLabel( lab_rvec );

	expv = uni10::contract( bra, lv );
	expv = uni10::contract( expv, O1 );
	expv = uni10::contract( expv, ket );
	expv = uni10::contract( expv, rv );

	return expv;
}

//======================================

uni10::CUniTensor calcCorr( ChainIBC& mpsIBC,
	uni10::CUniTensor& O1, uni10::CUniTensor& O2, int loc1, int loc2,
	const std::vector<uni10::CUniTensor>& lvecs, const std::vector<uni10::CUniTensor>& rvecs ) {
	///
	uni10::CUniTensor corr;
	uni10::CUniTensor lv, rv, bra, ket, op;
	int L = mpsIBC.getSize();
	int obn = O1.bondNum();
	int skip = (obn/2)-1;

	int lab_lvec[]= {0, 1};
	int lab_rvec[]= {2, 3};
	int lab_bra0[]= {0, 100, 2};
	int lab_ket0[]= {1, 100, 3};
	int lab_bra1[]= {0, 100, 2};
	int lab_op1[] = {100, 200};
	int lab_ket1[]= {1, 200, 3};
	int lab_bra2[]= {0, 100, 101, 2};
	int lab_op2[] = {100, 101, 200, 201};
	int lab_ket2[]= {1, 200, 201, 3};

	// build head
	if (loc1 == 0) {
		lv.assign( lvecs[0].bond() );
		lv.permute(1);
		lv.identity();
	}
	else if (loc1 == L-1-skip)
		lv = lvecs[L-2-skip];
	else
		lv = lvecs[loc1-1];

	// build tail
	uni10::CUniTensor lead_lam = mpsIBC.getLambda(loc2+1+skip);
	if (loc2 == L-1-skip)
		rv = netLL( lead_lam, lead_lam );
	else {
		rv = netLL( lead_lam, rvecs[loc2+1+skip] );
		rv = netLL( rv, lead_lam );
	}

	// contract body
	for (int i = loc1; i <= loc2; ++i) {

		if (i == loc1 || i == loc2) {
			op = (i == loc1)? O1 : O2;
			if (obn == 2) {
				ket = netLG( mpsIBC.getLambda(i), mpsIBC.getGamma(i) );
				bra = ket;
				bra.setLabel( lab_bra1 );
				op.setLabel( lab_op1 );
				ket.setLabel( lab_ket1 );
				skip = 0;
			}
			else if (obn == 4) {
				ket = netLGLG( mpsIBC.getLambda(i), mpsIBC.getGamma(i), 
					mpsIBC.getLambda(i+1), mpsIBC.getGamma(i+1) );
				bra = ket;
				bra.setLabel( lab_bra2 );
				op.setLabel( lab_op2 );
				ket.setLabel( lab_ket2 );
				skip = 1;
			}
		}
		else {
			ket = netLG( mpsIBC.getLambda(i), mpsIBC.getGamma(i) );
			bra = ket;
			bra.setLabel( lab_bra0 );
			ket.setLabel( lab_ket0 );
			skip = 0;
		}
		bra.conj();

		lv.setLabel( lab_lvec );

		lv = uni10::contract( bra, lv );
		if (i == loc1 || i == loc2)
			lv = uni10::contract( lv, op );
		lv = uni10::contract( lv, ket );

		i += skip;
	}

	rv.setLabel( lab_rvec );
	corr = uni10::contract( lv, rv );

	return corr;
}

//======================================

void corrOO( ChainIBC& mpsIBC, int L, 
	uni10::CUniTensor& O1, uni10::CUniTensor& O2, uni10::CUniTensor& id,
	int offset_l, int offset_r, bool detail ) {

	/// calculate < O1(r1) O2(r2) > - < O(r1) > < O(r2) >
	const std::vector<uni10::CUniTensor>& gams = mpsIBC.getGamVec();
	const std::vector<uni10::CUniTensor>& lams = mpsIBC.getLamVec();
	std::vector<uni10::CUniTensor> lvecs = lrVec( gams, lams, true );
	std::vector<uni10::CUniTensor> rvecs = lrVec( gams, lams, false );

	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expl, expr, corT;
	uni10::CUniTensor norm = calcExpV( mpsIBC, id, 0, lvecs, rvecs );

	int mid = L/2 - 1;
	int r1, r2;
	int ibn1 = O1.inBondNum();
	int ibn2 = O2.inBondNum();

	int r = 0;
	while ( r < L/2 - std::max(std::max(offset_l, offset_r), ibn2-1) ) {

		r1 = mid-r-offset_l;
		r2 = mid+r+offset_r;
		expl = calcExpV( mpsIBC, O1, r1, lvecs, rvecs );
		expr = calcExpV( mpsIBC, O2, r2, lvecs, rvecs );
		corT = calcCorr( mpsIBC, O1, O2, r1, r2, lvecs, rvecs );

		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		corr = ( corT[0].real()/norm[0].real() );
		std::cout << ((float)(r2-r1-(ibn1-1)-std::min(ibn1-1, ibn2-1)))/(std::min(ibn1, ibn2)) << '\t' << std::setprecision(10) 
			<< corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\n';

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//======================================

void corrOO_half( ChainIBC& mpsIBC, int L, 
	uni10::CUniTensor& O1, uni10::CUniTensor& O2, uni10::CUniTensor& id,
	int offset_l, int offset_r, bool left, bool detail ) {

	/// calculate < O1(r1) O2(r2) > - < O1(r1) > < O2(r2) >
	const std::vector<uni10::CUniTensor>& gams = mpsIBC.getGamVec();
	const std::vector<uni10::CUniTensor>& lams = mpsIBC.getLamVec();
	std::vector<uni10::CUniTensor> lvecs = lrVec( gams, lams, true );
	std::vector<uni10::CUniTensor> rvecs = lrVec( gams, lams, false );

	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expl, expr, corT;
	uni10::CUniTensor norm = calcExpV( mpsIBC, id, 0, lvecs, rvecs );

	int mid = L/2 - 1;
	int r1, r2;
	int ibn1 = O1.inBondNum();
	int ibn2 = O2.inBondNum();

	int r = 1;
	while ( r < L/2 - std::max(std::max(offset_l, offset_r), ibn2-1) ) {

		if (left) {
			r1 = mid-r-offset_l;
			r2 = mid;
		}
		else {
			r1 = mid+1;
			r2 = mid+1+r+offset_r;
		}

		expl = calcExpV( mpsIBC, O1, r1, lvecs, rvecs );
		expr = calcExpV( mpsIBC, O2, r2, lvecs, rvecs );
		corT = calcCorr( mpsIBC, O1, O2, r1, r2, lvecs, rvecs );

		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		corr = ( corT[0].real()/norm[0].real() );

		std::cout << r2-r1 << '\t' << std::setprecision(10)
			<< corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\n';

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//======================================

int main(int argc, char* argv[]) {


	// initialize XXZ chain
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;

	std::string mode = "JlJr";
	if (argc > 3) {
		if ( std::string( argv[3] ) == "SpSm" )
			mode = std::string( argv[3] );
		else if ( std::string( argv[3] ) == "SmSp" )
			mode = std::string( argv[3] );
		else if ( std::string( argv[3] ) == "SzSz" )
			mode = std::string( argv[3] );
		else if ( std::string( argv[3] ) == "SpSmHalf" )
			mode = std::string( argv[3] );
		else if ( std::string( argv[3] ) == "SzSzHalf" )
			mode = std::string( argv[3] );
	}

	std::string wf_dir = "mps-inf";
	if (argc > 4)
		wf_dir = std::string( argv[4] );

	bool default_offset = true;
	int offset_l, offset_r;
	if (argc > 6) {
		default_offset = false;
		std::istringstream(argv[5]) >> offset_l;
		std::istringstream(argv[6]) >> offset_r;
	}

	int d = uni10::CUniTensor( wf_dir + "/gamma_0" ).bond()[1].dim();
	ChainIBC mpsIBC(L, d, X);
	mpsIBC.importMPS( wf_dir );


	if ( mode == "JlJr" ) {
		// import operators
		uni10::CUniTensor id = uni10::CUniTensor("id");
		uni10::CUniTensor Jl = uni10::CUniTensor("Jl");
		uni10::CUniTensor Jr = uni10::CUniTensor("Jr");
		//====== calculate corrJJ ======
		if ( default_offset ) {
			offset_l = 1;
			offset_r = 1;
		}
		std::cout << "#r\t<Jl(r1) Jr(r2)>\t<Jl(r1) Jr(r2)> - <Jl(r1)> <Jr(r2)>\t<Jl(r1)>\t<Jr(r2)>\n";
		corrOO( mpsIBC, L, Jl, Jr, id, offset_l, offset_r, true );
	}
	else if ( mode == "SpSm" ) {
		// import operators
		uni10::CUniTensor id = uni10::CUniTensor("id");
		uni10::CUniTensor Sp = uni10::CUniTensor("sp");
		uni10::CUniTensor Sm = uni10::CUniTensor("sm");
		//====== calculate corrSS ======
		if ( default_offset ) {
			offset_l = 0;
			offset_r = 1;
		}
		std::cout << "#r\t<Sp(r1) Sm(r2)>\t<Sp(r1) Sm(r2)> - <Sp(r1)> <Sm(r2)>\t<Sp(r1)>\t<Sm(r2)>\n";
		corrOO( mpsIBC, L, Sp, Sm, id, offset_l, offset_r, true );
	}
	else if ( mode == "SmSp" ) {
		// import operators
		uni10::CUniTensor id = uni10::CUniTensor("id");
		uni10::CUniTensor Sm = uni10::CUniTensor("sm");
		uni10::CUniTensor Sp = uni10::CUniTensor("sp");
		//====== calculate corrSS ======
		if ( default_offset ) {
			offset_l = 0;
			offset_r = 1;
		}
		std::cout << "#r\t<Sm(r1) Sp(r2)>\t<Sm(r1) Sp(r2)> - <Sm(r1)> <Sp(r2)>\t<Sm(r1)>\t<Sp(r2)>\n";
		corrOO( mpsIBC, L, Sm, Sp, id, offset_l, offset_r, true );
	}
	else if ( mode == "SzSz" ) {
		// import operators
		uni10::CUniTensor id = uni10::CUniTensor("id");
		uni10::CUniTensor Sz = uni10::CUniTensor("sz");
		//====== calculate corrSS ======
		if ( default_offset ) {
			offset_l = 0;
			offset_r = 1;
		}
		std::cout << "#r\t<Sz(r1) Sz(r2)>\t<Sz(r1) Sz(r2)> - <Sz(r1)> <Sz(r2)>\t<Sz(r1)>\t<Sz(r2)>\n";
		corrOO( mpsIBC, L, Sz, Sz, id, offset_l, offset_r, true );
	}
	else if ( mode == "SpSmHalf" ) {
		// import operators
		uni10::CUniTensor id = uni10::CUniTensor("id");
		uni10::CUniTensor Sp = uni10::CUniTensor("sp");
		uni10::CUniTensor Sm = uni10::CUniTensor("sm");
		//====== calculate corrSS ======
		if ( default_offset ) {
			offset_l = 0;
			offset_r = 0;
		}
		std::cout << "#r\t<Sp(r1) Sm(r2)>\t<Sp(r1) Sm(r2)> - <Sp(r1)> <Sm(r2)>\t<Sp(r1)>\t<Sm(r2)>\n";
		corrOO_half( mpsIBC, L, Sp, Sm, id, offset_l, offset_r, false, true );
	}
	else if ( mode == "SzSzHalf" ) {
		// import operators
		uni10::CUniTensor id = uni10::CUniTensor("id");
		uni10::CUniTensor Sz = uni10::CUniTensor("sz");
		//====== calculate corrSS ======
		if ( default_offset ) {
			offset_l = 0;
			offset_r = 0;
		}
		std::cout << "#r\t<Sz(r1) Sz(r2)>\t<Sz(r1) Sz(r2)> - <Sz(r1)> <Sz(r2)>\t<Sz(r1)>\t<Sz(r2)>\n";
		corrOO_half( mpsIBC, L, Sz, Sz, id, offset_l, offset_r, false, true );
	}

	return 0;
}

