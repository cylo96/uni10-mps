#include <sstream>
#include <math.h>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

void corrOO( ChainOBC& xxzChain, int L, 
	uni10::CUniTensor& O1, uni10::CUniTensor& O2, uni10::CUniTensor& id, bool detail ) {

	/// calculate < O1(0) O2(r) > - < O(0) > < O(r) >
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor exp0, expr;
	uni10::CUniTensor norm = xxzChain.expVal(id, 0);
	exp0 = xxzChain.expVal(O1, 0);
	exp1 = exp0[0].real()/norm[0].real();

	int r = 1;
	while ( r < L-(O2.inBondNum()-1) ) {

		expr = xxzChain.expVal(O2, r);
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		corr = ( xxzChain.correlation( O1, O2, 0, r )[0].real()/norm[0].real() );
		std::cout << r << '\t' << std::setprecision(10)
			<< corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\n';

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//======================================

void corrOO_mid( ChainOBC& xxzChain, int L, 
	uni10::CUniTensor& O1, uni10::CUniTensor& O2, uni10::CUniTensor& id, bool detail ) {

	/// calculate < O1(r1) O2(r2) > - < O(r1) > < O(r2) >
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expl, expr;
	uni10::CUniTensor norm = xxzChain.expVal(id, 0);

	int marl = O1.inBondNum()-1;
	int marr = O2.inBondNum()-1;
	int mid = L/2 - 1;
	int r = 0;
	while ( r < L/2-marr ) {

		expl = xxzChain.expVal(O1, mid-r-marl);
		expr = xxzChain.expVal(O2, mid+r+1);
		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		corr = ( xxzChain.correlation( O1, O2, mid-r-marl, mid+r+1 )[0].real()/norm[0].real() );
		std::cout << (2/std::min(O1.inBondNum(), O2.inBondNum())) * r + (1-std::min(marl, marr)) << '\t' 
			<< std::setprecision(10) << corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\n';

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//======================================

int main(int argc, char* argv[]) {

	// initialize XXZ chain
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;
	std::string O1str = std::string( argv[3] );
	std::string O2str = std::string( argv[4] );

	std::string wf_dir = "mps-inf";
	if (argc > 5)
		wf_dir = std::string( argv[5] );

	bool from_mid = true;
	if (argc > 6) {
		int method;
		std::istringstream(argv[6]) >> method;
		from_mid = (bool) method;
	}

	int d = uni10::CUniTensor( wf_dir + "/gamma_0" ).bond()[1].dim();
	ChainOBC xxzChain(L, d, X);
	xxzChain.importMPS( wf_dir );

	// import operators
	uni10::CUniTensor id = OP("id");
	uni10::CUniTensor O1 = uni10::CUniTensor(O1str);
	uni10::CUniTensor O2 = uni10::CUniTensor(O2str);
	//====== calculate corrOO ======
	std::cout << "#r\t<"+O1str+"(r1) "+O2str+"(r2)>\t<"+O1str+"(r1) "+O2str+"(r2)> - <"+O1str+"(r1)> <"+O2str+"(r2)>\t<"+O1str+"(r1)>\t<"+O2str+"(r2)>\n";

	if (from_mid)
		corrOO_mid( xxzChain, L, O1, O2, id, true );
	else
		corrOO( xxzChain, L, O1, O2, id, true );

	return 0;
}

