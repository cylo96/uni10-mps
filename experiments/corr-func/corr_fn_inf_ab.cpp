#include <iostream>
#include <sstream>

#include <mps/ChainInf.h>
#include <tns-func/func_net.h>

//=========================================

std::vector<int> two_n( int i ) {

	std::vector<int> powers;
	int rmd;

	while ( i >= 1 ) {
		rmd = i % 2;
		powers.push_back(rmd);
		i /= 2;
	}

	return powers;
}

//=========================================

uni10::CUniTensor correlator( 
	std::vector<uni10::CUniTensor>& As, std::vector<uni10::CUniTensor>& Bs, uni10::CUniTensor& Ls, 
	uni10::CUniTensor op1, uni10::CUniTensor op2, int r ) {

	int labA0[] = {0, 100, 1};
	int labA1[] = {1, 101, 2};
	As[0].setLabel( labA0 );
	As[1].setLabel( labA1 );

	uni10::CUniTensor ket = As[0] * As[1];
	ket.permute(ket.label(), 3);
	uni10::CUniTensor bra = ket;
	bra.permute(bra.label(), 1).conj();

	if ( op1.bondNum() == 2 ) {
		int lab_bra[] = {0, 200, 101, 1};
		int lab_ket[] = {0, 100, 101, 2};
		int lab_op[]  = {200, 100};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);
		op1.setLabel(lab_op);
	}
	else if ( op1.bondNum() == 4 ) {
		int lab_bra[] = {0, 200, 201, 1};
		int lab_ket[] = {0, 100, 101, 2};
		int lab_op[]  = {200, 201, 100, 101};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);
		op1.setLabel(lab_op);
	}

	uni10::CUniTensor net = uni10::contract(op1, ket, false);
	net = uni10::contract(bra, net, false);
	net.permute(net.label(), 0);

	int lab_last[2];

	if ( r > 1 ) {

		uni10::CUniTensor id( Ls.bond() );
		id.identity();
		uni10::CUniTensor tM = uni10::otimes( id, id );

		int lab_tM[] = {1, 2, 10, 11};
		int lab_tM2[] = {10, 11, 20, 21};
		tM.setLabel( lab_tM );

		std::vector<int> powers = two_n( r-1 );
		uni10::CUniTensor tM2;

		for (int i = 0; i < powers.size(); ++i) {

			if (powers[i] == 1) {
				tM2 = tranMtxChain( ket, i );
				tM2.setLabel( lab_tM2 );
				tM = uni10::contract( tM, tM2, false );
				tM.setLabel( lab_tM );
			}
		}

		net = uni10::contract(net, tM, false);

		lab_last[0] = 10;
		lab_last[1] = 11;
	}
	else {
		lab_last[0] = 1;
		lab_last[1] = 2;
	}

	int labB0[] = {1, 101, 0};
	int labB1[] = {2, 100, 1};
	int labLs[] = {3, 2};
	Bs[0].setLabel( labB0 );
	Bs[1].setLabel( labB1 );
	Ls.setLabel( labLs );
	Bs[0].permute( labB0, 2 );
	Bs[1].permute( labB1, 2 );

	uni10::CUniTensor last_ket = Ls * Bs[1] * Bs[0];
	last_ket.permute( last_ket.label(), 3 );
	uni10::CUniTensor last_bra = last_ket;
	last_bra.permute( last_bra.label(), 1 ).conj();

	if ( op2.bondNum() == 2 ) {
		int lab_last_bra[] = {10, 200, 101, 12};
		int lab_last_ket[] = {11, 100, 101, 12};
		int lab_op2[] = {200, 100};
		last_ket.setLabel(lab_last_ket);
		last_bra.setLabel(lab_last_bra);
		op2.setLabel(lab_op2);
	}
	else if ( op2.bondNum() == 4 ) {
		int lab_last_bra[] = {10, 200, 201, 12};
		int lab_last_ket[] = {11, 100, 101, 12};
		int lab_op2[] = {200, 201, 100, 101};
		last_ket.setLabel(lab_last_ket);
		last_bra.setLabel(lab_last_bra);
		op2.setLabel(lab_op2);
	}

	uni10::CUniTensor last_block = uni10::contract(op2, last_ket, false);
	last_block = uni10::contract(last_bra, last_block, false);
	last_block.permute(last_block.label(), 2);

	last_block.setLabel(lab_last);
	net = uni10::contract(net, last_block, false);	

	return net;
}

//=========================================

int main( int argc, char* argv[] ) {

	if (argc < 4) {

		std::cerr << "Not Enough Arguments!" << std::endl;
		std::cerr << "Usage: " << argv[0] << " <op1_file> <op2_file> <distance> [option]" << std::endl;
		std::cerr << "Allowed options:" << std::endl;
		std::cerr << "-mean  to subtract |<op1>|^2" << std::endl;
		return 1;
	}

	std::string wf_dir = "mps-inf";
	std::string op1f, op2f;
	op1f = std::string(argv[1]);
	op2f = std::string(argv[2]);
	int r_max;
	std::istringstream(argv[3]) >> r_max;

	std::vector<uni10::CUniTensor> As;
	std::vector<uni10::CUniTensor> Bs;
	As.push_back( uni10::CUniTensor( wf_dir + "/A0" ) );
	As.push_back( uni10::CUniTensor( wf_dir + "/A1" ) );
	Bs.push_back( uni10::CUniTensor( wf_dir + "/B0" ) );
	Bs.push_back( uni10::CUniTensor( wf_dir + "/B1" ) );

	uni10::CUniTensor Ls( wf_dir + "/Ls" );

	int d = As[0].bond()[1].dim();
	int X = As[0].bond()[0].dim();

	uni10::CUniTensor op1( op1f );
	uni10::CUniTensor op2( op2f );

	std::vector< uni10::Bond > bdi;
	bdi.push_back( uni10::Bond( uni10::BD_IN, d ) );
	bdi.push_back( uni10::Bond( uni10::BD_OUT, d ) );
	uni10::CUniTensor id( bdi );
	id.identity();

	Complex correlation;

	for (int r = 1; r <= r_max/2; ++r) {

		if (r<=100 || (r>100 && r<=1000 && r%20==0) || 
			(r>1000 && r<=10000 && r%200==0) || (r>10000 && r%2000==0)) {

			uni10::CUniTensor corr = correlator( As, Bs, Ls, op1, op2, r );
			uni10::CUniTensor norm = correlator( As, Bs, Ls,  id,  id, r );

			if ( argc > 4 && std::string(argv[4]) == "-mean" ) {
				uni10::CUniTensor mean1 = correlator( As, Bs, Ls, op1, id, r );
				uni10::CUniTensor mean2 = correlator( As, Bs, Ls, id, op2, r );
				correlation = (corr[0] / norm[0]) - (mean1[0]/norm[0]) * (mean2[0]/norm[0]);
			}
			else {
				correlation = (corr[0] / norm[0]);
			}

			std::cout << 2*r << "\t" << std::setprecision(16) << correlation.real() << "\t" 
						<< std::log( 2.* (double)r ) << "\t" << std::log( correlation.real() ) <<"\n";
		}
	}

	return 0;
}

