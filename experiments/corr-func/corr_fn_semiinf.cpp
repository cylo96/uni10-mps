#include <sstream>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

void corrOO( ChainSemiInf& xxzChain, int L, 
	uni10::CUniTensor& O1, uni10::CUniTensor& O2, uni10::CUniTensor& id, bool detail ) {

	/// calculate < O1(0) O2(r) > - < O(0) > < O(r) >
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor exp0, expr;
	uni10::CUniTensor norm = xxzChain.expVal(id, 0);
	exp0 = xxzChain.expVal(O1, 0);
	exp1 = exp0[0].real()/norm[0].real();

	int r = 1;
	while ( r < L-(O2.inBondNum()-1) ) {

		expr = xxzChain.expVal(O2, r);
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		corr = ( xxzChain.correlation( O1, O2, 0, r )[0].real()/norm[0].real() );
		std::cout << r << '\t' << std::setprecision(10)
			<< corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\n';

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//======================================

int main(int argc, char* argv[]) {

	// initialize XXZ chain
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;
	std::string O1str = std::string( argv[3] );
	std::string O2str = std::string( argv[4] );

	std::string wf_dir = "mps-inf";
	if (argc > 5)
		wf_dir = std::string( argv[5] );

	int d = uni10::CUniTensor( wf_dir + "/gamma_0" ).bond()[1].dim();
	ChainSemiInf xxzChain(L, d, X);
	xxzChain.importMPS( wf_dir );

	// import operators
	uni10::CUniTensor id = OP("id");
	uni10::CUniTensor O1 = uni10::CUniTensor(O1str);
	uni10::CUniTensor O2 = uni10::CUniTensor(O2str);
	//====== calculate corrJJ ======
	std::cout << "#r\t<"+O1str+"(0) "+O2str+"(r)>\t<"+O1str+"(0) "+O2str+"(r)> - <"+O1str+"(0)> <"+O2str+"(r)>\t<"+O1str+"(0)>\t<"+O2str+"(r)>\n";
	corrOO( xxzChain, L, O1, O2, id, true );

	return 0;
}

