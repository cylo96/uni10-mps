#include <sstream>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

void corrJJ( ChainIBC& xxzChain, int L, 
	uni10::CUniTensor& Jl, uni10::CUniTensor& Jr, uni10::CUniTensor& id, bool detail ) {

	int mid = L/2 - 1;

	uni10::CUniTensor norm = xxzChain.expVal(id, 0);

	/// calculate |< Jl(r1) Jr(r2) > - < Jl(r1) > < Jr(r2) >|
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expl, expr;

	int r = 0;
	while ( r < L/2 -1 ) {

		expl = xxzChain.expVal(Jl, mid-r-1);
		expr = xxzChain.expVal(Jr, mid+r+1);
		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		corr = ( xxzChain.correlation( Jl, Jr, mid-r-1, mid+r+1 )[0].real()/norm[0].real() );
		std::cout << r << "\t" << std::setprecision(10) << corr << "\t" << corr-base << "\t"
			<< exp1 << "\t" << exp2 << "\n";

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//======================================

void corrSS( ChainIBC& xxzChain, int L, 
	uni10::CUniTensor& Sp, uni10::CUniTensor& Sm, uni10::CUniTensor& id, bool detail ) {

	int mid = L/2 - 1;

	uni10::CUniTensor norm = xxzChain.expVal(id, 0);

	/// calculate |< Sp(r1) Sm(r2) > - < Sp(r1) > < Sm(r2) >|
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expl, expr;

	int r = 0;
	while ( r < L/2 -1 ) {

		expl = xxzChain.expVal(Sp, mid-r);
		expr = xxzChain.expVal(Sm, mid+r+1);
		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		corr = ( xxzChain.correlation( Sp, Sm, mid-r, mid+r+1 )[0].real()/norm[0].real() );
		std::cout << 2*r+1 << "\t" << std::setprecision(10) << corr << "\t" << corr-base << "\t"
			<< exp1 << "\t" << exp2 << "\n";

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//======================================

void corrSS2( ChainIBC& xxzChain, int L, 
	uni10::CUniTensor& Sm, uni10::CUniTensor& Sp, uni10::CUniTensor& id ) {

	int mid = L/2 - 1;

	uni10::CUniTensor norm = xxzChain.expVal(id, 0);

	/// calculate |< Sp(r1) Sm(r2) > - < Sp(r1) > < Sm(r2) >|
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expl, expr;

	int r = 0;
	while ( r < L/2 -1 ) {

		expl = xxzChain.expVal(Sm, mid-r);
		expr = xxzChain.expVal(Sp, mid+r+1);
		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		//corr = ( xxzChain.correlation( Sm, Sp, mid-r, mid+r+1 )[0].real()/norm[0].real() );
		std::cout << 2*r+1 << "\t" << std::setprecision(10) << exp1 << "\t" << exp2 << "\n";

		if (r > 299)
			r += 20;
		else if (r > 99)
			r += 10;
		else if (r > 29)
			r += 2;
		else
			r += 1;
	}
}

//======================================

void corrSS_half( ChainIBC& xxzChain, int L, 
	uni10::CUniTensor& Sp, uni10::CUniTensor& Sm, uni10::CUniTensor& id, bool detail ) {

	int mid = L/2 - 1;

	uni10::CUniTensor norm = xxzChain.expVal(id, 0);

	/// calculate |< Sp(r1) Sm(r2) > - < Sp(r1) > < Sm(r2) >|
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expl, expr;

	int r = 1;
	while ( r < L/2 ) {

		expl = xxzChain.expVal(Sp, mid+1);
		expr = xxzChain.expVal(Sm, mid+r+1);
		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		corr = ( xxzChain.correlation( Sp, Sm, mid+1, mid+r+1 )[0].real()/norm[0].real() );
		std::cout << r << "\t" << std::setprecision(10) << corr << "\t" << corr-base << "\t"
			<< exp1 << "\t" << exp2 << "\n";

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//======================================

void corrZZ( ChainIBC& xxzChain, int L,
	uni10::CUniTensor& Sz, uni10::CUniTensor& id, bool detail ) {

	int mid = L/2 - 1;

	uni10::CUniTensor norm = xxzChain.expVal(id, 0);

	/// calculate |< Sz(r1) Sz(r2) > - < Sz(r1) > < Sz(r2) >|
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expl, expr;

	int r = 0;
	while ( r < L/2 -1 ) {

		expl = xxzChain.expVal(Sz, mid-r);
		expr = xxzChain.expVal(Sz, mid+r+1);
		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		corr = xxzChain.correlation( Sz, Sz, mid-r, mid+r+1 )[0].real()/norm[0].real();
		std::cout << 2*r+1 << "\t" << std::setprecision(10) << corr << "\t" << corr-base << '\t'
			<< exp1 << "\t" << exp2 << "\n";

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}


//======================================

int main(int argc, char* argv[]) {


	// initialize XXZ chain
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;

	std::string mode = "JlJr";
	if (argc > 3) {
		if ( std::string( argv[3] ) == "SpSm" )
			mode = std::string( argv[3] );
		else if ( std::string( argv[3] ) == "SmSp" )
			mode = std::string( argv[3] );
		else if ( std::string( argv[3] ) == "SzSz" )
			mode = std::string( argv[3] );
		else if ( std::string( argv[3] ) == "SpSmHalf" )
			mode = std::string( argv[3] );
	}

	std::string wf_dir = "mps-inf";
	if (argc > 4)
		wf_dir = std::string( argv[4] );

	int d = uni10::CUniTensor( wf_dir + "/gamma_0" ).bond()[1].dim();
	ChainIBC xxzChain(L, d, X);
	xxzChain.importMPS( wf_dir );

	int Lx = 1000;
	int expansion = (Lx-L)/2;
	xxzChain.expand(expansion, expansion, "mps-inf", 2);
	L = Lx;

	if ( mode == "JlJr" ) {
		// import operators
		uni10::CUniTensor id = uni10::CUniTensor("id");
		uni10::CUniTensor Jl = uni10::CUniTensor("Jl");
		uni10::CUniTensor Jr = uni10::CUniTensor("Jr");
		//====== calculate corrJJ ======
		std::cout << "#r\t<Jl(r1) Jr(r2)>\t<Jl(r1) Jr(r2)> - <Jl(r1)> <Jr(r2)>\n";
		corrJJ( xxzChain, L, Jl, Jr, id, true );
	}
	else if ( mode == "SpSm" ) {
		// import operators
		uni10::CUniTensor id = uni10::CUniTensor("id");
		uni10::CUniTensor Sp = uni10::CUniTensor("sp");
		uni10::CUniTensor Sm = uni10::CUniTensor("sm");
		//====== calculate corrSS ======
		std::cout << "#r\t<Sp(r1) Sm(r2)>\t<Sp(r1) Sm(r2)> - <Sp(r1)> <Sm(r2)>\t<Sp(r1)>\t<Sm(r2)>\n";
		corrSS( xxzChain, L, Sp, Sm, id, true );
	}
	else if ( mode == "SmSp" ) {
		// import operators
		uni10::CUniTensor id = uni10::CUniTensor("id");
		uni10::CUniTensor Sm = uni10::CUniTensor("sm");
		uni10::CUniTensor Sp = uni10::CUniTensor("sp");
		//====== calculate corrSS ======
		std::cout << "#r\t<Sm(r1)>\t<Sp(r2)>\n";
		corrSS2( xxzChain, L, Sm, Sp, id );
	}
	else if ( mode == "SzSz" ) {
		// import operators
		uni10::CUniTensor id = uni10::CUniTensor("id");
		uni10::CUniTensor Sz = uni10::CUniTensor("sz");
		//====== calculate corrZZ ======
		std::cout << "#r\t<Sz(r1) Sz(r2)>\t<Sz(r1) Sz(r2)> - <Sz(r1)> <Sz(r2)>\t<Sz(r1)>\t<Sz(r2)>\n";
		corrZZ( xxzChain, L, Sz, id, true );
	}
	else if ( mode == "SpSmHalf" ) {
		// import operators
		uni10::CUniTensor id = uni10::CUniTensor("id");
		uni10::CUniTensor Sp = uni10::CUniTensor("sp");
		uni10::CUniTensor Sm = uni10::CUniTensor("sm");
		//====== calculate corrSS ======
		std::cout << "#r\t<Sp(0) Sm(r)>\t<Sp(0) Sm(r)> - <Sp(0)> <Sm(r)>\t<Sp(0)>\t<Sm(r)>\n";
		corrSS_half( xxzChain, L, Sp, Sm, id, true );
	}

	return 0;
}

