#include <iostream>
#include <sstream>

#include <cmath>
#include <mkl.h>
#include <mkl_lapacke.h>

#include <mps/ChainInf.h>
#include <tns-func/func_net.h>
#include <tns-func/func_convert.h>

//=========================================

double power(double x, int n){

	double result = 1.0;

	for (int i=0; i<n; ++i)
		result *= x;

	return result;
}

//=========================================

uni10::CUniTensor corrEig( int r, 
	std::vector<uni10::CUniTensor>& As, std::vector<uni10::CUniTensor>& Bs, uni10::CUniTensor& Ls,
	uni10::CUniTensor op1, uni10::CUniTensor op2, 
	uni10::CUniTensor V, uni10::CUniTensor D, uni10::CUniTensor Vi ) {
	///
	int labA0[] = {0, 100, 1};
	int labA1[] = {1, 101, 2};
	As[0].setLabel( labA0 );
	As[1].setLabel( labA1 );

	uni10::CUniTensor ket = As[0] * As[1];
	ket.permute(ket.label(), 3);
	uni10::CUniTensor bra = ket;
	bra.permute(bra.label(), 1).conj();

	if ( op1.bondNum() == 2 ) {
		int lab_bra[] = {0, 200, 101, 1};
		int lab_ket[] = {0, 100, 101, 2};
		int lab_op[]  = {200, 100};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);
		op1.setLabel(lab_op);
	}
	else if ( op1.bondNum() == 4 ) {
		int lab_bra[] = {0, 200, 201, 1};
		int lab_ket[] = {0, 100, 101, 2};
		int lab_op[]  = {200, 201, 100, 101};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);
		op1.setLabel(lab_op);
	}

	uni10::CUniTensor net = uni10::contract(op1, ket, false);
	net = uni10::contract(bra, net, false);
	net.permute(net.label(), 0);

	int lab_last[2];

	if ( r > 1 ) {

		int lab_V[]  = {1, 2, 10, 11};
		int lab_D[]  = {10, 11, 20, 21};
		int lab_Vi[] = {20, 21, 30, 31};

		uni10::Matrix D_mtx = D.getBlock();
		for (int i = 0; i < D_mtx.row(); ++i)
			D_mtx.at(i, i) = power( D_mtx.at(i, i), r-1 );
		D.putBlock( D_mtx );

		V.setLabel( lab_V );
		D.setLabel( lab_D );
		Vi.setLabel( lab_Vi );

		net = uni10::contract(net, V, false);
		net = uni10::contract(net, D, false);
		net = uni10::contract(net, Vi, false);

		lab_last[0] = 10;
		lab_last[1] = 11;

		net.setLabel( lab_last );
	}
	else {
		lab_last[0] = 1;
		lab_last[1] = 2;
	}

	int labB0[] = {1, 101, 0};
	int labB1[] = {2, 100, 1};
	int labLs[] = {3, 2};
	Bs[0].setLabel( labB0 );
	Bs[1].setLabel( labB1 );
	Ls.setLabel( labLs );
	Bs[0].permute( labB0, 2 );
	Bs[1].permute( labB1, 2 );

	uni10::CUniTensor last_ket = Ls * Bs[1] * Bs[0];
	last_ket.permute( last_ket.label(), 3 );
	uni10::CUniTensor last_bra = last_ket;
	last_bra.permute( last_bra.label(), 1 ).conj();

	if ( op2.bondNum() == 2 ) {
		int lab_last_bra[] = {10, 200, 101, 12};
		int lab_last_ket[] = {11, 100, 101, 12};
		int lab_op2[] = {200, 100};
		last_ket.setLabel(lab_last_ket);
		last_bra.setLabel(lab_last_bra);
		op2.setLabel(lab_op2);
	}
	else if ( op2.bondNum() == 4 ) {
		int lab_last_bra[] = {10, 200, 201, 12};
		int lab_last_ket[] = {11, 100, 101, 12};
		int lab_op2[] = {200, 201, 100, 101};
		last_ket.setLabel(lab_last_ket);
		last_bra.setLabel(lab_last_bra);
		op2.setLabel(lab_op2);
	}

	uni10::CUniTensor last_block = uni10::contract(op2, last_ket, false);
	last_block = uni10::contract(last_bra, last_block, false);
	last_block.permute(last_block.label(), 2);

	last_block.setLabel(lab_last);
	net = uni10::contract(net, last_block, false);	

	return net;
}

//=========================================

uni10::Matrix mtxInv(uni10::Matrix mtx){

	int dim = mtx.col();
	double *tmp = mtx.getElem();

	//dgetrf( m, n, a, lda, ipiv, info );
	//dgetri( n, a, lda, ipiv, work, lwork, info );

	int *ipiv = new int [dim];
	int lwork, info;
	double wkopt;
	double *work;

	dgetrf( &dim, &dim, tmp, &dim, ipiv, &info );   // LU decomp

	/* Query and allocate the optimal workspace */
	lwork = -1;
	dgetri( &dim, tmp, &dim, ipiv, &wkopt, &lwork, &info );
	lwork = (int)wkopt;
	work = (double*)malloc( lwork*sizeof(double) );

	/* Inverse */
	dgetri( &dim, tmp, &dim, ipiv, work, &lwork, &info );

	/* Check validity */
	if( info > 0 ) {
		printf( "The algorithm failed to compute eigenvalues.\n" );
		exit( 1 );
	}

	uni10::Matrix inv(dim, dim);
	inv.setElem(tmp);

	delete [] work;

	return inv;
}

//=========================================

void mtxEigDecomp( uni10::CUniTensor ctM, 
	uni10::CUniTensor& cV, uni10::CUniTensor& cD, uni10::CUniTensor& cVi, uni10::CUniTensor& cIm ) {

	uni10::UniTensor tM = complex2Real( ctM );
	uni10::UniTensor V = complex2Real( cV );
	uni10::UniTensor D = complex2Real( cD );
	uni10::UniTensor Vi = complex2Real( cVi );
	uni10::UniTensor Im = complex2Real( cIm );

	std::vector<uni10::Bond> bd = tM.bond();
	V.assign( bd );
	D.assign( bd );
	Vi.assign( bd );
	Im.assign( bd );

	int chi = bd[0].dim();
	int dim = chi * chi;

	uni10::Matrix tMtx = tM.getBlock();
	double *mtx = tMtx.getElem();

	/* Locals */
	int N = dim;
	int LDA = N, LDVL = N, LDVR = N;
	int n = N, lda = LDA, ldvl = LDVL, ldvr = LDVR, info, lwork;
	double wkopt;
	double* work;
	/* Local arrays */
	double *wr = new double[N];
	double *wi = new double[N];
	double *vl = new double[LDVL*N];
	double *vr = new double[LDVR*N];

	/* Query and allocate the optimal workspace */
	lwork = -1;
	dgeev( "V", "V", &n, mtx, &lda, wr, wi, vr, &ldvr, vl, &ldvl,
											&wkopt, &lwork, &info );
	lwork = (int)wkopt;
	work = (double*)malloc( lwork*sizeof(double) );
	/* Solve eigenproblem */
	dgeev( "V", "V", &n, mtx, &lda, wr, wi, vr, &ldvr, vl, &ldvl,
											work, &lwork, &info );
	/* Check for convergence */
	if( info > 0 ) {
		std::cout << "The algorithm failed to compute eigenvalues.\n";
		exit( 1 );
	}

	uni10::Matrix mtx_V( N, N, vr, false );
	mtx_V.transpose();
	uni10::Matrix mtx_D( N, N, wr, true );
	uni10::Matrix mtx_Vi = mtxInv(mtx_V);
	uni10::Matrix mtx_Im( N, N, wi, true );

	V.putBlock( mtx_V );
	D.putBlock( mtx_D );
	Vi.putBlock( mtx_Vi );
	Im.putBlock( mtx_Im );

	cV = real2Complex( V );
	cD = real2Complex( D );
	cVi = real2Complex( Vi );
	cIm = real2Complex( Im );

	delete [] work;
	delete [] vl;
	delete [] vr;
}

//=========================================

int main( int argc, char* argv[] ) {

	if (argc < 4) {

		std::cerr << "Not Enough Arguments!" << std::endl;
		std::cerr << "Usage: " << argv[0] << " <op1_file> <op2_file> <distance> [option]" << std::endl;
		std::cerr << "Allowed options:" << std::endl;
		std::cerr << "-mean  to subtract |<op1>|^2" << std::endl;
		return 1;
	}

	std::string wf_dir = "mps-inf";
	std::string op1f, op2f;
	op1f = std::string(argv[1]);
	op2f = std::string(argv[2]);
	int r_max;
	std::istringstream(argv[3]) >> r_max;

	std::vector<uni10::CUniTensor> As;
	std::vector<uni10::CUniTensor> Bs;
	As.push_back( uni10::CUniTensor( wf_dir + "/A0" ) );
	As.push_back( uni10::CUniTensor( wf_dir + "/A1" ) );
	Bs.push_back( uni10::CUniTensor( wf_dir + "/B0" ) );
	Bs.push_back( uni10::CUniTensor( wf_dir + "/B1" ) );

	uni10::CUniTensor Ls( wf_dir + "/Ls" );

	int d = As[0].bond()[1].dim();
	int X = As[0].bond()[0].dim();

	uni10::CUniTensor op1( op1f );
	uni10::CUniTensor op2( op2f );

	std::vector< uni10::Bond > bdi;
	bdi.push_back( uni10::Bond( uni10::BD_IN, d ) );
	bdi.push_back( uni10::Bond( uni10::BD_OUT, d ) );
	uni10::CUniTensor id( bdi );
	id.identity();

	uni10::CUniTensor ket = netAA( As[0], As[1] );
	uni10::CUniTensor tM = tranMtx( ket );

	uni10::CUniTensor V;
	uni10::CUniTensor D;
	uni10::CUniTensor Vi;
	uni10::CUniTensor Im;
	mtxEigDecomp( tM, V, D, Vi, Im );

	Complex correlation;

	for (int r = 1; r <= r_max/2; ++r) {

		if (r<=100 || (r>100 && r<=1000 && r%20==0) || 
			(r>1000 && r<=10000 && r%200==0) || (r>10000 && r%2000==0)) {

			uni10::CUniTensor corr = corrEig( r, As, Bs, Ls, op1, op2, V, D, Vi );
			uni10::CUniTensor norm = corrEig( r, As, Bs, Ls,  id,  id, V, D, Vi );

			if ( argc > 4 && std::string(argv[4]) == "-mean" ) {
				uni10::CUniTensor mean1 = corrEig( r, As, Bs, Ls, op1, id, V, D, Vi );
				uni10::CUniTensor mean2 = corrEig( r, As, Bs, Ls, id, op2, V, D, Vi );
				correlation = (corr[0] / norm[0]) - (mean1[0]/norm[0]) * (mean2[0]/norm[0]);
			}
			else {
				correlation = (corr[0] / norm[0]);
			}

			std::cout << 2*r << "\t" << std::setprecision(16) << correlation.real() << "\t" 
						<< std::log( 2.* (double)r ) << "\t" << std::log( correlation.real() ) <<"\n";
		}
	}

	return 0;
}

