#include <sstream>
#include <mps.hpp>
#include "components_qlink_u1.hpp"

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H qlink <mu> <epsilon>    : Quantum LINK model spin-1/2 Hamiltonian" << std::endl;
	std::cerr << "-cstr <U_link> <mu_bdry>   : interactions for ConSTRaint" << std::endl;
	std::cerr << "-w <wf_dir>                : load initial Wavefuction fom directory" << std::endl;
	std::cerr << "-df <mps_dir>              : save final mps to Destination Folder arg" << std::endl;
	std::cerr << "-init [afm / fm]           : INITialize wavefuction with AFM/FM product state" << std::endl;
	std::cerr << "-l <len>                   : system Length" << std::endl;
	std::cerr << "-m <bond_dim>              : Max bond dimension" << std::endl;
	std::cerr << "-s <sweeps>                : number of dmrg Sweeps" << std::endl;
	std::cerr << "-s2 <u2-sweeps>            : number of 2-site update dmrg Sweeps after 1-site update" << std::endl;
	std::cerr << "-u2                        : dmrg performs 2-site Update" << std::endl;
	std::cerr << "-V                         : Verbose. show information during dmrg update" << std::endl;
	std::cerr << "-ite <iter>                : max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol <tol>                 : error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-ramp <sweeps>             : tolerance RaMPing factor for Lanczos algorithm" << std::endl;
	std::cerr << "-intm <sweeps>             : save INTerMediate state every arg sweeps" << std::endl;
}

//======================================

uni10::CUniTensor BundleDummy(uni10::CUniTensor op) {
	///
	std::vector<int> lab_op = {0, 1, 2, 3, 4, 5, -1};
	std::vector<int> labi = {0, 1, 2};
	std::vector<int> labo = {3, 4, 5};
	std::vector<int> labf = {0, 1, -1};
	op.setLabel(lab_op);
	op.combineBond(labi);
	op.combineBond(labo);
	op.setLabel(labf);
	return op;
}

//======================================

void BoundaryDummyMPO(
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	uni10::CUniTensor bdry_lam0, uni10::CUniTensor bdry_lamN, int dim_mpo = -1) {
	///
	if (dim_mpo < 0)
		dim_mpo = mpo_sy[0].bond()[mpo_sy[0].bondNum()-2].dim();
	int len = mpo_sy.size()-2;  // mpo.size = lattice_len + 2
	
	uni10::CUniTensor binv_lamN = bondInv(bdry_lamN);
	mpo_sy[0] = mpoDummy( true, dim_mpo, bdry_lam0 );
	mpo_sy[len+1] = mpoDummy( false, dim_mpo, binv_lamN );

	uni10::Bond bp = mpo_pl[0].bond()[4];
	uni10::Bond bm = mpo_mi[0].bond()[4];
	std::vector<uni10::Bond> bdp = bdry_lam0.bond();
	bdp.push_back(bp);
	std::vector<uni10::Bond> bdm = bdry_lam0.bond();
	bdm.push_back(bm);
	mpo_pl[0] = mpoDummy( true, dim_mpo, uni10::CUniTensor(bdp) );
	mpo_mi[0] = mpoDummy( true, dim_mpo, uni10::CUniTensor(bdm) );

	bdp = binv_lamN.bond();
	bdp.push_back(bp);
	bdm = binv_lamN.bond();
	bdm.push_back(bm);
	mpo_pl[len+1] = mpoDummy( false, dim_mpo, uni10::CUniTensor(bdp) );
	mpo_mi[len+1] = mpoDummy( false, dim_mpo, uni10::CUniTensor(bdm) );

	mpo_pl[0].set_zero();
	mpo_mi[0].set_zero();
	mpo_pl[len+1].set_zero();
	mpo_mi[len+1].set_zero();
}

//======================================

uni10::CUniTensor BoundaryDummyBond(bool left) {
	///
	uni10::CUniTensor dummy;
	if (left)
		dummy.assign(std::vector<uni10::Bond> {uni10::Bond(uni10::BD_IN, 3)});
	else
		dummy.assign(std::vector<uni10::Bond> {uni10::Bond(uni10::BD_OUT, 3)});
	dummy.setRawElem(std::vector<Complex> {Complex(1., 0.), Complex(0., 0.), Complex(0., 0.)});
	return dummy;
}

//======================================

uni10::CUniTensor MakeBdryGamma(uni10::CUniTensor gam, bool left) {
	///
	uni10::CUniTensor dummy = BoundaryDummyBond(left);
	uni10::CUniTensor gamB = uni10::otimes(dummy, gam);
	if (left) {
		gamB.setLabel(std::vector<int> {0, 10, 100, 1});
		gamB.combineBond(std::vector<int> {0, 10});
	}
	else {
		gamB.setLabel(std::vector<int> {0, 100, 1, 10});
		gamB.combineBond(std::vector<int> {1, 10});
	}
	return gamB;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// // hamiltonian mpo directory
	// std::string mpo_dir = "mpo-ham-itf";
	// inf wavefunction directory
	std::string wf_dir = "mps-inf";
	// final mps directory
	std::string mps_dir = "mps-obc-qlink";
	// load mps from file?
	bool load_file = false;
	// initialize mps with product state?
	int init_prod = 0;
	// save intermediate state?
	bool save_intm = false;
	int intm = 2;
	// system length
	int len = 100;
	// bond dimension
	int bd_dim = 5;
	// number of sweeps
	int sweep = 10;
	int sw_u2 = 0;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// tolerance ramping
	int ramp_tol = 10;
	// 2-site update
	bool two_site_update = false;
	// show truncation error
	bool verbose = false;

	double mu  = 1.0;
	double eps = 1.0;
	double gsh = 1.0;  // g-squre-half = (g**2)/2
	double Ulk = 100;  // U_link
	double mub = 100;  // mu_boundary

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-H") {
			if (std::string(argv[i+1]) == "qlink") {
				if (argc < i+3) {
					errMsg(argv[0]);
					return 1;
				}
				std::stringstream(argv[i+2]) >> mu;
				std::stringstream(argv[i+3]) >> eps;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-cstr") {
			if (i + 2 < argc) {
				std::stringstream(argv[i+1]) >> Ulk;
				std::stringstream(argv[i+2]) >> mub;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				wf_dir = std::string(argv[i+1]);
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-init") {
			if (std::string(argv[i+1]) == "afm") {
				init_prod = 1;
				load_file = false;
			}
			else if (std::string(argv[i+1]) == "fm") {
				init_prod = 2;
				load_file = false;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sweep;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s2") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sw_u2;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iter_max;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if ( (std::string(argv[i]) == "-ramp") || (std::string(argv[i]) == "-rmp") ) {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> ramp_tol;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) {
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-u2") {
			two_site_update = true;
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
	}

	/// main function body
	// dummies for raising/lowering MPO sectors
	std::vector<uni10::Bond> bdd = id.bond();
	bdd.push_back(uni10::Bond(uni10::BD_OUT, 1));
	uni10::CUniTensor dp(bdd);  // dummy+
	uni10::CUniTensor dm(bdd);  // dummy-
	dp.set_zero();
	dm.set_zero();
	uni10::CUniTensor IIP  = BundleDummy(uni10::otimes(uni10::otimes(id, id), dp));
	uni10::CUniTensor IIM  = BundleDummy(uni10::otimes(uni10::otimes(id, id), dm));

	// build MPO
	// symmetric blocks
	MPO mpo_ls (8, 'm');  // mpo_left
	MPO mpo_mos(8, 'm');  // mpo_middle odd site
	MPO mpo_mes(8, 'm');  // mpo_middle even site
	MPO mpo_rs (8, 'm');  // mpo_right
	// raising blocks
	MPO mpo_lp (8, 'm');
	MPO mpo_mp (8, 'm');
	MPO mpo_rp (8, 'm');
	// lowering blocks
	MPO mpo_lm (8, 'm');
	MPO mpo_mm (8, 'm');
	MPO mpo_rm (8, 'm');

	mpo_ls.putTensor( III,  0, 0 );
	mpo_ls.putTensor( PMI,  1, 0 );
	mpo_ls.putTensor( MPI,  2, 0 );
	mpo_ls.putTensor( N2II, 3, 0 );
	mpo_ls.putTensor( III,  4, 0 );
	mpo_ls.putTensor( NII,  5, 0 );
	mpo_ls.putTensor( NiII, 6, 0 );
	mpo_ls.putTensor( (-1.)*mu*INI + (-1.)*mub*NiII, 7, 0 );
	mpo_ls.putTensor( (-1.)*eps*IPM,  7, 1 );
	mpo_ls.putTensor( (-1.)*eps*IMP,  7, 2 );
	mpo_ls.putTensor( (.25)*gsh*III,  7, 3 );
	mpo_ls.putTensor( (.25)*gsh*IIN2, 7, 4 );
	mpo_ls.putTensor( (-.5)*gsh*IIN,  7, 5 );
	mpo_ls.putTensor( (+2.)*Ulk*IINi, 7, 6 );
	mpo_ls.putTensor( III, 7, 7 );
  
	mpo_mos.putTensor( III,  0, 0 );
	mpo_mos.putTensor( PMI,  1, 0 );
	mpo_mos.putTensor( MPI,  2, 0 );
	mpo_mos.putTensor( N2II, 3, 0 );
	mpo_mos.putTensor( III,  4, 0 );
	mpo_mos.putTensor( NII,  5, 0 );
	mpo_mos.putTensor( NiII, 6, 0 );
	mpo_mos.putTensor( (-1.)*mu*INI,   7, 0 );
	mpo_mos.putTensor( (-1.)*eps*IPM,  7, 1 );
	mpo_mos.putTensor( (-1.)*eps*IMP,  7, 2 );
	mpo_mos.putTensor( (.25)*gsh*III,  7, 3 );
	mpo_mos.putTensor( (.25)*gsh*IIN2, 7, 4 );
	mpo_mos.putTensor( (-.5)*gsh*IIN,  7, 5 );
	mpo_mos.putTensor( (+2.)*Ulk*IINi, 7, 6 );
	mpo_mos.putTensor( III, 7, 7 );

	mpo_mes.putTensor( III,  0, 0 );
	mpo_mes.putTensor( PMI,  1, 0 );
	mpo_mes.putTensor( MPI,  2, 0 );
	mpo_mes.putTensor( N2II, 3, 0 );
	mpo_mes.putTensor( III,  4, 0 );
	mpo_mes.putTensor( NII,  5, 0 );
	mpo_mes.putTensor( NiII, 6, 0 );
	mpo_mes.putTensor( (+1.)*mu*INI,   7, 0 );
	mpo_mes.putTensor( (-1.)*eps*IPM,  7, 1 );
	mpo_mes.putTensor( (-1.)*eps*IMP,  7, 2 );
	mpo_mes.putTensor( (.25)*gsh*III,  7, 3 );
	mpo_mes.putTensor( (.25)*gsh*IIN2, 7, 4 );
	mpo_mes.putTensor( (-.5)*gsh*IIN,  7, 5 );
	mpo_mes.putTensor( (+2.)*Ulk*IINi, 7, 6 );
	mpo_mes.putTensor( III, 7, 7 );
  
	mpo_rs.putTensor( III,  0, 0 );
	mpo_rs.putTensor( PMI,  1, 0 );
	mpo_rs.putTensor( MPI,  2, 0 );
	mpo_rs.putTensor( N2II, 3, 0 );
	mpo_rs.putTensor( III,  4, 0 );
	mpo_rs.putTensor( NII,  5, 0 );
	mpo_rs.putTensor( NiII, 6, 0 );
	if (len%2)
		mpo_rs.putTensor( (-1.)*mu*INI + (+1.)*mub*IINi, 7, 0 );
	else
		mpo_rs.putTensor( (+1.)*mu*INI + (+1.)*mub*IINi, 7, 0 );
	mpo_rs.putTensor( (-1.)*eps*IPM,  7, 1 );
	mpo_rs.putTensor( (-1.)*eps*IMP,  7, 2 );
	mpo_rs.putTensor( (.25)*gsh*III,  7, 3 );
	mpo_rs.putTensor( (.25)*gsh*IIN2, 7, 4 );
	mpo_rs.putTensor( (-.5)*gsh*IIN,  7, 5 );
	mpo_rs.putTensor( (+2.)*Ulk*IINi, 7, 6 );
	mpo_rs.putTensor( III, 7, 7 );
  
	mpo_lm.putTensor( IIM, 1, 0 );
	mpo_lp.putTensor( IIP, 2, 0 );
	mpo_lp.putTensor( IIP, 7, 1 );
	mpo_lm.putTensor( IIM, 7, 2 );

	mpo_mm.putTensor( IIM, 1, 0 );
	mpo_mp.putTensor( IIP, 2, 0 );
	mpo_mp.putTensor( IIP, 7, 1 );
	mpo_mm.putTensor( IIM, 7, 2 );

	mpo_rm.putTensor( IIM, 1, 0 );
	mpo_rp.putTensor( IIP, 2, 0 );
	mpo_rp.putTensor( IIP, 7, 1 );
	mpo_rm.putTensor( IIM, 7, 2 );

	// arrange MPO vector
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	uni10::CUniTensor mpo_to = mpo_mos.launch();
	uni10::CUniTensor mpo_te = mpo_mes.launch();
	uni10::CUniTensor mpo_tp = mpo_mp.launch();
	uni10::CUniTensor mpo_tm = mpo_mm.launch();
	for (int i = 0; i <= 1; ++i) {
		mpo_sy.push_back(mpo_ls.launch());
		mpo_pl.push_back(mpo_lp.launch());
		mpo_mi.push_back(mpo_lm.launch());
	}
	for (int i = 2; i <= len-1; ++i) {
		if (i%2)
			mpo_sy.push_back(mpo_to);
		else
			mpo_sy.push_back(mpo_te);
		mpo_pl.push_back(mpo_tp);
		mpo_mi.push_back(mpo_tm);
	}
	for (int i = len; i <= len+1; ++i) {
		mpo_sy.push_back(mpo_rs.launch());
		mpo_pl.push_back(mpo_rp.launch());
		mpo_mi.push_back(mpo_rm.launch());
	}
  
	/// initialize OBC Chain
	std::vector<uni10::Qnum> phys_dim = III.bond()[0].Qlist();
	std::vector<uni10::Qnum> qv_vec;
	for (int i = 0; i <= len; ++i) {
		if (i%2)
			qv_vec.push_back(uni10::Qnum(1));
		else
			qv_vec.push_back(uni10::Qnum(0));
	}

	ChainQnOBC mpsOBC(len, 1, phys_dim, uni10::Qnum(0), uni10::Qnum(0), uni10::Qnum(0), uni10::Qnum(0));
	if (load_file)
		mpsOBC.importMPS(wf_dir, 2, false);
	else {
		mpsOBC.init(qv_vec);
		mpsOBC.randomize();
		if (init_prod == 1) {
			// AFM prod state
			for (int i = 1; i < len-1; ++i) {
				if (i%2)
					mpsOBC.putGamma(gamE1, i);  // odd site in mps == even site in PRL paper
				else
					mpsOBC.putGamma(gamO0, i);  // even site in mps == odd site in PRL paper
			}
			mpsOBC.putGamma(MakeBdryGamma(gamO0, true), 0);
			mpsOBC.putGamma(MakeBdryGamma(gamE1, false), len-1);
		}
		else if (init_prod == 2) {
			// FM prod state
			for (int i = 1; i < len-1; ++i) {
				if (i%2)
					mpsOBC.putGamma(gamE0, i);  // odd site in mps == even site in PRL paper
				else
					mpsOBC.putGamma(gamO1, i);  // even site in mps == odd site in PRL paper
			}
			mpsOBC.putGamma(MakeBdryGamma(gamO1, true), 0);
			mpsOBC.putGamma(MakeBdryGamma(gamE0, false), len-1);
		}
	}
	mpsOBC.setMaxBD(bd_dim);
	mpsOBC.setSchValCutoff(1e-10);
  
	/// set qnum constraints
	std::vector<std::vector<uni10::Qnum>> fix_qns;
	for (int i = 0; i <= len; ++i) {
		std::vector<uni10::Qnum> qv = {qv_vec[i]};
		fix_qns.push_back(qv);
	}
	mpsOBC.setFixQn(fix_qns);

	/// perform DMRG
	BoundaryDummyMPO(mpo_sy, mpo_pl, mpo_mi, mpsOBC.getLambda(0), mpsOBC.getLambda(len));
	int s = 0;
	if (save_intm) {
		s = 0;
		while ( s < sweep ) {
			if (two_site_update)
				sw_u2 = sweep;
			if ( s < sweep-sw_u2 )
				mpsOBC.dmrgImp( mpo_sy, mpo_pl, mpo_mi, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose, true );
			else
				mpsOBC.dmrgImpU2( mpo_sy, mpo_pl, mpo_mi, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose, true );
			s += intm;
			mpsOBC.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );
		}
	}
	else {
		if (two_site_update)
			mpsOBC.dmrgImpU2( mpo_sy, mpo_pl, mpo_mi, sweep, iter_max, tolerance, verbose, true );
		else
			mpsOBC.dmrgImp( mpo_sy, mpo_pl, mpo_mi, sweep, iter_max, tolerance, verbose, true );
	}
  
	mpsOBC.exportMPS( mps_dir );
	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();

	return 0;
}
