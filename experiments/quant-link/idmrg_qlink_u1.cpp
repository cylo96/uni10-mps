#include <sstream>
#include <algorithm>
#include <mps.hpp>
#include "components_qlink_u1.hpp"

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H qlink <mu> <epsilon>    : Quantum LINK model spin-1/2 Hamiltonian" << std::endl;
	std::cerr << "-cstr <U_link> <mu_bdry>   : interactions for ConSTRaint" << std::endl;
	std::cerr << "-w <mps_dir>               : load initial Wavefuction fom directory" << std::endl;
	std::cerr << "-r <mps_dir>               : Resume DMRG using mps and renormalized mpo from directory" << std::endl;
	std::cerr << "-df <mps_dir>              : save final mps to Destination Folder arg" << std::endl;
	std::cerr << "-init [afm / fm]           : INITialize wavefuction with AFM/FM product state" << std::endl;
	std::cerr << "-m <bond_dim>              : Max bond dimension" << std::endl;
	std::cerr << "-s <steps>                 : number of iDMRG lattice growing/iteration Steps" << std::endl;
	std::cerr << "-V                         : Verbose. show information during dmrg update" << std::endl;
	std::cerr << "-ite <iter>                : max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol <tol>                 : error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-off                       : OFFset hb in boundary mpo when doing dmrg" << std::endl;
}

//======================================

uni10::CUniTensor BundleDummy(uni10::CUniTensor op) {
	///
	std::vector<int> lab_op = {0, 1, 2, 3, 4, 5, -1};
	std::vector<int> labi = {0, 1, 2};
	std::vector<int> labo = {3, 4, 5};
	std::vector<int> labf = {0, 1, -1};
	op.setLabel(lab_op);
	op.combineBond(labi);
	op.combineBond(labo);
	op.setLabel(labf);
	return op;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// // hamiltonian mpo directory
	// std::string mpo_dir = "mpo-ham-itf";
	// inf mps directory
	std::string mps_dir = "mps-inf";
	// load mps from file?
	bool load_file = false;
	bool resume = false;
	// initialize mps with product state?
	int init_prod = 0;
	// unit cell size
	int uc = 2;
	// bond dimension
	int bd_dim = 5;
	// number of sweeps
	int steps_max = 10;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// verbose
	bool verbose = false;

	double mu  = 1.0;
	double eps = 1.0;
	double gsh = 1.0;  // g-squre-half = (g**2)/2
	double Ulk = 100;  // U_link
	double mub = 100;  // mu_boundary

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-H") {
			if (std::string(argv[i+1]) == "qlink") {
				if (argc < i+3) {
					errMsg(argv[0]);
					return 1;
				}
				std::stringstream(argv[i+2]) >> mu;
				std::stringstream(argv[i+3]) >> eps;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-cstr") {
			if (i + 2 < argc) {
				std::stringstream(argv[i+1]) >> Ulk;
				std::stringstream(argv[i+2]) >> mub;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				mps_dir = std::string(argv[i+1]);
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-r") {
			load_file = true;
			resume = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				mps_dir = std::string(argv[i+1]);
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-init") {
			if (std::string(argv[i+1]) == "afm") {
				init_prod = 1;
				load_file = false;
			}
			else if (std::string(argv[i+1]) == "fm") {
				init_prod = 2;
				load_file = false;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> steps_max;
				steps_max = steps_max/2 * 2;  // force steps even => mps ends up in odd-even order
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iter_max;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
	}

	/// main function body
	// dummies for raising/lowering MPO sectors
	std::vector<uni10::Bond> bdd = id.bond();
	bdd.push_back(uni10::Bond(uni10::BD_OUT, 1));
	uni10::CUniTensor dp(bdd);  // dummy+
	uni10::CUniTensor dm(bdd);  // dummy-
	dp.set_zero();
	dm.set_zero();
	uni10::CUniTensor IIP  = BundleDummy(uni10::otimes(uni10::otimes(id, id), dp));
	uni10::CUniTensor IIM  = BundleDummy(uni10::otimes(uni10::otimes(id, id), dm));

	// build MPO
	// symmetric blocks
	MPO mpo_ls (8, 'l');  // mpo_left
	MPO mpo_mos(8, 'm');  // mpo_middle odd site
	MPO mpo_mes(8, 'm');  // mpo_middle even site
	MPO mpo_rs (8, 'r');  // mpo_right
	// raising blocks
	MPO mpo_lp (8, 'l');
	MPO mpo_mp (8, 'm');
	MPO mpo_rp (8, 'r');
	// lowering blocks
	MPO mpo_lm (8, 'l');
	MPO mpo_mm (8, 'm');
	MPO mpo_rm (8, 'r');

	mpo_ls.putTensor( (-1.)*mu*INI + (-1.)*mub*NiII, 0, 0 );
	mpo_ls.putTensor( (-1.)*eps*IPM,  0, 1 );
	mpo_ls.putTensor( (-1.)*eps*IMP,  0, 2 );
	mpo_ls.putTensor( (.25)*gsh*III,  0, 3 );
	mpo_ls.putTensor( (.25)*gsh*IIN2, 0, 4 );
	mpo_ls.putTensor( (-.5)*gsh*IIN,  0, 5 );
	mpo_ls.putTensor( (+2.)*Ulk*IINi, 0, 6 );
	mpo_ls.putTensor( III, 0, 7 );
  
	mpo_mos.putTensor( III,  0, 0 );
	mpo_mos.putTensor( PMI,  1, 0 );
	mpo_mos.putTensor( MPI,  2, 0 );
	mpo_mos.putTensor( N2II, 3, 0 );
	mpo_mos.putTensor( III,  4, 0 );
	mpo_mos.putTensor( NII,  5, 0 );
	mpo_mos.putTensor( NiII, 6, 0 );
	mpo_mos.putTensor( (-1.)*mu*INI,   7, 0 );
	mpo_mos.putTensor( (-1.)*eps*IPM,  7, 1 );
	mpo_mos.putTensor( (-1.)*eps*IMP,  7, 2 );
	mpo_mos.putTensor( (.25)*gsh*III,  7, 3 );
	mpo_mos.putTensor( (.25)*gsh*IIN2, 7, 4 );
	mpo_mos.putTensor( (-.5)*gsh*IIN,  7, 5 );
	mpo_mos.putTensor( (+2.)*Ulk*IINi, 7, 6 );
	mpo_mos.putTensor( III, 7, 7 );

	mpo_mes.putTensor( III,  0, 0 );
	mpo_mes.putTensor( PMI,  1, 0 );
	mpo_mes.putTensor( MPI,  2, 0 );
	mpo_mes.putTensor( N2II, 3, 0 );
	mpo_mes.putTensor( III,  4, 0 );
	mpo_mes.putTensor( NII,  5, 0 );
	mpo_mes.putTensor( NiII, 6, 0 );
	mpo_mes.putTensor( (+1.)*mu*INI,   7, 0 );
	mpo_mes.putTensor( (-1.)*eps*IPM,  7, 1 );
	mpo_mes.putTensor( (-1.)*eps*IMP,  7, 2 );
	mpo_mes.putTensor( (.25)*gsh*III,  7, 3 );
	mpo_mes.putTensor( (.25)*gsh*IIN2, 7, 4 );
	mpo_mes.putTensor( (-.5)*gsh*IIN,  7, 5 );
	mpo_mes.putTensor( (+2.)*Ulk*IINi, 7, 6 );
	mpo_mes.putTensor( III, 7, 7 );
  
	mpo_rs.putTensor( III,  0, 0 );
	mpo_rs.putTensor( PMI,  1, 0 );
	mpo_rs.putTensor( MPI,  2, 0 );
	mpo_rs.putTensor( N2II, 3, 0 );
	mpo_rs.putTensor( III,  4, 0 );
	mpo_rs.putTensor( NII,  5, 0 );
	mpo_rs.putTensor( NiII, 6, 0 );
	mpo_rs.putTensor( (+1.)*mu*INI + (+1.)*mub*IINi, 7, 0 );
  
	mpo_lp.putTensor( IIP, 0, 1 );
	mpo_lm.putTensor( IIM, 0, 2 );

	mpo_mm.putTensor( IIM, 1, 0 );
	mpo_mp.putTensor( IIP, 2, 0 );
	mpo_mp.putTensor( IIP, 7, 1 );
	mpo_mm.putTensor( IIM, 7, 2 );

	mpo_rm.putTensor( IIM, 1, 0 );
	mpo_rp.putTensor( IIP, 2, 0 );

	// arrange MPO vector
	uni10::CUniTensor mpo_to = mpo_mos.launch();
	uni10::CUniTensor mpo_te = mpo_mes.launch();
	uni10::CUniTensor mpo_tp = mpo_mp.launch();
	uni10::CUniTensor mpo_tm = mpo_mm.launch();

	std::vector<std::vector<uni10::CUniTensor>> mpo_lr;
	std::vector<std::vector<uni10::CUniTensor>> mpo_m;
	mpo_lr.push_back(std::vector<uni10::CUniTensor> {mpo_ls.launch(), mpo_lp.launch(), mpo_lm.launch()});
	for (int i = 0; i < uc; ++i) {
		if (i%2)
			mpo_m.push_back(std::vector<uni10::CUniTensor> {mpo_to, mpo_tp, mpo_tm});
		else
			mpo_m.push_back(std::vector<uni10::CUniTensor> {mpo_te, mpo_tp, mpo_tm});
	}
	mpo_lr.push_back(std::vector<uni10::CUniTensor> {mpo_rs.launch(), mpo_rp.launch(), mpo_rm.launch()});
  
	/// initialize OBC Chain
	std::vector<uni10::Qnum> phys_dim = III.bond()[0].Qlist();
	std::vector<uni10::Qnum> virt_dim = {uni10::Qnum(0)};
	ChainQnInf mpsInf(bd_dim, phys_dim, virt_dim);
	
	if (load_file)
		mpsInf.importMPS(mps_dir);
	else {
		mpsInf.randomize();
		if (init_prod == 1) {
			// AFM prod state
			for (int i = 0; i < uc; ++i) {
				if (i%2) {
					mpsInf.putLambda(lamE, i);
					mpsInf.putGamma(gamE1, i);  // odd site in mps == even site in PRL paper
				}
				else {
					mpsInf.putLambda(lamO, i);
					mpsInf.putGamma(gamO0, i);  // even site in mps == odd site in PRL paper
				}
			}
		}
		else if (init_prod == 2) {
			// FM prod state
			for (int i = 0; i < uc; ++i) {
				if (i%2) {
					mpsInf.putLambda(lamE, i);
					mpsInf.putGamma(gamE0, i);  // odd site in mps == even site in PRL paper
				}
				else {
					mpsInf.putLambda(lamO, i);
					mpsInf.putGamma(gamO1, i);  // even site in mps == odd site in PRL paper
				}
			}
		}
	}
	mpsInf.setMaxBD(bd_dim);
	mpsInf.setSchValCutoff(1e-10);
  
	/// set qnum constraints
	std::vector<uni10::Qnum> qv_vec;
	for (int i = 0; i < uc; ++i) {
		if (i%2)
			qv_vec.push_back(uni10::Qnum(0));
		else
			qv_vec.push_back(uni10::Qnum(1));	 // 1st fix_qn for 1st idmrg step => odd idx lambda => Qnum(1)
	}
	if (resume)
		std::swap(qv_vec[0], qv_vec[1]);  // because idmrgResume swaps the 2 sites in the beginning
	std::vector<std::vector<uni10::Qnum>> fix_qns;
	for (int i = 0; i < uc; ++i) {
		std::vector<uni10::Qnum> qv = {qv_vec[i]};
		fix_qns.push_back(qv);
	}
	mpsInf.setFixQn(fix_qns);

	/// perform DMRG
	// steps_max+1 ? one more update to keep the blk struct of final lam0 similar to original
	if (resume)
		mpsInf.idmrgU2Resume( mpo_lr, mpo_m, steps_max, iter_max, tolerance, mps_dir, false, true );
	else {
		mpsInf.exportMPS( mps_dir );
		mpsInf.idmrgU2( mpo_lr, mpo_m, steps_max+1, iter_max, tolerance, mps_dir, false, true );
	}
	mpsInf.exportMPS( mps_dir );

	mpo_lr.clear();
	mpo_m.clear();
	return 0;
}
