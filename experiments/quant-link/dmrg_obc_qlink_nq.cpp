#include <sstream>
#include <math.h>
#include <numeric>
#include <mps.hpp>
#define _USE_MATH_DEFINES

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H qlink <mu> <epsilon>    : Quantum LINK model spin-1/2 Hamiltonian" << std::endl;
	std::cerr << "-imp <loc> <mu> <epsilon>  : IMPurity Hamiltonian" << std::endl;
	std::cerr << "-w <wf_dir>                : load initial Wavefuction fom directory" << std::endl;
	std::cerr << "-df <mps_dir>              : save final mps to Destination Folder arg" << std::endl;
	std::cerr << "-l <len>                   : system Length" << std::endl;
	std::cerr << "-m <bond_dim>              : Max bond dimension" << std::endl;
	std::cerr << "-s <sweeps>                : number of dmrg Sweeps" << std::endl;
	std::cerr << "-s2 <u2-sweeps>            : number of 2-site update dmrg Sweeps after 1-site update" << std::endl;
	std::cerr << "-u2                        : dmrg performs 2-site Update" << std::endl;
	std::cerr << "-V                         : Verbose. show information during dmrg update" << std::endl;
	std::cerr << "-ite <iter>                : max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol <tol>                 : error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-ramp <sweeps>             : tolerance RaMPing factor for Lanczos algorithm" << std::endl;
	std::cerr << "-intm <sweeps>             : save INTerMediate state every arg sweeps" << std::endl;
	std::cerr << "-bdry <Sz_left> <Sz_right> : fix BounDaRY spins" << std::endl;
}

//======================================

uni10::CUniTensor Square(uni10::CUniTensor op) {
	///
	uni10::CUniTensor dp = op; // dupe
	int bn = op.bondNum();
	std::vector<int> lab_op(bn), lab_dp(bn);
	std::iota(std::begin(lab_op), std::end(lab_op), 0);
	std::iota(std::begin(lab_dp), std::end(lab_dp), op.inBondNum());
	op.setLabel(lab_op);
	dp.setLabel(lab_dp);
	uni10::CUniTensor op2 = uni10::contract(op, dp);
	op2.setLabel(lab_op);
	return op2;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// // hamiltonian mpo directory
	// std::string mpo_dir = "mpo-ham-itf";
	// inf wavefunction directory
	std::string wf_dir = "mps-inf";
	// final mps directory
	std::string mps_dir = "mps-obc-qlink";
	// load mps from file?
	bool load_file = false;
	// save intermediate state?
	bool save_intm = false;
	int intm = 2;
	// system length
	int size = 100;
	int len = 201;
	// bond dimension
	int bd_dim = 5;
	// number of sweeps
	int sweep = 10;
	int sw_u2 = 0;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// tolerance ramping
	int ramp_tol = 10;
	// 2-site update
	bool two_site_update = false;
	// show truncation error
	bool verbose = false;
	// fix boundary spins
	bool fix_bdry = false;
	int spin_l, spin_r;

	std::vector<std::vector<double>> imps;
	double mu  = 1.0;
	double eps = 1.0;
	double gsh = 1.0;  // g-squre-half = (g**2)/2

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-H") {
			if (std::string(argv[i+1]) == "qlink") {
				if (argc < i+3) {
					errMsg(argv[0]);
					return 1;
				}
				std::stringstream(argv[i+2]) >> mu;
				std::stringstream(argv[i+3]) >> eps;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-imp") {
			if (argc < i+3) {
				errMsg(argv[0]);
				return 1;
			}
			std::vector<double> imp_params(4);
			std::stringstream(argv[i+1]) >> imp_params[0];	// location index
			std::stringstream(argv[i+2]) >> imp_params[1];	// mu_imp
			std::stringstream(argv[i+3]) >> imp_params[2];	// eps_imp
			imps.push_back(imp_params);
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				wf_dir = std::string(argv[i+1]);
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> size;
				len = size*2+1;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sweep;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s2") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sw_u2;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iter_max;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if ( (std::string(argv[i]) == "-ramp") || (std::string(argv[i]) == "-rmp") ) {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> ramp_tol;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) {
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-bdry") {
			fix_bdry = true;
			if (i + 2 < argc) {
				std::istringstream(argv[i+1]) >> spin_l;
				std::istringstream(argv[i+2]) >> spin_r;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-u2") {
			two_site_update = true;
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
	}

	/// main function body
	// define component operators
	uni10::CUniTensor Sz  = OP( "Sz" );    // Sz
	uni10::CUniTensor Sp  = OP( "Sp" );    // S+
	uni10::CUniTensor Sm  = OP( "Sm" );    // S-
	uni10::CUniTensor sz  = OP( "sz" );    // sigmaz
	uni10::CUniTensor sp  = OP( "sp" );    // sigma+
	uni10::CUniTensor sm  = OP( "sm" );    // sigma-
	uni10::CUniTensor id  = OP( "id" );    // I
	uni10::CUniTensor N   = Sz + 0.5*id;   // N
	uni10::CUniTensor E0  = 0.0*id;        // E_0
	uni10::CUniTensor Eo  = Sz + E0;       // E_odd
	uni10::CUniTensor Ee  = Sz + (-1.)*E0; // E_even
	uni10::CUniTensor Eo2 = Square(Eo);    // E_odd**2
	uni10::CUniTensor Ee2 = Square(Ee);    // E_even**2

	/// build MPO
	MPO mpo_ls(4, 'l');   // mpo_left_spin
	MPO mpo_mso(4, 'm');  // mpo_middle_spin odd site
	MPO mpo_mse(4, 'm');  // mpo_middle_spin even site
	MPO mpo_mfo(4, 'm');  // mpo_middle_fermion odd site
	MPO mpo_mfe(4, 'm');  // mpo_middle_fermion even site
	MPO mpo_rs(4, 'r');   // mpo_right_spin

	mpo_ls.putTensor( gsh*Ee2, 0, 0 );
	mpo_ls.putTensor( id, 0, 3 );
  
	mpo_mso.putTensor( id, 0, 0 );
	mpo_mso.putTensor( gsh*Eo2, 3, 0 );
	mpo_mso.putTensor( id, 3, 3 );
	mpo_mso.putTensor( sp, 1, 1 );
	mpo_mso.putTensor( sm, 2, 2 );
	
	mpo_mse.putTensor( id, 0, 0 );
	mpo_mse.putTensor( gsh*Ee2, 3, 0 );
	mpo_mse.putTensor( id, 3, 3 );
	mpo_mse.putTensor( sp, 1, 1 );
	mpo_mse.putTensor( sm, 2, 2 );
  
	mpo_mfo.putTensor( id, 0, 0 );
	mpo_mfo.putTensor( sm, 1, 0 );
	mpo_mfo.putTensor( sp, 2, 0 );
	mpo_mfo.putTensor( (-1.)*mu*N, 3, 0 );
	mpo_mfo.putTensor( (-1.)*eps*sp, 3, 1 );
	mpo_mfo.putTensor( (-1.)*eps*sm, 3, 2 );
	mpo_mfo.putTensor( id, 3, 3 );
	
	mpo_mfe.putTensor( id, 0, 0 );
	mpo_mfe.putTensor( sm, 1, 0 );
	mpo_mfe.putTensor( sp, 2, 0 );
	mpo_mfe.putTensor( (+1.)*mu*N, 3, 0 );
	mpo_mfe.putTensor( (-1.)*eps*sp, 3, 1 );
	mpo_mfe.putTensor( (-1.)*eps*sm, 3, 2 );
	mpo_mfe.putTensor( id, 3, 3 );
	
	mpo_rs.putTensor( id, 0, 0 );
	if (size%2)
		mpo_rs.putTensor( gsh*Eo2, 3, 0 );
	else
		mpo_rs.putTensor( gsh*Ee2, 3, 0 );
  
	// arrange MPO vector
	std::vector<uni10::CUniTensor> mpo;
	uni10::CUniTensor mpo_tso = mpo_mso.launch();
	uni10::CUniTensor mpo_tse = mpo_mse.launch();
	uni10::CUniTensor mpo_tfo = mpo_mfo.launch();
	uni10::CUniTensor mpo_tfe = mpo_mfe.launch();
	mpo.push_back(mpo_ls.launch());
	for (int i = 1; i <= size; ++i) {
		if (i%2) {
			mpo.push_back(mpo_tfo);
			mpo.push_back(mpo_tso);
		}
		else {
			mpo.push_back(mpo_tfe);
			mpo.push_back(mpo_tse);
		}
	}
	mpo[len-1] = mpo_rs.launch();

	// impurities
	// int num_imps = imps.size();
	// if (num_imps > 0) {
	// 	for (int i = 0; i < num_imps; ++i) {
	// 		MPO mpo_imp(5, 'm');
	// 		mpo_imp.putTensor( id, 0, 0 );
	// 		mpo_imp.putTensor( sx, 1, 0 );
	// 		mpo_imp.putTensor( sy, 2, 0 );
	// 		mpo_imp.putTensor( sz, 3, 0 );
	// 		mpo_imp.putTensor(  0.5*imps[i][3]*sz, 4, 0 );
	// 		mpo_imp.putTensor( 0.25*imps[i][1]*sx, 4, 1 );
	// 		mpo_imp.putTensor( 0.25*imps[i][1]*sy, 4, 2 );
	// 		mpo_imp.putTensor( 0.25*imps[i][2]*sz, 4, 3 );
	// 		mpo_imp.putTensor( id, 4, 4 );
	// 		mpo[ (int)imps[i][0]+1 ] = mpo_imp.launch();
	// 	}
	// }
  
	int phys_dim = id.bond()[0].dim();
  
	/// initialize OBC Chain
	ChainOBC mpsOBC(len, phys_dim, 1);
	if (load_file) {
		mpsOBC.setMaxBD(bd_dim);
		mpsOBC.importMPS(wf_dir);
	}
	else
		mpsOBC.randomize();

	if (fix_bdry && !load_file) {
		Complex up[2] = {Complex(1.,0.), Complex(0.,0.)};
		Complex dn[2] = {Complex(0.,0.), Complex(1.,0.)};
		Complex et[2] = {Complex(std::sqrt(0.5),0.), Complex(std::sqrt(0.5),0.)};
		uni10::CUniTensor gam = mpsOBC.getGamma(0);
		if (spin_l)
			gam.setElem(up);
		else
			gam.setElem(dn);
		mpsOBC.putGamma(gam, 0);
		if (spin_r)
			gam.setElem(up);
		else
			gam.setElem(dn);
		mpsOBC.putGamma(gam, len-1);

		if (spin_l == spin_r) {
			gam.setElem(et);
			for (int i = 2; i < len-1; i+=2)
				mpsOBC.putGamma(gam, i);
			gam.setElem(up);
			for (int i = 1; i < len-1; i+=4)
				mpsOBC.putGamma(gam, i);
			gam.setElem(dn);
			for (int i = 3; i < len-1; i+=4)
				mpsOBC.putGamma(gam, i);
		}
	}
	mpsOBC.setMaxBD(bd_dim);
	mpsOBC.setSchValCutoff(1e-10);
  
	/// perform DMRG
	int s = 0;
	if (save_intm) {
		s = 0;
		while ( s < sweep ) {
			if (two_site_update)
				sw_u2 = sweep;
			if ( s < sweep-sw_u2 )
				mpsOBC.dmrgImpU3( mpo, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			else
				mpsOBC.dmrgImpU2( mpo, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			s += intm;
			mpsOBC.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );
		}
	}
	else {
		if (two_site_update)
			mpsOBC.dmrgImpU2( mpo, sweep, iter_max, tolerance, verbose );
		else
			mpsOBC.dmrgImpU3( mpo, sweep, iter_max, tolerance, verbose );
	}
  
	mpsOBC.exportMPS( mps_dir );
	mpo.clear();

	return 0;
}
