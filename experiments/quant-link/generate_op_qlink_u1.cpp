#include <sstream>
#include <mps.hpp>
#include "components_qlink_u1.hpp"

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-E                         : E_(x,x+1) = S^z_(x,x+1) operator" << std::endl;
	std::cerr << "-Nf                        : Nf_(x) operator" << std::endl;
	std::cerr << "-Nr                        : Nb_(x),r operator" << std::endl;
	std::cerr << "-Nl                        : Nb_(x),l operator" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-E") {
			uni10::CUniTensor op = 0.5 * (uni10::otimes(III, NII) + (-1.)*uni10::otimes(IIN, III));
			op.save("E");
		}
		else if (std::string(argv[i]) == "-Nf") {
			uni10::CUniTensor op = INI;
			op.save("Nf");
		}
		else if (std::string(argv[i]) == "-Nr") {
			uni10::CUniTensor op = NII;
			op.save("Nr");
		}
		else if (std::string(argv[i]) == "-Nl") {
			uni10::CUniTensor op = IIN;
			op.save("Nl");
		}
	}

	return 0;
}
