#include <sstream>
#include <mps.hpp>
#include "components_qlink_u1.hpp"

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H qlink <mu> <epsilon>    : Quantum LINK model spin-1/2 Hamiltonian" << std::endl;
	std::cerr << "-cstr <U_link> <mu_bdry>   : interactions for ConSTRaint" << std::endl;
	std::cerr << "-dvr <mu_drv_rate>         : DRiVing rate of mu" << std::endl;
	std::cerr << "-w <mps_dir>               : load initial Wavefuction fom directory" << std::endl;
	std::cerr << "-df <mps_dir>              : save final mps to Destination Folder arg" << std::endl;
	std::cerr << "-init [afm / fm]           : INITialize wavefuction with AFM/FM product state" << std::endl;
	std::cerr << "-m <bond_dim>              : Max bond dimension" << std::endl;
	std::cerr << "-s <steps>                 : number of evolution Steps" << std::endl;
	std::cerr << "-it <dT>                   : evolution (Imaginary) Timestep" << std::endl;
	std::cerr << "-rt <dt>                   : evolution (Real) Timestep" << std::endl;
	std::cerr << "-ord <TS_order>            : ORDer of Trotter-Suzuki expansion" << std::endl;
	std::cerr << "-tol <tol>                 : error TOLerance for truncation error" << std::endl;
	std::cerr << "-gs                        : evolve to Ground State using default method" << std::endl;
	std::cerr << "-V                         : Verbose. show information during tebd update" << std::endl;
}

//======================================

uni10::CUniTensor BoundaryDummyBond(bool left) {
	///
	uni10::CUniTensor dummy;
	if (left)
		dummy.assign(std::vector<uni10::Bond> {uni10::Bond(uni10::BD_IN, 3)});
	else
		dummy.assign(std::vector<uni10::Bond> {uni10::Bond(uni10::BD_OUT, 3)});
	dummy.setRawElem(std::vector<Complex> {Complex(1., 0.), Complex(0., 0.), Complex(0., 0.)});
	return dummy;
}

//======================================

uni10::CUniTensor MakeBdryGamma(uni10::CUniTensor gam, bool left) {
	///
	uni10::CUniTensor dummy = BoundaryDummyBond(left);
	uni10::CUniTensor gamB = uni10::otimes(dummy, gam);
	if (left) {
		gamB.setLabel(std::vector<int> {0, 10, 100, 1});
		gamB.combineBond(std::vector<int> {0, 10});
	}
	else {
		gamB.setLabel(std::vector<int> {0, 100, 1, 10});
		gamB.combineBond(std::vector<int> {1, 10});
	}
	return gamB;
}

//======================================

void DriveHam(std::vector<uni10::CUniTensor>& hams, double dvr, double dt, int uc = 2) {
	///
	double sgn;
	for (int i = 0; i < uc; ++i) {
		sgn = (i%2)? 1. : -1.;  // odd site in mps == even site in PRL paper
		hams[i] += (dvr*dt) * (sgn) * ((0.5) * uni10::otimes(INI, III) + (-.5) * uni10::otimes(III, INI));
	}
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// inf mps directory
	std::string wf_dir = "mps-inf";
	std::string df_dir = "mps-inf";
	// load mps from file?
	bool load_file = false;
	// initialize mps with product state?
	int init_prod = 0;
	// bond dimension
	int bd_dim = 5;
	// imag dt
	double dT = 0.1;
	// real dt
	double dt = 0.0;
	// number of steps
	int steps_max = -1;
	// order of Trotter-Suzuki expansion
	int ts_order = 1;
	// tolerance for truncation error
	double tolerance = 1e-15;
	// evolve to ground state?
	bool to_gs = false;
	// show truncation error
	bool verbose = false;

	int uc = 2;
	double mu  = 1.0;
	double eps = 1.0;
	double gsh = 1.0;  // g-squre-half = (g**2)/2
	double Ulk = 100;  // U_link
	double mub = 100;  // mu_boundary
	double dvr = 0.0;  // mu driving rate
	bool to_drive = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-H") {
			if (std::string(argv[i+1]) == "qlink") {
				if (argc < i+3) {
					errMsg(argv[0]);
					return 1;
				}
				std::stringstream(argv[i+2]) >> mu;
				std::stringstream(argv[i+3]) >> eps;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-cstr") {
			if (i + 2 < argc) {
				std::stringstream(argv[i+1]) >> Ulk;
				std::stringstream(argv[i+2]) >> mub;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-dvr") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> dvr;
				to_drive = true;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				wf_dir = std::string(argv[i+1]);
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				df_dir = std::string(argv[i+1]);
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-init") {
			if (std::string(argv[i+1]) == "afm") {
				init_prod = 1;
				load_file = false;
			}
			else if (std::string(argv[i+1]) == "fm") {
				init_prod = 2;
				load_file = false;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> steps_max;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-it") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> dT;
				dt = 0.0;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-rt") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> dt;
				dT = 0.0;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ord") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> ts_order;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-gs") {
			to_gs = true;
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
	}

	/// main function body
	/// initialize Inf Chain
	std::vector<uni10::Qnum> phys_dim = III.bond()[0].Qlist();
	std::vector<uni10::Qnum> virt_dim = {uni10::Qnum(0)};
	ChainQnInf mpsInf(bd_dim, phys_dim, virt_dim);

	if (load_file)
		mpsInf.importMPS(wf_dir);
	else {
		mpsInf.randomize();
		if (init_prod == 1) {
			// AFM prod state
			for (int i = 0; i < uc; ++i) {
				if (i%2) {
					mpsInf.putLambda(lamE, i);
					mpsInf.putGamma(gamE1, i);  // odd site in mps == even site in PRL paper
				}
				else {
					mpsInf.putLambda(lamO, i);
					mpsInf.putGamma(gamO0, i);  // even site in mps == odd site in PRL paper
				}
			}
		}
		else if (init_prod == 2) {
			// FM prod state
			for (int i = 0; i < uc; ++i) {
				if (i%2) {
					mpsInf.putLambda(lamE, i);
					mpsInf.putGamma(gamE0, i);  // odd site in mps == even site in PRL paper
				}
				else {
					mpsInf.putLambda(lamO, i);
					mpsInf.putGamma(gamO1, i);  // even site in mps == odd site in PRL paper
				}
			}
		}
	}
	mpsInf.setMaxBD(bd_dim);
	mpsInf.setSchValCutoff(1e-10);

	/// set qnum constraints
	std::vector<uni10::Qnum> qv_vec;
	for (int i = 0; i < uc; ++i) {
		if (i%2)
			qv_vec.push_back(uni10::Qnum(0));
		else
			qv_vec.push_back(uni10::Qnum(1));
	}
	std::vector<std::vector<uni10::Qnum>> fix_qns;
	for (int i = 0; i < uc; ++i) {
		std::vector<uni10::Qnum> qv = {qv_vec[i]};
		fix_qns.push_back(qv);
	}
	mpsInf.setFixQn(fix_qns);

	/// 2-site Hamiltonians
	uni10::CUniTensor E = 0.5 * (uni10::otimes(III, NII) + (-1.)*uni10::otimes(IIN, III));
	uni10::CUniTensor E2 = Square(E);
	uni10::CUniTensor Id = E;
	Id.identity();
	std::vector<uni10::CUniTensor> hams(uc, uni10::CUniTensor());
	double sgn;

	for (int i = 0; i < uc; ++i) {
		sgn = (i%2)? 1. : -1.;  // odd site in mps == even site in PRL paper
		hams[i] = gsh * E2
			+ mu  * (sgn) * ((0.5) * uni10::otimes(INI, III) + (-.5) * uni10::otimes(III, INI))
			+ eps * (-1.) * (uni10::otimes(IPM, PMI) + uni10::otimes(IMP, MPI))
			+ Ulk * (+2.) * uni10::otimes(IINi, NiII);
	}

	/// perform TEBD

	if (to_gs) {
		Complex cdt (0.0, dT);	// dt in complex form
		double dt_min = 5e-9;
		double t = 0.0;
		double eng0 = 0.0;
		double eng1, dE;
		int ckpt, Nevl;
		if (steps_max > 0)
			steps_max = 10 * (1 + steps_max / 10);

		std::cout << "\n# Starting iTEBD with: Bond Dimension = " << bd_dim << "\n";
		while ( cdt.imag() >= dt_min ) {
			ckpt = 0;
			Nevl = 0;
			while (ckpt < 1) {
				//====== evolve and update ======
				mpsInf.itebdU2(hams, cdt, 10, ts_order, true);
				//====== calculate energy ======
				eng1 = 0.5 * (mpsInf.expVal(hams[0], 0)[0].real() + mpsInf.expVal(hams[1], 1)[0].real());
				dE = fabs(eng0 - eng1);
				//====== output ======
				if ( Nevl == steps_max || ( Nevl > 0 && dE < ( std::min( 1e-14, 1e-8 * cdt.imag()) ) ) ) {
					std::cout << "\ndt = " << cdt << "\n# of evolution = " << Nevl << "\n";
					std::cout << "Energy = " << std::setprecision(10) << eng1 << "\n\n";
					ckpt = 1;
				}
				eng0 = eng1;
				Nevl += 10;
			}
			cdt /= 2.0;
		}
	}
	else if (to_drive) {
		std::vector<double> norm(uc), expv(uc);
		double eval = 0;
		Complex cdt (dt, dT);
		std::cout << "#t\tmu+Rt\tE" << '\n';
		for (int s = 0; s < steps_max; ++s) {
			// evolve
			mpsInf.itebdU2(hams, cdt, 1, ts_order, true);
			// measurement
			eval = 0;
			for (int i = 0; i < uc; ++i) {
				norm[i] = (mpsInf.expVal(Id, i))[0].real();
				expv[i] = (mpsInf.expVal(E, i))[0].real();
				eval += expv[i]/norm[i];
			}
			eval /= (double)uc;
			std::cout << dt*s << '\t' << mu + dvr*dt*s << '\t' << eval << '\n';
			// drive
			DriveHam(hams, dvr, dt, uc);
		}
	}
	else {
		std::cout << "\n# Starting iTEBD with: Bond Dimension = " << bd_dim << "\n";
		Complex cdt (dt, dT);
		mpsInf.itebdU2(hams, cdt, steps_max, ts_order, true);
		double eng = 0.5 * (mpsInf.expVal(hams[0], 0)[0].real() + mpsInf.expVal(hams[1], 1)[0].real());
		std::cout << "\ndt = " << cdt << "\n# of evolution = " << steps_max << "\n";
		std::cout << "Energy = " << std::setprecision(10) << eng << "\n\n";
	}

	mpsInf.exportMPS( df_dir );
	hams.clear();

	return 0;
}
