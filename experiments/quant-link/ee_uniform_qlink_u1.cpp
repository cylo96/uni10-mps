#include <sstream>
#include <mps.hpp>

//======================================

void errMsg( char* arg ) {
	std::cerr << "Not Enough Arguments!" << std::endl;
	std::cerr << "Usage: " << arg << " <lat_size> <virt_dim> <mps_dir>" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 4) {
		errMsg( argv[0] );
		return 1;
	}

	// initialize IBC chain
	int L, X;
	std::string wf_dir = "mps-obc-qlink";

	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;
	if (argc > 3)
		wf_dir = std::string( argv[3] );

	std::vector<uni10::Qnum> qphys = uni10::CUniTensor( wf_dir + "/gamma_0" ).bond()[1].Qlist();

	ChainQnIBC mpsIBC( L, X, qphys );
	mpsIBC.importMPS( wf_dir );

	std::cout << "#r\tEntEnt(r)\n";
	for (int r = 0; r < L; ++r)
		std::cout << r << '\t' << std::scientific << std::setprecision(14)
			<< 0.5 * (entangleEntropyQn(mpsIBC.getLambda(r)) + entangleEntropyQn(mpsIBC.getLambda(r+1))) << '\n';

	return 0;
}
