#include <sstream>
#include <mps.hpp>
#include "components_qlink_u1.hpp"

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H qlink <mu> <epsilon>    : Quantum LINK model spin-1/2 Hamiltonian" << std::endl;
	std::cerr << "-cstr <U_link> <mu_bdry>   : interactions for ConSTRaint" << std::endl;
	std::cerr << "-w <wf_dir>                : load initial Wavefuction fom directory" << std::endl;
	std::cerr << "-df <mps_dir>              : save final mps to Destination Folder arg" << std::endl;
	std::cerr << "-init [afm / fm]           : INITialize wavefuction with AFM/FM product state" << std::endl;
	std::cerr << "-l <len>                   : system Length" << std::endl;
	std::cerr << "-m <bond_dim>              : Max bond dimension" << std::endl;
	std::cerr << "-s <steps>                 : number of evolution Steps" << std::endl;
	std::cerr << "-it <dT>                   : evolution (Imaginary) Timestep" << std::endl;
	std::cerr << "-rt <dt>                   : evolution (Real) Timestep" << std::endl;
	std::cerr << "-ord <TS_order>            : ORDer of Trotter-Suzuki expansion" << std::endl;
	std::cerr << "-V                         : Verbose. show information during tebd update" << std::endl;
	std::cerr << "-tol <tol>                 : error TOLerance for truncation error" << std::endl;
	std::cerr << "-intm <steps>              : save INTerMediate state every arg steps" << std::endl;
}

//======================================

uni10::CUniTensor BoundaryDummyBond(bool left) {
	///
	uni10::CUniTensor dummy;
	if (left)
		dummy.assign(std::vector<uni10::Bond> {uni10::Bond(uni10::BD_IN, 3)});
	else
		dummy.assign(std::vector<uni10::Bond> {uni10::Bond(uni10::BD_OUT, 3)});
	dummy.setRawElem(std::vector<Complex> {Complex(1., 0.), Complex(0., 0.), Complex(0., 0.)});
	return dummy;
}

//======================================

uni10::CUniTensor MakeBdryGamma(uni10::CUniTensor gam, bool left) {
	///
	uni10::CUniTensor dummy = BoundaryDummyBond(left);
	uni10::CUniTensor gamB = uni10::otimes(dummy, gam);
	if (left) {
		gamB.setLabel(std::vector<int> {0, 10, 100, 1});
		gamB.combineBond(std::vector<int> {0, 10});
	}
	else {
		gamB.setLabel(std::vector<int> {0, 100, 1, 10});
		gamB.combineBond(std::vector<int> {1, 10});
	}
	return gamB;
}

//======================================
// 
// uni10::CUniTensor DriveHam(std::vector<uni10::CUniTensor>& hams, double para, Complex dt) {
// 	///
// }
// 
//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// inf wavefunction directory
	std::string wf_dir = "mps-inf";
	// final mps directory
	std::string mps_dir = "mps-obc-qlink";
	// load mps from file?
	bool load_file = false;
	// initialize mps with product state?
	int init_prod = 0;
	// system length
	int len = 100;
	// bond dimension
	int bd_dim = 5;
	// imag dt
	double dT = 0.0;
	// real dt
	double dt = 0.0;
	// number of steps
	int step = 10;
	// order of Trotter-Suzuki expansion
	int ts_order = 1;
	// tolerance for truncation error
	double tolerance = 1e-15;
	// show truncation error
	bool verbose = false;
	// save intermediate state?
	bool save_intm = false;
	int intm = 2;

	double mu  = 1.0;
	double eps = 1.0;
	double gsh = 1.0;  // g-squre-half = (g**2)/2
	double Ulk = 100;  // U_link
	double mub = 100;  // mu_boundary

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-H") {
			if (std::string(argv[i+1]) == "qlink") {
				if (argc < i+3) {
					errMsg(argv[0]);
					return 1;
				}
				std::stringstream(argv[i+2]) >> mu;
				std::stringstream(argv[i+3]) >> eps;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-cstr") {
			if (i + 2 < argc) {
				std::stringstream(argv[i+1]) >> Ulk;
				std::stringstream(argv[i+2]) >> mub;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				wf_dir = std::string(argv[i+1]);
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-init") {
			if (std::string(argv[i+1]) == "afm") {
				init_prod = 1;
				load_file = false;
			}
			else if (std::string(argv[i+1]) == "fm") {
				init_prod = 2;
				load_file = false;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> step;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-it") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> dT;
				dt = 0.0;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-rt") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> dt;
				dT = 0.0;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ord") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> ts_order;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) {
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
	}

	/// main function body
	/// initialize OBC Chain
	std::vector<uni10::Qnum> phys_dim = III.bond()[0].Qlist();
	std::vector<uni10::Qnum> qv_vec;
	for (int i = 0; i <= len; ++i) {
		if (i%2)
			qv_vec.push_back(uni10::Qnum(1));
		else
			qv_vec.push_back(uni10::Qnum(0));
	}

	ChainQnOBC mpsOBC(len, 1, phys_dim, uni10::Qnum(0), uni10::Qnum(0), uni10::Qnum(0), uni10::Qnum(0));
	if (load_file)
		mpsOBC.importMPS(wf_dir, 2, false);
	else {
		mpsOBC.init(qv_vec);
		mpsOBC.randomize();
		if (init_prod == 1) {
			// AFM prod state
			for (int i = 1; i < len-1; ++i) {
				if (i%2)
					mpsOBC.putGamma(gamE1, i);  // odd site in mps == even site in PRL paper
				else
					mpsOBC.putGamma(gamO0, i);  // even site in mps == odd site in PRL paper
			}
			mpsOBC.putGamma(MakeBdryGamma(gamO0, true), 0);
			mpsOBC.putGamma(MakeBdryGamma(gamE1, false), len-1);
		}
		else if (init_prod == 2) {
			// FM prod state
			for (int i = 1; i < len-1; ++i) {
				if (i%2)
					mpsOBC.putGamma(gamE0, i);  // odd site in mps == even site in PRL paper
				else
					mpsOBC.putGamma(gamO1, i);  // even site in mps == odd site in PRL paper
			}
			mpsOBC.putGamma(MakeBdryGamma(gamO1, true), 0);
			mpsOBC.putGamma(MakeBdryGamma(gamE0, false), len-1);
		}
	}
	mpsOBC.setMaxBD(bd_dim);
	mpsOBC.setSchValCutoff(1e-10);
  
	/// set qnum constraints
	std::vector<std::vector<uni10::Qnum>> fix_qns;
	for (int i = 0; i <= len; ++i) {
		std::vector<uni10::Qnum> qv = {qv_vec[i]};
		fix_qns.push_back(qv);
	}
	mpsOBC.setFixQn(fix_qns);

	/// 2-site Hamiltonians
	uni10::CUniTensor E = 0.5 * (uni10::otimes(III, NII) + (-1.)*uni10::otimes(IIN, III));
	uni10::CUniTensor E2 = Square(E);
	double sgn;
	std::vector<uni10::CUniTensor> hams(len-1, uni10::CUniTensor());
	hams[0] = gsh * E2
		+ mu  * (-1.) * ((1.0) * uni10::otimes(INI, III) + (-.5) * uni10::otimes(III, INI))
		+ eps * (-1.) * (uni10::otimes(IPM, PMI) + uni10::otimes(IMP, MPI))
		+ Ulk * (+2.) * uni10::otimes(IINi, NiII)
		+ mub * (-1.) * uni10::otimes(NiII, III);
	for (int i = 1; i < len-2; ++i) {
		sgn = (i%2)? 1. : -1.;  // odd site in mps == even site in PRL paper
		hams[i] = gsh * E2
			+ mu  * (sgn) * ((0.5) * uni10::otimes(INI, III) + (-.5) * uni10::otimes(III, INI))
			+ eps * (-1.) * (uni10::otimes(IPM, PMI) + uni10::otimes(IMP, MPI))
			+ Ulk * (+2.) * uni10::otimes(IINi, NiII);
	}
	sgn = ((len-2)%2)? 1. : -1.;
	hams[len-2] = gsh * E2
		+ mu  * (sgn) * ((0.5) * uni10::otimes(INI, III) + (-1.) * uni10::otimes(III, INI))
		+ eps * (-1.) * (uni10::otimes(IPM, PMI) + uni10::otimes(IMP, MPI))
		+ Ulk * (+2.) * uni10::otimes(IINi, NiII);
		+ mub * (+1.) * uni10::otimes(III, IINi);

	/// perform TEBD
	std::vector<uni10::CUniTensor> U;
	for (int i = 0; i < len-1; ++i)
		U.push_back( takeExpQn(I * Complex(dt, dT), hams[i]) );
		
	for (int s = 0; s < step; ++s) {
		if (save_intm && s%intm == 0)
			mpsOBC.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );
			
		// DriveHam(hams, 0.0, Complex(0,0));
		
		if (ts_order == 1)
			mpsOBC.tebdImp( U, 1, verbose, true );
		else
			mpsOBC.tebdImp( hams, Complex(dt, dT), 1, ts_order, verbose, true );
		
		// Measurements?
	}
  
	mpsOBC.exportMPS( mps_dir );
	hams.clear();
	U.clear();

	return 0;
}
