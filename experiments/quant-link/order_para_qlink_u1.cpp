#include <sstream>
#include <mps.hpp>
#include "components_qlink_u1.hpp"

//======================================

uni10::CUniTensor calcExpV( ChainQnIBC& mpsIBC, uni10::CUniTensor& O1, int loc,
	const std::vector<uni10::CUniTensor>& lvecs, const std::vector<uni10::CUniTensor>& rvecs ) {
	///
	uni10::CUniTensor expv;
	uni10::CUniTensor lv, rv, ket;
	int L = mpsIBC.getSize();
	int obn = O1.bondNum();
	int skip = (obn/2) - 1;

	if ( obn < 2 || obn > 4 ) {
		std::cerr << "In calcExpV : Unsupported operator.\n";
		return expv;
	}
	if ( obn%2 )
		return expv;

	if ( loc == 0 ) {
		lv.assign( mpsIBC.getLambda(0).bond() );
		lv.identity();
		rv = rvecs[1+skip];
	}
	else if ( loc == L-1-skip ) {
		lv = lvecs[L-2-skip];
		rv.assign( mpsIBC.getLambda(L).bond() );
		rv.identity();
	}
	else {
		lv = lvecs[loc-1];
		rv = rvecs[loc+1+skip];
	}

	if ( skip == 0 )
		ket = netLGL( mpsIBC.getLambda(loc), mpsIBC.getGamma(loc), mpsIBC.getLambda(loc+1) );
	else if ( skip == 1 )
		ket = netLGLGL( mpsIBC.getLambda(loc), mpsIBC.getGamma(loc),
			mpsIBC.getLambda(loc+1), mpsIBC.getGamma(loc+1), mpsIBC.getLambda(loc+2) );
	lv = buildLRVecQn( lv, ket, O1, true );

	expv = contrLRVecQn( lv, rv );
	return expv;
}

//======================================

void errMsg( char* arg ) {
	std::cerr << "Not Enough Arguments!" << std::endl;
	std::cerr << "Usage: " << arg << " <lat_size> <virt_dim> <mps_dir>" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 4) {
		errMsg( argv[0] );
		return 1;
	}

	// operator to measure
	uni10::CUniTensor E = 0.5 * (uni10::otimes(III, NII) + (-1.)*uni10::otimes(IIN, III));

	// initialize IBC chain
	int L, X;
	std::string wf_dir = "mps-obc-qlink";
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;
	wf_dir = std::string( argv[3] );

	std::vector<uni10::Qnum> qphys = uni10::CUniTensor( wf_dir + "/gamma_0" ).bond()[1].Qlist();
	ChainQnIBC mpsIBC(L, X, qphys);
	mpsIBC.importMPS( wf_dir );

	// import operators
	uni10::CUniTensor op = E;
	int len_op = op.inBondNum();

	std::vector<uni10::Bond> bdi;
	bdi.push_back( uni10::Bond(uni10::BD_IN, qphys) );
	bdi.push_back( uni10::Bond(uni10::BD_OUT, qphys) );
	uni10::CUniTensor id(bdi);
	id.identity();

	const std::vector<uni10::CUniTensor>& gams = mpsIBC.getGamVec();
	const std::vector<uni10::CUniTensor>& lams = mpsIBC.getLamVec();
	std::vector<uni10::CUniTensor> lvecs = allLRVecsQn( gams, lams, true );
	std::vector<uni10::CUniTensor> rvecs = allLRVecsQn( gams, lams, false );

	double order_para = 0.0;
	uni10::CUniTensor expt;
	uni10::CUniTensor norm = calcExpV( mpsIBC, id, 0, lvecs, rvecs );

	for (int r = 0; r < L-(len_op/2); ++r) {
		expt = calcExpV( mpsIBC, op, r, lvecs, rvecs );
		order_para += expt[0].real()/norm[0].real();
	}
	order_para *= (1./L);
	std::cout << std::scientific << std::setprecision(14) << order_para << '\n';

	return 0;
}
