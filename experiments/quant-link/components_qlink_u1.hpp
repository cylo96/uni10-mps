#include <numeric>
#include <mps.hpp>

//======================================

uni10::CUniTensor Square(uni10::CUniTensor op) {
	///
	uni10::CUniTensor dp = op; // dupe
	int bn = op.bondNum();
	std::vector<int> lab_op(bn), lab_dp(bn);
	std::iota(std::begin(lab_op), std::end(lab_op), 0);
	std::iota(std::begin(lab_dp), std::end(lab_dp), op.inBondNum());
	op.setLabel(lab_op);
	dp.setLabel(lab_dp);
	uni10::CUniTensor op2 = uni10::contract(op, dp);
	op2.setLabel(lab_op);
	return op2;
}

//======================================

uni10::CUniTensor Bundle(uni10::CUniTensor op) {
	///
	std::vector<int> lab_op = {0, 1, 2, 3, 4, 5};
	std::vector<int> labi = {0, 1, 2};
	std::vector<int> labo = {3, 4, 5};
	std::vector<int> labf = {0, 1};
	op.setLabel(lab_op);
	op.combineBond(labi);
	op.combineBond(labo);
	op.setLabel(labf);
	return op;
}

//======================================

uni10::CUniTensor ProdState(int nr, int nf, int nl, int qin = -1) {
	///
	std::vector<uni10::Bond> bd0 = {uni10::Bond(uni10::BD_IN, 2)};
	uni10::CUniTensor up = uni10::CUniTensor(bd0);
	uni10::CUniTensor dn = uni10::CUniTensor(bd0);
	up.setRawElem(std::vector<Complex>{Complex(1., 0.), Complex(0., 0.)});
	dn.setRawElem(std::vector<Complex>{Complex(0., 0.), Complex(1., 0.)});
	uni10::CUniTensor pr = (nr)? up : dn;
	uni10::CUniTensor pf = (nf)? up : dn;
	uni10::CUniTensor pl = (nl)? up : dn;
	uni10::CUniTensor gam0 = uni10::otimes(uni10::otimes(pr, pf), pl);
	
	int qtot = (2*nr-1) + (2*nf-1) + (2*nl-1);
	if (qin < 0)
		qin = (int)(qtot < 0);
	int qout = qin + qtot;
	
	std::vector<uni10::Qnum> qnp = {uni10::Qnum(1), uni10::Qnum(-1)};
	std::vector<uni10::Bond> bdg = {
		uni10::Bond(uni10::BD_IN, std::vector<uni10::Qnum>{uni10::Qnum(qin)}),
		uni10::Bond(uni10::BD_IN, qnp), uni10::Bond(uni10::BD_IN, qnp), uni10::Bond(uni10::BD_IN, qnp),
		uni10::Bond(uni10::BD_OUT, std::vector<uni10::Qnum>{uni10::Qnum(qout)})};
	uni10::CUniTensor gamU1(bdg);
	gamU1.setRawElem(gam0.getRawElem());
	
	gamU1.setLabel(std::vector<int>{0, 100, 101, 102, 1});
	gamU1.combineBond(std::vector<int>{100, 101, 102});
	return gamU1;
}

//======================================

uni10::CUniTensor DummyLam(int qn) {
	///
	std::vector<uni10::Qnum> qn_vec = {uni10::Qnum(qn)};
	std::vector<uni10::Bond> bdl = {uni10::Bond(uni10::BD_IN, qn_vec), uni10::Bond(uni10::BD_OUT, qn_vec)};
	uni10::CUniTensor lam(bdl);
	lam.identity();
	return lam;
}

//======================================

// define component operators
uni10::CUniTensor Sz  = opU1( "Sz" );  // Sz
uni10::CUniTensor Sp  = opU1( "Sp" );  // S+
uni10::CUniTensor Sm  = opU1( "Sm" );  // S-
uni10::CUniTensor sz  = opU1( "sz" );  // sigmaz
uni10::CUniTensor sp  = opU1( "sp" );  // sigma+
uni10::CUniTensor sm  = opU1( "sm" );  // sigma-
uni10::CUniTensor id  = opU1( "id" );  // I
uni10::CUniTensor Nf  = Sz + 0.5*id;   // N_fm
uni10::CUniTensor Nb  = Nf;            // N_bs
uni10::CUniTensor Nbi = Sz;            // N_bs - 1/2
uni10::CUniTensor Nb2 = Square(Nb);    // N_bs**2
uni10::CUniTensor bp  = sp;            // b+
uni10::CUniTensor bm  = sm;            // b

// one-site operators
uni10::CUniTensor III  = Bundle(uni10::otimes(uni10::otimes(id, id), id));
uni10::CUniTensor INI  = Bundle(uni10::otimes(uni10::otimes(id, Nf), id));
uni10::CUniTensor IIN  = Bundle(uni10::otimes(uni10::otimes(id, id), Nb));
uni10::CUniTensor NII  = Bundle(uni10::otimes(uni10::otimes(Nb, id), id));
uni10::CUniTensor IPM  = Bundle(uni10::otimes(id, otimesPM(sp, bm)));
uni10::CUniTensor IMP  = Bundle(uni10::otimes(id, otimesPM(sm, bp)));
uni10::CUniTensor PMI  = Bundle(uni10::otimes(otimesPM(bp, sm), id));
uni10::CUniTensor MPI  = Bundle(uni10::otimes(otimesPM(bm, sp), id));
uni10::CUniTensor IINi = Bundle(uni10::otimes(uni10::otimes(id, id), Nbi));
uni10::CUniTensor NiII = Bundle(uni10::otimes(uni10::otimes(Nbi, id), id));
uni10::CUniTensor IIN2 = Bundle(uni10::otimes(uni10::otimes(id, id), Nb2));
uni10::CUniTensor N2II = Bundle(uni10::otimes(uni10::otimes(Nb2, id), id));

// product states
uni10::CUniTensor gamO1 = ProdState(1, 1, 0);
uni10::CUniTensor gamO0 = ProdState(1, 0, 1);
uni10::CUniTensor gamE1 = ProdState(0, 1, 0);
uni10::CUniTensor gamE0 = ProdState(1, 0, 0);
uni10::CUniTensor lamO = DummyLam(0);
uni10::CUniTensor lamE = DummyLam(1);
