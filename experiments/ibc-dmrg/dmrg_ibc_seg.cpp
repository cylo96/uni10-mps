#include <sstream>
#include <math.h>
#include <mps.hpp>
#define _USE_MATH_DEFINES

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H <Ham-mpo>" << std::endl;
	std::cerr << "-H2 <Ham-mpo-1> <Ham-mpo-2>" << std::endl;
	std::cerr << "-H3 <Ham-mpo-1> <Ham-mpo-2> <Ham-mpo-3>" << std::endl;
	std::cerr << "-intf/-imp <INTerFace/IMPurity mpo file/directory>" << std::endl;
	std::cerr << "-intf2/-imp2 <INTerFace/IMPurity-1> <INTerFace/IMPurity-2>" << std::endl;
	std::cerr << "-w <Wavefn-mps>" << std::endl;
	std::cerr << "-w2 <Wavefn-mps-1> <Wavefn-mps-2>" << std::endl;
	std::cerr << "-w3 <Wavefn-mps-1> <Wavefn-mps-2> <Wavefn-mps-3>" << std::endl;
	std::cerr << "-df <Destination Folder>" << std::endl;
	std::cerr << "-l <lattice-Length>" << std::endl;
	std::cerr << "-m <Max bond dimension>" << std::endl;
	std::cerr << "-s <dmrg Sweeps>" << std::endl;
	std::cerr << "-s2 <2-site update dmrg Sweeps>" << std::endl;
	std::cerr << "-u2   dmrg use 2-site Update" << std::endl;
	std::cerr << "-bloc <Boundary/interface-LOCation>" << std::endl;
	std::cerr << "-bloc2 <Boundary/interface-LOCation-1> <Boundary/interface-LOCation-2>" << std::endl;
	std::cerr << "-V   Verbose. show information during dmrg update" << std::endl;
	std::cerr << "-ite <max ITEration for Lanczos algorithm>" << std::endl;
	std::cerr << "-tol <error TOLerance for Lanczos algorithm>" << std::endl;
	std::cerr << "-ramp <tolerance RaMPing factor for Lanczos algorithm>" << std::endl;
	std::cerr << "-intm   save INTerMediate state every arg sweeps" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian mpo directory
	std::string mpo_dir_1 = "mpo-ham-xxz";
	std::string mpo_dir_2 = "mpo-ham-xxz";
	std::string mpo_dir_3 = "mpo-ham-xxz";
	bool load_2mpo = false;
	bool load_3mpo = false;
	// interface/impurity mpo
	std::string intf_dir_1 = "";
	std::string intf_dir_2 = "";
	// inf wavefunction directory
	std::string wf_dir_1 = "mps-inf";
	std::string wf_dir_2 = "mps-inf";
	std::string wf_dir_3 = "mps-inf";
	// final mps directory
	std::string mps_dir = "mps-ibc-xxz";
	// load mps from file?
	bool load_file = true;
	bool load_2file = false;
	bool load_3file = false;
	// save intermediate state?
	bool save_intm = false;
	int intm = 2;
	// system length
	int len = 100;
	// initial location for dmrg sweep
	int init_loc = 0;
	// boundary location
	int bloc1 = -1;
	int bloc2 = -1;
	// bond dimension
	int bd_dim = 5;
	// number of sweeps
	int sweep = 10;
	int sw_u2 = 0;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// tolerance ramping
	int ramp_tol = 10;
	// 2-site update
	bool two_site_update = false;
	// show truncation error
	bool verbose = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc) {
				mpo_dir_1 = std::string(argv[i+1]);
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-H2") {
			if (i + 2 < argc) {
				mpo_dir_1 = std::string(argv[i+1]);
				mpo_dir_2 = std::string(argv[i+2]);
				load_2mpo = true;
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-H3") {
			if (i + 3 < argc) {
				mpo_dir_1 = std::string(argv[i+1]);
				mpo_dir_2 = std::string(argv[i+2]);
				mpo_dir_3 = std::string(argv[i+3]);
				load_2mpo = true;
				load_3mpo = true;
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intf" || std::string(argv[i]) == "-imp") {
			if (i + 1 < argc) {
				intf_dir_1 = std::string(argv[i+1]);
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intf2" || std::string(argv[i]) == "-imp2") {
			if (i + 2 < argc) {
				intf_dir_1 = std::string(argv[i+1]);
				intf_dir_2 = std::string(argv[i+2]);
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				wf_dir_1 = std::string(argv[i+1]);
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w2") {
			load_2file = true;
			if (i + 2 < argc &&
				((std::string)argv[i+1]).find("-") != std::string::npos &&
				((std::string)argv[i+2]).find("-") != std::string::npos) {
					wf_dir_1 = std::string(argv[i+1]);
					wf_dir_2 = std::string(argv[i+2]);
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w3") {
			load_2file = true;
			load_3file = true;
			if (i + 3 < argc &&
				((std::string)argv[i+1]).find("-") != std::string::npos &&
				((std::string)argv[i+2]).find("-") != std::string::npos &&
				((std::string)argv[i+3]).find("-") != std::string::npos) {
					wf_dir_1 = std::string(argv[i+1]);
					wf_dir_2 = std::string(argv[i+2]);
					wf_dir_3 = std::string(argv[i+3]);
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sweep;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s2") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sw_u2;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-bloc") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bloc1;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-bloc2") {
			if (i + 2 < argc) {
				std::istringstream(argv[i+1]) >> bloc1;
				std::istringstream(argv[i+2]) >> bloc2;
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iter_max;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if ( (std::string(argv[i]) == "-ramp") || (std::string(argv[i]) == "-rmp") ) {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> ramp_tol;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) {
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-u2") {
			two_site_update = true;
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
	}

	/// main function body
	/// build MPO
	std::vector<uni10::CUniTensor> mpo_1, mpo_2, mpo_3;
	importMPO( len, mpo_dir_1, mpo_1, "ibc" );
	if (load_2mpo)
		importMPO( len, mpo_dir_2, mpo_2, "ibc" );
	if (load_3mpo)
		importMPO( len, mpo_dir_3, mpo_3, "ibc" );

	std::vector<uni10::CUniTensor> mpo;
	if (bloc1 < 0 && bloc2 < 0) {
		bloc1 = len;
		bloc2 = len;
	}
	else if (bloc1 >= 0 && bloc2 < bloc1) {
		bloc2 = len;
	}

	// mpo is 2-site longer than mps
	for (int i = 0; i <= bloc1+1; ++i)
		mpo.push_back( mpo_1[i] );
	for (int i = bloc1+2; i <= bloc2+1; ++i)
		mpo.push_back( mpo_2[i] );
	for (int i = bloc2+2; i < len+2; ++i)
		mpo.push_back( mpo_3[i] );

	// impurity
	if (intf_dir_1 == "")
		intf_dir_1 = mpo_dir_1;
	if (intf_dir_2 == "" && load_3mpo)
		intf_dir_2 = mpo_dir_2;
	if (bloc1 >= 0 && bloc1 < len)
		mpo[bloc1+1] = uni10::CUniTensor( intf_dir_1 + "/mpo_imp" );
	if (bloc2 >= 0 && bloc2 < len)
		mpo[bloc2+1] = uni10::CUniTensor( intf_dir_2 + "/mpo_imp" );

	mpo_1.clear(); mpo_2.clear(); mpo_3.clear();

	uni10::CUniTensor hw = op2SiteFromMPO( mpo[1] );
	int phys_dim = hw.bond()[0].dim();

	/// initialize IBC Chain
	ChainIBC mpsIBC( len, phys_dim, bd_dim );

	if (load_file && !load_2file) {
		size_t found = wf_dir_1.find("semiinf");
		if (found != std::string::npos)	// if found semiinf in wf_dir
			mpsIBC.import2Semi( wf_dir_1 );
		else
			mpsIBC.importMPS( wf_dir_1 );
	}

	if (load_2file) {
		size_t found_l = wf_dir_1.find("semiinf");
		if (found_l != std::string::npos)
			mpsIBC.import2Semi( wf_dir_1 );
		else
			mpsIBC.importMPS( wf_dir_1 );

		ChainIBC mps_2( len, phys_dim, bd_dim );
		size_t found_r = wf_dir_2.find("semiinf");
		if (found_r != std::string::npos)
			mps_2.import2Semi( wf_dir_2 );
		else
			mps_2.importMPS( wf_dir_2 );

		for (int i = bloc1+1; i < len; ++i) {
			mpsIBC.putLambda( mps_2.getLambda(i), i );
			mpsIBC.putGamma( mps_2.getGamma(i), i );
		}
		mpsIBC.putLambda( mps_2.getLambda(len), len );

		std::vector<SchmidtVal> spec = entangleSpecQn( mpsIBC.getLambda(bloc1+1), 1 );
		std::vector<uni10::Qnum> ql = mpsIBC.getGamma(bloc1).bond()[2].Qlist();
		std::vector<uni10::Qnum> qr = mpsIBC.getGamma(bloc1+1).bond()[0].Qlist();
		std::vector<uni10::Bond> bd_lam;
		bd_lam.push_back( uni10::Bond( uni10::BD_IN, ql ) );
		bd_lam.push_back( uni10::Bond( uni10::BD_OUT, qr ) );
		// set initial state uncorrelated at center
		uni10::CUniTensor lam_bloc1(bd_lam);
		lam_bloc1.set_zero();
		uni10::CMatrix uni = lam_bloc1.getBlock( spec[0].qn );
		uni.at(0, 0) = Complex(1.0, 0.0);
		lam_bloc1.putBlock( spec[0].qn, uni );
		mpsIBC.putLambda( lam_bloc1, bloc1+1 );
	}

	if (load_3file) {
		ChainIBC mps_2( len, phys_dim, bd_dim );
		ChainIBC mps_3( len, phys_dim, bd_dim );
		mpsIBC.importMPS( wf_dir_1 );
		mps_2.importMPS( wf_dir_2 );
		mps_3.importMPS( wf_dir_3 );

		for (int i = bloc1+1; i <= bloc2; ++i) {
			mpsIBC.putLambda( mps_2.getLambda(i), i );
			mpsIBC.putGamma( mps_2.getGamma(i), i );
		}
		for (int i = bloc2+1; i < len; ++i) {
			mpsIBC.putLambda( mps_3.getLambda(i), i );
			mpsIBC.putGamma( mps_3.getGamma(i), i );
		}
		mpsIBC.putLambda( mps_3.getLambda(len), len );

		std::vector<SchmidtVal> spec1 = entangleSpecQn( mpsIBC.getLambda(bloc1+1), 1 );
		std::vector<uni10::Qnum> ql1 = mpsIBC.getGamma(bloc1).bond()[2].Qlist();
		std::vector<uni10::Qnum> qr1 = mpsIBC.getGamma(bloc1+1).bond()[0].Qlist();
		std::vector<uni10::Bond> bd_lam1;
		bd_lam1.push_back( uni10::Bond( uni10::BD_IN, ql1 ) );
		bd_lam1.push_back( uni10::Bond( uni10::BD_OUT, qr1 ) );
		// set initial state uncorrelated at center
		uni10::CUniTensor lam_bloc1(bd_lam1);
		lam_bloc1.set_zero();
		uni10::CMatrix uni = lam_bloc1.getBlock( spec1[0].qn );
		uni.at(0, 0) = Complex(1.0, 0.0);
		lam_bloc1.putBlock( spec1[0].qn, uni );
		mpsIBC.putLambda( lam_bloc1, bloc1+1 );

		std::vector<SchmidtVal> spec2 = entangleSpecQn( mpsIBC.getLambda(bloc2+1), 1 );
		std::vector<uni10::Qnum> ql2 = mpsIBC.getGamma(bloc2).bond()[2].Qlist();
		std::vector<uni10::Qnum> qr2 = mpsIBC.getGamma(bloc2+1).bond()[0].Qlist();
		std::vector<uni10::Bond> bd_lam2;
		bd_lam2.push_back( uni10::Bond( uni10::BD_IN, ql2 ) );
		bd_lam2.push_back( uni10::Bond( uni10::BD_OUT, qr2 ) );
		// set initial state uncorrelated at center
		uni10::CUniTensor lam_bloc2(bd_lam2);
		lam_bloc2.set_zero();
		uni = lam_bloc2.getBlock( spec2[0].qn );
		uni.at(0, 0) = Complex(1.0, 0.0);
		lam_bloc2.putBlock( spec2[0].qn, uni );
		mpsIBC.putLambda( lam_bloc2, bloc2+1 );
	}

	/// perform DMRG
	int s = 0;
	if (save_intm) {
		s = 0;
		while ( s < sweep ) {
			init_loc = (s == 0)? init_loc : 0;
			if (two_site_update)
				mpsIBC.dmrgImpU2( mpo, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			else {
				if ( s < sweep-sw_u2 )
					mpsIBC.dmrgImp( mpo, intm, iter_max,
						tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
				else
					mpsIBC.dmrgImpU2( mpo, intm, iter_max,
						tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			}
			s += intm;
			mpsIBC.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );
		}
	}
	else {
		if (two_site_update)
			mpsIBC.dmrgImpU2( mpo, sweep, iter_max, tolerance, verbose );
		else
			mpsIBC.dmrgImp( mpo, sweep, iter_max, tolerance, verbose );
	}

	mpsIBC.exportMPS( mps_dir );
	mpo.clear();

	return 0;
}
