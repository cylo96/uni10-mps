#include <sstream>
#include <math.h>
#include <mps.hpp>
#define _USE_MATH_DEFINES

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H itf arg1 arg2  Hamiltonian - Ising w/ Transverse Field with args = J, h" << std::endl;
	std::cerr << "-imp arg0 arg1 arg2  IMPurity Hamiltonian with args = location, J, h" << std::endl;
	std::cerr << "-w arg  load inf Wavefunction (gamma and lambda tensors) from folder arg" << std::endl;
	std::cerr << "-df arg  save final mps to Destination Folder arg" << std::endl;
	std::cerr << "-n arg  Network file folder arg" << std::endl;
	std::cerr << "-b arg  load Boundary operators from folder arg" << std::endl;
	std::cerr << "-l arg  system Length" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of dmrg Sweeps" << std::endl;
	std::cerr << "-s2 arg  number of 2-site update dmrg Sweeps after 1-site update" << std::endl;
	std::cerr << "-u2  dmrg performs 2-site Update" << std::endl;
	std::cerr << "-V  Verbose. show information during dmrg update" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-ramp arg  tolerance RaMPing factor for Lanczos algorithm" << std::endl;
	std::cerr << "-intm arg  save INTerMediate state every arg sweeps" << std::endl;
	std::cerr << "-alt  ALTernative Hamiltonian. here means swap Sx and Sz in Hamiltonian" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// // hamiltonian mpo directory
	// std::string mpo_dir = "mpo-ham-itf";
	// inf wavefunction directory
	std::string wf_dir = "mps-inf";
	// final mps directory
	std::string mps_dir = "mps-ibc-itf";
	// Network file directory
	std::string net_dir = ".";
	// inf boundary hamiltonian directory
	std::string hb_dir = ".";
	// load mps from file?
	bool load_file = true;
	// save intermediate state?
	bool save_intm = false;
	int intm = 2;
	// system length
	int len = 100;
	// bond dimension
	int bd_dim = 5;
	// number of sweeps
	int sweep = 10;
	int sw_u2 = 0;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// tolerance ramping
	int ramp_tol = 10;
	// 2-site update
	bool two_site_update = false;
	// show truncation error
	bool verbose = false;
	// swap Sx and Sz in Hamiltonian
	bool alter = false;

	std::vector<std::vector<double>> imps;
	double J = -1.0;
	double h = 1.0;	// transverse field

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-H") {
			if (std::string(argv[i+1]) == "itf") { // Make sure we aren't at the end of argv!
				if (argc < i+3) {
					errMsg(argv[0]);
					return 1;
				}
				std::stringstream(argv[i+2]) >> J;
				std::stringstream(argv[i+3]) >> h;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-imp") {
			if (argc < i+3) {
				errMsg(argv[0]);
				return 1;
			}
			std::vector<double> imp_params(3);
			std::stringstream(argv[i+1]) >> imp_params[0];	// location index
			std::stringstream(argv[i+2]) >> imp_params[1];	// J_imp
			std::stringstream(argv[i+3]) >> imp_params[2];	// h_imp
			imps.push_back(imp_params);
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				wf_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-w option requires the directory of the Wavefunction MPS." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-df option requires a string of Destination Folder's name for final mps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-n") {
			if (i + 1 < argc) {
				net_dir = std::string(argv[i+1]);
			}
			else {
				std::cerr << "-n option requires the directory of Network files." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-b") {
			if (i + 1 < argc)
				hb_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-b option requires the directory of the Boundary Hamiltonians." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l option requires a positive integer system length." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sweep;
			else {
				std::cerr << "-s option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s2") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sw_u2;
			else {
				std::cerr << "-s2 option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iter_max;
			else {
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
		else if ( (std::string(argv[i]) == "-ramp") || (std::string(argv[i]) == "-rmp") ) {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> ramp_tol;
			else {
				std::cerr << "-ramp option requires a positive integer number of tolerance ramping factor." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) {
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else {
				std::cerr << "-intm option requires a positive integer number of sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-u2") {
			two_site_update = true;
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
		else if (std::string(argv[i]) == "-alt") {
			alter = true;
		}
	}

	/// main function body
	/// build MPO
	std::vector<uni10::CUniTensor> mpo;

	uni10::CUniTensor sz = OP( "sz" );
	uni10::CUniTensor sx = OP( "sx" );
	uni10::CUniTensor id = OP( "id" );

	uni10::CUniTensor hl = uni10::CUniTensor( hb_dir + "/HL" );
	uni10::CUniTensor sx_l = uni10::CUniTensor( hb_dir + "/Sx_L" );
	uni10::CUniTensor sz_l = uni10::CUniTensor( hb_dir + "/Sz_L" );
	uni10::CUniTensor id_l = uni10::CUniTensor( hb_dir + "/Id_L" );

	uni10::CUniTensor hr = uni10::CUniTensor( hb_dir + "/HR" );
	uni10::CUniTensor sx_r = uni10::CUniTensor( hb_dir + "/Sx_R" );
	uni10::CUniTensor sz_r = uni10::CUniTensor( hb_dir + "/Sz_R" );
	uni10::CUniTensor id_r = uni10::CUniTensor( hb_dir + "/Id_R" );

	uni10::CUniTensor op1 = (alter)? sz : sx;
	uni10::CUniTensor op2 = (alter)? sx : sz;
	uni10::CUniTensor op1_l = (alter)? sz_l : sx_l;
	uni10::CUniTensor op2_l = (alter)? sx_l : sz_l;
	uni10::CUniTensor op1_r = (alter)? sz_r : sx_r;
	uni10::CUniTensor op2_r = (alter)? sx_r : sz_r;

	MPO mpo_l(3, 'l');
	MPO mpo_m(3, 'm');
	MPO mpo_r(3, 'r');

	mpo_l.putTensor( hl + 0.5*h*op1_l, 0, 0 );
	mpo_l.putTensor( J*op2_l, 0, 1 );
	mpo_l.putTensor( id_l, 0, 2 );

	mpo_m.putTensor( id, 0, 0 );
	mpo_m.putTensor( op2, 1, 0 );
	mpo_m.putTensor( h*op1, 2, 0 );
	mpo_m.putTensor( J*op2, 2, 1 );
	mpo_m.putTensor( id, 2, 2 );

	mpo_r.putTensor( id_r, 0, 0 );
	mpo_r.putTensor( op2_r, 1, 0 );
	mpo_r.putTensor( 0.5*h*op1_r + hr, 2, 0 );

	uni10::CUniTensor mpo_mt = mpo_m.launch();
	mpo.push_back(mpo_l.launch());
	for (int i = 0; i < len; ++i)
		mpo.push_back(mpo_mt);
	mpo.push_back(mpo_r.launch());

	// impurities
	int num_imps = imps.size();
	if (num_imps > 0) {
		for (int i = 0; i < num_imps; ++i) {
			MPO mpo_imp(3, 'm');
			mpo_imp.putTensor( id, 0, 0 );
			mpo_imp.putTensor( op2, 1, 0 );
			mpo_imp.putTensor( imps[i][2]*op1, 2, 0 );
			mpo_imp.putTensor( imps[i][1]*op2, 2, 1 );
			mpo_imp.putTensor( id, 2, 2 );
			mpo[ (int)imps[i][0]+1 ] = mpo_imp.launch();
		}
	}

	uni10::CUniTensor hw = op2SiteFromMPO( mpo[1] );
	int phys_dim = hw.bond()[0].dim();

	/// initialize IBC Chain
	ChainIBC mpsIBC( len, phys_dim, bd_dim );
	mpsIBC.loadNetIBC( net_dir );

	if (load_file) {
		size_t found = wf_dir.find("semiinf");
		if (found != std::string::npos)	// if found semiinf in wf_dir
			mpsIBC.import2Semi( wf_dir );
		else
			mpsIBC.importMPS( wf_dir );
	}

	/// perform DMRG
	int s = 0;
	if (save_intm) {
		s = 0;
		while ( s < sweep ) {
			if (two_site_update)
				sw_u2 = sweep;
			if ( s < sweep-sw_u2 )
				mpsIBC.dmrgImp( mpo, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			else
				mpsIBC.dmrgImpU2( mpo, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			s += intm;
			mpsIBC.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );
		}
	}
	else {
		if (two_site_update)
			mpsIBC.dmrgImpU2( mpo, sweep, iter_max, tolerance, verbose );
		else
			mpsIBC.dmrgImp( mpo, sweep, iter_max, tolerance, verbose );
	}

	mpsIBC.exportMPS( mps_dir );
	mpo.clear();

	return 0;
}
