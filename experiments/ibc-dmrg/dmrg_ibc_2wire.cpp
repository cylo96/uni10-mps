#include <sstream>
#include <math.h>
#include <mps.hpp>
#define _USE_MATH_DEFINES

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H2 arg1 arg2  load Hamiltonian mpo from folder args" << std::endl;
	std::cerr << "-intf/-imp arg  load INTerFace/IMPurity mpo from folder arg" << std::endl;
	std::cerr << "-w arg  load Wavefunction (gamma and lambda tensors) from folder arg" << std::endl;
	std::cerr << "-w2 arg1 arg2  load inf Wavefunction (gamma and lambda tensors) from folder args" << std::endl;
	std::cerr << "-df arg  save final mps to Destination Folder arg" << std::endl;
	std::cerr << "-l arg  system Length" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of dmrg Sweeps" << std::endl;
	std::cerr << "-s2 arg  number of 2-site update dmrg Sweeps after 1-site update" << std::endl;
	std::cerr << "-u2  dmrg performs 2-site Update" << std::endl;
	std::cerr << "-bloc arg  Boundary/interface Location" << std::endl;
	std::cerr << "-brk arg  BReaK link/entanglement at location arg" << std::endl;
	std::cerr << "-V  Verbose. show information during dmrg update" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-ramp arg  tolerance RaMPing factor for Lanczos algorithm" << std::endl;
	std::cerr << "-intm arg  save INTerMediate state every arg sweeps" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian mpo directory
	std::string mpo_l_dir = "mpo-ham-xxz";
	std::string mpo_r_dir = "mpo-ham-xxz";
	// interface/impurity mpo
	std::string intf_dir = "";
	// inf wavefunction directory
	std::string wf_dir = "mps-inf";
	std::string wf_l_dir = "mps-inf";
	std::string wf_r_dir = "mps-inf";
	// final mps directory
	std::string mps_dir = "mps-ibc-xxz";
	// load mps from file?
	bool load_file = true;
	bool load_2file = false;
	// save intermediate state?
	bool save_intm = false;
	int intm = 2;
	// system length
	int len = 100;
	// initial location for dmrg sweep
	int init_loc = 0;
	// boundary location
	int mid = -1;
	// broken link
	int brk_loc = -1;
	// bond dimension
	int bd_dim = 5;
	// number of sweeps
	int sweep = 10;
	int sw_u2 = 0;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// tolerance ramping
	int ramp_tol = 10;
	// 2-site update
	bool two_site_update = false;
	// show truncation error
	bool verbose = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H2") {
			if (i + 2 < argc) {
				mpo_l_dir = std::string(argv[i+1]);
				mpo_r_dir = std::string(argv[i+2]);
			}
			else {
				std::cerr << "-H2 option requires two directories of the Hamiltonian MPOs." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intf" || std::string(argv[i]) == "-imp") {
			if (i + 1 < argc) {
				intf_dir = std::string(argv[i+1]);
			}
			else {
				std::cerr << "-intf/-imp option requires the directory to interface/impurity mpo" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				wf_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-w option requires the directory of the Wavefunction MPS." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w2") {
			load_2file = true;
			if (i + 2 < argc &&
				((std::string)argv[i+1]).find("-") != std::string::npos &&
				((std::string)argv[i+2]).find("-") != std::string::npos) {
					wf_l_dir = std::string(argv[i+1]);
					wf_r_dir = std::string(argv[i+2]);
			}
			else {
				std::cerr << "-w2 option requires directories of the Wavefunction MPS." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-df option requires a string of Destination Folder's name for final mps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l option requires a positive integer system length." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sweep;
			else {
				std::cerr << "-s option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s2") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sw_u2;
			else {
				std::cerr << "-s2 option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-bloc") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> mid;
			else {
				std::cerr << "-bloc <(int) boundary location>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-brk") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> brk_loc;
			else {
				std::cerr << "-brk option requires a positive integer location of the broken link." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iter_max;
			else {
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
		else if ( (std::string(argv[i]) == "-ramp") || (std::string(argv[i]) == "-rmp") ) {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> ramp_tol;
			else {
				std::cerr << "-ramp option requires a positive integer number of tolerance ramping factor." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) {
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else {
				std::cerr << "-intm option requires a positive integer number of sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-u2") {
			two_site_update = true;
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
	}

	/// main function body
	/// build MPO
	std::vector<uni10::CUniTensor> mpo_l;
	importMPO( len, mpo_l_dir, mpo_l, "ibc" );
	std::vector<uni10::CUniTensor> mpo_r;
	importMPO( len, mpo_r_dir, mpo_r, "ibc" );

	std::vector<uni10::CUniTensor> mpo;
	if (mid < 0) mid = len/2 - 1;
	// mpo is 2-site longer than mps
	for (int i = 0; i <= mid+1; ++i)
		mpo.push_back( mpo_l[i] );
	for (int i = mid+2; i < len+2; ++i)
		mpo.push_back( mpo_r[i] );

	// impurity
	if (intf_dir == "")
		intf_dir = mpo_l_dir;
	mpo[mid+1] = uni10::CUniTensor( intf_dir + "/mpo_imp" );

	mpo_l.clear(); mpo_r.clear();

	uni10::CUniTensor hw = op2SiteFromMPO( mpo[1] );
	int phys_dim = hw.bond()[0].dim();

	/// initialize IBC Chain
	ChainIBC mpsIBC( len, phys_dim, bd_dim );

	if (load_file && !load_2file) {
		size_t found = wf_dir.find("semiinf");
		if (found != std::string::npos)	// if found semiinf in wf_dir
			mpsIBC.import2Semi( wf_dir );
		else
			mpsIBC.importMPS( wf_dir );
	}

	if (load_2file) {
		size_t found_l = wf_l_dir.find("semiinf");
		if (found_l != std::string::npos)
			mpsIBC.import2Semi( wf_l_dir );
		else
			mpsIBC.importMPS( wf_l_dir );

		ChainIBC mps_r( len, phys_dim, bd_dim );
		size_t found_r = wf_r_dir.find("semiinf");
		if (found_r != std::string::npos)
			mps_r.import2Semi( wf_r_dir );
		else
			mps_r.importMPS( wf_r_dir );

		for (int i = mid+1; i < len; ++i) {
			mpsIBC.putLambda( mps_r.getLambda(i), i );
			mpsIBC.putGamma( mps_r.getGamma(i), i );
		}
		mpsIBC.putLambda( mps_r.getLambda(len), len );

		std::vector<SchmidtVal> spec = entangleSpecQn( mpsIBC.getLambda(mid+1), 1 );
		std::vector<uni10::Qnum> ql = mpsIBC.getGamma(mid).bond()[2].Qlist();
		std::vector<uni10::Qnum> qr = mpsIBC.getGamma(mid+1).bond()[0].Qlist();
		std::vector<uni10::Bond> bd_lam;
		bd_lam.push_back( uni10::Bond( uni10::BD_IN, ql ) );
		bd_lam.push_back( uni10::Bond( uni10::BD_OUT, qr ) );
		// set initial state uncorrelated at center
		uni10::CUniTensor lam_mid(bd_lam);
		lam_mid.set_zero();
		uni10::CMatrix uni = lam_mid.getBlock( spec[0].qn );
		uni.at(0, 0) = Complex(1.0, 0.0);
		lam_mid.putBlock( spec[0].qn, uni );
		mpsIBC.putLambda( lam_mid, mid+1 );
	}

	// break link
	if (brk_loc > 0)
		mpsIBC.breakLink(brk_loc);

	/// perform DMRG
	int s = 0;
	if (save_intm) {
		s = 0;
		while ( s < sweep ) {
			init_loc = (s == 0)? init_loc : 0;
			if (two_site_update)
				mpsIBC.dmrgImpU2( mpo, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			else {
				if ( s < sweep-sw_u2 )
					mpsIBC.dmrgImp( mpo, intm, iter_max,
						tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
				else
					mpsIBC.dmrgImpU2( mpo, intm, iter_max,
						tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			}
			s += intm;
			mpsIBC.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );
		}
	}
	else {
		if (two_site_update)
			mpsIBC.dmrgImpU2( mpo, sweep, iter_max, tolerance, verbose );
		else
			mpsIBC.dmrgImp( mpo, sweep, iter_max, tolerance, verbose );
	}

	mpsIBC.exportMPS( mps_dir );
	mpo.clear();

	return 0;
}
