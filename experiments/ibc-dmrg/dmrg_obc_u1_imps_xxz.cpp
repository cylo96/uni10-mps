#include <sstream>
#include <math.h>
#include <mps.hpp>
#define _USE_MATH_DEFINES

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H arg  load Hamiltonian mpo from folder arg" << std::endl;
	std::cerr << "-imp <loc> <J> <Jz>  IMPurity Hamiltonian" << std::endl;
	std::cerr << "-w arg  load inf Wavefunction (gamma and lambda tensors) from folder arg" << std::endl;
	std::cerr << "-df arg  save final mps to Destination Folder arg" << std::endl;
	std::cerr << "-l arg  system Length" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of dmrg Sweeps" << std::endl;
	std::cerr << "-s2 arg  number of 2-site update dmrg Sweeps after 1-site update" << std::endl;
	std::cerr << "-u2  dmrg performs 2-site Update" << std::endl;
	std::cerr << "-V  Verbose. show information during dmrg update" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-ramp arg  tolerance RaMPing factor for Lanczos algorithm" << std::endl;
	std::cerr << "-intm arg  save INTerMediate state every arg sweeps" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian mpo directory
	std::string mpo_dir = "mpo-ham-xxz";
	// inf wavefunction directory
	std::string wf_dir = "mps-inf";
	// final mps directory
	std::string mps_dir = "mps-obc-xxz";
	// load mps from file?
	bool load_file = true;
	// save intermediate state?
	bool save_intm = false;
	int intm = 2;
	// system length
	int len = 100;
	// bond dimension
	int bd_dim = 5;
	// number of sweeps
	int sweep = 10;
	int sw_u2 = 0;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// tolerance ramping
	int ramp_tol = 10;
	// 2-site update
	bool two_site_update = false;
	// show truncation error
	bool verbose = false;

	std::vector<std::vector<double>> imps;

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc)
				mpo_dir = std::string(argv[i+1]);
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-imp") {
			if (argc < i+3) {
				errMsg(argv[0]);
				return 1;
			}
			std::vector<double> imp_params(3);
			std::stringstream(argv[i+1]) >> imp_params[0];	// location index
			std::stringstream(argv[i+2]) >> imp_params[1];	// J_imp
			std::stringstream(argv[i+3]) >> imp_params[2];	// Jz_imp
			imps.push_back(imp_params);
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				wf_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-w option requires the directory of the Wavefunction MPS." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-df option requires a string of Destination Folder's name for final mps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l option requires a positive integer system length." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sweep;
			else {
				std::cerr << "-s option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s2") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sw_u2;
			else {
				std::cerr << "-s2 option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iter_max;
			else {
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
		else if ( (std::string(argv[i]) == "-ramp") || (std::string(argv[i]) == "-rmp") ) {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> ramp_tol;
			else {
				std::cerr << "-ramp option requires a positive integer number of tolerance ramping factor." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) {
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else {
				std::cerr << "-intm option requires a positive integer number of sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-u2") {
			two_site_update = true;
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
	}

	/// main function body
	/// initialize IBC Chain
	uni10::CUniTensor mpo_ms(mpo_dir + "/mpo_ms");
	std::vector<uni10::Qnum> phys_dim = mpo_ms.bond()[1].Qlist();
	ChainQnOBC mpsOBC( len, bd_dim, phys_dim );

	if (load_file)
		mpsOBC.importMPS( wf_dir );
	else
		mpsOBC.randomize();

	/// build MPO
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( len, mpo_dir, mpo_sy, mpo_pl, mpo_mi, true, mpsOBC.getLambda(0), mpsOBC.getLambda(len) );

	uni10::CUniTensor Sp = opU1( "Sp" );
	uni10::CUniTensor Sm = opU1( "Sm" );
	uni10::CUniTensor Sz = opU1( "Sz" );
	uni10::CUniTensor id = opU1( "id" );

	// impurities
	int num_imps = imps.size();
	if (num_imps > 0) {
		for (int i = 0; i < num_imps; ++i) {
			MPO mpo_is(5, 'm');
			MPO mpo_ip(5, 'm');
			MPO mpo_im(5, 'm');

			mpo_is.putTensor( id, 0, 0 );
			mpo_ip.putTensor( Sp, 1, 0 );
			mpo_im.putTensor( Sm, 2, 0 );
			mpo_is.putTensor( Sz, 3, 0 );
			mpo_im.putTensor( 0.5*imps[i][1]*Sm, 4, 1 );
			mpo_ip.putTensor( 0.5*imps[i][1]*Sp, 4, 2 );
			mpo_is.putTensor( imps[i][2]*Sz, 4, 3 );
			mpo_is.putTensor( id, 4, 4 );
			
			mpo_sy[ (int)imps[i][0]+1 ] = mpo_is.launch();
			mpo_pl[ (int)imps[i][0]+1 ] = mpo_ip.launch();
			mpo_mi[ (int)imps[i][0]+1 ] = mpo_im.launch();
		}
	}

	/// perform DMRG
	int s = 0;
	if (save_intm) {
		s = 0;
		while ( s < sweep ) {
			if (two_site_update)
				mpsOBC.dmrgImpU2( mpo_sy, mpo_pl, mpo_mi, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			else {
				if ( s < sweep-sw_u2 )
					mpsOBC.dmrgImp( mpo_sy, mpo_pl, mpo_mi, intm, iter_max,
						tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
				else
					mpsOBC.dmrgImpU2( mpo_sy, mpo_pl, mpo_mi, intm, iter_max,
						tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			}
			s += intm;
			mpsOBC.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );
		}
	}
	else {
		if (two_site_update)
			mpsOBC.dmrgImpU2( mpo_sy, mpo_pl, mpo_mi, sweep, iter_max, tolerance, verbose );
		else
			mpsOBC.dmrgImp( mpo_sy, mpo_pl, mpo_mi, sweep, iter_max, tolerance, verbose );
	}

	mpsOBC.exportMPS( mps_dir );
	mpo_sy.clear(); mpo_pl.clear(); mpo_mi.clear();

	return 0;
}
