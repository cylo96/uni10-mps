#include <sstream>
#include <math.h>
#include <mps.hpp>
#define _USE_MATH_DEFINES

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H xxz <J> <Jz>  XXZ Hamiltonian" << std::endl;
	std::cerr << "-imp <loc> <J> <Jz>  IMPurity Hamiltonian" << std::endl;
	std::cerr << "-w arg  load inf Wavefunction (gamma and lambda tensors) from folder arg" << std::endl;
	std::cerr << "-df arg  save final mps to Destination Folder arg" << std::endl;
	std::cerr << "-n arg  Network file folder arg" << std::endl;
	std::cerr << "-b arg  load Boundary operators from folder arg" << std::endl;
	std::cerr << "-l arg  system Length" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of dmrg Sweeps" << std::endl;
	std::cerr << "-s2 arg  number of 2-site update dmrg Sweeps after 1-site update" << std::endl;
	std::cerr << "-u2  dmrg performs 2-site Update" << std::endl;
	std::cerr << "-V  Verbose. show information during dmrg update" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-ramp arg  tolerance RaMPing factor for Lanczos algorithm" << std::endl;
	std::cerr << "-intm arg  save INTerMediate state every arg sweeps" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// // hamiltonian mpo directory
	// std::string mpo_dir = "mpo-ham-itf";
	// inf wavefunction directory
	std::string wf_dir = "mps-inf";
	// final mps directory
	std::string mps_dir = "mps-ibc-xxz";
	// Network file directory
	std::string net_dir = ".";
	// inf boundary hamiltonian directory
	std::string hb_dir = ".";
	// load mps from file?
	bool load_file = true;
	// save intermediate state?
	bool save_intm = false;
	int intm = 2;
	// system length
	int len = 100;
	// bond dimension
	int bd_dim = 5;
	// number of sweeps
	int sweep = 10;
	int sw_u2 = 0;
	// initial location for dmrg sweep
	int init_loc = 0;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// tolerance ramping
	int ramp_tol = 10;
	// 2-site update
	bool two_site_update = false;
	// show truncation error
	bool verbose = false;

	std::vector<std::vector<double>> imps;
	double J  = 1.0;
	double Jz = 1.0;

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-H") {
			if (std::string(argv[i+1]) == "xxz") {
				if (argc < i+3) {
					errMsg(argv[0]);
					return 1;
				}
				std::stringstream(argv[i+2]) >> J;
				std::stringstream(argv[i+3]) >> Jz;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-imp") {
			if (argc < i+3) {
				errMsg(argv[0]);
				return 1;
			}
			std::vector<double> imp_params(3);
			std::stringstream(argv[i+1]) >> imp_params[0];	// location index
			std::stringstream(argv[i+2]) >> imp_params[1];	// J_imp
			std::stringstream(argv[i+3]) >> imp_params[2];	// Jz_imp
			imps.push_back(imp_params);
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				wf_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-w option requires the directory of the Wavefunction MPS." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-df option requires a string of Destination Folder's name for final mps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-n") {
			if (i + 1 < argc) {
				net_dir = std::string(argv[i+1]);
			}
			else {
				std::cerr << "-n option requires the directory of Network files." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-b") {
			if (i + 1 < argc)
				hb_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-b option requires the directory of the Boundary Hamiltonians." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l option requires a positive integer system length." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sweep;
			else {
				std::cerr << "-s option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s2") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sw_u2;
			else {
				std::cerr << "-s2 option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iter_max;
			else {
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
		else if ( (std::string(argv[i]) == "-ramp") || (std::string(argv[i]) == "-rmp") ) {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> ramp_tol;
			else {
				std::cerr << "-ramp option requires a positive integer number of tolerance ramping factor." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) {
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else {
				std::cerr << "-intm option requires a positive integer number of sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-u2") {
			two_site_update = true;
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
	}

	/// main function body
	/// build MPO
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	// importMPOQn( len, mpo_dir, mpo_sy, mpo_pl, mpo_mi );

	uni10::CUniTensor Sp = opU1( "Sp" );
	uni10::CUniTensor Sm = opU1( "Sm" );
	uni10::CUniTensor Sz = opU1( "Sz" );
	uni10::CUniTensor id = opU1( "id" );

	uni10::CUniTensor hl   = uni10::CUniTensor( hb_dir + "/HL" );
	uni10::CUniTensor Sp_l = uni10::CUniTensor( hb_dir + "/Sp_L" );
	uni10::CUniTensor Sm_l = uni10::CUniTensor( hb_dir + "/Sm_L" );
	uni10::CUniTensor Sz_l = uni10::CUniTensor( hb_dir + "/Sz_L" );
	uni10::CUniTensor id_l = uni10::CUniTensor( hb_dir + "/Id_L" );

	uni10::CUniTensor hr   = uni10::CUniTensor( hb_dir + "/HR" );
	uni10::CUniTensor Sp_r = uni10::CUniTensor( hb_dir + "/Sp_R" );
	uni10::CUniTensor Sm_r = uni10::CUniTensor( hb_dir + "/Sm_R" );
	uni10::CUniTensor Sz_r = uni10::CUniTensor( hb_dir + "/Sz_R" );
	uni10::CUniTensor id_r = uni10::CUniTensor( hb_dir + "/Id_R" );

	// symmetric blocks
	MPO mpo_ls(5, 'l');
	MPO mpo_ms(5, 'm');
	MPO mpo_rs(5, 'r');

	// raising blocks
	MPO mpo_lp(5, 'l');
	MPO mpo_mp(5, 'm');
	MPO mpo_rp(5, 'r');

	// lowering blocks
	MPO mpo_lm(5, 'l');
	MPO mpo_mm(5, 'm');
	MPO mpo_rm(5, 'r');

	mpo_ls.putTensor(         hl, 0, 0 );
	mpo_lm.putTensor( 0.5*J*Sm_l, 0, 1 );
	mpo_lp.putTensor( 0.5*J*Sp_l, 0, 2 );
	mpo_ls.putTensor(    Jz*Sz_l, 0, 3 );
	mpo_ls.putTensor(       id_l, 0, 4 );

	mpo_ms.putTensor(         id, 0, 0 );
	mpo_mp.putTensor(         Sp, 1, 0 );
	mpo_mm.putTensor(         Sm, 2, 0 );
	mpo_ms.putTensor(         Sz, 3, 0 );
	mpo_mm.putTensor(   0.5*J*Sm, 4, 1 );
	mpo_mp.putTensor(   0.5*J*Sp, 4, 2 );
	mpo_ms.putTensor(      Jz*Sz, 4, 3 );
	mpo_ms.putTensor(         id, 4, 4 );

	mpo_rs.putTensor(       id_r, 0, 0 );
	mpo_rp.putTensor(       Sp_r, 1, 0 );
	mpo_rm.putTensor(       Sm_r, 2, 0 );
	mpo_rs.putTensor(       Sz_r, 3, 0 );
	mpo_rs.putTensor(         hr, 4, 0 );

	uni10::CUniTensor mpo_mts = mpo_ms.launch();
	uni10::CUniTensor mpo_mtp = mpo_mp.launch();
	uni10::CUniTensor mpo_mtm = mpo_mm.launch();
	mpo_sy.push_back(mpo_ls.launch());
	mpo_pl.push_back(mpo_lp.launch());
	mpo_mi.push_back(mpo_lm.launch());
	for (int i = 0; i < len; ++i) {
		mpo_sy.push_back(mpo_mts);
		mpo_pl.push_back(mpo_mtp);
		mpo_mi.push_back(mpo_mtm);
	}
	mpo_sy.push_back(mpo_rs.launch());
	mpo_pl.push_back(mpo_rp.launch());
	mpo_mi.push_back(mpo_rm.launch());

	// impurities
	int num_imps = imps.size();
	if (num_imps > 0) {
		for (int i = 0; i < num_imps; ++i) {
			MPO mpo_is(5, 'm');
			MPO mpo_ip(5, 'm');
			MPO mpo_im(5, 'm');

			mpo_is.putTensor( id, 0, 0 );
			mpo_ip.putTensor( Sp, 1, 0 );
			mpo_im.putTensor( Sm, 2, 0 );
			mpo_is.putTensor( Sz, 3, 0 );
			mpo_im.putTensor( 0.5*imps[i][1]*Sm, 4, 1 );
			mpo_ip.putTensor( 0.5*imps[i][1]*Sp, 4, 2 );
			mpo_is.putTensor( imps[i][2]*Sz, 4, 3 );
			mpo_is.putTensor( id, 4, 4 );
			
			mpo_sy[ (int)imps[i][0]+1 ] = mpo_is.launch();
			mpo_pl[ (int)imps[i][0]+1 ] = mpo_ip.launch();
			mpo_mi[ (int)imps[i][0]+1 ] = mpo_im.launch();
		}
	}

	uni10::CUniTensor hw = op2SiteFromMPOQn( mpo_sy[1], mpo_pl[1], mpo_mi[1] );
	std::vector<uni10::Qnum> phys_dim = hw.bond()[0].Qlist();

	/// initialize IBC Chain
	ChainQnIBC mpsIBC( len, bd_dim, phys_dim );

	if (load_file) {
		size_t found = wf_dir.find("semiinf");
		if (found != std::string::npos)	// if found semiinf in wf_dir
			mpsIBC.import2Semi( wf_dir );
		else
			mpsIBC.importMPS( wf_dir );
	}

	/// perform DMRG
	int s = 0;
	if (save_intm) {
		s = 0;
		while ( s < sweep ) {
			init_loc = (s == 0)? init_loc : 0;
			if (two_site_update)
				mpsIBC.dmrgImpU2( init_loc, -10, mpo_sy, mpo_pl, mpo_mi, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			else {
				if ( s < sweep-sw_u2 )
					mpsIBC.dmrgImp( init_loc, -10, mpo_sy, mpo_pl, mpo_mi, intm, iter_max,
						tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
				else
					mpsIBC.dmrgImpU2( init_loc, -10, mpo_sy, mpo_pl, mpo_mi, intm, iter_max,
						tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			}
			s += intm;
			mpsIBC.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );
		}
	}
	else {
		if (two_site_update)
			mpsIBC.dmrgImpU2( init_loc, -10, mpo_sy, mpo_pl, mpo_mi, sweep, iter_max, tolerance, verbose );
		else
			mpsIBC.dmrgImp( init_loc, -10, mpo_sy, mpo_pl, mpo_mi, sweep, iter_max, tolerance, verbose );
	}

	mpsIBC.exportMPS( mps_dir );
	mpo_sy.clear(); mpo_pl.clear(); mpo_mi.clear();

	return 0;
}
