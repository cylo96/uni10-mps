#include <sstream>
#include <math.h>
#include <mps.hpp>
#define _USE_MATH_DEFINES

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H2 arg1 arg2  load Hamiltonian mpo from folder args" << std::endl;
	std::cerr << "-intf/-imp arg  load INTerFace/IMPurity mpo from folder arg" << std::endl;
	std::cerr << "-w arg  load inf Wavefunction (gamma and lambda tensors) from folder arg" << std::endl;
	std::cerr << "-w2 arg1 arg2  load inf Wavefunction (gamma and lambda tensors) from folder args" << std::endl;
	std::cerr << "-r arg  Resume dmrg update and/or Resize mps from folder arg" << std::endl;
	std::cerr << "-l arg  system Length" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of dmrg Sweeps" << std::endl;
	std::cerr << "-s2 arg  number of 2-site update dmrg Sweeps after 1-site update" << std::endl;
	std::cerr << "-u2  dmrg performs 2-site Update" << std::endl;
	std::cerr << "-iloc arg  Initial Location for dmrg sweep" << std::endl;
	std::cerr << "-bloc arg  Boundary/interface Location" << std::endl;
	std::cerr << "-V  Verbose. show information during dmrg update" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-ramp arg  tolerance RaMPing factor for Lanczos algorithm" << std::endl;
	std::cerr << "-intm arg  save INTerMediate state every arg sweeps" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian mpo directory
	std::string mpo_l_dir = "mpo-ham-xxz";
	std::string mpo_r_dir = "mpo-ham-xxz";
	// interface/impurity mpo
	std::string intf_dir = "";
	// inf wavefunction directory
	std::string wf_dir = "mps-inf";
	std::string wf_l_dir = "mps-inf";
	std::string wf_r_dir = "mps-inf";
	// resume wavefunction directory
	std::string mps_dir = "mps-ibc-xxz";
	// load mps from file?
	bool load_file = true;
	bool load_2file = false;
	bool resume = false;
	// save intermediate state?
	bool save_intm = false;
	int intm = 2;
	// system length
	int len = 100;
	int l0 = -1;
	// initial location for dmrg sweep
	int init_loc = 0;
	// boundary location
	int mid = -1;
	// bond dimension
	int bd_dim = 5;
	// number of sweeps
	int sweep = 10;
	int sw_u2 = 0;
	int s0 = -1;
	// imaginary tebd steps
	int tebd_st = 50;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// tolerance ramping
	int ramp_tol = 10;
	// 2-site update
	bool two_site_update = false;
	// show truncation error
	bool verbose = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H2") {
			if (i + 2 < argc) {
				mpo_l_dir = std::string(argv[i+1]);
				mpo_r_dir = std::string(argv[i+2]);
			}
			else {
				std::cerr << "-H2 option requires two directories of the Hamiltonian MPOs." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intf" || std::string(argv[i]) == "-imp") {
			if (i + 1 < argc) {
				intf_dir = std::string(argv[i+1]);
			}
			else {
				std::cerr << "-intf/-imp option requires the directory to interface/impurity mpo" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				wf_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-w option requires the directory of the Wavefunction MPS." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w2") {
			load_2file = true;
			if (i + 2 < argc &&
				((std::string)argv[i+1]).find("-") != std::string::npos &&
				((std::string)argv[i+2]).find("-") != std::string::npos) {
					wf_l_dir = std::string(argv[i+1]);
					wf_r_dir = std::string(argv[i+2]);
			}
			else {
				std::cerr << "-w2 option requires directories of the Wavefunction MPS." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-r") {
			resume = true;
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-r option requires the directory name of MPS to resume." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l option requires a positive integer system length." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sweep;
			else {
				std::cerr << "-s option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s2") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sw_u2;
			else {
				std::cerr << "-s2 option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l0") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> l0;
			else {
				std::cerr << "-l0 option requires a positive integer sub-system length." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s0") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> s0;
			else {
				std::cerr << "-s0 option requires a positive integer number of iDMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-iloc") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> init_loc;
			else {
				std::cerr << "-iloc option requires a positive integer initial location for DMRG sweep." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-bloc") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> mid;
			else {
				std::cerr << "-bloc <(int) boundary location>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ts") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> tebd_st;
			else {
				std::cerr << "-ts option requires a positive integer number of TEBD steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iter_max;
			else {
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
		else if ( (std::string(argv[i]) == "-ramp") || (std::string(argv[i]) == "-rmp") ) {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> ramp_tol;
			else {
				std::cerr << "-ramp option requires a positive integer number of tolerance ramping factor." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) {
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else {
				std::cerr << "-intm option requires a positive integer number of sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-u2") {
			two_site_update = true;
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
	}

	/// main function body
	/// build MPO
	std::vector<uni10::CUniTensor> mpo_l_sy;
	std::vector<uni10::CUniTensor> mpo_l_pl;
	std::vector<uni10::CUniTensor> mpo_l_mi;
	importMPOQn( len, mpo_l_dir, mpo_l_sy, mpo_l_pl, mpo_l_mi );
	std::vector<uni10::CUniTensor> mpo_r_sy;
	std::vector<uni10::CUniTensor> mpo_r_pl;
	std::vector<uni10::CUniTensor> mpo_r_mi;
	importMPOQn( len, mpo_r_dir, mpo_r_sy, mpo_r_pl, mpo_r_mi );

	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	if (mid < 0) mid = len/2 - 1;
	// mpo is 2-site longer than mps
	for (int i = 0; i <= mid+1; ++i) {
		mpo_sy.push_back( mpo_l_sy[i] );
		mpo_pl.push_back( mpo_l_pl[i] );
		mpo_mi.push_back( mpo_l_mi[i] );
	}
	for (int i = mid+2; i < len+2; ++i) {
		mpo_sy.push_back( mpo_r_sy[i] );
		mpo_pl.push_back( mpo_r_pl[i] );
		mpo_mi.push_back( mpo_r_mi[i] );
	}
	// impurity
	if (intf_dir == "")
		intf_dir = mpo_l_dir;
	mpo_sy[mid+1] = uni10::CUniTensor( intf_dir + "/mpo_imp_s" );
	mpo_pl[mid+1] = uni10::CUniTensor( intf_dir + "/mpo_imp_p" );
	mpo_mi[mid+1] = uni10::CUniTensor( intf_dir + "/mpo_imp_m" );

	mpo_l_sy.clear(); mpo_l_pl.clear(); mpo_l_mi.clear();
	mpo_r_sy.clear(); mpo_r_pl.clear(); mpo_r_mi.clear();

	uni10::CUniTensor hw = op2SiteFromMPOQn( mpo_sy[1], mpo_pl[1], mpo_mi[1] );
	std::vector<uni10::Qnum> phys_dim = hw.bond()[0].Qlist();

	/// initialize IBC Chain
	ChainQnIBC mpsIBC( len, bd_dim, phys_dim );

	if (load_file && !load_2file) {
		size_t found = wf_dir.find("semiinf");
		if (found != std::string::npos)	// if found semiinf in wf_dir
			mpsIBC.import2Semi( wf_dir );
		else
			mpsIBC.importMPS( wf_dir );
	}

	if (load_2file) {
		size_t found_l = wf_l_dir.find("semiinf");
		if (found_l != std::string::npos)
			mpsIBC.import2Semi( wf_l_dir );
		else
			mpsIBC.importMPS( wf_l_dir );

		ChainQnIBC mps_r( len, bd_dim, phys_dim );
		size_t found_r = wf_r_dir.find("semiinf");
		if (found_r != std::string::npos)
			mps_r.import2Semi( wf_r_dir );
		else
			mps_r.importMPS( wf_r_dir );

		for (int i = mid+1; i < len; ++i) {
			mpsIBC.putLambda( mps_r.getLambda(i), i );
			mpsIBC.putGamma( mps_r.getGamma(i), i );
		}
		mpsIBC.putLambda( mps_r.getLambda(len), len );

		std::vector<SchmidtVal> spec = entangleSpecQn( mpsIBC.getLambda(mid+1), 1 );
		std::vector<uni10::Qnum> ql = mpsIBC.getGamma(mid).bond()[2].Qlist();
		std::vector<uni10::Qnum> qr = mpsIBC.getGamma(mid+1).bond()[0].Qlist();
		std::vector<uni10::Bond> bd_lam;
		bd_lam.push_back( uni10::Bond( uni10::BD_IN, ql ) );
		bd_lam.push_back( uni10::Bond( uni10::BD_OUT, qr ) );
		// set initial state uncorrelated at center
		uni10::CUniTensor lam_mid(bd_lam);
		lam_mid.set_zero();
		uni10::CMatrix uni = lam_mid.getBlock( spec[0].qn );
		uni.at(0, 0) = Complex(1.0, 0.0);
		lam_mid.putBlock( spec[0].qn, uni );
		mpsIBC.putLambda( lam_mid, mid+1 );
	}

	if (resume) {
		uni10::CUniTensor lam = mpsIBC.getLambda(0);
		mpsIBC.importMPS( mps_dir );

		int len_init = mpsIBC.getSize();
		std::vector<uni10::Qnum> qnew = lam.bond()[0].Qlist();
		std::vector<uni10::Qnum> q0 = mpsIBC.getGamma(0).bond()[2].Qlist();
		std::vector<uni10::Qnum> qL = mpsIBC.getGamma(len_init-1).bond()[0].Qlist();
		mpsIBC.putLambda( lam, 0 );
		mpsIBC.putLambda( lam, len_init );
		mpsIBC.resizeGamma( 0, qnew, q0 );
		mpsIBC.resizeGamma( len_init-1, qL, qnew );
	}

	/// perform DMRG
	int s = 0;
	if (save_intm) {
		s = 0;
		while ( s < sweep ) {
			init_loc = (s == 0)? init_loc : 0;
			if (two_site_update)
				mpsIBC.dmrgImpU2( init_loc, mid, mpo_sy, mpo_pl, mpo_mi, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			else {
				if ( s < sweep-sw_u2 )
					mpsIBC.dmrgImp( init_loc, mid, mpo_sy, mpo_pl, mpo_mi, intm, iter_max,
						tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
				else
					mpsIBC.dmrgImpU2( init_loc, mid, mpo_sy, mpo_pl, mpo_mi, intm, iter_max,
						tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			}
			s += intm;
			mpsIBC.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );
		}
	}
	else {
		if (two_site_update)
			mpsIBC.dmrgImpU2( init_loc, mid, mpo_sy, mpo_pl, mpo_mi, sweep, iter_max, tolerance, verbose );
		else
			mpsIBC.dmrgImp( init_loc, mid, mpo_sy, mpo_pl, mpo_mi, sweep, iter_max, tolerance, verbose );
	}

	mpsIBC.exportMPS( mps_dir );
	mpo_sy.clear(); mpo_pl.clear(); mpo_mi.clear();

	return 0;
}
