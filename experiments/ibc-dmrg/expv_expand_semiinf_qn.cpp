#include <sstream>
#include <algorithm>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

template<typename T>
class Logspace {
private:
    T curValue, base;

public:
    Logspace(T first, T base) : curValue(first), base(base) {}

    T operator()() {
        T retval = curValue;
        curValue *= base;
        return retval;
    }
};

//======================================

uni10::CUniTensor calcExpV( ChainQnSemiInf& mpsSemiInf, uni10::CUniTensor& O1, int loc,
	const std::vector<uni10::CUniTensor>& lvecs, const std::vector<uni10::CUniTensor>& rvecs ) {
	///
	uni10::CUniTensor expv;
	uni10::CUniTensor lv, rv, ket;
	int L = mpsSemiInf.getSize();
	int obn = O1.bondNum();
	int skip = (obn/2) - 1;

	if ( obn < 2 || obn > 4 ) {
		std::cerr << "In calcExpV : Unsupported operator.\n";
		return expv;
	}
	if ( obn%2 )
		return expv;

	if ( loc == 0 ) {
		lv.assign( mpsSemiInf.getLambda(0).bond() );
		lv.identity();
		rv = rvecs[1+skip];
	}
	else if ( loc == L-1-skip ) {
		lv = lvecs[L-2-skip];
		rv.assign( mpsSemiInf.getLambda(L).bond() );
		rv.identity();
	}
	else {
		lv = lvecs[loc-1];
		rv = rvecs[loc+1+skip];
	}

	if ( skip == 0 )
		ket = netLGL( mpsSemiInf.getLambda(loc), mpsSemiInf.getGamma(loc), mpsSemiInf.getLambda(loc+1) );
	else if ( skip == 1 )
		ket = netLGLGL( mpsSemiInf.getLambda(loc), mpsSemiInf.getGamma(loc),
			mpsSemiInf.getLambda(loc+1), mpsSemiInf.getGamma(loc+1), mpsSemiInf.getLambda(loc+2) );
	lv = buildLRVecQn( lv, ket, O1, true );

	expv = contrLRVecQn( lv, rv );
	return expv;
}

//======================================

void expandExpV(
	ChainQnSemiInf& mpsSemiInf, ChainQnInf& mpsInf, uni10::CUniTensor& O1, std::vector<int>& locs,
	const std::vector<uni10::CUniTensor>& lvecs, const std::vector<uni10::CUniTensor>& rvecs ) {
	///
	uni10::CUniTensor expv, norm;
	uni10::CUniTensor lv, lvo, lvn, rv, ket, ket_o;
	double eval;

	int L = mpsSemiInf.getSize();
	int obn = O1.bondNum();
	int skip = (obn/2) - 1;

	if ( obn < 2 || obn > 4 || obn%2 )
		std::cerr << "In expandExpV : Unsupported operator.\n";

	lv = lvecs[L-1];
	int uc = mpsInf.getSize();
	int last_idx = (L-1)%uc;

	for (int i = 0; i < locs.size(); ++i) {
		int seg = (i == 0)? locs[i]-L+1 : locs[i]-locs[i-1];
		for (int j = 1; j < seg; ++j) {
			ket = netLG( mpsInf.getLambda((last_idx+j)%uc), mpsInf.getGamma((last_idx+j)%uc) );
			lv = buildLRVecQn( lv, ket, true );
		}
		if ( skip == 0 ) {
			ket = netLG(
				mpsInf.getLambda((last_idx+seg)%uc), mpsInf.getGamma((last_idx+seg)%uc) );
			ket_o = netLGL(
				mpsInf.getLambda((last_idx+seg)%uc), mpsInf.getGamma((last_idx+seg)%uc), mpsInf.getLambda((last_idx+seg+1)%uc) );
    }
		else if ( skip == 1 ) {
			ket = netLGLG(
				mpsInf.getLambda((last_idx+seg)%uc), mpsInf.getGamma((last_idx+seg)%uc),
				mpsInf.getLambda((last_idx+seg+1)%uc), mpsInf.getGamma((last_idx+seg+1)%uc) );
			ket_o = netLGLGL(
				mpsInf.getLambda((last_idx+seg)%uc), mpsInf.getGamma((last_idx+seg)%uc),
				mpsInf.getLambda((last_idx+seg+1)%uc), mpsInf.getGamma((last_idx+seg+1)%uc), mpsInf.getLambda((last_idx+seg+2)%uc) );
    }

		lvo = buildLRVecQn( lv, ket_o, O1, true );
		lvn = buildLRVecQn( lv, ket_o, true );
		lv = buildLRVecQn( lv, ket, true );
		last_idx = (last_idx+seg+skip)%uc;

		rv.assign( mpsInf.getLambda((last_idx+1)%uc).bond() );
		rv.identity();
		norm = contrLRVecQn( lvn, rv );
		expv = contrLRVecQn( lvo, rv );

		eval = expv[0].real()/norm[0].real();
		std::cout << locs[i] << '\t' << std::setprecision(12) << eval << '\n';
	}
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 8) {

		std::cerr << "Not Enough Arguments!" << std::endl;
		std::cerr << "Usage: " << argv[0]
			<< " <original_size> <virt_dim> <op_file> <mps_dir> <multiplier> <times> <imps_dir> [options]" << std::endl;
		return 1;
	}

	// initialize semiinf chain
	int L, X, pow_n;
  double base;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;
	std::string opf = std::string(argv[3]);
	std::string wf_dir = std::string( argv[4] );
	std::stringstream(argv[5]) >> base;
	std::istringstream(argv[6]) >> pow_n;
	std::string imps_dir = std::string( argv[7] );

	int uc = 2;
	if (argc > 7)
		std::istringstream(argv[7]) >> uc;

	std::vector<uni10::Qnum> qphys = uni10::CUniTensor( wf_dir + "/gamma_0" ).bond()[1].Qlist();
	ChainQnSemiInf mpsSemiInf(L, X, qphys);
	mpsSemiInf.importMPS( wf_dir );
	ChainQnInf mpsInf( X, qphys );
	mpsInf.importMPS( imps_dir );

	// import operators
	uni10::CUniTensor op(opf);

	std::vector<uni10::Bond> bdi;
	bdi.push_back( uni10::Bond(uni10::BD_IN, qphys) );
	bdi.push_back( uni10::Bond(uni10::BD_OUT, qphys) );
	uni10::CUniTensor id(bdi);
	id.identity();

	const std::vector<uni10::CUniTensor>& gams = mpsSemiInf.getGamVec();
	const std::vector<uni10::CUniTensor>& lams = mpsSemiInf.getLamVec();
	std::vector<uni10::CUniTensor> lvecs = allLRVecsQn( gams, lams, true );
	std::vector<uni10::CUniTensor> rvecs = allLRVecsQn( gams, lams, false );

	double eval = 0.0;
	uni10::CUniTensor expt;
	uni10::CUniTensor norm = calcExpV( mpsSemiInf, id, 0, lvecs, rvecs );

	std::cout << "#r\t<" << opf << "(r)>\n";

	for (int r = 0; r < L; ++r) {
		expt = calcExpV( mpsSemiInf, op, r, lvecs, rvecs );
		eval = expt[0].real()/norm[0].real();
		std::cout << r << '\t' << std::setprecision(12) << eval << '\n';
	}

	std::vector<int> locs;
	std::generate_n( std::back_inserter(locs), pow_n, Logspace<double>(L,base) );
	expandExpV( mpsSemiInf, mpsInf, op, locs, lvecs, rvecs );

	return 0;
}
