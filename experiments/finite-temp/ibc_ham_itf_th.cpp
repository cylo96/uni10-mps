/*******************************************************************

Boundary Hamiltonian generator for Ising model H = Sum(-SzSz + h*Sx)

********************************************************************/

#include <iostream>
#include <uni10.hpp>

#include <mps.hpp>

#include "func_hb_th.hpp"

//============================================

uni10::CUniTensor net2Site1Op (
	uni10::CNetwork net, uni10::CUniTensor A1, uni10::CUniTensor A2, 
	uni10::CUniTensor A1d, uni10::CUniTensor A2d, uni10::CUniTensor op ) {

	net.putTensor( "A1", A1 );
	net.putTensor( "A2", A2 );
	net.putTensor( "op", op );
	net.putTensor( "A1dag", A1d );
	net.putTensor( "A2dag", A2d );

	uni10::CUniTensor T = net.launch();
	return T;
}

//============================================

int main(int argc, char* argv[]) {

	double h = 1.0;

	if (argc > 1)
		std::stringstream(argv[1]) >> h;

	std::vector<uni10::CUniTensor> gamma;
	std::vector<uni10::CUniTensor> lambda;

	std::string wf_dir = "mps-inf";
	gamma.push_back( uni10::CUniTensor(wf_dir + "/gamma_0") );
	gamma.push_back( uni10::CUniTensor(wf_dir + "/gamma_1") );
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_0") );
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_1") );

	uni10::CUniTensor As = tnetLG( lambda[0], gamma[0] );
	uni10::CUniTensor A2s = tnetLG( lambda[1], gamma[1] );

	uni10::CUniTensor Bs = tnetGL( gamma[1], lambda[0] );
	uni10::CUniTensor B2s = tnetGL( gamma[0], lambda[1] );

	uni10::CUniTensor As_dag = As;
	uni10::CUniTensor A2s_dag = A2s;
	As_dag.permute( As_dag.label(), 1 );
	A2s_dag.permute( A2s_dag.label(), 1 );
	As_dag.conj();
	A2s_dag.conj();

	uni10::CUniTensor Bs_dag = Bs;
	uni10::CUniTensor B2s_dag = B2s;
	Bs_dag.permute( Bs_dag.label(), 1 );
	B2s_dag.permute( B2s_dag.label(), 1 );
	Bs_dag.conj();
	B2s_dag.conj();

	// define E3 and E3r
	int D = (lambda[0].bond())[0].dim();

	std::vector<uni10::Bond> bonds;
	bonds.push_back( uni10::Bond( uni10::BD_IN, D) );
	bonds.push_back( uni10::Bond( uni10::BD_OUT, D) );

	uni10::CUniTensor E3(bonds, "E3");
	E3.identity();
	uni10::CUniTensor E3r = E3;

	// define E2 and E2r
	uni10::CUniTensor op("sz");
	uni10::CNetwork net("network_th_left");
	uni10::CUniTensor E2 = net2Site1Op(net, As, A2s, As_dag, A2s_dag, op);
	net = uni10::CNetwork("network_th_right");
	uni10::CUniTensor E2r = net2Site1Op(net, Bs, B2s, Bs_dag, B2s_dag, op);

	// define Ex and Exr
	op = uni10::CUniTensor("sx");
	net = uni10::CNetwork("network_th_left");
	uni10::CUniTensor Ex = net2Site1Op(net, As, A2s, As_dag, A2s_dag, op);
	net = uni10::CNetwork("network_th_right");
	uni10::CUniTensor Exr = net2Site1Op(net, Bs, B2s, Bs_dag, B2s_dag, op);

	// define C
	op = uni10::CUniTensor("ham_itf");
	net = uni10::CNetwork("network_th_ham_odd_left");
	uni10::CUniTensor C1 = net2Site1Op(net, As, A2s, As_dag, A2s_dag, op);

	net = uni10::CNetwork("network_th_ham_even_left");
	net.putTensor( "A1", As );
	net.putTensor( "A2", A2s );
	net.putTensor( "A3", As );
	net.putTensor( "A4", A2s );
	net.putTensor( "op", op );
	net.putTensor( "A1dag", As_dag );
	net.putTensor( "A2dag", A2s_dag );
	net.putTensor( "A3dag", As_dag );
	net.putTensor( "A4dag", A2s_dag );
	uni10::CUniTensor C2 = net.launch();

	uni10::CUniTensor C = C1 + C2;

	// define Cr
	net = uni10::CNetwork("network_th_ham_odd_right");
	uni10::CUniTensor C1r = net2Site1Op(net, Bs, B2s, Bs_dag, B2s_dag, op);

	net = uni10::CNetwork("network_th_ham_even_right");
	net.putTensor( "A1", Bs );
	net.putTensor( "A2", B2s );
	net.putTensor( "A3", Bs );
	net.putTensor( "A4", B2s );
	net.putTensor( "op", op );
	net.putTensor( "A1dag", Bs_dag );
	net.putTensor( "A2dag", B2s_dag );
	net.putTensor( "A3dag", Bs_dag );
	net.putTensor( "A4dag", B2s_dag );
	uni10::CUniTensor C2r = net.launch();

	uni10::CUniTensor Cr = C1r + C2r;

	// calculate e0
	uni10::CNetwork netTrace("trace_rho_lvec");
	netTrace.putTensor( "lam1", lambda[0] );
	netTrace.putTensor( "vec", C );
	netTrace.putTensor( "lam2", lambda[0] );

	uni10::CUniTensor traceRhoC = netTrace.launch();
	double e0 = traceRhoC[0].real();
	std::cout << std::setprecision(10) << e0 << "\n";

	// find HL
	uni10::CUniTensor I2 = E3;
	uni10::CUniTensor rhs_l = C + (-1.0 * e0) * I2;
	rhs_l.permute( rhs_l.bondNum() );

	uni10::CUniTensor HL = findHb( true, tnetAA(As, A2s), tnetAA(As_dag, A2s_dag), lambda[0], rhs_l );
	uni10::CUniTensor tmp_soln = HL;	// save for HR solver's trial func 
	HL.permute(1);
	HL.setName("HL");

	netTrace = uni10::CNetwork("trace_rho_lvec");
	netTrace.putTensor( "lam1", lambda[0] );
	netTrace.putTensor( "vec", HL );
	netTrace.putTensor( "lam2", lambda[0] );
	uni10::CUniTensor traceRhoHL = netTrace.launch();
	double H0 = traceRhoHL[0].real();

	uni10::CUniTensor ground( HL.bond() );
	ground.identity();
	ground *= H0;
	HL = HL + (-1.0) * ground;

	HL.save("HL");

	// calculate e0
	netTrace = uni10::CNetwork("trace_rho_lvec");
	netTrace.putTensor( "lam1", lambda[0] );
	netTrace.putTensor( "vec", Cr );
	netTrace.putTensor( "lam2", lambda[0] );

	traceRhoC = netTrace.launch();
	e0 = traceRhoC[0].real();

	// find HR
	uni10::CUniTensor rhs_r = Cr + (-1.0 * e0) * I2;
	rhs_r.permute( rhs_r.bondNum() );

	uni10::CUniTensor HR = findHb( false, tnetBB(Bs, B2s), tnetBB(Bs_dag, B2s_dag), lambda[0], rhs_r, tmp_soln );
	HR.permute(1);
	HR.setName("HR");

	netTrace = uni10::CNetwork("trace_rho_rvec");
	netTrace.putTensor( "lam1", lambda[0] );
	netTrace.putTensor( "vec", HR );
	netTrace.putTensor( "lam2", lambda[0] );
	uni10::CUniTensor traceRhoHR = netTrace.launch();
	H0 = traceRhoHR[0].real();
	
	ground.assign( HR.bond() );
	ground.identity();
	ground *= H0;
	HR = HR + (-1.0) * ground;

	HR.save("HR");

	// define HLW
	uni10::CUniTensor HLW = 
		(-1.0) * uni10::otimes( E2, uni10::CUniTensor("sz") )
		+ (0.5) * h * uni10::otimes( Ex, uni10::CUniTensor("id") )
		+ (0.5) * h * uni10::otimes( E3, uni10::CUniTensor("sx") );
	HLW.save("HLW");

	// define HWR
	uni10::CUniTensor HWR = 
		(-1.0) * uni10::otimes( uni10::CUniTensor("sz"), E2r )
		+ (0.5) * h * uni10::otimes( uni10::CUniTensor("id"), Exr )
		+ (0.5) * h * uni10::otimes( uni10::CUniTensor("sx"), E3r );
	HWR.save("HWR");

	// output effective boundary one-site operators
	Ex.save("Sx_L");
	E2.save("Sz_L");
	E3.save("Id_L");
	Exr.save("Sx_R");
	E2r.save("Sz_R");
	E3r.save("Id_R");

	return 0;
}

