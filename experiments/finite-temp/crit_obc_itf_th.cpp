#include <sstream>
#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

int main(int argc, char* argv[]) {

	// import H
	uni10::CUniTensor hw = uni10::CUniTensor("ham_itf");

	// import operators
	uni10::CUniTensor sx = uni10::CUniTensor("sx");
	uni10::CUniTensor sz = uni10::CUniTensor("sz");
	uni10::CUniTensor id = uni10::CUniTensor("id");

	double h = 1.0;
	double A = 1000;
	double M0 = 1.0;

	if ( argc > 4 ) {
		if ( argc > 5 ) {
			std::stringstream(argv[3]) >> h;
			std::stringstream(argv[4]) >> A;
			std::stringstream(argv[5]) >> M0;
		}
		else {
			std::stringstream(argv[3]) >> A;
			std::stringstream(argv[4]) >> M0;
		}
	}

	uni10::CUniTensor hlw = 
		(-1.0) * uni10::otimes(sz, sz) + h * uni10::otimes(sx, id) + 0.5 * h * uni10::otimes(id, sx)
		+ (-1.0) * A * ( M0 * uni10::otimes(sz, id) + sqrt(1.0 - M0*M0) * uni10::otimes(sx, id) );

	uni10::CUniTensor hwr = 
		(-1.0) * uni10::otimes(sz, sz) + 0.5 * h * uni10::otimes(sx, id) + h * uni10::otimes(id, sx)
		+ (-1.0) * A * ( 0.0 * uni10::otimes(id, sz) + sqrt(1.0 - 0.0) * uni10::otimes(id, sx) );

	// initialize itf chain
	int d = hw.bond()[0].dim();
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;

	double norm, expv, ent;
	Complex dT (0.0, 0.01);

	ChainThOBC itfChain(L, d, X);
	itfChain.importMPS( "mps-inf" );

	for (int i = 0; i < 501; ++i) {

		if (i%10 == 0) {
			norm = itfChain.expVal(id, 0)[0].real();
			std::cout << "#D\t|<Sz(D)>|\tentropy(D|D+1)\tt=" << i*0.01*1 << "\n";

			for (int D = 0; D < L; ++D) {
				expv = itfChain.expVal( sz, D )[0].real()/norm;
				ent = entanglementEntropy( itfChain.getLambda(D+1) );
				std::cout << D << "\t" << std::setprecision(10) << expv << "\t" << ent << "\n";
			}
		}

		itfChain.tebd( hw, hlw, hwr, dT, 1, 1 );
	}

	itfChain.exportMPS( "mps-obc-itf" );
	return 0;
}

