#include <uni10.hpp>
#include <tns-func/tns_const.h>

void ibcIsingFT( double h, std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda );

