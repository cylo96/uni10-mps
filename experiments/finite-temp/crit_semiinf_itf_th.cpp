#include <sstream>
#include <mps.hpp>
#include "func_ibc_itf_th.hpp"

#define _USE_MATH_DEFINES

//======================================

int main(int argc, char* argv[]) {

	// import H
	uni10::CUniTensor hw = uni10::CUniTensor("ham_itf");
	uni10::CUniTensor hwr = uni10::CUniTensor("HWR");
	uni10::CUniTensor hr = uni10::CUniTensor("HR");

	// import operators
	uni10::CUniTensor sx = uni10::CUniTensor("sx");
	uni10::CUniTensor sz = uni10::CUniTensor("sz");
	uni10::CUniTensor id = uni10::CUniTensor("id");

	double h = 1.0;
	double A = 1000;
	double M0 = 1.0;

	if ( argc > 4 ) {
		if ( argc > 5 ) {
			std::stringstream(argv[3]) >> h;
			std::stringstream(argv[4]) >> A;
			std::stringstream(argv[5]) >> M0;
		}
		else {
			std::stringstream(argv[3]) >> A;
			std::stringstream(argv[4]) >> M0;
		}
	}

	uni10::CUniTensor hlw = 
		(-1.0) * uni10::otimes(sz, sz) + h * uni10::otimes(sx, id) + 0.5 * h * uni10::otimes(id, sx)
		+ (-1.0) * A * ( M0 * uni10::otimes(sz, id) + sqrt(1.0 - M0*M0) * uni10::otimes(sx, id) );


	// initialize itf chain
	int d = hw.bond()[0].dim();
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;

	double norm, expv, ent;
	Complex dT (0.0, 0.01);

	ChainThSemiInf itfChain(L, d, X);
	itfChain.importMPS( "mps-inf" );

	ChainThInf iMPS(d, X);
	iMPS.importMPS( "mps-inf" );
	std::vector<uni10::CUniTensor> gam;
	std::vector<uni10::CUniTensor> lam;
	gam.push_back( iMPS.getGamma(0) );
	gam.push_back( iMPS.getGamma(1) );
	lam.push_back( iMPS.getLambda(0) );
	lam.push_back( iMPS.getLambda(1) );

	for (int i = 0; i < 501; ++i) {

		iMPS.itebd( hw, dT, 1, 1 );
		gam[0] = iMPS.getGamma(0);
		gam[1] = iMPS.getGamma(1);
		lam[0] = iMPS.getLambda(0);
		lam[1] = iMPS.getLambda(1);
		ibcIsingFT( h, gam, lam );
		hwr = uni10::CUniTensor("HWR");
		hr = uni10::CUniTensor("HR");

		if (i%10 == 0) {
			norm = itfChain.expVal(id, 0)[0].real();
			std::cout << "#D\t|<Sz(D)>|\tentropy(D|D+1)\tt=" << i*0.01*1 << "\n";

			for (int D = 0; D < L; ++D) {
				expv = itfChain.expVal( sz, D )[0].real()/norm;
				ent = entanglementEntropy( itfChain.getLambda(D+1) );
				std::cout << D << "\t" << std::setprecision(10) << expv << "\t" << ent << "\n";
			}
		}

		itfChain.putLambda( lam[0], L );
		itfChain.tebd( hlw, hw, hwr, hr, dT, 1, 1 );
	}
/*
	uni10::CUniTensor Ur( hr.bond() );
	Ur = texpBO2( I*dT, hr );
	uni10::CUniTensor Ud = Ur;
	Ud.transpose();
	int lab_u[] = {200, 100};
	int lab_ud[] = {300, 200};
	Ur.setLabel( lab_u );
	Ud.setLabel( lab_ud );
	std::cout << Ud*Ur;
*/
/*
	uni10::CUniTensor Uwr( hwr.bond() );
	Uwr = texpBO2( I*dT, hwr );
	uni10::CUniTensor Udag = Uwr;
	Udag.transpose();
	int lab_u[] = {200, 201, 100, 101};
	int lab_ud[] = {300, 301, 200, 201};
	Uwr.setLabel( lab_u );
	Udag.setLabel( lab_ud );
	std::cout << Udag*Uwr;
*/
/*
	//uni10::CUniTensor ket = tnetGL( itfChain.getGamma(L-1), itfChain.getLambda(L) );
	uni10::CUniTensor ket = tnetGL( iMPS.getGamma(1), iMPS.getLambda(0) );
	uni10::CUniTensor bra = ket;
	int lab_k[] = {0, 100, -100, 1};
	int lab_b[] = {2, 100, -100, 1};
	ket.setLabel( lab_k );
	bra.setLabel( lab_b );
	//std::cout << bra << ket;
	std::cout << bra*ket;
*/
	itfChain.exportMPS( "mps-semiinf-itf" );
//	iMPS.exportMPS( "mps-inf" );
	return 0;
}

