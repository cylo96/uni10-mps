#include <sstream>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

uni10::CUniTensor calcExpV( ChainQnIBC& mpsIBC, uni10::CUniTensor& O1, int loc,
	const std::vector<uni10::CUniTensor>& lvecs, const std::vector<uni10::CUniTensor>& rvecs ) {
	///
	uni10::CUniTensor expv;
	uni10::CUniTensor lv, rv, ket;
	int L = mpsIBC.getSize();
	int obn = O1.bondNum();
	int skip = (obn/2) - 1;

	if ( obn < 2 || obn > 4 ) {
		std::cerr << "In calcExpV : Unsupported operator.\n";
		return expv;
	}
	if ( obn%2 )
		return expv;

	if ( loc == 0 ) {
		lv.assign( mpsIBC.getLambda(0).bond() );
		lv.identity();
		rv = rvecs[1+skip];
	}
	else if ( loc == L-1-skip ) {
		lv = lvecs[L-2-skip];
		rv.assign( mpsIBC.getLambda(L).bond() );
		rv.identity();
	}
	else {
		lv = lvecs[loc-1];
		rv = rvecs[loc+1+skip];
	}

	if ( skip == 0 )
		ket = netLGL( mpsIBC.getLambda(loc), mpsIBC.getGamma(loc), mpsIBC.getLambda(loc+1) );
	else if ( skip == 1 )
		ket = netLGLGL( mpsIBC.getLambda(loc), mpsIBC.getGamma(loc),
			mpsIBC.getLambda(loc+1), mpsIBC.getGamma(loc+1), mpsIBC.getLambda(loc+2) );
	lv = buildLRVecQn( lv, ket, O1, true );

	expv = contrLRVecQn( lv, rv );
	return expv;
}

//======================================

uni10::CUniTensor calcCorr( ChainQnIBC& mpsIBC,
	uni10::CUniTensor& O1, uni10::CUniTensor& O2, int loc1, int loc2,
	const std::vector<uni10::CUniTensor>& lvecs, const std::vector<uni10::CUniTensor>& rvecs ) {
	///
	uni10::CUniTensor corr;
	uni10::CUniTensor lv, rv, ket;
	int L = mpsIBC.getSize();
	int obn = O1.bondNum();
	int skip = (obn/2) - 1;

	if ( obn != O2.bondNum() ) {
		std::cerr << "In calcCorr : Bonds of two operators do not match.\n";
		return corr;
	}
	if ( obn < 2 || obn > 4 ) {
		std::cerr << "In calcCorr : Unsupported operator.\n";
		return corr;
	}

	// build head
	if (loc1 == 0) {
		lv.assign( mpsIBC.getLambda(0).bond() );
		lv.identity();
	}
	else
		lv = lvecs[loc1-1];

	// build tail
	if (loc2 == L-1-skip) {
		rv.assign( mpsIBC.getLambda(L).bond() );
		rv.identity();
	}
	else
		rv = rvecs[loc2+1+skip];

	// contract O1
	if ( skip == 0 )
		ket = netLG( mpsIBC.getLambda(loc1), mpsIBC.getGamma(loc1) );
	else if ( skip == 1 )
		ket = netLGLG( mpsIBC.getLambda(loc1), mpsIBC.getGamma(loc1),
			mpsIBC.getLambda(loc1+1), mpsIBC.getGamma(loc1+1) );
	lv = buildLRVecQn( lv, ket, O1, true );

	// contract body
	for (int i = loc1+1+skip; i < loc2; ++i) {
		ket = netLG( mpsIBC.getLambda(i), mpsIBC.getGamma(i) );
		lv = buildLRVecQn( lv, ket, true );
	}

	// contract O2
	if ( skip == 0 )
		ket = netLGL( mpsIBC.getLambda(loc2), mpsIBC.getGamma(loc2), mpsIBC.getLambda(loc2+1) );
	else if ( skip == 1 )
		ket = netLGLGL( mpsIBC.getLambda(loc2), mpsIBC.getGamma(loc2),
			mpsIBC.getLambda(loc2+1), mpsIBC.getGamma(loc2+1), mpsIBC.getLambda(loc2+2) );
	lv = buildLRVecQn( lv, ket, O2, true );

	corr = contrLRVecQn( lv, rv );
	return corr;
}

//======================================

void corrOO( ChainQnIBC& mpsIBC, int L,
	uni10::CUniTensor& O1, uni10::CUniTensor& O2, uni10::CUniTensor& id,
	int loc1_i, int loc1_f, int loc2_i, int loc2_f, bool r_from_mid ) {
	/// calculate < O1(r1) O2(r2) > - < O(r1) > < O(r2) >
	
	const std::vector<uni10::CUniTensor>& gams = mpsIBC.getGamVec();
	const std::vector<uni10::CUniTensor>& lams = mpsIBC.getLamVec();
	std::vector<uni10::CUniTensor> lvecs = allLRVecsQn( gams, lams, true );
	std::vector<uni10::CUniTensor> rvecs = allLRVecsQn( gams, lams, false );

	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expl, expr, corT;
	uni10::CUniTensor norm = calcExpV( mpsIBC, id, 0, lvecs, rvecs );

	int mid = L/2 - 1;
	int r1, r2;
	int ibn1 = O1.inBondNum();
	int ibn2 = O2.inBondNum();

	if (loc1_i < 0 || loc1_i >= L) loc1_i = 0;
	if (loc1_f < 0 || loc1_f >= L) loc1_f = (r_from_mid)? mid-ibn1+1 : L-ibn1;
	if (loc2_i < 0 || loc2_i >= L) loc2_i = (r_from_mid)? mid+1 : 0;
	if (loc2_f < 0 || loc2_f >= L) loc2_f = L-ibn2;
	
	for (int i = loc1_i; i <= loc1_f; ++i) {
		for (int j = loc2_i; j <= loc2_f; ++j) {
			if (i >= j) break;
			
			expl = calcExpV( mpsIBC, O1, i, lvecs, rvecs );
			expr = calcExpV( mpsIBC, O2, j, lvecs, rvecs );
			corT = calcCorr( mpsIBC, O1, O2, i, j, lvecs, rvecs );

			exp1 = expl[0].real()/norm[0].real();
			exp2 = expr[0].real()/norm[0].real();
			base = ( exp1 * exp2 );
			corr = ( corT[0].real()/norm[0].real() );

			if (r_from_mid) {
				r1 = mid-i;
				r2 = j-mid;
			}
			else {
				r1 = i;
				r2 = j;
			}
			std::cout << std::fixed << r1 << '\t' << r2 << '\t'
				<< std::scientific << std::setprecision(14)
				<< corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\n';
		}
	}
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 6) {

		std::cerr << "Not Enough Arguments!" << std::endl;
		std::cerr << "Usage: " << argv[0]
			<< " <lat_size> <virt_dim> <op1_file> <op2_file> <mps_dir> [<loc1_init> <loc1_fin> <loc2_init> <loc2_fin>] [options]" << std::endl;
		std::cerr << "Allowed options:" << std::endl;
		std::cerr << "-mid   distances calculated from mid" << std::endl;
		return 1;
	}

	// initialize IBC chain
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;

	std::string op1f, op2f;
	op1f = std::string(argv[3]);
	op2f = std::string(argv[4]);

	std::string wf_dir = "mps-inf";
	wf_dir = std::string( argv[5] );

	int mid = L/2 - 1;
	int loc1_i = mid, loc1_f = mid, loc2_i = mid+1, loc2_f = mid+1;
	if (argc > 9) {
		std::istringstream(argv[6]) >> loc1_i;
		std::istringstream(argv[7]) >> loc1_f;
		std::istringstream(argv[8]) >> loc2_i;
		std::istringstream(argv[9]) >> loc2_f;
	}
	
	bool r_from_mid = false;
	if (argc > 10 && std::string(argv[10]) == "-mid") {
		r_from_mid = true;
	}

	std::vector<uni10::Qnum> qphys = uni10::CUniTensor( wf_dir + "/gamma_0" ).bond()[1].Qlist();
	ChainQnIBC mpsIBC(L, X, qphys);
	mpsIBC.importMPS( wf_dir );

	// import operators
	uni10::CUniTensor op1 = uni10::CUniTensor(op1f);
	uni10::CUniTensor op2 = uni10::CUniTensor(op2f);

	std::vector<uni10::Bond> bdi;
	bdi.push_back( uni10::Bond(uni10::BD_IN, qphys) );
	bdi.push_back( uni10::Bond(uni10::BD_OUT, qphys) );
	uni10::CUniTensor id(bdi);
	id.identity();

	std::cout << "#r1\t" << "r2\t<" 
		<< op1f << "(r1) " << op2f << "(r2)>\t<" 
		<< op1f << "(r1) " << op2f << "(r2)> - <" << op1f << "(r1)> <" << op2f << "(r2)>\t<" 
		<< op1f << "(r1)>\t<" << op2f << "(r2)>\n";

	corrOO( mpsIBC, L, op1, op2, id, loc1_i, loc1_f, loc2_i, loc2_f, r_from_mid );

	return 0;
}
