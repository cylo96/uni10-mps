#include <iostream>
#include <sstream>
#include <uni10.hpp>

#include <mps-qn/func_qn_meas.h>

//================================

void errMsg( char* arg ) {
	///
	std::cerr << "Usage: " << arg << " <mps_folder> <site_idx> <num_vals> [options]" << std::endl;
	std::cerr << "Available options:" << std::endl;
	std::cerr << "-V  Verbose. show Qnum information" << std::endl;
}

//================================

int main( int argc, char* argv[] ){

	if (argc < 3) {

		errMsg( argv[0] );
		return 1;
	}

	int idx = 0, Nsv = 4;
	bool verbose = false;
	std::string wf_dir = "mps-inf";

	wf_dir = std::string(argv[1]);
	std::istringstream(argv[2]) >> idx;
	if (argc > 3)
		std::istringstream(argv[3]) >> Nsv;
	if (argc > 4 && std::string(argv[4]) == "-V")
		verbose = true;

	// load lambda tensor
	uni10::CUniTensor lam(wf_dir + "/lambda_" + std::to_string((long long) idx));

	Nsv = std::min( lam.bond()[0].dim(), Nsv );
	std::vector< SchmidtVal > spec = entangleSpecQn( lam, Nsv );

	if (verbose) {
		for (int i = 0; i < Nsv; ++i)
			std::cout << std::scientific << std::setprecision(14) << spec[i].val << '\t' << spec[i].qn << '\n';
	}
	else {
		for (int i = 0; i < Nsv; ++i)
			std::cout << std::scientific << std::setprecision(14) << spec[i].val << '\n';
	}

	return 0;
}
