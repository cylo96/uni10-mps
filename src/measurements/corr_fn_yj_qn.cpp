#include <sstream>
#include <algorithm>
#include <mps.hpp>

//======================================

uni10::CUniTensor calcExpV( YJuncQnIBC mpsYJ,
	std::vector< std::vector< std::vector<uni10::CUniTensor> > >& all_vecs,
	uni10::CUniTensor& O1, std::string loc ) {
	///
	uni10::CUniTensor exp_val;
	uni10::CUniTensor ket, bra;
	std::vector<int> lab_op, lab_bra, lab_ket;
	uni10::CUniTensor w1_lvec, w2_rvec, w3_rvec;

	int L = mpsYJ.getSize();
	int bno = O1.bondNum();
	int lop = (bno/2); // length of operator
	if ( bno != 2 && bno != 4 ) {
		std::cerr << "In calcExpV : Unsupported operator." << '\n';
		return exp_val;
	}

	if (loc == "WJ1" || loc == "WJ2" || loc == "WJ3") {
		if (lop != 2) {
			std::cerr << "Location " << loc << " only for 2-site operators." << '\n';
			return exp_val;
		}
		if (loc == "WJ1") {
			ket = mpsYJ.netThetaTrngl_WJT("WJ1", O1);
			bra = dag( mpsYJ.netThetaTrngl_WJT("WJ1") );
			lab_ket = {1, 100, 101, 102, 103, -2, -3};
			lab_bra = {2, 3, -1, 100, 101, 102, 103};
			ket.setLabel(lab_ket);
			bra.setLabel(lab_bra);

			w1_lvec = all_vecs[0][0][L-3];
			w2_rvec = all_vecs[1][1][1];
			w3_rvec = all_vecs[2][1][1];
		}
		else if (loc == "WJ2") {
			ket = mpsYJ.netThetaTrngl_WJT("WJ2", O1);
			bra = dag( mpsYJ.netThetaTrngl_WJT("WJ2") );
			int lab_ket[] = {1, -3, 101, 102, 103, 104, -2};
			int lab_bra[] = {104, 2, -1, 3, 101, 102, 103};
			ket.setLabel(lab_ket);
			bra.setLabel(lab_bra);

			w1_lvec = all_vecs[0][0][L-2];
			w2_rvec = all_vecs[1][1][2];
			w3_rvec = all_vecs[2][1][1];
		}
		else if (loc == "WJ3") {
			ket = mpsYJ.netThetaTrngl_WJT("WJ3", O1);
			bra = dag( mpsYJ.netThetaTrngl_WJT("WJ3") );
			int lab_ket[] = {1, -2, 101, 102, 103, 105, -3};
			int lab_bra[] = {105, 3, -1, 2, 101, 102, 103};
			ket.setLabel(lab_ket);
			bra.setLabel(lab_bra);

			w1_lvec = all_vecs[0][0][L-2];
			w2_rvec = all_vecs[1][1][1];
			w3_rvec = all_vecs[2][1][2];
		}
	}
	else {
		if (lop == 1) {
			if (loc == "J1" || loc == "1") {
				ket = mpsYJ.netThetaTrngl("1", O1);
				bra = dag( mpsYJ.netThetaTrngl() );
			}
			else if (loc == "J2" || loc == "2") {
				ket = mpsYJ.netThetaTrngl("2", O1);
				bra = dag( mpsYJ.netThetaTrngl() );
			}
			else if (loc == "J3" || loc == "3") {
				ket = mpsYJ.netThetaTrngl("3", O1);
				bra = dag( mpsYJ.netThetaTrngl() );
			}
			else {
				std::cerr << "In calcExpV : Unsupported location + operator configuration." << '\n';
				return exp_val;
			}
		}
		else if (lop == 2) {
			if (loc == "JJ12" || loc == "12") {
				ket = mpsYJ.netThetaTrngl("12", O1);
				bra = dag( mpsYJ.netThetaTrngl() );
			}
			else if (loc == "JJ23" || loc == "23") {
				ket = mpsYJ.netThetaTrngl("23", O1);
				bra = dag( mpsYJ.netThetaTrngl() );
			}
			else if (loc == "JJ13" || loc == "13") {
				ket = mpsYJ.netThetaTrngl("13", O1);
				bra = dag( mpsYJ.netThetaTrngl() );
			}
			else {
				std::cerr << "In calcExpV : Unsupported location + operator configuration." << '\n';
				return exp_val;
			}
		}
		int lab_ket[] = {1, 101, 102, 103, -2, -3};
		int lab_bra[] = {2, 3, -1, 101, 102, 103};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);

		w1_lvec = all_vecs[0][0][L-2];
		w2_rvec = all_vecs[1][1][1];
		w3_rvec = all_vecs[2][1][1];
	}

	int lab_w1l[] = {-1, 1};
	int lab_w2r[] = {-2, 2};
	int lab_w3r[] = {-3, 3};
	w1_lvec.setLabel(lab_w1l);
	w2_rvec.setLabel(lab_w2r);
	w3_rvec.setLabel(lab_w3r);
	uni10::CUniTensor rvec = uni10::contract(ket, w2_rvec);
	rvec = uni10::contract(rvec, w3_rvec);
	rvec = uni10::contract(rvec, bra);
	exp_val = contrLRVecQn( w1_lvec, rvec );

	return exp_val;
}

//======================================

uni10::CUniTensor calcExpV( YJuncQnIBC mpsYJ,
	std::vector< std::vector< std::vector<uni10::CUniTensor> > >& all_vecs,
	uni10::CUniTensor& O1, int w1, int loc ) {
	///
	uni10::CUniTensor expv;
	uni10::CUniTensor lv, rv, ket;
	int L = mpsYJ.getSize();
	int bno = O1.bondNum();
	int skip = (bno/2) - 1;

	if ( bno < 2 || bno > 4 ) {
		std::cerr << "In calcExpV : Unsupported operator.\n";
		return expv;
	}
	if ( bno%2 )
		return expv;

	if (w1 == 1) {
		if (loc == L-1-skip) {
			if (bno == 2)
				return calcExpV(mpsYJ, all_vecs, O1, "1");
			else if (bno == 4)
				return calcExpV(mpsYJ, all_vecs, O1, "WJ1");
		}
		else if (loc == 0) {
			lv.assign( mpsYJ.getLambda(1, 0).bond() );
			lv.identity();
			rv = all_vecs[0][1][1+skip];
		}
		else {
			lv = all_vecs[0][0][loc-1];
			rv = all_vecs[0][1][loc+1+skip];
		}
	}
	else if (w1 == 2) {
		if (loc == 0) {
			if (bno == 2)
				return calcExpV(mpsYJ, all_vecs, O1, "2");
			else if (bno == 4)
				return calcExpV(mpsYJ, all_vecs, O1, "WJ2");
		}
		else if (loc == L-1-skip) {
			lv = all_vecs[1][0][L-2-skip];
			rv.assign( mpsYJ.getLambda(2, L).bond() );
			rv.identity();
		}
		else {
			lv = all_vecs[1][0][loc-1];
			rv = all_vecs[1][1][loc+1+skip];
		}
	}
	else if (w1 == 3) {
		if (loc == 0) {
			if (bno == 2)
				return calcExpV(mpsYJ, all_vecs, O1, "3");
			else if (bno == 4)
				return calcExpV(mpsYJ, all_vecs, O1, "WJ3");
		}
		else if (loc == L-1-skip) {
			lv = all_vecs[2][0][L-2-skip];
			rv.assign( mpsYJ.getLambda(3, L).bond() );
			rv.identity();
		}
		else {
			lv = all_vecs[2][0][loc-1];
			rv = all_vecs[2][1][loc+1+skip];
		}
	}

	if ( skip == 0 )
		ket = netLGL( mpsYJ.getLambda(w1, loc), mpsYJ.getGamma(w1, loc), mpsYJ.getLambda(w1, loc+1) );
	else if ( skip == 1 )
		ket = netLGLGL( mpsYJ.getLambda(w1, loc), mpsYJ.getGamma(w1, loc),
			mpsYJ.getLambda(w1, loc+1), mpsYJ.getGamma(w1, loc+1), mpsYJ.getLambda(w1, loc+2) );
	lv = buildLRVecQn( lv, ket, O1, true );

	expv = contrLRVecQn( lv, rv );
	return expv;
}

//======================================

uni10::CUniTensor calcCorr( YJuncQnIBC mpsYJ,
	std::vector< std::vector< std::vector<uni10::CUniTensor> > >& all_vecs,
	uni10::CUniTensor& O1, uni10::CUniTensor& O2,
	std::string loc1, std::string loc2 ) {
	///
	uni10::CUniTensor corr;
	uni10::CUniTensor ket, bra;
	uni10::CUniTensor w1_lvec, w2_rvec, w3_rvec;

	int L = mpsYJ.getSize();
	int bno = O1.bondNum();
	int lop = (bno/2);

	if (lop == 1) {
		uni10::CUniTensor op = (bno == 3 && O1.bond()[2].dim() == 1)?
			otimesPM(O1, O2) : uni10::otimes(O1, O2);	// need a more general otimesPM function

		if ((loc1 == "J1" || loc1 == "1") && (loc2 == "J2" || loc2 == "2"))
			return calcExpV(mpsYJ, all_vecs, op, "12");
		else if ((loc1 == "J2" || loc1 == "2") && (loc2 == "J3" || loc2 == "3"))
			return calcExpV(mpsYJ, all_vecs, op, "23");
		else if ((loc1 == "J1" || loc1 == "1") && (loc2 == "J3" || loc2 == "3"))
			return calcExpV(mpsYJ, all_vecs, op, "13");
		else {
			std::cerr << "In calcCorr : Unsupported location + operator configuration.\n";
			return corr;
		}
	}

	if (loc1 == "WJ1" && loc2 == "WJ2") {
		ket = mpsYJ.netThetaTrngl_WJTW("12", O1, O2);
		bra = dag( mpsYJ.netThetaTrngl_WJTW("12") );
		int lab_ket[] = {1, 100, 101, 102, 103, 104, -2, -3};
		int lab_bra[] = {2, 3, -1, 100, 101, 102, 103, 104};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);

		w1_lvec = all_vecs[0][0][L-3];
		w2_rvec = all_vecs[1][1][2];
		w3_rvec = all_vecs[2][1][1];
	}
	else if (loc1 == "WJ2" && loc2 == "WJ3") {
		ket = mpsYJ.netThetaTrngl_WJTW("23", O1, O2);
		bra = dag( mpsYJ.netThetaTrngl_WJTW("23") );
		int lab_ket[] = {1, 101, 102, 103, 104, 105, -2, -3};
		int lab_bra[] = {2, 3, -1, 101, 102, 103, 104, 105};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);

		w1_lvec = all_vecs[0][0][L-2];
		w2_rvec = all_vecs[1][1][2];
		w3_rvec = all_vecs[2][1][2];
	}
	else if (loc1 == "WJ1" && loc2 == "WJ3") {
		ket = mpsYJ.netThetaTrngl_WJTW("13", O1, O2);
		bra = dag( mpsYJ.netThetaTrngl_WJTW("13") );
		int lab_ket[] = {1, 100, 101, 102, 103, 105, -2, -3};
		int lab_bra[] = {2, 3, -1, 100, 101, 102, 103, 105};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);

		w1_lvec = all_vecs[0][0][L-3];
		w2_rvec = all_vecs[1][1][1];
		w3_rvec = all_vecs[2][1][2];
	}
	else {
		std::cerr << "In calcCorr : Unsupported location + operator configuration.\n";
		return corr;
	}

	int lab_w1l[] = {-1, 1};
	int lab_w2r[] = {-2, 2};
	int lab_w3r[] = {-3, 3};
	w1_lvec.setLabel(lab_w1l);
	w2_rvec.setLabel(lab_w2r);
	w3_rvec.setLabel(lab_w3r);
	uni10::CUniTensor rvec = uni10::contract(ket, w2_rvec);
	rvec = uni10::contract(rvec, w3_rvec);
	rvec = uni10::contract(rvec, bra);
	corr = contrLRVecQn( w1_lvec, rvec );

	return corr;
}

//======================================

uni10::CUniTensor calcCorr( YJuncQnIBC mpsYJ,
	std::vector< std::vector< std::vector<uni10::CUniTensor> > >& all_vecs,
	uni10::CUniTensor& O1, uni10::CUniTensor& O2,
	int w1, int loc1, int loc2 ) {
	///
	uni10::CUniTensor corr;
	uni10::CUniTensor ket, bra;
	uni10::CUniTensor lvec, rvec;

	int L = mpsYJ.getSize();
	int bno = O1.bondNum();
	int lop = (bno/2);
	int skip = lop - 1;

	if (loc1 > loc2) {
		std::swap(loc1, loc2);
		std::swap(O1, O2);
	}

	if (w1 == 1) {
		// contract up to op1
		if (loc1 == 0) {
			lvec.assign( mpsYJ.getLambda(1, 0).bond() );
			lvec.identity();
		}
		else
			lvec = all_vecs[0][0][loc1-1];
		ket = (lop == 1)? netLG( mpsYJ.getLambda(1, loc1), mpsYJ.getGamma(1, loc1) )
		: netLGLG( mpsYJ.getLambda(1, loc1), mpsYJ.getGamma(1, loc1), mpsYJ.getLambda(1, loc1+1), mpsYJ.getGamma(1, loc1+1) );
		lvec = buildLRVecQn( lvec, ket, O1, true );
		// contract up to op2
		for (int i = loc1+lop; i < loc2; ++i) {
			ket = netLG( mpsYJ.getLambda(1, i), mpsYJ.getGamma(1, i) );
			lvec = buildLRVecQn( lvec, ket, true );
		}
		ket = (lop == 1)? netLGL( mpsYJ.getLambda(1, loc2), mpsYJ.getGamma(1, loc2), mpsYJ.getLambda(1, loc2+1) )
		: netLGLGL( mpsYJ.getLambda(1, loc2), mpsYJ.getGamma(1, loc2),
			mpsYJ.getLambda(1, loc2+1), mpsYJ.getGamma(1, loc2+1), mpsYJ.getLambda(1, loc2+2) );
		lvec = buildLRVecQn( lvec, ket, O2, true );

		rvec = all_vecs[0][1][loc2+lop];
	}
	else if (w1 == 2) {
		// contract up to op2
		if (loc2 == L-1-skip) {
			rvec.assign( mpsYJ.getLambda(2, L).bond() );
			rvec.identity();
		}
		else
			rvec = all_vecs[1][1][loc2+1+skip];
		ket = (lop == 1)? netGL( mpsYJ.getGamma(2, loc2), mpsYJ.getLambda(2, loc2+1) )
		: netGLGL( mpsYJ.getGamma(2, loc2), mpsYJ.getLambda(2, loc2+1), mpsYJ.getGamma(2, loc2+1), mpsYJ.getLambda(2, loc2+2) );
		rvec = buildLRVecQn( rvec, ket, O2, false );
		// contract up to op1
		for (int i = loc2-1; i >= loc1+lop; --i) {
			ket = netGL( mpsYJ.getGamma(2, i), mpsYJ.getLambda(2, i+1) );
			rvec = buildLRVecQn( rvec, ket, false );
		}
		ket = (lop == 1)? netLGL( mpsYJ.getLambda(2, loc1), mpsYJ.getGamma(2, loc1), mpsYJ.getLambda(2, loc1+1) )
		: netLGLGL( mpsYJ.getLambda(2, loc1-1), mpsYJ.getGamma(2, loc1),
			mpsYJ.getLambda(2, loc1+1), mpsYJ.getGamma(2, loc1+1), mpsYJ.getLambda(2, loc1+2) );
		rvec = buildLRVecQn( rvec, ket, O1, false );

		lvec = all_vecs[1][0][loc1-1];
	}
	else if (w1 == 3) {
		// contract up to op2
		if (loc2 == L-1-skip) {
			rvec.assign( mpsYJ.getLambda(3, L).bond() );
			rvec.identity();
		}
		else
			rvec = all_vecs[2][1][loc2+1+skip];
		ket = (lop == 1)? netGL( mpsYJ.getGamma(3, loc2), mpsYJ.getLambda(3, loc2+1) )
		: netGLGL( mpsYJ.getGamma(3, loc2), mpsYJ.getLambda(3, loc2+1), mpsYJ.getGamma(3, loc2+1), mpsYJ.getLambda(3, loc2+2) );
		rvec = buildLRVecQn( rvec, ket, O2, false );
		// contract up to op1
		for (int i = loc2-1; i >= loc1+lop; --i) {
			ket = netGL( mpsYJ.getGamma(3, i), mpsYJ.getLambda(3, i+1) );
			rvec = buildLRVecQn( rvec, ket, false );
		}
		ket = (lop == 1)? netLGL( mpsYJ.getLambda(3, loc1), mpsYJ.getGamma(3, loc1), mpsYJ.getLambda(3, loc1+1) )
		: netLGLGL( mpsYJ.getLambda(3, loc1-1), mpsYJ.getGamma(3, loc1),
			mpsYJ.getLambda(3, loc1+1), mpsYJ.getGamma(3, loc1+1), mpsYJ.getLambda(3, loc1+2) );
		rvec = buildLRVecQn( rvec, ket, O1, false );
		// contract up to before JT
		for (int i = loc1-1; i > 0; --i) {
			ket = netGL( mpsYJ.getGamma(3, i), mpsYJ.getLambda(3, i+1) );
			rvec = buildLRVecQn( rvec, ket, false );
		}

		lvec = all_vecs[2][0][loc1-1];
	}

	corr = contrLRVecQn( lvec, rvec );
	return corr;
}

//======================================

uni10::CUniTensor calcCorr( YJuncQnIBC mpsYJ,
	std::vector< std::vector< std::vector<uni10::CUniTensor> > >& all_vecs,
	uni10::CUniTensor& O1, uni10::CUniTensor& O2,
	int w1, int loc1, int w2, int loc2 ) {
	///
	uni10::CUniTensor corr;
	uni10::CUniTensor ket, bra;
	uni10::CUniTensor w1_lvec, w2_rvec, w3_rvec;

	int L = mpsYJ.getSize();
	int bno1 = O1.bondNum();
	int bno2 = O2.bondNum();
	int lop1 = (bno1/2);
	int lop2 = (bno2/2);
	int skip1 = lop1 - 1;
	int skip2 = lop2 - 1;
	bool fermi = O1.bond()[0].Qlist()[0].isFermionic();

	// if ( bno != O2.bondNum() ) {
	// 	std::cerr << "In calcCorr : Bonds of two operators do not match.\n";
	// 	return corr;
	// }
	if ( bno1 < 2 || bno2 < 2 || bno1 > 4 || bno2 > 4) {
		std::cerr << "In calcCorr : Unsupported operator.\n";
		return corr;
	}

	if ((w1 == 1 && loc1 == L-1-skip1) || (w1 != 1 && loc1 == 0)) {
		if (loc2 != loc1 && loc2 != L-1-skip1-loc1) {
			// special location and non-symmetric
			std::cerr << "In calcCorr : Unsupported location + operator configuration." << '\n';
			return corr;
		}
	}

	// correlation on same wire
	if (w1 == w2) {
		return calcCorr(mpsYJ, all_vecs, O1, O2, w1, loc1, loc2);
	}

	// correlation at special locations
	if ((w1 == 1 && loc1 == L-1-skip1) && (w2 == 2 && loc2 == 0)) {
		if (lop1 == 1)
			return calcCorr(mpsYJ, all_vecs, O1, O2, "1", "2");
		else if (lop1 == 2)
			return calcCorr(mpsYJ, all_vecs, O1, O2, "WJ1", "WJ2");
	}
	if ((w1 == 1 && loc1 == L-1-skip1) && (w2 == 3 && loc2 == 0)) {
		if (lop1 == 1)
			return calcCorr(mpsYJ, all_vecs, O1, O2, "1", "3");
		else if (lop1 == 2)
			return calcCorr(mpsYJ, all_vecs, O1, O2, "WJ1", "WJ3");
	}
	if ((w1 == 2 && loc1 == 0) && (w2 == 3 && loc2 == 0)) {
		if (lop1 == 1)
			return calcCorr(mpsYJ, all_vecs, O1, O2, "2", "3");
		else if (lop1 == 2)
			return calcCorr(mpsYJ, all_vecs, O1, O2, "WJ2", "WJ3");
	}

	// correlation on different wires
	if (w1 == 1 && w2 == 2) {
		// w1_lvec, contract O1
		if (loc1 == 0) {
			w1_lvec.assign( mpsYJ.getLambda(1, 0).bond() );
			w1_lvec.identity();
		}
		else
			w1_lvec = all_vecs[0][0][loc1-1];
		ket = (lop1 == 1)? netLG( mpsYJ.getLambda(1, loc1), mpsYJ.getGamma(1, loc1) )
		: netLGLG( mpsYJ.getLambda(1, loc1), mpsYJ.getGamma(1, loc1), mpsYJ.getLambda(1, loc1+1), mpsYJ.getGamma(1, loc1+1) );
		w1_lvec = buildLRVecQn( w1_lvec, ket, O1, true );
		for (int i = loc1+lop1; i < L-1; ++i) {
			ket = netLG( mpsYJ.getLambda(1, i), mpsYJ.getGamma(1, i) );
			w1_lvec = buildLRVecQn( w1_lvec, ket, true );
		}
		// w2_rvec, contract O2
		if (loc2 == L-1-skip2) {
			w2_rvec.assign( mpsYJ.getLambda(2, L).bond() );
			w2_rvec.identity();
		}
		else
			w2_rvec = all_vecs[1][1][loc2+1+skip2];
		ket = (lop2 == 1)? netGL( mpsYJ.getGamma(2, loc2), mpsYJ.getLambda(2, loc2+1) )
		: netGLGL( mpsYJ.getGamma(2, loc2), mpsYJ.getLambda(2, loc2+1), mpsYJ.getGamma(2, loc2+1), mpsYJ.getLambda(2, loc2+2) );
		w2_rvec = buildLRVecQn( w2_rvec, ket, O2, false );
		for (int i = loc2-1; i > 0; --i) {
			ket = netGL( mpsYJ.getGamma(2, i), mpsYJ.getLambda(2, i+1) );
			w2_rvec = buildLRVecQn( w2_rvec, ket, false );
		}
		// w3_rvec
		w3_rvec = all_vecs[2][1][1];
	}
	else if (w1 == 1 && w2 == 3) {
		// w1_lvec, contract O1
		if (loc1 == 0) {
			w1_lvec.assign( mpsYJ.getLambda(1, 0).bond() );
			w1_lvec.identity();
		}
		else
			w1_lvec = all_vecs[0][0][loc1-1];
		ket = (lop1 == 1)? netLG( mpsYJ.getLambda(1, loc1), mpsYJ.getGamma(1, loc1) )
		: netLGLG( mpsYJ.getLambda(1, loc1), mpsYJ.getGamma(1, loc1), mpsYJ.getLambda(1, loc1+1), mpsYJ.getGamma(1, loc1+1) );
		w1_lvec = buildLRVecQn( w1_lvec, ket, O1, true );
		for (int i = loc1+lop1; i < L-1; ++i) {
			ket = netLG( mpsYJ.getLambda(1, i), mpsYJ.getGamma(1, i) );
			w1_lvec = buildLRVecQn( w1_lvec, ket, true );
		}
		// w2_rvec
		w2_rvec = all_vecs[1][1][1];
		// w3_rvec, contract O2
		if (loc2 == L-1-skip2) {
			w3_rvec.assign( mpsYJ.getLambda(3, L).bond() );
			w3_rvec.identity();
		}
		else
			w3_rvec = all_vecs[2][1][loc2+1+skip2];
		ket = (lop2 == 1)? netGL( mpsYJ.getGamma(3, loc2), mpsYJ.getLambda(3, loc2+1) )
		: netGLGL( mpsYJ.getGamma(3, loc2), mpsYJ.getLambda(3, loc2+1), mpsYJ.getGamma(3, loc2+1), mpsYJ.getLambda(3, loc2+2) );
		w3_rvec = buildLRVecQn( w3_rvec, ket, O2, false );
		for (int i = loc2-1; i > 0; --i) {
			ket = netGL( mpsYJ.getGamma(3, i), mpsYJ.getLambda(3, i+1) );
			w3_rvec = buildLRVecQn( w3_rvec, ket, false );
		}
	}
	else if (w1 == 2 && w2 == 3) {
		// w1_lvec
		w1_lvec = all_vecs[0][0][L-2];
		// w2_rvec, contract O1
		if (loc1 == L-1-skip1) {
			w2_rvec.assign( mpsYJ.getLambda(2, L).bond() );
			w2_rvec.identity();
		}
		else
			w2_rvec = all_vecs[1][1][loc1+1+skip1];
		ket = (lop1 == 1)? netGL( mpsYJ.getGamma(2, loc1), mpsYJ.getLambda(2, loc1+1) )
		: netGLGL( mpsYJ.getGamma(2, loc1), mpsYJ.getLambda(2, loc1+1), mpsYJ.getGamma(2, loc1+1), mpsYJ.getLambda(2, loc1+2) );
		if (O1.bondNum() == 3 && fermi) {
			std::vector<int> lab_op1 = {200, 100, -1};
			O1.setLabel(lab_op1);
			applyCrossGate(O1, -1, 200);
		}
		w2_rvec = buildLRVecQn( w2_rvec, ket, O1, false );
		for (int i = loc1-1; i > 0; --i) {
			ket = netGL( mpsYJ.getGamma(2, i), mpsYJ.getLambda(2, i+1) );
			w2_rvec = buildLRVecQn( w2_rvec, ket, false );
		}
		// w3_rvec, contract O2
		if (loc2 == L-1-skip2) {
			w3_rvec.assign( mpsYJ.getLambda(3, L).bond() );
			w3_rvec.identity();
		}
		else
			w3_rvec = all_vecs[2][1][loc2+1+skip2];
		ket = (lop2 == 1)? netGL( mpsYJ.getGamma(3, loc2), mpsYJ.getLambda(3, loc2+1) )
		: netGLGL( mpsYJ.getGamma(3, loc2), mpsYJ.getLambda(3, loc2+1), mpsYJ.getGamma(3, loc2+1), mpsYJ.getLambda(3, loc2+2) );
		w3_rvec = buildLRVecQn( w3_rvec, ket, O2, false );
		for (int i = loc2-1; i > 0; --i) {
			ket = netGL( mpsYJ.getGamma(3, i), mpsYJ.getLambda(3, i+1) );
			w3_rvec = buildLRVecQn( w3_rvec, ket, false );
		}
	}

	ket = mpsYJ.netThetaTrngl();
	bra = dag(ket);

	int lab_ket[] = {1, 101, 102, 103, -2, -3};
	int lab_bra[] = {2, 3, -1, 101, 102, 103};
	int lab_w1l[] = {-1, 1};
	int lab_w2r[] = {-2, 2};
	int lab_w3r[] = {-3, 3};
	int lab_w1l_nh[] = {-1, -10, 1};
	int lab_w2r_nh[] = {-10, -2, 2};
	int lab_w3r_nh[] = {-10, -3, 3};
	ket.setLabel(lab_ket);
	bra.setLabel(lab_bra);

	if (w1_lvec.bondNum() == 3)	w1_lvec.setLabel(lab_w1l_nh);
	else	w1_lvec.setLabel(lab_w1l);
	if (w2_rvec.bondNum() == 3)	w2_rvec.setLabel(lab_w2r_nh);
	else	w2_rvec.setLabel(lab_w2r);
	if (w3_rvec.bondNum() == 3)	w3_rvec.setLabel(lab_w3r_nh);
	else	w3_rvec.setLabel(lab_w3r);

	uni10::CUniTensor rvec;
	std::vector<int> to_cross;
	if (fermi && w1 == 1 && w2 == 2 && w2_rvec.bondNum() == 3) {
		rvec = uni10::contract(ket, w3_rvec);
		rvec = uni10::contract(rvec, w2_rvec);
		applyCrossGate(rvec, -10, 102);
		applyCrossGate(rvec, -10, 101);
	}
	else if (fermi && w1 == 2 && w2 == 3 && w3_rvec.bondNum() == 3) {
		rvec = uni10::contract(ket, w3_rvec);
		applyCrossGate(rvec, -10, 103);
		applyCrossGate(rvec, -10, -2);
		rvec = uni10::contract(rvec, w2_rvec);
	}
	else if (fermi && w1 == 1 && w2 == 3 && w3_rvec.bondNum() == 3) {
		rvec = uni10::contract(ket, w3_rvec);
		applyCrossGate(rvec, -10, 103);
		applyCrossGate(rvec, -10, -2);
		applyCrossGate(rvec, -10, 102);
		applyCrossGate(rvec, -10, 101);
		rvec = uni10::contract(rvec, w2_rvec);
	}
	else {
		rvec = uni10::contract(ket, w3_rvec);
		rvec = uni10::contract(rvec, w2_rvec);
	}
	rvec = uni10::contract(rvec, bra);
	if (rvec.bondNum() == 3) {
		int lab_rvi[] = {-1, -10, 1};
		int lab_rvf[] = {-10, -1, 1};
		rvec.setLabel(lab_rvi);
		rvec.permute(lab_rvf, 2);
	}
	corr = contrLRVecQn( w1_lvec, rvec );

	return corr;
}

//======================================

void corrOO( YJuncQnIBC mpsYJ,
	std::vector< std::vector< std::vector<uni10::CUniTensor> > >& all_vecs,
	uni10::CUniTensor& O1, uni10::CUniTensor& O2, uni10::CUniTensor& id,
	int w1, int w2, int offset1, int offset2, int init_loc, bool detail ) {
	/// calculate < O1(r1) O2(r2) > - < O(r1) > < O(r2) >
	int L = mpsYJ.getSize();
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expl, expr, corT;
	uni10::CUniTensor norm = calcExpV( mpsYJ, all_vecs, id, w1, 1 );

	int r1, r2;
	int ibn1 = O1.inBondNum();
	int ibn2 = O2.inBondNum();
	int lop1 = ibn1;

	int r = init_loc;
	while ( r < L - std::max(std::max(offset1, offset2), std::max(ibn1, ibn2)-1) ) {

		r1 = (w1 == 1)? L-lop1-r-offset1 : r+offset1;
		r2 = r+offset2;
		expl = calcExpV( mpsYJ, all_vecs, O1, w1, r1 );
		expr = calcExpV( mpsYJ, all_vecs, O2, w2, r2 );
		corT = calcCorr( mpsYJ, all_vecs, O1, O2, w1, r1, w2, r2 );

		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		corr = ( corT[0].real()/norm[0].real() );
		std::cout << r + 1 << '\t' << std::scientific << std::setprecision(14)
			<< corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\t'
			<< std::to_string((long long) w1) + std::to_string((long long) w2) << '\n';

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//======================================

void corrOO_semi( YJuncQnIBC mpsYJ,
	std::vector< std::vector< std::vector<uni10::CUniTensor> > >& all_vecs,
	uni10::CUniTensor& O1, uni10::CUniTensor& O2, uni10::CUniTensor& id,
	int w1, int offset1, int offset2, int init_loc, bool detail ) {
	/// calculate < O1(r1) O2(r2) > - < O(r1) > < O(r2) >
	int L = mpsYJ.getSize();
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expl, expr, corT;
	uni10::CUniTensor norm = calcExpV( mpsYJ, all_vecs, id, w1, 1 );

	int r1, r2;
	int ibn1 = O1.inBondNum();
	int ibn2 = O2.inBondNum();
	int lop = ibn1;

	int r = init_loc;
	while ( r < L - std::max(std::max(offset1, offset2), ibn2-1) ) {

		r1 = (w1 == 1)? L-lop-offset1 : offset1;
		r2 = (w1 == 1)? L-lop-r-offset2 : r+offset2;
		expl = calcExpV( mpsYJ, all_vecs, O1, w1, r1 );
		expr = calcExpV( mpsYJ, all_vecs, O2, w1, r2 );
		corT = calcCorr( mpsYJ, all_vecs, O1, O2, w1, r1, w1, r2 );

		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		corr = ( corT[0].real()/norm[0].real() );
		std::cout << r + 1 << '\t' << std::scientific << std::setprecision(14)
			<< corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\t'
			<< std::to_string((long long) w1) + std::to_string((long long) w1) << '\n';

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-w <(str) Y-Junct Wavefunction directory>" << std::endl;
	std::cerr << "-l <(int) system Length>" << std::endl;
	std::cerr << "-uc <(int) Unit Cell size>" << std::endl;
	std::cerr << "-op <(str) OPerator1> <(str) OPerator2>" << std::endl;
	std::cerr << "-wire <(int) WIRE1> <(int) WIRE2>" << std::endl;
	std::cerr << "-offset <(int) op1 OFFSET> <(int) op2 OFFSET>" << std::endl;
	std::cerr << "-iloc <(int) Inital LOCation>" << std::endl;
	std::cerr << "-all    loop over ALL wires" << std::endl;
	std::cerr << "-semi    SEMI-inf wire measurement" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	// wavefunction directory
	std::string wf_dir = "mps-ibc-yj";
	// length of each wire
	int len = 16;
	// unit-cell size
	int uc = 4;
	// bond dimension
	int chi = 8;
	// operators
	std::string op1f = "id";
	std::string op2f = "id";
	// offsets
	bool default_offset = true;
	int offset1 = 0;
	int offset2 = 0;
	// separate junction gams
	bool sep_gj = false;
	// wires to measure
	int w1 = 1;
	int w2 = 2;
	// initial location/index to measure
	int iloc = 1;
	// measure all wires
	bool all_wires = false;
	// semi-inf measurement
	bool semi = false;

	/// set parameters
	if (argc < 2) {
		errMsg(argv[0]);
		return 1;
	}
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-w") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-uc") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> uc;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-op") {
			if (i + 2 < argc) {
				op1f = std::string(argv[i+1]);
				op2f = std::string(argv[i+2]);
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-wire") {
			if (i + 2 < argc) {
				std::istringstream(argv[i+1]) >> w1;
				std::istringstream(argv[i+2]) >> w2;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-offset") {
			if (i + 2 < argc) {
				default_offset = false;
				std::istringstream(argv[i+1]) >> offset1;
				std::istringstream(argv[i+2]) >> offset2;
			}
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-iloc") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iloc;
			else {
				errMsg(argv[0]);
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-all") {
			all_wires = true;
		}
		else if (std::string(argv[i]) == "-semi") {
			semi = true;
		}
	}

	// initialize YJ
	uni10::CUniTensor gam0;
	try {
		gam0 = uni10::CUniTensor(wf_dir + "/wire1/gamma_0");
	}
	catch(const std::exception& e) {
		try {
			gam0 = uni10::CUniTensor(wf_dir + "/gamma_0");
		}
		catch(const std::exception& e) {
			errMsg(argv[0]);
			return 1;
		}
	}
	std::vector<uni10::Qnum> qphys = gam0.bond()[1].Qlist();
	std::vector<uni10::Qnum> qvirt = gam0.bond()[0].Qlist();
	chi = gam0.bond()[0].dim();
	YJuncQnIBC mpsYJ(len, chi, qphys, qvirt);
	mpsYJ.importMPS(wf_dir, uc, sep_gj);

	// import operators
	uni10::CUniTensor op1, op2;
	try {
		op1 = uni10::CUniTensor(op1f);
		op2 = uni10::CUniTensor(op2f);
	}
	catch(const std::exception& e) {
		if (qphys[0].U1() == 0) {
			op1 = opFU1_grd0(op1f, 0, 0);
			op2 = opFU1_grd0(op2f, 0, 0);
		}
		else {
			op1 = opFU1_grd0(op1f, 1, 2);
			op2 = opFU1_grd0(op2f, 1, 2);
		}
	}

	std::vector<uni10::Bond> bdi;
	bdi.push_back( uni10::Bond(uni10::BD_IN, qphys) );
	bdi.push_back( uni10::Bond(uni10::BD_OUT, qphys) );
	uni10::CUniTensor id(bdi);
	id.identity();

	std::vector< std::vector< std::vector<uni10::CUniTensor> > > all_vecs = mpsYJ.allLRVecs();
	if (w1 > w2) {
		std::swap(w1, w2);
		std::swap(op1f, op2f);
		std::swap(op1, op2);
	}

	if (semi) {
		std::cout << "#r\t<" << op1f << "(r1) " << op2f << "(r2)>\t<" << op1f << "(r1) " << op2f << "(r2)> - <"
			<< op1f << "(r1)> <" << op2f << "(r2)>\t<" << op1f << "(r1)>\t<" << op2f << "(r2)>\t" << "wires" << '\n';
		if (default_offset) {
			if ( op1.inBondNum() == 2 && op2.inBondNum() == 2 ) {
				std::cerr << "Operator not supported." << '\n';
				return 1;
				// offset1 = 1;
				// offset2 = 2;
			}
			else {
				offset1 = 1;
				offset2 = 1;
			}
		}
		if (all_wires) {
			for (w1 = 1; w1 <= 3; ++w1) {
				corrOO_semi( mpsYJ, all_vecs, op1, op2, id, w1, offset1, offset2, iloc, true );
				std::cout << '\n';
			}
		}
		else
			corrOO_semi( mpsYJ, all_vecs, op1, op2, id, w1, offset1, offset2, iloc, true );
	}
	else {
		std::cout << "#r\t<" << op1f << "(r1) " << op2f << "(r2)>\t<" << op1f << "(r1) " << op2f << "(r2)> - <"
			<< op1f << "(r1)> <" << op2f << "(r2)>\t<" << op1f << "(r1)>\t<" << op2f << "(r2)>\t" << "wires" << '\n';
		if (all_wires) {
			for (w1 = 1; w1 <= 2; ++w1) {
				for (w2 = w1+1; w2 <=3; ++w2) {
					corrOO( mpsYJ, all_vecs, op1, op2, id, w1, w2, offset1, offset2, iloc, true );
					std::cout << '\n';
				}
			}
		}
		else
			corrOO( mpsYJ, all_vecs, op1, op2, id, w1, w2, offset1, offset2, iloc, true );
	}

	return 0;
}
