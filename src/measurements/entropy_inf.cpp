#include <iostream>
#include <string>
#include <uni10.hpp>

#include <tns-func/tns_const.h>

//==============================

double entanglementEntropy( const uni10::CUniTensor & l ){

	uni10::CMatrix mat = l.getBlock();
	int D = mat.row();

	double S = 0.0, sch_val = 0.0;
	for (int i = 0; i < D; i++) {
		sch_val = mat.at(i, i).real();
		if ( sch_val > 1e-16 )
			S -= ( sch_val * sch_val ) * log( sch_val * sch_val );
	}

	return S;
}

//================================

int main( int argc, char* argv[] ){

	// deploy lambda tensors
	std::vector<uni10::UniTensor> lambda;

	std::string wf_dir = "mps-inf";
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_0") );
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_1") );

	if ( argc > 1 && std::string( argv[1] ) == "-a" )
		std::cout << std::scientific << std::setprecision(14)
			<< entanglementEntropy(lambda[0]) << "\t" << entanglementEntropy(lambda[1]) << "\t";
	else if ( argc > 1 && std::string( argv[1] ) == "-s" ) {
		int chi0 = lambda[0].bond()[0].dim();
		int chi1 = lambda[1].bond()[0].dim();
		uni10::CMatrix lam0 = lambda[0].getBlock();
		uni10::CMatrix lam1 = lambda[1].getBlock();
		for (int i = 0; i < chi0; ++i)
			std::cout << std::scientific << std::setprecision(14) << lam0.at(i,i).real() << "\n";
		std::cout << "\n";
		for (int i = 0; i < chi1; ++i)
			std::cout << std::scientific << std::setprecision(14) << lam1.at(i,i).real() << "\n";
	}
	else
		std::cout << std::scientific << std::setprecision(14) << entanglementEntropy(lambda[0]) << "\t";

	lambda.clear();

	return 0;
}
