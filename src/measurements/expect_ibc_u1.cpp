#include <sstream>
#include <math.h>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

void expValIBC( ChainQnIBC& mpsIBC, int L,
	uni10::CUniTensor& O1, uni10::CUniTensor& id, bool detail ) {

	/// calculate < O1(r) >
	double eVal = 0.0;
	uni10::CUniTensor expv;
	uni10::CUniTensor norm = mpsIBC.expVal(id, 0);

	int r = 0;
	while ( r < L-(O1.inBondNum()-1) ) {

		expv = mpsIBC.expVal(O1, r);
		eVal = ( expv[0].real()/norm[0].real() );
		std::cout << r << "\t" << std::scientific << std::setprecision(14) << eVal << "\n";

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//======================================

void errMsg( char* arg ) {
	///
	std::cerr << "Usage: " << arg << " <lattice_size> <virt_bond_dim> <operator_file> <mps_folder>" << std::endl;
}

//================================

int main( int argc, char* argv[] ){

	if (argc < 4) {

		errMsg( argv[0] );
		return 1;
	}

	// initialize IBC chain
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;
	std::string O1str = std::string( argv[3] );

	std::string wf_dir = "mps-inf";
	if (argc > 4)
		wf_dir = std::string( argv[4] );

	std::vector<uni10::Qnum> qphys = uni10::CUniTensor( wf_dir + "/gamma_0" ).bond()[1].Qlist();

	ChainQnIBC mpsIBC( L, X, qphys );
	mpsIBC.importMPS( wf_dir );

	// import operators
	std::vector<uni10::Bond> bdi;
	bdi.push_back( uni10::Bond(uni10::BD_IN, qphys) );
	bdi.push_back( uni10::Bond(uni10::BD_OUT, qphys) );
	uni10::CUniTensor id(bdi);
	id.identity();
	uni10::CUniTensor O1 = uni10::CUniTensor(O1str);
	//====== calculate expectation value ======
	std::cout << "#r\t<"+O1str+"(r)>\n";
	expValIBC( mpsIBC, L, O1, id, true );

	return 0;
}
