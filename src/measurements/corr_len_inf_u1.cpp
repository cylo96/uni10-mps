#include <iostream>
#include <sstream>
#include <cmath>
#include <algorithm>

#include <tns-func/func_net.h>
#include <mps-qn/func_qn_la.h>

//=========================================

int main(int argc, char* argv[]){

	// deploy gamma, lambda tensors
	std::vector<uni10::CUniTensor> gamma;
	std::vector<uni10::CUniTensor> lambda;
	std::vector<double> corrL;

	std::string wf_dir = "mps-inf";
	gamma.push_back( uni10::CUniTensor(wf_dir + "/gamma_0") );
	gamma.push_back( uni10::CUniTensor(wf_dir + "/gamma_1") );
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_0") );
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_1") );

	// calc eigenvalue of transfer matrix
	double E0, CL0, CL1;

	uni10::CUniTensor ket = netGLGL( gamma[0], lambda[1], gamma[1], lambda[0] );
	uni10::CUniTensor trial( lambda[0].bond() );
	trial = bondInv( trial );
	trial.identity();
	trial.permute(2);

	int s0 = myQnArpLanczosDomEig( ket, trial, E0, 500, 1e-15, 2 );
	CL0 = (double)-1.0 / std::log( E0 );

	ket = netGLGL( gamma[1], lambda[0], gamma[0], lambda[1] );
	trial.assign( lambda[1].bond() );
	trial = bondInv( trial );
	trial.identity();
	trial.permute(2);

	s0 = myQnArpLanczosDomEig( ket, trial, E0, 500, 1e-15, 2 );
	CL1 = (double)-1.0 / std::log( E0 );

	// output
	double avgV = 0.5 * ( CL0 + CL1 );
	std::cout << std::scientific << std::setprecision(14) << avgV << '\t';

	gamma.clear();
	lambda.clear();
	corrL.clear();

	return 0;
}
