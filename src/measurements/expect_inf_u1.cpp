#include <iostream>
#include <string>

#include <mps-qn/ChainQnInf.h>

//=========================================

int main( int argc, char* argv[] ) {

	if (argc < 2 ) {

		std::cerr << "Usage: " << argv[0] << " [options] or <op_file>" << std::endl;
		std::cerr << "Allowed options:" << std::endl;
		std::cerr << "-n  calculate the norm" << std::endl;
		return 1;
	}

	std::string wf_dir = "mps-inf";

	uni10::CUniTensor g0( wf_dir + "/gamma_0" );
	std::vector<uni10::Bond> bdg = g0.bond();
	std::vector<uni10::Qnum> qph = bdg[1].Qlist();
	int d = bdg[1].dim();
	int X = bdg[0].dim();

	ChainQnInf mps( X, qph );
	mps.importMPS( wf_dir, 2, false, false );

	std::vector< uni10::Bond > bdi;
	bdi.push_back( uni10::Bond( uni10::BD_IN, qph ) );
	bdi.push_back( uni10::Bond( uni10::BD_OUT, qph ) );
	uni10::CUniTensor id( bdi );
	id.identity();

	std::vector<uni10::CUniTensor> norm;
	norm.push_back( mps.expVal(id, 0) );
	norm.push_back( mps.expVal(id, 1) );

	if ( std::string(argv[1]) == "-n" ) {
		std::cout << std::scientific << std::setprecision(16)
			<< norm[0][0].real() << '\t' << norm[1][0].real() << '\t';
	}
	else {
		uni10::CUniTensor op( argv[1] );
		if ( op.bondNum()%2 ) {
			std::cerr << "Non-symmetric operators are not supported.\n";
			return 1;
		}
		std::vector<uni10::CUniTensor> expv;
		expv.push_back( mps.expVal(op, 0) );
		expv.push_back( mps.expVal(op, 1) );

		std::vector<double> eval;
		eval.push_back( expv[0][0].real()/norm[0][0].real() );
		eval.push_back( expv[1][0].real()/norm[1][0].real() );

		std::cout << std::scientific << std::setprecision(16)
			<< 0.5 * (eval[0] + eval[1]) << '\t'
			<< 0.5 * (eval[0] - eval[1]) << '\t'
			<< eval[0] << '\t' << eval[1] << '\t';
	}

	mps.clear();

	return 0;
}
