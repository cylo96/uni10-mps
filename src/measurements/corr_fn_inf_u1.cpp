#include <iostream>
#include <sstream>

#include <mps-qn/ChainQnInf.h>
#include <mps-qn/func_qn_op.h>
#include <mps-qn/func_qn_net.h>
#include <tns-func/func_net.h>

//=========================================

void corrFnInf1SU1( ChainQnInf mps,
	uni10::CUniTensor op1, uni10::CUniTensor op2, uni10::CUniTensor id,
	int r_max ) {

	/// calculate < op1(0) op2(r) > - < op1(0) > < op2(r) >
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expv, expl, expr, norm;

	uni10::CUniTensor ga = mps.getGamma(0);
	uni10::CUniTensor gb = mps.getGamma(1);
	uni10::CUniTensor la = mps.getLambda(0);
	uni10::CUniTensor lb = mps.getLambda(1);

	uni10::CUniTensor ket_a = netLG( la, ga );
	uni10::CUniTensor bra_a = dag( ket_a );
	uni10::CUniTensor ket_b = netLG( lb, gb );
	uni10::CUniTensor bra_b = dag( ket_b );

	int lab_bra[] = {1, 0, 200};
	int lab_ket[] = {0, 100, 2};
	int lab_idn[] = {200, 100};

	int bno1 = op1.bondNum();
	std::vector<int> lab_op1;
	lab_op1.push_back(200);
	lab_op1.push_back(100);
	if (bno1 == 3)
		lab_op1.push_back(-1);

	ket_a.setLabel( lab_ket );
	bra_a.setLabel( lab_bra );
	ket_b.setLabel( lab_ket );
	bra_b.setLabel( lab_bra );
	op1.setLabel( lab_op1 );
	id.setLabel( lab_idn );

	uni10::CUniTensor head_op = uni10::contract( op1, ket_a, false );
	head_op = uni10::contract( bra_a, head_op, false );
	uni10::CUniTensor head_id = uni10::contract( id, ket_a, false );
	head_id = uni10::contract( bra_a, head_id, false );

	std::vector<int> lab_head = head_op.label();
	std::vector<int> lab_hidn = head_id.label();


	uni10::CUniTensor last_ket_a = netLGL( la, ga, lb );
	uni10::CUniTensor last_bra_a = dag( last_ket_a );
	uni10::CUniTensor last_ket_b = netLGL( lb, gb, la );
	uni10::CUniTensor last_bra_b = dag( last_ket_b );

	int lab_last_bra[] = {12, 10, 200};
	int lab_last_ket[] = {11, 100, 12};

	int bno2 = op2.bondNum();
	std::vector<int> lab_op2;
	lab_op2.push_back(200);
	lab_op2.push_back(100);
	if (bno2 == 3)
		lab_op2.push_back(-1);

	last_ket_a.setLabel(lab_last_ket);
	last_bra_a.setLabel(lab_last_bra);
	last_ket_b.setLabel(lab_last_ket);
	last_bra_b.setLabel(lab_last_bra);
	op2.setLabel(lab_op2);

	uni10::CUniTensor tail_a_op = uni10::contract(op2, last_ket_a, false);
	tail_a_op = uni10::contract(tail_a_op, last_bra_a, false);
	uni10::CUniTensor tail_b_op = uni10::contract(op2, last_ket_b, false);
	tail_b_op = uni10::contract(tail_b_op, last_bra_b, false);

	uni10::CUniTensor tail_a_id = uni10::contract(id, last_ket_a, false);
	tail_a_id = uni10::contract(tail_a_id, last_bra_a, false);
	uni10::CUniTensor tail_b_id = uni10::contract(id, last_ket_b, false);
	tail_b_id = uni10::contract(tail_b_id, last_bra_b, false);

	std::vector<int> lab_tail;
	if (bno2 == 3)
		lab_tail.push_back(-1);
	lab_tail.push_back(11);
	lab_tail.push_back(10);
	int lab_tidn[] = {11, 10};

	tail_a_op.setLabel( lab_tail );
	tail_a_id.setLabel( lab_tidn );
	tail_b_op.setLabel( lab_tail );
	tail_b_id.setLabel( lab_tidn );


	int r = 1, r_head = 1;
	bool odd;

	while ( r <= r_max ) {

		for (int i = r_head; i < r; ++i) {

			lab_head[0] = 1;
			lab_head[lab_head.size()-1] = 2;
			lab_hidn[0] = 1;
			lab_hidn[1] = 2;
			head_op.setLabel( lab_head );
			head_id.setLabel( lab_hidn );

			int lab_bra[] = {10, 1, 100};
			int lab_ket[] = {2, 100, 11};

			if (i%2) {
				bra_b.setLabel(lab_bra);
				ket_b.setLabel(lab_ket);
				head_op = uni10::contract(head_op, ket_b, false);
				head_op = uni10::contract(bra_b, head_op, false);
				head_id = uni10::contract(head_id, ket_b, false);
				head_id = uni10::contract(bra_b, head_id, false);
			}
			else {
				bra_a.setLabel(lab_bra);
				ket_a.setLabel(lab_ket);

				head_op = uni10::contract(head_op, ket_a, false);
				head_op = uni10::contract(bra_a, head_op, false);
				head_id = uni10::contract(head_id, ket_a, false);
				head_id = uni10::contract(bra_a, head_id, false);
			}
		}

		lab_head[0] = 10;
		lab_head[lab_head.size()-1] = 11;
		lab_hidn[0] = 10;
		lab_hidn[1] = 11;
		head_op.setLabel( lab_head );
		head_id.setLabel( lab_hidn );

		odd = (bool)(r%2);
		if (odd) {
			expv = (head_op.bondNum() == tail_b_op.bondNum())?
				uni10::contract(head_op, tail_b_op, false) : uni10::CUniTensor();
			expl = (head_op.bondNum() == tail_b_id.bondNum())?
				uni10::contract(head_op, tail_b_id, false) : uni10::CUniTensor();
			expr = (head_id.bondNum() == tail_b_op.bondNum())?
				uni10::contract(head_id, tail_b_op, false) : uni10::CUniTensor();
			norm = uni10::contract(head_id, tail_b_id, false);
		}
		else {
			expv = (head_op.bondNum() == tail_a_op.bondNum())?
				uni10::contract(head_op, tail_a_op, false) : uni10::CUniTensor();
			expl = (head_op.bondNum() == tail_a_id.bondNum())?
				uni10::contract(head_op, tail_a_id, false) : uni10::CUniTensor();
			expr = (head_id.bondNum() == tail_a_op.bondNum())?
				uni10::contract(head_id, tail_a_op, false) : uni10::CUniTensor();
			norm = uni10::contract(head_id, tail_a_id, false);
		}

		corr = expv[0].real()/norm[0].real();
		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		std::cout << r << '\t' << std::scientific << std::setprecision(14)
			<< corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\n';

		r_head = r;
		r += 1;
	}
}

//=========================================

void corrFnInf2SU1( ChainQnInf mps,
	uni10::CUniTensor op1, uni10::CUniTensor op2, uni10::CUniTensor id,
	int r_max ) {

	/// calculate < op1(0) op2(r) > - < op1(0) > < op2(r) >
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expv, expl, expr, norm;

	uni10::CUniTensor ga = mps.getGamma(0);
	uni10::CUniTensor gb = mps.getGamma(1);
	uni10::CUniTensor la = mps.getLambda(0);
	uni10::CUniTensor lb = mps.getLambda(1);

	// one site A tensors
	uni10::CUniTensor ket_a1 = netLG( la, ga );
	uni10::CUniTensor bra_a1 = dag( ket_a1 );
	uni10::CUniTensor ket_b1 = netLG( lb, gb );
	uni10::CUniTensor bra_b1 = dag( ket_b1 );

	// two site AA tensors
	uni10::CUniTensor ket_a = netLGLG( la, ga, lb, gb );
	uni10::CUniTensor bra_a = dag( ket_a );
	uni10::CUniTensor ket_b = netLGLG( lb, gb, la, ga );
	uni10::CUniTensor bra_b = dag( ket_b );

	int lab_bra[] = {1, 0, 200, 201};
	int lab_ket[] = {0, 100, 101, 2};
	int lab_op[] = {200, 201, 100, 101};

	ket_a.setLabel( lab_ket );
	bra_a.setLabel( lab_bra );
	ket_b.setLabel( lab_ket );
	bra_b.setLabel( lab_bra );
	op1.setLabel( lab_op );
	id.setLabel( lab_op );

	uni10::CUniTensor head_op = uni10::contract( op1, ket_a, false );
	head_op = uni10::contract( bra_a, head_op, false );
	uni10::CUniTensor head_id = uni10::contract( id, ket_a, false );
	head_id = uni10::contract( bra_a, head_id, false );

	std::vector<int> lab_head = head_op.label();


	uni10::CUniTensor last_ket_a = netLGLGL( la, ga, lb, gb, la );
	uni10::CUniTensor last_bra_a = dag( last_ket_a );
	uni10::CUniTensor last_ket_b = netLGLGL( lb, gb, la, ga, lb );
	uni10::CUniTensor last_bra_b = dag( last_ket_b );

	int lab_last_bra[] = {12, 10, 200, 201};
	int lab_last_ket[] = {11, 100, 101, 12};

	last_ket_a.setLabel(lab_last_ket);
	last_bra_a.setLabel(lab_last_bra);
	last_ket_b.setLabel(lab_last_ket);
	last_bra_b.setLabel(lab_last_bra);
	op2.setLabel(lab_op);

	uni10::CUniTensor tail_a_op = uni10::contract(op2, last_ket_a, false);
	tail_a_op = uni10::contract(tail_a_op, last_bra_a, false);
	uni10::CUniTensor tail_b_op = uni10::contract(op2, last_ket_b, false);
	tail_b_op = uni10::contract(tail_b_op, last_bra_b, false);

	uni10::CUniTensor tail_a_id = uni10::contract(id, last_ket_a, false);
	tail_a_id = uni10::contract(tail_a_id, last_bra_a, false);
	uni10::CUniTensor tail_b_id = uni10::contract(id, last_ket_b, false);
	tail_b_id = uni10::contract(tail_b_id, last_bra_b, false);


	int r = 1, r_head = 1;
	bool odd;

	while ( r <= r_max ) {

		for (int i = r_head; i < r; ++i) {

			lab_head[0] = 1;
			lab_head[1] = 2;
			head_op.setLabel( lab_head );
			head_id.setLabel( lab_head );

			int lab_bra[] = {10, 1, 100};
			int lab_ket[] = {2, 100, 11};

			if (i%2 == 0) {	// cause op is 2-site
				bra_b1.setLabel(lab_bra);
				ket_b1.setLabel(lab_ket);
				head_op = uni10::contract(head_op, ket_b1, false);
				head_op = uni10::contract(bra_b1, head_op, false);
				head_id = uni10::contract(head_id, ket_b1, false);
				head_id = uni10::contract(bra_b1, head_id, false);
			}
			else {
				bra_a1.setLabel(lab_bra);
				ket_a1.setLabel(lab_ket);

				head_op = uni10::contract(head_op, ket_a1, false);
				head_op = uni10::contract(bra_a1, head_op, false);
				head_id = uni10::contract(head_id, ket_a1, false);
				head_id = uni10::contract(bra_a1, head_id, false);
			}
		}

		lab_head[0] = 10;
		lab_head[1] = 11;
		head_op.setLabel( lab_head );
		head_id.setLabel( lab_head );

		odd = (bool)(r%2);
		if (!odd) {	// cause op is 2-site
			expv = uni10::contract(head_op, tail_b_op, false);
			expl = uni10::contract(head_op, tail_b_id, false);
			expr = uni10::contract(head_id, tail_b_op, false);
			norm = uni10::contract(head_id, tail_b_id, false);
		}
		else {
			expv = uni10::contract(head_op, tail_a_op, false);
			expl = uni10::contract(head_op, tail_a_id, false);
			expr = uni10::contract(head_id, tail_a_op, false);
			norm = uni10::contract(head_id, tail_a_id, false);
		}

		corr = expv[0].real()/norm[0].real();
		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		std::cout << r+1 << '\t' << std::scientific << std::setprecision(14)
			<< corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\n';

		r_head = r;
		r += 1;
	}
}

//=========================================

int main( int argc, char* argv[] ) {

	if (argc < 4) {

		std::cerr << "Not Enough Arguments!" << std::endl;
		std::cerr << "Usage: " << argv[0] << " <op1_file> <op2_file> <distance> [option]" << std::endl;
		std::cerr << "Allowed options:" << std::endl;
		return 1;
	}

	std::string wf_dir = "mps-inf";
	std::string op1f, op2f;
	op1f = std::string(argv[1]);
	op2f = std::string(argv[2]);
	int r_max;
	std::istringstream(argv[3]) >> r_max;

	uni10::CUniTensor g0( wf_dir + "/gamma_0" );
	std::vector<uni10::Bond> bdg = g0.bond();
	std::vector<uni10::Qnum> qph = bdg[1].Qlist();
	int d = bdg[1].dim();
	int X = bdg[0].dim();

	ChainQnInf mps( X, qph );
	mps.importMPS( wf_dir, 2, false, false );

	std::vector< uni10::Bond > bdi;
	bdi.push_back( uni10::Bond( uni10::BD_IN, qph ) );
	bdi.push_back( uni10::Bond( uni10::BD_OUT, qph ) );
	uni10::CUniTensor id( bdi );
	id.identity();

	uni10::CUniTensor op1( op1f );
	uni10::CUniTensor op2( op2f );


	if ( (op1.bondNum() != op2.bondNum()) && (op1.bondNum()+op1.bondNum() > 5) ) {
		std::cerr << "corrFnInfU1 error: incorrect bond structure.";
		return 1;
	}

	std::cout << "#r" << '\t' << "< "+op1f+"(0) "+op2f+"(r) >" << '\t'
		<< "< "+op1f+"(0) "+op2f+"(r) > - < "+op1f+"(0) > < "+op2f+"(r) >" << '\t'
		<< "< "+op1f+"(0) >" << '\t' << "< "+op2f+"(r) >" << '\n';

	if ( op1.bondNum() == 4 && op2.bondNum() == 4 ) {
		id = uni10::otimes(id, id);
		corrFnInf2SU1( mps, op1, op2, id, r_max );
	}
	else
		corrFnInf1SU1( mps, op1, op2, id, r_max );

	mps.clear();

	return 0;
}
