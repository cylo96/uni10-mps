#include <iostream>
#include <sstream>
#include <uni10.hpp>

#include <mps-qn/func_qn_meas.h>

//================================

int main( int argc, char* argv[] ){

	// deploy lambda tensors
	std::vector<uni10::UniTensor> lambda;

	std::string wf_dir = "mps-inf";
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_0") );
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_1") );

	if ( argc > 1 && std::string( argv[1] ) == "-a" )
		std::cout << std::scientific << std::setprecision(14)
			<< entangleEntropyQn( lambda[0] ) << '\t' << entangleEntropyQn( lambda[1] ) << '\t';

	else if ( argc > 1 && std::string( argv[1] ) == "-s" ) {
		int N = std::min( lambda[0].bond()[0].dim(), 4 );
		if (argc > 2)
			std::istringstream(argv[2]) >> N;

		std::vector< SchmidtVal > spec0 = entangleSpecQn( lambda[0], N );
		std::vector< SchmidtVal > spec1 = entangleSpecQn( lambda[1], N );

		for (int i = 0; i < N; ++i)
			std::cout << std::scientific << std::setprecision(14) << spec0[i].val << '\t' << spec0[i].qn << '\n';
		std::cout << '\n';
		for (int i = 0; i < N; ++i)
			std::cout << std::scientific << std::setprecision(14) << spec1[i].val << '\t' << spec1[i].qn << '\n';
	}

	else
		std::cout << std::scientific << std::setprecision(14) << entangleEntropyQn( lambda[0] ) << '\t';

	lambda.clear();

	return 0;
}
