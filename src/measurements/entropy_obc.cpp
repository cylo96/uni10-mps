#include <iostream>
#include <string>
#include <uni10.hpp>

#include <mps.hpp>

//==============================

double entanglementEntropy( const uni10::CUniTensor & l ){

	uni10::CMatrix mat = l.getBlock();
	int D = mat.row();

	double S = 0.0, sch_val = 0.0;
	for (int i = 0; i < D; i++) {
		sch_val = mat.at(i, i).real();
		if ( sch_val > 1e-16 )
			S -= ( sch_val * sch_val ) * log( sch_val * sch_val );
	}

	return S;
}

//======================================

void errMsg( char* arg ) {
	///
	std::cerr << "Usage: " << arg << " <lattice_size> <virt_bond_dim> <mps_folder>" << std::endl;
}

//================================

int main( int argc, char* argv[] ){

	if (argc < 4) {

		errMsg( argv[0] );
		return 1;
	}

	// initialize OBC chain
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;

	std::string wf_dir = "mps-inf";
	if (argc > 3)
		wf_dir = std::string( argv[3] );

	int d = uni10::CUniTensor( wf_dir + "/gamma_0" ).bond()[1].dim();
	ChainOBC mpsOBC(L, d, X);
	mpsOBC.importMPS( wf_dir );

	std::cout << "#r\tEntEnt(r)\n";
	for (int r = 0; r <= L; ++r)
		std::cout << r << '\t' << std::scientific << std::setprecision(14)
			<< entanglementEntropy( mpsOBC.getLambda(r) ) << '\n';

	return 0;
}
