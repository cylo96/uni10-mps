#include <iostream>
#include <sstream>
#include <uni10.hpp>
#include <mps.hpp>

//======================================

void errMsg( char* arg ) {
	///
	std::cerr << "Usage: " << arg << " <mps_folder> <site_start> <site_mid> <site_term>" << std::endl;
}

//================================

int main( int argc, char* argv[] ){

	if (argc < 5) {

		errMsg( argv[0] );
		return 1;
	}

	// initialize IBC chain
	int L, X;
	std::string wf_dir;
	int start, mid, term;

	wf_dir = std::string( argv[1] );
	std::istringstream(argv[2]) >> start;
	std::istringstream(argv[3]) >> mid;
	std::istringstream(argv[4]) >> term;

	uni10::CUniTensor lam_start(wf_dir + "/lambda_" + std::to_string((long long) start));
	uni10::CUniTensor lam_mid(wf_dir + "/lambda_" + std::to_string((long long) mid));
	std::vector<SchmidtVal> spec_start = entangleSpecQn(lam_start, lam_start.bond()[0].dim());
	std::vector<SchmidtVal> spec_mid = entangleSpecQn(lam_mid, lam_mid.bond()[0].dim());
	
	double qn_start = 0., qn_mid = 0.;
	for (std::vector<SchmidtVal>::iterator it = spec_start.begin(); it != spec_start.end(); ++it)
		qn_start += ((*it).val * (*it).val * (*it).qn.U1());
	for (std::vector<SchmidtVal>::iterator it = spec_mid.begin(); it != spec_mid.end(); ++it)
		qn_mid += ((*it).val * (*it).val * (*it).qn.U1());

	double qn_term = qn_start + (qn_mid - qn_start)/(mid - start) * (term - start);
	std::cout << "Average Qnum at site " << start << " = " << qn_start << '\n';
	std::cout << "Average Qnum at site " << mid << " = " << qn_mid << '\n';
	std::cout << "Extrapolated Qnum at site " << term << " = " << qn_term << '\n';

	return 0;
}
