#include <iostream>
#include <sstream>
#include <stdexcept>

#include <mps.hpp>

//=========================================

int main( int argc, char* argv[] ) {

	if (argc < 3 ) {

		std::cerr << "Usage: " << argv[0] << " <mps_dir> <index>" << std::endl;
		return 1;
	}

	int idx = 0;
	std::string mps_dir = std::string( argv[1] );
	std::istringstream(argv[2]) >> idx;

	uni10::CUniTensor gam( mps_dir + "/gamma_" + std::to_string( (long long)idx ) );
	uni10::CUniTensor ll( mps_dir + "/lambda_" + std::to_string( (long long)idx ) );

	uni10::CUniTensor lr;
	try {
		lr = uni10::CUniTensor( mps_dir + "/lambda_" + std::to_string( (long long)(idx+1) ) );
	}
	catch(const std::exception& e) {
		lr = uni10::CUniTensor( mps_dir + "/lambda_0" );
	}

	uni10::CUniTensor A = netLG( ll, gam );
	uni10::CUniTensor B = netGL( gam, lr );
	uni10::CUniTensor Ad = dag(A);
	uni10::CUniTensor Bd = dag(B);

	int lab_a[] = {0, 100, 1};
	int lab_ad[] = {2, 0, 100};
	A.setLabel(lab_a);
	Ad.setLabel(lab_ad);
	uni10::CUniTensor lvec = Ad * A;

	int lab_b[] = {0, 100, 1};
	int lab_bd[] = {1, 2, 100};
	B.setLabel(lab_b);
	Bd.setLabel(lab_bd);
	uni10::CUniTensor rvec = B * Bd;

	std::cout << lvec << rvec;

	return 0;
}

