#include <sstream>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

uni10::CUniTensor calcExpV( ChainQnIBC& mpsIBC, uni10::CUniTensor& O1, int loc,
	const std::vector<uni10::CUniTensor>& lvecs, const std::vector<uni10::CUniTensor>& rvecs ) {
	///
	uni10::CUniTensor expv;
	uni10::CUniTensor lv, rv, ket;
	int L = mpsIBC.getSize();
	int obn = O1.bondNum();
	int skip = (obn/2) - 1;

	if ( obn < 2 || obn > 4 ) {
		std::cerr << "In calcExpV : Unsupported operator.\n";
		return expv;
	}
	if ( obn%2 )
		return expv;

	if ( loc == 0 ) {
		lv.assign( mpsIBC.getLambda(0).bond() );
		lv.identity();
		rv = rvecs[1+skip];
	}
	else if ( loc == L-1-skip ) {
		lv = lvecs[L-2-skip];
		rv.assign( mpsIBC.getLambda(L).bond() );
		rv.identity();
	}
	else {
		lv = lvecs[loc-1];
		rv = rvecs[loc+1+skip];
	}

	if ( skip == 0 )
		ket = netLGL( mpsIBC.getLambda(loc), mpsIBC.getGamma(loc), mpsIBC.getLambda(loc+1) );
	else if ( skip == 1 )
		ket = netLGLGL( mpsIBC.getLambda(loc), mpsIBC.getGamma(loc),
			mpsIBC.getLambda(loc+1), mpsIBC.getGamma(loc+1), mpsIBC.getLambda(loc+2) );
	lv = buildLRVecQn( lv, ket, O1, true );

	expv = contrLRVecQn( lv, rv );
	return expv;
}

//======================================

uni10::CUniTensor calcCorr( ChainQnIBC& mpsIBC,
	uni10::CUniTensor& O1, uni10::CUniTensor& O2, int loc1, int loc2,
	const std::vector<uni10::CUniTensor>& lvecs, const std::vector<uni10::CUniTensor>& rvecs ) {
	///
	uni10::CUniTensor corr;
	uni10::CUniTensor lv, rv, ket;
	int L = mpsIBC.getSize();
	int obn = O1.bondNum();
	int skip = (obn/2) - 1;

	if ( obn != O2.bondNum() ) {
		std::cerr << "In calcCorr : Bonds of two operators do not match.\n";
		return corr;
	}
	if ( obn < 2 || obn > 4 ) {
		std::cerr << "In calcCorr : Unsupported operator.\n";
		return corr;
	}

	// build head
	if (loc1 == 0) {
		lv.assign( mpsIBC.getLambda(0).bond() );
		lv.identity();
	}
	else
		lv = lvecs[loc1-1];

	// build tail
	if (loc2 == L-1-skip) {
		rv.assign( mpsIBC.getLambda(L).bond() );
		rv.identity();
	}
	else
		rv = rvecs[loc2+1+skip];

	// contract O1
	if ( skip == 0 )
		ket = netLG( mpsIBC.getLambda(loc1), mpsIBC.getGamma(loc1) );
	else if ( skip == 1 )
		ket = netLGLG( mpsIBC.getLambda(loc1), mpsIBC.getGamma(loc1),
			mpsIBC.getLambda(loc1+1), mpsIBC.getGamma(loc1+1) );
	lv = buildLRVecQn( lv, ket, O1, true );

	// contract body
	for (int i = loc1+1+skip; i < loc2; ++i) {
		ket = netLG( mpsIBC.getLambda(i), mpsIBC.getGamma(i) );
		lv = buildLRVecQn( lv, ket, true );
	}

	// contract O2
	if ( skip == 0 )
		ket = netLGL( mpsIBC.getLambda(loc2), mpsIBC.getGamma(loc2), mpsIBC.getLambda(loc2+1) );
	else if ( skip == 1 )
		ket = netLGLGL( mpsIBC.getLambda(loc2), mpsIBC.getGamma(loc2),
			mpsIBC.getLambda(loc2+1), mpsIBC.getGamma(loc2+1), mpsIBC.getLambda(loc2+2) );
	lv = buildLRVecQn( lv, ket, O2, true );

	corr = contrLRVecQn( lv, rv );
	return corr;
}

//======================================

void corrOO( ChainQnIBC& mpsIBC, int L,
	uni10::CUniTensor& O1, uni10::CUniTensor& O2, uni10::CUniTensor& id,
	int offset_l, int offset_r, bool detail ) {

	/// calculate < O1(r1) O2(r2) > - < O(r1) > < O(r2) >
	const std::vector<uni10::CUniTensor>& gams = mpsIBC.getGamVec();
	const std::vector<uni10::CUniTensor>& lams = mpsIBC.getLamVec();
	std::vector<uni10::CUniTensor> lvecs = allLRVecsQn( gams, lams, true );
	std::vector<uni10::CUniTensor> rvecs = allLRVecsQn( gams, lams, false );

	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expl, expr, corT;
	uni10::CUniTensor norm = calcExpV( mpsIBC, id, 0, lvecs, rvecs );

	int mid = L/2 - 1;
	int r1, r2;
	int ibn1 = O1.inBondNum();
	int ibn2 = O2.inBondNum();

	int r = 0;
	while ( r < L/2 - std::max(std::max(offset_l, offset_r), ibn2-1) ) {

		r1 = mid-r-offset_l;
		r2 = mid+r+offset_r;
		expl = calcExpV( mpsIBC, O1, r1, lvecs, rvecs );
		expr = calcExpV( mpsIBC, O2, r2, lvecs, rvecs );
		corT = calcCorr( mpsIBC, O1, O2, r1, r2, lvecs, rvecs );

		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		corr = ( corT[0].real()/norm[0].real() );
		std::cout << std::fixed << std::setprecision(2)
			<< ((float)(r2-r1-(ibn1-1)-std::min(ibn1-1, ibn2-1)))/(std::min(ibn1, ibn2)) << '\t'
			<< std::scientific << std::setprecision(14)
			<< corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\n';

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//======================================

void corrOO_half( ChainQnIBC& mpsIBC, int L,
	uni10::CUniTensor& O1, uni10::CUniTensor& O2, uni10::CUniTensor& id,
	int offset_l, int offset_r, bool left, bool detail ) {

	/// calculate < O1(r1) O2(r2) > - < O1(r1) > < O2(r2) >
	const std::vector<uni10::CUniTensor>& gams = mpsIBC.getGamVec();
	const std::vector<uni10::CUniTensor>& lams = mpsIBC.getLamVec();
	std::vector<uni10::CUniTensor> lvecs = allLRVecsQn( gams, lams, true );
	std::vector<uni10::CUniTensor> rvecs = allLRVecsQn( gams, lams, false );

	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expl, expr, corT;
	uni10::CUniTensor norm = calcExpV( mpsIBC, id, 0, lvecs, rvecs );

	int mid = L/2 - 1;
	int r1, r2;
	int ibn1 = O1.inBondNum();
	int ibn2 = O2.inBondNum();

	int r = 1;
	while ( r < L/2 - std::max(std::max(offset_l, offset_r), ibn2-1) ) {

		if (left) {
			r1 = mid-r-offset_l;
			r2 = mid;
		}
		else {
			r1 = mid+1;
			r2 = mid+1+r+offset_r;
		}

		expl = calcExpV( mpsIBC, O1, r1, lvecs, rvecs );
		expr = calcExpV( mpsIBC, O2, r2, lvecs, rvecs );
		corT = calcCorr( mpsIBC, O1, O2, r1, r2, lvecs, rvecs );

		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		corr = ( corT[0].real()/norm[0].real() );

		std::cout << std::fixed << std::setprecision(2) << r2-r1 << '\t'
			<< std::scientific << std::setprecision(14)
			<< corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\n';

		if (!detail) {
			if (r > 299)
				r += 20;
			else if (r > 99)
				r += 10;
			else if (r > 29)
				r += 2;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 6) {

		std::cerr << "Not Enough Arguments!" << std::endl;
		std::cerr << "Usage: " << argv[0]
			<< " <lat_size> <virt_dim> <op1_file> <op2_file> <mps_dir> [<offset_l> <offset_r>] [options]" << std::endl;
		return 1;
	}

	// initialize IBC chain
	int L, X;
	std::istringstream(argv[1]) >> L;
	std::istringstream(argv[2]) >> X;

	std::string op1f, op2f;
	op1f = std::string(argv[3]);
	op2f = std::string(argv[4]);

	std::string wf_dir = "mps-inf";
	wf_dir = std::string( argv[5] );

	bool default_offset = true;
	int offset_l, offset_r;
	bool half = false;
	if (argc > 7) {
		default_offset = false;
		std::istringstream(argv[6]) >> offset_l;
		std::istringstream(argv[7]) >> offset_r;
	}
	else if (argc > 6 && std::string(argv[6]) == "-half")
		half = true;

	std::vector<uni10::Qnum> qphys = uni10::CUniTensor( wf_dir + "/gamma_0" ).bond()[1].Qlist();
	ChainQnIBC mpsIBC(L, X, qphys);
	mpsIBC.importMPS( wf_dir );

	// import operators
	uni10::CUniTensor op1 = uni10::CUniTensor(op1f);
	uni10::CUniTensor op2 = uni10::CUniTensor(op2f);

	std::vector<uni10::Bond> bdi;
	bdi.push_back( uni10::Bond(uni10::BD_IN, qphys) );
	bdi.push_back( uni10::Bond(uni10::BD_OUT, qphys) );
	uni10::CUniTensor id(bdi);
	id.identity();

	if (half) {
		std::cout << "#r\t<" << op1f << "(mid) " << op2f << "(r2)>\t<" << op1f << "(mid) " << op2f << "(r2)> - <"
			<< op1f << "(mid)> <" << op2f << "(r2)>\t<" << op1f << "(mid)>\t<" << op2f << "(r2)>\n";
		if (default_offset) {
			if ( op1.inBondNum() == 2 || op2.inBondNum() == 2 ) {
				std::cerr << "Operator not supported." << '\n';
				return 1;
			}
			else {
				offset_l = 0;
				offset_r = 0;
			}
		}
		corrOO_half( mpsIBC, L, op1, op2, id, offset_l, offset_r, false, true );
	}
	else {
		std::cout << "#r\t<" << op1f << "(r1) " << op2f << "(r2)>\t<" << op1f << "(r1) " << op2f << "(r2)> - <"
			<< op1f << "(r1)> <" << op2f << "(r2)>\t<" << op1f << "(r1)>\t<" << op2f << "(r2)>\n";
		if (default_offset) {
			if ( op1.inBondNum() == 2 && op2.inBondNum() == 2 ) {
				offset_l = 1;
				offset_r = 1;
			}
			else {
				offset_l = 0;
				offset_r = 1;
			}
		}
		corrOO( mpsIBC, L, op1, op2, id, offset_l, offset_r, true );
	}

	return 0;
}
