#include <iostream>
#include <sstream>

#include <mps/ChainInf.h>
#include <tns-func/func_net.h>

//=========================================

void corrFnInf1S( ChainInf mps,
	uni10::CUniTensor op1, uni10::CUniTensor op2, uni10::CUniTensor id,
	int r_max ) {

	/// calculate < op1(0) op2(r) > - < op1(0) > < op2(r) >
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expv, expl, expr, norm;

	uni10::CUniTensor ga = mps.getGamma(0);
	uni10::CUniTensor gb = mps.getGamma(1);
	uni10::CUniTensor la = mps.getLambda(0);
	uni10::CUniTensor lb = mps.getLambda(1);

	uni10::CUniTensor ket_a = netLG(la, ga);
	uni10::CUniTensor bra_a = ket_a;
	bra_a.conj();
	uni10::CUniTensor ket_b = netLG(lb, gb);
	uni10::CUniTensor bra_b = ket_b;
	bra_b.conj();

	int lab_bra[] = {0, 200, 1};
	int lab_ket[] = {0, 100, 2};
	int lab_op[]  = {200, 100};
	ket_a.setLabel(lab_ket);
	bra_a.setLabel(lab_bra);
	ket_b.setLabel(lab_ket);
	bra_b.setLabel(lab_bra);
	op1.setLabel(lab_op);
	id.setLabel(lab_op);

	uni10::CUniTensor head_op = uni10::contract(op1, ket_a, false);
	head_op = uni10::contract(bra_a, head_op, false);
	head_op.permute(0);

	uni10::CUniTensor head_id = uni10::contract(id, ket_a, false);
	head_id = uni10::contract(bra_a, head_id, false);
	head_id.permute(0);

	int lab_last[2];
	lab_last[0] = 10;
	lab_last[1] = 11;

	uni10::CUniTensor last_ket_a = netLGL(la, ga, lb);
	uni10::CUniTensor last_bra_a = last_ket_a;
	last_bra_a.permute(1).conj();
	uni10::CUniTensor last_ket_b = netLGL(lb, gb, la);
	uni10::CUniTensor last_bra_b = last_ket_b;
	last_bra_b.permute(1).conj();

	int lab_last_bra[] = {10, 200, 12};
	int lab_last_ket[] = {11, 100, 12};
	int lab_op2[] = {200, 100};
	last_ket_a.setLabel(lab_last_ket);
	last_bra_a.setLabel(lab_last_bra);
	last_ket_b.setLabel(lab_last_ket);
	last_bra_b.setLabel(lab_last_bra);
	op2.setLabel(lab_op2);
	id.setLabel(lab_op2);

	uni10::CUniTensor tail_a_op = uni10::contract(op2, last_ket_a, false);
	tail_a_op = uni10::contract(last_bra_a, tail_a_op, false);
	tail_a_op.permute(2);
	uni10::CUniTensor tail_b_op = uni10::contract(op2, last_ket_b, false);
	tail_b_op = uni10::contract(last_bra_b, tail_b_op, false);
	tail_b_op.permute(2);

	uni10::CUniTensor tail_a_id = uni10::contract(id, last_ket_a, false);
	tail_a_id = uni10::contract(last_bra_a, tail_a_id, false);
	tail_a_id.permute(2);
	uni10::CUniTensor tail_b_id = uni10::contract(id, last_ket_b, false);
	tail_b_id = uni10::contract(last_bra_b, tail_b_id, false);
	tail_b_id.permute(2);

	tail_a_op.setLabel( lab_last );
	tail_a_id.setLabel( lab_last );
	tail_b_op.setLabel( lab_last );
	tail_b_id.setLabel( lab_last );


	int r = 1, r_head = 1;
	bool odd;

	while ( r <= r_max ) {

		for (int i = r_head; i < r; ++i) {

			lab_last[0] = 1;
			lab_last[1] = 2;
			head_op.setLabel( lab_last );
			head_id.setLabel( lab_last );

			int lab_bra[] = {1, 100, 10};
			int lab_ket[] = {2, 100, 11};

			if (i%2) {
				bra_b.setLabel(lab_bra);
				ket_b.setLabel(lab_ket);

				head_op = uni10::contract(head_op, bra_b, false);
				head_op = uni10::contract(head_op, ket_b, false);
				head_op.permute(0);
				head_id = uni10::contract(head_id, bra_b, false);
				head_id = uni10::contract(head_id, ket_b, false);
				head_id.permute(0);
			}
			else {
				bra_a.setLabel(lab_bra);
				ket_a.setLabel(lab_ket);

				head_op = uni10::contract(head_op, bra_a, false);
				head_op = uni10::contract(head_op, ket_a, false);
				head_op.permute(0);
				head_id = uni10::contract(head_id, bra_a, false);
				head_id = uni10::contract(head_id, ket_a, false);
				head_id.permute(0);
			}
		}

		lab_last[0] = 10;
		lab_last[1] = 11;
		head_op.setLabel( lab_last );
		head_id.setLabel( lab_last );

		odd = (bool)(r%2);
		if (odd) {
			expv = uni10::contract(head_op, tail_b_op, false);
			expl = uni10::contract(head_op, tail_b_id, false);
			expr = uni10::contract(head_id, tail_b_op, false);
			norm = uni10::contract(head_id, tail_b_id, false);
		}
		else {
			expv = uni10::contract(head_op, tail_a_op, false);
			expl = uni10::contract(head_op, tail_a_id, false);
			expr = uni10::contract(head_id, tail_a_op, false);
			norm = uni10::contract(head_id, tail_a_id, false);
		}

		corr = expv[0].real()/norm[0].real();
		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		std::cout << r << '\t' << std::scientific << std::setprecision(14)
			<< corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\n';

		r_head = r;
		r += 1;
	}
}

//=========================================

int main( int argc, char* argv[] ) {

	if (argc < 4) {

		std::cerr << "Not Enough Arguments!" << std::endl;
		std::cerr << "Usage: " << argv[0] << " <op1_file> <op2_file> <distance> [option]" << std::endl;
		std::cerr << "Allowed options:" << std::endl;
		return 1;
	}

	std::string wf_dir = "mps-inf";
	std::string op1f, op2f;
	op1f = std::string(argv[1]);
	op2f = std::string(argv[2]);
	int r_max;
	std::istringstream(argv[3]) >> r_max;

	if ( argc > 4 ) {
		for (int i = 4; i < argc; ++i) {
			//
			continue;
		}
	}

	ChainInf mps(2, 5);
	mps.importMPS( wf_dir, 2, false, false );
	int d = mps.getGamma(0).bond()[1].dim();
	int X = mps.getGamma(0).bond()[0].dim();
	mps.setPhysD(d);
	mps.setMaxBD(X);

	uni10::CUniTensor op1( op1f );
	uni10::CUniTensor op2( op2f );

	std::vector< uni10::Bond > bdi;
	bdi.push_back( uni10::Bond( uni10::BD_IN, d ) );
	bdi.push_back( uni10::Bond( uni10::BD_OUT, d ) );
	uni10::CUniTensor id( bdi );
	id.identity();

	if ( op1.bondNum() > 2 || op2.bondNum() > 2 ) {
		std::cerr << "corrFnInf1S error: incorrect bond structure.";
		return 1;
	}

	std::cout << "#r" << '\t' << "< "+op1f+"(0) "+op2f+"(r) >" << '\t'
		<< "< "+op1f+"(0) "+op2f+"(r) > - < "+op1f+"(0) > < "+op2f+"(r) >" << '\t'
		<< "< "+op1f+"(0) >" << '\t' << "< "+op2f+"(r) >" << '\n';

	corrFnInf1S( mps, op1, op2, id, r_max );

	mps.clear();

	return 0;
}
