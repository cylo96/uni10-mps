#include <iostream>
#include <sstream>

#include <mps/ChainInf.h>
#include <tns-func/func_net.h>

//=========================================

void corrFnInf( ChainInf mps,
	uni10::CUniTensor op1, uni10::CUniTensor op2, uni10::CUniTensor id,
	int r_max, bool detail ) {

	/// calculate < op1(0) op2(r) > - < op1(0) > < op2(r) >
	double corr = 0.0;
	double base = 0.0;
	double exp1 = 0.0, exp2 = 0.0;
	uni10::CUniTensor expv, expl, expr, norm;

	uni10::CUniTensor ga = mps.getGamma(0);
	uni10::CUniTensor gb = mps.getGamma(1);
	uni10::CUniTensor la = mps.getLambda(0);
	uni10::CUniTensor lb = mps.getLambda(1);

	uni10::CUniTensor ket = netLGLG(la, ga, lb, gb);
	uni10::CUniTensor bra = ket;
	bra.conj();

	if ( op1.bondNum() == 2 ) {
		int lab_bra[] = {0, 200, 101, 1};
		int lab_ket[] = {0, 100, 101, 2};
		int lab_op[]  = {200, 100};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);
		op1.setLabel(lab_op);
		id.setLabel(lab_op);
	}
	else if ( op1.bondNum() == 4 ) {
		id = uni10::otimes(id, id);
		int lab_bra[] = {0, 200, 201, 1};
		int lab_ket[] = {0, 100, 101, 2};
		int lab_op[]  = {200, 201, 100, 101};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);
		op1.setLabel(lab_op);
		id.setLabel(lab_op);
	}

	uni10::CUniTensor head_op = uni10::contract(op1, ket, false);
	head_op = uni10::contract(bra, head_op, false);
	head_op.permute(0);

	uni10::CUniTensor head_id = uni10::contract(id, ket, false);
	head_id = uni10::contract(bra, head_id, false);
	head_id.permute(0);

	int lab_last[2];
	lab_last[0] = 10;
	lab_last[1] = 11;

	uni10::CUniTensor last_ket = netLGLGL(la, ga, lb, gb, la);
	uni10::CUniTensor last_bra = last_ket;
	last_bra.permute(1).conj();

	if ( op2.bondNum() == 2 ) {
		int lab_last_bra[] = {10, 200, 101, 12};
		int lab_last_ket[] = {11, 100, 101, 12};
		int lab_op2[] = {200, 100};
		last_ket.setLabel(lab_last_ket);
		last_bra.setLabel(lab_last_bra);
		op2.setLabel(lab_op2);
		id.setLabel(lab_op2);
	}
	else if ( op2.bondNum() == 4 ) {
		int lab_last_bra[] = {10, 200, 201, 12};
		int lab_last_ket[] = {11, 100, 101, 12};
		int lab_op2[] = {200, 201, 100, 101};
		last_ket.setLabel(lab_last_ket);
		last_bra.setLabel(lab_last_bra);
		op2.setLabel(lab_op2);
		id.setLabel(lab_op2);
	}

	uni10::CUniTensor tail_op = uni10::contract(op2, last_ket, false);
	tail_op = uni10::contract(last_bra, tail_op, false);
	tail_op.permute(2);

	uni10::CUniTensor tail_id = uni10::contract(id, last_ket, false);
	tail_id = uni10::contract(last_bra, tail_id, false);
	tail_id.permute(2);

	tail_op.setLabel( lab_last );
	tail_id.setLabel( lab_last );


	int r = 1, r_head = 1;
	while ( r <= r_max ) {

		for (int i = r_head; i < r; ++i) {

			lab_last[0] = 1;
			lab_last[1] = 2;
			head_op.setLabel( lab_last );
			head_id.setLabel( lab_last );

			int lab_bra[] = {1, 100, 101, 10};
			int lab_ket[] = {2, 100, 101, 11};
			bra.setLabel(lab_bra);
			ket.setLabel(lab_ket);

			head_op = uni10::contract(head_op, bra, false);
			head_op = uni10::contract(head_op, ket, false);
			head_op.permute(0);
			head_id = uni10::contract(head_id, bra, false);
			head_id = uni10::contract(head_id, ket, false);
			head_id.permute(0);
		}

		lab_last[0] = 10;
		lab_last[1] = 11;
		head_op.setLabel( lab_last );
		head_id.setLabel( lab_last );
		expv = uni10::contract(head_op, tail_op, false);
		expl = uni10::contract(head_op, tail_id, false);
		expr = uni10::contract(head_id, tail_op, false);
		norm = uni10::contract(head_id, tail_id, false);

		corr = expv[0].real()/norm[0].real();
		exp1 = expl[0].real()/norm[0].real();
		exp2 = expr[0].real()/norm[0].real();
		base = ( exp1 * exp2 );
		std::cout << 2*r << '\t' << std::scientific << std::setprecision(14)
			<< corr << '\t' << corr-base << '\t' << exp1 << '\t' << exp2 << '\n';

		r_head = r;

		if (!detail) {
			if (r > 9999)
				r += 2000;
			else if (r > 999)
				r += 200;
			else if (r > 99)
				r += 20;
			else
				r += 1;
		}
		else
			r += 1;
	}
}

//=========================================

int main( int argc, char* argv[] ) {

	if (argc < 4) {

		std::cerr << "Not Enough Arguments!" << std::endl;
		std::cerr << "Usage: " << argv[0] << " <op1_file> <op2_file> <distance> [option]" << std::endl;
		std::cerr << "Allowed options:" << std::endl;
		std::cerr << "-a  calculate All available distances." << std::endl;
		return 1;
	}

	bool detail = false;

	std::string wf_dir = "mps-inf";
	std::string op1f, op2f;
	op1f = std::string(argv[1]);
	op2f = std::string(argv[2]);
	int r_max;
	std::istringstream(argv[3]) >> r_max;

	if ( argc > 4 ) {
		for (int i = 4; i < argc; ++i) {
			if ( std::string(argv[i]) == "-a" )
				detail = true;
		}
	}

	ChainInf mps(2, 5);
	mps.importMPS( wf_dir, 2, false, false );
	int d = mps.getGamma(0).bond()[1].dim();
	int X = mps.getGamma(0).bond()[0].dim();
	mps.setPhysD(d);
	mps.setMaxBD(X);

	uni10::CUniTensor op1( op1f );
	uni10::CUniTensor op2( op2f );

	std::vector< uni10::Bond > bdi;
	bdi.push_back( uni10::Bond( uni10::BD_IN, d ) );
	bdi.push_back( uni10::Bond( uni10::BD_OUT, d ) );
	uni10::CUniTensor id( bdi );
	id.identity();

	std::cout << "#r" << '\t' << "< "+op1f+"(0) "+op2f+"(r) >" << '\t'
		<< "< "+op1f+"(0) "+op2f+"(r) > - < "+op1f+"(0) > < "+op2f+"(r) >" << '\t'
		<< "< "+op1f+"(0) >" << '\t' << "< "+op2f+"(r) >" << '\n';

	corrFnInf( mps, op1, op2, id, r_max/2, detail );

	mps.clear();

	return 0;
}
