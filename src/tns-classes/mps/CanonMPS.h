#ifndef CANONMPS_H
#define CANONMPS_H
#include <string>
#include <uni10.hpp>
#include <tns-func/tns_const.h>

class CanonMPS {

public:
	/// constructor
	CanonMPS(int L, int d, int X);
	/// destructor
	~CanonMPS();

	void setSize(int L);
	void setPhysD(int d);
	void setMaxBD(int X);
	void setSchValCutoff(double cutoff);
	int getSize();
	int getPhysD();
	int getMaxBD();

	uni10::CUniTensor getGamma(int idx);
	uni10::CUniTensor getLambda(int idx);
	void putGamma(uni10::CUniTensor gam, int idx = -1);
	void putLambda(uni10::CUniTensor lam, int idx = -1);
	const std::vector<uni10::CUniTensor>& getGamVec() const;
	const std::vector<uni10::CUniTensor>& getLamVec() const;

	void exportGamma(int idx, const std::string& fname = "");
	void exportLambda(int idx, const std::string& fname = "");
	void importGamma(int idx, const std::string& fname, bool fit_chi = true);
	void importLambda(int idx, const std::string& fname, bool fit_chi = true);
	void exportMPS( std::string dirname );
	void importMPS( std::string dirname, bool end_lam = false );
	void importMPS( std::string dirname, int unit_cell, bool end_lam, bool fit_chi = false );
	void importMPS( std::vector<uni10::CUniTensor>& gams, std::vector<uni10::CUniTensor>& lams );

	void oneSiteOP( uni10::CUniTensor op, int idx = 0 );

	void mps2SiteSVD( uni10::CUniTensor& psi,
		uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
		uni10::CUniTensor& lam1, uni10::CUniTensor& gam1, uni10::CUniTensor& lam2,
		bool show_err = false );
	double mps2SiteSVD( uni10::CUniTensor& psi,
		uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
		uni10::CUniTensor& lam1, uni10::CUniTensor& gam1, uni10::CUniTensor& lam2,
		bool show_err, bool dummy );
	void mps3SiteSVD( uni10::CUniTensor& psi,
		uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
		uni10::CUniTensor& lam1, uni10::CUniTensor& gam1,
		uni10::CUniTensor& lam2, uni10::CUniTensor& gam2,
		uni10::CUniTensor& lam3, bool left_to_right );

	void slice(int length, int start = 0);
	void clear();

protected:
	int lat_size;
	int dim_phys;
	int chi_max;
	double sch_val_cutoff;
	std::vector<uni10::CUniTensor> gamma;
	std::vector<uni10::CUniTensor> lambda;

	void vecSlice( std::vector<uni10::CUniTensor>& vec, int length, int start = 0);
	uni10::CUniTensor initGamma(int chi1, int chi2, int d);
	uni10::CUniTensor initLambda(int chi);
	uni10::CUniTensor resizeGamma( uni10::CUniTensor gam0, int chi_l, int chi_r );
	uni10::CUniTensor resizeLambda( uni10::CUniTensor lam0, int chi_new );

	uni10::CUniTensor initNSite(int N, int chi1, int chi2, int d);
	uni10::CUniTensor initNSite(int N, int chi1, int chi2, int* d);

	std::vector<monitor> availMonitors( std::vector<monitor> monitors, std::vector<std::string> avil_list );
};

#endif
