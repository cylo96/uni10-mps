#ifndef CHAINOBC_H
#define CHAINOBC_H
#include "CanonMPS.h"

class ChainOBC: public CanonMPS {

public:
	/// constructor
	ChainOBC(int L, int d, int X);

	void randomize();

	void importMPS( std::string dirname, int unit_cell = 2 );

	void tebd( uni10::CUniTensor ham, uni10::CUniTensor hbl, uni10::CUniTensor hbr,
		Complex dt, int steps, int orderTS = 1 );

	void dmrg( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgU2( std::string mpo_dir, int sweeps, int iter_max, double tolerance, double trunc_err_lim, bool verbose = false );
	void dmrgU2( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose = false );

	void dmrgImp( std::vector<uni10::CUniTensor>& mpo,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgImpU2( std::vector<uni10::CUniTensor>& mpo,
		int sweeps, int iter_max, double tolerance, double trunc_err_lim, bool verbose = false );
	void dmrgImpU2( std::vector<uni10::CUniTensor>& mpo,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgImpU3( std::vector<uni10::CUniTensor>& mpo,
		int sweeps, int iter_max, double tolerance, bool verbose = false );

	uni10::CUniTensor expVal( uni10::CUniTensor op, int idx, bool constBD = false );
	uni10::CUniTensor correlation( uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 );

private:
	uni10::CUniTensor dummy;

};

#endif
