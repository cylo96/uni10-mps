#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <time.h>
#include <sys/stat.h>

#include <mps/CanonMPS.h>
#include <tns-func/func_evol.h>
#include <tns-func/func_net.h>
#include <tns-func/func_op.h>

//======================================

CanonMPS::CanonMPS(int L, int d, int X) {
	/// object constructor
	lat_size = L;	// lattice size
	dim_phys = d;	// physical dimension
	chi_max = X;	// maximum bond dimension
	sch_val_cutoff = 1e-10;
}

//======================================

CanonMPS::~CanonMPS() {
	/// object destructor
	clear();
}

//======================================

void CanonMPS::setSize(int L) {
	/// set lattice size
	// todo: if L < current lat_size => do slice
	//if ( L < lat_size )
	//	slice(L, 0);

	lat_size = L;
}

void CanonMPS::setPhysD(int d) {
	/// set physical dimension
	dim_phys = d;
}

void CanonMPS::setMaxBD(int X) {
	/// set maximum bond dimension
	// todo: if X < current chi_max => do truncation
	chi_max = X;
}

void CanonMPS::setSchValCutoff(double cutoff) {
	/// set minimal Schmidt Value for SVD 
	sch_val_cutoff = cutoff;
}

//======================================

int CanonMPS::getSize() {
	///
	return lat_size;
}

int CanonMPS::getPhysD() {
	///
	return dim_phys;
}

int CanonMPS::getMaxBD() {
	///
	return chi_max;
}

//======================================

void CanonMPS::clear() {
	/// clear tensors in current MPS; keep size and bond dim settings
	gamma.clear();
	lambda.clear();
}

//======================================

void CanonMPS::vecSlice( std::vector<uni10::CUniTensor>& vec, int length, int start ) {
	/// slice vec into desired chunk
	// todo: assert start >= 0
	if (start == 0) {
		vec.resize(length);
	}
	else {
		vec.erase( vec.begin(), vec.begin()+start );
		vec.erase( vec.begin()+length, vec.end() );
	}
}

//======================================

void CanonMPS::slice(int length, int start) {
	/// slice MPS into desired chunk
	// todo: assert start >= 0
	vecSlice( gamma, length, start );
	vecSlice( lambda, length, start );
}

//======================================

uni10::CUniTensor CanonMPS::getGamma(int idx) {
	/// return the idx'th gamma
	return gamma[idx];
	// todo: raise exception if idx out of range
}

uni10::CUniTensor CanonMPS::getLambda(int idx) {
	/// return the idx'th lambda
	return lambda[idx];
	// todo: raise exception if idx out of range
}

//======================================

const std::vector<uni10::CUniTensor>& CanonMPS::getGamVec() const {
	/// return the reference of gamma vector
	return gamma;
}

const std::vector<uni10::CUniTensor>& CanonMPS::getLamVec() const {
	/// return the reference of gamma vector
	return lambda;
}

//======================================

uni10::CUniTensor CanonMPS::initGamma(int chi1, int chi2, int d) {
	/// initialize a gamma tensor
	std::vector<uni10::Bond> bond_gam;
	bond_gam.push_back( uni10::Bond(uni10::BD_IN, chi1) );
	bond_gam.push_back( uni10::Bond(uni10::BD_IN, d) );
	bond_gam.push_back( uni10::Bond(uni10::BD_OUT, chi2) );

	uni10::CUniTensor gam(bond_gam);

	return gam;
}

//======================================

uni10::CUniTensor CanonMPS::initLambda(int chi) {
	/// initialize a lambda tensor
	std::vector<uni10::Bond> bond_lam;
	bond_lam.push_back( uni10::Bond(uni10::BD_IN, chi) );
	bond_lam.push_back( uni10::Bond(uni10::BD_OUT, chi) );

	uni10::CUniTensor lam(bond_lam);

	return lam;
}

//======================================

uni10::CUniTensor CanonMPS::resizeGamma( uni10::CUniTensor gam0, int chi_l, int chi_r ) {
	/// resize a gamma tensor
	std::vector<uni10::Bond> bd = gam0.bond();
	int chi1 = bd[0].dim();
	int d    = bd[1].dim();
	int chi2 = bd[2].dim();

	uni10::CUniTensor gam;

	if ( chi_l == chi1 && chi_r == chi2 ) {
		gam = gam0;
	}
	else {
		int dim1 = std::min(chi1, chi_l);
		int dim2 = std::min(chi2, chi_r);
		gam = initGamma( chi_l, chi_r, d );

		uni10::CMatrix gam0_blk = gam0.getBlock();
		uni10::CMatrix gam_blk = gam.getBlock();

		for (int i = 0; i < dim1 * d; ++i)
			for (int j = 0; j < dim2; ++j)
				gam_blk.at(i, j) = gam0_blk.at(i, j);

		gam.putBlock( gam_blk );
	}

	return gam;
}

//======================================

uni10::CUniTensor CanonMPS::resizeLambda( uni10::CUniTensor lam0, int chi_new ) {
	/// resize a lambda tensor
	int chi = lam0.bond()[0].dim();
	uni10::CUniTensor lam;

	if ( chi_new == chi ) {
		lam = lam0;
	}
	else {
		int dim = std::min(chi, chi_new);
		lam = initLambda( chi_new );

		uni10::CMatrix lam0_blk = lam0.getBlock();
		uni10::CMatrix lam_blk = lam.getBlock();

		for (int i = 0; i < dim; ++i)
			lam_blk.at(i, i) = lam0_blk.at(i, i);

		lam.putBlock( lam_blk );
	}

	return lam;
}

//======================================

uni10::CUniTensor CanonMPS::initNSite(int N, int chi1, int chi2, int d) {
	/// initialize a N-site unit cell with 2 virt bonds and N phys bonds
	std::vector<uni10::Bond> bond_cell;
	bond_cell.push_back( uni10::Bond(uni10::BD_IN, chi1) );
	for (int i = 0; i < N; ++i)
		bond_cell.push_back( uni10::Bond(uni10::BD_IN, d) );
	bond_cell.push_back( uni10::Bond(uni10::BD_OUT, chi2) );

	uni10::CUniTensor cell(bond_cell);

	return cell;
}

//======================================

uni10::CUniTensor CanonMPS::initNSite(int N, int chi1, int chi2, int* d) {
	/// initialize a N-site unit cell with 2 virt bonds and N phys bonds
	std::vector<uni10::Bond> bond_cell;
	bond_cell.push_back( uni10::Bond(uni10::BD_IN, chi1) );
	for (int i = 0; i < N; ++i)
		bond_cell.push_back( uni10::Bond(uni10::BD_IN, d[i]) );
	bond_cell.push_back( uni10::Bond(uni10::BD_OUT, chi2) );

	uni10::CUniTensor cell(bond_cell);

	return cell;
}

//======================================

void CanonMPS::putGamma(uni10::CUniTensor gam, int idx) {
	/// put a gamma tensor at gamma[idx]
	// todo: assert -1 <= idx < gamma.size();
	if (idx == -1)
		gamma.push_back( gam );
	else
		gamma[idx] = gam;
}

//======================================

void CanonMPS::putLambda(uni10::CUniTensor lam, int idx) {
	/// put a lambda tensor at lambda[idx]
	// todo: assert -1 <= idx < lambda.size();
	if (idx == -1)
		lambda.push_back( lam );
	else
		lambda[idx] = lam;
}

//======================================

void CanonMPS::exportGamma(int idx, const std::string& fname) {
	/// export a gamma tensor to file
	if ( fname == "" )
		gamma[idx].save( "gamma_" + std::to_string((long long)idx) );
	else
		gamma[idx].save( fname );
}

//======================================

void CanonMPS::exportLambda(int idx, const std::string& fname) {
	/// export a gamma tensor to file
	if ( fname == "" )
		lambda[idx].save( "lambda_" + std::to_string((long long)idx) );
	else
		lambda[idx].save( fname );
}

//======================================

void CanonMPS::importGamma(int idx, const std::string &fname, bool fit_chi) {
	/// import a gamma tensor from file. decide to fit chi_max or not
	uni10::CUniTensor gam0( fname );

	std::vector<uni10::Bond> bd = gam0.bond();
	int chi1 = bd[0].dim();
	int d    = bd[1].dim();
	int chi2 = bd[2].dim();

	if ( fit_chi && chi_max < std::max(chi1, chi2) ) {
		/// truncate bond dimension to fit chi_max
		int dim1 = std::min(chi1, chi_max);
		int dim2 = std::min(chi2, chi_max);
		uni10::CUniTensor gam = initGamma( dim1, dim2, d );

		uni10::CMatrix gam0_blk = gam0.getBlock();
		uni10::CMatrix gam_blk = gam.getBlock();

		for (int i = 0; i < dim1 * d; ++i)
			for (int j = 0; j < dim2; ++j)
				gam_blk.at(i, j) = gam0_blk.at(i, j);

		gam.putBlock( gam_blk );
		putGamma( gam, idx );
	}
	else {
		putGamma( gam0, idx );
	}
}

//======================================

void CanonMPS::importLambda(int idx, const std::string &fname, bool fit_chi) {
	/// import a lambda tensor from file. decide to fit chi_max or not
	uni10::CUniTensor lam0( fname );

	int chi = lam0.bond()[0].dim();

	if ( fit_chi && chi_max < chi ) {
		/// truncate bond dimension to fit chi_max
		uni10::CUniTensor lam = initLambda( chi_max );

		uni10::CMatrix lam0_blk = lam0.getBlock();
		uni10::CMatrix lam_blk = lam.getBlock();

		for (int i = 0; i < chi_max; ++i)
			lam_blk.at(i, i) = lam0_blk.at(i, i);

		lam.putBlock( lam_blk );
		putLambda( lam, idx );
	}
	else {
		putLambda( lam0, idx );
	}
}

//======================================

void CanonMPS::exportMPS( std::string dirname ) {
	/// export MPS to a directory
	// http://stackoverflow.com/questions/3828192/checking-if-a-directory-exists-in-unix-system-call
	// http://pubs.opengroup.org/onlinepubs/009695399/basedefs/sys/stat.h.html
	struct stat info;

	if ( stat( dirname.c_str(), &info ) != 0 )
		mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

	std::string fname;
	for (int i = 0; i < gamma.size(); ++i) {
		fname = dirname + "/gamma_" + std::to_string((long long)i);
		gamma[i].save( fname );
	}
	for (int i = 0; i < lambda.size(); ++i) {
		fname = dirname + "/lambda_" + std::to_string((long long)i);
		lambda[i].save( fname );
	}
}

//======================================

void CanonMPS::importMPS( std::string dirname, int unit_cell, bool end_lam, bool fit_chi ) {
	/// import MPS from a directory
	// todo: raise exception if num of gam/lam files in folder < lat_size
	struct stat info;

	if ( stat( dirname.c_str(), &info ) == 0 && S_ISDIR(info.st_mode) ) {

		gamma.clear();
		lambda.clear();
		std::string fname;
		int last = 0;

		for (int i = 0; i < lat_size; ++i) {

			try {
				fname = dirname + "/gamma_" + std::to_string((long long)i);
				importGamma( -1, fname, fit_chi );
				last = i;
			}
			catch( const std::logic_error& e ) {
				if ( i > unit_cell - 1 ) {
					fname = dirname + "/gamma_" + std::to_string((long long) last-unit_cell+1+(i%unit_cell) );
					importGamma( -1, fname, fit_chi );
				}
			}
		}
		for (int i = 0; i < lat_size + (int)end_lam; ++i) {

			try {
				fname = dirname + "/lambda_" + std::to_string((long long)i);
				importLambda( -1, fname, fit_chi );
			}
			catch( const std::logic_error& e ) {
				if ( i > unit_cell - 1 ) {
					fname = dirname + "/lambda_" + std::to_string((long long) last-unit_cell+1+(i%unit_cell) );
					importLambda( -1, fname, fit_chi );
				}
			}
		}
	}
	else {
		std::cerr << "In CanonMPS::importMPS : path name error" << '\n';
		throw std::invalid_argument( "path name error" );
	}
}

//======================================

void CanonMPS::importMPS( std::string dirname, bool end_lam ) {
	/// import MPS from a directory
	importMPS( dirname, 2, end_lam, false );
}

//======================================

void CanonMPS::importMPS( std::vector<uni10::CUniTensor>& gams, std::vector<uni10::CUniTensor>& lams ) {
	/// import MPS from a vecor of tensors
	// assert length of gams/lams meets lat_size
	if ( lat_size == (int)gams.size() ) {
		gamma.clear();
		lambda.clear();
		for (int i = 0; i < (int)gams.size(); ++i)
			putGamma( gams[i] );
		for (int i = 0; i < (int)lams.size(); ++i)
			putLambda( lams[i] );
	}
	else {
		std::cerr << "In CanonMPS::importMPS: lattice size doesn't match." << '\n';
	}
}

//======================================

void CanonMPS::oneSiteOP( uni10::CUniTensor op, int idx ) {
	/// Act one-site operator on a specific site

	// todo: assert op is one-site operator
	int lab_gam[] = {0, 100, 1};
	int lab_op[] = {200, 100};
	int lab_fin[] = {0, 200, 1};

	gamma[idx].setLabel( lab_gam );
	op.setLabel( lab_op );
	gamma[idx] = uni10::contract( op, gamma[idx] );
	gamma[idx].permute( lab_fin, 2 );
}

//======================================

void CanonMPS::mps2SiteSVD( uni10::CUniTensor& psi,
	uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1, uni10::CUniTensor& lam2,
	bool show_err ) {
	///
	psi.permute( psi.label(), psi.bondNum()/2 );
	std::vector<uni10::CMatrix> svda = psi.getBlock().svd();
	double svsq_sum = svda[1].norm();

	std::vector<double> sch_vals;
	for (int i = 0; i < svda[1].col(); ++i) {
		double sch_val = svda[1][i].real();
		if ( sch_val > sch_val_cutoff )
			sch_vals.push_back( sch_val );
	}
	
	int dim_l = gam0.bond()[0].dim();
	int dim_m = std::min( (int)sch_vals.size(), chi_max );
	int dim_r = gam1.bond()[2].dim();

	if ( dim_m != gam0.bond()[2].dim() )
		gam0 = initGamma(dim_l, dim_m, dim_phys );
	if ( dim_m != lam1.bond()[0].dim() )
		lam1 = initLambda(dim_m);
	if ( dim_m != gam1.bond()[0].dim() )
		gam1 = initGamma(dim_m, dim_r, dim_phys );

	gam0.putBlock( svda[0].resize( dim_l * dim_phys, dim_m ) );
	gam0 = extractGamma2( gam0, lam0 );

	svda[1].resize(dim_m, dim_m);
	if (show_err) {
		double trunc_err = (svsq_sum - svda[1].norm()) / svsq_sum;
		std::cout << std::setprecision(12) << trunc_err << '\t';
	}
	svda[1] *= ( 1.0 / svda[1].norm() );
	lam1.putBlock( svda[1] );

	gam1.permute( gam1.label(), 1 );
	gam1.putBlock( svda[2].resize( dim_m, dim_phys * dim_r ) );
	gam1.permute( gam1.label(), 2 );
	gam1 = extractGamma( gam1, lam2 );
}

//======================================

double CanonMPS::mps2SiteSVD( uni10::CUniTensor& psi,
	uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1, uni10::CUniTensor& lam2,
	bool show_err, bool dummy ) {
	/// dummy just to avoid ambiguous functional overload
	psi.permute( psi.label(), psi.bondNum()/2 );
	std::vector<uni10::CMatrix> svda = psi.getBlock().svd();
	double svsq_sum = svda[1].norm();

	std::vector<double> sch_vals;
	for (int i = 0; i < svda[1].col(); ++i) {
		double sch_val = svda[1][i].real();
		if ( sch_val > sch_val_cutoff )
			sch_vals.push_back( sch_val );
	}
	
	int dim_l = gam0.bond()[0].dim();
	int dim_m = std::min( (int)sch_vals.size(), chi_max );
	int dim_r = gam1.bond()[2].dim();

	if ( dim_m != gam0.bond()[2].dim() )
		gam0 = initGamma(dim_l, dim_m, dim_phys );
	if ( dim_m != lam1.bond()[0].dim() )
		lam1 = initLambda(dim_m);
	if ( dim_m != gam1.bond()[0].dim() )
		gam1 = initGamma(dim_m, dim_r, dim_phys );

	gam0.putBlock( svda[0].resize( dim_l * dim_phys, dim_m ) );
	gam0 = extractGamma2( gam0, lam0 );

	svda[1].resize(dim_m, dim_m);
	double trunc_err = 0.0;
	if (show_err) {
		trunc_err = (svsq_sum - svda[1].norm()) / svsq_sum;
		std::cout << std::setprecision(12) << trunc_err << '\t';
	}
	svda[1] *= ( 1.0 / svda[1].norm() );
	lam1.putBlock( svda[1] );

	gam1.permute( gam1.label(), 1 );
	gam1.putBlock( svda[2].resize( dim_m, dim_phys * dim_r ) );
	gam1.permute( gam1.label(), 2 );
	gam1 = extractGamma( gam1, lam2 );

	return trunc_err;
}

//======================================

void CanonMPS::mps3SiteSVD( uni10::CUniTensor& psi,
	uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1,
	uni10::CUniTensor& lam2, uni10::CUniTensor& gam2,
	uni10::CUniTensor& lam3, bool left_to_right ) {
	///
	int ibn;
	if (left_to_right) {
		for (int i = 0; i < 2; ++i) {
			ibn = 2;
			auto svd = thetaSVD(psi, chi_max, ibn, sch_val_cutoff);
			if (i == 1) {
				gam1 = std::get<0>(svd);
				gam1 = extractGamma2( gam1, lam1 );
				lam2 = std::get<1>(svd);
				gam2 = std::get<2>(svd);
				gam2.permute(2);
				gam2 = extractGamma( gam2, lam3 );
			}
			else {
				gam0 = std::get<0>(svd);
				gam0 = extractGamma2( gam0, lam0 );
				lam1 = std::get<1>(svd);
				psi = std::get<2>(svd);
				psi.permute(2);
			}
		}
	}
	else {
		for (int i = 1; i > -1; --i) {
			ibn = psi.bondNum()-2;
			auto svd = thetaSVD(psi, chi_max, ibn, sch_val_cutoff);
			if (i == 0) {
				gam0 = std::get<0>(svd);
				gam0 = extractGamma2( gam0, lam0 );
				lam1 = std::get<1>(svd);
				gam1 = std::get<2>(svd);
				gam1.permute(2);
				gam1 = extractGamma( gam1, lam2 );
			}
			else {
				psi = std::get<0>(svd);
				psi.permute(psi.bondNum()-2);
				lam2 = std::get<1>(svd);
				gam2 = std::get<2>(svd);
				gam2.permute(2);
				gam2 = extractGamma( gam2, lam3 );
			}
		}
	}
}

//======================================

bool is_file_exist( std::string fileName ) {

    std::ifstream infile(fileName);
    return infile.good();
}

//======================================

std::vector< monitor > CanonMPS::availMonitors(
				std::vector< monitor > monitors, std::vector< std::string > avil_list ) {
	/// Check availability of monitor list
	std::vector< monitor > my_list;

	for (int i = 0; i < monitors.size(); ++i) {
		if (std::find( avil_list.begin(), avil_list.end(), monitors[i].name) != avil_list.end() )
			my_list.push_back( monitors[i] );
		else if ( is_file_exist( monitors[i].name ) )
			my_list.push_back( monitors[i] );
	}

	return my_list;
}

//======================================
