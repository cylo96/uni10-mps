#include <iostream>
#include <cstdlib>
#include <time.h>

#include <mps/ChainOBC.h>
#include <tns-func/func_la.h>
#include <tns-func/func_net.h>
#include <tns-func/func_evol.h>
#include <tns-func/func_meas.h>
#include <tns-func/func_convert.h>

//======================================

ChainOBC::ChainOBC(int L, int d, int X) : CanonMPS(L, d, X) {
	/// object constructor

	// dummy tensor
	dummy = initLambda(1);
	dummy.identity();
}

//======================================

void ChainOBC::randomize() {
	/// randomize a complex MPS having only real part
	std::srand( time(NULL) );

	if (gamma.size() > 0)
		gamma.clear();
	if (lambda.size() > 0)
		lambda.clear();

	lambda.push_back(dummy);

	for (int i = 0; i < lat_size; ++i) {

		int chi1 = (int) std::min( std::min( pow(dim_phys, i), pow(dim_phys, lat_size-i) ), (double) chi_max );
		int chi2 = (int) std::min( std::min( pow(dim_phys, i+1), pow(dim_phys, lat_size-i-1) ), (double) chi_max );

		gamma.push_back( initGamma(chi1, chi2, dim_phys) );
		gamma[i] = randT( gamma[i] );

		lambda.push_back( initLambda(chi2) );
		lambda[i+1] = randT( lambda[i+1] );
	}

	// replace last lambda with dummy (since it's at boundary)
	lambda.pop_back();
	lambda.push_back(dummy);
}

//======================================

void ChainOBC::importMPS( std::string dirname, int unit_cell ) {
	///
	CanonMPS::importMPS( dirname, unit_cell, true, true );

	int chi = lambda[0].bond()[0].dim();
	uni10::CMatrix uni(chi, chi);
	uni.set_zero();
	uni.at(0, 0) = Complex(1.0, 0.0);

	lambda[0] = initLambda(chi);
	lambda[0].putBlock(uni);
	lambda.pop_back();
	lambda.push_back( lambda[0] );
}

//======================================

void ChainOBC::tebd( uni10::CUniTensor ham, uni10::CUniTensor hbl, uni10::CUniTensor hbr,
	Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS
	// todo: handle the dimension trncation/expansion to fit chi_max

	// generate time evolution operator
	uni10::CUniTensor U   = expBO2( I * dt, ham );
	uni10::CUniTensor Ubl = expBO2( I * dt, hbl );
	uni10::CUniTensor Ubr = expBO2( I * dt, hbr );

	if (orderTS == 1) {
		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update
			// update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if ( n == 0 )
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubl);
				else if ( n == lat_size-2 )
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubr);
				else
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], U);
			}
			// update even sites
			for (int n = 1; n < lat_size-1; n+=2) {

				if ( n == lat_size-2 )
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubr);
				else
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], U);
			}
		}
	}
	else if (orderTS == 2) {
		uni10::CUniTensor U2   = expBO2( 0.5 * I * dt, ham );
		uni10::CUniTensor Ubl2 = expBO2( 0.5 * I * dt, hbl );
		uni10::CUniTensor Ubr2 = expBO2( 0.5 * I * dt, hbr );

		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update
			// update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if ( n == 0 )
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubl2);
				else if ( n == lat_size-2 )
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubr2);
				else
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], U2);
			}
			// update even sites
			for (int n = 1; n < lat_size-1; n+=2) {

				if ( n == lat_size-2 )
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubr);
				else
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], U);
			}
			// update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if ( n == 0 )
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubl2);
				else if ( n == lat_size-2 )
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubr2);
				else
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], U2);
			}
		}
	}
}

//======================================

void ChainOBC::dmrg( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	std::vector<uni10::CUniTensor> mpo;
	for (int i = 0; i < lat_size; ++i) {
		if (i == 0)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_l" ) );
		else if (i == lat_size-1)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_r" ) );
		else
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	}
	try {
		// a temporary workaround to put in impurity
		int mid = lat_size/2-1;
		mpo[mid] = uni10::CUniTensor( mpo_dir + "/mpo_imp" );
	}
	catch(const std::exception& e) {}
	
	dmrgImp(mpo, sweeps, iter_max, tolerance, verbose);
	mpo.clear();
}

//======================================

void ChainOBC::dmrgU2( std::string mpo_dir, int sweeps, int iter_max, double tolerance,
	double trunc_err_lim, bool verbose ) {
	///
	std::vector<uni10::CUniTensor> mpo;
	for (int i = 0; i < lat_size; ++i) {
		if (i == 0)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_l" ) );
		else if (i == lat_size-1)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_r" ) );
		else
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	}
	try {
		// a temporary workaround to put in impurity
		int mid = lat_size/2-1;
		mpo[mid] = uni10::CUniTensor( mpo_dir + "/mpo_imp" );
	}
	catch(const std::exception& e) {}

	dmrgImpU2(mpo, sweeps, iter_max, tolerance, trunc_err_lim, verbose);
	mpo.clear();
}

//======================================

void ChainOBC::dmrgU2( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	dmrgU2( mpo_dir, sweeps, iter_max, tolerance, 1.0, verbose );
}

//======================================

void ChainOBC::dmrgImp(
	std::vector<uni10::CUniTensor>& mpo, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	int uc = 1;
	uni10::CUniTensor ham;
	uni10::CUniTensor psi;
	double eng;

	std::vector<uni10::CUniTensor> mpoH;
	std::vector<uni10::CUniTensor> blk;

	for (int s = 0; s < sweeps; ++s) {
		//
		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 0; i < lat_size; ++i) {
				//
				mpoDMRGH( i, true, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.bondNum() );
				int s1 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s1 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				psi.permute(2);

				if (i == lat_size-1) {
					gamma[i] = extractGamma( extractGamma2( psi, lambda[i] ), lambda[i+1] );
				}
				else {
					std::vector<uni10::CMatrix> svda = psi.getBlock().svd();

					int dim_l = gamma[i].bond()[0].dim();
					int dim_m = gamma[i].bond()[2].dim();
					gamma[i].putBlock( svda[0].resize( dim_l * dim_phys, dim_m ) );
					gamma[i] = extractGamma2( gamma[i], lambda[i] );

					svda[1].resize(dim_m, dim_m);
					svda[1] *= ( 1.0 / svda[1].norm() );
					lambda[i+1].putBlock( svda[1] );

					uni10::CUniTensor vi( lambda[i+1].bond() );
					vi.putBlock( svda[2].resize(dim_m, dim_m) );
					gamma[i+1] = netLG( vi, gamma[i+1] );
				}

				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					std::cout << std::setprecision(12) << entanglementEntropy( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBlk( i, true, uc, gamma, lambda, mpo, blk );
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size-1; i >= 0; --i) {
				//
				mpoDMRGH( i, false, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.bondNum() );
				int s1 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s1 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				psi.permute(1);

				if (i == 0) {
					psi.permute( psi.label(), 2 );
					gamma[i] = extractGamma( extractGamma2( psi, lambda[i] ), lambda[i+1] );
				}
				else {
					std::vector<uni10::CMatrix> svdb = psi.getBlock().svd();

					int dim_m = gamma[i].bond()[0].dim();
					int dim_r = gamma[i].bond()[2].dim();
					uni10::CUniTensor vi( lambda[i].bond() );
					vi.putBlock( svdb[0].resize(dim_m, dim_m) );
					gamma[i-1] = netGL( gamma[i-1], vi );

					svdb[1].resize(dim_m, dim_m);
					svdb[1] *= ( 1.0 / svdb[1].norm() );
					lambda[i].putBlock( svdb[1] );

					gamma[i].permute( gamma[i].label(), 1 );
					gamma[i].putBlock( svdb[2].resize( dim_m, dim_phys * dim_r ) );
					gamma[i].permute( gamma[i].label(), 2 );
					gamma[i] = extractGamma( gamma[i], lambda[i+1] );
				}

				if (verbose) {
					std::cout << i << '\t' << lat_size-i << '\t';
					std::cout << std::setprecision(12) << entanglementEntropy( lambda[i] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBlk( i, false, uc, gamma, lambda, mpo, blk );
			}
		}
	}

	blk.clear();
	mpoH.clear();
}

//======================================

void ChainOBC::dmrgImpU2(
	std::vector<uni10::CUniTensor>& mpo, int sweeps, int iter_max, double tolerance,
	double trunc_err_lim, bool verbose ) {
	///
	int uc = 2;	// unit_cell size
	uni10::CUniTensor ham;
	uni10::CUniTensor psi;
	double eng = 0.0, trunc_err = 0.0, trunc_err_max = 0.0;
	int chi_step_size = std::max(chi_max/10, 1);

	std::vector<uni10::CUniTensor> mpoH;
	std::vector<uni10::CUniTensor> blk;

	for (int s = 0; s < sweeps; ++s) {

		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 0; i < lat_size-1; ++i) {

				mpoDMRGH( i, true, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.label(), psi.bondNum() );
				int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );

				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					trunc_err = mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], verbose, true );
					std::cout << std::setprecision(12) << entanglementEntropy( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				else
					trunc_err = mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], false, true );

				updateDMRGBlk( i, true, uc, gamma, lambda, mpo, blk );
				if (trunc_err > trunc_err_max)
					trunc_err_max = trunc_err;
			}
			// change bond dim according to truncation error
			if (trunc_err_max > trunc_err_lim) {
				chi_max += chi_step_size;
				trunc_err_max = 0.0;
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size-1; i > 0; --i) {

				mpoDMRGH( i, false, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGLGL( lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), psi.bondNum() );
				int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );

				if (verbose) {
					std::cout << i << '\t' << lat_size-i << '\t';
					trunc_err = mps2SiteSVD( psi, lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1], verbose, true );
					std::cout << std::setprecision(12) << entanglementEntropy( lambda[i] ) << '\t'
						<< eng << '\n';
				}
				else
					trunc_err = mps2SiteSVD( psi, lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1], false, true );

				updateDMRGBlk( i, false, uc, gamma, lambda, mpo, blk );
				if (trunc_err > trunc_err_max)
					trunc_err_max = trunc_err;
			}
			// change bond dim according to truncation error
			if (trunc_err_max > trunc_err_lim) {
				chi_max += chi_step_size;
				trunc_err_max = 0.0;
			}
		}
	}

	blk.clear();
	mpoH.clear();
}

//======================================

void ChainOBC::dmrgImpU2(
	std::vector<uni10::CUniTensor>& mpo, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	dmrgImpU2( mpo, sweeps, iter_max, tolerance, 1.0, verbose );
}

//======================================

void ChainOBC::dmrgImpU3(
	std::vector<uni10::CUniTensor>& mpo, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	int uc = 3;	// unit_cell size
	uni10::CUniTensor ham;
	uni10::CUniTensor psi;
	double eng = 0.0;

	std::vector<uni10::CUniTensor> mpoH;
	std::vector<uni10::CUniTensor> blk;

	for (int s = 0; s < sweeps; ++s) {

		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 0; i < lat_size-2; ++i) {
			// for (int i = 1; i < lat_size-2; i+=2) {

				mpoDMRGH( i, true, uc, gamma, lambda, mpo, mpoH, blk );
				psi = net3Site( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], gamma[i+2], lambda[i+3] );
				psi.permute( psi.label(), psi.bondNum() );
				int s3 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s3 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );

				mps3SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], gamma[i+2], lambda[i+3], true );
				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					std::cout << std::setprecision(12)
						<< entanglementEntropy( lambda[i+1] ) << '\t' << entanglementEntropy( lambda[i+2] ) << '\t'
						<< eng << '\n';
				}

				updateDMRGBlk( i, true, uc, gamma, lambda, mpo, blk );
				// updateDMRGBlk( i+2, false, uc, gamma, lambda, mpo, blk );
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size-1; i > 1; --i) {
			// for (int i = lat_size-2; i > 1; i-=2) {

				mpoDMRGH( i, false, uc, gamma, lambda, mpo, mpoH, blk );
				psi = net3Site( lambda[i-2], gamma[i-2], lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), psi.bondNum() );
				int s3 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s3 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );

				mps3SiteSVD( psi, lambda[i-2], gamma[i-2], lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1], false );
				if (verbose) {
					std::cout << i << '\t' << lat_size-i << '\t';
					std::cout << std::setprecision(12)
						<< entanglementEntropy( lambda[i-1] ) << '\t' << entanglementEntropy( lambda[i] ) << '\t'
						<< eng << '\n';
				}

				updateDMRGBlk( i, false, uc, gamma, lambda, mpo, blk );
				// updateDMRGBlk( i-2, true, uc, gamma, lambda, mpo, blk );
			}
		}
	}

	blk.clear();
	mpoH.clear();
}

//======================================

uni10::CUniTensor ChainOBC::expVal( uni10::CUniTensor op, int idx, bool constBD ) {
	/// return expectation value (a contracted uni10) of an one-site operator on site idx
	return expectOBCLongContract(gamma, lambda, op, idx);
}

//======================================

uni10::CUniTensor ChainOBC::correlation( uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 ) {
	///
	return correlationOBC( gamma, lambda, op1, op2, idx1, idx2 );
}
