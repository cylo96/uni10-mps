#ifndef CHAINIBC_H
#define CHAINIBC_H
#include "CanonMPS.h"
#include "ChainInf.h"

/// ref: PHYSICAL REVIEW B 86, 245107 (2012)
class ChainIBC: public CanonMPS {

public:
	/// constructor
	ChainIBC(int L, int d, int X);
	~ChainIBC();

	uni10::CUniTensor getAs(int idx);
	void putAs(uni10::CUniTensor A, int idx = -1);
	void refresh();
	void randomize();
	void expand(int add_left, int add_right, std::string dirname, int unit_cell = 2);

	void importMPS( std::string dirname, int unit_cell = 2 );
	void import2Semi( std::string dirname, int unit_cell = 2 );
	void loadNetIBC( std::string net_dir = "." );

	void breakLink( int imp_loc );
	void oneSiteOP( uni10::CUniTensor op, int idx = 0 );

	void tebd( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
		uni10::CUniTensor hwr, uni10::CUniTensor hr, Complex dt, int steps );

	void tebd2( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
		uni10::CUniTensor hwr, uni10::CUniTensor hr, Complex dt, int steps, int orderTS = 1 );

	void tebdImp( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
		uni10::CUniTensor hwr, uni10::CUniTensor hr, uni10::CUniTensor himp, int idx,
		Complex dt, int steps, int orderTS = 1 );

	void tebd( uni10::CUniTensor hl, uni10::CUniTensor hlw,
		std::vector<uni10::CUniTensor>& hw,
		uni10::CUniTensor hwr, uni10::CUniTensor hr,
		Complex dt, int steps, int orderTS = 1 );

	void tebd( std::vector<uni10::CUniTensor>& hw, uni10::CUniTensor hbl, uni10::CUniTensor hbr,
		ChainInf& mps_bkl, ChainInf& mps_bkr, Complex dt, int steps, int orderTS = 1 );

	void dmrg( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose = false );

	void dmrgImp( std::vector<uni10::CUniTensor>& mpo,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgImp( int loc, std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose = false );

	void dmrgImpU2( std::vector<uni10::CUniTensor>& mpo,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgImpU2( int loc, std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgU2( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose = false );

	void idmrg( std::string mpo_dir, int sweeps, int steps, int iter_max, double tolerance, std::string sav_dir = "" );
	void idmrgResume( std::string mpo_dir, int sweeps, int steps, int iter_max, double tolerance, std::string sav_dir );
	void idmrgAC( double J, double delta, std::string mpo_dir, std::string hb_dir,
		int steps, int iter_max, double tolerance, std::string sav_dir );

	uni10::CUniTensor expVal( uni10::CUniTensor op, int idx );

	uni10::CUniTensor correlation(
		uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 );

private:
	std::vector<uni10::CUniTensor> As;
	uni10::CUniTensor idl;
	uni10::CUniTensor idr;
	uni10::CUniTensor dummy;

	void updateCorner( bool left,
		uni10::CNetwork net, uni10::CUniTensor Ub, uni10::CUniTensor Ubw );

	void truncUpdateWindow( uni10::CUniTensor& theta,
		uni10::CUniTensor& Aa, uni10::CUniTensor& lam, uni10::CUniTensor& Ab );

	uni10::CNetwork net_r;
	uni10::CNetwork net_l;
	uni10::CNetwork net_contr;
	uni10::CNetwork net_contr_o1;
	uni10::CNetwork net_contr_o2;
	uni10::CNetwork net_contr_last;
	uni10::CNetwork net_lcorner;
	uni10::CNetwork net_rcorner;

	bool net_ibc_loaded = false;
	std::string net_ibc_dir = ".";

};

#endif
