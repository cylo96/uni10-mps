#include <iostream>
#include <cstdlib>
#include <time.h>

#include <mps/ChainSemiInf.h>
#include <tns-func/func_net.h>
#include <tns-func/func_evol.h>
#include <tns-func/func_convert.h>
#include <tns-func/func_meas.h>
#include <tns-func/func_la.h>

//======================================

ChainSemiInf::ChainSemiInf(int L, int d, int X) : CanonMPS(L, d, X) {
	/// object constructor
	// todo: assert L even
	// dummy tensor
	dummy = initLambda(1);
	dummy.identity();

	// left corner identity
	idl = dummy;
	// right corner identity
	idr = initLambda(chi_max);
	idr.identity();
	idr.permute(idr.label(), 2);
}

//======================================

ChainSemiInf::~ChainSemiInf() {
	/// object destructor
	As.clear();
	gamma.clear();
	lambda.clear();
}

//======================================

uni10::CUniTensor ChainSemiInf::getAs(int idx) {
	/// return the idx'th As
	return As[idx];
}

//======================================

void ChainSemiInf::putAs(uni10::CUniTensor A, int idx) {
	/// put a As tensor at As[idx]
	// todo: assert -1 <= idx < gamma.size();
	if (idx == -1)
		As.push_back( A );
	else
		As[idx] = A;
}

//======================================

void ChainSemiInf::refresh() {
	/// refresh As vector using gamma lambda vectors
	// todo: assert gamma.size() lambda.size() > sth
	if (As.size() == 0) {
		for (int n = 0; n < lat_size; ++n) {
			if (n == lat_size-1)
				putAs( netGL( netLG( lambda[n], gamma[n] ), lambda[n+1] ) );
			else
				putAs( netLG( lambda[n], gamma[n] ) );
		}
	}
	else if (As.size() == lat_size) {
		for (int n = 0; n < lat_size; ++n) {
			if (n == lat_size-1)
				As[n] = netGL( netLG( lambda[n], gamma[n] ), lambda[n+1] );
			else
				As[n] = netLG( lambda[n], gamma[n] );
		}
	}
}

//======================================

void ChainSemiInf::randomize() {
	/// randomize a complex MPS having only real part
	std::srand( time(NULL) );

	if (As.size() > 0)
		As.clear();
	if (gamma.size() > 0)
		gamma.clear();
	if (lambda.size() > 0)
		lambda.clear();

	lambda.push_back( dummy );	// lambda[0]

	for (int i = 0; i < lat_size; ++i) {

		int chi1 = (int) std::min( pow(dim_phys, i), (double) chi_max );
		int chi2 = (int) std::min( pow(dim_phys, i+1), (double) chi_max );

		As.push_back( initGamma(chi1, chi2, dim_phys) );
		As[i] = randT( As[i] );
		gamma.push_back( As[i] );

		lambda.push_back( initLambda(chi2) );
		lambda[i+1].identity();
	}
}

//======================================

void ChainSemiInf::expand( int add_right, std::string dirname, int unit_cell ) {
	/// expand the MPS with add_left sites on the left, add_right sites on the right
	// todo: raise exception if gam/lam vectors empty
	if (gamma.size() == 0 || lambda.size() ==0)
		setSize( lat_size + add_right );
	else {
		std::vector< uni10::CUniTensor > ext_gam;
		std::vector< uni10::CUniTensor > ext_lam;
		for (int idx = 0; idx < unit_cell; ++idx) {
			ext_gam.push_back( uni10::CUniTensor( dirname + "/gamma_" + std::to_string((long long)idx ) ) );
			ext_lam.push_back( uni10::CUniTensor( dirname + "/lambda_" + std::to_string((long long)idx ) ) );
		}

		for (int i = 0; i < add_right; i+=unit_cell) {
			for (int j = 0; j < unit_cell; ++j) {
				gamma.push_back( ext_gam[ j ] );
				lambda.push_back( ext_lam[ (j+1)%unit_cell ] );
			}
		}
		setSize( lat_size + add_right );
		refresh();

		ext_gam.clear();
		ext_lam.clear();
	}
}

//======================================

void ChainSemiInf::importMPS( std::string dirname, int unit_cell ) {
	///
	CanonMPS::importMPS( dirname, unit_cell, true, true );

	int chi = lambda[0].bond()[0].dim();
	uni10::CMatrix uni(chi, chi);
	uni.set_zero();
	uni.at(0, 0) = Complex(1.0, 0.0);

	lambda[0] = initLambda(chi);
	lambda[0].putBlock(uni);

	refresh();
}

//======================================

void ChainSemiInf::loadNetIBC( std::string net_dir ) {
	///
	net_ibc_dir = net_dir;
	net_r = uni10::CNetwork( net_ibc_dir + "/exp_rvec" );
	net_l = uni10::CNetwork( net_ibc_dir + "/exp_lvec" );
	net_contr = uni10::CNetwork( net_ibc_dir + "/exp_contr" );
	net_contr_o1 = uni10::CNetwork( net_ibc_dir + "/exp_contr_op" );
	net_contr_o2 = uni10::CNetwork( net_ibc_dir + "/exp_contr_op2" );
	net_contr_last = uni10::CNetwork( net_ibc_dir + "/exp_contr_last" );
	net_rcorner = uni10::CNetwork( net_ibc_dir + "/net_rcorner" );
  net_ibc_loaded = true;
}

//======================================

void ChainSemiInf::oneSiteOP( uni10::CUniTensor op, int idx ) {
	/// Act one-site operator on a specific site
	CanonMPS::oneSiteOP( op, idx );
	As[idx] = netLG( lambda[idx], gamma[idx] );
}

//======================================

uni10::CUniTensor ChainSemiInf::expVal( uni10::CUniTensor op, int idx ) {
	/// return expectation value (a contracted uni10) of an one-site operator on site idx
	// left vector: corner id * corner id
	// loop over sites
	// if not idx, transfer matrix = As * As_dagger
	// if at idx, transfer matrix = As * op * As_dagger
	// right vector: id * id
	if (!net_ibc_loaded)
		loadNetIBC(net_ibc_dir);

	uni10::CUniTensor idL;
	int BD = lambda[0].bond()[0].dim();

	if ( BD > 1 ) {
		idL = initLambda( BD );
		idL.identity();
	}
	else {
		idL = idl;
	}

	net_r.putTensor( "bot", idr );
	net_r.putTensor( "top", idr.permute(idr.label(), 1) );
	uni10::CUniTensor rvec = net_r.launch();
	idr.permute(idr.label(), 2);

	net_l.putTensor( "bot", idL );
	net_l.putTensor( "top", idL.permute(idL.label(), 0) );
	uni10::CUniTensor lvec = net_l.launch();
	idL.permute(idL.label(), 1);

	for (int i = 0; i < lat_size; ++i) {

		if (i == idx) {
			if ( op.bondNum() == 2 ) {
				net_contr_o1.putTensor( "left", lvec );
				net_contr_o1.putTensor( "bot", As[i] );
				net_contr_o1.putTensor( "top", As[i].permute(As[i].label(), 1).conj() );
				net_contr_o1.putTensor( "op", op );
				lvec = net_contr_o1.launch();

				As[i].permute(As[i].label(), 2).conj();
			}
			else if ( op.bondNum() == 4 ) {
				net_contr_o2.putTensor( "left", lvec );
				net_contr_o2.putTensor( "bot1", As[i] );
				net_contr_o2.putTensor( "bot2", As[i+1] );
				net_contr_o2.putTensor( "top1", As[i].permute(As[i].label(), 1).conj() );
				net_contr_o2.putTensor( "top2", As[i+1].permute(As[i+1].label(), 1).conj() );
				net_contr_o2.putTensor( "op", op );
				lvec = net_contr_o2.launch();

				As[i].permute(As[i].label(), 2).conj();
				As[i+1].permute(As[i+1].label(), 2).conj();
				i += 1;
			}
		}
		else {
			net_contr.putTensor( "left", lvec );
			net_contr.putTensor( "bot", As[i] );
			net_contr.putTensor( "top", As[i].permute(As[i].label(), 1).conj() );
			lvec = net_contr.launch();

			As[i].permute(As[i].label(), 2).conj();
		}
	}

	net_contr_last.putTensor( "left", lvec );
	net_contr_last.putTensor( "right", rvec );
	uni10::CUniTensor expect = net_contr_last.launch();

	return expect;
}

//======================================

uni10::CUniTensor ChainSemiInf::correlation( uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 ) {

	return correlationOBC( gamma, lambda, op1, op2, idx1, idx2 );
}

//======================================

void ChainSemiInf::updateCorner( uni10::CNetwork net, uni10::CUniTensor Ub, uni10::CUniTensor Ubw ) {
	/// evolve and update corner tensor
	if (!net_ibc_loaded)
		loadNetIBC(net_ibc_dir);

	net.putTensor("Id", idr);
	net.putTensor("As", As[lat_size-1]);
	net.putTensor("Ub", Ub);
	net.putTensor("Ubw", Ubw);
	uni10::CUniTensor theta = net.launch();
	As[lat_size-1] = theta.permute( theta.label(), 2 );
}

//======================================

void ChainSemiInf::tebd( uni10::CUniTensor hlw, uni10::CUniTensor hw,
	uni10::CUniTensor hwr, uni10::CUniTensor hr, Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS, involving multipications of lambda^-1
	// todo: handle the dimension trncation/expansion to fit chi_max
	// time evolution operator
	uni10::CUniTensor Ulw = expBO( I * dt, hlw );
	uni10::CUniTensor Uw  = expBO( I * dt, hw );
	uni10::CUniTensor Uwr = expBO( I * dt, hwr );
	uni10::CUniTensor Ur  = expBO( I * dt, hr );

	if (orderTS == 1) {
		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if ( n == 0 )
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ulw);
				else
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);
			}
			refresh();

			// evolve and update corners
			updateCorner( net_rcorner, Ur, Uwr );
			gamma[lat_size-1] = extractGamma2( extractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );

			// evolve and update even sites
			// sweep left to right
			for (int n = 1; n < lat_size-1; n+=2)
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);

			refresh();
		}
	}
	else if (orderTS == 2) {
		uni10::CUniTensor Ulw2 = expBO( 0.5 * I * dt, hlw );
		uni10::CUniTensor Uw2  = expBO( 0.5 * I * dt, hw );
		uni10::CUniTensor Uwr2 = expBO( 0.5 * I * dt, hwr );
		uni10::CUniTensor Ur2  = expBO( 0.5 * I * dt, hr );

		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if ( n == 0 )
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ulw2);
				else
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw2);
			}
			refresh();

			// evolve and update corners
			updateCorner( net_rcorner, Ur2, Uwr2 );
			gamma[lat_size-1] = extractGamma2( extractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );

			// evolve and update even sites
			// sweep left to right
			for (int n = 1; n < lat_size-1; n+=2)
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);

			refresh();

			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if ( n == 0 )
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ulw2);
				else
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw2);
			}
			refresh();

			// evolve and update corners
			updateCorner( net_rcorner, Ur2, Uwr2 );
			gamma[lat_size-1] = extractGamma2( extractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );
		}
	}
}

//======================================

void ChainSemiInf::dmrg( std::vector<uni10::CUniTensor>& mpo,
	int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	int uc = 1;	// unit_cell size

	uni10::CUniTensor gam_R = initGamma(chi_max, 1, chi_max);
	gam_R.putBlock( idr.getBlock() );
	uni10::CUniTensor lam_R = initLambda(1);
	lam_R.identity();

	gamma.push_back( gam_R );
	lambda.push_back( lam_R );

	uni10::CUniTensor psi;
	double eng = 0.0;

	std::vector<uni10::CUniTensor> mpoH;
	std::vector<uni10::CUniTensor> blk;

	for (int s = 0; s < sweeps; ++s) {
		//
		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 0; i < lat_size; ++i) {
				//
				mpoDMRGH( i, true, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), psi.bondNum() );
				int s1 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s1 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				psi.permute( psi.label(), 2 );

				if (i == lat_size-1) {
					gamma[i] = extractGamma( extractGamma2( psi, lambda[i] ), lambda[i+1] );
				}
				else {
					std::vector<uni10::CMatrix> svda = psi.getBlock().svd();

					int dim_l = gamma[i].bond()[0].dim();
					int dim_m = gamma[i].bond()[2].dim();
					gamma[i].putBlock( svda[0].resize( dim_l * dim_phys, dim_m ) );
					gamma[i] = extractGamma2( gamma[i], lambda[i] );

					svda[1].resize(dim_m, dim_m);
					svda[1] *= ( 1.0 / svda[1].norm() );
					lambda[i+1].putBlock( svda[1] );

					uni10::CUniTensor vi( lambda[i+1].bond() );
					vi.putBlock( svda[2].resize(dim_m, dim_m) );
					gamma[i+1] = netLG( vi, gamma[i+1] );
				}

				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					std::cout << std::setprecision(12) << entanglementEntropy( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBlk( i, true, uc, gamma, lambda, mpo, blk );
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size-1; i >= 0; --i) {
				//
				mpoDMRGH( i, false, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), psi.bondNum() );
				int s1 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s1 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				psi.permute( psi.label(), 1 );

				if (i == 0) {
					psi.permute( psi.label(), 2 );
					gamma[i] = extractGamma( extractGamma2( psi, lambda[i] ), lambda[i+1] );
				}
				else {
					std::vector<uni10::CMatrix> svdb = psi.getBlock().svd();

					int dim_m = gamma[i].bond()[0].dim();
					int dim_r = gamma[i].bond()[2].dim();
					uni10::CUniTensor vi( lambda[i].bond() );
					vi.putBlock( svdb[0].resize(dim_m, dim_m) );
					gamma[i-1] = netGL( gamma[i-1], vi );

					svdb[1].resize(dim_m, dim_m);
					svdb[1] *= ( 1.0 / svdb[1].norm() );
					lambda[i].putBlock( svdb[1] );

					gamma[i].permute( gamma[i].label(), 1 );
					gamma[i].putBlock( svdb[2].resize( dim_m, dim_phys * dim_r ) );
					gamma[i].permute( gamma[i].label(), 2 );
					gamma[i] = extractGamma( gamma[i], lambda[i+1] );
				}

				if (verbose) {
					std::cout << i << '\t' << lat_size-i << '\t';
					std::cout << std::setprecision(12) << entanglementEntropy( lambda[i] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBlk( i, false, uc, gamma, lambda, mpo, blk );
			}
		}
	}

	CanonMPS::vecSlice( gamma, lat_size, 0 );
	CanonMPS::vecSlice( lambda, lat_size+1, 0 );
	blk.clear();
	mpoH.clear();
	refresh();
}

//======================================

void ChainSemiInf::dmrg( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	std::vector<uni10::CUniTensor> mpo;
	for (int i = 0; i < lat_size+1; ++i) {
		if (i == 0)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_l" ) );
		else if (i == lat_size)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_r" ) );
		else
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	}

	dmrg( mpo, sweeps, iter_max, tolerance, verbose );
}

//======================================

void ChainSemiInf::dmrgU2( std::vector<uni10::CUniTensor>& mpo,
	int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	int uc = 2;	// unit_cell size

	uni10::CUniTensor gam_R = initGamma(chi_max, 1, chi_max);
	gam_R.putBlock( idr.getBlock() );
	uni10::CUniTensor lam_R = initLambda(1);
	lam_R.identity();

	gamma.push_back( gam_R );
	lambda.push_back( lam_R );

	uni10::CUniTensor psi;
	double eng = 0.0;

	std::vector<uni10::CUniTensor> mpoH;
	std::vector<uni10::CUniTensor> blk;

	for (int s = 0; s < sweeps; ++s) {
		//
		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 0; i < lat_size-1; ++i) {
				//
				mpoDMRGH( i, true, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.label(), psi.bondNum() );
				//int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				psi.permute( psi.label(), psi.bondNum()/2 );

				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], verbose );
					std::cout << std::setprecision(12) << entanglementEntropy( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				else
					mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], verbose );

				updateDMRGBlk( i, true, uc, gamma, lambda, mpo, blk );
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size-1; i >= 1; --i) {
				//
				mpoDMRGH( i, false, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGLGL( lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), psi.bondNum() );
				//int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				psi.permute( psi.label(), psi.bondNum()/2 );

				if (verbose) {
					std::cout << i << '\t' << lat_size-i << '\t';
					mps2SiteSVD( psi, lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1], verbose );
					std::cout << std::setprecision(12) << entanglementEntropy( lambda[i] ) << '\t'
						<< eng << '\n';
				}
				else
					mps2SiteSVD( psi, lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1], verbose );

				updateDMRGBlk( i, false, uc, gamma, lambda, mpo, blk );
			}
		}
	}

	CanonMPS::vecSlice( gamma, lat_size, 0 );
	CanonMPS::vecSlice( lambda, lat_size+1, 0 );
	blk.clear();
	refresh();
}

//======================================

void ChainSemiInf::dmrgU2( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	std::vector<uni10::CUniTensor> mpo;
	for (int i = 0; i < lat_size+1; ++i) {
		if (i == 0)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_l" ) );
		else if (i == lat_size)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_r" ) );
		else
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	}

	dmrgU2( mpo, sweeps, iter_max, tolerance, verbose );
}

//======================================
