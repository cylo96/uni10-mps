#ifndef CHAININF_H
#define CHAININF_H
#include "ChainPBC.h"

class ChainInf: public ChainPBC {

public:
	/// constructor
	ChainInf(int d, int X);

	void itebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS = 1 );
	void itebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS,
		std::vector<monitor> mon_in, int interval = 10);

	void idmrg( std::string mpo_dir,
		int steps, int iter_max, double tolerance, std::string sav_dir = "", bool offset_hb = false );
	void idmrgResume( std::string mpo_dir,
		int steps, int iter_max, double tolerance, std::string sav_dir, bool offset_hb = false );
	void idmrgSweep( std::string mpo_dir, int steps, int iter_max, double tolerance );
	void idmrgUC( std::string mpo_dir, int steps, int iter_max, double tolerance );

	uni10::CUniTensor expVal( uni10::CUniTensor op, int loc = -1 );
	uni10::CUniTensor expValStagger( uni10::CUniTensor op );

	void mps4SiteSVD( uni10::CUniTensor& psi,
		uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
		uni10::CUniTensor& lam1, uni10::CUniTensor& gam1,
		uni10::CUniTensor& lam2, uni10::CUniTensor& gam2,
		uni10::CUniTensor& lam3, uni10::CUniTensor& gam3, uni10::CUniTensor& lam4 );

	friend class ChainIBC;

private:

	uni10::CUniTensor HL, HS, HR;
	std::vector<uni10::CUniTensor> As, Bs;
	uni10::CUniTensor lamL, lamR;
	std::vector<std::string> mon_avail;
	void monitorOutput( std::vector<monitor> monitors, uni10::CUniTensor ham );
};

#endif
