#ifndef CHAINPBC_H
#define CHAINPBC_H
#include "CanonMPS.h"

class ChainPBC: public CanonMPS {

public:
	/// constructor
	ChainPBC(int L, int d, int X);

	void randomize();

private:

};

#endif
