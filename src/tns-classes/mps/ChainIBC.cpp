#include <iostream>
#include <cstdlib>
#include <time.h>

#include <mps/ChainIBC.h>
#include <tns-func/func_net.h>
#include <tns-func/func_evol.h>
#include <tns-func/func_convert.h>
#include <tns-func/func_la.h>
#include <tns-func/func_op.h>
#include <tns-func/func_meas.h>
#include <tns-func/func_ibc_xxz.hpp>

//======================================

ChainIBC::ChainIBC(int L, int d, int X) : CanonMPS(L, d, X) {
	/// object constructor

	// todo: assert L even

	// dummy tensor
	dummy = initLambda(chi_max);
	dummy.identity();

	// left corner identity
	idl = dummy;
	// right corner identity
	idr = dummy;
	idr.permute(idr.label(), 2);
}

//======================================

ChainIBC::~ChainIBC() {
	/// object destructor
	As.clear();
	gamma.clear();
	lambda.clear();
}

//======================================

uni10::CUniTensor ChainIBC::getAs(int idx) {
	/// return the idx'th As
	return As[idx];
}

//======================================

void ChainIBC::putAs(uni10::CUniTensor A, int idx) {
    /// put a As tensor at As[idx]
    // todo: assert -1 <= idx < gamma.size();
    if (idx == -1)
        As.push_back( A );
    else
        As[idx] = A;
}

//======================================

void ChainIBC::refresh() {
	/// refresh As vector using gamma lambda vectors
	// todo: assert gamma.size() lambda.size() > sth
	if (As.size() == 0) {
		for (int n = 0; n < lat_size; ++n) {
			if (n == lat_size-1)
				putAs( netGL( netLG( lambda[n], gamma[n] ), lambda[n+1] ) );
			else
				putAs( netLG( lambda[n], gamma[n] ) );
		}
	}
	else if (As.size() == lat_size) {
		for (int n = 0; n < lat_size; ++n) {
			if (n == lat_size-1)
				As[n] = netGL( netLG( lambda[n], gamma[n] ), lambda[n+1] );
			else
				As[n] = netLG( lambda[n], gamma[n] );
		}
	}
	else {
		int Asize = As.size();
		for (int n = 0; n < Asize; ++n)
			As[n] = netLG( lambda[n], gamma[n] );

		for (int n = Asize; n < lat_size; ++n) {
			if (n == lat_size-1)
				putAs( netGL( netLG( lambda[n], gamma[n] ), lambda[n+1] ) );
			else
				putAs( netLG( lambda[n], gamma[n] ) );
		}
	}
}

//======================================

void ChainIBC::randomize() {
	/// randomize a complex MPS having only real part
	std::srand( time(NULL) );

	if (As.size() > 0)
		As.clear();

	for (int i = 0; i < lat_size; ++i) {

		As.push_back( initGamma(chi_max, chi_max, dim_phys) );
		As[i] = randT( As[i] );
		gamma.push_back( As[i] );
		lambda.push_back( dummy );
	}

	lambda.push_back( dummy );	// lambda vec is one elem longer
}

//======================================

void ChainIBC::expand(int add_left, int add_right, std::string dirname, int unit_cell) {
	/// expand the MPS with add_left sites on the left, add_right sites on the right
	// todo: raise exception if gam/lam vectors empty
	if (gamma.size() == 0 || lambda.size() ==0)
		setSize( lat_size + add_left + add_right );
	else {
		std::vector< uni10::CUniTensor > ext_gam;
		std::vector< uni10::CUniTensor > ext_lam;
		for (int idx = 0; idx < unit_cell; ++idx) {
			ext_gam.push_back( uni10::CUniTensor( dirname + "/gamma_" + std::to_string((long long)idx ) ) );
			ext_lam.push_back( uni10::CUniTensor( dirname + "/lambda_" + std::to_string((long long)idx ) ) );
		}

		for (int i = 0; i < add_left; i+=unit_cell) {
			for (int j = 0; j < unit_cell; ++j) {
				gamma.insert( gamma.begin(), ext_gam[ unit_cell - j - 1 ] );
				lambda.insert( lambda.begin(), ext_lam[ unit_cell - j - 1 ] );
			}
		}
		for (int i = 0; i < add_right; i+=unit_cell) {
			for (int j = 0; j < unit_cell; ++j) {
				gamma.push_back( ext_gam[ j ] );
				lambda.push_back( ext_lam[ (j+1)%unit_cell ] );
			}
		}
		setSize( lat_size + add_left + add_right );
		refresh();

		ext_gam.clear();
		ext_lam.clear();
	}
}

//======================================

void ChainIBC::importMPS( std::string dirname, int unit_cell ) {
	///
	CanonMPS::importMPS( dirname, unit_cell, true, true );
	refresh();
}

//======================================

void ChainIBC::import2Semi( std::string dirname, int unit_cell ) {
	///
	// presume both L and L/2 are even
	int L = lat_size;
	setSize( L/2 );
	CanonMPS::importMPS( dirname, unit_cell, true, true );

	uni10::CUniTensor lam_last = lambda[ L/2 ];
	lambda.pop_back();

	for (int i = 0; i < L/2; ++i) {
		putGamma( gamma[i] );
		putLambda( lambda[i] );
	}
	putLambda( lam_last );

	for (int i = 0; i < L/2; ++i) {
		putGamma( bondInvGam( gamma[L-1-i] ), i );
		putLambda( lambda[L-i], i );
	}

	setSize(L);
	refresh();
}

//======================================

void ChainIBC::loadNetIBC( std::string net_dir ) {
	///
	net_ibc_dir = net_dir;
	net_r = uni10::CNetwork( net_ibc_dir + "/exp_rvec" );
	net_l = uni10::CNetwork( net_ibc_dir + "/exp_lvec" );
	net_contr = uni10::CNetwork( net_ibc_dir + "/exp_contr" );
	net_contr_o1 = uni10::CNetwork( net_ibc_dir + "/exp_contr_op" );
	net_contr_o2 = uni10::CNetwork( net_ibc_dir + "/exp_contr_op2" );
	net_contr_last = uni10::CNetwork( net_ibc_dir + "/exp_contr_last" );
	net_lcorner = uni10::CNetwork( net_ibc_dir + "/net_lcorner" );
	net_rcorner = uni10::CNetwork( net_ibc_dir + "/net_rcorner" );
  net_ibc_loaded = true;
}

//======================================

void ChainIBC::breakLink( int imp_loc ) {
	///
	uni10::CMatrix uni = lambda[imp_loc].getBlock();
	uni.set_zero();
	uni.at(0, 0) = Complex(1.0, 0.0);
	lambda[imp_loc].set_zero();
	lambda[imp_loc].putBlock( uni );
}

//======================================

void ChainIBC::oneSiteOP( uni10::CUniTensor op, int idx ) {
	/// Act one-site operator on a specific site
	CanonMPS::oneSiteOP( op, idx );
	As[idx] = netLG( lambda[idx], gamma[idx] );
}

//======================================

uni10::CUniTensor ChainIBC::expVal( uni10::CUniTensor op, int idx ) {
	/// return expectation value (a contracted uni10) of an one-site operator on site idx
	// todo: assert 1-site or 2-site operator
	// left vector: corner id * corner id
	// loop over sites
	// if not idx, transfer matrix = As * As_dagger
	// if at idx, transfer matrix = As * op * As_dagger
	// right vector: id * id
	if (!net_ibc_loaded)
		loadNetIBC(net_ibc_dir);

	net_r.putTensor( "bot", idr );
	net_r.putTensor( "top", idr.permute(idr.label(), 1) );
	uni10::CUniTensor rvec = net_r.launch();
	idr.permute(idr.label(), 2);

	net_l.putTensor( "bot", idl );
	net_l.putTensor( "top", idl.permute(idl.label(), 0) );
	uni10::CUniTensor lvec = net_l.launch();
	idl.permute(idl.label(), 1);

	int bn = op.bondNum();
	for (int i = 0; i < lat_size; ++i) {

		if (i == idx) {
			if ( op.bondNum() == 2 ) {
				net_contr_o1.putTensor( "left", lvec );
				net_contr_o1.putTensor( "bot", As[i] );
				net_contr_o1.putTensor( "top", As[i].permute(As[i].label(), 1).conj() );
				net_contr_o1.putTensor( "op", op );
				lvec = net_contr_o1.launch();

				As[i].permute(As[i].label(), 2).conj();
			}
			else if ( op.bondNum() == 4 ) {
				net_contr_o2.putTensor( "left", lvec );
				net_contr_o2.putTensor( "bot1", As[i] );
				net_contr_o2.putTensor( "bot2", As[i+1] );
				net_contr_o2.putTensor( "top1", As[i].permute(As[i].label(), 1).conj() );
				net_contr_o2.putTensor( "top2", As[i+1].permute(As[i+1].label(), 1).conj() );
				net_contr_o2.putTensor( "op", op );
				lvec = net_contr_o2.launch();

				As[i].permute(As[i].label(), 2).conj();
				As[i+1].permute(As[i+1].label(), 2).conj();
				i += 1;
			}
		}
		else {
			net_contr.putTensor( "left", lvec );
			net_contr.putTensor( "bot", As[i] );
			net_contr.putTensor( "top", As[i].permute(As[i].label(), 1).conj() );
			lvec = net_contr.launch();

			As[i].permute(As[i].label(), 2).conj();
		}
	}

	net_contr_last.putTensor( "left", lvec );
	net_contr_last.putTensor( "right", rvec );
	uni10::CUniTensor expect = net_contr_last.launch();

	return expect;
}

//======================================

uni10::CUniTensor ChainIBC::correlation(
	uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 ) {
	// left vector: corner id * corner id
	// loop over sites
	// if not idx, transfer matrix = As * As_dagger
	// if at idx, transfer matrix = As * op * As_dagger
	// right vector: id * id
	if (!net_ibc_loaded)
		loadNetIBC(net_ibc_dir);

	net_r.putTensor( "bot", idr );
	net_r.putTensor( "top", idr.permute(idr.label(), 1) );
	uni10::CUniTensor rvec = net_r.launch();
	idr.permute(idr.label(), 2);

	net_l.putTensor( "bot", idl );
	net_l.putTensor( "top", idl.permute(idl.label(), 0) );
	uni10::CUniTensor lvec = net_l.launch();
	idl.permute(idl.label(), 1);

	for (int i = 0; i < lat_size; ++i) {

		if (i == idx1) {
			if ( op1.bondNum() == 2 ) {
				net_contr_o1.putTensor( "left", lvec );
				net_contr_o1.putTensor( "bot", As[i] );
				net_contr_o1.putTensor( "top", As[i].permute(As[i].label(), 1).conj() );
				net_contr_o1.putTensor( "op", op1 );
				lvec = net_contr_o1.launch();

				As[i].permute(As[i].label(), 2).conj();
			}
			else if ( op1.bondNum() == 4 ) {
				net_contr_o2.putTensor( "left", lvec );
				net_contr_o2.putTensor( "bot1", As[i] );
				net_contr_o2.putTensor( "bot2", As[i+1] );
				net_contr_o2.putTensor( "top1", As[i].permute(As[i].label(), 1).conj() );
				net_contr_o2.putTensor( "top2", As[i+1].permute(As[i+1].label(), 1).conj() );
				net_contr_o2.putTensor( "op", op1 );
				lvec = net_contr_o2.launch();

				As[i].permute(As[i].label(), 2).conj();
				As[i+1].permute(As[i+1].label(), 2).conj();
				i += 1;
			}
		}
		else if (i == idx2) {
			if ( op2.bondNum() == 2 ) {
				net_contr_o1.putTensor( "left", lvec );
				net_contr_o1.putTensor( "bot", As[i] );
				net_contr_o1.putTensor( "top", As[i].permute(As[i].label(), 1).conj() );
				net_contr_o1.putTensor( "op", op2 );
				lvec = net_contr_o1.launch();

				As[i].permute(As[i].label(), 2).conj();
			}
			else if ( op2.bondNum() == 4 ) {
				net_contr_o2.putTensor( "left", lvec );
				net_contr_o2.putTensor( "bot1", As[i] );
				net_contr_o2.putTensor( "bot2", As[i+1] );
				net_contr_o2.putTensor( "top1", As[i].permute(As[i].label(), 1).conj() );
				net_contr_o2.putTensor( "top2", As[i+1].permute(As[i+1].label(), 1).conj() );
				net_contr_o2.putTensor( "op", op2 );
				lvec = net_contr_o2.launch();

				As[i].permute(As[i].label(), 2).conj();
				As[i+1].permute(As[i+1].label(), 2).conj();
				i += 1;
			}
		}
		else {
			net_contr.putTensor( "left", lvec );
			net_contr.putTensor( "bot", As[i] );
			net_contr.putTensor( "top", As[i].permute(As[i].label(), 1).conj() );
			lvec = net_contr.launch();

			As[i].permute(As[i].label(), 2).conj();
		}
	}

	net_contr_last.putTensor( "left", lvec );
	net_contr_last.putTensor( "right", rvec );
	uni10::CUniTensor expect = net_contr_last.launch();

	return expect;
}

//======================================

void ChainIBC::truncUpdateWindow( uni10::CUniTensor& theta,
	uni10::CUniTensor& Aa, uni10::CUniTensor& lam, uni10::CUniTensor& Ab) {

	std::vector<uni10::Bond> bd_a = Aa.bond();
	int dim = bd_a[0].dim();    // get bond dimension
	int d   = bd_a[1].dim();

	if (theta.bondNum() != theta.inBondNum()*2)
		theta.permute(theta.label(), theta.bondNum()/2);	// permute theta to square matrix

	std::vector<uni10::CMatrix> theta_svd = theta.getBlock().svd();

	// truncate and update gamma
	Aa.permute(Aa.label(), 2);
	Aa.putBlock( theta_svd[0].resize(d*dim, dim) );

	// truncate and update lambda
	theta_svd[1].resize(dim, dim);
	theta_svd[1] *= ( 1.0 / theta_svd[1].norm() );	// rescale/normalize lambda
	lam.putBlock( theta_svd[1] );

	// truncate and update gamma2
	Ab.permute(Ab.label(), 1);
	Ab.putBlock( theta_svd[2].resize(dim, d*dim) );
	Ab.permute(Ab.label(), 2);

	theta_svd.clear();
}

//======================================

void ChainIBC::updateCorner( bool left,
	uni10::CNetwork net, uni10::CUniTensor Ub, uni10::CUniTensor Ubw ) {
	/// evolve and update corner tensor
	if (!net_ibc_loaded)
		loadNetIBC(net_ibc_dir);

	if (left) {
		net.putTensor("Id", idl);
		net.putTensor("As", As[0]);
		net.putTensor("Ub", Ub);
		net.putTensor("Ubw", Ubw);
		uni10::CUniTensor theta = net.launch();
		As[0] = theta;
	}
	else {
		net.putTensor("Id", idr);
		net.putTensor("As", As[lat_size-1]);
		net.putTensor("Ub", Ub);
		net.putTensor("Ubw", Ubw);
		uni10::CUniTensor theta = net.launch();
		As[lat_size-1] = theta.permute( theta.label(), 2 );
	}
}

//======================================

void ChainIBC::tebd( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
	uni10::CUniTensor hwr, uni10::CUniTensor hr, Complex dt, int steps ) {
	/// perform TEBD on MPS, implementation of PHYSICAL REVIEW B 86, 245107 (2012)
	// todo: handle the dimension trncation/expansion to fit chi_max

	// time evolution operator
	uni10::CUniTensor Ul  = expBO( I * dt, hl );
	uni10::CUniTensor Ulw = expBO( I * dt, hlw );
	uni10::CUniTensor Uw  = expBO( I * dt, hw );
	uni10::CUniTensor Uwr = expBO( I * dt, hwr );
	uni10::CUniTensor Ur  = expBO( I * dt, hr );

	for (int ite = 0; ite < steps; ++ite) {
		// evolve and update

		// evolve and update odd sites
		// sweep left to right
		uni10::CUniTensor lam = dummy;

		for (int n = 0; n < lat_size-1; n+=2) {

			uni10::CUniTensor ket;
			ket = netGLG(As[n], lam, As[n+1]);
			ket = tensorOp(ket, Uw);
			truncUpdateWindow(ket, As[n], lam, As[n+1]);

			if ( n == lat_size-2 ) {
				As[n+1] = netLG(lam, As[n+1]);
			}
			else {
				As[n+1] = netLG(lam, As[n+1]);
				ket = netGLG(As[n+1], dummy, As[n+2]);
				truncUpdateWindow(ket, As[n+1], lam, As[n+2]);

				As[n+2] = netLG(lam, As[n+2]);
				ket = netGLG(As[n+2], dummy, As[n+3]);
				truncUpdateWindow(ket, As[n+2], lam, As[n+3]);
			}
		}

		// sweep right to left
		lam = dummy;

		for (int n = lat_size-2; n >= 0; n-=2) {

			uni10::CUniTensor ket;
			ket = netGLG(As[n], lam, As[n+1]);
			truncUpdateWindow(ket, As[n], lam, As[n+1]);


			if ( n == 0 ) {
				As[n] = netGL(As[n], lam);
			}
			else {
				As[n] = netGL(As[n], lam);
				ket = netGLG(As[n-1], dummy, As[n]);
				truncUpdateWindow(ket, As[n-1], lam, As[n]);

				As[n-1] = netGL(As[n-1], lam);
				ket = netGLG(As[n-2], dummy, As[n-1]);
				truncUpdateWindow(ket, As[n-2], lam, As[n-1]);
			}
		}

		// evolve and update corners
		updateCorner( true, net_lcorner, Ul, Ulw );
		updateCorner( false, net_rcorner, Ur, Uwr );

		// evolve and update even sites
		// sweep left to right
		lam = dummy;

		for (int n = 1; n < lat_size-1; n+=2) {

			uni10::CUniTensor ket;
			ket = netGLG(As[n], lam, As[n+1]);
			ket = tensorOp(ket, Uw);
			truncUpdateWindow(ket, As[n], lam, As[n+1]);

			if ( n == lat_size-3 ) {
				As[n+1] = netLG(lam, As[n+1]);
			}
			else {
				As[n+1] = netLG(lam, As[n+1]);
				ket = netGLG(As[n+1], dummy, As[n+2]);
				truncUpdateWindow(ket, As[n+1], lam, As[n+2]);

				As[n+2] = netLG(lam, As[n+2]);
				ket = netGLG(As[n+2], dummy, As[n+3]);
				truncUpdateWindow(ket, As[n+2], lam, As[n+3]);
			}
		}

		// sweep right to left
		lam = dummy;

		for (int n = lat_size-3; n >= 1; n-=2) {

			uni10::CUniTensor ket;
			ket = netGLG(As[n], lam, As[n+1]);
			truncUpdateWindow(ket, As[n], lam, As[n+1]);

			if ( n == 1 ) {
				As[n] = netGL(As[n], lam);
				ket = netGLG(As[n-1], dummy, As[n]);
				truncUpdateWindow(ket, As[n-1], lam, As[n]);

				As[n-1] = netGL(As[n-1], lam);
			}
			else {
				As[n] = netGL(As[n], lam);
				ket = netGLG(As[n-1], dummy, As[n]);
				truncUpdateWindow(ket, As[n-1], lam, As[n]);

				As[n-1] = netGL(As[n-1], lam);
				ket = netGLG(As[n-2], dummy, As[n-1]);
				truncUpdateWindow(ket, As[n-2], lam, As[n-1]);
			}
		}
	}
}

//======================================

void ChainIBC::tebd2( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
	uni10::CUniTensor hwr, uni10::CUniTensor hr, Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS, involving multipications of lambda^-1
	// todo: handle the dimension trncation/expansion to fit chi_max

	// time evolution operator
	uni10::CUniTensor Ul  = expBO( I * dt, hl );
	uni10::CUniTensor Ulw = expBO( I * dt, hlw );
	uni10::CUniTensor Uw  = expBO( I * dt, hw );
	uni10::CUniTensor Uwr = expBO( I * dt, hwr );
	uni10::CUniTensor Ur  = expBO( I * dt, hr );

	if (orderTS == 1) {
		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2)
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);

			refresh();

			// evolve and update corners
			updateCorner( true, net_lcorner, Ul, Ulw );
			updateCorner( false, net_rcorner, Ur, Uwr );

			gamma[0] = extractGamma2( As[0], lambda[0] );
			gamma[lat_size-1] = extractGamma2( extractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );


			// evolve and update even sites
			// sweep left to right
			for (int n = 1; n < lat_size-1; n+=2)
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);

			refresh();
		}
	}

	else if (orderTS == 2) {
		uni10::CUniTensor Ul2  = expBO( 0.5 * I * dt, hl );
		uni10::CUniTensor Ulw2 = expBO( 0.5 * I * dt, hlw );
		uni10::CUniTensor Uw2  = expBO( 0.5 * I * dt, hw );
		uni10::CUniTensor Uwr2 = expBO( 0.5 * I * dt, hwr );
		uni10::CUniTensor Ur2  = expBO( 0.5 * I * dt, hr );

		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2)
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw2);

			refresh();

			// evolve and update corners
			updateCorner( true, net_lcorner, Ul2, Ulw2 );
			updateCorner( false, net_rcorner, Ur2, Uwr2 );

			gamma[0] = extractGamma2( As[0], lambda[0] );
			gamma[lat_size-1] = extractGamma2( extractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );


			// evolve and update even sites
			for (int n = 1; n < lat_size-1; n+=2)
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);

			refresh();

			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2)
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw2);

			refresh();

			// evolve and update corners
			updateCorner( true, net_lcorner, Ul2, Ulw2 );
			updateCorner( false, net_rcorner, Ur2, Uwr2 );

			gamma[0] = extractGamma2( As[0], lambda[0] );
			gamma[lat_size-1] = extractGamma2( extractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );
		}
	}
}

//======================================

void ChainIBC::tebdImp( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
	uni10::CUniTensor hwr, uni10::CUniTensor hr, uni10::CUniTensor himp, int idx,
	Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS, involving multipications of lambda^-1
	// todo: handle the dimension trncation/expansion to fit chi_max

	// time evolution operator
	uni10::CUniTensor Ul   = expBO( I * dt, hl );
	uni10::CUniTensor Ulw  = expBO( I * dt, hlw );
	uni10::CUniTensor Uw   = expBO( I * dt, hw );
	uni10::CUniTensor Uwr  = expBO( I * dt, hwr );
	uni10::CUniTensor Ur   = expBO( I * dt, hr );
	uni10::CUniTensor Uimp = expBO( I * dt, himp );

	if (orderTS == 1) {
		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if (n == idx)
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uimp);
				else
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);
			}
			refresh();

			// evolve and update corners
			updateCorner( true, net_lcorner, Ul, Ulw );
			updateCorner( false, net_rcorner, Ur, Uwr );

			gamma[0] = extractGamma2( As[0], lambda[0] );
			gamma[lat_size-1] = extractGamma2( extractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );


			// evolve and update even sites
			// sweep left to right
			for (int n = 1; n < lat_size-1; n+=2) {

				if (n == idx)
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uimp);
				else
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);
			}
			refresh();
		}
	}
	else if (orderTS == 2) {
		uni10::CUniTensor Ul2   = expBO( 0.5 * I * dt, hl );
		uni10::CUniTensor Ulw2  = expBO( 0.5 * I * dt, hlw );
		uni10::CUniTensor Uw2   = expBO( 0.5 * I * dt, hw );
		uni10::CUniTensor Uwr2  = expBO( 0.5 * I * dt, hwr );
		uni10::CUniTensor Ur2   = expBO( 0.5 * I * dt, hr );
		uni10::CUniTensor Uimp2 = expBO( 0.5 * I * dt, himp );

		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if (n == idx)
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uimp2);
				else
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw2);
			}
			refresh();

			// evolve and update corners
			updateCorner( true, net_lcorner, Ul2, Ulw2 );
			updateCorner( false, net_rcorner, Ur2, Uwr2 );

			gamma[0] = extractGamma2( As[0], lambda[0] );
			gamma[lat_size-1] = extractGamma2( extractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );


			// evolve and update even sites
			for (int n = 1; n < lat_size-1; n+=2) {

				if (n == idx)
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uimp);
				else
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);
			}
			refresh();

			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if (n == idx)
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uimp2);
				else
					evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw2);
			}
			refresh();

			// evolve and update corners
			updateCorner( true, net_lcorner, Ul2, Ulw2 );
			updateCorner( false, net_rcorner, Ur2, Uwr2 );

			gamma[0] = extractGamma2( As[0], lambda[0] );
			gamma[lat_size-1] = extractGamma2( extractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );
		}
	}
}

//======================================

void ChainIBC::tebd( uni10::CUniTensor hl, uni10::CUniTensor hlw,
	std::vector<uni10::CUniTensor>& hw,
	uni10::CUniTensor hwr, uni10::CUniTensor hr,
	Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS, involving multipications of lambda^-1
	// todo: handle the dimension trncation/expansion to fit chi_max

	// time evolution operator
	uni10::CUniTensor Ul   = expBO( I * dt, hl );
	uni10::CUniTensor Ulw  = expBO( I * dt, hlw );
	uni10::CUniTensor Uw;
	uni10::CUniTensor Uwr  = expBO( I * dt, hwr );
	uni10::CUniTensor Ur   = expBO( I * dt, hr );

	if (orderTS == 1) {
		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				Uw = expBO( I * dt, hw[n] );
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);
			}
			refresh();

			// evolve and update corners
			updateCorner( true, net_lcorner, Ul, Ulw );
			updateCorner( false, net_rcorner, Ur, Uwr );

			gamma[0] = extractGamma2( As[0], lambda[0] );
			gamma[lat_size-1] = extractGamma2( extractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );


			// evolve and update even sites
			for (int n = 1; n < lat_size-1; n+=2) {
				Uw = expBO( I * dt, hw[n] );
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);
			}
			refresh();
		}
	}
	else if (orderTS == 2) {
		uni10::CUniTensor Ul2   = expBO( 0.5 * I * dt, hl );
		uni10::CUniTensor Ulw2  = expBO( 0.5 * I * dt, hlw );
		uni10::CUniTensor Uw2;
		uni10::CUniTensor Uwr2  = expBO( 0.5 * I * dt, hwr );
		uni10::CUniTensor Ur2   = expBO( 0.5 * I * dt, hr );

		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				Uw2 = expBO( 0.5 * I * dt, hw[n] );
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw2);
			}
			refresh();

			// evolve and update corners
			updateCorner( true, net_lcorner, Ul2, Ulw2 );
			updateCorner( false, net_rcorner, Ur2, Uwr2 );

			gamma[0] = extractGamma2( As[0], lambda[0] );
			gamma[lat_size-1] = extractGamma2( extractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );


			// evolve and update even sites
			for (int n = 1; n < lat_size-1; n+=2) {
				Uw = expBO( I * dt, hw[n] );
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);
			}
			refresh();

			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				Uw2 = expBO( 0.5 * I * dt, hw[n] );
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw2);
			}
			refresh();

			// evolve and update corners
			updateCorner( true, net_lcorner, Ul2, Ulw2 );
			updateCorner( false, net_rcorner, Ur2, Uwr2 );

			gamma[0] = extractGamma2( As[0], lambda[0] );
			gamma[lat_size-1] = extractGamma2( extractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );
		}
	}
}

//======================================

void ChainIBC::tebd( std::vector<uni10::CUniTensor>& hw, uni10::CUniTensor hbl, uni10::CUniTensor hbr,
	ChainInf& mps_bkl, ChainInf& mps_bkr, Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS, involving multipications of lambda^-1
	// todo: handle the dimension trncation/expansion to fit chi_max

	// time evolution operator
	uni10::CUniTensor Uw;
	uni10::CUniTensor Ubl = expBO( I * dt, hbl );
	uni10::CUniTensor Ubr = expBO( I * dt, hbr );
	uni10::CUniTensor gtmp, ltmp, thl, thr;

	if (orderTS == 1) {
		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				Uw = expBO( I * dt, hw[n] );
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);
			}

			// evolve and update bdry -- Nishino 2015 paper Fig.A2
			// A2.1
			evol2Site(mps_bkl.lambda[0], mps_bkl.gamma[0], mps_bkl.lambda[1], mps_bkl.gamma[1], mps_bkl.lambda[0], Ubl);
			evol2Site(mps_bkr.lambda[0], mps_bkr.gamma[0], mps_bkr.lambda[1], mps_bkr.gamma[1], mps_bkr.lambda[0], Ubr);
			// A2.2 ~ A2.3
			ltmp = mps_bkl.lambda[1]; gtmp = mps_bkl.gamma[1];
			thl = tensorOp(netLGLGL(ltmp, gtmp, lambda[0], gamma[0], lambda[1]), Ubl);
			gtmp = mps_bkr.gamma[0]; ltmp = mps_bkr.lambda[1];
			thr = tensorOp(netLGLGL(lambda[lat_size-1], gamma[lat_size-1], lambda[lat_size], gtmp, ltmp), Ubr);
			// A2.4
			evol2Site(mps_bkl.lambda[1], mps_bkl.gamma[1], mps_bkl.lambda[0], mps_bkl.gamma[0], mps_bkl.lambda[1], Ubl);
			evol2Site(mps_bkr.lambda[1], mps_bkr.gamma[1], mps_bkr.lambda[0], mps_bkr.gamma[0], mps_bkr.lambda[1], Ubr);
			// A2.5
			gtmp = netLG(mps_bkl.lambda[1], mps_bkl.gamma[1]); gtmp.cTranspose();
			thl.setLabel(std::vector<int> {0, 100, 101, 1}); gtmp.setLabel(std::vector<int> {-1, 0, 100});
			thl = gtmp * thl; thl.permute(2);
			gtmp = netGL(mps_bkr.gamma[0], mps_bkr.lambda[1]); gtmp.cTranspose();
			thr.setLabel(std::vector<int> {0, 100, 101, 1}); gtmp.setLabel(std::vector<int> {1, 2, 101});
			thr = thr * gtmp;
			// back to gamma lambda form
			gamma[0] = extractGamma2( extractGamma(thl, lambda[1]), lambda[0] );
			gamma[lat_size-1] = extractGamma2( extractGamma(thr, lambda[lat_size]), lambda[lat_size-1] );

			// evolve and update even sites
			for (int n = 1; n < lat_size-1; n+=2) {
				Uw = expBO( I * dt, hw[n] );
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);
			}
		}
		refresh();
	}
	else if (orderTS == 2) {
		uni10::CUniTensor Uw2;
		uni10::CUniTensor Ubl2 = expBO( 0.5 * I * dt, hbl );
		uni10::CUniTensor Ubr2 = expBO( 0.5 * I * dt, hbr );

		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				Uw2 = expBO( 0.5 * I * dt, hw[n] );
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw2);
			}

			// evolve and update bdry -- Nishino 2015 paper Fig.A2
			// A2.1
			evol2Site(mps_bkl.lambda[0], mps_bkl.gamma[0], mps_bkl.lambda[1], mps_bkl.gamma[1], mps_bkl.lambda[0], Ubl2);
			evol2Site(mps_bkr.lambda[0], mps_bkr.gamma[0], mps_bkr.lambda[1], mps_bkr.gamma[1], mps_bkr.lambda[0], Ubr2);
			// A2.2 ~ A2.3
			ltmp = mps_bkl.lambda[1]; gtmp = mps_bkl.gamma[1];
			thl = tensorOp(netLGLGL(ltmp, gtmp, lambda[0], gamma[0], lambda[1]), Ubl);
			gtmp = mps_bkr.gamma[0]; ltmp = mps_bkr.lambda[1];
			thr = tensorOp(netLGLGL(lambda[lat_size-1], gamma[lat_size-1], lambda[lat_size], gtmp, ltmp), Ubr);
			// A2.4
			evol2Site(mps_bkl.lambda[1], mps_bkl.gamma[1], mps_bkl.lambda[0], mps_bkl.gamma[0], mps_bkl.lambda[1], Ubl);
			evol2Site(mps_bkr.lambda[1], mps_bkr.gamma[1], mps_bkr.lambda[0], mps_bkr.gamma[0], mps_bkr.lambda[1], Ubr);
			// A2.5
			gtmp = netLG(mps_bkl.lambda[1], mps_bkl.gamma[1]); gtmp.cTranspose();
			thl.setLabel(std::vector<int> {0, 100, 101, 1}); gtmp.setLabel(std::vector<int> {-1, 0, 100});
			thl = gtmp * thl; thl.permute(2);
			gtmp = netGL(mps_bkr.gamma[0], mps_bkr.lambda[1]); gtmp.cTranspose();
			thr.setLabel(std::vector<int> {0, 100, 101, 1}); gtmp.setLabel(std::vector<int> {1, 2, 101});
			thr = thr * gtmp;
			// back to gamma lambda form
			gamma[0] = extractGamma2( extractGamma(thl, lambda[1]), lambda[0] );
			gamma[lat_size-1] = extractGamma2( extractGamma(thr, lambda[lat_size]), lambda[lat_size-1] );
			// A2.1
			evol2Site(mps_bkl.lambda[0], mps_bkl.gamma[0], mps_bkl.lambda[1], mps_bkl.gamma[1], mps_bkl.lambda[0], Ubl2);
			evol2Site(mps_bkr.lambda[0], mps_bkr.gamma[0], mps_bkr.lambda[1], mps_bkr.gamma[1], mps_bkr.lambda[0], Ubr2);

			// evolve and update even sites
			for (int n = 1; n < lat_size-1; n+=2) {
				Uw = expBO( I * dt, hw[n] );
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);
			}

			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				Uw2 = expBO( 0.5 * I * dt, hw[n] );
				evol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw2);
			}
		}
		refresh();
	}
}

//======================================

void ChainIBC::dmrg( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	std::vector<uni10::CUniTensor> mpo;
	for (int i = 0; i < lat_size+2; ++i) {
		if (i == 0)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_l" ) );
		else if (i == lat_size+1)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_r" ) );
		else
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	}

	uni10::CUniTensor gam_L = initGamma(1, chi_max, chi_max);
	gam_L.putBlock( idl.getBlock() );
	uni10::CUniTensor lam_L = initLambda(1);
	lam_L.identity();

	uni10::CUniTensor gam_R = initGamma(chi_max, 1, chi_max);
	gam_R.putBlock( idr.getBlock() );
	uni10::CUniTensor lam_R = initLambda(1);
	lam_R.identity();

	gamma.insert( gamma.begin(), gam_L );
	lambda.insert( lambda.begin(), lam_L );
	gamma.push_back( gam_R );
	lambda.push_back( lam_R );

	uni10::CUniTensor ham;
	uni10::CUniTensor psi;
	double eng;

	for (int s = 0; s < sweeps; ++s) {
		//
		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 1; i < lat_size+1; ++i) {
				//
				ham = netDMRGH( i, gamma, lambda, mpo );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), 3 );

				uni10::CMatrix ham_mtx = ham.getBlock();
				uni10::CMatrix psi_mtx = psi.getBlock();
				int s1 = ham_mtx.lanczosEigh( eng, psi_mtx, iter_max, tolerance );

				psi.putBlock( psi_mtx );
				psi.permute( psi.label(), 2 );

				if (i == lat_size) {
					gamma[i] = extractGamma( extractGamma2( psi, lambda[i] ), lambda[i+1] );
				}
				else {
					std::vector<uni10::CMatrix> svda = psi.getBlock().svd();

					int dim_l = gamma[i].bond()[0].dim();
					int dim_m = gamma[i].bond()[2].dim();
					gamma[i].putBlock( svda[0].resize( dim_l * dim_phys, dim_m ) );
					gamma[i] = extractGamma2( gamma[i], lambda[i] );

					svda[1].resize(dim_m, dim_m);
					svda[1] *= ( 1.0 / svda[1].norm() );
					lambda[i+1].putBlock( svda[1] );

					uni10::CUniTensor vi( lambda[i+1].bond() );
					vi.putBlock( svda[2].resize(dim_m, dim_m) );
					gamma[i+1] = netLG( vi, gamma[i+1] );
				}
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size; i >= 1; --i) {
				//
				ham = netDMRGH( i, gamma, lambda, mpo );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), 3 );

				uni10::CMatrix ham_mtx = ham.getBlock();
				uni10::CMatrix psi_mtx = psi.getBlock();
				int s1 = ham_mtx.lanczosEigh( eng, psi_mtx, iter_max, tolerance );

				psi.putBlock( psi_mtx );
				psi.permute( psi.label(), 1 );

				if (i == 1) {
					psi.permute( psi.label(), 2 );
					gamma[i] = extractGamma( extractGamma2( psi, lambda[i] ), lambda[i+1] );
				}
				else {
					std::vector<uni10::CMatrix> svdb = psi.getBlock().svd();

					int dim_m = gamma[i].bond()[0].dim();
					int dim_r = gamma[i].bond()[2].dim();
					uni10::CUniTensor vi( lambda[i].bond() );
					vi.putBlock( svdb[0].resize(dim_m, dim_m) );
					gamma[i-1] = netGL( gamma[i-1], vi );

					svdb[1].resize(dim_m, dim_m);
					svdb[1] *= ( 1.0 / svdb[1].norm() );
					lambda[i].putBlock( svdb[1] );

					gamma[i].permute( gamma[i].label(), 1 );
					gamma[i].putBlock( svdb[2].resize( dim_m, dim_phys * dim_r ) );
					gamma[i].permute( gamma[i].label(), 2 );
					gamma[i] = extractGamma( gamma[i], lambda[i+1] );
				}
			}
		}
	}

	CanonMPS::vecSlice( gamma, lat_size, 1 );
	CanonMPS::vecSlice( lambda, lat_size+1, 1 );
	mpo.clear();
	refresh();
}

//======================================

void ChainIBC::dmrgImp( std::vector<uni10::CUniTensor>& mpo,
	int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	uni10::CUniTensor gam_L = initGamma(1, chi_max, chi_max);
	gam_L.putBlock( idl.getBlock() );
	uni10::CUniTensor lam_L = initLambda(1);
	lam_L.identity();

	uni10::CUniTensor gam_R = initGamma(chi_max, 1, chi_max);
	gam_R.putBlock( idr.getBlock() );
	uni10::CUniTensor lam_R = initLambda(1);
	lam_R.identity();

	gamma.insert( gamma.begin(), gam_L );
	lambda.insert( lambda.begin(), lam_L );
	gamma.push_back( gam_R );
	lambda.push_back( lam_R );

	uni10::CUniTensor ham;
	uni10::CUniTensor psi;
	double eng = 0.0;

	std::vector<uni10::CUniTensor> mpoH;
	std::vector<uni10::CUniTensor> blk;

	for (int s = 0; s < sweeps; ++s) {
		//
		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 1; i < lat_size+1; ++i) {
				//
				mpoDMRGH( i, true, 1, gamma, lambda, mpo, mpoH, blk );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), psi.bondNum() );
				int s1 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s1 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				psi.permute( psi.label(), 2 );

				if (i == lat_size) {
					gamma[i] = extractGamma( extractGamma2( psi, lambda[i] ), lambda[i+1] );
				}
				else {
					std::vector<uni10::CMatrix> svda = psi.getBlock().svd();

					int dim_l = gamma[i].bond()[0].dim();
					int dim_m = gamma[i].bond()[2].dim();
					gamma[i].putBlock( svda[0].resize( dim_l * dim_phys, dim_m ) );
					gamma[i] = extractGamma2( gamma[i], lambda[i] );

					svda[1].resize(dim_m, dim_m);
					svda[1] *= ( 1.0 / svda[1].norm() );
					lambda[i+1].putBlock( svda[1] );

					uni10::CUniTensor vi( lambda[i+1].bond() );
					vi.putBlock( svda[2].resize(dim_m, dim_m) );
					gamma[i+1] = netLG( vi, gamma[i+1] );
				}

				if (verbose) {
					std::cout << i << '\t' << lat_size-i << '\t';
					std::cout << std::setprecision(12) << entanglementEntropy( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBlk( i, true, 1, gamma, lambda, mpo, blk );
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size; i >= 1; --i) {
				//
				mpoDMRGH( i, false, 1, gamma, lambda, mpo, mpoH, blk );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), psi.bondNum() );
				int s1 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s1 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				psi.permute( psi.label(), 1 );

				if (i == 1) {
					psi.permute( psi.label(), 2 );
					gamma[i] = extractGamma( extractGamma2( psi, lambda[i] ), lambda[i+1] );
				}
				else {
					std::vector<uni10::CMatrix> svdb = psi.getBlock().svd();

					int dim_m = gamma[i].bond()[0].dim();
					int dim_r = gamma[i].bond()[2].dim();
					uni10::CUniTensor vi( lambda[i].bond() );
					vi.putBlock( svdb[0].resize(dim_m, dim_m) );
					gamma[i-1] = netGL( gamma[i-1], vi );

					svdb[1].resize(dim_m, dim_m);
					svdb[1] *= ( 1.0 / svdb[1].norm() );
					lambda[i].putBlock( svdb[1] );

					gamma[i].permute( gamma[i].label(), 1 );
					gamma[i].putBlock( svdb[2].resize( dim_m, dim_phys * dim_r ) );
					gamma[i].permute( gamma[i].label(), 2 );
					gamma[i] = extractGamma( gamma[i], lambda[i+1] );
				}

				if (verbose) {
					std::cout << i-1 << '\t' << lat_size-i+1 << '\t';
					std::cout << std::setprecision(12) << entanglementEntropy( lambda[i] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBlk( i, false, 1, gamma, lambda, mpo, blk );
			}
		}
	}

	CanonMPS::vecSlice( gamma, lat_size, 1 );
	CanonMPS::vecSlice( lambda, lat_size+1, 1 );
	blk.clear();
	mpoH.clear();
	refresh();
}

//======================================

void ChainIBC::dmrgImp( int loc, std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	std::vector<uni10::CUniTensor> mpo;
	for (int i = 0; i < lat_size+2; ++i) {
		if (i == 0)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_l" ) );
		else if (i == lat_size+1)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_r" ) );
		else if (i == loc+1)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_imp" ) );
		else
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	}

	dmrgImp( mpo, sweeps, iter_max, tolerance, verbose );
}

//======================================

void ChainIBC::dmrgImpU2( std::vector<uni10::CUniTensor>& mpo,
	int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	int uc = 2;	// unit_cell size

	uni10::CUniTensor gam_L = initGamma(1, chi_max, chi_max);
	gam_L.putBlock( idl.getBlock() );
	uni10::CUniTensor lam_L = initLambda(1);
	lam_L.identity();

	uni10::CUniTensor gam_R = initGamma(chi_max, 1, chi_max);
	gam_R.putBlock( idr.getBlock() );
	uni10::CUniTensor lam_R = initLambda(1);
	lam_R.identity();

	gamma.insert( gamma.begin(), gam_L );
	lambda.insert( lambda.begin(), lam_L );
	gamma.push_back( gam_R );
	lambda.push_back( lam_R );

	uni10::CUniTensor ham;
	uni10::CUniTensor psi;
	double eng = 0.0;

	std::vector<uni10::CUniTensor> mpoH;
	std::vector<uni10::CUniTensor> blk;

	for (int s = 0; s < sweeps; ++s) {

		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 1; i < lat_size; ++i) {

				mpoDMRGH( i, true, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.label(), psi.bondNum() );
				int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );

				if (verbose) {
					std::cout << i << '\t' << lat_size-i << '\t';
					mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], verbose );
					std::cout << std::setprecision(12) << entanglementEntropy( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				else
					mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], verbose );

				updateDMRGBlk( i, true, uc, gamma, lambda, mpo, blk );
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size; i > 1; --i) {

				mpoDMRGH( i, false, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGLGL( lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), psi.bondNum() );
				int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );

				if (verbose) {
					std::cout << i-1 << '\t' << lat_size-i+1 << '\t';
					mps2SiteSVD( psi, lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1], verbose );
					std::cout << std::setprecision(12) << entanglementEntropy( lambda[i] ) << '\t'
						<< eng << '\n';
				}
				else
					mps2SiteSVD( psi, lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1], verbose );

				updateDMRGBlk( i, false, uc, gamma, lambda, mpo, blk );
			}
		}
	}

	CanonMPS::vecSlice( gamma, lat_size, 1 );
	CanonMPS::vecSlice( lambda, lat_size+1, 1 );
	blk.clear();
	mpoH.clear();
	refresh();
}

//======================================

void ChainIBC::dmrgImpU2( int loc, std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	std::vector<uni10::CUniTensor> mpo;
	for (int i = 0; i < lat_size+2; ++i) {
		if (i == 0)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_l" ) );
		else if (i == lat_size+1)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_r" ) );
		else if (i == loc+1)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_imp" ) );
		else
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	}

	dmrgImpU2( mpo, sweeps, iter_max, tolerance, verbose );
}

//======================================

void ChainIBC::dmrgU2( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	dmrgImpU2( -10, mpo_dir, sweeps, iter_max, tolerance, verbose );
}

//======================================

double eng2S( uni10::CUniTensor ham,
	uni10::CUniTensor lam0, uni10::CUniTensor gam0, uni10::CUniTensor lam1, uni10::CUniTensor gam1 ) {
	///
	uni10::CUniTensor expV0 = expect( netLGLGL( lam0, gam0, lam1, gam1, lam0 ), ham );
	uni10::CUniTensor expV1 = expect( netLGLGL( lam1, gam1, lam0, gam0, lam1 ), ham );

	return 0.5 * ( expV0[0].real() + expV1[0].real() );
}

//======================================

double eng2S( bool left, uni10::CUniTensor ham,
	uni10::CUniTensor A0, uni10::CUniTensor A1, uni10::CUniTensor C0, uni10::CUniTensor C1 ) {
	///
	uni10::CUniTensor expV0, expV1;

	if (left) {
		expV0 = expect( netAA( A0, netGL(A1, C0) ), ham );
		expV1 = expect( netAA( A1, netGL(A0, C1) ), ham );
	}
	else {
		expV0 = expect( netAA( netLG(C0, A1), A0 ), ham );
		expV1 = expect( netAA( netLG(C1, A0), A1 ), ham );
	}

	return 0.5 * ( expV0[0].real() + expV1[0].real() );
}

//======================================

void ChainIBC::idmrg( std::string mpo_dir, int sweeps, int steps, int iter_max, double tolerance, std::string sav_dir ) {
	/// First part: IBC dmrg on initial lattice
	int uc = 2;	// unit_cell size

	std::vector<uni10::CUniTensor> mpo;
	for (int i = 0; i < lat_size+2; ++i) {
		if (i == 0)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_l" ) );
		else if (i == lat_size+1)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_r" ) );
		else
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	}

	uni10::CUniTensor ham2s = op2SiteFromMPO( mpo[1] );

	uni10::CUniTensor gam_L = initGamma(1, chi_max, chi_max);
	gam_L.putBlock( idl.getBlock() );
	uni10::CUniTensor lam_L = initLambda(1);
	lam_L.identity();

	uni10::CUniTensor gam_R = initGamma(chi_max, 1, chi_max);
	gam_R.putBlock( idr.getBlock() );
	uni10::CUniTensor lam_R = initLambda(1);
	lam_R.identity();

	gamma.insert( gamma.begin(), gam_L );
	lambda.insert( lambda.begin(), lam_L );
	gamma.push_back( gam_R );
	lambda.push_back( lam_R );

	uni10::CUniTensor psi;
	double eng = 0.0;

	std::vector<uni10::CUniTensor> mpoH;
	std::vector<uni10::CUniTensor> blk;

	for (int s = 0; s < sweeps; ++s) {

		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 1; i < lat_size; ++i) {
				//
				mpoDMRGH( i, true, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.label(), psi.bondNum() );
				int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );

				mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

				updateDMRGBlk( i, true, uc, gamma, lambda, mpo, blk );
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size; i > 1; --i) {
				//
				mpoDMRGH( i, false, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGLGL( lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), psi.bondNum() );
				int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				//int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );

				mps2SiteSVD( psi, lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1] );

				updateDMRGBlk( i, false, uc, gamma, lambda, mpo, blk );
			}
		}
	}

	//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	/// Second part: idmrg and lattice growing from mid
	int mid = lat_size/2;
	refreshDMRGBlk( mid+1, gamma, lambda, mpo, blk );

	// resize ibc lattice to 2; reset gam/lam vecs
	gamma[1] = gamma[mid+1];
	gamma[2] = gamma[mid];
	gamma[3] = gamma[ gamma.size()-1 ];
	lambda[1] = lambda[mid+1];
	lambda[2] = lambda[mid];
	lambda[3] = lambda[1];
	lambda[4] = lambda[ lambda.size()-1 ];
	CanonMPS::setSize( uc );
	CanonMPS::vecSlice( gamma, lat_size+2, 0 );
	CanonMPS::vecSlice( lambda, lat_size+3, 0 );
	mpo.clear();

	for (int i = 0; i < lat_size+2; ++i) {
		if (i == 0)
			mpo.push_back( blk[mid] );
		else if (i == lat_size+1)
			mpo.push_back( blk[mid+1] );
		else
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	}

	mpoH.clear();
	blk.clear();

	mid = lat_size/2;

	// perform iDMRG
	for (int st = 0; st < steps; ++st) {

		if (st > 0) {

			uni10::CUniTensor gtmp = gamma[1];
			gamma[1] = gamma[2];
			gamma[2] = gtmp;

			uni10::CUniTensor ltmp = lambda[1];
			lambda[1] = lambda[2];
			lambda[2] = ltmp;
			lambda[3] = lambda[1];

			refreshDMRGBlk( 1, gamma, lambda, mpo, blk );
		}

		mpoDMRGH( 1, true, uc, gamma, lambda, mpo, mpoH, blk );
		psi = netLGLGL( lambda[1], gamma[1], lambda[2], gamma[2], lambda[3] );
		psi.permute( psi.label(), psi.bondNum() );
		int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
		//int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );

		mps2SiteSVD( psi, lambda[1], gamma[1], lambda[2], gamma[2], lambda[3] );

		updateDMRGBlk( 1, true, uc, gamma, lambda, mpo, blk );
		//refreshDMRGBlk( i+1, gamma, lambda, mpo, blk );

		blk[0] = blk[1];
		blk[3] = blk[2];
		mpo[0] = blk[0];
		mpo[3] = blk[3];

		eng = eng2S( ham2s, lambda[1], gamma[1], lambda[2], gamma[2] );
		std::cout << st << '\t' << std::setprecision(12) << eng << '\t'
			<< lambda[2].getBlock().at(0,0).real() << '\t' << lambda[2].getBlock().at(1,1).real() << '\t'
			<< lambda[2].getBlock().at(2,2).real() << '\t' << lambda[2].getBlock().at(3,3).real() << '\n';
	}

	if ( sav_dir != "" ) {
		mpo[0].save( sav_dir + "/mpo_l" );
		mpo[3].save( sav_dir + "/mpo_r" );
	}

	CanonMPS::setSize( uc );
	CanonMPS::vecSlice( gamma, lat_size, 1 );
	CanonMPS::vecSlice( lambda, lat_size+1, 1 );
	mpoH.clear();
	blk.clear();
	mpo.clear();
	As.clear();
	refresh();
}

//======================================

void ChainIBC::idmrgResume( std::string mpo_dir, int sweeps, int steps, int iter_max, double tolerance, std::string sav_dir ) {
	/// First part: IBC dmrg on initial lattice
	int uc = 2;	// unit_cell size

	std::vector<uni10::CUniTensor> mpo;
	for (int i = 0; i < uc+2; ++i) {
		if (i == 0)
			mpo.push_back( uni10::CUniTensor( sav_dir + "/mpo_l" ) );
		else if (i == lat_size+1)
			mpo.push_back( uni10::CUniTensor( sav_dir + "/mpo_r" ) );
		else
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	}

	uni10::CUniTensor ham2s = op2SiteFromMPO( mpo[1] );

	uni10::CUniTensor gam_L = initGamma(1, chi_max, chi_max);
	gam_L.putBlock( idl.getBlock() );
	uni10::CUniTensor lam_L = initLambda(1);
	lam_L.identity();

	uni10::CUniTensor gam_R = initGamma(chi_max, 1, chi_max);
	gam_R.putBlock( idr.getBlock() );
	uni10::CUniTensor lam_R = initLambda(1);
	lam_R.identity();

	gamma.insert( gamma.begin(), gam_L );
	lambda.insert( lambda.begin(), lam_L );
	gamma.push_back( gam_R );
	lambda.push_back( lam_R );

	uni10::CUniTensor psi;
	double eng = 0.0;

	std::vector<uni10::CUniTensor> mpoH;
	std::vector<uni10::CUniTensor> blk;

	initDMRGBlk( 2, gamma, lambda, mpo, blk );

	// perform iDMRG
	for (int st = 0; st < steps; ++st) {

		if (st > 0) {

			uni10::CUniTensor gtmp = gamma[1];
			gamma[1] = gamma[2];
			gamma[2] = gtmp;

			uni10::CUniTensor ltmp = lambda[1];
			lambda[1] = lambda[2];
			lambda[2] = ltmp;
			lambda[3] = lambda[1];

			refreshDMRGBlk( 1, gamma, lambda, mpo, blk );
		}

		mpoDMRGH( 1, true, uc, gamma, lambda, mpo, mpoH, blk );
		psi = netLGLGL( lambda[1], gamma[1], lambda[2], gamma[2], lambda[3] );
		psi.permute( psi.label(), psi.bondNum() );
		int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
		//int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );

		mps2SiteSVD( psi, lambda[1], gamma[1], lambda[2], gamma[2], lambda[3] );

		updateDMRGBlk( 1, true, uc, gamma, lambda, mpo, blk );
		//refreshDMRGBlk( i+1, gamma, lambda, mpo, blk );

		blk[0] = blk[1];
		blk[3] = blk[2];
		mpo[0] = blk[0];
		mpo[3] = blk[3];

		eng = eng2S( ham2s, lambda[1], gamma[1], lambda[2], gamma[2] );
		std::cout << st << '\t' << std::setprecision(12) << eng << '\t'
			<< lambda[2].getBlock().at(0,0).real() << '\t' << lambda[2].getBlock().at(1,1).real() << '\t'
			<< lambda[2].getBlock().at(2,2).real() << '\t' << lambda[2].getBlock().at(3,3).real() << '\n';
	}

	if ( sav_dir != "" ) {
		mpo[0].save( sav_dir + "/mpo_l" );
		mpo[3].save( sav_dir + "/mpo_r" );
	}

	CanonMPS::setSize( uc );
	CanonMPS::vecSlice( gamma, lat_size, 1 );
	CanonMPS::vecSlice( lambda, lat_size+1, 1 );
	mpoH.clear();
	blk.clear();
	mpo.clear();
	As.clear();
	refresh();
}

//======================================

void ChainIBC::idmrgAC( double J, double delta, std::string mpo_dir, std::string hb_dir,
	int steps, int iter_max, double tolerance, std::string sav_dir ) {
	///
	// assume initial state and boundary hams are given, with virtual bd = chi_max
	int uc = 2;	// unit_cell size
	double eng = 0.0;
	uni10::CUniTensor AAC, C0, CAA, C1;

	std::vector<uni10::CUniTensor> mpo;
	for (int i = 0; i < uc+2; ++i) {
		if (i == 0)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_l" ) );
		else if (i == uc+1)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_r" ) );
		else
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	}

	uni10::CUniTensor ham2s = op2SiteFromMPO( mpo[1] );

	std::vector<uni10::CUniTensor> mpoH;
	std::vector<uni10::CUniTensor> blk;
	std::vector<uni10::CUniTensor> AL;
	std::vector<uni10::CUniTensor> AR;
	std::vector<uni10::CUniTensor> Cs;

	uni10::CUniTensor gam_L = initGamma(1, chi_max, chi_max);
	gam_L.putBlock( idl.getBlock() );
	uni10::CUniTensor lam_L = initLambda(1);
	lam_L.identity();

	uni10::CUniTensor gam_R = initGamma(chi_max, 1, chi_max);
	gam_R.putBlock( idr.getBlock() );
	uni10::CUniTensor lam_R = initLambda(1);
	lam_R.identity();

	gamma.insert( gamma.begin(), gam_L );
	lambda.insert( lambda.begin(), lam_L );
	gamma.push_back( gam_R );
	lambda.push_back( lam_R );

	AL.push_back( netLG(lambda[0], gamma[0]) );
	AL.push_back( netLG(lambda[1], gamma[1]) );
	AL.push_back( netLG(lambda[2], gamma[2]) );
	AL.push_back( netLG(lambda[3], gamma[3]) );
	AR.push_back( netGL(gamma[3], lambda[4]) );
	AR.push_back( netGL(gamma[2], lambda[3]) );
	AR.push_back( netGL(gamma[1], lambda[2]) );
	AR.push_back( netGL(gamma[0], lambda[1]) );
	Cs.push_back( lambda[3] );
	Cs.push_back( lambda[1] );

	std::vector<uni10::CUniTensor> HBL;
	std::vector<uni10::CUniTensor> HBR;

	J*=0.25;
	delta*=0.25;

	for (int st = 0; st < steps; ++st) {

		if (st > 0) {
			refreshDMRGBlk( 1, AL, AR, Cs[0], mpo, blk );
		}

		// solve AAC
		mpoDMRGH( 1, true, uc, AL, AR, Cs[0], mpo, mpoH, blk );
		AAC = netAA( AL[1], netGL(AL[2], Cs[0]) );
		AAC.permute( AAC.label(), AAC.bondNum() );
		//int s2 = myLanczosEigh( mpoH, AAC, eng, iter_max, tolerance );
		int s2 = myArpLanczosEigh( mpoH, AAC, eng, iter_max, tolerance );

		// solve C0
		refreshDMRGBlk( 3, AL, AR, Cs[0], mpo, blk );
		mpoDMRGH( 3, true, 0, AL, AR, Cs[0], mpo, mpoH, blk );
		C0 = Cs[0];
		C0.permute( C0.label(), C0.bondNum() );
		//int s1 = myLanczosEigh( mpoH, C0, eng, iter_max, tolerance );
		int s1 = myArpLanczosEigh( mpoH, C0, eng, iter_max, tolerance );
		Cs[0] = C0.permute(1);

		// solve C1
		refreshDMRGBlk( 1, AL, AR, Cs[1], mpo, blk );
		mpoDMRGH( 1, true, 0, AL, AR, Cs[1], mpo, mpoH, blk );
		C1 = Cs[1];
		C1.permute( C1.label(), C1.bondNum() );
		//s1 = myLanczosEigh( mpoH, C1, eng, iter_max, tolerance );
		s1 = myArpLanczosEigh( mpoH, C1, eng, iter_max, tolerance );
		Cs[1] = C1.permute(1);

		// AL1AL2 = U*V of AACC's SVD
		int lab_aac[] = {0, 100, 101, 1};
		int lab_c0[] = {2, 1};
		int lab_c0d[] = {1, 2};
		AAC.setLabel( lab_aac );
		C0.setLabel( lab_c0 );
		C0.permute( lab_c0d, 1 );
		C0.conj();

		uni10::CUniTensor AACC = uni10::contract( AAC, C0, false);
		AACC.permute(3);
		std::vector<uni10::CMatrix> svdl = AACC.getBlock().svd();
		uni10::CMatrix AAL_mtx = svdl[0]*svdl[2];
		uni10::CUniTensor AAL( AACC.bond() );
		AAL.putBlock( AAL_mtx );

		for (int i = 0; i < svdl[1].col(); ++i)
			svdl[1].at(i, i) = std::sqrt(svdl[1].at(i, i));
		Cs[0].putBlock( svdl[2].cTranspose() * (svdl[1] * svdl[2]) );

/*
		// find C0 = dominant right eig vec of tranMtx of A1A2
		uni10::CUniTensor tm = tranMtx( AAL );
		uni10::CMatrix tmb = tm.getBlock();
		tmb.transpose();
		std::vector<uni10::CMatrix> tm_eig = tmb.eig();
		Cs[0].permute(0);
		Cs[0].putBlock( tm_eig[1].resize(1, tm_eig[1].col()) );
		Cs[0].permute(1);
*/
		// find AL[1]
		int lab_aal[] = {0, 100, 101, 1};
		int lab_l0[] = {1, 2};
		AAL.setLabel( lab_aal );
		Cs[0].setLabel( lab_l0 );
		uni10::CUniTensor theta = AAL * Cs[0];
		theta.permute(2);
		std::vector<uni10::CMatrix> svdm = theta.getBlock().svd();
		AL[1].putBlock( svdm[0].resize( svdm[0].row(), AL[1].bond()[2].dim() ) );
		svdm[1].resize( lambda[2].bond()[0].dim(), lambda[2].bond()[0].dim() );
		svdm[1] *= 1./svdm[1].norm();
		lambda[2].putBlock( svdm[1] );

		// AL2 = AL1AL2 * AL1_dag
		int lab_alal[] = {0, 100, 101, 1};
		int lab_al[] = {0, 100, 2};
		uni10::CUniTensor AL1d = AL[1];
		AL1d.conj();
		AAL.setLabel( lab_alal );
		AL1d.setLabel( lab_al );
		AL[2] = AL1d * AAL;
		AL[2].permute(2);

		// AR2AR1 = U*V of CCAA's SVD (CARAR = ALALC = AAC)
		int lab_caa[] = {1, 101, 100, 0};
		int lab_c1[] = {1, 2};
		int lab_c1d[] = {2, 1};
		AAC.setLabel( lab_caa );
		C1.setLabel( lab_c1 );
		C1.permute( lab_c1d, 1 );
		C1.conj();
/*
		int lab_ccaa0[] = {2, 0, 101, 100};
		int lab_ccaa1[] = {2, 101, 100, 0};
		uni10::CUniTensor CCAA = C1*AAC;
		CCAA.permute(lab_ccaa0, 1);  // result of svd goes funny if w/o such permutation
		std::vector<uni10::CMatrix> svdr = CCAA.getBlock().svd();
		uni10::CMatrix AAR_mtx = svdr[0]*svdr[2];
		uni10::CUniTensor AAR( CCAA.bond() );
		AAR.putBlock( AAR_mtx );
		AAR.setLabel( lab_ccaa0 );
		AAR.permute( lab_ccaa1, 3 );  // permute back to convention
*/
		uni10::CUniTensor CCAA = uni10::contract( C1, AAC, false );
		CCAA.permute(1);
		std::vector<uni10::CMatrix> svdr = CCAA.getBlock().svd();
		uni10::CMatrix AAR_mtx = svdr[0]*svdr[2];
		uni10::CUniTensor AAR( CCAA.bond() );
		AAR.putBlock( AAR_mtx );
		AAR.permute(3);

		for (int i = 0; i < svdr[1].col(); ++i)
			svdr[1].at(i, i) = std::sqrt(svdr[1].at(i, i));
		Cs[1].putBlock( (svdr[0] * svdr[1]) * svdr[0].cTranspose() );

/*
		// find C1 = dominant left eig vec of tranMtx of B2B1
		uni10::CUniTensor tm2 = tranMtx( AAR );
		uni10::CMatrix tm2b = tm2.getBlock();
		std::vector<uni10::CMatrix> tm2_eig = tm2b.eig();
		Cs[1].permute(0);
		Cs[1].putBlock( tm2_eig[1].resize(1, tm2_eig[1].col()) );
		Cs[1].permute(1);
*/
		// find AR[1]
		int lab_aar[] = {2, 101, 100, 0};
		AAR.setLabel( lab_aar );
		Cs[1].setLabel( lab_l0 );
		theta = Cs[1] * AAR;
		theta.permute(2);
		std::vector<uni10::CMatrix> svdm2 = theta.getBlock().svd();
		AR[1].permute(1);
		AR[1].putBlock( svdm2[2].resize( AR[1].bond()[0].dim(), svdm2[2].col() ) );
		AR[1].permute(2);

		// AR2 = AR2AR1 * AR1_dag
		int lab_arar[] = {0, 100, 101, 1};
		int lab_ar[] = {2, 101, 1};
		uni10::CUniTensor AR1d = AR[1];
		AR1d.conj();
		AAR.setLabel( lab_arar );
		AR1d.setLabel( lab_ar );
		AR[2] = AAR * AR1d;

		// update IBC
		ibcXXZ( J*4., delta*4., AL, AR, Cs, HBL, HBR, iter_max );

		MPO mpo_l(5, 'l');
		MPO mpo_r(5, 'r');

		mpo_l.putTensor( HBL[0], 0, 0 );
		mpo_l.putTensor( J*HBL[2], 0, 1 );
		mpo_l.putTensor( J*I*HBL[3], 0, 2 );
		mpo_l.putTensor( delta*HBL[4], 0, 3 );
		mpo_l.putTensor( HBL[5], 0, 4 );

		mpo_r.putTensor( HBR[5], 0, 0 );
		mpo_r.putTensor( HBR[2], 1, 0 );
		mpo_r.putTensor( I*HBR[3], 2, 0 );
		mpo_r.putTensor( HBR[4], 3, 0 );
		mpo_r.putTensor( HBR[0], 4, 0 );

		mpo[0] = mpo_l.launch();
		mpo[3] = mpo_r.launch();

		// output
		eng = eng2S( true, ham2s, AL[1], AL[2], Cs[0], lambda[2] );

		std::cout << st << '\t' << std::setprecision(12) << eng << '\t'
			<< lambda[2].getBlock().at(0,0).real() << '\t' << lambda[2].getBlock().at(1,1).real() << '\t'
			<< lambda[2].getBlock().at(2,2).real() << '\t' << lambda[2].getBlock().at(3,3).real() << '\n';

	}

	if ( sav_dir != "" ) {
		mpo[0].save( sav_dir + "/mpo_l" );
		mpo[3].save( sav_dir + "/mpo_r" );
	}

	// transform AR Cs to gamma, lambda
	lambda[1] = Cs[1];
	uni10::CUniTensor U = expBO( Complex(0.0, 0.0), ham2s );
	trotterSuzuki(AR[2], AR[1], lambda[2], lambda[1], U, true, chi_max);
	trotterSuzuki(AR[2], AR[1], lambda[2], lambda[1], U, true, chi_max);
	gamma[1] = extractGamma(AR[2], lambda[2]);
	gamma[2] = extractGamma(AR[1], lambda[1]);

	HBL[0].save( hb_dir + "/HL" );
	HBL[1].save( hb_dir + "/HLW" );
	HBL[2].save( hb_dir + "/Sx_L" );
	HBL[3].save( hb_dir + "/_iSy_L" );
	HBL[4].save( hb_dir + "/Sz_L" );
	HBL[5].save( hb_dir + "/Id_L" );

	HBR[0].save( hb_dir + "/HR" );
	HBR[1].save( hb_dir + "/HWR" );
	HBR[2].save( hb_dir + "/Sx_R" );
	HBR[3].save( hb_dir + "/_iSy_R" );
	HBR[4].save( hb_dir + "/Sz_R" );
	HBR[5].save( hb_dir + "/Id_R" );

	CanonMPS::setSize( uc );
	CanonMPS::vecSlice( gamma, uc, 1 );
	CanonMPS::vecSlice( lambda, uc+1, 1 );
	AL.clear();
	AR.clear();
	Cs.clear();
	HBL.clear();
	HBR.clear();
	mpoH.clear();
	blk.clear();
	mpo.clear();
	As.clear();
	refresh();
}
