#include <iostream>
#include <string>

#include <mps/ChainInf.h>
#include <tns-func/func_op.h>
#include <tns-func/func_net.h>
#include <tns-func/func_evol.h>
#include <tns-func/func_meas.h>
#include <tns-func/func_convert.h>
#include <tns-func/func_la.h>
#include <mps-qn/func_qn_meas.h>

//======================================

void _offsetMPOL( uni10::CUniTensor& mpo_l, uni10::CUniTensor& lam ) {
	///
	MPO idl( mpo_l.bond()[1].dim(), 'l' );
	uni10::CUniTensor Id( lam.bond() );
	Id.identity();
	idl.putTensor( Id, 0, 0 );

	uni10::CUniTensor lam_dag = lam;

	int lab_mpol[] = {0, 10, 1};
	int lab_ltop[] = {2, 0};
	int lab_lbot[] = {1, 2};
	mpo_l.setLabel( lab_mpol );
	lam_dag.setLabel( lab_ltop );
	lam.setLabel( lab_lbot );
	uni10::CUniTensor traces = uni10::contract( lam_dag, mpo_l, false );
	traces = uni10::contract( traces, lam, false );

	Complex e0 = traces[0];
	mpo_l = mpo_l + (-1.) * e0 * idl.launch();
}

//======================================

void _offsetMPOR( uni10::CUniTensor& mpo_r, uni10::CUniTensor& lam ) {
	///
	int dim_mpo = mpo_r.bond()[0].dim();
	MPO idr( dim_mpo, 'r' );
	uni10::CUniTensor Id( bondInv(lam).bond() );
	Id.identity();
	idr.putTensor( Id, dim_mpo-1, 0 );

	uni10::CUniTensor lam_dag = lam;

	int lab_mpor[] = {10, 2, 3};
	int lab_ltop[] = {2, 0};
	int lab_lbot[] = {0, 3};
	mpo_r.setLabel( lab_mpor );
	lam_dag.setLabel( lab_ltop );
	lam.setLabel( lab_lbot );
	uni10::CUniTensor traces = uni10::contract( mpo_r, lam, false );
	traces = uni10::contract( traces, lam_dag, false );

	Complex e0 = traces[ dim_mpo-1 ];
	mpo_r = mpo_r + (-1.) * e0 * idr.launch();
}

//======================================

ChainInf::ChainInf(int d, int X) : ChainPBC(2, d, X) {
	/// object constructor
	mon_avail.push_back( "energy" );
	mon_avail.push_back( "entropy" );
	mon_avail.push_back( "spec" );
	mon_avail.push_back( "corr" );
}

//======================================

void ChainInf::monitorOutput( std::vector<monitor> monitors, uni10::CUniTensor ham ) {
	/// set output format of monitoring
	for (int m = 0; m < monitors.size(); ++m) {
		if (monitors[m].name == "energy")
			std::cout << std::setprecision(10) << expVal( ham )[0].real() << "\t";
		else if (monitors[m].name == "entropy")
			std::cout << std::setprecision(10) << entanglementEntropy( lambda[0] ) << "\t";
		else if (monitors[m].name == "spec") {
			uni10::Matrix spec = entanglementSpec( lambda[0], monitors[m].idx );
			for (int i = 0; i < monitors[m].idx; ++i)
				std::cout << std::setprecision(10) << spec.at(i, i) << "\t";
		}
		else if (monitors[m].name == "corr")
			std::cout << std::setprecision(10) << correlationEig( gamma, lambda ) << "\t";
		else
			std::cout << std::setprecision(10) << expVal( uni10::CUniTensor(monitors[m].name) )[0].real() << "\t";
	}
	std::cout << "\n";
}

//======================================

void ChainInf::itebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS ) {
	/// perform iTEBD on iMPS using 2-site evolution operator
	std::vector<uni10::CUniTensor> gl;
	gl.push_back( netGL(gamma[0], lambda[1]) );
	gl.push_back( netGL(gamma[1], lambda[0]) );

	if (orderTS == 1) {
		uni10::CUniTensor U = expBO( I * dt, ham );       // generate exp(Hdt)

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			trotterSuzuki(gl[0], gl[1], lambda[1], lambda[0], U, true, chi_max);
			//trotterSuzuki(lambda[0], gamma[0], lambda[1], gamma[1], lambda[0], U);
		}
	}
	else if (orderTS == 2) {
		uni10::CUniTensor U1 = expBO( I * dt, ham );
		uni10::CUniTensor U2 = expBO( 0.5 * I * dt, ham );

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			trotterSuzuki2(gl[0], gl[1], lambda[1], lambda[0], U2, U1, true, chi_max);
		}
	}
	else if (orderTS == 4) {
		double th = 1.0 / (2.0 - std::pow(2.0, 1./3.));
		uni10::CUniTensor U1 = expBO( I * dt * th * 0.5, ham );
		uni10::CUniTensor U2 = expBO( I * dt * th, ham );
		uni10::CUniTensor U3 = expBO( I * dt * (1.0-th) * 0.5, ham );
		uni10::CUniTensor U4 = expBO( I * dt * (1.0 - 2*th), ham );

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			trotterSuzuki4(gl[0], gl[1], lambda[1], lambda[0], U1, U2, U3, U4, true, chi_max);
		}
	}
	else {
		// raise exception
	}

	gamma[0] = extractGamma(gl[0], lambda[1]);
	gamma[1] = extractGamma(gl[1], lambda[0]);

	gl.clear();
}

//======================================

void ChainInf::itebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS,
	std::vector<monitor> mon_in, int interval ) {
	/// perform iTEBD on iMPS using 2-site evolution operator
	// todo: available monitor list
	std::vector<uni10::CUniTensor> gl;
	gl.push_back( netGL(gamma[0], lambda[1]) );
	gl.push_back( netGL(gamma[1], lambda[0]) );

	std::vector<monitor> mon_out = availMonitors( mon_in, mon_avail );

	if (orderTS == 1) {
		uni10::CUniTensor U = expBO( I * dt, ham );       // generate exp(Hdt)

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			trotterSuzuki(gl[0], gl[1], lambda[1], lambda[0], U, true, chi_max);

			if (ite % interval == 0) {
				gamma[0] = extractGamma(gl[0], lambda[1]);
				gamma[1] = extractGamma(gl[1], lambda[0]);

				monitorOutput( mon_out, ham );
			}
		}
	}
	else if (orderTS == 2) {
		uni10::CUniTensor U1 = expBO( I * dt, ham );
		uni10::CUniTensor U2 = expBO( 0.5 * I * dt, ham );

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			trotterSuzuki2(gl[0], gl[1], lambda[1], lambda[0], U2, U1, true, chi_max);

			if (ite % interval == 0) {
				gamma[0] = extractGamma(gl[0], lambda[1]);
				gamma[1] = extractGamma(gl[1], lambda[0]);

				monitorOutput( mon_out, ham );
			}
		}
	}
	else if (orderTS == 4) {
		double th = 1.0 / (2.0 - std::pow(2.0, 1./3.));
		uni10::CUniTensor U1 = expBO( I * dt * th * 0.5, ham );
		uni10::CUniTensor U2 = expBO( I * dt * th, ham );
		uni10::CUniTensor U3 = expBO( I * dt * (1.0-th) * 0.5, ham );
		uni10::CUniTensor U4 = expBO( I * dt * (1.0 - 2*th), ham );

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			trotterSuzuki4(gl[0], gl[1], lambda[1], lambda[0], U1, U2, U3, U4, true, chi_max);

			if (ite % interval == 0) {
				gamma[0] = extractGamma(gl[0], lambda[1]);
				gamma[1] = extractGamma(gl[1], lambda[0]);

				monitorOutput( mon_out, ham );
			}
		}
	}
	else {
		// raise exception
	}

	gamma[0] = extractGamma(gl[0], lambda[1]);
	gamma[1] = extractGamma(gl[1], lambda[0]);

	gl.clear();
}

//======================================

uni10::CUniTensor ChainInf::expVal( uni10::CUniTensor op, int loc ) {
    /// return expectation value (a contracted uni10) of an 1-site/2-site operator
	std::vector<uni10::CUniTensor> expV;
	expV.push_back( expect(netLGLGL(lambda[0], gamma[0], lambda[1], gamma[1], lambda[0]), op) );
	expV.push_back( expect(netLGLGL(lambda[1], gamma[1], lambda[0], gamma[0], lambda[1]), op) );

	if (loc == 0)
		return expV[0];
	else if (loc == 1)
		return expV[1];

    return 0.5 * (expV[0] + expV[1]);
}

//======================================

uni10::CUniTensor ChainInf::expValStagger( uni10::CUniTensor op ) {
    /// return expectation value (a contracted uni10) of an 1-site/2-site operator
	std::vector<uni10::CUniTensor> expV;
	expV.push_back( expect(netLGLGL(lambda[0], gamma[0], lambda[1], gamma[1], lambda[0]), op) );
	expV.push_back( expect(netLGLGL(lambda[1], gamma[1], lambda[0], gamma[0], lambda[1]), op) );

    return 0.5 * (expV[0] + (-1.0) * expV[1]);
}

//======================================

void ChainInf::idmrg( std::string mpo_dir,
	int steps, int iter_max, double tolerance, std::string sav_dir, bool offset_hb ) {
	///
	std::vector<uni10::CUniTensor> mpo;
	mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_l" ) );
	mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_r" ) );

	uni10::CUniTensor ham;
	uni10::CUniTensor psi;
	double eng = 0.0;
	int uc = 2;

	uni10::CUniTensor ham2s = mpoLR( mpo[0], mpo[3] );	// for energy output

	// initialize trial state
	bool has_init_trial = ( gamma.size() >= 2 && lambda.size() >= 2 && lambda[0].bond()[0].dim() == 1 );

	if ( ! has_init_trial ) {
		// create dummy state
		lambda[0] = initLambda(1);
		gamma[0]  = initGamma(1, dim_phys, dim_phys);
		lambda[1] = initLambda(1);
		gamma[1]  = initGamma(dim_phys, 1, dim_phys);
		lambda[0].identity();
		lambda[1].identity();
		lambda[1] = resizeLambda( lambda[1], dim_phys );
		gamma[0].identity();
		gamma[1].permute( gamma[1].label(), 1 );
		gamma[1].identity();
		gamma[1].permute( gamma[1].label(), 2 );
	}

	for (int st = 0; st < steps; ++st) {

		if (st > 0) {
			uni10::CUniTensor gtmp, ltmp;
			gtmp = gamma[0];
			gamma[0] = gamma[1];
			gamma[1] = gtmp;
			ltmp = lambda[0];
			lambda[0] = lambda[1];
			lambda[1] = ltmp;
		}

		psi = netLGLGL( lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );
		psi.permute( psi.label(), psi.bondNum() );

		if (st == 0) {
			std::vector<uni10::CMatrix> eig2s = ham2s.getBlock().eigh();
			psi.permute( psi.label(), 0 );
			psi.putBlock( eig2s[1].resize(1, eig2s[1].col()) );
			eng = eig2s[0][0].real();
		}
		else {
			int s2 = myLanczosEigh( mpo, psi, eng, iter_max, tolerance );
			//int s2 = myArpLanczosEigh( mpo, psi, eng, iter_max, tolerance );
		}

		mps2SiteSVD( psi, lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );

		// update mpo_l mpo_r
		uni10::CUniTensor Lket = netLG( lambda[0], gamma[0] );
		uni10::CUniTensor Lbra = Lket;
		Lbra.conj();
		uni10::CUniTensor Rket = netGL( gamma[1], lambda[0] );
		uni10::CUniTensor Rbra = Rket;
		Rbra.conj();

		if (st == 0) {
			int lab_Lbra[] = {-1, 100, 0};
			int lab_mpol[] = {100, 1, 101};
			int lab_Lket[] = {-1, 101, 2};
			Lbra.setLabel( lab_Lbra );
			Lket.setLabel( lab_Lket );
			mpo[0].setLabel( lab_mpol );
			mpo[0] = Lbra * mpo[0] * Lket;
			mpo[0].permute( mpo[0].label(), 1 );

			int lab_Rbra[] = {0, 100, -1};
			int lab_mpor[] = {1, 100, 101};
			int lab_Rket[] = {2, 101, -1};
			int lab_newr[] = {1, 0, 2};
			Rbra.setLabel( lab_Rbra );
			Rket.setLabel( lab_Rket );
			mpo[3].setLabel( lab_mpor );
			mpo[3] = Rbra * mpo[3] * Rket;
			mpo[3].permute( lab_newr, 2 );
		}
		else {
			mpo[0] = mpoLM( mpo[0], mpo[1] );
			int lab_Lbra[] = {100, 200, 0};
			int lab_mpol[] = {100, 200, 1, 101, 201};
			int lab_Lket[] = {101, 201, 2};
			Lbra.setLabel( lab_Lbra );
			Lket.setLabel( lab_Lket );
			mpo[0].setLabel( lab_mpol );
			mpo[0] = Lbra * mpo[0] * Lket;
			mpo[0].permute( mpo[0].label(), 1 );

			mpo[3] = mpoMR( mpo[2], mpo[3] );
			int lab_Rbra[] = {0, 100, 200};
			int lab_mpor[] = {1, 100, 200, 101, 201};
			int lab_Rket[] = {2, 101, 201};
			int lab_newr[] = {1, 0, 2};
			Rbra.setLabel( lab_Rbra );
			Rket.setLabel( lab_Rket );
			mpo[3].setLabel( lab_mpor );
			mpo[3] = Rbra * mpo[3] * Rket;
			mpo[3].permute( lab_newr, 2 );
		}

		if ( offset_hb && (st > 0) ) {
			// offset HL/HR diagonal elems
			_offsetMPOL( mpo[0], lambda[1] );
			_offsetMPOR( mpo[3], lambda[1] );
		}

		eng = expVal( ham2s )[0].real();
		std::vector< SchmidtVal > sv = entangleSpecQn( lambda[1], std::min( lambda[1].bond()[0].dim(), 4 ) );
		std::cout << st << '\t' << std::setprecision(12) << eng;
		for (int i = 0; i < sv.size(); ++i)
			std::cout << '\t' << sv[i].val;
		std::cout << '\n';
	}

	if ( sav_dir != "" ) {
		mpo[0].save( sav_dir + "/mpo_l" );
		mpo[3].save( sav_dir + "/mpo_r" );
	}

	mpo.clear();
}

//======================================

void ChainInf::idmrgResume( std::string mpo_dir,
	int steps, int iter_max, double tolerance, std::string sav_dir, bool offset_hb ) {
	///
	std::vector<uni10::CUniTensor> mpo;
	mpo.push_back( uni10::CUniTensor( sav_dir + "/mpo_l" ) );
	mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	mpo.push_back( uni10::CUniTensor( sav_dir + "/mpo_r" ) );

	uni10::CUniTensor ham;
	uni10::CUniTensor psi;
	double eng = 0.0;
	int uc = 2;

	uni10::CUniTensor ham2s = op2SiteFromMPO( mpo[1] );	// for energy output

	for (int st = 0; st < steps; ++st) {

		uni10::CUniTensor gtmp, ltmp;
		gtmp = gamma[0];
		gamma[0] = gamma[1];
		gamma[1] = gtmp;
		ltmp = lambda[0];
		lambda[0] = lambda[1];
		lambda[1] = ltmp;

		psi = netLGLGL( lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );

		psi.permute( psi.label(), psi.bondNum() );
		int s2 = myLanczosEigh( mpo, psi, eng, iter_max, tolerance );
		//int s2 = myArpLanczosEigh( mpo, psi, eng, iter_max, tolerance );

		mps2SiteSVD( psi, lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );

		// update mpo_l mpo_r
		uni10::CUniTensor Lket = netLG( lambda[0], gamma[0] );
		uni10::CUniTensor Lbra = Lket;
		Lbra.conj();
		uni10::CUniTensor Rket = netGL( gamma[1], lambda[0] );
		uni10::CUniTensor Rbra = Rket;
		Rbra.conj();

		mpo[0] = mpoLM( mpo[0], mpo[1] );
		int lab_Lbra[] = {100, 200, 0};
		int lab_mpol[] = {100, 200, 1, 101, 201};
		int lab_Lket[] = {101, 201, 2};
		Lbra.setLabel( lab_Lbra );
		Lket.setLabel( lab_Lket );
		mpo[0].setLabel( lab_mpol );
		mpo[0] = Lbra * mpo[0] * Lket;
		mpo[0].permute( mpo[0].label(), 1 );

		mpo[3] = mpoMR( mpo[2], mpo[3] );
		int lab_Rbra[] = {0, 100, 200};
		int lab_mpor[] = {1, 100, 200, 101, 201};
		int lab_Rket[] = {2, 101, 201};
		int lab_newr[] = {1, 0, 2};
		Rbra.setLabel( lab_Rbra );
		Rket.setLabel( lab_Rket );
		mpo[3].setLabel( lab_mpor );
		mpo[3] = Rbra * mpo[3] * Rket;
		mpo[3].permute( lab_newr, 2 );

		if ( offset_hb && (st > 0) ) {
			// offset HL/HR diagonal elems
			_offsetMPOL( mpo[0], lambda[1] );
			_offsetMPOR( mpo[3], lambda[1] );
		}

		eng = expVal( ham2s )[0].real();
		std::vector< SchmidtVal > sv = entangleSpecQn( lambda[1], std::min( lambda[1].bond()[0].dim(), 4 ) );
		std::cout << st << '\t' << std::setprecision(12) << eng;
		for (int i = 0; i < sv.size(); ++i)
			std::cout << '\t' << sv[i].val;
		std::cout << '\n';
	}

	mpo[0].save( sav_dir + "/mpo_l" );
	mpo[3].save( sav_dir + "/mpo_r" );

	mpo.clear();
}

//======================================

void ChainInf::idmrgSweep( std::string mpo_dir, int steps, int iter_max, double tolerance ) {
	/// mpo_l mpo_r must be dummies
	std::vector<uni10::CUniTensor> mpo(6);
	mpo[1] = uni10::CUniTensor( mpo_dir + "/mpo_m" );
	mpo[2] = mpo[1];
	mpo[3] = mpo[1];
	mpo[4] = mpo[1];
	mpo[0] = mpoDummy( true, mpo[1].bond()[0].dim() );
	mpo[5] = mpoDummy( false, mpo[1].bond()[0].dim() );

	uni10::CUniTensor ham;
	uni10::CUniTensor psi;
	double eng = 0.0;
	int uc = 2;	// unit cell size

	uni10::CUniTensor ham2s = op2SiteFromMPO( mpo[1] );	// for energy output

	// left identity for dmrg sweep
	lambda[0] = initLambda(1);
	gamma[0] = initGamma(1, 1, 1);
	lambda[0].identity();
	gamma[0].identity();

	// 1st site of interest
	lambda[1] = lambda[0];
	gamma[1] = initGamma(1, dim_phys, dim_phys);
	gamma[1].identity();

	// 2nd site of interest
	lambda.push_back( initLambda(dim_phys) );
	gamma.push_back( initGamma(dim_phys, 1, dim_phys) );
	lambda[2].identity();
	gamma[2].permute( gamma[2].label(), 1 );
	gamma[2].identity();
	gamma[2].permute( gamma[2].label(), 2 );

	// 3rd and 4th site
	gamma.push_back( gamma[1] );
	gamma.push_back( gamma[2] );
	lambda.push_back( lambda[1] );
	lambda.push_back( lambda[2] );
	lambda.push_back( lambda[1] );

	// right identity for dmrg sweep
	gamma.push_back( gamma[0] );
	lambda.push_back( lambda[0] );

	// vectors to save unit cells
	std::vector<uni10::CUniTensor> gamL;
	std::vector<uni10::CUniTensor> lamL;
	std::vector<uni10::CUniTensor> gamR;
	std::vector<uni10::CUniTensor> lamR;

	// mpo vectors for dmrg
	std::vector<uni10::CUniTensor> mpoH;
	std::vector<uni10::CUniTensor> blk;

	// dmrg iteration
	for (int st = 0; st < steps; ++st) {

		if (st == 0) {
			// start with ED
			psi = netLGLGL( lambda[1], gamma[1], lambda[2], gamma[2], lambda[3] );
			ham = ham2s;
			std::vector<uni10::CMatrix> eig2s = ham.getBlock().eigh();
			psi.permute( psi.label(), 0 );
			psi.putBlock( eig2s[1].resize(1, eig2s[1].col()) );
			eng = eig2s[0][0].real();

			// update [2]
			mps2SiteSVD( psi, lambda[1], gamma[1], lambda[2], gamma[2], lambda[3] );

			// resize gam, lam to chi_max (then we dont bother bond dim growth)
			gamma[1] = resizeGamma( gamma[1], chi_max, chi_max );
			gamma[2] = resizeGamma( gamma[2], chi_max, chi_max );
			lambda[1] = resizeLambda( lambda[1], chi_max );
			lambda[2] = resizeLambda( lambda[2], chi_max );
			lambda[3] = resizeLambda( lambda[3], chi_max );

			gamma[0] = resizeGamma( gamma[0], 1, chi_max );
			gamma[5] = resizeGamma( gamma[5], chi_max, 1 );

			// update [3]
			gamma[3] = gamma[1];
			gamma[4] = gamma[2];
			lambda[4] = lambda[2];
			lambda[5] = lambda[3];

			// save [2]
			gamL.push_back( gamma[1] );
			gamL.push_back( gamma[2] );
			lamL.push_back( lambda[1] );
			lamL.push_back( lambda[2] );
			lamL.push_back( lambda[3] );

			// save [3]
			gamR.push_back( gamma[3] );
			gamR.push_back( gamma[4] );
			lamR.push_back( lambda[3] );
			lamR.push_back( lambda[4] );
			lamR.push_back( lambda[5] );
		}

		else {

			for (int sw = 0; sw < 4; ++sw) {
				for (int i = 2; i < 2*uc; ++i) {

					mpoDMRGH( i, true, uc, gamma, lambda, mpo, mpoH, blk );
					psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
					psi.permute( psi.label(), psi.bondNum() );
					//int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
					int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
					mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
					updateDMRGBlk( i, true, 2, gamma, lambda, mpo, blk );
				}
				for (int i = 2*uc; i > 2; --i) {

					mpoDMRGH( i, false, uc, gamma, lambda, mpo, mpoH, blk );
					psi = netLGLGL( lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1] );
					psi.permute( psi.label(), psi.bondNum() );
					//int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
					int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
					mps2SiteSVD( psi, lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1] );
					updateDMRGBlk( i, false, 2, gamma, lambda, mpo, blk );
				}
			}

			// sweep right to update [2] and obtain [4]
			for (int i = 2; i < 2*uc; ++i) {

				mpoDMRGH( i, true, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.label(), psi.bondNum() );
				//int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );

				mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

				updateDMRGBlk( i, true, 2, gamma, lambda, mpo, blk );

				if (i == 2) {

					// save [2]
					gamL[0] = gamma[1];
					gamL[1] = gamma[2];
					lamL[0] = lambda[1];
					lamL[1] = lambda[2];
					//lamL[2] = lambda[3];	// keep old lam_21

					// save [3]
					gamR[0] = gamma[3];
					gamR[1] = gamma[4];
					lamR[0] = lambda[3];	// this is now lam_23
					lamR[1] = lambda[4];
					lamR[2] = lambda[5];
				}
			}

			// absorb [2] into mpo_l
			mpo[0] = blk[2];

			// resize boundary identity according to mpo_l
			int dim_rn_l = mpo[0].bond()[2].dim();
			if (gamma[0].bond()[1].dim() < dim_rn_l) {
				gamma[0] = initGamma( 1, dim_rn_l, dim_rn_l );
				gamma[0].identity();
				gamma[0] = resizeGamma( gamma[0], 1, chi_max );
			}

			// shift [4] to 1st 2nd site
			gamma[1] = gamma[3];
			gamma[2] = gamma[4];
			lambda[1] = lambda[3];
			lambda[2] = lambda[4];
			lambda[3] = lambda[5];	// lam_41

			// copy [3] to the right and initialize lam_43
			gamma[3] = gamR[0];
			gamma[4] = gamR[1];
			lambda[3] = netLLL( lambda[3], lamInv( lamL[2] ), lamR[0] );	// lam_43 = lam_41 * lam_21^-1 * lam_23
			lambda[4] = lamR[1];
			lambda[5] = lamR[2];

			//mpo[0] = bdryMPO( mpo[0], lambda[1], true );
			refreshDMRGBlk( 2*uc, gamma, lambda, mpo, blk );


			for (int sw = 0; sw < 4; ++sw) {
				for (int i = 2*uc-1; i > 1; --i) {

					mpoDMRGH( i, false, uc, gamma, lambda, mpo, mpoH, blk );
					psi = netLGLGL( lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1] );
					psi.permute( psi.label(), psi.bondNum() );
					//int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
					int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
					mps2SiteSVD( psi, lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1] );
					updateDMRGBlk( i, false, 2, gamma, lambda, mpo, blk );
				}
				for (int i = 1; i < 2*uc-1; ++i) {

					mpoDMRGH( i, true, uc, gamma, lambda, mpo, mpoH, blk );
					psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
					psi.permute( psi.label(), psi.bondNum() );
					//int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
					int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
					mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
					updateDMRGBlk( i, true, 2, gamma, lambda, mpo, blk );
				}
			}

			// sweep left to update [3] and obtain [5]
			for (int i = 2*uc-1; i > 1; --i) {

				mpoDMRGH( i, false, uc, gamma, lambda, mpo, mpoH, blk );
				psi = netLGLGL( lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), psi.bondNum() );
				//int s2 = myLanczosEigh( mpoH, psi, eng, iter_max, tolerance );
				int s2 = myArpLanczosEigh( mpoH, psi, eng, iter_max, tolerance );

				mps2SiteSVD( psi, lambda[i-1], gamma[i-1], lambda[i], gamma[i], lambda[i+1] );

				updateDMRGBlk( i, false, 2, gamma, lambda, mpo, blk );

				if (i == 2*uc-1) {

					// save [4]
					gamL[0] = gamma[1];
					gamL[1] = gamma[2];
					lamL[0] = lambda[1];
					lamL[1] = lambda[2];
					lamL[2] = lambda[3];	// this is now lam_43

					// save [3]
					gamR[0] = gamma[3];
					gamR[1] = gamma[4];
					//lamR[0] = lambda[3];	// keep old lam_23
					lamR[1] = lambda[4];
					lamR[2] = lambda[5];
				}
			}

			// absorb [3] into mpo_r
			mpo[5] = blk[3];

			// resize boundary identity according to mpo_r
			int dim_rn_r = mpo[5].bond()[2].dim();
			if (gamma[5].bond()[1].dim() < dim_rn_r) {
				gamma[5] = initGamma( dim_rn_r, 1, dim_rn_r );
				gamma[5].permute(1);
				gamma[5].identity();
				gamma[5].permute(2);
				gamma[5] = resizeGamma( gamma[5], chi_max, 1 );
			}

			// shift [5] to 3rd 4th site
			gamma[3] = gamma[1];
			gamma[4] = gamma[2];
			lambda[3] = lambda[1];	// lam_25
			lambda[4] = lambda[2];
			lambda[5] = lambda[3];

			// copy [4] to the left and initialize lam_45
			gamma[1] = gamL[0];
			gamma[2] = gamL[1];
			lambda[1] = lamL[0];
			lambda[2] = lamL[1];
			lambda[3] = netLLL( lamL[2], lamInv(lamR[0]) , lambda[3] );	// lam_45 = lam_43 * lam_23^-1 * lam_25

			//mpo[5] = bdryMPO( mpo[5], lambda[5], false );
			refreshDMRGBlk( 2, gamma, lambda, mpo, blk );
		}

		if (st > 1) {
			eng = 0.5* ( expect( netLGLGL(lambda[3], gamma[3], lambda[4], gamma[4], lambda[3]), ham2s )[0].real()
				+ expect( netLGLGL(lambda[4], gamma[4], lambda[3], gamma[3], lambda[4]), ham2s )[0].real() );
			std::cout << std::setprecision(12) << eng << '\t'
				<< lambda[4].getBlock().at(0,0).real() << '\t' << lambda[4].getBlock().at(1,1).real() << '\t'
				<< lambda[4].getBlock().at(2,2).real() << '\t' << lambda[4].getBlock().at(3,3).real() << '\n';
		}
	}

	CanonMPS::vecSlice( gamma, uc, 3 );
	CanonMPS::vecSlice( lambda, uc, 3 );
	mpoH.clear();
	blk.clear();
	mpo.clear();
}

//======================================

void ChainInf::mps4SiteSVD( uni10::CUniTensor& psi,
	uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1,
	uni10::CUniTensor& lam2, uni10::CUniTensor& gam2,
	uni10::CUniTensor& lam3, uni10::CUniTensor& gam3, uni10::CUniTensor& lam4 ) {
	///
	psi.permute( psi.label(), psi.bondNum()/2 );
	std::vector<uni10::CMatrix> svda = psi.getBlock().svd();

	int dim_l = gam0.bond()[0].dim();
	int dim_m = std::min( (int)svda[1].col(), chi_max );
	int dim_r = gam3.bond()[2].dim();

	if ( dim_m != gam1.bond()[2].dim() )
		gam1 = initGamma(gam1.bond()[0].dim(), dim_m, dim_phys );
	if ( dim_m != lam2.bond()[0].dim() )
		lam2 = initLambda(dim_m);
	if ( dim_m != gam2.bond()[0].dim() )
		gam2 = initGamma(dim_m, gam2.bond()[2].dim(), dim_phys );

	std::vector<uni10::Bond> bdl;
	bdl.push_back( uni10::Bond( uni10::BD_IN, dim_l ) );
	bdl.push_back( uni10::Bond( uni10::BD_IN, dim_phys ) );
	bdl.push_back( uni10::Bond( uni10::BD_IN, dim_phys ) );
	bdl.push_back( uni10::Bond( uni10::BD_OUT, dim_m ) );
	uni10::CUniTensor blk_l( bdl );

	std::vector<uni10::Bond> bdr;
	bdr.push_back( uni10::Bond( uni10::BD_IN, dim_m ) );
	bdr.push_back( uni10::Bond( uni10::BD_OUT, dim_phys ) );
	bdr.push_back( uni10::Bond( uni10::BD_OUT, dim_phys ) );
	bdr.push_back( uni10::Bond( uni10::BD_OUT, dim_r ) );
	uni10::CUniTensor blk_r( bdr );

	blk_l.putBlock( svda[0].resize( dim_l * dim_phys * dim_phys, dim_m ) );

	svda[1].resize(dim_m, dim_m);
	svda[1] *= ( 1.0 / svda[1].norm() );
	lam2.putBlock( svda[1] );

	blk_r.putBlock( svda[2].resize( dim_m, dim_phys * dim_phys * dim_r ) );

	int lab_bl[] = {0, 100, 101, 1};
	int lab_br[] = {2, 102, 103, 3};
	int lab_lm[] = {1, 2};
	blk_l.setLabel( lab_bl );
	blk_r.setLabel( lab_br );
	lam2.setLabel( lab_lm );

	blk_l = uni10::contract( blk_l, lam2, false );
	blk_r = uni10::contract( lam2, blk_r, false );

	mps2SiteSVD( blk_l, lam0, gam0, lam1, gam1, lam2 );
	mps2SiteSVD( blk_r, lam2, gam2, lam3, gam3, lam4 );
}

//======================================

void ChainInf::idmrgUC( std::string mpo_dir, int steps, int iter_max, double tolerance ) {
	///
	std::vector<uni10::CUniTensor> mpo;
	mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_l" ) );
	mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_r" ) );

	uni10::CUniTensor ham;
	uni10::CUniTensor psi;
	double eng = 0.0, eng4s = 0.0, elast = 0.0;
	int uc = 2;

	std::vector<uni10::CUniTensor> mpoH4S;
	std::vector<uni10::CUniTensor> mpoH2S;
	std::vector<uni10::CUniTensor> blk;

	// initial dummy state
	lambda[0] = initLambda(1);
	gamma[0] = initGamma(1, dim_phys, dim_phys);
	lambda[1] = initLambda(dim_phys);
	gamma[1] = initGamma(dim_phys, 1, dim_phys);
	lambda[0].identity();
	lambda[1].identity();
	gamma[0].identity();
	gamma[1].permute( gamma[1].label(), 1 );
	gamma[1].identity();
	gamma[1].permute( gamma[1].label(), 2 );
	lambda.push_back( lambda[0] );


	for (int st = 0; st < steps; ++st) {

		if (st == 0) {

			ham = mpoLR( mpo[0], mpo[3] );
			std::vector<uni10::CMatrix> eig2s = ham.getBlock().eigh();
			psi = netLGLGL( lambda[0], gamma[0], lambda[1], gamma[1], lambda[2] );
			psi.permute( psi.label(), 0 );
			psi.putBlock( eig2s[1].resize(1, eig2s[1].col()) );
			eng = eig2s[0][0].real();

			mps2SiteSVD( psi, lambda[0], gamma[0], lambda[1], gamma[1], lambda[2] );
		}

		else if (st == 1) {

			// append 2 sites at the right
			gamma.push_back( gamma[0] );
			gamma.push_back( gamma[1] );
			lambda.push_back( lambda[1] );
			lambda.push_back( lambda[0] );

			// 4-site eigen
			ham = mpoLMMR( mpo[0], mpo[1], mpo[3] );
			std::vector<uni10::CMatrix> eig4s = ham.getBlock().eigh();
			psi = net4Site( lambda[0], gamma[0], lambda[1], gamma[1],
							lambda[2], gamma[2], lambda[3], gamma[3], lambda[4] );
			psi.permute( psi.label(), 0 );
			psi.putBlock( eig4s[1].resize(1, eig4s[1].col()) );
			eng = eig4s[0][0].real();
			//elast = eng;
			eng4s = eng;

			// 4-site SVD
			mps4SiteSVD( psi, lambda[0], gamma[0], lambda[1], gamma[1],
						lambda[2], gamma[2], lambda[3], gamma[3], lambda[4] );

			// renew mpo and blks
			initDMRGBlk( 2, gamma, lambda, mpo, blk );
			mpoH2S.push_back( blk[1] );
			mpoH2S.push_back( mpo[1] );
			mpoH2S.push_back( mpo[2] );
			mpoH2S.push_back( blk[2] );

			// insert 2 sites at the center
			gamma.insert( gamma.begin()+2, gamma[2] );
			gamma.insert( gamma.begin()+3, gamma[1] );
			lambda.insert( lambda.begin()+3, lambda[1] );
			lambda.insert( lambda.begin()+4, lambda[2] );	// now len(gam) = 6

			// 2-site eigen (4 phys bonds) using Lanczos
			psi = netLGLGL( lambda[2], gamma[2], lambda[3], gamma[3], lambda[4] );
			uni10::CUniTensor noise( psi.bond() );
			noise.randomize();
			psi = psi + 0.05 * noise;
			psi.permute( psi.label(), psi.bondNum() );
			//int s2 = myLanczosEigh( mpoH2S, psi, eng, iter_max, tolerance );
			int s2 = myArpLanczosEigh( mpoH2S, psi, eng, iter_max, tolerance );

			// SVD
			mps2SiteSVD( psi, lambda[2], gamma[2], lambda[3], gamma[3], lambda[4] );

			uni10::CUniTensor left = netLGLGL( lambda[1], gamma[1], lambda[2], gamma[2], lambda[3] );
			uni10::CUniTensor right = netLGLGL( lambda[3], gamma[3], lambda[4], gamma[4], lambda[5] );
			mps2SiteSVD( left, lambda[1], gamma[1], lambda[2], gamma[2], lambda[3] );
			mps2SiteSVD( right, lambda[3], gamma[3], lambda[4], gamma[4], lambda[5] );

			// build mpoH4S for next iteration
			mpoH4S.push_back( mpo[0] );
			mpoH4S.push_back( mpo[1] );
			mpoH4S.push_back( mpo[1] );
			mpoH4S.push_back( mpo[1] );
			mpoH4S.push_back( mpo[1] );
			mpoH4S.push_back( mpo[3] );

			blk.clear();
			initDMRGBlk( 3, gamma, lambda, mpoH4S, blk );
			mpoH4S[0] = blk[1];
			mpoH4S[5] = blk[4];
		}

		else {
			int dim_bd = mpoH4S[0].bond()[2].dim();
			gamma[0] = initGamma(1, dim_bd, dim_bd);
			gamma[0].identity();
			gamma[5] = initGamma(dim_bd, 1, dim_bd);
			gamma[5].permute(1);
			gamma[5].identity();
			gamma[5].permute(2);

			gamma[1] = gamma[2];
			gamma[2] = gamma[3];
			gamma[3] = gamma[1];
			gamma[4] = gamma[2];
			lambda[1] = lambda[2];
			lambda[2] = lambda[3];
			lambda[3] = lambda[1];
			lambda[4] = lambda[2];
			lambda[5] = lambda[1];

			psi = net4Site( lambda[1], gamma[1], lambda[2], gamma[2],
							lambda[3], gamma[3], lambda[4], gamma[4], lambda[5] );
			psi.permute( psi.label(), psi.bondNum() );
			//int s4 = myLanczosEigh( mpoH4S, psi, eng, iter_max, tolerance );
			int s4 = myArpLanczosEigh( mpoH4S, psi, eng, iter_max, tolerance );
			//elast = eng;
			eng4s = eng;

			mps4SiteSVD( psi, lambda[1], gamma[1], lambda[2], gamma[2],
						lambda[3], gamma[3], lambda[4], gamma[4], lambda[5] );

			// renew mpo and blks
			refreshDMRGBlk( 3, gamma, lambda, mpoH4S, blk );
			mpoH2S[0] = blk[2];
			mpoH2S[3] = blk[3];

			mpoH4S[0] = blk[1];
			mpoH4S[5] = blk[4];

			// insert 2 sites at center ( reassign gam/lam )
			gamma[1] = gamma[2];
			gamma[4] = gamma[3];
			gamma[2] = gamma[4];
			gamma[3] = gamma[1];
			lambda[1] = lambda[2];
			lambda[5] = lambda[4];
			lambda[2] = lambda[3];
			lambda[3] = lambda[1];
			lambda[4] = lambda[2];

			// 2-site eigen (4 phys bonds) using Lanczos
			psi = netLGLGL( lambda[2], gamma[2], lambda[3], gamma[3], lambda[4] );
			//uni10::CUniTensor noise( psi.bond() );
			//noise.randomize();
			//psi = psi + 0.05 * noise;
			psi.permute( psi.label(), psi.bondNum() );
			//int s2 = myLanczosEigh( mpoH2S, psi, eng, iter_max, tolerance );
			int s2 = myArpLanczosEigh( mpoH2S, psi, eng, iter_max, tolerance );

			// SVD
			mps2SiteSVD( psi, lambda[2], gamma[2], lambda[3], gamma[3], lambda[4] );

			if (st < steps-1) {
				uni10::CUniTensor left = netLGLGL( lambda[1], gamma[1], lambda[2], gamma[2], lambda[3] );
				uni10::CUniTensor right = netLGLGL( lambda[3], gamma[3], lambda[4], gamma[4], lambda[5] );
				mps2SiteSVD( left, lambda[1], gamma[1], lambda[2], gamma[2], lambda[3] );
				mps2SiteSVD( right, lambda[3], gamma[3], lambda[4], gamma[4], lambda[5] );
			}

			// build mpoH4S for next iteration
			dim_bd = mpoH4S[0].bond()[2].dim();
			gamma[0] = initGamma(1, dim_bd, dim_bd);
			gamma[0].identity();
			gamma[5] = initGamma(dim_bd, 1, dim_bd);
			gamma[5].permute(1);
			gamma[5].identity();
			gamma[5].permute(2);

			refreshDMRGBlk( 3, gamma, lambda, mpoH4S, blk );
			mpoH4S[0] = blk[1];
			mpoH4S[5] = blk[4];
		}

		if (st == 0) {
			std::cout << st << "\t" << std::setprecision(12) << 0.5*(eng-elast) << "\t"
				<< lambda[1].getBlock().at(0,0).real() << "\t" << lambda[1].getBlock().at(1,1).real() << "\n";
			elast = eng;
		}
		else if (st == 1) {
			std::cout << st << "\t" << std::setprecision(12) << 0.5*(eng4s-elast) << "\t"
				<< lambda[3].getBlock().at(0,0).real() << "\t" << lambda[3].getBlock().at(1,1).real() << "\t"
				<< lambda[3].getBlock().at(2,2).real() << "\t" << lambda[3].getBlock().at(3,3).real() << "\n";
			elast = eng4s;
		}
		else {
			std::cout << st << "\t" << std::setprecision(12) << 0.25*(eng4s-elast) << "\t"
				<< lambda[3].getBlock().at(0,0).real() << "\t" << lambda[3].getBlock().at(1,1).real() << "\t"
				<< lambda[3].getBlock().at(2,2).real() << "\t" << lambda[3].getBlock().at(3,3).real() << "\n";
			elast = eng4s;
		}
	}

	CanonMPS::vecSlice( gamma, uc, 2 );
	CanonMPS::vecSlice( lambda, uc, 2 );
	mpoH2S.clear();
	blk.clear();
	mpo.clear();
}
