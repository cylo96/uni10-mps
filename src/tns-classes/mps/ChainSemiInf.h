#ifndef CHAINSEMIINF_H
#define CHAINSEMIINF_H
#include "CanonMPS.h"

/// ref: PHYSICAL REVIEW B 86, 245107 (2012)
class ChainSemiInf: public CanonMPS {

public:
	/// constructor
	ChainSemiInf(int L, int d, int X);
	~ChainSemiInf();

	uni10::CUniTensor getAs(int idx);
	void putAs(uni10::CUniTensor A, int idx = -1);
	void refresh();
	void randomize();
	void expand( int add_right, std::string dirname, int unit_cell = 2 );
	void importMPS( std::string dirname, int unit_cell = 2 );
	void loadNetIBC( std::string net_dir = "." );

	void oneSiteOP( uni10::CUniTensor op, int idx = 0 );

	void tebd( uni10::CUniTensor hlw, uni10::CUniTensor hw,
		uni10::CUniTensor hwr, uni10::CUniTensor hr, Complex dt, int steps, int orderTS = 1 );

	void dmrg( std::vector<uni10::CUniTensor>& mpo,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrg( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgU2( std::vector<uni10::CUniTensor>& mpo,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgU2( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose = false );

	uni10::CUniTensor expVal( uni10::CUniTensor op, int idx );

	uni10::CUniTensor correlation(
		uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 );

private:
	std::vector<uni10::CUniTensor> As;
	uni10::CUniTensor idl;
	uni10::CUniTensor idr;
	uni10::CUniTensor dummy;

	void updateCorner( uni10::CNetwork net, uni10::CUniTensor Ub, uni10::CUniTensor Ubw );

	uni10::CNetwork net_r;
	uni10::CNetwork net_l;
	uni10::CNetwork net_contr;
	uni10::CNetwork net_contr_o1;
	uni10::CNetwork net_contr_o2;
	uni10::CNetwork net_contr_last;
	uni10::CNetwork net_rcorner;

	bool net_ibc_loaded = false;
	std::string net_ibc_dir = ".";

};

#endif
