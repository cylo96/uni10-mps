#include <iostream>
#include <cstdlib>
#include <algorithm>	// std::find
#include <stdexcept>
#include <time.h>
#include <sys/stat.h>

#include <mps-qn/YJuncQnIBC.h>
#include <mps-qn/func_qn_la.h>
#include <mps-qn/func_qn_op.h>
#include <mps-qn/func_qn_net.h>
#include <mps-qn/func_qn_meas.h>
#include <mps-qn/func_qn_blkops.h>
#include <mps-qn/func_qn_sycfg.h>
#include <tns-func/func_net.h>
#include <tns-func/func_op.h>

//======================================

YJuncQnIBC::YJuncQnIBC( int L, int X, std::vector<uni10::Qnum>& q_phys, std::vector<uni10::Qnum>& q_virt ) :
	CanonQnMPS( 1, 1, q_phys, q_virt ) {
	/// object constructor
	// todo: assert L even
	wire1 = ChainQnIBC( L, X, q_phys, q_virt );
	wire2 = ChainQnIBC( L, X, q_phys, q_virt );
	wire3 = ChainQnIBC( L, X, q_phys, q_virt );
	lat_size = L;
	chi_max = X;
	chi_junc = X;

	fermi = qphys[0].isFermionic();
	u1 = ((qphys[0].U1() != 0) || (qphys[qphys.size()-1].U1() != 0 ));
}

//======================================

YJuncQnIBC::YJuncQnIBC( int L, int X, std::vector<uni10::Qnum>& q_phys ) :
	CanonQnMPS( 1, 1, q_phys ) {
	/// object constructor
	qvirt.push_back( uni10::Qnum(0) );
	YJuncQnIBC( L, X, q_phys, qvirt );
}

//======================================

void YJuncQnIBC::setBDJT( int X ) { chi_junc = X; }

//======================================

void YJuncQnIBC::setBDRamp( double r ) { chi_ramp = r; }

//======================================

void YJuncQnIBC::buildJT() {
	///
	// junction triangle tensor
	if (!net_yj_loaded)
		loadNetYJ(net_yj_dir);

	net_jt.putTensor( "gamJ1", wire1.gamma[lat_size-1] );
	net_jt.putTensor( "gamJ2", wire2.gamma[0] );
	net_jt.putTensor( "gamJ3", wire3.gamma[0] );
	net_jt.putTensor( "lam12", wire1.lambda[lat_size] );
	net_jt.putTensor( "lam23", wire2.lambda[0] );
	net_jt.putTensor( "lam13", wire3.lambda[0] );
	jt = net_jt.launch(false);
}

//======================================

void YJuncQnIBC::initJT() {
	///
	std::vector<uni10::Bond> bdj;
	bdj.push_back(uni10::Bond(uni10::BD_IN, wire1.lambda[lat_size-1].bond()[1].Qlist()));
	bdj.push_back(uni10::Bond(uni10::BD_IN, qphys));
	bdj.push_back(uni10::Bond(uni10::BD_IN, qphys));
	bdj.push_back(uni10::Bond(uni10::BD_IN, qphys));
	bdj.push_back(uni10::Bond(uni10::BD_OUT, wire2.lambda[1].bond()[0].Qlist()));
	bdj.push_back(uni10::Bond(uni10::BD_OUT, wire3.lambda[1].bond()[0].Qlist()));
	jt.assign(bdj);
}

//======================================

void YJuncQnIBC::init() {
	///
	// assert qin_virt even at site-0 (left most site)
	// wire1.lam_last = lam_junc12; wire2.lam_0 = lam_jun23; wire3.lam_0 = lam_junc31
	wire1.init();
	wire2.init();
	wire3.init();
	initJT();
}

//======================================

void YJuncQnIBC::randomize() {
	/// randomize a complex MPS having only real part
	if (wire1.gamma.size() < lat_size)
		YJuncQnIBC::init();

	std::srand( time(NULL) );
	for (int i = 0; i < lat_size; ++i) {
		wire1.gamma[i] = randTQn( wire1.gamma[i] );
		wire2.gamma[i] = randTQn( wire2.gamma[i] );
		wire3.gamma[i] = randTQn( wire3.gamma[i] );
	}
	for (int i = 0; i < lat_size+1; ++i) {
		wire1.lambda[i] = randTQn( wire1.lambda[i] );
		wire2.lambda[i] = randTQn( wire2.lambda[i] );
		wire3.lambda[i] = randTQn( wire3.lambda[i] );
	}
	jt = randTQn(jt);
}

//======================================

void YJuncQnIBC::expand(int add_end, std::string dirname, int unit_cell) {
	/// expand the MPS with add_end sites to each wire
	// todo: raise exception if gam/lam vectors empty
	if (!wire1.gamma.empty() && !wire2.gamma.empty() && !wire3.gamma.empty()) {
		std::vector< uni10::CUniTensor > ext_gam;
		std::vector< uni10::CUniTensor > ext_lam;
		for (int idx = 0; idx < unit_cell; ++idx) {
			ext_gam.push_back( uni10::CUniTensor( dirname + "/gamma_" + std::to_string((long long)idx ) ) );
			ext_lam.push_back( uni10::CUniTensor( dirname + "/lambda_" + std::to_string((long long)idx ) ) );
		}

		for (int i = 0; i < add_end; i+=unit_cell) {
			for (int j = 0; j < unit_cell; ++j) {
				wire1.gamma.insert( wire1.gamma.begin(), ext_gam[ unit_cell - j - 1 ] );
				wire1.lambda.insert( wire1.lambda.begin(), ext_lam[ unit_cell - j - 1 ] );
			}
		}
		wire1.setSize( lat_size + add_end );

		for (int i = 0; i < add_end; i+=unit_cell) {
			for (int j = 0; j < unit_cell; ++j) {
				wire2.gamma.push_back( ext_gam[ j ] );
				wire2.lambda.push_back( ext_lam[ (j+1)%unit_cell ] );
			}
		}
		wire2.setSize( lat_size + add_end );

		for (int i = 0; i < add_end; i+=unit_cell) {
			for (int j = 0; j < unit_cell; ++j) {
				wire3.gamma.push_back( ext_gam[ j ] );
				wire3.lambda.push_back( ext_lam[ (j+1)%unit_cell ] );
			}
		}
		wire3.setSize( lat_size + add_end );

		ext_gam.clear();
		ext_lam.clear();
	}
	if (wire1.gamma.empty() || wire1.lambda.empty())
		wire1.setSize( lat_size + add_end );
	if (wire2.gamma.empty() || wire2.lambda.empty())
		wire2.setSize( lat_size + add_end );
	if (wire3.gamma.empty() || wire3.lambda.empty())
		wire3.setSize( lat_size + add_end );
}

//======================================

uni10::CUniTensor YJuncQnIBC::getGamma( int wire, int idx ) {
	///
	if (wire == 1)
		return wire1.getGamma(idx);
	else if (wire == 2)
		return wire2.getGamma(idx);
	else if (wire == 3)
		return wire3.getGamma(idx);
	else {
		std::cerr << "In YJuncQnIBC::getGamma : Invalid index." << '\n';
	}
}

//======================================

uni10::CUniTensor YJuncQnIBC::getLambda( int wire, int idx ) {
	///
	if (wire == 1)
		return wire1.getLambda(idx);
	else if (wire == 2)
		return wire2.getLambda(idx);
	else if (wire == 3)
		return wire3.getLambda(idx);
	else {
		std::cerr << "In YJuncQnIBC::getLambda : Invalid index." << '\n';
	}
}

//======================================

uni10::CUniTensor YJuncQnIBC::getJT() { return jt; }

//======================================

const std::vector<uni10::CUniTensor>& YJuncQnIBC::getGamVec(int wire) const {
	/// return the reference of gamma vector
	if (wire == 1)
		return wire1.gamma;
	else if (wire == 2)
		return wire2.gamma;
	else if (wire == 3)
		return wire3.gamma;
	else
		std::cerr << "In YJuncQnIBC::getGamVec : Invalid wire index." << '\n';
}

//======================================

const std::vector<uni10::CUniTensor>& YJuncQnIBC::getLamVec(int wire) const {
	/// return the reference of gamma vector
	if (wire == 1)
		return wire1.lambda;
	else if (wire == 2)
		return wire2.lambda;
	else if (wire == 3)
		return wire3.lambda;
	else
		std::cerr << "In YJuncQnIBC::getLamVec : Invalid wire index." << '\n';
}

//======================================

void YJuncQnIBC::importMPS(
	std::string dir_w1, std::string dir_w2, std::string dir_w3, int unit_cell, bool sep_gj ) {
	///
	// fit_chi in CanonMPS doesn't work for Qn classes => set false
	// assert the mps to import has the same length as lat_size
	wire1.CanonMPS::importMPS( dir_w1, unit_cell, true, false );
	wire2.CanonMPS::importMPS( dir_w2, unit_cell, true, false );
	wire3.CanonMPS::importMPS( dir_w3, unit_cell, true, false );
	if (wire1.gamma[lat_size-1].bondNum() != 4 || wire2.gamma[0].bondNum() != 4 || wire3.gamma[0].bondNum() != 4)
		sep_gj = false;

	if (sep_gj)
		buildJT();
	else {
		try {
			jt = uni10::CUniTensor( dir_w1 + "/junct" );
		}
		catch(const std::exception& e) {
			initJT();
			jt = randTQn(jt);
		}
	}
}

//======================================

void YJuncQnIBC::importMPS( std::string dirname, int unit_cell, bool sep_gj ) {
	///
	try {
		importMPS( dirname + "/wire1", dirname + "/wire2", dirname + "/wire3", unit_cell, sep_gj );
	}
	catch(const std::exception& e) {
		importMPS( dirname, dirname, dirname, unit_cell, sep_gj );
	}
}

//======================================

void YJuncQnIBC::importWireMPS(
	std::string dir_w1, std::string dir_w2, std::string dir_w3, int unit_cell ) {
	///
	// import bulk mps to replace current tensors on wires except around WJT
	int chi_wire = uni10::CUniTensor(dir_w1 + "/lambda_0").bond()[0].dim();
	int chi_jt = jt.bond()[0].dim();
	if (chi_wire < chi_jt) {
		std::cerr << "In YJuncQnIBC::importWireMPS : wire bond dim should > junction." << '\n';
		return;
	}
	
	std::vector<uni10::Qnum> qn1 = 
		uni10::CUniTensor(dir_w1 + "/lambda_" + std::to_string((long long)(unit_cell-3))).bond()[1].Qlist();
	std::vector<uni10::Qnum> qn2 = 
		uni10::CUniTensor(dir_w2 + "/lambda_" + std::to_string((long long)((unit_cell+3)%unit_cell))).bond()[0].Qlist();
	std::vector<uni10::Qnum> qn3 = 
		uni10::CUniTensor(dir_w3 + "/lambda_" + std::to_string((long long)((unit_cell+3)%unit_cell))).bond()[0].Qlist();
	std::vector<uni10::Qnum> qo1 = wire1.gamma[lat_size-3].bond()[2].Qlist();
	std::vector<uni10::Qnum> qi2 = wire2.gamma[2].bond()[0].Qlist();
	std::vector<uni10::Qnum> qi3 = wire3.gamma[2].bond()[0].Qlist();
	wire1.resizeGamma(lat_size-3, qn1, qo1);
	wire2.resizeGamma(2, qi2, qn2);
	wire3.resizeGamma(2, qi3, qn3);

	uni10::CUniTensor w1g1 = wire1.gamma[lat_size-2];
	uni10::CUniTensor w1g2 = wire1.gamma[lat_size-3];
	uni10::CUniTensor w2g1 = wire2.gamma[1];
	uni10::CUniTensor w2g2 = wire2.gamma[2];
	uni10::CUniTensor w3g1 = wire3.gamma[1];
	uni10::CUniTensor w3g2 = wire3.gamma[2];
	uni10::CUniTensor w1l1 = wire1.lambda[lat_size-1];
	uni10::CUniTensor w1l2 = wire1.lambda[lat_size-2];
	uni10::CUniTensor w2l1 = wire2.lambda[1];
	uni10::CUniTensor w2l2 = wire2.lambda[2];
	uni10::CUniTensor w3l1 = wire3.lambda[1];
	uni10::CUniTensor w3l2 = wire3.lambda[2];

	wire1.CanonMPS::importMPS( dir_w1, unit_cell, true, false );
	wire2.CanonMPS::importMPS( dir_w2, unit_cell, true, false );
	wire3.CanonMPS::importMPS( dir_w3, unit_cell, true, false );
	wire1.gamma[lat_size-2] = w1g1;
	wire1.gamma[lat_size-3] = w1g2;
	wire2.gamma[1] = w2g1;
	wire2.gamma[2] = w2g2;
	wire3.gamma[1] = w3g1;
	wire3.gamma[2] = w3g2;
	wire1.lambda[lat_size-1] = w1l1;
	wire1.lambda[lat_size-2] = w1l2;
	wire2.lambda[1] = w2l1;
	wire2.lambda[2] = w2l2;
	wire3.lambda[1] = w3l1;
	wire3.lambda[2] = w3l2;
	
	chi_junc = chi_jt;
	chi_max = chi_wire;
}

//======================================

void YJuncQnIBC::importWireMPS( std::string dirname, int unit_cell ) {
	///
	importWireMPS(dirname, dirname, dirname, unit_cell);
}

// //======================================
//
// void YJuncQnIBC::import3Semi( std::string dirname, int unit_cell ) {
// 	///
// }
//
// 
//======================================

void YJuncQnIBC::matchQnIBC(
	std::string dir_w1, std::string dir_w2, std::string dir_w3 ) {
	///
	uni10::CUniTensor lam1 = uni10::CUniTensor(dir_w1 + "/lambda_0");
	uni10::CUniTensor lam2 = uni10::CUniTensor(dir_w2 + "/lambda_0");
	uni10::CUniTensor lam3 = uni10::CUniTensor(dir_w3 + "/lambda_0");
	
	std::vector<uni10::Qnum> qn1 = lam1.bond()[1].Qlist();
	std::vector<uni10::Qnum> qn2 = lam2.bond()[0].Qlist();
	std::vector<uni10::Qnum> qn3 = lam3.bond()[0].Qlist();
	std::vector<uni10::Qnum> qo1 = wire1.gamma[0].bond()[2].Qlist();
	std::vector<uni10::Qnum> qi2 = wire2.gamma[lat_size-1].bond()[0].Qlist();
	std::vector<uni10::Qnum> qi3 = wire3.gamma[lat_size-1].bond()[0].Qlist();
	wire1.resizeGamma(0, qn1, qo1);
	wire2.resizeGamma(lat_size-1, qi2, qn2);
	wire3.resizeGamma(lat_size-1, qi3, qn3);
	
	wire1.lambda[0] = lam1;
	wire2.lambda[lat_size] = lam2;
	wire3.lambda[lat_size] = lam3;
}

//======================================

void YJuncQnIBC::exportMPS( std::string dirname ) {
	///
	struct stat info;
	if ( stat( dirname.c_str(), &info ) != 0 )
		mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

	wire1.exportMPS( dirname + "/wire1" );
	wire2.exportMPS( dirname + "/wire2" );
	wire3.exportMPS( dirname + "/wire3" );
	jt.save( dirname + "/wire1/junct" );
}

//======================================

void YJuncQnIBC::loadNetYJ( std::string net_dir ) {
	///
	net_yj_dir = net_dir;
	net_jt = uni10::CNetwork( net_dir + "/junct_trngl.net" );
	net_th_trngl_jt = uni10::CNetwork( net_dir + "/theta_trngl_jt.net" );
	net_th_trngl_jt_1_red = uni10::CNetwork( net_dir + "/theta_trngl_jt_1_red.net" );
	net_th_trngl_jt_2_red = uni10::CNetwork( net_dir + "/theta_trngl_jt_2_red.net" );
	net_th_trngl_jt_3_red = uni10::CNetwork( net_dir + "/theta_trngl_jt_3_red.net" );
	net_th_trngl_wjt_1 = uni10::CNetwork( net_dir + "/theta_trngl_wjt_1.net" );
	net_th_trngl_wjt_2 = uni10::CNetwork( net_dir + "/theta_trngl_wjt_2.net" );
	net_th_trngl_wjt_3 = uni10::CNetwork( net_dir + "/theta_trngl_wjt_3.net" );
	net_th_trngl_wjt_1_op = uni10::CNetwork( net_dir + "/theta_trngl_wjt_1_op.net" );
	net_th_trngl_wjt_2_op = uni10::CNetwork( net_dir + "/theta_trngl_wjt_2_op.net" );
	net_th_trngl_wjt_3_op = uni10::CNetwork( net_dir + "/theta_trngl_wjt_3_op.net" );
	net_th_trngl_wjtw_12 = uni10::CNetwork( net_dir + "/theta_trngl_wjtw_12.net" );
	net_th_trngl_wjtw_23 = uni10::CNetwork( net_dir + "/theta_trngl_wjtw_23.net" );
	net_th_trngl_wjtw_13 = uni10::CNetwork( net_dir + "/theta_trngl_wjtw_13.net" );
	net_th_trngl_wjtw_12_op = uni10::CNetwork( net_dir + "/theta_trngl_wjtw_12_op.net" );
	net_th_trngl_wjtw_23_op = uni10::CNetwork( net_dir + "/theta_trngl_wjtw_23_op.net" );
	net_th_trngl_wjtw_13_op = uni10::CNetwork( net_dir + "/theta_trngl_wjtw_13_op.net" );
	net_yj_loaded = true;
}

//======================================

void YJuncQnIBC::loadNetIBC( std::string net_dir ) { net_ibc_dir = net_dir; }

//======================================

std::vector<uni10::CUniTensor> YJuncQnIBC::hosvdJT() {
	///
	uni10::CUniTensor junct = netThetaTrngl();
	std::vector<int> lab_jo = {0, 100, 101, 102, 1, 2};
	std::vector<int> lab_jn = {0, 100, 1, 101, 2, 102};
	junct.setLabel(lab_jo);
	junct.permute(lab_jn, 2);
	std::vector<uni10::CUniTensor> units = junct.hosvd(3);
	return units;
}

//======================================

uni10::CUniTensor YJuncQnIBC::netLJT( uni10::CUniTensor lam, uni10::CUniTensor jt ) {
	///
	uni10::CUniTensor ten;
	int ibn = jt.inBondNum();
	std::vector<int> lab_lam = {-1, 1};
	std::vector<int> lab_jti = {1, 101, 102, 103, 2, 3};
	std::vector<int> lab_jtf = {-1, 101, 102, 103, 2, 3};
	lam.setLabel(lab_lam); jt.setLabel(lab_jti);
	ten = uni10::contract(lam, jt, true);
	ten.permute(lab_jtf, ibn);
	return ten;
}

//======================================

uni10::CUniTensor YJuncQnIBC::netJTL( uni10::CUniTensor jt, uni10::CUniTensor lam, std::string loc ) {
	///
	uni10::CUniTensor ten;
	int ibn = jt.inBondNum();
	std::vector<int> lab_lam, lab_jti, lab_jtf;
	if (loc == "2") {
		lab_lam = {2, -2};
		lab_jti = {1, 101, 102, 103, 2, 3};
		lab_jtf = {1, 101, 102, 103, -2, 3};
	}
	else if (loc == "3") {
		lab_lam = {3, -3};
		lab_jti = {1, 101, 102, 103, 2, 3};
		lab_jtf = {1, 101, 102, 103, 2, -3};
	}
	jt.setLabel(lab_jti); lam.setLabel(lab_lam);
	ten = uni10::contract(jt, lam, true);
	ten.permute(lab_jtf, ibn);
	return ten;
}

//======================================

void YJuncQnIBC::JTUpdate( std::string loc, uni10::CUniTensor& theta,
	std::map<uni10::Qnum, std::vector<uni10::CMatrix>>& th_svd,
	uni10::CUniTensor& lamC, uni10::CUniTensor& jjjt, uni10::CUniTensor& lamJ1, uni10::CUniTensor& lamJ2,
	uni10::CUniTensor& gamN ) {
	///
	uni10::CUniTensor l1_inv = tenInvQn( lamJ1 );
	uni10::CUniTensor l2_inv = tenInvQn( lamJ2 );
	int ibnn = gamN.inBondNum();

	std::vector<uni10::Qnum> lc_qlist;
	for (std::map<uni10::Qnum, std::vector<uni10::CMatrix>>::iterator it = th_svd.begin(); it != th_svd.end(); ++it) {
		uni10::Qnum q = it->first;
		for (int i = 0; i < th_svd.at(q)[1].col(); ++i)
			lc_qlist.push_back(q);
	}

	std::vector<uni10::Bond> bdlc;
	bdlc.push_back( uni10::Bond(uni10::BD_IN, lc_qlist) );
	bdlc.push_back( uni10::Bond(uni10::BD_OUT, lc_qlist) );
	std::vector<uni10::Bond> bdjt;
	if (loc == "1") {
		bdjt.push_back( uni10::Bond(uni10::BD_IN, lc_qlist) );
		bdjt.push_back( theta.bond()[1] );
		bdjt.push_back( theta.bond()[2] );
		bdjt.push_back( theta.bond()[3] );
		bdjt.push_back( theta.bond()[4] );
		bdjt.push_back( theta.bond()[5] );
	}
	else {
		bdjt.push_back( theta.bond()[0] );
		bdjt.push_back( theta.bond()[1] );
		bdjt.push_back( theta.bond()[2] );
		bdjt.push_back( theta.bond()[3] );
		bdjt.push_back( theta.bond()[4] );
		bdjt.push_back( uni10::Bond(uni10::BD_OUT, lc_qlist) );
	}

	lamC.assign( bdlc );
	jjjt.assign( bdjt );
	uni10::CUniTensor exUV( bdlc );	// extra U/V tensor after SVD

	std::vector<uni10::Qnum> lc_qnums = lamC.blockQnum();

	if (loc == "1") {
		for (std::vector<uni10::Qnum>::iterator it = lc_qnums.begin(); it != lc_qnums.end(); ++it) {
			uni10::Qnum q = *it;
			exUV.putBlock(q, th_svd.at(q)[0]);
			lamC.putBlock(q, th_svd.at(q)[1]);
			jjjt.putBlock(q, th_svd.at(q)[2]);
		}

		std::vector<int> lab_jti = {1, 101, 102, 103, 2, 3};
		std::vector<int> lab_l1i = {2, -2};
		std::vector<int> lab_l2i = {3, -3};
		std::vector<int> lab_jtf = {1, 101, 102, 103, -2, -3};
		l1_inv.setLabel(lab_l1i);
		l2_inv.setLabel(lab_l2i);
		jjjt.setLabel(lab_jti);
		jjjt = jjjt * l1_inv * l2_inv;
		jjjt.permute( lab_jtf, 4 );

		// absorb exUV into gamN
		int lab_uv[] = {1, 2};
		exUV.setLabel(lab_uv);
		std::vector<int> lab_gni = {0, 100, 1};
		std::vector<int> lab_gnf = {0, 100, 2};
		gamN.setLabel(lab_gni);
		gamN = gamN * exUV;
		gamN.permute(lab_gnf, ibnn);
	}

	else {
		for (std::vector<uni10::Qnum>::iterator it = lc_qnums.begin(); it != lc_qnums.end(); ++it) {
			uni10::Qnum q = *it;
			jjjt.putBlock(q, th_svd.at(q)[0]);
			lamC.putBlock(q, th_svd.at(q)[1]);
			exUV.putBlock(q, th_svd.at(q)[2]);
		}

		std::vector<int> lab_jti, lab_jtf, lab_l1i, lab_l2i;
		if (loc == "2") {
			lab_jti = {1, 101, 102, 103, 3, 2};
			lab_l1i = {-1, 1};
			lab_l2i = {3, -3};
			lab_jtf = {-1, 101, 102, 103, 2, -3};
		}
		else {
			lab_jti = {1, 101, 102, 103, 2, 3};
			lab_l1i = {-1, 1};
			lab_l2i = {2, -2};
			lab_jtf = {-1, 101, 102, 103, -2, 3};
		}
		l1_inv.setLabel(lab_l1i);
		l2_inv.setLabel(lab_l2i);
		jjjt.setLabel(lab_jti);
		jjjt = jjjt * l1_inv * l2_inv;
		jjjt.permute( lab_jtf, 4 );

		// absorb exUV into gamN
		int lab_uv[] = {0, 1};
		exUV.setLabel(lab_uv);
		std::vector<int> lab_gni = {1, 100, 2};
		std::vector<int> lab_gnf = {0, 100, 2};
		gamN.setLabel(lab_gni);
		gamN = exUV * gamN;
		gamN.permute(lab_gnf, ibnn);
	}
}

//======================================

void YJuncQnIBC::JTSVD( std::string loc, uni10::CUniTensor& theta, bool show_err ) {
	///
	// theta should be the junct triangle
	std::vector<int> lab_orig = {1, 101, 102, 103, 2, 3};
	std::vector<int> lab_perm;
	int ibn;

	if (loc == "1") {
		lab_perm = {1, 101, 102, 103, 2, 3};
		ibn = 1;
	}
	else if (loc == "2") {
		lab_perm = {1, 101, 102, 103, 3, 2};
		ibn = 5;
	}
	else if (loc == "3") {
		lab_perm = {1, 101, 102, 103, 2, 3};
		ibn = 5;
	}
	else {
		std::cerr << "In YJuncQnIBC::JTSVD : Invalid update location." << '\n';
		return;
	}

	theta.setLabel(lab_orig);
	theta.permute(lab_perm, ibn);
	std::map<uni10::Qnum, std::vector<uni10::CMatrix>> th_svd = svdQn( theta );
	truncateQn( th_svd, chi_junc );

	if (loc == "1")
		JTUpdate(loc, theta, th_svd, wire1.lambda[lat_size-1], jt,
			wire2.lambda[1], wire3.lambda[1], wire1.gamma[lat_size-2]);
	else if (loc == "2")
		JTUpdate(loc, theta, th_svd, wire2.lambda[1], jt,
			wire1.lambda[lat_size-1], wire3.lambda[1], wire2.gamma[1]);
	else if (loc == "3")
		JTUpdate(loc, theta, th_svd, wire3.lambda[1], jt,
			wire1.lambda[lat_size-1], wire2.lambda[1], wire3.gamma[1]);
}

//======================================

void YJuncQnIBC::WJTUpdate( std::string loc, uni10::CUniTensor& theta,
	std::map<uni10::Qnum, std::vector<uni10::CMatrix>>& th_svd,
	uni10::CUniTensor& lamW, uni10::CUniTensor& gamW, uni10::CUniTensor& lamC,
	uni10::CUniTensor& jjjt, uni10::CUniTensor& lamJ1, uni10::CUniTensor& lamJ2 ) {
	///
	uni10::CUniTensor lw_inv = tenInvQn( lamW );
	uni10::CUniTensor l1_inv = tenInvQn( lamJ1 );
	uni10::CUniTensor l2_inv = tenInvQn( lamJ2 );

	std::vector<uni10::Qnum> lc_qlist;
	for (std::map<uni10::Qnum, std::vector<uni10::CMatrix>>::iterator it = th_svd.begin(); it != th_svd.end(); ++it) {
		uni10::Qnum q = it->first;
		for (int i = 0; i < th_svd.at(q)[1].col(); ++i)
			lc_qlist.push_back(q);
	}

	std::vector<uni10::Bond> bdlc;
	bdlc.push_back( uni10::Bond(uni10::BD_IN, lc_qlist) );
	bdlc.push_back( uni10::Bond(uni10::BD_OUT, lc_qlist) );
	std::vector<uni10::Bond> bdgw, bdjt;

	if (loc == "WJ1") {
		bdgw.push_back( theta.bond()[0] );
		bdgw.push_back( theta.bond()[1] );
		bdgw.push_back( uni10::Bond(uni10::BD_OUT, lc_qlist) );
		bdjt.push_back( uni10::Bond(uni10::BD_IN, lc_qlist) );
		bdjt.push_back( theta.bond()[2] );
		bdjt.push_back( theta.bond()[3] );
		bdjt.push_back( theta.bond()[4] );
		bdjt.push_back( theta.bond()[5] );
		bdjt.push_back( theta.bond()[6] );
	}
	else {
		bdjt.push_back( theta.bond()[0] );
		bdjt.push_back( theta.bond()[1] );
		bdjt.push_back( theta.bond()[2] );
		bdjt.push_back( theta.bond()[3] );
		bdjt.push_back( theta.bond()[4] );
		bdjt.push_back( uni10::Bond(uni10::BD_OUT, lc_qlist) );
		bdgw.push_back( uni10::Bond(uni10::BD_IN, lc_qlist) );
		bdgw.push_back( theta.bond()[5] );
		bdgw.push_back( theta.bond()[6] );
	}

	gamW.assign( bdgw );
	lamC.assign( bdlc );
	jjjt.assign( bdjt );

	std::vector<uni10::Qnum> lc_qnums = lamC.blockQnum();
	std::vector<int> lab_jt, lab_jtf, lab_l1i, lab_l2i;

	if (loc == "WJ1") {
		for (std::vector<uni10::Qnum>::iterator it = lc_qnums.begin(); it != lc_qnums.end(); ++it) {
			uni10::Qnum q = *it;
			gamW.putBlock(q, th_svd.at(q)[0]);
			lamC.putBlock(q, th_svd.at(q)[1]);
			jjjt.putBlock(q, th_svd.at(q)[2]);
		}

		gamW = netLG(lw_inv, gamW);

		lab_jt  = {1, 101, 102, 103, 2, 3};
		lab_jtf = {1, 101, 102, 103, -2, -3};
		lab_l1i = {2, -2};
		lab_l2i = {3, -3};
		l1_inv.setLabel(lab_l1i);
		l2_inv.setLabel(lab_l2i);
		jjjt.setLabel(lab_jt);
		jjjt = jjjt * l1_inv * l2_inv;
		jjjt.permute( lab_jtf, 4 );
	}

	else {
		for (std::vector<uni10::Qnum>::iterator it = lc_qnums.begin(); it != lc_qnums.end(); ++it) {
			uni10::Qnum q = *it;
			jjjt.putBlock(q, th_svd.at(q)[0]);
			lamC.putBlock(q, th_svd.at(q)[1]);
			gamW.putBlock(q, th_svd.at(q)[2]);
		}

		gamW = netGL(gamW, lw_inv);

		if (loc == "WJ2") {
			lab_jt  = {1, 3, 101, 102, 103, 2};
			lab_jtf = {-1, 101, 102, 103, 2, -3};
			lab_l1i = {-1, 1};
			lab_l2i = {3, -3};
		}
		else if (loc == "WJ3") {
			lab_jt  = {1, 2, 101, 102, 103, 3};
			lab_jtf = {-1, 101, 102, 103, -2, 3};
			lab_l1i = {-1, 1};
			lab_l2i = {2, -2};
		}
		l1_inv.setLabel(lab_l1i);
		l2_inv.setLabel(lab_l2i);
		jjjt.setLabel(lab_jt);
		jjjt = jjjt * l1_inv * l2_inv;
		jjjt.permute( lab_jtf, 4 );
	}
}

//======================================

void YJuncQnIBC::WJTSVD( std::string loc, uni10::CUniTensor& theta, bool show_err, double tol ) {
	///
	if (loc == "WJ1" && theta.inBondNum() != 2)
		theta.permute(2);
	else if ((loc == "WJ2" || loc == "WJ3") && (theta.inBondNum() != theta.bondNum()-2))
		theta.permute( theta.bondNum()-2 );

	std::map<uni10::Qnum, std::vector<uni10::CMatrix>> th_svd = svdQn( theta );
	truncateQn( th_svd, chi_junc, tol );

	if (loc == "WJ1")
		WJTUpdate(loc, theta, th_svd, wire1.lambda[lat_size-2], wire1.gamma[lat_size-2],
			wire1.lambda[lat_size-1], jt, wire2.lambda[1], wire3.lambda[1]);
	else if (loc == "WJ2")
		WJTUpdate(loc, theta, th_svd, wire2.lambda[2], wire2.gamma[1],
			wire2.lambda[1], jt, wire1.lambda[lat_size-1], wire3.lambda[1]);
	else if (loc == "WJ3")
		WJTUpdate(loc, theta, th_svd, wire3.lambda[2], wire3.gamma[1],
			wire3.lambda[1], jt, wire1.lambda[lat_size-1], wire2.lambda[1]);
	else
		std::cerr << "In YJuncQnIBC::WJTSVD : Invalid location." << '\n';
}

//======================================

uni10::CUniTensor YJuncQnIBC::netThetaTrngl() {
	///
	uni10::CUniTensor theta;
	if (!net_yj_loaded)
		loadNetYJ(net_yj_dir);

	net_th_trngl_jt.putTensor( "JT", jt );
	net_th_trngl_jt.putTensor( "lam1", wire1.lambda[lat_size-1] );
	net_th_trngl_jt.putTensor( "lam2", wire2.lambda[1] );
	net_th_trngl_jt.putTensor( "lam3", wire3.lambda[1] );
	theta = net_th_trngl_jt.launch(true);

	return theta;
}

//======================================

uni10::CUniTensor YJuncQnIBC::netThetaTrngl( std::string loc, uni10::CUniTensor op ) {
	///
	uni10::CUniTensor theta = netThetaTrngl();
	std::vector<int> lab_jt = {1, 101, 102, 103, 2, 3};
	std::vector<int> lab_op, lab_th;
	theta.setLabel(lab_jt);

	if (loc == "12") {
		lab_op = {201, 202, 101, 102};
		lab_th = {1, 201, 202, 103, 2, 3};
		op.setLabel(lab_op);
		theta = uni10::contract(op, theta, true);
		theta.permute(lab_th, 4);
	}
	else if (loc == "23") {
		lab_op = {202, 203, 102, 103};
		lab_th = {1, 101, 202, 203, 2, 3};
		op.setLabel(lab_op);
		if (fermi)
			applyCrossGate(theta, 2, 102);
		theta = uni10::contract(op, theta);
		if (fermi)
			applyCrossGate(theta, 2, 202);
		theta.permute(lab_th, 4);
	}
	else if (loc == "13") {
		lab_op = {201, 203, 101, 103};
		lab_th = {1, 201, 102, 203, 2, 3};
		op.setLabel(lab_op);
		if (fermi) {
			applyCrossGate(theta, 102, 101);
			applyCrossGate(theta, 2, 103);
		}
		theta = uni10::contract(op, theta);
		if (fermi) {
			applyCrossGate(theta, 102, 201);
			applyCrossGate(theta, 2, 203);
		}
		theta.permute(lab_th, 4);
	}
	else if (loc == "1") {
		if (op.bondNum() == 2) {
			lab_op = {201, 101};
			lab_th = {1, 201, 102, 103, 2, 3};
		}
		else if (op.bondNum() == 3) {
			lab_op = {201, 101, -10};
			lab_th = {1, 201, 102, 103, 2, 3, -10};
		}
		op.setLabel(lab_op);
		theta = uni10::contract(op, theta, true);
		theta.permute(lab_th, 4);
	}
	else if (loc == "2") {
		if (op.bondNum() == 2) {
			lab_op = {202, 102};
			lab_th = {1, 101, 202, 103, 2, 3};
		}
		else if (op.bondNum() == 3) {
			lab_op = {202, 102, -10};
			lab_th = {1, 101, 202, 103, 2, 3, -10};
		}
		op.setLabel(lab_op);
		theta = uni10::contract(op, theta, true);
		theta.permute(lab_th, 4);
	}
	else if (loc == "3") {
		if (op.bondNum() == 2) {
			lab_op = {203, 103};
			lab_th = {1, 101, 102, 203, 2, 3};
		}
		else if (op.bondNum() == 3) {
			lab_op = {203, 103, -10};
			lab_th = {1, 101, 102, 203, 2, 3, -10};
		}
		op.setLabel(lab_op);
		theta = uni10::contract(op, theta, true);
		theta.permute(lab_th, 4);
	}

	return theta;
}

//======================================

uni10::CUniTensor YJuncQnIBC::netThetaTrngl_red( std::string loc ) {
	///
	if (!net_yj_loaded)
		loadNetYJ(net_yj_dir);

	uni10::CUniTensor theta;
	uni10::CNetwork net;

	if (loc == "1") {
		net = net_th_trngl_jt_1_red;
		net.putTensor("JT", jt);
		net.putTensor("lam2", wire2.lambda[1]);
		net.putTensor("lam3", wire3.lambda[1]);
		theta = net.launch(true);
	}
	else if (loc == "2") {
		net = net_th_trngl_jt_2_red;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam3", wire3.lambda[1]);
		theta = net.launch(true);
	}
	else if (loc == "3") {
		net = net_th_trngl_jt_3_red;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam2", wire2.lambda[1]);
		theta = net.launch(true);
	}
	else
		std::cerr << "In YJuncQnIBC::netThetaTrngl_red : Invalid junction location." << '\n';

	return theta;
}

//======================================

uni10::CUniTensor YJuncQnIBC::netThetaTrngl_WJT( std::string loc ) {
	///
	if (!net_yj_loaded)
		loadNetYJ(net_yj_dir);

	uni10::CUniTensor theta;
	uni10::CNetwork net;

	if (loc == "WJ1") {
		net = net_th_trngl_wjt_1;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam2", wire2.lambda[1]);
		net.putTensor("lam3", wire3.lambda[1]);
		net.putTensor("gamW", wire1.gamma[lat_size-2]);
		net.putTensor("lamW", wire1.lambda[lat_size-2]);
		theta = net.launch(true);
	}
	else if (loc == "WJ2") {
		net = net_th_trngl_wjt_2;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam2", wire2.lambda[1]);
		net.putTensor("lam3", wire3.lambda[1]);
		net.putTensor("gamW", wire2.gamma[1]);
		net.putTensor("lamW", wire2.lambda[2]);
		theta = net.launch(true);
	}
	else if (loc == "WJ3") {
		net = net_th_trngl_wjt_3;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam2", wire2.lambda[1]);
		net.putTensor("lam3", wire3.lambda[1]);
		net.putTensor("gamW", wire3.gamma[1]);
		net.putTensor("lamW", wire3.lambda[2]);
		theta = net.launch(true);
	}
	else
		std::cerr << "In YJuncQnIBC::netThetaTrngl_WJT : Invalid location." << '\n';

	return theta;
}

//======================================

uni10::CUniTensor YJuncQnIBC::netThetaTrngl_WJT( std::string loc, uni10::CUniTensor op ) {
	///
	if (!net_yj_loaded)
		loadNetYJ(net_yj_dir);

	uni10::CUniTensor theta;
	uni10::CNetwork net;

	if (loc == "WJ1") {
		net = net_th_trngl_wjt_1_op;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam2", wire2.lambda[1]);
		net.putTensor("lam3", wire3.lambda[1]);
		net.putTensor("gamW", wire1.gamma[lat_size-2]);
		net.putTensor("lamW", wire1.lambda[lat_size-2]);
		net.putTensor("op", op);
		theta = net.launch(true);
	}
	else if (loc == "WJ2") {
		net = net_th_trngl_wjt_2_op;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam2", wire2.lambda[1]);
		net.putTensor("lam3", wire3.lambda[1]);
		net.putTensor("gamW", wire2.gamma[1]);
		net.putTensor("lamW", wire2.lambda[2]);
		net.putTensor("op", op);
		theta = net.launch(true);
	}
	else if (loc == "WJ3") {
		net = net_th_trngl_wjt_3_op;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam2", wire2.lambda[1]);
		net.putTensor("lam3", wire3.lambda[1]);
		net.putTensor("gamW", wire3.gamma[1]);
		net.putTensor("lamW", wire3.lambda[2]);
		net.putTensor("op", op);
		theta = net.launch(true);
	}
	else
		std::cerr << "In YJuncQnIBC::netThetaTrngl_WJT : Invalid location." << '\n';

	return theta;
}

//======================================

uni10::CUniTensor YJuncQnIBC::netThetaTrngl_WJTW( std::string loc ) {
	///
	if (!net_yj_loaded)
		loadNetYJ(net_yj_dir);

	uni10::CUniTensor theta;
	uni10::CNetwork net;

	if (loc == "12") {
		net = net_th_trngl_wjtw_12;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam2", wire2.lambda[1]);
		net.putTensor("lam3", wire3.lambda[1]);
		net.putTensor("gamW1", wire1.gamma[lat_size-2]);
		net.putTensor("lamW1", wire1.lambda[lat_size-2]);
		net.putTensor("gamW2", wire2.gamma[1]);
		net.putTensor("lamW2", wire2.lambda[2]);
		theta = net.launch(true);
	}
	else if (loc == "23") {
		net = net_th_trngl_wjtw_23;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam2", wire2.lambda[1]);
		net.putTensor("lam3", wire3.lambda[1]);
		net.putTensor("gamW2", wire2.gamma[1]);
		net.putTensor("lamW2", wire2.lambda[2]);
		net.putTensor("gamW3", wire3.gamma[1]);
		net.putTensor("lamW3", wire3.lambda[2]);
		theta = net.launch(true);
	}
	else if (loc == "13") {
		net = net_th_trngl_wjtw_13;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam2", wire2.lambda[1]);
		net.putTensor("lam3", wire3.lambda[1]);
		net.putTensor("gamW1", wire1.gamma[lat_size-2]);
		net.putTensor("lamW1", wire1.lambda[lat_size-2]);
		net.putTensor("gamW3", wire3.gamma[1]);
		net.putTensor("lamW3", wire3.lambda[2]);
		theta = net.launch(true);
	}
	else
		std::cerr << "In YJuncQnIBC::netThetaTrngl_WJTW : Invalid location." << '\n';

	return theta;
}

//======================================

uni10::CUniTensor YJuncQnIBC::netThetaTrngl_WJTW(
	std::string loc, uni10::CUniTensor op1, uni10::CUniTensor op2 ) {
	///
	if (!net_yj_loaded)
		loadNetYJ(net_yj_dir);

	uni10::CUniTensor theta;
	uni10::CNetwork net;

	if (loc == "12") {
		net = net_th_trngl_wjtw_12_op;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam2", wire2.lambda[1]);
		net.putTensor("lam3", wire3.lambda[1]);
		net.putTensor("gamW1", wire1.gamma[lat_size-2]);
		net.putTensor("lamW1", wire1.lambda[lat_size-2]);
		net.putTensor("gamW2", wire2.gamma[1]);
		net.putTensor("lamW2", wire2.lambda[2]);
		net.putTensor("op1", op1);
		net.putTensor("op2", op2);
		theta = net.launch(true);
	}
	else if (loc == "23") {
		net = net_th_trngl_wjtw_23_op;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam2", wire2.lambda[1]);
		net.putTensor("lam3", wire3.lambda[1]);
		net.putTensor("gamW2", wire2.gamma[1]);
		net.putTensor("lamW2", wire2.lambda[2]);
		net.putTensor("gamW3", wire3.gamma[1]);
		net.putTensor("lamW3", wire3.lambda[2]);
		net.putTensor("op1", op1);
		net.putTensor("op2", op2);
		theta = net.launch(true);
	}
	else if (loc == "13") {
		net = net_th_trngl_wjtw_13_op;
		net.putTensor("JT", jt);
		net.putTensor("lam1", wire1.lambda[lat_size-1]);
		net.putTensor("lam2", wire2.lambda[1]);
		net.putTensor("lam3", wire3.lambda[1]);
		net.putTensor("gamW1", wire1.gamma[lat_size-2]);
		net.putTensor("lamW1", wire1.lambda[lat_size-2]);
		net.putTensor("gamW3", wire3.gamma[1]);
		net.putTensor("lamW3", wire3.lambda[2]);
		net.putTensor("op1", op1);
		net.putTensor("op2", op2);
		theta = net.launch(true);
	}
	else
		std::cerr << "In YJuncQnIBC::netThetaTrngl_WJTW : Invalid location." << '\n';

	return theta;
}

//======================================

std::vector< std::vector< std::vector<uni10::CUniTensor> > > YJuncQnIBC::allLRVecs() {
	///
	std::vector<uni10::CUniTensor> w1_lvec, w1_rvec, w2_lvec, w2_rvec, w3_lvec, w3_rvec;
	for (int i = 0; i < lat_size; ++i) {
		w1_lvec.push_back( uni10::CUniTensor() );
		w1_rvec.push_back( uni10::CUniTensor() );
		w2_lvec.push_back( uni10::CUniTensor() );
		w2_rvec.push_back( uni10::CUniTensor() );
		w3_lvec.push_back( uni10::CUniTensor() );
		w3_rvec.push_back( uni10::CUniTensor() );
	}

	uni10::CUniTensor lvec;
	uni10::CUniTensor rvec;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	std::vector<int> lab_lvec = {0, 1};
	std::vector<int> lab_bra1 = {2, 0, 100};
	std::vector<int> lab_ket1 = {1, 100, 3};

	// wire1 lvec
	lvec.assign( wire1.lambda[0].bond() );
	lvec.identity();
	lvec.setLabel( lab_lvec );
	for (int i = 0; i < lat_size-1; ++i) {
		ket = netLG( wire1.lambda[i], wire1.gamma[i] );
		bra = dag( ket );
		bra.setLabel( lab_bra1 );
		ket.setLabel( lab_ket1 );
		lvec = uni10::contract( bra, lvec, false );
		lvec = uni10::contract( lvec, ket, false );
		lvec.setLabel( lab_lvec );
		w1_lvec[i] = lvec;
	}

	std::vector<int> lab_rvec = {3, 2};
	std::vector<int> lab_bra2 = {2, 0, 100};
	std::vector<int> lab_ket2 = {1, 100, 3};

	// wire2 rvec
	rvec.assign( wire2.lambda[lat_size].bond() );
	rvec.identity();
	rvec.setLabel( lab_rvec );
	for (int i = lat_size-1; i > 0; --i) {
		ket = netGL( wire2.gamma[i], wire2.lambda[i+1] );
		bra = dag( ket );
		bra.setLabel( lab_bra2 );
		ket.setLabel( lab_ket2 );
		rvec = uni10::contract( ket, rvec, false );
		rvec = uni10::contract( rvec, bra, false );
		rvec.setLabel( lab_rvec );
		w2_rvec[i] = rvec;
	}

	// wire3 rvec
	rvec.assign( wire3.lambda[lat_size].bond() );
	rvec.identity();
	rvec.setLabel( lab_rvec );
	for (int i = lat_size-1; i > 0; --i) {
		ket = netGL( wire3.gamma[i], wire3.lambda[i+1] );
		bra = dag( ket );
		bra.setLabel( lab_bra2 );
		ket.setLabel( lab_ket2 );
		rvec = uni10::contract( ket, rvec, false );
		rvec = uni10::contract( rvec, bra, false );
		rvec.setLabel( lab_rvec );
		w3_rvec[i] = rvec;
	}

	std::vector<int> lab_ket = {1, 101, 102, 103, -2, -3};
	std::vector<int> lab_bra = {2, 3, -1, 101, 102, 103};
	std::vector<int> lab_w1l = {-1, 1};
	std::vector<int> lab_w2r = {-2, 2};
	std::vector<int> lab_w3r = {-3, 3};
	w1_lvec[lat_size-2].setLabel(lab_w1l);
	w2_rvec[1].setLabel(lab_w2r);
	w3_rvec[1].setLabel(lab_w3r);
	// first w1_rvec, w2_lvec, w3_lvec
	ket = netThetaTrngl_red("1");
	bra = dag(ket);
	ket.setLabel(lab_ket);
	bra.setLabel(lab_bra);
	w1_rvec[lat_size-1] = uni10::contract(ket, w2_rvec[1]);
	w1_rvec[lat_size-1] = uni10::contract(w1_rvec[lat_size-1], w3_rvec[1]);
	w1_rvec[lat_size-1] = uni10::contract(w1_rvec[lat_size-1], bra);
	ket = netThetaTrngl_red("2");
	bra = dag(ket);
	ket.setLabel(lab_ket);
	bra.setLabel(lab_bra);
	w2_lvec[0] = uni10::contract(bra, w1_lvec[lat_size-2]);
	w2_lvec[0] = uni10::contract(w2_lvec[0], w3_rvec[1]);
	w2_lvec[0] = uni10::contract(w2_lvec[0], ket);
	ket = netThetaTrngl_red("3");
	bra = dag(ket);
	ket.setLabel(lab_ket);
	bra.setLabel(lab_bra);
	w3_lvec[0] = uni10::contract(bra, w1_lvec[lat_size-2]);
	w3_lvec[0] = uni10::contract(w3_lvec[0], w2_rvec[1]);
	w3_lvec[0] = uni10::contract(w3_lvec[0], ket);

	// wire1 rvec
	rvec = w1_rvec[lat_size-1];
	rvec.setLabel( lab_rvec );
	for (int i = lat_size-2; i > 0; --i) {
		ket = netGL( wire1.gamma[i], wire1.lambda[i+1] );
		bra = dag( ket );
		bra.setLabel( lab_bra2 );
		ket.setLabel( lab_ket2 );
		rvec = uni10::contract( ket, rvec, false );
		rvec = uni10::contract( rvec, bra, false );
		rvec.setLabel( lab_rvec );
		w1_rvec[i] = rvec;
	}

	// wire2 lvec
	lvec = w2_lvec[0];
	lvec.setLabel( lab_lvec );
	for (int i = 1; i < lat_size-1; ++i) {
		ket = netLG( wire2.lambda[i], wire2.gamma[i] );
		bra = dag( ket );
		bra.setLabel( lab_bra1 );
		ket.setLabel( lab_ket1 );
		lvec = uni10::contract( bra, lvec, false );
		lvec = uni10::contract( lvec, ket, false );
		lvec.setLabel( lab_lvec );
		w2_lvec[i] = lvec;
	}

	// wire3 lvec
	lvec = w3_lvec[0];
	lvec.setLabel( lab_lvec );
	for (int i = 1; i < lat_size-1; ++i) {
		ket = netLG( wire3.lambda[i], wire3.gamma[i] );
		bra = dag( ket );
		bra.setLabel( lab_bra1 );
		ket.setLabel( lab_ket1 );
		lvec = uni10::contract( bra, lvec, false );
		lvec = uni10::contract( lvec, ket, false );
		lvec.setLabel( lab_lvec );
		w3_lvec[i] = lvec;
	}

	std::vector< std::vector<uni10::CUniTensor> > w1, w2, w3;
	w1.push_back(w1_lvec); w1.push_back(w1_rvec);
	w2.push_back(w2_lvec); w2.push_back(w2_rvec);
	w3.push_back(w3_lvec); w3.push_back(w3_rvec);

	std::vector< std::vector< std::vector<uni10::CUniTensor> > > vecs;
	vecs.push_back(w1); vecs.push_back(w2); vecs.push_back(w3);

	return vecs;
}

//======================================

uni10::CUniTensor YJuncQnIBC::expVal( uni10::CUniTensor op, int w, int idx ) {
	/// return expectation value (a contracted uni10) of an 1-site/2-site operator on site idx
	uni10::CUniTensor exp_val;
	uni10::CUniTensor ten;

	uni10::CUniTensor w1_lvec( wire1.lambda[0].bond() );
	uni10::CUniTensor w2_rvec( wire2.lambda[lat_size].bond() );
	uni10::CUniTensor w3_rvec( wire3.lambda[lat_size].bond() );
	w1_lvec.identity();
	w2_rvec.identity();
	w3_rvec.identity();

	int bno = op.bondNum();
	int lop = (bno/2);
	if ( bno != 2 && bno != 4 ) {
		std::cerr << "In YJuncQnIBC::expVal : Unsupported operator.\n";
		return exp_val;
	}

	uni10::CUniTensor onsite;
	if (w == 1 && idx < lat_size-lop) {
		onsite = (lop == 1)? netLG( wire1.lambda[idx], wire1.gamma[idx] )
		: netLGLG( wire1.lambda[idx], wire1.gamma[idx], wire1.lambda[idx+1], wire1.gamma[idx+1] );
		w1_lvec = buildLRVecQn( idx-1, w1_lvec, wire1.gamma, wire1.lambda, true );
		w1_lvec = buildLRVecQn( w1_lvec, onsite, op, true );
		for (int i = idx+lop; i < lat_size-1; ++i) {
			ten = netLG( wire1.lambda[i], wire1.gamma[i] );
			w1_lvec = buildLRVecQn( w1_lvec, ten, true );
		}
		w2_rvec = buildLRVecQn( 1, w2_rvec, wire2.gamma, wire2.lambda, false );
		w3_rvec = buildLRVecQn( 1, w3_rvec, wire3.gamma, wire3.lambda, false );
	}
	else if (w == 2 && idx > 0 && idx <= lat_size-lop) {
		onsite = (lop == 1)? netGL( wire2.gamma[idx], wire2.lambda[idx+1] )
		: netGLGL( wire2.gamma[idx], wire2.lambda[idx+1], wire2.gamma[idx+1], wire2.lambda[idx+2] );
		w1_lvec = buildLRVecQn( lat_size-2, w1_lvec, wire1.gamma, wire1.lambda, true );
		w2_rvec = buildLRVecQn( idx+lop, w2_rvec, wire2.gamma, wire2.lambda, false );
		w2_rvec = buildLRVecQn( w2_rvec, onsite, op, false );
		for (int i = idx-1; i > 0; --i) {
			ten = netGL( wire2.gamma[i], wire2.lambda[i+1] );
			w2_rvec = buildLRVecQn( w2_rvec, ten, false );
		}
		w3_rvec = buildLRVecQn( 1, w3_rvec, wire3.gamma, wire3.lambda, false );
	}
	else if (w == 3 && idx > 0 && idx <= lat_size-lop) {
		onsite = (lop == 1)? netGL( wire3.gamma[idx], wire3.lambda[idx+1] )
		: netGLGL( wire3.gamma[idx], wire3.lambda[idx+1], wire3.gamma[idx+1], wire3.lambda[idx+2] );
		w1_lvec = buildLRVecQn( lat_size-2, w1_lvec, wire1.gamma, wire1.lambda, true );
		w2_rvec = buildLRVecQn( 1, w2_rvec, wire2.gamma, wire2.lambda, false );
		w3_rvec = buildLRVecQn( idx+lop, w3_rvec, wire3.gamma, wire3.lambda, false );
		w3_rvec = buildLRVecQn( w3_rvec, onsite, op, false );
		for (int i = idx-1; i > 0; --i) {
			ten = netGL( wire3.gamma[i], wire3.lambda[i+1] );
			w3_rvec = buildLRVecQn( w3_rvec, ten, false );
		}
	}
	else {
		std::cerr << "In YJuncQnIBC::expVal : Invalid operator location.\n";
		return exp_val;
	}

	uni10::CUniTensor ket = netThetaTrngl();
	uni10::CUniTensor bra = dag(ket);

	int lab_ket[] = {1, 101, 102, 103, -2, -3};
	int lab_bra[] = {2, 3, -1, 101, 102, 103};
	int lab_w1l[] = {-1, 1};
	int lab_w2r[] = {-2, 2};
	int lab_w3r[] = {-3, 3};
	ket.setLabel(lab_ket);
	bra.setLabel(lab_bra);
	w1_lvec.setLabel(lab_w1l);
	w2_rvec.setLabel(lab_w2r);
	w3_rvec.setLabel(lab_w3r);
	uni10::CUniTensor rvec = uni10::contract(ket, w2_rvec);
	rvec = uni10::contract(rvec, w3_rvec);
	rvec = uni10::contract(rvec, bra);
	exp_val = contrLRVecQn( w1_lvec, rvec );

	return exp_val;
}

//======================================

uni10::CUniTensor YJuncQnIBC::expVal( uni10::CUniTensor op, std::string loc ) {
	/// return expectation value (a contracted uni10) of an 1-site/2-site operator on special locations
	uni10::CUniTensor exp_val;
	uni10::CUniTensor ket, bra;
	std::vector<int> lab_op, lab_bra, lab_ket;

	uni10::CUniTensor w1_lvec( wire1.lambda[0].bond() );
	uni10::CUniTensor w2_rvec( wire2.lambda[lat_size].bond() );
	uni10::CUniTensor w3_rvec( wire3.lambda[lat_size].bond() );
	w1_lvec.identity();
	w2_rvec.identity();
	w3_rvec.identity();

	int bno = op.bondNum();
	int lop = (bno/2); // length of operator
	if ( bno != 2 && bno != 4 ) {
		std::cerr << "In YJuncQnIBC::expVal : Unsupported operator." << '\n';
		return exp_val;
	}

	if (loc == "WJ1" || loc == "WJ2" || loc == "WJ3") {
		if (lop != 2) {
			std::cerr << "Location " << loc << " only for 2-site operators." << '\n';
			return exp_val;
		}
		if (loc == "WJ1") {
			ket = netThetaTrngl_WJT("WJ1", op);
			bra = dag( netThetaTrngl_WJT("WJ1") );
			lab_ket = {1, 100, 101, 102, 103, -2, -3};
			lab_bra = {2, 3, -1, 100, 101, 102, 103};
			ket.setLabel(lab_ket);
			bra.setLabel(lab_bra);

			w1_lvec = buildLRVecQn( lat_size-3, w1_lvec, wire1.gamma, wire1.lambda, true );
			w2_rvec = buildLRVecQn( 1, w2_rvec, wire2.gamma, wire2.lambda, false );
			w3_rvec = buildLRVecQn( 1, w3_rvec, wire3.gamma, wire3.lambda, false );
		}
		else if (loc == "WJ2") {
			ket = netThetaTrngl_WJT("WJ2", op);
			bra = dag( netThetaTrngl_WJT("WJ2") );
			int lab_ket[] = {1, -3, 101, 102, 103, 104, -2};
			int lab_bra[] = {104, 2, -1, 3, 101, 102, 103};
			ket.setLabel(lab_ket);
			bra.setLabel(lab_bra);

			w1_lvec = buildLRVecQn( lat_size-2, w1_lvec, wire1.gamma, wire1.lambda, true );
			w2_rvec = buildLRVecQn( 2, w2_rvec, wire2.gamma, wire2.lambda, false );
			w3_rvec = buildLRVecQn( 1, w3_rvec, wire3.gamma, wire3.lambda, false );
		}
		else if (loc == "WJ3") {
			ket = netThetaTrngl_WJT("WJ3", op);
			bra = dag( netThetaTrngl_WJT("WJ3") );
			int lab_ket[] = {1, -2, 101, 102, 103, 105, -3};
			int lab_bra[] = {105, 3, -1, 2, 101, 102, 103};
			ket.setLabel(lab_ket);
			bra.setLabel(lab_bra);

			w1_lvec = buildLRVecQn( lat_size-2, w1_lvec, wire1.gamma, wire1.lambda, true );
			w2_rvec = buildLRVecQn( 1, w2_rvec, wire2.gamma, wire2.lambda, false );
			w3_rvec = buildLRVecQn( 2, w3_rvec, wire3.gamma, wire3.lambda, false );
		}
	}
	else {
		if (lop == 1) {
			if (loc == "J1" || loc == "1") {
				ket = netThetaTrngl("1", op);
				bra = dag( netThetaTrngl() );
			}
			else if (loc == "J2" || loc == "2") {
				ket = netThetaTrngl("2", op);
				bra = dag( netThetaTrngl() );
			}
			else if (loc == "J3" || loc == "3") {
				ket = netThetaTrngl("3", op);
				bra = dag( netThetaTrngl() );
			}
			else {
				std::cerr << "In YJuncQnIBC::expVal : Unsupported location + operator configuration." << '\n';
				return exp_val;
			}
		}
		else if (lop == 2) {
			if (loc == "JJ12" || loc == "12") {
				ket = netThetaTrngl("12", op);
				bra = dag( netThetaTrngl() );
			}
			else if (loc == "JJ23" || loc == "23") {
				ket = netThetaTrngl("23", op);
				bra = dag( netThetaTrngl() );
			}
			else if (loc == "JJ13" || loc == "13") {
				ket = netThetaTrngl("13", op);
				bra = dag( netThetaTrngl() );
			}
			else {
				std::cerr << "In YJuncQnIBC::expVal : Unsupported location + operator configuration." << '\n';
				return exp_val;
			}
		}
		int lab_ket[] = {1, 101, 102, 103, -2, -3};
		int lab_bra[] = {2, 3, -1, 101, 102, 103};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);

		w1_lvec = buildLRVecQn( lat_size-2, w1_lvec, wire1.gamma, wire1.lambda, true );
		w2_rvec = buildLRVecQn( 1, w2_rvec, wire2.gamma, wire2.lambda, false );
		w3_rvec = buildLRVecQn( 1, w3_rvec, wire3.gamma, wire3.lambda, false );
	}

	int lab_w1l[] = {-1, 1};
	int lab_w2r[] = {-2, 2};
	int lab_w3r[] = {-3, 3};
	w1_lvec.setLabel(lab_w1l);
	w2_rvec.setLabel(lab_w2r);
	w3_rvec.setLabel(lab_w3r);
	uni10::CUniTensor rvec = uni10::contract(ket, w2_rvec);
	rvec = uni10::contract(rvec, w3_rvec);
	rvec = uni10::contract(rvec, bra);
	exp_val = contrLRVecQn( w1_lvec, rvec );

	return exp_val;
}

//======================================

uni10::CUniTensor YJuncQnIBC::correlation(
	uni10::CUniTensor op1, uni10::CUniTensor op2, int w1, int idx1, int w2, int idx2 ) {
	/// correlation of 1-site/2-site operators on wires (exclude special locations)
	uni10::CUniTensor corr;
	uni10::CUniTensor onsite, ten;

	uni10::CUniTensor w1_lvec( wire1.lambda[0].bond() );
	uni10::CUniTensor w2_rvec( wire2.lambda[lat_size].bond() );
	uni10::CUniTensor w3_rvec( wire3.lambda[lat_size].bond() );
	w1_lvec.identity();
	w2_rvec.identity();
	w3_rvec.identity();

	int bno = op1.bondNum();
	int lop = (bno/2);
	if ( bno < 2 || bno > 4 || bno != op2.bondNum() ) {
		std::cerr << "In YJuncQnIBC::correlation : Unsupported operator configuration.\n";
		return corr;
	}

	if ( w1 == 1 && w2 == 1 && idx1 < idx2 && idx2 < lat_size-lop ) {
		// contract up to op1
		onsite = (lop == 1)? netLG( wire1.lambda[idx1], wire1.gamma[idx1] )
		: netLGLG( wire1.lambda[idx1], wire1.gamma[idx1], wire1.lambda[idx1+1], wire1.gamma[idx1+1] );
		w1_lvec = buildLRVecQn( idx1-1, w1_lvec, wire1.gamma, wire1.lambda, true );
		w1_lvec = buildLRVecQn( w1_lvec, onsite, op1, true );
		// contract up to op2
		for (int i = idx1+lop; i < idx2; ++i) {
			ten = netLG( wire1.lambda[i], wire1.gamma[i] );
			w1_lvec = buildLRVecQn( w1_lvec, ten, true );
		}
		onsite = (lop == 1)? netLG( wire1.lambda[idx2], wire1.gamma[idx2] )
		: netLGLG( wire1.lambda[idx2], wire1.gamma[idx2], wire1.lambda[idx2+1], wire1.gamma[idx2+1] );
		w1_lvec = buildLRVecQn( w1_lvec, onsite, op2, true );
		// contract up to before junction 1
		for (int i = idx2+lop; i < lat_size-1; ++i) {
			ten = netLG( wire1.lambda[i], wire1.gamma[i] );
			w1_lvec = buildLRVecQn( w1_lvec, ten, true );
		}

		w2_rvec = buildLRVecQn( 1, w2_rvec, wire2.gamma, wire2.lambda, false );
		w3_rvec = buildLRVecQn( 1, w3_rvec, wire3.gamma, wire3.lambda, false );
	}
	else if ( w1 == 2 && w2 == 2 && idx1 > 0 && idx1 < idx2 && idx2 <= lat_size-lop ) {
		// contract up to op2
		onsite = (lop == 1)? netGL( wire2.gamma[idx2], wire2.lambda[idx2+1] )
		: netGLGL( wire2.gamma[idx2], wire2.lambda[idx2+1], wire2.gamma[idx2+1], wire2.lambda[idx2+2] );
		w2_rvec = buildLRVecQn( idx2+lop, w2_rvec, wire2.gamma, wire2.lambda, false );
		w2_rvec = buildLRVecQn( w2_rvec, onsite, op2, false );
		// contract up to op1
		for (int i = idx2-1; i >= idx1+lop; --i) {
			ten = netGL( wire2.gamma[i], wire2.lambda[i+1] );
			w2_rvec = buildLRVecQn( w2_rvec, ten, false );
		}
		onsite = (lop == 1)? netGL( wire2.gamma[idx1], wire2.lambda[idx1+1] )
		: netGLGL( wire2.gamma[idx1], wire2.lambda[idx1+1], wire2.gamma[idx1+1], wire2.lambda[idx1+2] );
		w2_rvec = buildLRVecQn( w2_rvec, onsite, op1, false );
		// contract up to before junction 2
		for (int i = idx1-1; i > 0; --i) {
			ten = netGL( wire2.gamma[i], wire2.lambda[i+1] );
			w2_rvec = buildLRVecQn( w2_rvec, ten, false );
		}

		w1_lvec = buildLRVecQn( lat_size-2, w1_lvec, wire1.gamma, wire1.lambda, true );
		w3_rvec = buildLRVecQn( 1, w3_rvec, wire3.gamma, wire3.lambda, false );
	}
	else if ( w1 == 3 && w2 == 3 && idx1 > 0 && idx1 < idx2 && idx2 <= lat_size-lop ) {
		// contract up to op2
		onsite = (lop == 1)? netGL( wire3.gamma[idx2], wire3.lambda[idx2+1] )
		: netGLGL( wire3.gamma[idx2], wire3.lambda[idx2+1], wire3.gamma[idx2+1], wire3.lambda[idx2+2] );
		w3_rvec = buildLRVecQn( idx2+lop, w3_rvec, wire3.gamma, wire3.lambda, false );
		w3_rvec = buildLRVecQn( w3_rvec, onsite, op2, false );
		// contract up to op1
		for (int i = idx2-1; i >= idx1+lop; --i) {
			ten = netGL( wire3.gamma[i], wire3.lambda[i+1] );
			w3_rvec = buildLRVecQn( w3_rvec, ten, false );
		}
		onsite = (lop == 1)? netGL( wire3.gamma[idx1], wire3.lambda[idx1+1] )
		: netGLGL( wire3.gamma[idx1], wire3.lambda[idx1+1], wire3.gamma[idx1+1], wire3.lambda[idx1+2] );
		w3_rvec = buildLRVecQn( w3_rvec, onsite, op1, false );
		// contract up to before junction 3
		for (int i = idx1-1; i > 0; --i) {
			ten = netGL( wire3.gamma[i], wire3.lambda[i+1] );
			w3_rvec = buildLRVecQn( w3_rvec, ten, false );
		}

		w1_lvec = buildLRVecQn( lat_size-2, w1_lvec, wire1.gamma, wire1.lambda, true );
		w2_rvec = buildLRVecQn( 1, w2_rvec, wire2.gamma, wire2.lambda, false );
	}
	else if ( w1 == 1 && w2 == 2 && idx1 < lat_size-lop && idx2 <= lat_size-lop && idx2 > 0 ) {
		// contract up to op1
		onsite = (lop == 1)? netLG( wire1.lambda[idx1], wire1.gamma[idx1] )
		: netLGLG( wire1.lambda[idx1], wire1.gamma[idx1], wire1.lambda[idx1+1], wire1.gamma[idx1+1] );
		w1_lvec = buildLRVecQn( idx1-1, w1_lvec, wire1.gamma, wire1.lambda, true );
		w1_lvec = buildLRVecQn( w1_lvec, onsite, op1, true );
		// contract up to before junction 1
		for (int i = idx1+lop; i < lat_size-1; ++i) {
			ten = netLG( wire1.lambda[i], wire1.gamma[i] );
			w1_lvec = buildLRVecQn( w1_lvec, ten, true );
		}

		// contract up to op2
		onsite = (lop == 1)? netGL( wire2.gamma[idx2], wire2.lambda[idx2+1] )
		: netGLGL( wire2.gamma[idx2], wire2.lambda[idx2+1], wire2.gamma[idx2+1], wire2.lambda[idx2+2] );
		w2_rvec = buildLRVecQn( idx2+lop, w2_rvec, wire2.gamma, wire2.lambda, false );
		w2_rvec = buildLRVecQn( w2_rvec, onsite, op2, false );
		// contract up to before junction 2
		for (int i = idx2-1; i > 0; --i) {
			ten = netGL( wire2.gamma[i], wire2.lambda[i+1] );
			w2_rvec = buildLRVecQn( w2_rvec, ten, false );
		}

		w3_rvec = buildLRVecQn( 1, w3_rvec, wire3.gamma, wire3.lambda, false );
	}
	else if ( w1 == 2 && w2 == 3 && idx1 > 0 && idx1 <= lat_size-lop && idx2 <= lat_size-lop && idx2 > 0 ) {
		// contract up to op2
		onsite = (lop == 1)? netGL( wire2.gamma[idx1], wire2.lambda[idx1+1] )
		: netGLGL( wire2.gamma[idx1], wire2.lambda[idx1+1], wire2.gamma[idx1+1], wire2.lambda[idx1+2] );
		w2_rvec = buildLRVecQn( idx1+lop, w2_rvec, wire2.gamma, wire2.lambda, false );
		if (op1.bondNum() == 3 && fermi) {
			std::vector<int> lab_op1 = {200, 100, -1};
			op1.setLabel(lab_op1);
			applyCrossGate(op1, -1, 200);
		}
		w2_rvec = buildLRVecQn( w2_rvec, onsite, op1, false );
		// contract up to before junction 2
		for (int i = idx1-1; i > 0; --i) {
			ten = netGL( wire2.gamma[i], wire2.lambda[i+1] );
			w2_rvec = buildLRVecQn( w2_rvec, ten, false );
		}

		// contract up to op2
		onsite = (lop == 1)? netGL( wire3.gamma[idx2], wire3.lambda[idx2+1] )
		: netGLGL( wire3.gamma[idx2], wire3.lambda[idx2+1], wire3.gamma[idx2+1], wire3.lambda[idx2+2] );
		w3_rvec = buildLRVecQn( idx2+lop, w3_rvec, wire3.gamma, wire3.lambda, false );
		w3_rvec = buildLRVecQn( w3_rvec, onsite, op2, false );
		// contract up to before junction 3
		for (int i = idx2-1; i > 0; --i) {
			ten = netGL( wire3.gamma[i], wire3.lambda[i+1] );
			w3_rvec = buildLRVecQn( w3_rvec, ten, false );
		}

		w1_lvec = buildLRVecQn( lat_size-2, w1_lvec, wire1.gamma, wire1.lambda, true );
	}
	else if ( w1 == 1 && w2 == 3 && idx1 < lat_size-lop && idx2 <= lat_size-lop && idx2 > 0 ) {
		// contract up to op1
		onsite = (lop == 1)? netLG( wire1.lambda[idx1], wire1.gamma[idx1] )
		: netLGLG( wire1.lambda[idx1], wire1.gamma[idx1], wire1.lambda[idx1+1], wire1.gamma[idx1+1] );
		w1_lvec = buildLRVecQn( idx1-1, w1_lvec, wire1.gamma, wire1.lambda, true );
		w1_lvec = buildLRVecQn( w1_lvec, onsite, op1, true );
		// contract up to before junction 1
		for (int i = idx1+lop; i < lat_size-1; ++i) {
			ten = netLG( wire1.lambda[i], wire1.gamma[i] );
			w1_lvec = buildLRVecQn( w1_lvec, ten, true );
		}

		// contract up to op2
		onsite = (lop == 1)? netGL( wire3.gamma[idx2], wire3.lambda[idx2+1] )
		: netGLGL( wire3.gamma[idx2], wire3.lambda[idx2+1], wire3.gamma[idx2+1], wire3.lambda[idx2+2] );
		w3_rvec = buildLRVecQn( idx2+lop, w3_rvec, wire3.gamma, wire3.lambda, false );
		w3_rvec = buildLRVecQn( w3_rvec, onsite, op2, false );
		// contract up to before junction 3
		for (int i = idx2-1; i > 0; --i) {
			ten = netGL( wire3.gamma[i], wire3.lambda[i+1] );
			w3_rvec = buildLRVecQn( w3_rvec, ten, false );
		}

		w2_rvec = buildLRVecQn( 1, w2_rvec, wire2.gamma, wire2.lambda, false );
	}
	else {
		std::cerr << "In YJuncQnIBC::correlation : Unsupported configuration.\n";
		return corr;
	}

	uni10::CUniTensor ket = netThetaTrngl();
	uni10::CUniTensor bra = dag(ket);

	int lab_ket[] = {1, 101, 102, 103, -2, -3};
	int lab_bra[] = {2, 3, -1, 101, 102, 103};
	int lab_w1l[] = {-1, 1};
	int lab_w2r[] = {-2, 2};
	int lab_w3r[] = {-3, 3};
	int lab_w1l_nh[] = {-1, -10, 1};
	int lab_w2r_nh[] = {-10, -2, 2};
	int lab_w3r_nh[] = {-10, -3, 3};
	ket.setLabel(lab_ket);
	bra.setLabel(lab_bra);

	if (w1_lvec.bondNum() == 3)	w1_lvec.setLabel(lab_w1l_nh);
	else	w1_lvec.setLabel(lab_w1l);
	if (w2_rvec.bondNum() == 3)	w2_rvec.setLabel(lab_w2r_nh);
	else	w2_rvec.setLabel(lab_w2r);
	if (w3_rvec.bondNum() == 3)	w3_rvec.setLabel(lab_w3r_nh);
	else	w3_rvec.setLabel(lab_w3r);

	uni10::CUniTensor rvec;
	std::vector<int> to_cross;
	if (fermi && w1 == 1 && w2 == 2 && w2_rvec.bondNum() == 3) {
		rvec = uni10::contract(ket, w3_rvec);
		rvec = uni10::contract(rvec, w2_rvec);
		applyCrossGate(rvec, -10, 102);
		applyCrossGate(rvec, -10, 101);
	}
	else if (fermi && w1 == 2 && w2 == 3 && w3_rvec.bondNum() == 3) {
		rvec = uni10::contract(ket, w3_rvec);
		applyCrossGate(rvec, -10, 103);
		applyCrossGate(rvec, -10, -2);
		rvec = uni10::contract(rvec, w2_rvec);
	}
	else if (fermi && w1 == 1 && w2 == 3 && w3_rvec.bondNum() == 3) {
		rvec = uni10::contract(ket, w3_rvec);
		applyCrossGate(rvec, -10, 103);
		applyCrossGate(rvec, -10, -2);
		applyCrossGate(rvec, -10, 102);
		applyCrossGate(rvec, -10, 101);
		rvec = uni10::contract(rvec, w2_rvec);
	}
	else {
		rvec = uni10::contract(ket, w3_rvec);
		rvec = uni10::contract(rvec, w2_rvec);
	}
	rvec = uni10::contract(rvec, bra);
	if (rvec.bondNum() == 3) {
		int lab_rvi[] = {-1, -10, 1};
		int lab_rvf[] = {-10, -1, 1};
		rvec.setLabel(lab_rvi);
		rvec.permute(lab_rvf, 2);
	}
	corr = contrLRVecQn( w1_lvec, rvec );

	return corr;
}

//======================================

uni10::CUniTensor YJuncQnIBC::correlation(
	uni10::CUniTensor op1, uni10::CUniTensor op2, std::string loc1, std::string loc2 ) {
	/// correlation on special locations
	uni10::CUniTensor corr;
	uni10::CUniTensor ket, bra;

	uni10::CUniTensor w1_lvec( wire1.lambda[0].bond() );
	uni10::CUniTensor w2_rvec( wire2.lambda[lat_size].bond() );
	uni10::CUniTensor w3_rvec( wire3.lambda[lat_size].bond() );
	w1_lvec.identity();
	w2_rvec.identity();
	w3_rvec.identity();

	int bno = op1.bondNum();
	int lop = (bno/2);
	if ( bno < 2 || bno > 4 || bno != op2.bondNum() ) {
		std::cerr << "In YJuncQnIBC::correlation : Unsupported operator configuration.\n";
		return corr;
	}

	if (lop == 1) {
		uni10::CUniTensor op = (bno == 3 && op1.bond()[2].dim() == 1)?
			otimesPM(op1, op2) : uni10::otimes(op1, op2);	// need a more general otimesPM function

		if ((loc1 == "J1" || loc1 == "1") && (loc2 == "J2" || loc2 == "2"))
			return expVal(op, "12");
		else if ((loc1 == "J2" || loc1 == "2") && (loc2 == "J3" || loc2 == "3"))
			return expVal(op, "23");
		else if ((loc1 == "J1" || loc1 == "1") && (loc2 == "J3" || loc2 == "3"))
			return expVal(op, "13");
		else {
			std::cerr << "In YJuncQnIBC::correlation : Unsupported location + operator configuration.\n";
			return corr;
		}
	}

	if (loc1 == "WJ1" && loc2 == "WJ2") {
		ket = netThetaTrngl_WJTW("12", op1, op2);
		bra = dag( netThetaTrngl_WJTW("12") );
		int lab_ket[] = {1, 100, 101, 102, 103, 104, -2, -3};
		int lab_bra[] = {2, 3, -1, 100, 101, 102, 103, 104};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);

		w1_lvec = buildLRVecQn( lat_size-3, w1_lvec, wire1.gamma, wire1.lambda, true );
		w2_rvec = buildLRVecQn( 2, w2_rvec, wire2.gamma, wire2.lambda, false );
		w3_rvec = buildLRVecQn( 1, w3_rvec, wire3.gamma, wire3.lambda, false );
	}
	else if (loc1 == "WJ2" && loc2 == "WJ3") {
		ket = netThetaTrngl_WJTW("23", op1, op2);
		bra = dag( netThetaTrngl_WJTW("23") );
		int lab_ket[] = {1, 101, 102, 103, 104, 105, -2, -3};
		int lab_bra[] = {2, 3, -1, 101, 102, 103, 104, 105};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);

		w1_lvec = buildLRVecQn( lat_size-2, w1_lvec, wire1.gamma, wire1.lambda, true );
		w2_rvec = buildLRVecQn( 2, w2_rvec, wire2.gamma, wire2.lambda, false );
		w3_rvec = buildLRVecQn( 2, w3_rvec, wire3.gamma, wire3.lambda, false );
	}
	else if (loc1 == "WJ1" && loc2 == "WJ3") {
		ket = netThetaTrngl_WJTW("13", op1, op2);
		bra = dag( netThetaTrngl_WJTW("13") );
		int lab_ket[] = {1, 100, 101, 102, 103, 105, -2, -3};
		int lab_bra[] = {2, 3, -1, 100, 101, 102, 103, 105};
		ket.setLabel(lab_ket);
		bra.setLabel(lab_bra);

		w1_lvec = buildLRVecQn( lat_size-3, w1_lvec, wire1.gamma, wire1.lambda, true );
		w2_rvec = buildLRVecQn( 1, w2_rvec, wire2.gamma, wire2.lambda, false );
		w3_rvec = buildLRVecQn( 2, w3_rvec, wire3.gamma, wire3.lambda, false );
	}
	else {
		std::cerr << "In YJuncQnIBC::correlation : Unsupported location + operator configuration.\n";
		return corr;
	}

	int lab_w1l[] = {-1, 1};
	int lab_w2r[] = {-2, 2};
	int lab_w3r[] = {-3, 3};
	w1_lvec.setLabel(lab_w1l);
	w2_rvec.setLabel(lab_w2r);
	w3_rvec.setLabel(lab_w3r);
	uni10::CUniTensor rvec = uni10::contract(ket, w2_rvec);
	rvec = uni10::contract(rvec, w3_rvec);
	rvec = uni10::contract(rvec, bra);
	corr = contrLRVecQn( w1_lvec, rvec );

	return corr;
}

//======================================

void YJuncQnIBC::tebdImp( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
	uni10::CUniTensor hwr, uni10::CUniTensor hr, std::vector<uni10::CUniTensor>& himps,
	Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS, involving multipications of lambda^-1
	// time evolution operator
	uni10::CUniTensor Ul   = takeExpQn( I * dt, hl );
	uni10::CUniTensor Ulw  = takeExpQn( I * dt, hlw );
	uni10::CUniTensor Uw   = takeExpQn( I * dt, hw );
	uni10::CUniTensor Uwr  = takeExpQn( I * dt, hwr );
	uni10::CUniTensor Ur   = takeExpQn( I * dt, hr );
	uni10::CUniTensor Uimp12 = takeExpQn( I * dt, himps[0] );
	uni10::CUniTensor Uimp23 = takeExpQn( I * dt, himps[1] );
	uni10::CUniTensor Uimp13 = takeExpQn( I * dt, himps[2] );
	uni10::CUniTensor theta;

	if (orderTS == 1) {
		for (int iter = 0; iter < steps; ++iter) {
			// evolve and update odd bonds
			// wire 1
			for (int n = 0; n < lat_size-2; n+=2)
				evol2Site( Uw, wire1.lambda[n], wire1.gamma[n], wire1.lambda[n+1], wire1.gamma[n+1], wire1.lambda[n+2] );
			// wire 2
			for (int n = 2; n < lat_size-1; n+=2)
				evol2Site( Uw, wire2.lambda[n], wire2.gamma[n], wire2.lambda[n+1], wire2.gamma[n+1], wire2.lambda[n+2] );
			// wire 3
			for (int n = 2; n < lat_size-1; n+=2)
				evol2Site( Uw, wire3.lambda[n], wire3.gamma[n], wire3.lambda[n+1], wire3.gamma[n+1], wire3.lambda[n+2] );
			// wire-junct 1
			theta = netThetaTrngl_WJT("WJ1", Uw);
			WJTSVD("WJ1", theta);
			// wire-junct 2
			theta = netThetaTrngl_WJT("WJ2", Uw);
			WJTSVD("WJ2", theta);
			// wire-junct 3
			theta = netThetaTrngl_WJT("WJ3", Uw);
			WJTSVD("WJ3", theta);

			// evolve and update corners
			wire1.updateCorner( true, Ul, Ulw, net_ibc_dir );
			wire2.updateCorner( false, Ur, Uwr, net_ibc_dir );
			wire3.updateCorner( false, Ur, Uwr, net_ibc_dir );

			// evolve and update even bonds
			// wire 1
			for (int n = 1; n < lat_size-1; n+=2)
				evol2Site( Uw, wire1.lambda[n], wire1.gamma[n], wire1.lambda[n+1], wire1.gamma[n+1], wire1.lambda[n+2] );
			// wire 2
			for (int n = 1; n < lat_size-1; n+=2)
				evol2Site( Uw, wire2.lambda[n], wire2.gamma[n], wire2.lambda[n+1], wire2.gamma[n+1], wire2.lambda[n+2] );
			// wire 3
			for (int n = 1; n < lat_size-1; n+=2)
				evol2Site( Uw, wire3.lambda[n], wire3.gamma[n], wire3.lambda[n+1], wire3.gamma[n+1], wire3.lambda[n+2] );
			// junct-junct 12
			theta = netThetaTrngl("12", Uimp12);
			JTSVD("2", theta);
			// junct-junct 23
			theta = netThetaTrngl("23", Uimp23);
			JTSVD("3", theta);
			// junct-junct 13
			theta = netThetaTrngl("13", Uimp13);
			JTSVD("1", theta);
		}
	}
	else {
		// raise exception
	}
}

//======================================

void YJuncQnIBC::tebdImp( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
	uni10::CUniTensor hwr, uni10::CUniTensor hr, uni10::CUniTensor himp,
	Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS, involving multipications of lambda^-1
	std::vector<uni10::CUniTensor> himps;
	himps.push_back(himp);
	himps.push_back(himp);
	himps.push_back(himp);
	tebdImp(hl, hlw, hw, hwr, hr, himps, dt, steps, orderTS);
}

//======================================

void YJuncQnIBC::tebd( uni10::CUniTensor hl, uni10::CUniTensor hlw,
	uni10::CUniTensor hw, uni10::CUniTensor hwr, uni10::CUniTensor hr,
	Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS, involving multipications of lambda^-1
	tebdImp(hl, hlw, hw, hwr, hr, hw, dt, steps, orderTS);
}

//======================================

void YJuncQnIBC::importMPOYJ(
	std::string mpo1_dir, std::string mpo2_dir, std::string mpo3_dir ) {
	///
	// network A: wire1 at left, wire2 + wire3 at right
	// network B: wire1 + wire3 at left, wire2 at right
	// network C: wire1 + wire2 at left, wire3 at right
	// mpoj?[0,1,2] - sy, pl, mi in network A (to update J1)
	// mpoj?[3,4,5] - sy, pl, mi in network B (to update J2)
	// mpoj?[6,7,8] - sy, pl, mi in network C (to update J3)

	// import wire MPO
	importMPOQn( lat_size, mpo1_dir, mpo1_sy, mpo1_pl, mpo1_mi, false );
	importMPOQn( lat_size, mpo2_dir, mpo2_sy, mpo2_pl, mpo2_mi, false );
	importMPOQn( lat_size, mpo3_dir, mpo3_sy, mpo3_pl, mpo3_mi, false );
	// import junction MPO
	mpoj1.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_1as" ) );
	mpoj1.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_1ap" ) );
	mpoj1.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_1am" ) );
	mpoj1.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_1bs" ) );
	mpoj1.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_1bp" ) );
	mpoj1.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_1bm" ) );
	mpoj1.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_1cs" ) );
	mpoj1.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_1cp" ) );
	mpoj1.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_1cm" ) );
	mpoj2.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_2as" ) );
	mpoj2.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_2ap" ) );
	mpoj2.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_2am" ) );
	mpoj2.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_2bs" ) );
	mpoj2.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_2bp" ) );
	mpoj2.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_2bm" ) );
	mpoj2.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_2cs" ) );
	mpoj2.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_2cp" ) );
	mpoj2.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_2cm" ) );
	mpoj3.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_3as" ) );
	mpoj3.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_3ap" ) );
	mpoj3.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_3am" ) );
	mpoj3.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_3bs" ) );
	mpoj3.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_3bp" ) );
	mpoj3.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_3bm" ) );
	mpoj3.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_3cs" ) );
	mpoj3.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_3cp" ) );
	mpoj3.push_back( uni10::CUniTensor( mpo1_dir + "/mpo_3cm" ) );
}

//======================================

void YJuncQnIBC::importMPOYJ( std::string mpo_dir ) {
	///
	importMPOYJ(mpo_dir, mpo_dir, mpo_dir);
}

//======================================

void YJuncQnIBC::perturbMPOYJ( int type, double param ) {
	///
	if (mpo1_sy.size() < lat_size) {
		std::cerr << "In YJuncQnIBC::perturbMPOYJ : should importMPOYJ before perturb.";
		return;
	}
	uni10::CUniTensor Ni = opFU1_grd0( "N", 1, 2 ) + (-0.5) * opFU1_grd0( "id", 1, 2 );
	int dim = mpo1_sy[1].bond()[0].dim();
	switch(type) {
		case 0:
			{
				// constant chemical potential
				MPO pert(dim, 'm');
				pert.putTensor(param * Ni, dim-1, 0);
				uni10::CUniTensor mpo_pert = pert.launch();
				for (int i = 1; i < lat_size; ++i) {
					mpo1_sy[i] += mpo_pert;
					mpo2_sy[i] += mpo_pert;
					mpo3_sy[i] += mpo_pert;
				}
			}
			break;
		case 1:
			{
				// stagger chemical potential
				MPO pert_e(dim, 'm');
				MPO pert_o(dim, 'm');
				pert_e.putTensor(param * Ni, dim-1, 0);
				pert_o.putTensor((-1.)*param * Ni, dim-1, 0);
				uni10::CUniTensor mpo_pert_e = pert_e.launch();
				uni10::CUniTensor mpo_pert_o = pert_o.launch();
				for (int i = 1; i < lat_size; i+=2) {
					mpo1_sy[i] += mpo_pert_e;
					mpo2_sy[i] += mpo_pert_o;
					mpo3_sy[i] += mpo_pert_o;
				}
				for (int i = 2; i < lat_size; i+=2) {
					mpo1_sy[i] += mpo_pert_o;
					mpo2_sy[i] += mpo_pert_e;
					mpo3_sy[i] += mpo_pert_e;
				}
			}
			break;
		default:
			break;
	}
}

//======================================

void YJuncQnIBC::putImpMPOYJ( std::vector<uni10::CUniTensor>& mpo_imp, int wire, int idx ) {
	///
	if (wire == 1) {
		mpo1_sy[idx] = mpo_imp[0];
		mpo1_pl[idx] = mpo_imp[1];
		mpo1_mi[idx] = mpo_imp[2];
	}
	else if (wire == 2) {
		mpo2_sy[idx] = mpo_imp[0];
		mpo2_pl[idx] = mpo_imp[1];
		mpo2_mi[idx] = mpo_imp[2];
	}
	else if (wire == 3) {
		mpo3_sy[idx] = mpo_imp[0];
		mpo3_pl[idx] = mpo_imp[1];
		mpo3_mi[idx] = mpo_imp[2];
	}
	else {
		throw std::invalid_argument("Invalid wire index.");
	}
}

//======================================

void YJuncQnIBC::initMPOHYJ( int update_size, int wire ) {
	///
	int mpoH_size = update_size + 2;
	int n = mpo3_sy.size()-1;

	mpoH_sy.clear();
	mpoH_pl.clear();
	mpoH_mi.clear();
	mpoH_sy.push_back( mpo1_sy[0] );
	mpoH_pl.push_back( mpo1_pl[0] );
	mpoH_mi.push_back( mpo1_mi[0] );
	if (wire == 1) {
		for (int i = 1; i <= update_size; ++i) {
			mpoH_sy.push_back( mpo1_sy[i] );
			mpoH_pl.push_back( mpo1_pl[i] );
			mpoH_mi.push_back( mpo1_mi[i] );
		}
		mpoH_sy.push_back( mpo2_sy[n] );
		mpoH_pl.push_back( mpo2_pl[n] );
		mpoH_mi.push_back( mpo2_mi[n] );
	}
	else if (wire == 2) {
		for (int i = update_size; i >= 1; --i) {
			mpoH_sy.push_back( mpo2_sy[n-i] );
			mpoH_pl.push_back( mpo2_pl[n-i] );
			mpoH_mi.push_back( mpo2_mi[n-i] );
		}
		mpoH_sy.push_back( mpo2_sy[n] );
		mpoH_pl.push_back( mpo2_pl[n] );
		mpoH_mi.push_back( mpo2_mi[n] );
	}
	else if (wire == 3) {
		for (int i = update_size; i >= 1; --i) {
			mpoH_sy.push_back( mpo3_sy[n-i] );
			mpoH_pl.push_back( mpo3_pl[n-i] );
			mpoH_mi.push_back( mpo3_mi[n-i] );
		}
		mpoH_sy.push_back( mpo3_sy[n] );
		mpoH_pl.push_back( mpo3_pl[n] );
		mpoH_mi.push_back( mpo3_mi[n] );
	}
}

//======================================

void YJuncQnIBC::dmrgMPOHYJ( int wire, int idx, int update_size ) {
	///
	bool J1, J2, J3, OOR;
	J1 = (wire == 1 && idx == lat_size-1);
	J2 = (wire == 2 && idx == 0);
	J3 = (wire == 3 && idx == 0);
	OOR = (idx < 0 || idx > lat_size-1);
	if (J1 || J2 || J3 || OOR) {
		std::cerr << "In YJuncQnIBC::dmrgMPOHYJ : On-site index error" << '\n';
		return;
	}

	int mpoH_size = update_size + 2;
	int n = mpoH_size-1;
	if ( mpoH_sy.size() != mpoH_size )
		initMPOHYJ(update_size);
	if (buff1_sy.size() == 0 || buff2_sy.size() == 0 || buff3_sy.size() == 0)
		initDMRGBufferYJ(wire, idx);

	if (wire == 1) {
		if (idx == 0) {
			for (int i = 0; i <= update_size; ++i) {
				mpoH_sy[i] = mpo1_sy[i];
				mpoH_pl[i] = mpo1_pl[i];
				mpoH_mi[i] = mpo1_mi[i];
			}
			mpoH_sy[n] = buff1_sy[ idx + update_size ];
			mpoH_pl[n] = buff1_pl[ idx + update_size ];
			mpoH_mi[n] = buff1_mi[ idx + update_size ];
		}
		else {
			mpoH_sy[0] = buff1_sy[ idx-1 ];
			mpoH_pl[0] = buff1_pl[ idx-1 ];
			mpoH_mi[0] = buff1_mi[ idx-1 ];
			for (int i = 0; i < update_size; ++i) {
				mpoH_sy[i+1] = mpo1_sy[ idx+i+1 ];
				mpoH_pl[i+1] = mpo1_pl[ idx+i+1 ];
				mpoH_mi[i+1] = mpo1_mi[ idx+i+1 ];
			}
			mpoH_sy[n] = buff1_sy[ idx + update_size ];
			mpoH_pl[n] = buff1_pl[ idx + update_size ];
			mpoH_mi[n] = buff1_mi[ idx + update_size ];
		}
	}
	else if (wire == 2) {
		if (idx == lat_size - update_size) {
			mpoH_sy[0] = buff2_sy[ idx-1 ];
			mpoH_pl[0] = buff2_pl[ idx-1 ];
			mpoH_mi[0] = buff2_mi[ idx-1 ];
			for (int i = 0; i <= update_size; ++i) {
				mpoH_sy[i+1] = mpo2_sy[ idx+i+1 ];
				mpoH_pl[i+1] = mpo2_pl[ idx+i+1 ];
				mpoH_mi[i+1] = mpo2_mi[ idx+i+1 ];
			}
		}
		else {
			mpoH_sy[0] = buff2_sy[ idx-1 ];
			mpoH_pl[0] = buff2_pl[ idx-1 ];
			mpoH_mi[0] = buff2_mi[ idx-1 ];
			for (int i = 0; i < update_size; ++i) {
				mpoH_sy[i+1] = mpo2_sy[ idx+i+1 ];
				mpoH_pl[i+1] = mpo2_pl[ idx+i+1 ];
				mpoH_mi[i+1] = mpo2_mi[ idx+i+1 ];
			}
			mpoH_sy[n] = buff2_sy[ idx + update_size ];
			mpoH_pl[n] = buff2_pl[ idx + update_size ];
			mpoH_mi[n] = buff2_mi[ idx + update_size ];
		}
	}
	else if (wire == 3) {
		if (idx == lat_size - update_size) {
			mpoH_sy[0] = buff3_sy[ idx-1 ];
			mpoH_pl[0] = buff3_pl[ idx-1 ];
			mpoH_mi[0] = buff3_mi[ idx-1 ];
			for (int i = 0; i <= update_size; ++i) {
				mpoH_sy[i+1] = mpo3_sy[ idx+i+1 ];
				mpoH_pl[i+1] = mpo3_pl[ idx+i+1 ];
				mpoH_mi[i+1] = mpo3_mi[ idx+i+1 ];
			}
		}
		else {
			mpoH_sy[0] = buff3_sy[ idx-1 ];
			mpoH_pl[0] = buff3_pl[ idx-1 ];
			mpoH_mi[0] = buff3_mi[ idx-1 ];
			for (int i = 0; i < update_size; ++i) {
				mpoH_sy[i+1] = mpo3_sy[ idx+i+1 ];
				mpoH_pl[i+1] = mpo3_pl[ idx+i+1 ];
				mpoH_mi[i+1] = mpo3_mi[ idx+i+1 ];
			}
			mpoH_sy[n] = buff3_sy[ idx + update_size ];
			mpoH_pl[n] = buff3_pl[ idx + update_size ];
			mpoH_mi[n] = buff3_mi[ idx + update_size ];
		}
	}
}

//======================================

void YJuncQnIBC::tensMPOHJT( std::string loc,
	std::vector<int>& cfg, std::vector<uni10::CUniTensor>& tens, bool& JJ23 ) {
	///
	// assert cfg.size() == 3
	tens.clear();

	if (loc == "JT") {
		for (int i = 0; i < cfg.size(); ++i) {
			switch (i) {
				case 0:
					if (cfg[i] == 0) tens.push_back(bfjt1_sy[0]);
					else if (cfg[i] == 1) tens.push_back(bfjt1_pl[0]);
					else if (cfg[i] == -1) tens.push_back(bfjt1_mi[0]);
					else tens.push_back(uni10::CUniTensor());
					break;
				case 1:
					if (cfg[i] == 0) tens.push_back(bfjt2_sy[0]);
					else if (cfg[i] == 1) tens.push_back( bfjt2_pl[ 0 + 3*(int)JJ23 ] );
					else if (cfg[i] == -1) tens.push_back( bfjt2_mi[ 0 + 3*(int)JJ23 ] );
					else tens.push_back(uni10::CUniTensor());
					break;
				case 2:
					if (cfg[i] == 0) tens.push_back(bfjt3_sy[0]);
					else if (cfg[i] == 1) tens.push_back(bfjt3_pl[0]);
					else if (cfg[i] == -1) tens.push_back(bfjt3_mi[0]);
					else tens.push_back(uni10::CUniTensor());
					break;
				default:
					return;
			}
		}
	}
	else if (loc == "1") {
		for (int i = 0; i < cfg.size(); ++i) {
			switch (i) {
				case 0:
					if (cfg[i] == 0) tens.push_back(mpoj1[0]);
					else if (cfg[i] == 1) tens.push_back(mpoj1[1]);
					else if (cfg[i] == -1) tens.push_back(mpoj1[2]);
					else tens.push_back(uni10::CUniTensor());
					break;
				case 1:
					if (cfg[i] == 0) tens.push_back(bfjt2_sy[0]);
					else if (cfg[i] == 1) tens.push_back( bfjt2_pl[ 0 + 3*(int)JJ23 ] );
					else if (cfg[i] == -1) tens.push_back( bfjt2_mi[ 0 + 3*(int)JJ23 ] );
					else tens.push_back(uni10::CUniTensor());
					break;
				case 2:
					if (cfg[i] == 0) tens.push_back(bfjt3_sy[0]);
					else if (cfg[i] == 1) tens.push_back(bfjt3_pl[0]);
					else if (cfg[i] == -1) tens.push_back(bfjt3_mi[0]);
					else tens.push_back(uni10::CUniTensor());
					break;
				default:
					return;
			}
		}
	}
	else if (loc == "2") {
		for (int i = 0; i < cfg.size(); ++i) {
			switch (i) {
				case 0:
					if (cfg[i] == 0) tens.push_back(bfjt1_sy[1]);
					else if (cfg[i] == 1) tens.push_back(bfjt1_pl[1]);
					else if (cfg[i] == -1) tens.push_back(bfjt1_mi[1]);
					else tens.push_back(uni10::CUniTensor());
					break;
				case 1:
					if (cfg[i] == 0) tens.push_back(mpoj2[3]);
					else if (cfg[i] == 1) tens.push_back(mpoj2[4]);
					else if (cfg[i] == -1) tens.push_back(mpoj2[5]);
					else tens.push_back(uni10::CUniTensor());
					break;
				case 2:
					if (cfg[i] == 0) tens.push_back(bfjt3_sy[1]);
					else if (cfg[i] == 1) tens.push_back(bfjt3_pl[1]);
					else if (cfg[i] == -1) tens.push_back(bfjt3_mi[1]);
					else tens.push_back(uni10::CUniTensor());
					break;
				default:
					return;
			}
		}
	}
	else if (loc == "3") {
		for (int i = 0; i < cfg.size(); ++i) {
			switch (i) {
				case 0:
					if (cfg[i] == 0) tens.push_back(bfjt1_sy[2]);
					else if (cfg[i] == 1) tens.push_back(bfjt1_pl[2]);
					else if (cfg[i] == -1) tens.push_back(bfjt1_mi[2]);
					else tens.push_back(uni10::CUniTensor());
					break;
				case 1:
					if (cfg[i] == 0) tens.push_back(bfjt2_sy[2]);
					else if (cfg[i] == 1) tens.push_back( bfjt2_pl[ 2 + 3*(int)JJ23 ] );
					else if (cfg[i] == -1) tens.push_back( bfjt2_mi[ 2 + 3*(int)JJ23 ] );
					else tens.push_back(uni10::CUniTensor());
					break;
				case 2:
					if (cfg[i] == 0) tens.push_back(mpoj3[6]);
					else if (cfg[i] == 1) tens.push_back(mpoj3[7]);
					else if (cfg[i] == -1) tens.push_back(mpoj3[8]);
					else tens.push_back(uni10::CUniTensor());
					break;
				default:
					return;
			}
		}
	}
}

//======================================

void YJuncQnIBC::tensMPOHWJT( std::string loc,
	std::vector<int>& cfg, std::vector<uni10::CUniTensor>& tens, bool& JJ23 ) {
	///
	// assert cfg.size() == 3
	tens.clear();

	if (loc == "WJ1") {
		for (int i = 0; i < cfg.size(); ++i) {
			switch (i) {
				case 0:
					if (cfg[i] == 0) tens.push_back(bfwjt1_sy[0]);
					else if (cfg[i] == 1) tens.push_back(bfwjt1_pl[0]);
					else if (cfg[i] == -1) tens.push_back(bfwjt1_mi[0]);
					else tens.push_back(uni10::CUniTensor());
					break;
				case 1:
					if (cfg[i] == 0) tens.push_back(bfjt2_sy[0]);
					else if (cfg[i] == 1) tens.push_back( bfjt2_pl[ 0 + 3*(int)JJ23 ] );
					else if (cfg[i] == -1) tens.push_back( bfjt2_mi[ 0 + 3*(int)JJ23 ] );
					else tens.push_back(uni10::CUniTensor());
					break;
				case 2:
					if (cfg[i] == 0) tens.push_back(bfjt3_sy[0]);
					else if (cfg[i] == 1) tens.push_back(bfjt3_pl[0]);
					else if (cfg[i] == -1) tens.push_back(bfjt3_mi[0]);
					else tens.push_back(uni10::CUniTensor());
					break;
				default:
					return;
			}
		}
	}
	else if (loc == "WJ2") {
		for (int i = 0; i < cfg.size(); ++i) {
			switch (i) {
				case 0:
					if (cfg[i] == 0) tens.push_back(bfjt1_sy[1]);
					else if (cfg[i] == 1) tens.push_back(bfjt1_pl[1]);
					else if (cfg[i] == -1) tens.push_back(bfjt1_mi[1]);
					else tens.push_back(uni10::CUniTensor());
					break;
				case 1:
					if (cfg[i] == 0) tens.push_back(bfwjt2_sy[1]);
					else if (cfg[i] == 1) tens.push_back( bfwjt2_pl[ 1 + 3*(int)JJ23 ] );
					else if (cfg[i] == -1) tens.push_back( bfwjt2_mi[ 1 + 3*(int)JJ23 ] );
					else tens.push_back(uni10::CUniTensor());
					break;
				case 2:
					if (cfg[i] == 0) tens.push_back(bfjt3_sy[1]);
					else if (cfg[i] == 1) tens.push_back(bfjt3_pl[1]);
					else if (cfg[i] == -1) tens.push_back(bfjt3_mi[1]);
					else tens.push_back(uni10::CUniTensor());
					break;
				default:
					return;
			}
		}
	}
	else if (loc == "WJ3") {
		for (int i = 0; i < cfg.size(); ++i) {
			switch (i) {
				case 0:
					if (cfg[i] == 0) tens.push_back(bfjt1_sy[2]);
					else if (cfg[i] == 1) tens.push_back(bfjt1_pl[2]);
					else if (cfg[i] == -1) tens.push_back(bfjt1_mi[2]);
					else tens.push_back(uni10::CUniTensor());
					break;
				case 1:
					if (cfg[i] == 0) tens.push_back(bfjt2_sy[2]);
					else if (cfg[i] == 1) tens.push_back( bfjt2_pl[ 2 + 3*(int)JJ23 ] );
					else if (cfg[i] == -1) tens.push_back( bfjt2_mi[ 2 + 3*(int)JJ23 ] );
					else tens.push_back(uni10::CUniTensor());
					break;
				case 2:
					if (cfg[i] == 0) tens.push_back(bfwjt3_sy[2]);
					else if (cfg[i] == 1) tens.push_back(bfwjt3_pl[2]);
					else if (cfg[i] == -1) tens.push_back(bfwjt3_mi[2]);
					else tens.push_back(uni10::CUniTensor());
					break;
				default:
					return;
			}
		}
	}
}

//======================================

void YJuncQnIBC::linksMPOHJ( std::vector<int>& cfg,
	bool& JJ12, bool& JJ23, bool& JJ13, bool& WJ1, bool& WJ2, bool& WJ3 ) {
	///
	std::vector<std::string> links;
	findLink(links, cfg, adjs);
	JJ12 = ( std::find(links.begin(), links.end(), "01") != links.end() );
	JJ23 = ( std::find(links.begin(), links.end(), "12") != links.end() );
	JJ13 = ( std::find(links.begin(), links.end(), "02") != links.end() );
	WJ1  = ( std::find(links.begin(), links.end(), "03") != links.end() );
	WJ2  = ( std::find(links.begin(), links.end(), "14") != links.end() );
	WJ3  = ( std::find(links.begin(), links.end(), "25") != links.end() );
}

//======================================

void YJuncQnIBC::linksMPOHJT( std::vector<int>& cfg,
	bool& JJ12, bool& JJ23, bool& JJ13 ) {
	///
	std::vector<std::string> links;
	findLink(links, cfg, adjs_jt);
	JJ12 = ( std::find(links.begin(), links.end(), "01") != links.end() );
	JJ23 = ( std::find(links.begin(), links.end(), "12") != links.end() );
	JJ13 = ( std::find(links.begin(), links.end(), "02") != links.end() );
}

//======================================

void YJuncQnIBC::labsMPOHJT( std::string loc, std::vector<std::vector<int>>& labs,
	bool JJ12, bool JJ23, bool JJ13, bool WJ1, bool WJ2, bool WJ3 ) {
	///
	labs = {{0},{0},{0}};

	if (loc == "JT") {
		if (JJ12) labs[0] = {-1, 201, 12, 13, 1, 101, -12};
		else if (JJ13) labs[0] = {-1, 201, 12, 13, 1, 101, -13};
		else labs[0] = {-1, 201, 12, 13, 1, 101};
		if (JJ12) labs[1] = {12, 202, -2, 23, 102, 2, -12};
		else if (JJ23) labs[1] = {12, 202, -2, 23, 102, 2, -23};
		else labs[1] = {12, 202, -2, 23, 102, 2};
		if (JJ23) labs[2] = {13, 203, -3, 23, 103, 3, -23};
		else if (JJ13) labs[2] = {13, 203, -3, 23, 103, 3, -13};
		else labs[2] = {13, 203, -3, 23, 103, 3};
	}
	else if (loc == "1") {
		if (JJ12) labs[0] = {10, 201, 12, 13, 101, -12};
		else if (JJ13) labs[0] = {10, 201, 12, 13, 101, -13};
		else if (WJ1) labs[0] = {10, 201, 12, 13, 101, -10};
		else labs[0] = {10, 201, 12, 13, 101};
		if (JJ12) labs[1] = {12, 202, -2, 23, 102, 2, -12};
		else if (JJ23) labs[1] = {12, 202, -2, 23, 102, 2, -23};
		else labs[1] = {12, 202, -2, 23, 102, 2};
		if (JJ23) labs[2] = {13, 203, -3, 23, 103, 3, -23};
		else if (JJ13) labs[2] = {13, 203, -3, 23, 103, 3, -13};
		else labs[2] = {13, 203, -3, 23, 103, 3};
	}
	else if (loc == "2") {
		if (JJ12) labs[0] = {-1, 201, 12, 13, 1, 101, -12};
		else if (JJ13) labs[0] = {-1, 201, 12, 13, 1, 101, -13};
		else labs[0] = {-1, 201, 12, 13, 1, 101};
		if (JJ12) labs[1] = {12, 202, 23, 20, 102, -12};
		else if (JJ23) labs[1] = {12, 202, 23, 20, 102, -23};
		else if (WJ2) labs[1] = {12, 202, 23, 20, 102, -20};
		else labs[1] = {12, 202, 23, 20, 102};
		if (JJ23) labs[2] = {13, 203, -3, 23, 103, 3, -23};
		else if (JJ13) labs[2] = {13, 203, -3, 23, 103, 3, -13};
		else labs[2] = {13, 203, -3, 23, 103, 3};
	}
	else if (loc == "3") {
		if (JJ12) labs[0] = {-1, 201, 12, 13, 1, 101, -12};
		else if (JJ13) labs[0] = {-1, 201, 12, 13, 1, 101, -13};
		else labs[0] = {-1, 201, 12, 13, 1, 101};
		if (JJ12) labs[1] = {12, 202, -2, 23, 102, 2, -12};
		else if (JJ23) labs[1] = {12, 202, -2, 23, 102, 2, -23};
		else labs[1] = {12, 202, -2, 23, 102, 2};
		if (JJ23) labs[2] = {13, 203, 23, 30, 103, -23};
		else if (JJ13) labs[2] = {13, 203, 23, 30, 103, -13};
		else if (WJ3) labs[2] = {13, 203, 23, 30, 103, -30};
		else labs[2] = {13, 203, 23, 30, 103};
	}
}

//======================================

void YJuncQnIBC::labsMPOHWJT( std::string loc, std::vector<std::vector<int>>& labs,
	bool JJ12, bool JJ23, bool JJ13 ) {
	///
	// labels for : mpo1, mpo2, mpo3, vec/ket, matvec
	labs = {{0}, {0}, {0}, {0}, {0}};

	if (loc == "WJ1") {
		if (JJ12) labs[0] = {-1, 200, 201, 12, 13, 1, 100, 101, -12};
		else if (JJ13) labs[0] = {-1, 200, 201, 12, 13, 1, 100, 101, -13};
		else labs[0] = {-1, 200, 201, 12, 13, 1, 100, 101};
		if (JJ12) labs[1] = {12, 202, -2, 23, 102, 2, -12};
		else if (JJ23) labs[1] = {12, 202, -2, 23, 102, 2, -23};
		else labs[1] = {12, 202, -2, 23, 102, 2};
		if (JJ23) labs[2] = {13, 203, -3, 23, 103, 3, -23};
		else if (JJ13) labs[2] = {13, 203, -3, 23, 103, 3, -13};
		else labs[2] = {13, 203, -3, 23, 103, 3};
		labs[3] = {1, 100, 101, 102, 103, 2, 3};
		labs[4] = {-1, 200, 201, 202, 203, -2, -3};
	}
	else if (loc == "WJ2") {
		if (JJ12) labs[0] = {-1, 201, 12, 13, 1, 101, -12};
		else if (JJ13) labs[0] = {-1, 201, 12, 13, 1, 101, -13};
		else labs[0] = {-1, 201, 12, 13, 1, 101};
		if (JJ12) labs[1] = {12, 202, 204, -2, 23, 102, 104, 2, -12};
		else if (JJ23) labs[1] = {12, 202, 204, -2, 23, 102, 104, 2, -23};
		else labs[1] = {12, 202, 204, -2, 23, 102, 104, 2};
		if (JJ23) labs[2] = {13, 203, -3, 23, 103, 3, -23};
		else if (JJ13) labs[2] = {13, 203, -3, 23, 103, 3, -13};
		else labs[2] = {13, 203, -3, 23, 103, 3};
		labs[3] = {1, 3, 101, 102, 103, 104, 2};
		labs[4] = {-1, -3, 201, 202, 203, 204, -2};
	}
	else if (loc == "WJ3") {
		if (JJ12) labs[0] = {-1, 201, 12, 13, 1, 101, -12};
		else if (JJ13) labs[0] = {-1, 201, 12, 13, 1, 101, -13};
		else labs[0] = {-1, 201, 12, 13, 1, 101};
		if (JJ12) labs[1] = {12, 202, -2, 23, 102, 2, -12};
		else if (JJ23) labs[1] = {12, 202, -2, 23, 102, 2, -23};
		else labs[1] = {12, 202, -2, 23, 102, 2};
		if (JJ23) labs[2] = {13, 203, 205, -3, 23, 103, 105, 3, -23};
		else if (JJ13) labs[2] = {13, 203, 205, -3, 23, 103, 105, 3, -13};
		else labs[2] = {13, 203, 205, -3, 23, 103, 105, 3};
		labs[3] = {1, 2, 101, 102, 103, 105, 3};
		labs[4] = {-1, -2, 201, 202, 203, 205, -3};
	}
}

//======================================

uni10::CUniTensor YJuncQnIBC::netRenormJT1( std::vector<int>& cfg, uni10::CUniTensor ket ) {
	///
	uni10::CUniTensor rnj;

	bool JJ12, JJ23, JJ13, WJ1, WJ2, WJ3;
	linksMPOHJ(cfg, JJ12, JJ23, JJ13, WJ1, WJ2, WJ3);
	std::vector<uni10::CUniTensor> tens;
	tensMPOHJT("1", cfg, tens, JJ23);

	// set labels
	std::vector<std::vector<int>> labs;
	labsMPOHJT("1", labs, JJ12, JJ23, JJ13, WJ1, WJ2, WJ3);
	std::vector<int> lab_ket = {1, 101, 102, 103, 2, 3};
	ket.setLabel(lab_ket);
	tens[0].setLabel(labs[0]); tens[1].setLabel(labs[1]); tens[2].setLabel(labs[2]);

	// contraction
	std::vector<int> to_cross;
	if (JJ23) {
		rnj = uni10::contract(ket, tens[2], true);
		// apply cross gate between 23
		applyCrossGate(rnj, -23, 2);
		rnj = uni10::contract(rnj, tens[1], true);
		rnj = uni10::contract(rnj, tens[0], true);
	}
	else if (JJ13) {
		rnj = uni10::contract(ket, tens[2], true);
		// apply cross gate between 13
		applyCrossGate(rnj, -13, 2);
		applyCrossGate(rnj, -13, 102);
		rnj = uni10::contract(rnj, tens[1], true);
		rnj = uni10::contract(rnj, tens[0], true);
	}
	else {
		rnj = uni10::contract(ket, tens[2], true);
		rnj = uni10::contract(rnj, tens[1], true);
		if (WJ1) {
			to_cross = {201, 12, 13, 101};
			applyCrossGate(tens[0], -10, to_cross);
		}
		rnj = uni10::contract(rnj, tens[0], true);
	}

	// permute rnj
	std::vector<int> lab_rv0 = {10, 201, 202, 203, -2, -3, 1};
	std::vector<int> lab_rv1 = {10, 201, 202, 203, -2, -3, 1, -10};
	if (WJ1)
		rnj.permute(lab_rv1, 6);
	else
		rnj.permute(lab_rv0, 6);

	tens.clear();
	return rnj;
}

//======================================

uni10::CUniTensor YJuncQnIBC::netRenormJT2( std::vector<int>& cfg, uni10::CUniTensor ket ) {
	///
	uni10::CUniTensor rnj;

	bool JJ12, JJ23, JJ13, WJ1, WJ2, WJ3;
	linksMPOHJ(cfg, JJ12, JJ23, JJ13, WJ1, WJ2, WJ3);
	std::vector<uni10::CUniTensor> tens;
	tensMPOHJT("2", cfg, tens, JJ23);

	// set labels
	std::vector<std::vector<int>> labs;
	labsMPOHJT("2", labs, JJ12, JJ23, JJ13, WJ1, WJ2, WJ3);
	std::vector<int> lab_ket = {1, 101, 102, 103, 2, 3};
	ket.setLabel(lab_ket);
	tens[0].setLabel(labs[0]); tens[1].setLabel(labs[1]); tens[2].setLabel(labs[2]);

	// contraction
	std::vector<int> to_cross;
	if (JJ23) {
		rnj = uni10::contract(ket, tens[2], true);
		// apply cross gate between 23
		applyCrossGate(rnj, -23, 2);
		rnj = uni10::contract(rnj, tens[1], true);
		rnj = uni10::contract(rnj, tens[0], true);
	}
	else if (JJ13) {
		rnj = uni10::contract(ket, tens[2], true);
		// apply cross gate between 13
		applyCrossGate(rnj, -13, 2);
		applyCrossGate(rnj, -13, 102);
		rnj = uni10::contract(rnj, tens[1], true);
		rnj = uni10::contract(rnj, tens[0], true);
	}
	else {
		rnj = uni10::contract(ket, tens[2], true);
		if (JJ12) {
			// apply cross gate between 12
			to_cross = {202, 23, 20, 102};
			applyCrossGate(tens[1], -12, to_cross);
		}
		rnj = uni10::contract(rnj, tens[1], true);
		rnj = uni10::contract(rnj, tens[0], true);
	}

	// permute rnj
	std::vector<int> lab_lv0 = {-1, 201, 202, 203, -3, 20, 2};
	std::vector<int> lab_lv1 = {-1, 201, 202, 203, -3, 20, 2, -20};
	if (WJ2)
		rnj.permute(lab_lv1, 5);
	else
		rnj.permute(lab_lv0, 5);

	tens.clear();
	return rnj;
}

//======================================

uni10::CUniTensor YJuncQnIBC::netRenormJT3( std::vector<int>& cfg, uni10::CUniTensor ket ) {
	///
	uni10::CUniTensor rnj;

	bool JJ12, JJ23, JJ13, WJ1, WJ2, WJ3;
	linksMPOHJ(cfg, JJ12, JJ23, JJ13, WJ1, WJ2, WJ3);
	std::vector<uni10::CUniTensor> tens;
	tensMPOHJT("3", cfg, tens, JJ23);

	// set labels
	std::vector<std::vector<int>> labs;
	labsMPOHJT("3", labs, JJ12, JJ23, JJ13, WJ1, WJ2, WJ3);
	std::vector<int> lab_ket = {1, 101, 102, 103, 2, 3};
	ket.setLabel(lab_ket);
	tens[0].setLabel(labs[0]); tens[1].setLabel(labs[1]); tens[2].setLabel(labs[2]);

	// contraction
	std::vector<int> to_cross;
	if (JJ23) {
		to_cross = {203, 23, 30, 103};
		applyCrossGate(tens[2], -23, to_cross);
		rnj = uni10::contract(ket, tens[2], true);
		// apply cross gate between 23
		applyCrossGate(rnj, -23, 2);
		rnj = uni10::contract(rnj, tens[1], true);
		rnj = uni10::contract(rnj, tens[0], true);
	}
	else if (JJ13) {
		to_cross = {203, 23, 30, 103};
		applyCrossGate(tens[2], -13, to_cross);
		rnj = uni10::contract(ket, tens[2], true);
		// apply cross gate between 13
		applyCrossGate(rnj, -13, 2);
		applyCrossGate(rnj, -13, 102);
		rnj = uni10::contract(rnj, tens[1], true);
		rnj = uni10::contract(rnj, tens[0], true);
	}
	else {
		rnj = uni10::contract(ket, tens[2], true);
		rnj = uni10::contract(rnj, tens[1], true);
		rnj = uni10::contract(rnj, tens[0], true);
	}

	// permute rnj
	std::vector<int> lab_lv0 = {-1, 201, 202, 203, -2, 30, 3};
	std::vector<int> lab_lv1 = {-1, 201, 202, 203, -2, 30, 3, -30};
	if (WJ3)
		rnj.permute(lab_lv1, 5);
	else
		rnj.permute(lab_lv0, 5);

	tens.clear();
	return rnj;
}

//======================================

void YJuncQnIBC::renormMPOJT1() {
	///
	if (buff1_sy.size() == 0 || buff2_sy.size() == 0 || buff3_sy.size() == 0) {
		std::cerr << "In YJuncQnIBC::renormMPOJT1 : Buffer(s) not found." << '\n';
		return;
	}
	int n = (int)mpoH_sy.size()-1;
	std::vector<std::vector<int>> cfgs;
	std::vector<std::vector<int>> given;

	uni10::CUniTensor ket = netThetaTrngl_red("1");
	uni10::CUniTensor bra = dag(ket);

	std::vector<int> lab_bra = {-2, -3, -1, 201, 202, 203};
	std::vector<int> lab_rv0 = {10, -1, 1};
	std::vector<int> lab_rv1 = {10, -1, 1, -10};
	bra.setLabel(lab_bra);

	// symmetric part
	given = {{3, 0}, {4, 0}, {5, 0}};
	findCfgs(cfgs, 0, adjs, given);
	mpoH_sy[n] = netRenormJT1(cfgs[0], ket);
	for (int i = 1; i < cfgs.size(); ++i)
		mpoH_sy[n] += netRenormJT1(cfgs[i], ket);
	mpoH_sy[n] = uni10::contract(mpoH_sy[n], bra, true);
	mpoH_sy[n].permute(lab_rv0, 2);
	cfgs.clear();

	// plus part
	given = {{3, -1}, {4, 0}, {5, 0}};
	findCfgs(cfgs, 0, adjs, given);
	mpoH_pl[n] = netRenormJT1(cfgs[0], ket);
	for (int i = 1; i < cfgs.size(); ++i)
		mpoH_pl[n] += netRenormJT1(cfgs[i], ket);
	mpoH_pl[n] = uni10::contract(mpoH_pl[n], bra, true);
	mpoH_pl[n].permute(lab_rv1, 2);
	cfgs.clear();

	// minus part
	given = {{3, 1}, {4, 0}, {5, 0}};
	findCfgs(cfgs, 0, adjs, given);
	mpoH_mi[n] = netRenormJT1(cfgs[0], ket);
	for (int i = 1; i < cfgs.size(); ++i)
		mpoH_mi[n] += netRenormJT1(cfgs[i], ket);
	mpoH_mi[n] = uni10::contract(mpoH_mi[n], bra, true);
	mpoH_mi[n].permute(lab_rv1, 2);
	cfgs.clear();

	// save the renormalized mpo to buffer
	buff1_sy[lat_size-1] = mpoH_sy[n];
	buff1_pl[lat_size-1] = mpoH_pl[n];
	buff1_mi[lat_size-1] = mpoH_mi[n];
}

//======================================

void YJuncQnIBC::renormMPOJT2() {
	///
	if (buff1_sy.size() == 0 || buff2_sy.size() == 0 || buff3_sy.size() == 0) {
		std::cerr << "In YJuncQnIBC::renormMPOJT2 : Buffer(s) not found." << '\n';
		return;
	}
	std::vector<std::vector<int>> cfgs;
	std::vector<std::vector<int>> given;

	uni10::CUniTensor ket = netThetaTrngl_red("2");
	uni10::CUniTensor bra = dag(ket);

	std::vector<int> lab_bra = {-2, -3, -1, 201, 202, 203};
	std::vector<int> lab_lv0 = {-2, 20, 2};
	std::vector<int> lab_lv1 = {-2, 20, 2, -20};
	bra.setLabel(lab_bra);

	// symmetric part
	given = {{3, 0}, {4, 0}, {5, 0}};
	findCfgs(cfgs, 0, adjs, given);
	mpoH_sy[0] = netRenormJT2(cfgs[0], ket);
	for (int i = 1; i < cfgs.size(); ++i)
		mpoH_sy[0] += netRenormJT2(cfgs[i], ket);
	mpoH_sy[0] = uni10::contract(mpoH_sy[0], bra, true);
	mpoH_sy[0].permute(lab_lv0, 1);
	cfgs.clear();

	// plus part
	given = {{3, 0}, {4, -1}, {5, 0}};
	findCfgs(cfgs, 0, adjs, given);
	mpoH_pl[0] = netRenormJT2(cfgs[0], ket);
	for (int i = 1; i < cfgs.size(); ++i)
		mpoH_pl[0] += netRenormJT2(cfgs[i], ket);
	mpoH_pl[0] = uni10::contract(mpoH_pl[0], bra, true);
	mpoH_pl[0].permute(lab_lv1, 1);
	cfgs.clear();

	// minus part
	given = {{3, 0}, {4, 1}, {5, 0}};
	findCfgs(cfgs, 0, adjs, given);
	mpoH_mi[0] = netRenormJT2(cfgs[0], ket);
	for (int i = 1; i < cfgs.size(); ++i)
		mpoH_mi[0] += netRenormJT2(cfgs[i], ket);
	mpoH_mi[0] = uni10::contract(mpoH_mi[0], bra, true);
	mpoH_mi[0].permute(lab_lv1, 1);
	cfgs.clear();

	// save the renormalized mpo to buffer
	buff2_sy[0] = mpoH_sy[0];
	buff2_pl[0] = mpoH_pl[0];
	buff2_mi[0] = mpoH_mi[0];
}

//======================================

void YJuncQnIBC::renormMPOJT3() {
	///
	if (buff1_sy.size() == 0 || buff2_sy.size() == 0 || buff3_sy.size() == 0) {
		std::cerr << "In YJuncQnIBC::renormMPOJT3 : Buffer(s) not found." << '\n';
		return;
	}
	std::vector<std::vector<int>> cfgs;
	std::vector<std::vector<int>> given;

	uni10::CUniTensor ket = netThetaTrngl_red("3");
	uni10::CUniTensor bra = dag(ket);

	std::vector<int> lab_bra = {-2, -3, -1, 201, 202, 203};
	std::vector<int> lab_lv0 = {-3, 30, 3};
	std::vector<int> lab_lv1 = {-3, 30, 3, -30};
	bra.setLabel(lab_bra);

	// symmetric part
	given = {{3, 0}, {4, 0}, {5, 0}};
	findCfgs(cfgs, 0, adjs, given);
	mpoH_sy[0] = netRenormJT3(cfgs[0], ket);
	for (int i = 1; i < cfgs.size(); ++i)
		mpoH_sy[0] += netRenormJT3(cfgs[i], ket);
	mpoH_sy[0] = uni10::contract(mpoH_sy[0], bra, true);
	mpoH_sy[0].permute(lab_lv0, 1);
	cfgs.clear();

	// plus part
	given = {{3, 0}, {4, 0}, {5, -1}};
	findCfgs(cfgs, 0, adjs, given);
	mpoH_pl[0] = netRenormJT3(cfgs[0], ket);
	for (int i = 1; i < cfgs.size(); ++i)
		mpoH_pl[0] += netRenormJT3(cfgs[i], ket);
	mpoH_pl[0] = uni10::contract(mpoH_pl[0], bra, true);
	mpoH_pl[0].permute(lab_lv1, 1);
	cfgs.clear();

	// minus part
	given = {{3, 0}, {4, 0}, {5, 1}};
	findCfgs(cfgs, 0, adjs, given);
	mpoH_mi[0] = netRenormJT3(cfgs[0], ket);
	for (int i = 1; i < cfgs.size(); ++i)
		mpoH_mi[0] += netRenormJT3(cfgs[i], ket);
	mpoH_mi[0] = uni10::contract(mpoH_mi[0], bra, true);
	mpoH_mi[0].permute(lab_lv1, 1);
	cfgs.clear();

	// save the renormalized mpo to buffer
	buff3_sy[0] = mpoH_sy[0];
	buff3_pl[0] = mpoH_pl[0];
	buff3_mi[0] = mpoH_mi[0];
}

//======================================

void YJuncQnIBC::updateDMRGBufferYJ( int wire, int idx,
	bool sweep_right, int update_size, int inc_size ) {
	///
	if (wire != 1 && wire != 2 && wire != 3) {
		std::cerr << "In YJuncQnIBC::updateDMRGBufferYJ : Invalid wire index." << '\n';
		return;
	}
	if (idx < 0 || idx > lat_size-1) {
		std::cerr << "In YJuncQnIBC::updateDMRGBufferYJ : On-site index error." << '\n';
		return;
	}

	int n = (int)mpoH_sy.size()-1;

	if (wire == 1) {
		if (sweep_right) {
			for (int i = idx; i < idx+inc_size; ++i) {
				renormMPOL( mpoH_sy, mpoH_pl, mpoH_mi, netLG( wire1.lambda[i], wire1.gamma[i] ), false );
				buff1_sy[i] = mpoH_sy[0];
				buff1_pl[i] = mpoH_pl[0];
				buff1_mi[i] = mpoH_mi[0];
				mpoH_sy[1] = mpo1_sy[i+2];  // mpo has 2 more sites than mps: mpo[2] acts on mps[1]
				mpoH_pl[1] = mpo1_pl[i+2];
				mpoH_mi[1] = mpo1_mi[i+2];
			}
		}
		else {
			for (int i = idx; i > idx-inc_size; --i) {
				if (i == lat_size-1) {
					updateDMRGBufferWJ("2");
					updateDMRGBufferWJ("3");
					renormMPOJT1();
				}
				else {
					renormMPOR( mpoH_sy, mpoH_pl, mpoH_mi, netGL( wire1.gamma[i], wire1.lambda[i+1]), false );
					buff1_sy[i] = mpoH_sy[n];
					buff1_pl[i] = mpoH_pl[n];
					buff1_mi[i] = mpoH_mi[n];
					mpoH_sy[n-1] = mpo1_sy[i];  // mpo has 2 more sites than mps: mpo[L-1] acts on mps[L-2]
					mpoH_pl[n-1] = mpo1_pl[i];
					mpoH_mi[n-1] = mpo1_mi[i];
				}
			}
		}
	}
	else if (wire == 2) {
		if (sweep_right) {
			for (int i = idx; i < idx+inc_size; ++i) {
				if (i == 0) {
					updateDMRGBufferWJ("1");
					updateDMRGBufferWJ("3");
					renormMPOJT2();
				}
				else {
					renormMPOL( mpoH_sy, mpoH_pl, mpoH_mi, netLG( wire2.lambda[i], wire2.gamma[i] ), false );
					buff2_sy[i] = mpoH_sy[0];
					buff2_pl[i] = mpoH_pl[0];
					buff2_mi[i] = mpoH_mi[0];
					mpoH_sy[1] = mpo2_sy[i+2];  // mpo has 2 more sites than mps: mpo[2] acts on mps[1]
					mpoH_pl[1] = mpo2_pl[i+2];
					mpoH_mi[1] = mpo2_mi[i+2];
				}
			}
		}
		else {
			for (int i = idx; i > idx-inc_size; --i) {
				renormMPOR( mpoH_sy, mpoH_pl, mpoH_mi, netGL( wire2.gamma[i], wire2.lambda[i+1]), false );
				buff2_sy[i] = mpoH_sy[n];
				buff2_pl[i] = mpoH_pl[n];
				buff2_mi[i] = mpoH_mi[n];
				mpoH_sy[n-1] = mpo2_sy[i];  // mpo has 2 more sites than mps: mpo[L-1] acts on mps[L-2]
				mpoH_pl[n-1] = mpo2_pl[i];
				mpoH_mi[n-1] = mpo2_mi[i];
			}
		}
	}
	else if (wire == 3) {
		if (sweep_right) {
			for (int i = idx; i < idx+inc_size; ++i) {
				if (i == 0) {
					updateDMRGBufferWJ("1");
					updateDMRGBufferWJ("2");
					renormMPOJT3();
				}
				else {
					renormMPOL( mpoH_sy, mpoH_pl, mpoH_mi, netLG( wire3.lambda[i], wire3.gamma[i] ), false );
					buff3_sy[i] = mpoH_sy[0];
					buff3_pl[i] = mpoH_pl[0];
					buff3_mi[i] = mpoH_mi[0];
					mpoH_sy[1] = mpo3_sy[i+2];  // mpo has 2 more sites than mps: mpo[2] acts on mps[1]
					mpoH_pl[1] = mpo3_pl[i+2];
					mpoH_mi[1] = mpo3_mi[i+2];
				}
			}
		}
		else {
			for (int i = idx; i > idx-inc_size; --i) {
				renormMPOR( mpoH_sy, mpoH_pl, mpoH_mi, netGL( wire3.gamma[i], wire3.lambda[i+1]), false );
				buff3_sy[i] = mpoH_sy[n];
				buff3_pl[i] = mpoH_pl[n];
				buff3_mi[i] = mpoH_mi[n];
				mpoH_sy[n-1] = mpo3_sy[i];  // mpo has 2 more sites than mps: mpo[L-1] acts on mps[L-2]
				mpoH_pl[n-1] = mpo3_pl[i];
				mpoH_mi[n-1] = mpo3_mi[i];
			}
		}
	}
}

//======================================

void YJuncQnIBC::initDMRGBufferYJ( int wire, int idx ) {
	///
	if (wire != 1 && wire != 2 && wire != 3) {
		std::cerr << "In YJuncQnIBC::initDMRGBufferYJ : Invalid wire index." << '\n';
		return;
	}
	// wire = 1 && idx = lat_size-1 unavailable cuz it should not be a mpo_l buffer
	bool to_exclude = (wire == 1 && idx == lat_size-1);
	bool OOR = (idx < 0 || idx > lat_size-1);
	if (to_exclude || OOR) {
		std::cerr << "In YJuncQnIBC::initDMRGBufferYJ : On-site index error." << '\n';
		return;
	}

	buff1_sy.clear(); buff1_pl.clear(); buff1_mi.clear();
	buff2_sy.clear(); buff2_pl.clear(); buff2_mi.clear();
	buff3_sy.clear(); buff3_pl.clear(); buff3_mi.clear();
	bfjt1_sy.clear(); bfjt1_pl.clear(); bfjt1_mi.clear();
	bfjt2_sy.clear(); bfjt2_pl.clear(); bfjt2_mi.clear();
	bfjt3_sy.clear(); bfjt3_pl.clear(); bfjt3_mi.clear();
	for (int i = 0; i < lat_size; ++i) {
		buff1_sy.push_back( uni10::CUniTensor() );
		buff1_pl.push_back( uni10::CUniTensor() );
		buff1_mi.push_back( uni10::CUniTensor() );
		buff2_sy.push_back( uni10::CUniTensor() );
		buff2_pl.push_back( uni10::CUniTensor() );
		buff2_mi.push_back( uni10::CUniTensor() );
		buff3_sy.push_back( uni10::CUniTensor() );
		buff3_pl.push_back( uni10::CUniTensor() );
		buff3_mi.push_back( uni10::CUniTensor() );
	}

	// build buffers up to the site before junctions
	int update_size = 1;
	initMPOHYJ(update_size, 1);
	for (int i = 0; i < lat_size-1; ++i) {
		renormMPOL( mpoH_sy, mpoH_pl, mpoH_mi, netLG( wire1.lambda[i], wire1.gamma[i] ), false );
		buff1_sy[i] = mpoH_sy[0];
		buff1_pl[i] = mpoH_pl[0];
		buff1_mi[i] = mpoH_mi[0];
		mpoH_sy[1] = mpo1_sy[i+2];  // mpo has 2 more sites than mps: mpo[2] acts on mps[1]
		mpoH_pl[1] = mpo1_pl[i+2];
		mpoH_mi[1] = mpo1_mi[i+2];
	}
	int n = (int)mpoH_sy.size()-1;
	initMPOHYJ(update_size, 2);
	for (int i = lat_size-1; i > 0; --i) {
		renormMPOR( mpoH_sy, mpoH_pl, mpoH_mi, netGL( wire2.gamma[i], wire2.lambda[i+1]), false );
		buff2_sy[i] = mpoH_sy[n];
		buff2_pl[i] = mpoH_pl[n];
		buff2_mi[i] = mpoH_mi[n];
		mpoH_sy[n-1] = mpo2_sy[i];  // mpo has 2 more sites than mps: mpo[L-1] acts on mps[L-2]
		mpoH_pl[n-1] = mpo2_pl[i];
		mpoH_mi[n-1] = mpo2_mi[i];
	}
	initMPOHYJ(update_size, 3);
	for (int i = lat_size-1; i > 0; --i) {
		renormMPOR( mpoH_sy, mpoH_pl, mpoH_mi, netGL( wire3.gamma[i], wire3.lambda[i+1]), false );
		buff3_sy[i] = mpoH_sy[n];
		buff3_pl[i] = mpoH_pl[n];
		buff3_mi[i] = mpoH_mi[n];
		mpoH_sy[n-1] = mpo3_sy[i];  // mpo has 2 more sites than mps: mpo[L-1] acts on mps[L-2]
		mpoH_pl[n-1] = mpo3_pl[i];
		mpoH_mi[n-1] = mpo3_mi[i];
	}

	// build buffer of junction sites
	updateDMRGBufferWJ("1");
	updateDMRGBufferWJ("2");
	updateDMRGBufferWJ("3");
	renormMPOJT1();
	renormMPOJT2();
	renormMPOJT3();

	// update buffer to the desired location
	if (wire == 1) {
		for (int i = lat_size-1; i > idx; --i)
			updateDMRGBufferYJ( 1, i, false, 1 );
	}
	else if (wire == 2) {
		for (int i = 0; i <= idx; ++i)
			updateDMRGBufferYJ( 2, i, true, 1 );
	}
	else if (wire == 3) {
		for (int i = 0; i <= idx; ++i)
			updateDMRGBufferYJ( 3, i, true, 1 );
	}
}

//======================================

void YJuncQnIBC::updateDMRGBufferWJ( std::string loc ) {
	///
	// assert lat_size > =4
	if (bfjt1_sy.size() < 3) {
		bfjt1_sy = std::vector<uni10::CUniTensor>(3);
		bfjt1_pl = std::vector<uni10::CUniTensor>(3);
		bfjt1_mi = std::vector<uni10::CUniTensor>(3);
		bfjt2_sy = std::vector<uni10::CUniTensor>(6);
		bfjt2_pl = std::vector<uni10::CUniTensor>(6);
		bfjt2_mi = std::vector<uni10::CUniTensor>(6);
		bfjt3_sy = std::vector<uni10::CUniTensor>(3);
		bfjt3_pl = std::vector<uni10::CUniTensor>(3);
		bfjt3_mi = std::vector<uni10::CUniTensor>(3);
	}

	std::vector<int> lab_lsy, lab_lpm, lab_rsy, lab_rpm;
	std::vector<int> lab_msy, lab_mpm;
	std::vector<int> lab_jsy, lab_jpm;
	std::vector<int> lab_tsy, lab_tpm;
	std::vector<int> to_cross;
	uni10::CUniTensor ten_sy, ten_pl, ten_mi;
	uni10::CUniTensor mpo_sy, mpo_pl, mpo_mi;
	int ibn_pl, ibn_mi;

	if (loc == "1") {
		lab_lsy = {-1, 10, 1};
		lab_lpm = {-1, 10, 1, -10};
		lab_msy = {10, 201, 12, 13, 101};
		lab_mpm = {10, 201, 12, 13, 101, -10};
		to_cross = {201, 12, 13, 101};
		lab_tsy = {-1, 201, 12, 13, 1, 101};
		lab_tpm = {-1, 201, 12, 13, 1, 101, -10};

		ten_sy = buff1_sy[lat_size-2]; ten_pl = buff1_pl[lat_size-2]; ten_mi = buff1_mi[lat_size-2];
		ten_sy.setLabel(lab_lsy); ten_pl.setLabel(lab_lpm); ten_mi.setLabel(lab_lpm);

		// network A
		mpo_sy = mpoj1[0]; mpo_pl = mpoj1[1]; mpo_mi = mpoj1[2];
		mpo_sy.setLabel(lab_msy); mpo_pl.setLabel(lab_mpm); mpo_mi.setLabel(lab_mpm);
		ibn_pl = mpo_pl.inBondNum(); ibn_mi = mpo_mi.inBondNum();
		bfjt1_pl[0] = uni10::contract(ten_sy, mpo_pl, true);
		bfjt1_mi[0] = uni10::contract(ten_sy, mpo_mi, true);
		mpo_pl.permute(ibn_pl); mpo_mi.permute(ibn_mi);
		applyCrossGate(mpo_pl, -10, to_cross);
		applyCrossGate(mpo_mi, -10, to_cross);
		bfjt1_sy[0] = uni10::contract(ten_sy, mpo_sy, true);
		bfjt1_sy[0] += uni10::contract(ten_pl, mpo_mi, true);
		bfjt1_sy[0] += uni10::contract(ten_mi, mpo_pl, true);
		bfjt1_sy[0].permute(lab_tsy, 2); bfjt1_pl[0].permute(lab_tpm, 2); bfjt1_mi[0].permute(lab_tpm, 2);

		// network B
		mpo_sy = mpoj1[3]; mpo_pl = mpoj1[4]; mpo_mi = mpoj1[5];
		mpo_sy.setLabel(lab_msy); mpo_pl.setLabel(lab_mpm); mpo_mi.setLabel(lab_mpm);
		ibn_pl = mpo_pl.inBondNum(); ibn_mi = mpo_mi.inBondNum();
		bfjt1_pl[1] = uni10::contract(ten_sy, mpo_pl, true);
		bfjt1_mi[1] = uni10::contract(ten_sy, mpo_mi, true);
		mpo_pl.permute(ibn_pl); mpo_mi.permute(ibn_mi);
		applyCrossGate(mpo_pl, -10, to_cross);
		applyCrossGate(mpo_mi, -10, to_cross);
		bfjt1_sy[1] = uni10::contract(ten_sy, mpo_sy, true);
		bfjt1_sy[1] += uni10::contract(ten_pl, mpo_mi, true);
		bfjt1_sy[1] += uni10::contract(ten_mi, mpo_pl, true);
		bfjt1_sy[1].permute(lab_tsy, 2); bfjt1_pl[1].permute(lab_tpm, 2); bfjt1_mi[1].permute(lab_tpm, 2);

		// network C
		mpo_sy = mpoj1[6]; mpo_pl = mpoj1[7]; mpo_mi = mpoj1[8];
		mpo_sy.setLabel(lab_msy); mpo_pl.setLabel(lab_mpm); mpo_mi.setLabel(lab_mpm);
		ibn_pl = mpo_pl.inBondNum(); ibn_mi = mpo_mi.inBondNum();
		bfjt1_pl[2] = uni10::contract(ten_sy, mpo_pl, true);
		bfjt1_mi[2] = uni10::contract(ten_sy, mpo_mi, true);
		mpo_pl.permute(ibn_pl); mpo_mi.permute(ibn_mi);
		applyCrossGate(mpo_pl, -10, to_cross);
		applyCrossGate(mpo_mi, -10, to_cross);
		bfjt1_sy[2] = uni10::contract(ten_sy, mpo_sy, true);
		bfjt1_sy[2] += uni10::contract(ten_pl, mpo_mi, true);
		bfjt1_sy[2] += uni10::contract(ten_mi, mpo_pl, true);
		bfjt1_sy[2].permute(lab_tsy, 2); bfjt1_pl[2].permute(lab_tpm, 2); bfjt1_mi[2].permute(lab_tpm, 2);
	}
	else if (loc == "2") {
		lab_rsy = {20, -2, 2};
		lab_rpm = {20, -2, 2, -20};
		lab_msy = {12, 202, 23, 20, 102};
		lab_mpm = {12, 202, 23, 20, 102, -20};
		to_cross = {202, 23, 20, 102};
		lab_tsy = {12, 202, -2, 23, 102, 2};
		lab_tpm = {12, 202, -2, 23, 102, 2, -20};

		ten_sy = buff2_sy[1]; ten_pl = buff2_pl[1]; ten_mi = buff2_mi[1];
		ten_sy.setLabel(lab_rsy); ten_pl.setLabel(lab_rpm); ten_mi.setLabel(lab_rpm);

		// network A
		mpo_sy = mpoj2[0]; mpo_pl = mpoj2[1]; mpo_mi = mpoj2[2];
		mpo_sy.setLabel(lab_msy); mpo_pl.setLabel(lab_mpm); mpo_mi.setLabel(lab_mpm);
		ibn_pl = mpo_pl.inBondNum(); ibn_mi = mpo_mi.inBondNum();
		bfjt2_sy[0] = uni10::contract(mpo_sy, ten_sy, true);
		bfjt2_sy[0] += uni10::contract(mpo_pl, ten_mi, true);
		bfjt2_sy[0] += uni10::contract(mpo_mi, ten_pl, true);
		bfjt2_sy[3] = bfjt2_sy[0];
		bfjt2_pl[3] = uni10::contract(mpo_pl, ten_sy, true);
		bfjt2_mi[3] = uni10::contract(mpo_mi, ten_sy, true);
		mpo_pl.permute(ibn_pl); mpo_mi.permute(ibn_mi);
		applyCrossGate(mpo_pl, -20, to_cross);
		applyCrossGate(mpo_mi, -20, to_cross);
		bfjt2_pl[0] = uni10::contract(mpo_pl, ten_sy, true);
		bfjt2_mi[0] = uni10::contract(mpo_mi, ten_sy, true);
		bfjt2_sy[0].permute(lab_tsy, 3); bfjt2_pl[0].permute(lab_tpm, 3); bfjt2_mi[0].permute(lab_tpm, 3);
		bfjt2_sy[3].permute(lab_tsy, 3); bfjt2_pl[3].permute(lab_tpm, 3); bfjt2_mi[3].permute(lab_tpm, 3);

		// network B
		mpo_sy = mpoj2[3]; mpo_pl = mpoj2[4]; mpo_mi = mpoj2[5];
		mpo_sy.setLabel(lab_msy); mpo_pl.setLabel(lab_mpm); mpo_mi.setLabel(lab_mpm);
		ibn_pl = mpo_pl.inBondNum(); ibn_mi = mpo_mi.inBondNum();
		bfjt2_sy[1] = uni10::contract(mpo_sy, ten_sy, true);
		bfjt2_sy[1] += uni10::contract(mpo_pl, ten_mi, true);
		bfjt2_sy[1] += uni10::contract(mpo_mi, ten_pl, true);
		bfjt2_sy[4] = bfjt2_sy[1];
		bfjt2_pl[4] = uni10::contract(mpo_pl, ten_sy, true);
		bfjt2_mi[4] = uni10::contract(mpo_mi, ten_sy, true);
		mpo_pl.permute(ibn_pl); mpo_mi.permute(ibn_mi);
		applyCrossGate(mpo_pl, -20, to_cross);
		applyCrossGate(mpo_mi, -20, to_cross);
		bfjt2_pl[1] = uni10::contract(mpo_pl, ten_sy, true);
		bfjt2_mi[1] = uni10::contract(mpo_mi, ten_sy, true);
		bfjt2_sy[1].permute(lab_tsy, 3); bfjt2_pl[1].permute(lab_tpm, 3); bfjt2_mi[1].permute(lab_tpm, 3);
		bfjt2_sy[4].permute(lab_tsy, 3); bfjt2_pl[4].permute(lab_tpm, 3); bfjt2_mi[4].permute(lab_tpm, 3);

		// network C
		mpo_sy = mpoj2[6]; mpo_pl = mpoj2[7]; mpo_mi = mpoj2[8];
		mpo_sy.setLabel(lab_msy); mpo_pl.setLabel(lab_mpm); mpo_mi.setLabel(lab_mpm);
		ibn_pl = mpo_pl.inBondNum(); ibn_mi = mpo_mi.inBondNum();
		bfjt2_sy[2] = uni10::contract(mpo_sy, ten_sy, true);
		bfjt2_sy[2] += uni10::contract(mpo_pl, ten_mi, true);
		bfjt2_sy[2] += uni10::contract(mpo_mi, ten_pl, true);
		bfjt2_sy[5] = bfjt2_sy[2];
		bfjt2_pl[5] = uni10::contract(mpo_pl, ten_sy, true);
		bfjt2_mi[5] = uni10::contract(mpo_mi, ten_sy, true);
		mpo_pl.permute(ibn_pl); mpo_mi.permute(ibn_mi);
		applyCrossGate(mpo_pl, -20, to_cross);
		applyCrossGate(mpo_mi, -20, to_cross);
		bfjt2_pl[2] = uni10::contract(mpo_pl, ten_sy, true);
		bfjt2_mi[2] = uni10::contract(mpo_mi, ten_sy, true);
		bfjt2_sy[2].permute(lab_tsy, 3); bfjt2_pl[2].permute(lab_tpm, 3); bfjt2_mi[2].permute(lab_tpm, 3);
		bfjt2_sy[5].permute(lab_tsy, 3); bfjt2_pl[5].permute(lab_tpm, 3); bfjt2_mi[5].permute(lab_tpm, 3);
	}
	else if (loc == "3") {
		lab_rsy = {30, -3, 3};
		lab_rpm = {30, -3, 3, -30};
		lab_msy = {13, 203, 23, 30, 103};
		lab_mpm = {13, 203, 23, 30, 103, -30};
		to_cross = {203, 23, 30, 103};
		lab_tsy = {13, 203, -3, 23, 103, 3};
		lab_tpm = {13, 203, -3, 23, 103, 3, -30};

		ten_sy = buff3_sy[1]; ten_pl = buff3_pl[1]; ten_mi = buff3_mi[1];
		ten_sy.setLabel(lab_rsy); ten_pl.setLabel(lab_rpm); ten_mi.setLabel(lab_rpm);

		// network A
		mpo_sy = mpoj3[0]; mpo_pl = mpoj3[1]; mpo_mi = mpoj3[2];
		mpo_sy.setLabel(lab_msy); mpo_pl.setLabel(lab_mpm); mpo_mi.setLabel(lab_mpm);
		ibn_pl = mpo_pl.inBondNum(); ibn_mi = mpo_mi.inBondNum();
		bfjt3_sy[0] = uni10::contract(mpo_sy, ten_sy, true);
		bfjt3_sy[0] += uni10::contract(mpo_pl, ten_mi, true);
		bfjt3_sy[0] += uni10::contract(mpo_mi, ten_pl, true);
		mpo_pl.permute(ibn_pl); mpo_mi.permute(ibn_mi);
		applyCrossGate(mpo_pl, -30, to_cross);
		applyCrossGate(mpo_mi, -30, to_cross);
		bfjt3_pl[0] = uni10::contract(mpo_pl, ten_sy, true);
		bfjt3_mi[0] = uni10::contract(mpo_mi, ten_sy, true);
		bfjt3_sy[0].permute(lab_tsy, 3); bfjt3_pl[0].permute(lab_tpm, 3); bfjt3_mi[0].permute(lab_tpm, 3);

		// network B
		mpo_sy = mpoj3[3]; mpo_pl = mpoj3[4]; mpo_mi = mpoj3[5];
		mpo_sy.setLabel(lab_msy); mpo_pl.setLabel(lab_mpm); mpo_mi.setLabel(lab_mpm);
		ibn_pl = mpo_pl.inBondNum(); ibn_mi = mpo_mi.inBondNum();
		bfjt3_sy[1] = uni10::contract(mpo_sy, ten_sy, true);
		bfjt3_sy[1] += uni10::contract(mpo_pl, ten_mi, true);
		bfjt3_sy[1] += uni10::contract(mpo_mi, ten_pl, true);
		mpo_pl.permute(ibn_pl); mpo_mi.permute(ibn_mi);
		applyCrossGate(mpo_pl, -30, to_cross);
		applyCrossGate(mpo_mi, -30, to_cross);
		bfjt3_pl[1] = uni10::contract(mpo_pl, ten_sy, true);
		bfjt3_mi[1] = uni10::contract(mpo_mi, ten_sy, true);
		bfjt3_sy[1].permute(lab_tsy, 3); bfjt3_pl[1].permute(lab_tpm, 3); bfjt3_mi[1].permute(lab_tpm, 3);

		// network C
		mpo_sy = mpoj3[6]; mpo_pl = mpoj3[7]; mpo_mi = mpoj3[8];
		mpo_sy.setLabel(lab_msy); mpo_pl.setLabel(lab_mpm); mpo_mi.setLabel(lab_mpm);
		ibn_pl = mpo_pl.inBondNum(); ibn_mi = mpo_mi.inBondNum();
		bfjt3_sy[2] = uni10::contract(mpo_sy, ten_sy, true);
		bfjt3_sy[2] += uni10::contract(mpo_pl, ten_mi, true);
		bfjt3_sy[2] += uni10::contract(mpo_mi, ten_pl, true);
		mpo_pl.permute(ibn_pl); mpo_mi.permute(ibn_mi);
		applyCrossGate(mpo_pl, -30, to_cross);
		applyCrossGate(mpo_mi, -30, to_cross);
		bfjt3_pl[2] = uni10::contract(mpo_pl, ten_sy, true);
		bfjt3_mi[2] = uni10::contract(mpo_mi, ten_sy, true);
		bfjt3_sy[2].permute(lab_tsy, 3); bfjt3_pl[2].permute(lab_tpm, 3); bfjt3_mi[2].permute(lab_tpm, 3);
	}
}

//======================================

void YJuncQnIBC::updateDMRGBufferWJT( std::string loc ) {
	///
	// assert lat_size > =4
	if (bfwjt1_sy.size() < 3) {
		bfwjt1_sy = std::vector<uni10::CUniTensor>(3);
		bfwjt1_pl = std::vector<uni10::CUniTensor>(3);
		bfwjt1_mi = std::vector<uni10::CUniTensor>(3);
		bfwjt2_sy = std::vector<uni10::CUniTensor>(6);
		bfwjt2_pl = std::vector<uni10::CUniTensor>(6);
		bfwjt2_mi = std::vector<uni10::CUniTensor>(6);
		bfwjt3_sy = std::vector<uni10::CUniTensor>(3);
		bfwjt3_pl = std::vector<uni10::CUniTensor>(3);
		bfwjt3_mi = std::vector<uni10::CUniTensor>(3);
	}

	std::vector<int> lab_lmsy, lab_lmpm;
	std::vector<int> lab_mrsy, lab_mrpm;
	std::vector<int> lab_jtsy, lab_jtpm;
	std::vector<int> lab_bfsy, lab_bfpm;
	std::vector<int> to_cross;
	int ibn_1, ibn_2;

	if (loc == "WJ1") {
		std::vector<uni10::CUniTensor> mpo_l, mpo_m, mpo_j, mpo_lm;
		mpo_l = { buff1_sy[lat_size-3], buff1_pl[lat_size-3], buff1_mi[lat_size-3] };
		mpo_m = { mpo1_sy[lat_size-2], mpo1_pl[lat_size-2], mpo1_mi[lat_size-2] };

		lab_lmsy = {-1, 200, 10, 1, 100};
		lab_lmpm = {-1, 200, 10, 1, 100, -10};
		lab_jtsy = {10, 201, 12, 13, 101};
		lab_jtpm = {10, 201, 12, 13, 101, -10};
		to_cross = {201, 12, 13, 101};
		lab_bfsy = {-1, 200, 201, 12, 13, 1, 100, 101};
		lab_bfpm = {-1, 200, 201, 12, 13, 1, 100, 101, -10};

		// mpoL * mpoM -> mpoLM
		mpoLMQn(mpo_lm, mpo_l, mpo_m);
		mpo_lm[0].setLabel(lab_lmsy); mpo_lm[1].setLabel(lab_lmpm); mpo_lm[2].setLabel(lab_lmpm);

		for (int i = 0; i < 3; ++i) {
			// 0 for network A, 1 for B, 2 for C
			mpo_j = { mpoj1[i*3+0], mpoj1[i*3+1], mpoj1[i*3+2] };
			mpo_j[0].setLabel(lab_jtsy); mpo_j[1].setLabel(lab_jtpm); mpo_j[2].setLabel(lab_jtpm);
			ibn_1 = mpo_j[1].inBondNum(); ibn_2 = mpo_j[2].inBondNum();
			bfwjt1_pl[i] = uni10::contract(mpo_lm[0], mpo_j[1], true);
			bfwjt1_mi[i] = uni10::contract(mpo_lm[0], mpo_j[2], true);
			mpo_j[1].permute(ibn_1); mpo_j[2].permute(ibn_2);
			applyCrossGate(mpo_j[1], -10, to_cross);
			applyCrossGate(mpo_j[2], -10, to_cross);
			bfwjt1_sy[i] = uni10::contract(mpo_lm[0], mpo_j[0], true);
			bfwjt1_sy[i] += uni10::contract(mpo_lm[1], mpo_j[2], true);
			bfwjt1_sy[i] += uni10::contract(mpo_lm[2], mpo_j[1], true);
			bfwjt1_sy[i].permute(lab_bfsy, 3); bfwjt1_pl[i].permute(lab_bfpm, 3); bfwjt1_mi[i].permute(lab_bfpm, 3);
		}
	}
	else if (loc == "WJ2") {
		std::vector<uni10::CUniTensor> mpo_j, mpo_m, mpo_r, mpo_mr;
		mpo_m = { mpo2_sy[1], mpo2_pl[1], mpo2_mi[1] };
		mpo_r = { buff2_sy[2], buff2_pl[2], buff2_mi[2] };

		lab_mrsy = {20, 204, -2, 104, 2};
		lab_mrpm = {20, 204, -2, 104, 2, -20};
		lab_jtsy = {12, 202, 23, 20, 102};
		lab_jtpm = {12, 202, 23, 20, 102, -20};
		to_cross = {202, 23, 20, 102};
		lab_bfsy = {12, 202, 204, -2, 23, 102, 104, 2};
		lab_bfpm = {12, 202, 204, -2, 23, 102, 104, 2, -20};

		// mpoM * mpoR -> mpoMR
		mpoMRQn(mpo_mr, mpo_m, mpo_r, false);
		mpo_mr[0].setLabel(lab_mrsy); mpo_mr[1].setLabel(lab_mrpm); mpo_mr[2].setLabel(lab_mrpm);

		for (int i = 0; i < 3; ++i) {
			// 0 for network A, 1 for B, 2 for C
			mpo_j = { mpoj2[i*3+0], mpoj2[i*3+1], mpoj2[i*3+2] };
			mpo_j[0].setLabel(lab_jtsy); mpo_j[1].setLabel(lab_jtpm); mpo_j[2].setLabel(lab_jtpm);
			ibn_1 = mpo_j[1].inBondNum(); ibn_2 = mpo_j[2].inBondNum();
			bfwjt2_sy[i] = uni10::contract(mpo_j[0], mpo_mr[0], true);
			bfwjt2_sy[i] += uni10::contract(mpo_j[1], mpo_mr[2], true);
			bfwjt2_sy[i] += uni10::contract(mpo_j[2], mpo_mr[1], true);
			bfwjt2_sy[i+3] = bfwjt2_sy[i];
			bfwjt2_pl[i+3] = uni10::contract(mpo_j[1], mpo_mr[0], true);
			bfwjt2_mi[i+3] = uni10::contract(mpo_j[2], mpo_mr[0], true);
			mpo_j[1].permute(ibn_1); mpo_j[2].permute(ibn_2);
			applyCrossGate(mpo_j[1], -20, to_cross);
			applyCrossGate(mpo_j[2], -20, to_cross);
			bfwjt2_pl[i] = uni10::contract(mpo_j[1], mpo_mr[0], true);
			bfwjt2_mi[i] = uni10::contract(mpo_j[2], mpo_mr[0], true);
			bfwjt2_sy[i].permute(lab_bfsy, 4); bfwjt2_pl[i].permute(lab_bfpm, 4); bfwjt2_mi[i].permute(lab_bfpm, 4);
			bfwjt2_sy[i+3].permute(lab_bfsy, 4); bfwjt2_pl[i+3].permute(lab_bfpm, 4); bfwjt2_mi[i+3].permute(lab_bfpm, 4);
		}
	}
	else if (loc == "WJ3") {
		std::vector<uni10::CUniTensor> mpo_j, mpo_m, mpo_r, mpo_mr;
		mpo_m = { mpo3_sy[1], mpo3_pl[1], mpo3_mi[1] };
		mpo_r = { buff3_sy[2], buff3_pl[2], buff3_mi[2] };

		lab_mrsy = {30, 205, -3, 105, 3};
		lab_mrpm = {30, 205, -3, 105, 3, -30};
		lab_jtsy = {13, 203, 23, 30, 103};
		lab_jtpm = {13, 203, 23, 30, 103, -30};
		to_cross = {203, 23, 30, 103};
		lab_bfsy = {13, 203, 205, -3, 23, 103, 105, 3};
		lab_bfpm = {13, 203, 205, -3, 23, 103, 105, 3, -30};

		// mpoM * mpoR -> mpoMR
		mpoMRQn(mpo_mr, mpo_m, mpo_r, false);
		mpo_mr[0].setLabel(lab_mrsy); mpo_mr[1].setLabel(lab_mrpm); mpo_mr[2].setLabel(lab_mrpm);

		for (int i = 0; i < 3; ++i) {
			// 0 for network A, 1 for B, 2 for C
			mpo_j = { mpoj3[i*3+0], mpoj3[i*3+1], mpoj3[i*3+2] };
			mpo_j[0].setLabel(lab_jtsy); mpo_j[1].setLabel(lab_jtpm); mpo_j[2].setLabel(lab_jtpm);
			ibn_1 = mpo_j[1].inBondNum(); ibn_2 = mpo_j[2].inBondNum();
			bfwjt3_sy[i] = uni10::contract(mpo_j[0], mpo_mr[0], true);
			bfwjt3_sy[i] += uni10::contract(mpo_j[1], mpo_mr[2], true);
			bfwjt3_sy[i] += uni10::contract(mpo_j[2], mpo_mr[1], true);
			mpo_j[1].permute(ibn_1); mpo_j[2].permute(ibn_2);
			applyCrossGate(mpo_j[1], -30, to_cross);
			applyCrossGate(mpo_j[2], -30, to_cross);
			bfwjt3_pl[i] = uni10::contract(mpo_j[1], mpo_mr[0], true);
			bfwjt3_mi[i] = uni10::contract(mpo_j[2], mpo_mr[0], true);
			bfwjt3_sy[i].permute(lab_bfsy, 4); bfwjt3_pl[i].permute(lab_bfpm, 4); bfwjt3_mi[i].permute(lab_bfpm, 4);
		}
	}
}

//======================================

uni10::CUniTensor YJuncQnIBC::netMatVecJT( std::string loc,
	std::vector<int>& cfg, uni10::CUniTensor ket ) {
	///
	uni10::CUniTensor vec;
	std::vector<int> lab_ket = {1, 101, 102, 103, 2, 3};
	std::vector<int> lab_vec = {-1, 201, 202, 203, -2, -3};
	std::vector<std::vector<int>> labs;
	int ibn = ket.inBondNum();

	bool JJ12, JJ23, JJ13, WJ1, WJ2, WJ3;
	linksMPOHJ(cfg, JJ12, JJ23, JJ13, WJ1, WJ2, WJ3);
	std::vector<uni10::CUniTensor> tens;
	tensMPOHJT("JT", cfg, tens, JJ23);

	// set labels
	labsMPOHJT("JT", labs, JJ12, JJ23, JJ13, WJ1, WJ2, WJ3);
	ket.setLabel(lab_ket);
	tens[0].setLabel(labs[0]); tens[1].setLabel(labs[1]); tens[2].setLabel(labs[2]);

	// contraction
	if (JJ23) {
		vec = uni10::contract(ket, tens[2], true);
		// apply cross gate between 23
		applyCrossGate(vec, -23, 2);
		vec = uni10::contract(vec, tens[1], true);
		vec = uni10::contract(vec, tens[0], true);
	}
	else if (JJ13) {
		vec = uni10::contract(ket, tens[2], true);
		// apply cross gate between 13
		applyCrossGate(vec, -13, 2);
		applyCrossGate(vec, -13, 102);
		vec = uni10::contract(vec, tens[1], true);
		vec = uni10::contract(vec, tens[0], true);
	}
	else {
		vec = uni10::contract(ket, tens[2], true);
		vec = uni10::contract(vec, tens[1], true);
		vec = uni10::contract(vec, tens[0], true);
	}

	// permute vec
	vec.permute(lab_vec, ibn);

	tens.clear();
	return vec;
}

//======================================

uni10::CUniTensor YJuncQnIBC::netMatVecWJT( std::string loc,
	std::vector<int>& cfg, uni10::CUniTensor ket ) {
	///
	uni10::CUniTensor vec;
	std::vector<std::vector<int>> labs;
	int ibn = ket.inBondNum();

	bool JJ12, JJ23, JJ13, WJ1, WJ2, WJ3;
	linksMPOHJ(cfg, JJ12, JJ23, JJ13, WJ1, WJ2, WJ3);
	std::vector<uni10::CUniTensor> tens;
	tensMPOHWJT(loc, cfg, tens, JJ23);

	// set labels
	labsMPOHWJT(loc, labs, JJ12, JJ23, JJ13);
	tens[0].setLabel(labs[0]); tens[1].setLabel(labs[1]); tens[2].setLabel(labs[2]);
	ket.setLabel(labs[3]);

	// contraction
	if (JJ23) {
		vec = uni10::contract(ket, tens[2], true);
		// apply cross gate between 23
		applyCrossGate(vec, -23, 2);
		if (loc == "WJ2") applyCrossGate(vec, -23, 104);
		vec = uni10::contract(vec, tens[1], true);
		vec = uni10::contract(vec, tens[0], true);
	}
	else if (JJ13) {
		vec = uni10::contract(ket, tens[2], true);
		// apply cross gate between 13
		applyCrossGate(vec, -13, 2);
		if (loc == "WJ2") applyCrossGate(vec, -13, 104);
		applyCrossGate(vec, -13, 102);
		vec = uni10::contract(vec, tens[1], true);
		vec = uni10::contract(vec, tens[0], true);
	}
	else {
		vec = uni10::contract(ket, tens[2], true);
		vec = uni10::contract(vec, tens[1], true);
		vec = uni10::contract(vec, tens[0], true);
	}

	// permute vec
	vec.permute(labs[4], ibn);

	tens.clear();
	return vec;
}

//======================================

uni10::CUniTensor YJuncQnIBC::mpoMatVecJT( std::string loc, uni10::CUniTensor& psi ) {
	///
	uni10::CUniTensor vec;
	if (buff1_sy.size() == 0 || buff2_sy.size() == 0 || buff3_sy.size() == 0) {
		std::cerr << "In YJuncQnIBC::mpoMatVecJT : Buffer(s) not found." << '\n';
		return vec;
	}
	if (loc != "1" && loc != "2" && loc != "3") {
		std::cerr << "In YJuncQnIBC::mpoMatVecJT : Invalid update location." << '\n';
		return vec;
	}

	int ibn = psi.inBondNum();

	std::vector<std::vector<int>> cfgs;
	std::vector<std::vector<int>> given = {{3, 0}, {4, 0}, {5, 0}};
	findCfgs(cfgs, 0, adjs, given);

	vec = netMatVecJT(loc, cfgs[0], psi);
	for (int i = 1; i < cfgs.size(); ++i)
		vec += netMatVecJT(loc, cfgs[i], psi);

	cfgs.clear();
	return vec;
}

//======================================

uni10::CUniTensor YJuncQnIBC::mpoMatVecWJT( std::string loc, uni10::CUniTensor& psi ) {
	///
	uni10::CUniTensor vec;
	if (buff1_sy.size() == 0 || buff2_sy.size() == 0 || buff3_sy.size() == 0) {
		std::cerr << "In YJuncQnIBC::mpoMatVecWJT : Buffer(s) not found." << '\n';
		return vec;
	}
	if (loc != "WJ1" && loc != "WJ2" && loc != "WJ3") {
		std::cerr << "In YJuncQnIBC::mpoMatVecWJT : Invalid update location." << '\n';
		return vec;
	}

	int ibn = psi.inBondNum();

	std::vector<std::vector<int>> cfgs;
	std::vector<std::vector<int>> given = {{3, 0}, {4, 0}, {5, 0}};
	findCfgs(cfgs, 0, adjs, given);

	vec = netMatVecWJT(loc, cfgs[0], psi);
	for (int i = 1; i < cfgs.size(); ++i)
		vec += netMatVecWJT(loc, cfgs[i], psi);

	cfgs.clear();
	return vec;
}

//======================================

void YJuncQnIBC::dmrgJT( int wire_start, int idx_start,
	int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	int update_size = 1;
	int term = lat_size - update_size;
	uni10::CUniTensor psi;
	double eng = 0.0;

	int w1_start, w2_start, w3_start;	// starting point
	int w2_term = term;	// early term
	if (wire_start == 1) {
		w1_start = idx_start;
		w2_start = 1;
		w3_start = 1;
	}
	else if (wire_start == 2) {
		w1_start = term;
		w2_start = idx_start;
		w3_start = 1;
	}
	else if (wire_start == 3) {
		w1_start = term;
		w2_start = term+1;
		w3_start = idx_start;
		w2_term = 0;
	}

	// initialize buffers
	initDMRGBufferYJ( wire_start, idx_start );

	for (int s = 0; s < sweeps; ++s) {
		// wire1 ->
		for (int i = w1_start; i < term; ++i) {
			dmrgMPOHYJ( 1, i, update_size );
			psi = netLGL( wire1.lambda[i], wire1.gamma[i], wire1.lambda[i+1] );
			psi.permute( psi.bondNum() );
			int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );
			psi.permute(2);

			uni10::CUniTensor vi;
			mps1SiteSVD( psi, wire1.lambda[i], wire1.gamma[i], wire1.lambda[i+1], vi );
			if (i == lat_size-2) jt = netLJT( vi, jt );
			else wire1.gamma[i+1] = netLG( vi, wire1.gamma[i+1] );

			if (verbose) {
				std::cout << 1 << '\t' << i+1 << '\t' << lat_size-i-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire1.lambda[i+1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 1, i, true, update_size );
			if (i == lat_size-2) updateDMRGBufferWJ("1");
		}
		if (w1_start < term) {
			// JT -> wire2
			psi = netThetaTrngl();
			lanczosEighJT( "2", psi, eng, iter_max, tolerance*10 );  // tol*10? update on JT way more difficult
			JTSVD( "2", psi );
			if (verbose) {
				std::cout << 2 << '\t' << 1 << '\t' << lat_size-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire2.lambda[1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 2, 0, true, update_size );
		}
		// wire2 ->
		for (int i = w2_start; i <= term; ++i) {
			dmrgMPOHYJ( 2, i, update_size );
			psi = netLGL( wire2.lambda[i], wire2.gamma[i], wire2.lambda[i+1] );
			psi.permute( psi.bondNum() );
			int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );
			psi.permute(2);

			if (i == lat_size-1)
				wire2.gamma[i] = netLG( tenInvQn(wire2.lambda[i]), netGL( psi, tenInvQn(wire2.lambda[i+1]) ) );
			else {
				uni10::CUniTensor vi;
				mps1SiteSVD( psi, wire2.lambda[i], wire2.gamma[i], wire2.lambda[i+1], vi );
				wire2.gamma[i+1] = netLG( vi, wire2.gamma[i+1] );
			}

			if (verbose) {
				std::cout << 2 << '\t' << i+1 << '\t' << lat_size-i-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire2.lambda[i+1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 2, i, true, update_size );
		}
		// wire2 <-
		for (int i = w2_term; i > 0; --i) {
			dmrgMPOHYJ( 2, i, update_size );
			psi = netLGL( wire2.lambda[i], wire2.gamma[i], wire2.lambda[i+1] );
			psi.permute( psi.bondNum() );
			int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );
			psi.permute(1);

			uni10::CUniTensor ui;
			mps1SiteSVD( psi, wire2.lambda[i], wire2.gamma[i], wire2.lambda[i+1], ui );
			if (i == 1) jt = netJTL( jt, ui, "2" );
			else wire2.gamma[i-1] = netGL( wire2.gamma[i-1], ui );

			if (verbose) {
				std::cout << 2 << '\t' << i << '\t' << lat_size-i << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire2.lambda[i] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 2, i, false, update_size );
			if (i == 1) updateDMRGBufferWJ("2");
		}
		if (w2_term != 0) { // if dmrg not starts at wire 3
			// JT -> wire3
			psi = netThetaTrngl();
			lanczosEighJT( "3", psi, eng, iter_max, tolerance*10 );
			JTSVD( "3", psi );
			if (verbose) {
				std::cout << 3 << '\t' << 1 << '\t' << lat_size-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire3.lambda[1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 3, 0, true, update_size );
		}
		// wire3 ->
		for (int i = w3_start; i <= term; ++i) {
			dmrgMPOHYJ( 3, i, update_size );
			psi = netLGL( wire3.lambda[i], wire3.gamma[i], wire3.lambda[i+1] );
			psi.permute( psi.bondNum() );
			int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );
			psi.permute(2);

			if (i == lat_size-1)
				wire3.gamma[i] = netLG( tenInvQn(wire3.lambda[i]), netGL( psi, tenInvQn(wire3.lambda[i+1]) ) );
			else {
				uni10::CUniTensor vi;
				mps1SiteSVD( psi, wire3.lambda[i], wire3.gamma[i], wire3.lambda[i+1], vi );
				wire3.gamma[i+1] = netLG( vi, wire3.gamma[i+1] );
			}

			if (verbose) {
				std::cout << 3 << '\t' << i+1 << '\t' << lat_size-i-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire3.lambda[i+1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 3, i, true, update_size );
		}
		// wire3 <-
		for (int i = term; i > 0; --i) {
			dmrgMPOHYJ( 3, i, update_size );
			psi = netLGL( wire3.lambda[i], wire3.gamma[i], wire3.lambda[i+1] );
			psi.permute( psi.bondNum() );
			int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );
			psi.permute(1);

			uni10::CUniTensor ui;
			mps1SiteSVD( psi, wire3.lambda[i], wire3.gamma[i], wire3.lambda[i+1], ui );
			if (i == 1) jt = netJTL( jt, ui, "3" );
			else wire3.gamma[i-1] = netGL( wire3.gamma[i-1], ui );

			if (verbose) {
				std::cout << 3 << '\t' << i << '\t' << lat_size-i << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire3.lambda[i] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 3, i, false, update_size );
			if (i == 1) updateDMRGBufferWJ("3");
		}
		// wire1 <- JT
		psi = netThetaTrngl();
		lanczosEighJT( "1", psi, eng, iter_max, tolerance*10 );
		JTSVD( "1", psi );
		if (verbose) {
			std::cout << 1 << '\t' << lat_size-1 << '\t' << 1 << '\t';
			std::cout << std::setprecision(12) << entangleEntropyQn( wire1.lambda[lat_size-1] ) << '\t'
				<< eng << '\n';
		}
		updateDMRGBufferYJ( 1, lat_size-1, false, update_size );
		// wire1 <-
		for (int i = term-1; i >= 0; --i) {
			dmrgMPOHYJ( 1, i, update_size );
			psi = netLGL( wire1.lambda[i], wire1.gamma[i], wire1.lambda[i+1] );
			psi.permute( psi.bondNum() );
			int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );
			psi.permute(1);

			if (i == 0)
				wire1.gamma[i] = netGL( netLG( tenInvQn(wire1.lambda[i]), psi ), tenInvQn(wire1.lambda[i+1]) );
			else {
				uni10::CUniTensor ui;
				mps1SiteSVD( psi, wire1.lambda[i], wire1.gamma[i], wire1.lambda[i+1], ui );
				wire1.gamma[i-1] = netGL( wire1.gamma[i-1], ui );
			}

			if (verbose) {
				std::cout << 1 << '\t' << i << '\t' << lat_size-i << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire1.lambda[i] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 1, i, false, update_size );
		}

		if (s == 0) {
			w1_start = 0; w2_start = 1; w3_start = 1;	w2_term = term; // back to normal
		}
	}
}

//======================================

void YJuncQnIBC::dmrgJTU2( int wire_start, int idx_start,
	int sweeps, int iter_max, double tolerance, bool verbose, int chi_jt ) {
	///
	int update_size = 2;
	int term = lat_size - update_size;
	uni10::CUniTensor psi;
	double eng = 0.0;
	int chi_ori = chi_max;
	int chi_new = (chi_jt > 0)? chi_jt : chi_junc;

	int w1_start, w2_start, w3_start;	// starting point
	int w2_term = term;	// early term
	if (wire_start == 1) {
		w1_start = idx_start;
		w2_start = 1;
		w3_start = 1;
	}
	else if (wire_start == 2) {
		w1_start = term;
		w2_start = idx_start;
		w3_start = 1;
	}
	else if (wire_start == 3) {
		w1_start = term;
		w2_start = term+1;
		w3_start = idx_start;
		w2_term = 0;
	}

	// initialize buffers
	initDMRGBufferYJ( wire_start, idx_start );

	for (int s = 0; s < sweeps; ++s) {
		// wire1 ->
		for (int i = w1_start; i < term; ++i) {
			dmrgMPOHYJ( 1, i, update_size );
			psi = netLGLGL( wire1.lambda[i], wire1.gamma[i], wire1.lambda[i+1], wire1.gamma[i+1], wire1.lambda[i+2] );
			psi.permute( psi.bondNum() );
			int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

			if (i <= lat_size-3 && i > lat_size-23)
				chi_max = std::min((int)(chi_new * std::pow(chi_ramp, lat_size-3-i))/2*2, chi_ori);
			mps2SiteSVD( psi, wire1.lambda[i], wire1.gamma[i], wire1.lambda[i+1], wire1.gamma[i+1], wire1.lambda[i+2] );
			if (i <= lat_size-3 && i > lat_size-23) chi_max = chi_ori;

			if (verbose) {
				std::cout << 1 << '\t' << i+1 << '\t' << lat_size-i-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire1.lambda[i+1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 1, i, true, update_size );
		}
		if (w1_start < term) {
			chi_max = chi_new;
			// WJ1 ->
			updateDMRGBufferWJT("WJ1");
			psi = netThetaTrngl_WJT("WJ1");
			lanczosEighWJT( "WJ1", psi, eng, iter_max, tolerance*10 );
			WJTSVD( "WJ1", psi );
			if (verbose) {
				std::cout << 1 << '\t' << lat_size-1 << '\t' << 1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire1.lambda[lat_size-1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 1, lat_size-2, true, update_size );
			updateDMRGBufferWJ("1");
			if (chi_jt < 0) {
				// JT -> wire2
				psi = netThetaTrngl();
				lanczosEighJT( "2", psi, eng, iter_max, tolerance*10 );
				JTSVD( "2", psi );
				if (verbose) {
					std::cout << 2 << '\t' << 1 << '\t' << lat_size-1 << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( wire2.lambda[1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferYJ( 2, 0, true, update_size );
			}
			// WJ2 ->
			updateDMRGBufferWJT("WJ2");
			psi = netThetaTrngl_WJT("WJ2");
			lanczosEighWJT( "WJ2", psi, eng, iter_max, tolerance*10 );
			WJTSVD( "WJ2", psi );
			if (verbose) {
				std::cout << 2 << '\t' << 1 << '\t' << lat_size-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire2.lambda[1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 2, 0, true, update_size );
			chi_max = chi_ori;
		}
		// wire2 ->
		for (int i = w2_start; i <= term; ++i) {
			dmrgMPOHYJ( 2, i, update_size );
			psi = netLGLGL( wire2.lambda[i], wire2.gamma[i], wire2.lambda[i+1], wire2.gamma[i+1], wire2.lambda[i+2] );
			psi.permute( psi.bondNum() );
			int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

			if (i >= 1 && i < 21) chi_max = std::min((int)(chi_new * std::pow(chi_ramp, i-1))/2*2, chi_ori);
			mps2SiteSVD( psi, wire2.lambda[i], wire2.gamma[i], wire2.lambda[i+1], wire2.gamma[i+1], wire2.lambda[i+2] );
			if (i >= 1 && i < 21) chi_max = chi_ori;

			if (verbose) {
				std::cout << 2 << '\t' << i+1 << '\t' << lat_size-i-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire2.lambda[i+1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 2, i, true, update_size );
		}
		// wire2 <-
		for (int i = w2_term; i > 0; --i) {
			dmrgMPOHYJ( 2, i, update_size );
			psi = netLGLGL( wire2.lambda[i], wire2.gamma[i], wire2.lambda[i+1], wire2.gamma[i+1], wire2.lambda[i+2] );
			psi.permute( psi.bondNum() );
			int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

			if (i >= 1 && i < 21) chi_max = std::min((int)(chi_new * std::pow(chi_ramp, i-1))/2*2, chi_ori);
			mps2SiteSVD( psi, wire2.lambda[i], wire2.gamma[i], wire2.lambda[i+1], wire2.gamma[i+1], wire2.lambda[i+2] );
			if (i >= 1 && i < 21) chi_max = chi_ori;

			if (verbose) {
				std::cout << 2 << '\t' << i+1 << '\t' << lat_size-i-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire2.lambda[i+1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 2, i+1, false, update_size );
		}
		if (w2_term != 0) { // if dmrg not starts at wire 3
			chi_max = chi_new;
			// WJ2 <-
			updateDMRGBufferWJT("WJ2");
			psi = netThetaTrngl_WJT("WJ2");
			lanczosEighWJT( "WJ2", psi, eng, iter_max, tolerance*10 );
			WJTSVD( "WJ2", psi );
			if (verbose) {
				std::cout << 2 << '\t' << 1 << '\t' << lat_size-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire2.lambda[1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 2, 1, false, update_size );
			updateDMRGBufferWJ("2");
			if (chi_jt < 0) {
				// JT -> wire3
				psi = netThetaTrngl();
				lanczosEighJT( "3", psi, eng, iter_max, tolerance*10 );
				JTSVD( "3", psi );
				if (verbose) {
					std::cout << 3 << '\t' << 1 << '\t' << lat_size-1 << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( wire3.lambda[1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferYJ( 3, 0, true, update_size );
			}
			// WJ3 ->
			updateDMRGBufferWJT("WJ3");
			psi = netThetaTrngl_WJT("WJ3");
			lanczosEighWJT( "WJ3", psi, eng, iter_max, tolerance*10 );
			WJTSVD( "WJ3", psi );
			if (verbose) {
				std::cout << 3 << '\t' << 1 << '\t' << lat_size-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire3.lambda[1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 3, 0, true, update_size );
			chi_max = chi_ori;
		}
		// wire3 ->
		for (int i = w3_start; i <= term; ++i) {
			dmrgMPOHYJ( 3, i, update_size );
			psi = netLGLGL( wire3.lambda[i], wire3.gamma[i], wire3.lambda[i+1], wire3.gamma[i+1], wire3.lambda[i+2] );
			psi.permute( psi.bondNum() );
			int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

			if (i >= 1 && i < 21) chi_max = std::min((int)(chi_new * std::pow(chi_ramp, i-1))/2*2, chi_ori);
			mps2SiteSVD( psi, wire3.lambda[i], wire3.gamma[i], wire3.lambda[i+1], wire3.gamma[i+1], wire3.lambda[i+2] );
			if (i >= 1 && i < 21) chi_max = chi_ori;

			if (verbose) {
				std::cout << 3 << '\t' << i+1 << '\t' << lat_size-i-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire3.lambda[i+1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 3, i, true, update_size );
		}
		// wire3 <-
		for (int i = term; i > 0; --i) {
			dmrgMPOHYJ( 3, i, update_size );
			psi = netLGLGL( wire3.lambda[i], wire3.gamma[i], wire3.lambda[i+1], wire3.gamma[i+1], wire3.lambda[i+2] );
			psi.permute( psi.bondNum() );
			int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

			if (i >= 1 && i < 21) chi_max = std::min((int)(chi_new * std::pow(chi_ramp, i-1))/2*2, chi_ori);
			mps2SiteSVD( psi, wire3.lambda[i], wire3.gamma[i], wire3.lambda[i+1], wire3.gamma[i+1], wire3.lambda[i+2] );
			if (i >= 1 && i < 21) chi_max = chi_ori;

			if (verbose) {
				std::cout << 3 << '\t' << i+1 << '\t' << lat_size-i-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire3.lambda[i+1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 3, i+1, false, update_size );
		}
		chi_max = chi_new;
		// WJ3 <-
		updateDMRGBufferWJT("WJ3");
		psi = netThetaTrngl_WJT("WJ3");
		lanczosEighWJT( "WJ3", psi, eng, iter_max, tolerance*10 );
		WJTSVD( "WJ3", psi );
		if (verbose) {
			std::cout << 3 << '\t' << 1 << '\t' << lat_size-1 << '\t';
			std::cout << std::setprecision(12) << entangleEntropyQn( wire3.lambda[1] ) << '\t'
				<< eng << '\n';
		}
		updateDMRGBufferYJ( 3, 1, false, update_size );
		updateDMRGBufferWJ("3");
		if (chi_jt < 0) {
			// wire1 <- JT
			psi = netThetaTrngl();
			lanczosEighJT( "1", psi, eng, iter_max, tolerance*10 );
			JTSVD( "1", psi );
			if (verbose) {
				std::cout << 1 << '\t' << lat_size-1 << '\t' << 1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire1.lambda[lat_size-1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 1, lat_size-1, false, update_size );
		}
		// WJ1 <-
		updateDMRGBufferWJT("WJ1");
		psi = netThetaTrngl_WJT("WJ1");
		lanczosEighWJT( "WJ1", psi, eng, iter_max, tolerance*10 );
		WJTSVD( "WJ1", psi );
		if (verbose) {
			std::cout << 1 << '\t' << lat_size-1 << '\t' << 1 << '\t';
			std::cout << std::setprecision(12) << entangleEntropyQn( wire1.lambda[lat_size-1] ) << '\t'
				<< eng << '\n';
		}
		updateDMRGBufferYJ( 1, lat_size-1, false, update_size );
		chi_max = chi_ori;
		// wire1 <-
		for (int i = term-1; i >= 0; --i) {
			dmrgMPOHYJ( 1, i, update_size );
			psi = netLGLGL( wire1.lambda[i], wire1.gamma[i], wire1.lambda[i+1], wire1.gamma[i+1], wire1.lambda[i+2] );
			psi.permute( psi.bondNum() );
			int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

			if (i <= lat_size-3 && i > lat_size-23)
				chi_max = std::min((int)(chi_new * std::pow(chi_ramp, lat_size-3-i))/2*2, chi_ori);
			mps2SiteSVD( psi, wire1.lambda[i], wire1.gamma[i], wire1.lambda[i+1], wire1.gamma[i+1], wire1.lambda[i+2] );
			if (i <= lat_size-3 && i > lat_size-23) chi_max = chi_ori;

			if (verbose) {
				std::cout << 1 << '\t' << i+1 << '\t' << lat_size-i-1 << '\t';
				std::cout << std::setprecision(12) << entangleEntropyQn( wire1.lambda[i+1] ) << '\t'
					<< eng << '\n';
			}
			updateDMRGBufferYJ( 1, i+1, false, update_size );
		}

		if (s == 0) {
			w1_start = 0; w2_start = 1; w3_start = 1;	w2_term = term; // back to normal
		}
	}
}

//======================================
//======================================

uni10::CUniTensor YJuncQnIBC::testDMRGBufferYJ( int wire, int idx ) {
	///
	uni10::CUniTensor eng;
	uni10::CUniTensor lvec, rvec;
	uni10::CUniTensor klam, blam;
	std::vector<int> lab_buff_lsy = {-1, 10, 1};
	std::vector<int> lab_buff_rsy = {10, -2, 2};
	std::vector<int> lab_buff_lpm = {-1, 10, 1, -10};
	std::vector<int> lab_buff_rpm = {10, -2, 2, -10};
	std::vector<int> lab_klam = {1, 2};
	std::vector<int> lab_blam = {-2, -1};

	if (wire == 1) {
		klam = wire1.lambda[idx+1]; blam = wire1.lambda[idx+1];
		klam.setLabel(lab_klam); blam.setLabel(lab_blam);

		lvec = buff1_sy[idx]; rvec = buff1_sy[idx+1];
		lvec.setLabel(lab_buff_lsy); rvec.setLabel(lab_buff_rsy);
		lvec = lvec * klam; rvec = blam * rvec;
		eng = uni10::contract(lvec, rvec);
		lvec = buff1_pl[idx]; rvec = buff1_mi[idx+1];
		lvec.setLabel(lab_buff_lpm); rvec.setLabel(lab_buff_rpm);
		lvec = lvec * klam; rvec = blam * rvec;
		eng += uni10::contract(lvec, rvec);
		lvec = buff1_mi[idx]; rvec = buff1_pl[idx+1];
		lvec.setLabel(lab_buff_lpm); rvec.setLabel(lab_buff_rpm);
		lvec = lvec * klam; rvec = blam * rvec;
		eng += uni10::contract(lvec, rvec);
	}
	else if (wire == 2) {
		klam = wire2.lambda[idx+1]; blam = wire2.lambda[idx+1];
		klam.setLabel(lab_klam); blam.setLabel(lab_blam);

		lvec = buff2_sy[idx]; rvec = buff2_sy[idx+1];
		lvec.setLabel(lab_buff_lsy); rvec.setLabel(lab_buff_rsy);
		lvec = lvec * klam; rvec = blam * rvec;
		eng = uni10::contract(lvec, rvec);
		lvec = buff2_pl[idx]; rvec = buff2_mi[idx+1];
		lvec.setLabel(lab_buff_lpm); rvec.setLabel(lab_buff_rpm);
		lvec = lvec * klam; rvec = blam * rvec;
		eng += uni10::contract(lvec, rvec);
		lvec = buff2_mi[idx]; rvec = buff2_pl[idx+1];
		lvec.setLabel(lab_buff_lpm); rvec.setLabel(lab_buff_rpm);
		lvec = lvec * klam; rvec = blam * rvec;
		eng += uni10::contract(lvec, rvec);
	}
	else if (wire == 3) {
		klam = wire3.lambda[idx+1]; blam = wire3.lambda[idx+1];
		klam.setLabel(lab_klam); blam.setLabel(lab_blam);

		lvec = buff3_sy[idx]; rvec = buff3_sy[idx+1];
		lvec.setLabel(lab_buff_lsy); rvec.setLabel(lab_buff_rsy);
		lvec = lvec * klam; rvec = blam * rvec;
		eng = uni10::contract(lvec, rvec);
		lvec = buff3_pl[idx]; rvec = buff3_mi[idx+1];
		lvec.setLabel(lab_buff_lpm); rvec.setLabel(lab_buff_rpm);
		lvec = lvec * klam; rvec = blam * rvec;
		eng += uni10::contract(lvec, rvec);
		lvec = buff3_mi[idx]; rvec = buff3_pl[idx+1];
		lvec.setLabel(lab_buff_lpm); rvec.setLabel(lab_buff_rpm);
		lvec = lvec * klam; rvec = blam * rvec;
		eng += uni10::contract(lvec, rvec);
	}
	else
		std::cerr << "Wire index error!" << '\n';

	return eng;
}
