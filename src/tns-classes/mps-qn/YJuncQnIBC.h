#ifndef YJUNCQNIBC_H
#define YJUNCQNIBC_H
#include "ChainQnIBC.h"

class YJuncQnIBC: public CanonQnMPS {

public:
	/// constructor
	YJuncQnIBC( int L, int X, std::vector<uni10::Qnum>& q_phys, std::vector<uni10::Qnum>& q_virt );
	YJuncQnIBC( int L, int X, std::vector<uni10::Qnum>& q_phys );
	void setBDJT(int X);
	void setBDRamp(double r);

	void init();
	void randomize();
	void expand(int add_end, std::string dirname, int unit_cell = 2);

	uni10::CUniTensor getGamma(int wire, int idx);
	uni10::CUniTensor getLambda(int wire, int idx);
	uni10::CUniTensor getJT();
	const std::vector<uni10::CUniTensor>& getGamVec(int wire) const;
	const std::vector<uni10::CUniTensor>& getLamVec(int wire) const;
	std::vector< std::vector< std::vector<uni10::CUniTensor> > > allLRVecs();

	void importMPS( std::string dir_w1, std::string dir_w2, std::string dir_w3, int unit_cell = 2, bool sep_gj = true );
	void importMPS( std::string dirname, int unit_cell = 2, bool sep_gj = true );
	void importWireMPS( std::string dir_w1, std::string dir_w2, std::string dir_w3, int unit_cell = 2 );
	void importWireMPS( std::string dirname, int unit_cell = 2 );
	void import3Semi( std::string dirname, int unit_cell = 2 );
	void matchQnIBC( std::string dir_w1, std::string dir_w2, std::string dir_w3 );
	void exportMPS( std::string dirname );

	void loadNetYJ( std::string net_dir = "." );
	void loadNetIBC( std::string net_dir = "." );

	std::vector<uni10::CUniTensor> hosvdJT();

	void tebdImp( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
		uni10::CUniTensor hwr, uni10::CUniTensor hr, std::vector<uni10::CUniTensor>& himps,
		Complex dt, int steps, int orderTS = 1 );
	void tebdImp( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
		uni10::CUniTensor hwr, uni10::CUniTensor hr, uni10::CUniTensor himp,
		Complex dt, int steps, int orderTS = 1 );
	void tebd( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
		uni10::CUniTensor hwr, uni10::CUniTensor hr, Complex dt, int steps, int orderTS = 1 );

	void importMPOYJ( std::string mpo1_dir, std::string mpo2_dir, std::string mpo3_dir );
	void importMPOYJ( std::string mpo_dir );
	void perturbMPOYJ( int type, double param );
	void putImpMPOYJ( std::vector<uni10::CUniTensor>& mpo_imp, int wire, int idx );

	void dmrgJT( int wire_start, int idx_start,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgJTU2( int wire_start, int idx_start,
		int sweeps, int iter_max, double tolerance, bool verbose = false, int chi_jt = -1 );

	uni10::CUniTensor netLJT( uni10::CUniTensor lam, uni10::CUniTensor jt );
	uni10::CUniTensor netJTL( uni10::CUniTensor jt, uni10::CUniTensor lam, std::string loc = "2" );
	uni10::CUniTensor netThetaTrngl();
	uni10::CUniTensor netThetaTrngl( std::string loc, uni10::CUniTensor op );
	uni10::CUniTensor netThetaTrngl_red( std::string loc );
	uni10::CUniTensor netThetaTrngl_WJT( std::string loc );
	uni10::CUniTensor netThetaTrngl_WJT( std::string loc, uni10::CUniTensor op );
	uni10::CUniTensor netThetaTrngl_WJTW( std::string loc );
	uni10::CUniTensor netThetaTrngl_WJTW(
		std::string loc, uni10::CUniTensor op1, uni10::CUniTensor op2 );

	uni10::CUniTensor expVal( uni10::CUniTensor op, int w, int idx );
	uni10::CUniTensor expVal( uni10::CUniTensor op, std::string loc );

	uni10::CUniTensor correlation(
		uni10::CUniTensor op1, uni10::CUniTensor op2, int w1, int idx1, int w2, int idx2 );
	uni10::CUniTensor correlation(
		uni10::CUniTensor op1, uni10::CUniTensor op2, std::string loc1, std::string loc2 );

	uni10::CUniTensor testDMRGBufferYJ( int wire, int idx );	// for test

private:
	bool fermi, u1;
	ChainQnIBC wire1, wire2, wire3;
	uni10::CUniTensor jt;
	int chi_junc;
	double chi_ramp = 1.25;

	uni10::CNetwork net_jt, net_th_trngl_jt, net_th_trngl_jt_1_red, net_th_trngl_jt_2_red, net_th_trngl_jt_3_red;
	uni10::CNetwork net_th_trngl_wjt_1, net_th_trngl_wjt_2, net_th_trngl_wjt_3;
	uni10::CNetwork net_th_trngl_wjt_1_op, net_th_trngl_wjt_2_op, net_th_trngl_wjt_3_op;
	uni10::CNetwork net_th_trngl_wjtw_12, net_th_trngl_wjtw_23, net_th_trngl_wjtw_13;
	uni10::CNetwork net_th_trngl_wjtw_12_op, net_th_trngl_wjtw_23_op, net_th_trngl_wjtw_13_op;

	bool net_yj_loaded = false;
	std::string net_yj_dir = ".";
	std::string net_ibc_dir = ".";

	std::vector<uni10::CUniTensor> mpoj1, mpoj2, mpoj3;
	std::vector<uni10::CUniTensor> mpo1_sy, mpo1_pl, mpo1_mi;
	std::vector<uni10::CUniTensor> mpo2_sy, mpo2_pl, mpo2_mi;
	std::vector<uni10::CUniTensor> mpo3_sy, mpo3_pl, mpo3_mi;
	std::vector<uni10::CUniTensor> mpoH_sy, mpoH_pl, mpoH_mi;	// local H_eff for Lanczos
	std::vector<uni10::CUniTensor> buff1_sy, buff1_pl, buff1_mi;
	std::vector<uni10::CUniTensor> buff2_sy, buff2_pl, buff2_mi;
	std::vector<uni10::CUniTensor> buff3_sy, buff3_pl, buff3_mi;
	std::vector<uni10::CUniTensor> bfjt1_sy, bfjt1_pl, bfjt1_mi;
	std::vector<uni10::CUniTensor> bfjt2_sy, bfjt2_pl, bfjt2_mi;
	std::vector<uni10::CUniTensor> bfjt3_sy, bfjt3_pl, bfjt3_mi;
	std::vector<uni10::CUniTensor> bfwjt1_sy, bfwjt1_pl, bfwjt1_mi;
	std::vector<uni10::CUniTensor> bfwjt2_sy, bfwjt2_pl, bfwjt2_mi;
	std::vector<uni10::CUniTensor> bfwjt3_sy, bfwjt3_pl, bfwjt3_mi;

	// adjacency list - {{adj of site-0}, {adj of site-1}, ...}
	std::vector<std::vector<int>> adjs = {{1, 2, 3}, {0, 2, 4}, {0, 1, 5}, {0}, {1}, {2}};
	std::vector<std::vector<int>> adjs_jt = {{1, 2}, {0, 2}, {0, 1}};

	void initJT();
	void buildJT();

	void JTUpdate( std::string loc, uni10::CUniTensor& theta, std::map<uni10::Qnum, std::vector<uni10::CMatrix>>& th_svd,
		uni10::CUniTensor& lamC, uni10::CUniTensor& jjjt, uni10::CUniTensor& lamJ1, uni10::CUniTensor& lamJ2,
		uni10::CUniTensor& gamN );
	void WJTUpdate( std::string loc, uni10::CUniTensor& theta, std::map<uni10::Qnum, std::vector<uni10::CMatrix>>& th_svd,
		uni10::CUniTensor& lamW, uni10::CUniTensor& gamW, uni10::CUniTensor& lamC,
		uni10::CUniTensor& jjjt, uni10::CUniTensor& lamJ1, uni10::CUniTensor& lamJ2 );
	void JTSVD( std::string loc, uni10::CUniTensor& theta, bool show_err = false );
	void WJTSVD( std::string loc, uni10::CUniTensor& theta, bool show_err = false, double tol = 1e-10 );

	uni10::CUniTensor netMatVecJT(
		std::string loc, std::vector<int>& cfg, uni10::CUniTensor ket );
	uni10::CUniTensor netMatVecWJT(
		std::string loc, std::vector<int>& cfg, uni10::CUniTensor ket );
	uni10::CUniTensor mpoMatVecJT( std::string loc, uni10::CUniTensor& psi );
	uni10::CUniTensor mpoMatVecWJT( std::string loc, uni10::CUniTensor& psi );

	bool lanczosEVJT( std::string loc, uni10::CUniTensor& psi,
		size_t& max_iter, double err_tol, double& eigVal, uni10::CUniTensor& eigVec );
	size_t lanczosEighJT( std::string loc, uni10::CUniTensor& psi,
		double& E0, size_t max_iter, double err_tol );
	bool lanczosEVWJT( std::string loc, uni10::CUniTensor& psi,
		size_t& max_iter, double err_tol, double& eigVal, uni10::CUniTensor& eigVec );
	size_t lanczosEighWJT( std::string loc, uni10::CUniTensor& psi,
		double& E0, size_t max_iter, double err_tol );

	void initMPOHYJ( int update_size = 1, int wire = 3 );
	void tensMPOHJT( std::string loc,
		std::vector<int>& cfg, std::vector<uni10::CUniTensor>& tens, bool& JJ23 );
	void tensMPOHWJT( std::string loc,
		std::vector<int>& cfg, std::vector<uni10::CUniTensor>& tens, bool& JJ23 );
	void linksMPOHJ( std::vector<int>& cfg,
		bool& JJ12, bool& JJ23, bool& JJ13, bool& WJ1, bool& WJ2, bool& WJ3 );
	void linksMPOHJT( std::vector<int>& cfg,
		bool& JJ12, bool& JJ23, bool& JJ13 );
	void labsMPOHJT( std::string loc, std::vector<std::vector<int>>& labs,
		bool JJ12, bool JJ23, bool JJ13, bool WJ1, bool WJ2, bool WJ3 );
	void labsMPOHWJT( std::string loc, std::vector<std::vector<int>>& labs,
		bool JJ12, bool JJ23, bool JJ13 );
	uni10::CUniTensor netRenormJT1( std::vector<int>& cfg, uni10::CUniTensor ket );
	uni10::CUniTensor netRenormJT2( std::vector<int>& cfg, uni10::CUniTensor ket );
	uni10::CUniTensor netRenormJT3( std::vector<int>& cfg, uni10::CUniTensor ket );
	void renormMPOJT1();
	void renormMPOJT2();
	void renormMPOJT3();

	void dmrgMPOHYJ( int wire, int idx, int update_size = 1 );
	void initDMRGBufferYJ( int wire, int idx );
	void updateDMRGBufferYJ( int wire, int idx,
		bool sweep_right = true, int update_size = 1, int inc_size = 1 );
	void updateDMRGBufferWJ( std::string loc );
	void updateDMRGBufferWJT( std::string loc );

};

#endif
