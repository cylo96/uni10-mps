#ifndef FUNC_QN_MEAS_H
#define FUNC_QN_MEAS_H
#include <uni10.hpp>

struct SchmidtVal {
	double val;
	uni10::Qnum qn;

	SchmidtVal( double sv, uni10::Qnum q = uni10::Qnum(0) ) {
		val = sv;
		qn = q;
	}
};

double entangleEntropyQn( const uni10::CUniTensor& lam );
std::vector< SchmidtVal > entangleSpecQn( const uni10::CUniTensor& lam, int N );

#endif
