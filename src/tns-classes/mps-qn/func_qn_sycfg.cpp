#include <vector>
#include <sstream>
#include <algorithm>	// std::find
#include <mps-qn/func_qn_sycfg.h>

//=============================================

bool verify(std::vector<int>& ten_qns, std::vector<std::vector<int>>& adjs) {
	///
	int sum_qns = 0;
	for(std::vector<int>::iterator it = ten_qns.begin(); it != ten_qns.end(); ++it)
		sum_qns += *it;

	bool accept = (sum_qns == 0);
	for (int i = 0; i < ten_qns.size(); ++i) {
		bool status = (ten_qns[i] == 0);
		for (int j = 0; j < adjs[i].size(); ++j)
			status += (ten_qns[i] + ten_qns[adjs[i][j]] == 0);
		accept *= status;
	}

	return accept;
}

//=============================================

void findCfgRecur(std::vector<std::vector<int>>& cfgs, int idx_site,
	std::vector<int>& ten_qns, std::vector<std::vector<int>>& adjs,
	std::vector<std::vector<int>>& given) {
	///
	if (idx_site >= adjs.size()) {
		if (verify(ten_qns, adjs))
			cfgs.push_back(ten_qns);
		return;
	}

	std::vector<int> qns = {0, 1, -1};
	for (int i = 0; i < given.size(); ++i) {
		if (given[i].size() > 0 && given[i][0] == idx_site)
			qns = {given[i][1]};
	}
	for (int i = 0; i < qns.size(); ++i) {
		ten_qns[idx_site] = qns[i];
		findCfgRecur(cfgs, idx_site+1, ten_qns, adjs, given);
	}
}

//=============================================

void findCfgs(std::vector<std::vector<int>>& cfgs, int idx_site,
	std::vector<std::vector<int>>& adjs, std::vector<std::vector<int>>& given) {
	///
	std::vector<int> ten_qns(adjs.size());
	findCfgRecur(cfgs, idx_site, ten_qns, adjs, given);
}

//=============================================

void findCfgs(std::vector<std::vector<int>>& cfgs, int idx_site,
	std::vector<std::vector<int>>& adjs) {
	///
	std::vector<std::vector<int>> given;
	findCfgs(cfgs, idx_site, adjs, given);
}

//=============================================

void findLink(std::vector<std::string>& links,
	std::vector<int>& ten_qns, std::vector<std::vector<int>>& adjs,
	std::vector<int>& order) {
	///
	links.clear();
	int num_site = ten_qns.size();
	std::vector<std::vector<int>> cnts;
	std::vector<int> empt(num_site);
	for (int i = 0; i < num_site; ++i)
		cnts.push_back(empt);

	if (order.size() < num_site) {
		for (int i = 0; i < num_site; ++i)
			if ( std::find(order.begin(), order.end(), i) == order.end() )  // not found in order
				order.push_back(i);
	}

	for (int i = 0; i < order.size(); ++i) {
		if (ten_qns[order[i]] == 0) {
			for (int j = 0; j < num_site; ++j)
				cnts[i][j] = -1;
		}
		else {
			int possibles = 0;
			int found = -1;
			for (int j = 0; j < adjs[i].size(); ++j) {
				if (cnts[i][adjs[i][j]] == 0) {
					bool match = (ten_qns[i] + ten_qns[adjs[i][j]] == 0);
					if (possibles == 0 && match)
						found = adjs[i][j];
					possibles += match;
				}
			}
			if (possibles == 1) {
				for (int j = 0; j < num_site; ++j)
					cnts[i][j] = -1;
				cnts[i][found] = 1;
				for (int j = 0; j < num_site; ++j)
					cnts[found][j] = -1;
				cnts[found][i] = 1;
				if (i > found)
					links.push_back(std::to_string((long long) found) + std::to_string((long long) i));
				else
					links.push_back(std::to_string((long long) i) + std::to_string((long long) found));
			}
		}
	}
}

//=============================================

void findLink(std::vector<std::string>& links,
	std::vector<int>& ten_qns, std::vector<std::vector<int>>& adjs) {
	///
	std::vector<int> order;
	findLink(links, ten_qns, adjs, order);
}
