#ifndef FUNC_QN_BLKOPS_H
#define FUNC_QN_BLKOPS_H
#include <uni10.hpp>
#include <tns-func/tns_const.h>

uni10::CUniTensor real2ComplexQn( uni10::UniTensor& ten );
uni10::CUniTensor randTQn( uni10::CUniTensor& cten );
uni10::CUniTensor tenInvQn( uni10::CUniTensor& ten );
uni10::CUniTensor takeExpQn( Complex beta, uni10::CUniTensor& op );
std::map<uni10::Qnum, std::vector<uni10::CMatrix>> svdQn( uni10::CUniTensor& theta );
void truncateQn( std::map<uni10::Qnum, std::vector<uni10::CMatrix>>& svd_map,
	int chi, double tol = 0.0, const std::vector<uni10::Qnum>& qvec = std::vector<uni10::Qnum>() );
void resturctQn( uni10::CUniTensor& before, uni10::CUniTensor& after );
uni10::CUniTensor lamMix( uni10::CUniTensor lam, double rnd_scale = 0.1 );

#endif
