#ifndef CANONQNMPS_H
#define CANONQNMPS_H
#include <mps/CanonMPS.h>

class CanonQnMPS: public CanonMPS {

public:
	/// constructor
	CanonQnMPS(int L, int X, std::vector<uni10::Qnum>& q_phys, std::vector<uni10::Qnum>& q_virt );
	CanonQnMPS(int L, int X, std::vector<uni10::Qnum>& q_phys );
	CanonQnMPS(int L, int X );
	/// destructor
	~CanonQnMPS();

	void oneSiteOP( uni10::CUniTensor op, int idx = 0 );
	void resizeGamma( int idx, std::vector<uni10::Qnum>& qin_virt, std::vector<uni10::Qnum>& qout_virt );
	void resizeLambda( int idx, std::vector<uni10::Qnum>& q_virt );

protected:
	std::vector<uni10::Qnum> qphys;
	std::vector<uni10::Qnum> qvirt;
	uni10::CUniTensor initGamma( std::vector<uni10::Qnum>& qin_virt, std::vector<uni10::Qnum>& qin_phys,
		std::vector<uni10::Qnum>& qout_virt );
	uni10::CUniTensor initGamma( std::vector<uni10::Qnum>& qin_virt, std::vector<uni10::Qnum>& qin_phys );
	uni10::CUniTensor initLambda( std::vector<uni10::Qnum>& q_virt );

	void mps2SiteSVD( uni10::CUniTensor& psi,
		uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
		uni10::CUniTensor& lam1, uni10::CUniTensor& gam1, uni10::CUniTensor& lam2,
		bool show_err = false, const std::vector<uni10::Qnum>& qvec = std::vector<uni10::Qnum>() );

	void mps1SiteSVD( uni10::CUniTensor& theta,
		uni10::CUniTensor& lam0, uni10::CUniTensor& gam0, uni10::CUniTensor& lam1,
		uni10::CUniTensor& extra_uv, const std::vector<uni10::Qnum>& qvec = std::vector<uni10::Qnum>() );

	void evol2Site( uni10::CUniTensor& U,
		uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
		uni10::CUniTensor& lam1, uni10::CUniTensor& gam1, uni10::CUniTensor& lam2,
		bool show_err = false, const std::vector<uni10::Qnum>& qvec = std::vector<uni10::Qnum>() );

};

#endif
