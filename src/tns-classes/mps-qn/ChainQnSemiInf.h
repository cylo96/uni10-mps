#ifndef CHAINQNSEMIINF_H
#define CHAINQNSEMIINF_H
#include "CanonQnMPS.h"

/// ref: PHYSICAL REVIEW B 86, 245107 (2012)
class ChainQnSemiInf: public CanonQnMPS {

public:
	/// constructor
	ChainQnSemiInf( int L, int X, std::vector<uni10::Qnum>& q_phys, uni10::Qnum q_vin, std::vector<uni10::Qnum>& q_vout );
	ChainQnSemiInf( int L, int X, std::vector<uni10::Qnum>& q_phys, uni10::Qnum q_virt );
	ChainQnSemiInf( int L, int X, std::vector<uni10::Qnum>& q_phys );

	void reverse();
	void init();
	void randomize();
	void expand( int add_right, std::string dirname, int unit_cell = 2 );
	void importMPS( std::string dirname, int unit_cell = 2 );

	void tebd( uni10::CUniTensor hlw, uni10::CUniTensor hw,
		uni10::CUniTensor hwr, uni10::CUniTensor hr, Complex dt, int steps, int orderTS = 1 );

	void dmrg( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgU2( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose = false );

	uni10::CUniTensor expVal( uni10::CUniTensor op, int idx );
	uni10::CUniTensor correlation(
		uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 );

private:
	std::vector<uni10::Qnum> qvout;
	bool reverse_bdry = false;

};

#endif
