#include <iostream>
#include <cstdlib>
#include <cassert>
#include <stdexcept>
#include <time.h>

#include <mps-qn/ChainQnOBC.h>
#include <mps-qn/func_qn_la.h>
#include <mps-qn/func_qn_op.h>
#include <mps-qn/func_qn_net.h>
#include <mps-qn/func_qn_meas.h>
#include <mps-qn/func_qn_blkops.h>
#include <tns-func/func_net.h>

//======================================

ChainQnOBC::ChainQnOBC( int L, int X, std::vector<uni10::Qnum>& q_phys,
	uni10::Qnum q_vin, uni10::Qnum q_vout, uni10::Qnum qp_pl, uni10::Qnum qp_mi ) :
	CanonQnMPS( L, X, q_phys ) {
	/// object constructor
	// todo: assert L even
	uni10::Qnum q1 = qp_pl;
	uni10::Qnum q2 = qp_mi;

	qvirt.push_back( q1 * (-q2) * q_vin );
	qvirt.push_back( q_vin );
	qvirt.push_back( -q1 * q2 * q_vin );

	qvout.push_back( q1 * (-q2) * q_vout );
	qvout.push_back( q_vout );
	qvout.push_back( -q1 * q2 * q_vout );
	grd0 = false;
}

//======================================

ChainQnOBC::ChainQnOBC( int L, int X, std::vector<uni10::Qnum>& q_phys, uni10::Qnum q_vin, uni10::Qnum q_vout ) :
	CanonQnMPS( L, X, q_phys ) {
	/// object constructor
	// todo: assert L even
	uni10::Qnum q1 = q_phys[0];
	uni10::Qnum q2 = q_phys[q_phys.size()-1];

	qvirt.push_back( q1 * (-q2) * q_vin );
	qvirt.push_back( q_vin );
	qvirt.push_back( -q1 * q2 * q_vin );

	qvout.push_back( q1 * (-q2) * q_vout );
	qvout.push_back( q_vout );
	qvout.push_back( -q1 * q2 * q_vout );
	grd0 = false;
}

//======================================

ChainQnOBC::ChainQnOBC( int L, int X, std::vector<uni10::Qnum>& q_phys, uni10::Qnum q_vin ) :
	CanonQnMPS( L, X, q_phys ) {
	/// object constructor
	uni10::Qnum q_vout = q_vin;

	uni10::Qnum q1 = q_phys[0];
	uni10::Qnum q2 = q_phys[q_phys.size()-1];

	qvirt.push_back( q1 * (-q2) * q_vin );
	qvirt.push_back( q_vin );
	qvirt.push_back( -q1 * q2 * q_vin );

	qvout.push_back( q1 * (-q2) * q_vout );
	qvout.push_back( q_vout );
	qvout.push_back( -q1 * q2 * q_vout );
}

//======================================

ChainQnOBC::ChainQnOBC( int L, int X, std::vector<uni10::Qnum>& q_phys ) :
	CanonQnMPS( L, X, q_phys ) {
	/// object constructor
	// Why bond dim at boundary >= 3, not 1? Because of the stupid only-symm-block-survive design !!
	// To be specific, it is to handle the dummy quantum number bond of rasing/lowering operators
	// which contract with boundary gamma/lambda. Plus the dummy mpo and boundaries!
	uni10::Qnum q1 = q_phys[0];
	uni10::Qnum q2 = q_phys[q_phys.size()-1];

	qvirt.push_back( q1 * (-q2) );
	qvirt.push_back( uni10::Qnum(0) );
	qvirt.push_back( -q1 * q2 );

	qvout.push_back( q1 * (-q2) );
	qvout.push_back( uni10::Qnum(0) );
	qvout.push_back( -q1 * q2 );
}

//======================================

void ChainQnOBC::init() {
	///
	if (gamma.size() > 0)
		gamma.clear();
	if (lambda.size() > 0)
		lambda.clear();

	gamma.push_back( initGamma(qvirt, qphys) );
	lambda.push_back( initLambda(qvirt) );

	bool fermi = qphys[0].isFermionic();
	bool u1 = ((qphys[0].U1() != 0) || (qphys[qphys.size()-1].U1() != 0 ));

	if (fermi && u1 && grd0) {
		// assert lat_size >= 4
		// only works for half-filling
		std::vector<uni10::Qnum> qin1 = gamma[0].bond()[2].Qlist();
		gamma.push_back( initGamma(qin1, qphys) );
		lambda.push_back( initLambda(qin1) );
		std::vector<uni10::Qnum> qin2 = gamma[1].bond()[2].Qlist();
		gamma.push_back( initGamma(qin2, qphys) );
		lambda.push_back( initLambda(qin2) );
		std::vector<uni10::Qnum> qin3 = gamma[2].bond()[2].Qlist();

		for (int i = 3; i < lat_size-1; ++i) {
			if (i%4 == 1) {
				gamma.push_back( initGamma(qin1, qphys, qin2) );
				lambda.push_back( initLambda(qin1) );
			}
			else if (i%4 == 2) {
				gamma.push_back( initGamma(qin2, qphys, qin3) );
				lambda.push_back( initLambda(qin2) );
			}
			else if (i%4 == 3) {
				gamma.push_back( initGamma(qin3, qphys, qvirt) );
				lambda.push_back( initLambda(qin3) );
			}
			else {
				gamma.push_back( initGamma(qvirt, qphys, qin1) );
				lambda.push_back( initLambda(qvirt) );
			}
		}

		std::vector<uni10::Qnum> qinL = gamma[lat_size-2].bond()[2].Qlist();
		lambda.push_back( initLambda(qinL) );
		gamma.push_back( initGamma(qinL, qphys, qvout) );
		lambda.push_back( initLambda(qvout) );
	}
	else if (!fermi && grd0) {
		std::vector<uni10::Qnum> qin = gamma[0].bond()[2].Qlist();

		for (int i = 1; i < lat_size-1; ++i) {
			if (i%2) {
				gamma.push_back( initGamma(qin, qphys, qvirt) );
				lambda.push_back( initLambda(qin) );
			}
			else {
				gamma.push_back( initGamma(qvirt, qphys, qin) );
				lambda.push_back( initLambda(qvirt) );
			}
		}
		// assert lat_size even
		lambda.push_back( initLambda(qin) );
		gamma.push_back( initGamma(qin, qphys, qvout) );
		// lambda vec is one elem longer than gam
		lambda.push_back( initLambda(qvout) );
	}
	else {
		std::vector<uni10::Qnum> qin = gamma[0].bond()[2].Qlist();
		int qn_abs = std::abs(qvout[1].U1() - qvirt[1].U1());

		for (int i = 1; i < lat_size-1; ++i) {
			uni10::CUniTensor gtmp = initGamma(qin, qphys);
			std::map<uni10::Qnum, int> qmap = gtmp.bond()[2].degeneracy();
			std::vector<uni10::Qnum> qout;
			for (std::map<uni10::Qnum, int>::iterator it = qmap.begin(); it != qmap.end(); ++it) {
				uni10::Qnum q = it->first;
				qout.push_back(q);
			}
			for (std::vector<uni10::Qnum>::iterator it=qout.begin(); it!=qout.end();) {
			  if(std::abs((*it).U1() - qvirt[1].U1()) > qn_abs) {
			    it = qout.erase(it);
			  }
			  else
			    ++it;
			}

			gamma.push_back( initGamma(qin, qphys, qout) );
			lambda.push_back( initLambda(qin) );
			qin = gamma[i].bond()[2].Qlist();
		}
		// assert lat_size even
		lambda.push_back( initLambda(qin) );
		gamma.push_back( initGamma(qin, qphys, qvout) );
		// lambda vec is one elem longer than gam
		lambda.push_back( initLambda(qvout) );
	}
}

//======================================

void ChainQnOBC::init( std::vector<uni10::Qnum>& qv_vec ) {
	///
	assert(qv_vec.size() > lat_size);
	if (lambda.size() > 0)
		lambda.clear();
	if (gamma.size() > 0)
		gamma.clear();

	std::vector<uni10::Qnum> qin = {uni10::Qnum(0)};
	std::vector<uni10::Qnum> qout = {qv_vec[1]};
	lambda.push_back( initLambda(qvirt) );
	gamma.push_back( initGamma(qvirt, qphys, qout) );

	for (int i = 1; i < lat_size-1; ++i) {
		qin[0] = qv_vec[i];
		qout[0] = qv_vec[i+1];
		lambda.push_back( initLambda(qin) );
		gamma.push_back( initGamma(qin, qphys, qout) );
	}
	// assert lat_size even
	qin[0] = qv_vec[lat_size-1];
	lambda.push_back( initLambda(qin) );
	gamma.push_back( initGamma(qin, qphys, qvout) );
	// lambda vec is one elem longer than gam
	lambda.push_back( initLambda(qvout) );
}

//======================================

void ChainQnOBC::randomize() {
	/// randomize a complex MPS having only real part
	if (gamma.size() < lat_size || lambda.size() < lat_size+1)
		ChainQnOBC::init();

	std::srand( time(NULL) );

	for (int i = 0; i < lat_size; ++i)
		gamma[i] = randTQn( gamma[i] );
	for (int i = 0; i < lat_size+1; ++i)
		lambda[i] = randTQn( lambda[i] );

	uni10::CMatrix uni = lambda[0].getBlock( qvirt[1] );
	uni.set_zero();
	uni.at(0, 0) = Complex(1.0, 0.0);
	lambda[0].set_zero();
	lambda[0].putBlock( qvirt[1], uni );

	uni = lambda[lat_size].getBlock( qvout[1] );
	uni.set_zero();
	uni.at(0, 0) = Complex(1.0, 0.0);
	lambda[lat_size].set_zero();
	lambda[lat_size].putBlock( qvout[1], uni );
}

//======================================

void ChainQnOBC::importMPS( std::string dirname, int unit_cell, bool remake_bdry ) {
	///
	// fit_chi in CanonMPS doesn't work for Qn classes => set false
	CanonMPS::importMPS( dirname, unit_cell, true, false );

	if (remake_bdry) {
		SchmidtVal sv_l0 = entangleSpecQn( lambda[0], 1 )[0];
		SchmidtVal sv_ll = entangleSpecQn( lambda[lat_size], 1 )[0];

		uni10::CMatrix uni = lambda[0].getBlock( sv_l0.qn );
		uni.set_zero();
		uni.at(0, 0) = Complex(1.0, 0.0);
		lambda[0].set_zero();
		lambda[0].putBlock( sv_l0.qn, uni );

		uni = lambda[lat_size].getBlock( sv_ll.qn );
		uni.set_zero();
		uni.at(0, 0) = Complex(1.0, 0.0);
		lambda[lat_size].set_zero();
		lambda[lat_size].putBlock( sv_ll.qn, uni );
	}
}

//======================================

void ChainQnOBC::setFixQn( const std::vector<std::vector<uni10::Qnum>>& qvecs ) { qfix = qvecs; }

//======================================

uni10::CUniTensor ChainQnOBC::expVal( uni10::CUniTensor op, int idx ) {
	/// return expectation value (a contracted uni10) of an 1-site/2-site operator on site idx
	uni10::CUniTensor exp_val;

	uni10::CUniTensor lvec( lambda[0].bond() );
	uni10::CUniTensor rvec( lambda[lat_size].bond() );
	lvec.identity();
	rvec.identity();

	int bno = op.bondNum();
	if ( bno < 2 || bno > 4 ) {
		std::cerr << "In ChainQnOBC::expVal : Unsupported Operator.\n";
		return exp_val;
	}
	if ( bno%2 && op.bond()[bno-1].dim() == 1 ) {
		std::cerr << "In ChainQnOBC::expVal : Non-symmetric operator.\n";
		return exp_val;
	}

	uni10::CUniTensor onsite;
	if (bno == 2)
		onsite = netLGL( lambda[idx], gamma[idx], lambda[idx+1] );
	else if (bno == 4)
		onsite = netLGLGL( lambda[idx], gamma[idx], lambda[idx+1], gamma[idx+1], lambda[idx+2] );

	if (idx > 0)
		lvec = buildLRVecQn( idx-1, lvec, gamma, lambda, true );
	lvec = buildLRVecQn( lvec, onsite, op, true );
	if (idx < lat_size-(bno/2))
		rvec = buildLRVecQn( idx+(bno/2), rvec, gamma, lambda, false );
	exp_val = contrLRVecQn( lvec, rvec );

	return exp_val;
}

//======================================

uni10::CUniTensor ChainQnOBC::correlation(
	uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 ) {
	///
	// todo: assert idx2 - idx1 >= op.bondNum
	// todo: assert lat_size - idx2 >= op.bondNum
	uni10::CUniTensor corr;

	uni10::CUniTensor lvec( lambda[0].bond() );
	uni10::CUniTensor rvec( lambda[lat_size].bond() );
	lvec.identity();
	rvec.identity();

	int bno1 = op1.bondNum();
	int bno2 = op2.bondNum();
	if ( bno1 != bno2 ) {
		std::cerr << "In ChainQnOBC::correlation : Bonds of two operators do not match.\n";
		return corr;
	}
	if ( bno1 < 2 || bno1 > 4 ) {
		std::cerr << "In ChainQnOBC::correlation : Unsupported Operator.\n";
		return corr;
	}

	uni10::CUniTensor onsite1, onsite2;
	if (bno1 == 2 || bno1 == 3) {
		onsite1 = netLG( lambda[idx1], gamma[idx1] );
		onsite2 = netLGL( lambda[idx2], gamma[idx2], lambda[idx2+1] );
	}
	else if (bno1 == 4) {
		onsite1 = netLGLG( lambda[idx1], gamma[idx1], lambda[idx1+1], gamma[idx1+1] );
		onsite2 = netLGLGL( lambda[idx2], gamma[idx2], lambda[idx2+1], gamma[idx2+1], lambda[idx2+2] );
	}

	if (idx1 > 0)
		lvec = buildLRVecQn( idx1-1, lvec, gamma, lambda, true );

	lvec = buildLRVecQn( lvec, onsite1, op1, true );

	for (int i = idx1+bno1/2; i < idx2; ++i) {
		uni10::CUniTensor cell = netLG( lambda[i], gamma[i] );
		lvec = buildLRVecQn( lvec, cell, true );
	}

	lvec = buildLRVecQn( lvec, onsite2, op2, true );

	if (idx2 < lat_size-(bno2/2))
		rvec = buildLRVecQn( idx2+(bno2/2), rvec, gamma, lambda, false );

	corr = contrLRVecQn( lvec, rvec );

	return corr;
}

//======================================

void ChainQnOBC::tebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS,
	bool show_err, bool fix_qn ) {
	///
	std::vector<uni10::CUniTensor> hams(lat_size-1, ham);
	tebdImp(hams, dt, steps, orderTS, show_err, fix_qn);
}

//======================================

void ChainQnOBC::tebd( uni10::CUniTensor Uevol, int steps,
	bool show_err, bool fix_qn ) {
	///
	std::vector<uni10::CUniTensor> Uevols(lat_size-1, Uevol);
	tebdImp(Uevols, steps, show_err, fix_qn);
}

//======================================

void ChainQnOBC::tebdImp( std::vector<uni10::CUniTensor>& hams, Complex dt, int steps, int orderTS,
	bool show_err, bool fix_qn ) {
	///
	if (orderTS == 1) {
		std::vector<uni10::CUniTensor> U(lat_size-1, uni10::CUniTensor());
		for (int i = 0; i < lat_size-1; ++i)
			U[i] = takeExpQn(I * dt, hams[i]);

		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites/even indices
			for (int i = 0; i < lat_size-1; i+=2) {
				if (fix_qn)
					evol2Site(U[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err, qfix[i+1]);
				else
					evol2Site(U[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err);
			}
			// evolve and update even sites/odd indices
			for (int i = 1; i < lat_size-1; i+=2) {
				if (fix_qn)
					evol2Site(U[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err, qfix[i+1]);
				else
					evol2Site(U[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err);
			}
		}
	}
	else if (orderTS == 2) {
		std::vector<uni10::CUniTensor> U(lat_size-1, uni10::CUniTensor());
		std::vector<uni10::CUniTensor> U2(lat_size-1, uni10::CUniTensor());
		for (int i = 0; i < lat_size-1; ++i) {
			U[i] = takeExpQn(I * dt, hams[i]);
			U2[i] = takeExpQn(0.5 * I * dt, hams[i]);
		}

		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int i = 0; i < lat_size-1; i+=2) {
				if (fix_qn)
					evol2Site(U2[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err, qfix[i+1]);
				else
					evol2Site(U2[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err);
			}
			// evolve and update even sites
			for (int i = 1; i < lat_size-1; i+=2) {
				if (fix_qn)
					evol2Site(U[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err, qfix[i+1]);
				else
					evol2Site(U[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err);
			}
			// evolve and update odd sites
			for (int i = 0; i < lat_size-1; i+=2) {
				if (fix_qn)
					evol2Site(U2[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err, qfix[i+1]);
				else
					evol2Site(U2[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err);
			}
		}
	}
	else {
		throw std::invalid_argument("Unsupported Trotter-Suzuki order.");
	}
}

//======================================

void ChainQnOBC::tebdImp( std::vector<uni10::CUniTensor>& Uevols, int steps,
	bool show_err, bool fix_qn ) {
	///
	// only for orderTS == 1
	for (int ite = 0; ite < steps; ++ite) {
		// evolve and update odd sites/even indices
		for (int i = 0; i < lat_size-1; i+=2) {
			if (fix_qn)
				evol2Site(Uevols[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err, qfix[i+1]);
			else
				evol2Site(Uevols[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err);
		}
		// evolve and update even sites/odd indices
		for (int i = 1; i < lat_size-1; i+=2) {
			if (fix_qn)
				evol2Site(Uevols[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err, qfix[i+1]);
			else
				evol2Site(Uevols[i], lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], show_err);
		}
	}
}

//======================================

void ChainQnOBC::dmrg( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( lat_size, mpo_dir, mpo_sy, mpo_pl, mpo_mi, true, lambda[0], lambda[lat_size] );

	dmrgImp(mpo_sy, mpo_pl, mpo_mi, sweeps, iter_max, tolerance, verbose, false);
	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();
}

//======================================

void ChainQnOBC::dmrgU2( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( lat_size, mpo_dir, mpo_sy, mpo_pl, mpo_mi, true, lambda[0], lambda[lat_size] );

	dmrgImpU2(mpo_sy, mpo_pl, mpo_mi, sweeps, iter_max, tolerance, verbose, false);
	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();
}

//======================================

void ChainQnOBC::dmrgImp(
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	int sweeps, int iter_max, double tolerance, bool verbose, bool fix_qn ) {
	///
	int update_size = 1;
	uni10::CUniTensor psi;
	double eng = 0.0;

	std::vector<uni10::CUniTensor> mpoH_sy;
	std::vector<uni10::CUniTensor> mpoH_pl;
	std::vector<uni10::CUniTensor> mpoH_mi;
	std::vector<uni10::CUniTensor> buff_sy;
	std::vector<uni10::CUniTensor> buff_pl;
	std::vector<uni10::CUniTensor> buff_mi;

	for (int s = 0; s < sweeps; ++s) {
		//
		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 0; i <= lat_size-update_size; ++i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.bondNum() );
				int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );
				psi.permute(2);

				if (i == lat_size-1)
					gamma[i] = netLG( tenInvQn(lambda[i]), netGL( psi, tenInvQn(lambda[i+1]) ) );
				else {
					uni10::CUniTensor vi;
					if (fix_qn)
						mps1SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], vi, qfix[i+1] );
					else
						mps1SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], vi );
					gamma[i+1] = netLG( vi, gamma[i+1] );
				}

				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, true );
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size-update_size; i >= 0; --i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.bondNum() );
				int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );
				psi.permute(1);

				if (i == 0)
					gamma[i] = netGL( netLG( tenInvQn(lambda[i]), psi ), tenInvQn(lambda[i+1]) );
				else {
					uni10::CUniTensor ui;
					if (fix_qn)
						mps1SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], ui, qfix[i] );
					else
						mps1SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], ui );
					gamma[i-1] = netGL( gamma[i-1], ui );
				}

				if (verbose) {
					std::cout << i << '\t' << lat_size-i << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( lambda[i] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, false );
			}
		}
	}

	mpoH_sy.clear();
	mpoH_pl.clear();
	mpoH_mi.clear();
	buff_sy.clear();
	buff_pl.clear();
	buff_mi.clear();
}

//======================================

void ChainQnOBC::dmrgImpU2(
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	int sweeps, int iter_max, double tolerance, bool verbose, bool fix_qn ) {
	///
	int update_size = 2;
	uni10::CUniTensor psi;
	double eng = 0.0;

	std::vector<uni10::CUniTensor> mpoH_sy;
	std::vector<uni10::CUniTensor> mpoH_pl;
	std::vector<uni10::CUniTensor> mpoH_mi;
	std::vector<uni10::CUniTensor> buff_sy;
	std::vector<uni10::CUniTensor> buff_pl;
	std::vector<uni10::CUniTensor> buff_mi;

	for (int s = 0; s < sweeps; ++s) {
		//
		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 0; i <= lat_size-update_size; ++i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.bondNum() );
				int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

				if (fix_qn)
					mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], false, qfix[i+1] );
				else
					mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, true );
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size-update_size; i >= 0; --i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.bondNum() );
				int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

				if (fix_qn)
					mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2], false, qfix[i+1] );
				else
					mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, false );
			}
		}
	}

	mpoH_sy.clear();
	mpoH_pl.clear();
	mpoH_mi.clear();
	buff_sy.clear();
	buff_pl.clear();
	buff_mi.clear();
}

//======================================

void ChainQnOBC::rmRedundantBlk() {
	/// remove redundant, useless, all-zero, only-to-serve-the-stupid-only-symm-block-survive-design, blocks.
	int bd_dim = chi_max;
	chi_max = 1;

	uni10::CUniTensor theta = netLGLGL( lambda[lat_size-1], gamma[lat_size-1], lambda[0], gamma[0], lambda[1] );
	mps2SiteSVD( theta, lambda[lat_size-1], gamma[lat_size-1], lambda[0], gamma[0], lambda[1] );
	// side effect: gamma[0] becomes right canonical, gamma[lat_size-1] left canonical

	lambda[lat_size] = lambda[0];
	chi_max = bd_dim;
}

//======================================

ChainOBC ChainQnOBC::toNoQ() {
	///
	rmRedundantBlk();

	ChainOBC mpsNQ( lat_size, dim_phys, chi_max );

	for (int i = 0; i < lat_size; ++i) {
		std::vector<uni10::Bond> bdg = gamma[i].bond();
		std::vector<uni10::Bond> bdl = lambda[i].bond();

		uni10::CUniTensor gam = CanonMPS::initGamma( bdg[0].dim(), bdg[2].dim(), bdg[1].dim() );
		uni10::CUniTensor lam = CanonMPS::initLambda( bdl[0].dim() );

		gam.setRawElem( gamma[i].getRawElem() );
		lam.setRawElem( lambda[i].getRawElem() );

		mpsNQ.putGamma( gam );
		mpsNQ.putLambda( lam );
	}
	std::vector<uni10::Bond> bdl = lambda[lat_size].bond();
	uni10::CUniTensor lam = CanonMPS::initLambda( bdl[0].dim() );
	lam.setRawElem( lambda[lat_size].getRawElem() );
	mpsNQ.putLambda( lam );

	return mpsNQ;
}

//======================================
