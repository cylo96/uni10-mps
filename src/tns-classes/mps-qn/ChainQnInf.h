#ifndef CHAINQNINF_H
#define CHAINQNINF_H
#include <mps/ChainInf.h>
#include "CanonQnMPS.h"

class ChainQnInf: public CanonQnMPS {

public:
	/// constructor
	ChainQnInf( int L, int X, std::vector<uni10::Qnum>& q_phys, std::vector<uni10::Qnum>& q_virt );
	ChainQnInf( int L, int X, std::vector<uni10::Qnum>& q_phys );
	ChainQnInf( int X, std::vector<uni10::Qnum>& q_phys, std::vector<uni10::Qnum>& q_virt );
	ChainQnInf( int X, std::vector<uni10::Qnum>& q_phys );

	void init();
	void randomize();
	void setFixQn( const std::vector<std::vector<uni10::Qnum>>& qvecs );

	void itebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS = 1 );
	void itebdU2( std::vector<uni10::CUniTensor>& ham, Complex dt, int steps, int orderTS = 1, bool fix_qn = false );
	void itebdUC( uni10::CUniTensor ham, Complex dt, int steps, int orderTS = 1 );
	void idmrg( std::string mpo_dir, int steps, int iter_max, double tolerance,
		std::string sav_dir = "", bool offset_hb = false );
	void idmrgResume( std::string mpo_dir, int steps, int iter_max, double tolerance,
		std::string sav_dir, bool offset_hb = false );
	void idmrgU2(
		std::vector<std::vector<uni10::CUniTensor>>& mpo_lr,
		std::vector<std::vector<uni10::CUniTensor>>& mpo_m,
		int steps, int iter_max, double tolerance,
		std::string sav_dir = "", bool offset_hb = false, bool fix_qn = false );
	void idmrgU2Resume(
		std::vector<std::vector<uni10::CUniTensor>>& mpo_lr,
		std::vector<std::vector<uni10::CUniTensor>>& mpo_m,
		int steps, int iter_max, double tolerance,
		std::string sav_dir = "", bool offset_hb = false, bool fix_qn = false, bool first_swap = false );
	void idmrgUC( std::string mpo_dir, int steps, int iter_max, double tolerance,
		std::string sav_dir = "", bool offset_hb = false, int sweep_rt = 2 );
	void idmrgUCResume( std::string mpo_dir, int steps, int iter_max, double tolerance,
		std::string sav_dir, bool offset_hb = false, int sweep_rt = 2 );

	uni10::CUniTensor expVal( uni10::CUniTensor op, int loc = -1 );

	ChainInf toNoQ();

	friend class ChainQnIBC;

private:
	std::vector<std::vector<uni10::Qnum>> qfix;

};

#endif
