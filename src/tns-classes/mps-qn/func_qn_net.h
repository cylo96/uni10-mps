#ifndef FUNC_QN_NET_H
#define FUNC_QN_NET_H
#include <uni10.hpp>

void applyCrossGate( uni10::CUniTensor& ten, int to_permute, std::vector<int>& to_cross );
void applyCrossGate( uni10::CUniTensor& ten, int to_permute, int to_cross );
uni10::CUniTensor crossGate( uni10::CUniTensor ten, int to_permute, std::vector<int>& to_cross );
uni10::CUniTensor crossGate( uni10::CUniTensor ten, int to_permute, int to_cross );
uni10::CUniTensor dag( uni10::CUniTensor ten );
uni10::CUniTensor innerProdQn( uni10::CUniTensor bra, uni10::CUniTensor ket );
double vecNormQn( uni10::CUniTensor vec );
uni10::CUniTensor bondInvGamU1( uni10::CUniTensor gam );
uni10::CUniTensor expValQn( uni10::CUniTensor ket, uni10::CUniTensor op );

uni10::CUniTensor traceRhoVecQn( bool left,
	uni10::CUniTensor& lambda, uni10::CUniTensor& vec );

uni10::CUniTensor tranMatVecQn( bool left,
	uni10::CUniTensor& ket, uni10::CUniTensor& bra,
	uni10::CUniTensor& vec );

void mpoLMQn( std::vector<uni10::CUniTensor>& mpo_lm,
	std::vector<uni10::CUniTensor>& mpo_l,
	std::vector<uni10::CUniTensor>& mpo_m );

void mpoMRQn( std::vector<uni10::CUniTensor>& mpo_mr,
	std::vector<uni10::CUniTensor>& mpo_m,
	std::vector<uni10::CUniTensor>& mpo_r,
	bool r_cross = false );

uni10::CUniTensor mpoLMMRQn(
	std::vector<uni10::CUniTensor>& mpo_l,
	std::vector<uni10::CUniTensor>& mpo_m1,
	std::vector<uni10::CUniTensor>& mpo_m2,
	std::vector<uni10::CUniTensor>& mpo_r,
	bool r_cross = false );

uni10::CUniTensor mpoLRQn(
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi );

uni10::CUniTensor mpoLMMRQn(
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi );

uni10::CUniTensor op2SiteFromMPOQn(
	uni10::CUniTensor mpo_ms, uni10::CUniTensor mpo_mp, uni10::CUniTensor mpo_mm );
uni10::CUniTensor op2SiteFromMPOQn( std::vector<std::vector<uni10::CUniTensor>>& mpo_m );

uni10::CUniTensor mpoMatVecQn( int size,
	std::vector<uni10::CUniTensor> mpoH_sy,
	std::vector<uni10::CUniTensor> mpoH_pl,
	std::vector<uni10::CUniTensor> mpoH_mi,
	uni10::CUniTensor psi );

void renormMPOL(
	std::vector<uni10::CUniTensor>& mpoH_sy,
	std::vector<uni10::CUniTensor>& mpoH_pl,
	std::vector<uni10::CUniTensor>& mpoH_mi,
	uni10::CUniTensor ket, bool initial = false );

void renormMPOR(
	std::vector<uni10::CUniTensor>& mpoH_sy,
	std::vector<uni10::CUniTensor>& mpoH_pl,
	std::vector<uni10::CUniTensor>& mpoH_mi,
	uni10::CUniTensor ket, bool initial = false );

std::vector<uni10::CUniTensor> allLRVecsQn(
	const std::vector<uni10::CUniTensor>& gams,
	const std::vector<uni10::CUniTensor>& lams,
	bool left = true );

uni10::CUniTensor buildLRVecQn( int loc,
	const std::vector<uni10::CUniTensor>& gams,
	const std::vector<uni10::CUniTensor>& lams,
	bool left = true );

uni10::CUniTensor buildLRVecQn(
	uni10::CUniTensor& vec_last, uni10::CUniTensor ket,
	bool left = true );

uni10::CUniTensor buildLRVecQn(
	uni10::CUniTensor& vec_last, uni10::CUniTensor ket,
	uni10::CUniTensor& op, bool left = true );

uni10::CUniTensor buildLRVecQn(
	int loc, uni10::CUniTensor& vec_last,
	const std::vector<uni10::CUniTensor>& gams,
	const std::vector<uni10::CUniTensor>& lams,
	bool left = true );

uni10::CUniTensor contrLRVecQn(
	uni10::CUniTensor& lvec, uni10::CUniTensor& rvec );

void initDMRGBufferQn( int loc,
	std::vector<uni10::CUniTensor>& gamma,
	std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	std::vector<uni10::CUniTensor>& mpoH_sy,
	std::vector<uni10::CUniTensor>& mpoH_pl,
	std::vector<uni10::CUniTensor>& mpoH_mi,
	std::vector<uni10::CUniTensor>& buff_sy,
	std::vector<uni10::CUniTensor>& buff_pl,
	std::vector<uni10::CUniTensor>& buff_mi );

void updateDMRGBufferQn( int loc, int update_size,
	std::vector<uni10::CUniTensor>& gamma,
	std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpoH_sy,
	std::vector<uni10::CUniTensor>& mpoH_pl,
	std::vector<uni10::CUniTensor>& mpoH_mi,
	std::vector<uni10::CUniTensor>& buff_sy,
	std::vector<uni10::CUniTensor>& buff_pl,
	std::vector<uni10::CUniTensor>& buff_mi,
	bool sweep_right = true, int inc_size = 1 );

void dmrgMPOHQn( int loc, int update_size,
	std::vector<uni10::CUniTensor>& gamma,
	std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	std::vector<uni10::CUniTensor>& mpoH_sy,
	std::vector<uni10::CUniTensor>& mpoH_pl,
	std::vector<uni10::CUniTensor>& mpoH_mi,
	std::vector<uni10::CUniTensor>& buff_sy,
	std::vector<uni10::CUniTensor>& buff_pl,
	std::vector<uni10::CUniTensor>& buff_mi );

#endif
