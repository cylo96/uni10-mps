#include <map>
#include <algorithm>    // std::sort

#include <mps-qn/func_qn_meas.h>
//#include <tns-func/func_net.h>

//======================================

bool compare( SchmidtVal x, SchmidtVal y ) { return ( x.val > y.val ); }

//======================================

void sortSchmidtValsQn( const uni10::CUniTensor& lam, std::vector< SchmidtVal >& sch_vals ) {
	///
	std::map<uni10::Qnum, uni10::CMatrix> blks = lam.getBlocks();
	
	for (std::map<uni10::Qnum, uni10::CMatrix>::iterator it = blks.begin(); it != blks.end(); ++it) {
		uni10::Qnum q = it->first;
		uni10::CMatrix blk = it->second;
		for (int i = 0; i < blk.col(); ++i)
			sch_vals.push_back( SchmidtVal( blk.at(i, i).real(), q ) );
	}
	std::sort( sch_vals.begin(), sch_vals.end(), compare );
}

//======================================

double entangleEntropyQn( const uni10::CUniTensor& lam ) {
	///
	std::vector< SchmidtVal > sch_vals;
	sortSchmidtValsQn( lam, sch_vals );

	double S = 0.;

	for (int i = 0; i < sch_vals.size(); ++i)
		if ( sch_vals[i].val > 1e-16 )
			S -= ( sch_vals[i].val * sch_vals[i].val ) * log( sch_vals[i].val * sch_vals[i].val );

	return S;
}

//======================================

std::vector< SchmidtVal > entangleSpecQn( const uni10::CUniTensor& lam, int N ) {
	///
	std::vector< SchmidtVal > spec;
	sortSchmidtValsQn( lam, spec );
	spec.erase( spec.begin()+N, spec.end() );

	return spec;
}

//======================================

