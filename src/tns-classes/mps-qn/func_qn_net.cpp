#include <tns-func/tns_const.h>
#include <tns-func/func_op.h>
#include <tns-func/func_net.h>
#include <mps-qn/func_qn_net.h>
#include <algorithm>	// std::find

//=============================================

void applyCrossGate( uni10::CUniTensor& ten, int to_permute, std::vector<int>& to_cross ) {
	/// bonds in to_cross must be consecutive and obey their order in ten.label
	std::vector<int> lab_ori = ten.label();
	int tbn = lab_ori.size();
	int ibn = ten.inBondNum();
	int cgn = to_cross.size();	// cross gate number
	std::vector<int> lab_tmp;
	std::vector<int> lab_fin;

	std::vector<int> indi;	// indicate the bond is before/at/after crossed bonds
	bool before = true;
	for (int i = 0; i < tbn; ++i) {
		bool found = (lab_ori[i] == to_permute);
		int j = 0;
		while (!found && j < cgn) {
			found = (lab_ori[i] == to_cross[j]);
			j += 1;
		}
		if (found) {
			indi.push_back(1);
			if (before)
				before = false;
		}
		else
			indi.push_back((int)(!before)*2);
	}

	for (int n = 0; n < tbn; ++n) {
		if (indi[n] == 0) {
			lab_tmp.push_back(lab_ori[n]);
			lab_fin.push_back(lab_ori[n]);
		}
	}

	lab_fin.push_back(to_permute);
	for (int j = 0; j < cgn; ++j) {
		lab_tmp.push_back(to_cross[j]);
		lab_fin.push_back(to_cross[j]);
	}
	lab_tmp.push_back(to_permute);

	for (int n = 0; n < tbn; ++n) {
		if (indi[n] == 2) {
			lab_tmp.push_back(lab_ori[n]);
			lab_fin.push_back(lab_ori[n]);
		}
	}

	auto itc1 = std::find(lab_ori.begin(), lab_ori.end(), to_cross[0]);
	auto itc2 = std::find(lab_ori.begin(), lab_ori.end(), to_cross[to_cross.size()-1]);
	auto itp = std::find(lab_ori.begin(), lab_ori.end(), to_permute);
	auto ic1 = std::distance(lab_ori.begin(), itc1);
	auto ic2 = std::distance(lab_ori.begin(), itc2);
	auto ip = std::distance(lab_ori.begin(), itp);
	bool out2in_tmp = (ten.bond()[ic2].type() == 1) && (ten.bond()[ip].type() == -1);
	bool out2in_fin = (ten.bond()[ic1].type() == 1) && (ten.bond()[ip].type() == -1);
	ten.permute(lab_tmp, ibn+(int)out2in_tmp);
	ten.permuteFm(lab_fin, ibn+(int)out2in_fin);	// where cross gate applied
	ten.permute(lab_ori, ibn);
}

//=============================================

void applyCrossGate( uni10::CUniTensor& ten, int to_permute, int to_cross ) {
	///
	std::vector<int> crosses;
	crosses.push_back(to_cross);
	applyCrossGate(ten, to_permute, crosses);
}

//=============================================

uni10::CUniTensor crossGate( uni10::CUniTensor ten, int to_permute, std::vector<int>& to_cross ) {
	///
	applyCrossGate(ten, to_permute, to_cross);
	return ten;
}

//=============================================

uni10::CUniTensor crossGate( uni10::CUniTensor ten, int to_permute, int to_cross ) {
	///
	applyCrossGate(ten, to_permute, to_cross);
	return ten;
}

//=============================================

uni10::CUniTensor dag( uni10::CUniTensor ten ) {
	///
	return ten.cTranspose();
}

//=============================================

uni10::CUniTensor innerProdQn( uni10::CUniTensor bra, uni10::CUniTensor ket ) {
	///
	// assume bra, ket same bond structure
	bra.setLabel( ket.label() );
	bra.cTranspose();
	uni10::CUniTensor net = uni10::contract(bra, ket);

	return net;
}

//=============================================

double vecNormQn( uni10::CUniTensor vec ) {
	///
	return std::sqrt( innerProdQn( vec, vec )[0].real() );
}

//=============================================

uni10::CUniTensor bondInvGamU1( uni10::CUniTensor gam ) {
	/// only works when phys bond's qnums are symmetric (ex: +1, -1)
	std::vector<uni10::Bond> bdg0 = gam.bond();

	gam = dag(gam);
	int lab_gd0[] = {1, 0, 100};
	int lab_gd1[] = {100, 1, 0};
	gam.setLabel( lab_gd0 );
	gam.permute( lab_gd1, 1 );
	std::vector<uni10::Bond> bdg1 = gam.bond();
	std::vector<uni10::Qnum> qlist = gam.blockQnum();

	std::vector<uni10::Bond> bdgf;
	bdgf.push_back( uni10::Bond(uni10::BD_IN, bdg0[1].Qlist()) );	// we want the original qlist of phy bond
	bdgf.push_back( uni10::Bond(uni10::BD_OUT, bdg1[1].Qlist()) );
	bdgf.push_back( uni10::Bond(uni10::BD_OUT, bdg1[2].Qlist()) );
	uni10::CUniTensor gam_bi( bdgf );

	for (int i = 0; i < qlist.size(); ++i) {
		uni10::Qnum q = qlist[i];
		try {
			gam_bi.putBlock( q, gam.getBlock(q) );
		}
		catch(const std::exception& e) {
			int col = gam_bi.getBlock(q).col();
			gam_bi.putBlock( q, gam.getBlock(q).resize(1,col) );
		}
	}

	int lab_gdf[] = {1, 100, 0};
	gam_bi.setLabel( lab_gd1 );
	gam_bi.permute( lab_gdf , 2);

	return gam_bi;
}

//=============================================

uni10::CUniTensor expValQn( uni10::CUniTensor ket, uni10::CUniTensor op ) {

	// todo: assert ket.bondNum() == 3 or 4
	uni10::CUniTensor bra = ket;
	uni10::CUniTensor net = tensorOp( ket, op );
	bra.setLabel( net.label() );
	bra.cTranspose();
	net = uni10::contract( bra, net );

	return net;
}

//============================================

uni10::CUniTensor traceRhoVecQn( bool left,
	uni10::CUniTensor& lambda, uni10::CUniTensor& vec ) {
	///
	uni10::CUniTensor trace;
	uni10::CUniTensor lam_dag = dag(lambda);
	int lab_lv[] = {0, 2};
	int lab_rv[] = {1, 3};

	if (left) {
		int lab_lup[] = {1, 0};
		int lab_ldn[] = {2, 1};
		vec.setLabel( lab_lv );
		lam_dag.setLabel( lab_lup );
		trace = uni10::contract( lam_dag, vec, false );
		lambda.setLabel( lab_ldn );
		trace = uni10::contract( trace, lambda, false );
	}
	else {
		int lab_lup[] = {1, 0};
		int lab_ldn[] = {0, 3};
		vec.setLabel( lab_rv );
		lam_dag.setLabel( lab_lup );
		trace = uni10::contract( lam_dag, vec, false );
		lambda.setLabel( lab_ldn );
		trace = uni10::contract( trace, lambda, false );
	}

	return trace;
}

//============================================

uni10::CUniTensor tranMatVecQn( bool left,
	uni10::CUniTensor& ket, uni10::CUniTensor& bra,
	uni10::CUniTensor& vec ) {
	///
	uni10::CUniTensor matvec;

	int lab_lv[] = {0, 2};
	int lab_rv[] = {1, 3};

	int bn = ket.bondNum();
	std::vector<int> lab_bra(bn), lab_ket(bn);
	lab_ket[0] = 2;
	lab_ket[bn-1] = 3;
	lab_bra[0] = 1;
	lab_bra[1] = 0;
	for (int i = 0; i < bn-2; ++i) {
		lab_ket[i+1] = 100+i;
		lab_bra[i+2] = 100+i;
	}

	bra.setLabel( lab_bra );
	ket.setLabel( lab_ket );

	if (left) {
		vec.setLabel( lab_lv );
		matvec = uni10::contract( bra, vec, false );
		matvec = uni10::contract( matvec, ket, false );
		// after contraction, [0, 2] -> [1, 3]
		matvec.permute( lab_rv, vec.inBondNum() );
	}
	else {
		vec.setLabel( lab_rv );
		matvec = uni10::contract( ket, vec, false );
		matvec = uni10::contract( matvec, bra, false );
		// after contraction, [1, 3] -> [0, 2]
		matvec.permute( lab_lv, vec.inBondNum() );
	}

	return matvec;
}

//===============================================

void mpoLMQn( std::vector<uni10::CUniTensor>& mpo_lm,
	std::vector<uni10::CUniTensor>& mpo_l,
	std::vector<uni10::CUniTensor>& mpo_m ) {
	///
	// mpo vectors: [0] = sy, [1] = pl, [2] = mi
	std::vector<uni10::Bond> bd = mpo_m[0].bond();
	uni10::Bond bd_phys = bd[ bd.size()-1 ];
	// check Fermionic
	bool fermi = bd_phys.Qlist()[0].isFermionic();
	// check long range mpo
	int dim_phys = bd_phys.dim();
	uni10::CMatrix mat_sy = mpo_m[0].getRawElem();
	bool long_range = ( mat_sy.trace() != (dim_phys * 2.) );

	int lab_lsy[] = {200, 0, 100};
	int lab_lpm[] = {200, 0, 100, -1};
	int lab_msy[] = {0, 201, 1, 101};
	int lab_mpm[] = {0, 201, 1, 101, -1};
	int lab_lmsy[] = {200, 201, 1, 100, 101};
	int lab_lmpm[] = {200, 201, 1, 100, 101, -1};

	if (mpo_lm.size() < 3) {
		mpo_lm.clear();
		for (int i = 0; i < 3; ++i)
			mpo_lm.push_back(uni10::CUniTensor());
	}
	uni10::CUniTensor net_pl, net_mi;
	uni10::CUniTensor pl_cg, mi_cg;
	std::vector<int> to_cross;

	// mpoLM
	mpo_l[0].setLabel( lab_lsy );
	mpo_l[1].setLabel( lab_lpm );
	mpo_l[2].setLabel( lab_lpm );
	mpo_m[0].setLabel( lab_msy );
	mpo_m[1].setLabel( lab_mpm );
	mpo_m[2].setLabel( lab_mpm );

	to_cross = {201, 1, 101};
	mi_cg = (fermi)? crossGate(mpo_m[2], -1, to_cross) : mpo_m[2];
	pl_cg = (fermi)? crossGate(mpo_m[1], -1, to_cross) : mpo_m[1];
	mpo_lm[0] = uni10::contract(mpo_l[0], mpo_m[0], false);
	mpo_lm[0] += uni10::contract(mpo_l[1], mi_cg, false);
	mpo_lm[0] += uni10::contract(mpo_l[2], pl_cg, false);
	to_cross.clear();

	mpo_lm[1] = uni10::contract(mpo_l[0], mpo_m[1], false);
	mpo_lm[2] = uni10::contract(mpo_l[0], mpo_m[2], false);

	if (long_range) {
		mpo_l[1].setLabel( lab_lpm );
		mpo_l[2].setLabel( lab_lpm );
		mpo_m[0].setLabel( lab_msy );
		net_pl = uni10::contract(mpo_l[1], mpo_m[0], false);
		net_mi = uni10::contract(mpo_l[2], mpo_m[0], false);
		if (fermi) {
			applyCrossGate(net_pl, -1, 101);
			applyCrossGate(net_mi, -1, 101);
		}
		net_pl.permute( mpo_lm[1].label(), mpo_lm[1].inBondNum() );
		net_mi.permute( mpo_lm[2].label(), mpo_lm[2].inBondNum() );
		mpo_lm[1] += net_pl;
		mpo_lm[2] += net_mi;
	}

	mpo_lm[0].permute(lab_lmsy, 2);
	mpo_lm[1].permute(lab_lmpm, 2);
	mpo_lm[2].permute(lab_lmpm, 2);
}

//===============================================

void mpoMRQn( std::vector<uni10::CUniTensor>& mpo_mr,
	std::vector<uni10::CUniTensor>& mpo_m,
	std::vector<uni10::CUniTensor>& mpo_r,
	bool r_cross ) {
	///
	// mpo vectors: [0] = sy, [1] = pl, [2] = mi
	std::vector<uni10::Bond> bd = mpo_m[0].bond();
	uni10::Bond bd_phys = bd[ bd.size()-1 ];
	// check Fermionic
	bool fermi = bd_phys.Qlist()[0].isFermionic();
	// check long range mpo
	int dim_phys = bd_phys.dim();
	uni10::CMatrix mat_sy = mpo_m[0].getRawElem();
	bool long_range = ( mat_sy.trace() != (dim_phys * 2.) );

	int lab_msy[] = {1, 202, 2, 102};
	int lab_mpm[] = {1, 202, 2, 102, -1};
	int lab_rsy[] = {2, 203, 103};
	int lab_rpm[] = {2, 203, 103, -1};
	int lab_mrsy[] = {1, 202, 203, 102, 103};
	int lab_mrpm[] = {1, 202, 203, 102, 103, -1};

	if (mpo_mr.size() < 3) {
		mpo_mr.clear();
		for (int i = 0; i < 3; ++i)
			mpo_mr.push_back(uni10::CUniTensor());
	}
	uni10::CUniTensor net_pl, net_mi;
	uni10::CUniTensor pl_cg, mi_cg;
	std::vector<int> to_cross;

	// mpoMR
	mpo_m[0].setLabel( lab_msy );
	mpo_m[1].setLabel( lab_mpm );
	mpo_m[2].setLabel( lab_mpm );
	mpo_r[0].setLabel( lab_rsy );
	mpo_r[1].setLabel( lab_rpm );
	mpo_r[2].setLabel( lab_rpm );

	to_cross = {203, 103};
	mi_cg = (fermi && r_cross)? crossGate(mpo_r[2], -1, to_cross) : mpo_r[2];
	pl_cg = (fermi && r_cross)? crossGate(mpo_r[1], -1, to_cross) : mpo_r[1];
	mpo_mr[0] = uni10::contract(mpo_m[0], mpo_r[0], false);
	mpo_mr[0] += uni10::contract(mpo_m[1], mi_cg, false);
	mpo_mr[0] += uni10::contract(mpo_m[2], pl_cg, false);
	to_cross.clear();

	to_cross = {202, 2, 102};
	pl_cg = (fermi)? crossGate(mpo_m[1], -1, to_cross) : mpo_m[1];
	mi_cg = (fermi)? crossGate(mpo_m[2], -1, to_cross) : mpo_m[2];
	mpo_mr[1] = uni10::contract(pl_cg, mpo_r[0], false);
	mpo_mr[2] = uni10::contract(mi_cg, mpo_r[0], false);
	to_cross.clear();

	if (long_range) {
		to_cross = {203, 103};
		pl_cg = (fermi && r_cross)? crossGate(mpo_r[1], -1, to_cross) : mpo_r[1];
		mi_cg = (fermi && r_cross)? crossGate(mpo_r[2], -1, to_cross) : mpo_r[2];
		net_pl = uni10::contract(mpo_m[0], pl_cg, false);
		net_mi = uni10::contract(mpo_m[0], mi_cg, false);
		to_cross.clear();
		if (fermi) {
			applyCrossGate(net_pl, -1, 102);
			applyCrossGate(net_mi, -1, 102);
		}
		net_pl.permute( mpo_mr[1].label(), mpo_mr[1].inBondNum() );
		net_mi.permute( mpo_mr[2].label(), mpo_mr[2].inBondNum() );
		mpo_mr[1] += net_pl;
		mpo_mr[2] += net_mi;
	}

	mpo_mr[0].permute(lab_mrsy, 3);
	mpo_mr[1].permute(lab_mrpm, 3);
	mpo_mr[2].permute(lab_mrpm, 3);
}

//=============================================

uni10::CUniTensor mpoLMMRQn(
	std::vector<uni10::CUniTensor>& mpo_l,
	std::vector<uni10::CUniTensor>& mpo_m1,
	std::vector<uni10::CUniTensor>& mpo_m2,
	std::vector<uni10::CUniTensor>& mpo_r,
	bool r_cross ) {
	///
	std::vector<uni10::CUniTensor> mpo_lm, mpo_mr;
	mpoLMQn(mpo_lm, mpo_l, mpo_m1);
	mpoMRQn(mpo_mr, mpo_m2, mpo_r, r_cross);

	int lab_lmsy[] = {200, 201, 1, 100, 101};
	int lab_lmpm[] = {200, 201, 1, 100, 101, -1};
	int lab_mrsy[] = {1, 202, 203, 102, 103};
	int lab_mrpm[] = {1, 202, 203, 102, 103, -1};
	mpo_lm[0].setLabel(lab_lmsy);
	mpo_lm[1].setLabel(lab_lmpm);
	mpo_lm[2].setLabel(lab_lmpm);
	mpo_mr[0].setLabel(lab_mrsy);
	mpo_mr[1].setLabel(lab_mrpm);
	mpo_mr[2].setLabel(lab_mrpm);

	int lab_lmmr[] = {200, 201, 202, 203, 100, 101, 102, 103};
	uni10::CUniTensor op = uni10::contract(mpo_lm[0], mpo_mr[0], false);
	op += uni10::contract(mpo_lm[1], mpo_mr[2], false);
	op += uni10::contract(mpo_lm[2], mpo_mr[1], false);
	op.permute(lab_lmmr, 4);

	return op;
}

//=============================================

uni10::CUniTensor mpoLRQn(
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi ) {
	/// Works on OBC. Problematic for fermionic IBC/renormalized boundary
	bool fermi = mpo_sy[0].bond()[0].Qlist()[0].isFermionic();

	int lab_l[] = {100, 0, 101};
	int lab_r[] = {0, 200, 201};
	int lab_lpm[] = {100, 0, 101, -1};
	int lab_rpm[] = {0, 200, 201, -1};
	int lab_fin[] = {100, 200, 101, 201};

	mpo_sy[0].setLabel( lab_l );
	mpo_sy[mpo_sy.size()-1].setLabel( lab_r );
	mpo_pl[0].setLabel( lab_lpm );
	mpo_mi[mpo_mi.size()-1].setLabel( lab_rpm );
	mpo_mi[0].setLabel( lab_lpm );
	mpo_pl[mpo_pl.size()-1].setLabel( lab_rpm );

	std::vector<int> to_cross;
	to_cross.push_back(200);
	to_cross.push_back(201);
	uni10::CUniTensor mi_cg = (fermi)?
		crossGate(mpo_mi[mpo_mi.size()-1], -1, to_cross) : mpo_mi[mpo_mi.size()-1];
	uni10::CUniTensor pl_cg = (fermi)?
		crossGate(mpo_pl[mpo_pl.size()-1], -1, to_cross) : mpo_pl[mpo_pl.size()-1];

	uni10::CUniTensor op = uni10::contract(mpo_sy[0], mpo_sy[mpo_sy.size()-1], false);
	op += uni10::contract(mpo_pl[0], mi_cg, false);
	op += uni10::contract(mpo_mi[0], pl_cg, false);
	op.permute( lab_fin, 2 );

	return op;
}

//===============================================

uni10::CUniTensor mpoLMMRQn(
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi ) {
	///
	std::vector<uni10::Bond> bd = mpo_sy[1].bond();
	uni10::Bond bd_phys = bd[ bd.size()-1 ];
	// check Fermionic
	bool fermi = bd_phys.Qlist()[0].isFermionic();
	// check long range mpo
	int dim_phys = bd_phys.dim();
	uni10::CMatrix mat_sy = mpo_sy[1].getRawElem();
	bool long_range = ( mat_sy.trace() != (dim_phys * 2.) );

	int L = mpo_sy.size();

	int lab_l[] = {100, 0, 101};
	int lab_m1[]= {0, 200, 1, 201};
	int lab_m2[]= {1, 300, 2, 301};
	int lab_r[] = {2, 400, 401};

	int lab_lpm[] = {100, 0, 101, -1};
	int lab_mpm1[]= {0, 200, 1, 201, -1};
	int lab_mpm2[]= {1, 300, 2, 301, -1};
	int lab_rpm[] = {2, 400, 401, -1};

	int lab_fin[] = {100, 200, 300, 400, 101, 201, 301, 401};

	std::vector<uni10::CUniTensor> contr_mpo_sy;
	std::vector<uni10::CUniTensor> contr_mpo_pl;
	std::vector<uni10::CUniTensor> contr_mpo_mi;
	for (int i = 0; i < 2; ++i) {
		contr_mpo_sy.push_back( uni10::CUniTensor() );
		contr_mpo_pl.push_back( uni10::CUniTensor() );
		contr_mpo_mi.push_back( uni10::CUniTensor() );
	}
	uni10::CUniTensor net_pl, net_mi;
	uni10::CUniTensor pl_cg, mi_cg;
	std::vector<int> to_cross;

	// mpoLM
	mpo_sy[0].setLabel( lab_l );
	mpo_sy[1].setLabel( lab_m1 );
	mpo_pl[0].setLabel( lab_lpm );
	mpo_mi[1].setLabel( lab_mpm1 );
	mpo_mi[0].setLabel( lab_lpm );
	mpo_pl[1].setLabel( lab_mpm1 );

	to_cross.push_back(200);
	to_cross.push_back(1);
	to_cross.push_back(201);
	mi_cg = (fermi)? crossGate(mpo_mi[1], -1, to_cross) : mpo_mi[1];
	pl_cg = (fermi)? crossGate(mpo_pl[1], -1, to_cross) : mpo_pl[1];
	contr_mpo_sy[0] = uni10::contract(mpo_sy[0], mpo_sy[1], false);
	contr_mpo_sy[0] += uni10::contract(mpo_pl[0], mi_cg, false);
	contr_mpo_sy[0] += uni10::contract(mpo_mi[0], pl_cg, false);
	to_cross.clear();

	mpo_sy[0].setLabel( lab_l );
	mpo_pl[1].setLabel( lab_mpm1 );
	mpo_mi[1].setLabel( lab_mpm1 );
	contr_mpo_pl[0] = uni10::contract(mpo_sy[0], mpo_pl[1], false);
	contr_mpo_mi[0] = uni10::contract(mpo_sy[0], mpo_mi[1], false);

	if (long_range) {
		mpo_pl[0].setLabel( lab_lpm );
		mpo_mi[0].setLabel( lab_lpm );
		mpo_sy[1].setLabel( lab_m1 );
		net_pl = uni10::contract(mpo_pl[0], mpo_sy[1], false);
		net_mi = uni10::contract(mpo_mi[0], mpo_sy[1], false);
		if (fermi) {
			applyCrossGate(net_pl, -1, 201);
			applyCrossGate(net_mi, -1, 201);
		}
		net_pl.permute( contr_mpo_pl[0].label(), contr_mpo_pl[0].inBondNum() );
		net_mi.permute( contr_mpo_mi[0].label(), contr_mpo_mi[0].inBondNum() );
		contr_mpo_pl[0] += net_pl;
		contr_mpo_mi[0] += net_mi;
	}

	// mpoMR
	mpo_sy[L-2].setLabel( lab_m2 );
	mpo_sy[L-1].setLabel( lab_r );
	mpo_pl[L-2].setLabel( lab_mpm2 );
	mpo_mi[L-1].setLabel( lab_rpm );
	mpo_mi[L-2].setLabel( lab_mpm2 );
	mpo_pl[L-1].setLabel( lab_rpm );

	to_cross.push_back(400);
	to_cross.push_back(401);
	mi_cg = (fermi)? crossGate(mpo_mi[L-1], -1, to_cross) : mpo_mi[L-1];
	pl_cg = (fermi)? crossGate(mpo_pl[L-1], -1, to_cross) : mpo_pl[L-1];
	contr_mpo_sy[1] = uni10::contract(mpo_sy[L-2], mpo_sy[L-1], false);
	contr_mpo_sy[1] += uni10::contract(mpo_pl[L-2], mi_cg, false);
	contr_mpo_sy[1] += uni10::contract(mpo_mi[L-2], pl_cg, false);
	to_cross.clear();

	mpo_pl[L-2].setLabel( lab_mpm2 );
	mpo_mi[L-2].setLabel( lab_mpm2 );
	mpo_sy[L-1].setLabel( lab_r );

	to_cross.push_back(300);
	to_cross.push_back(2);
	to_cross.push_back(301);
	pl_cg = (fermi)? crossGate(mpo_pl[L-2], -1, to_cross) : mpo_pl[L-2];
	mi_cg = (fermi)? crossGate(mpo_mi[L-2], -1, to_cross) : mpo_mi[L-2];
	contr_mpo_pl[1] = uni10::contract(pl_cg, mpo_sy[L-1], false);
	contr_mpo_mi[1] = uni10::contract(mi_cg, mpo_sy[L-1], false);
	to_cross.clear();

	if (long_range) {
		mpo_sy[L-2].setLabel( lab_m2 );
		mpo_pl[L-1].setLabel( lab_rpm );
		mpo_mi[L-1].setLabel( lab_rpm );

		to_cross.push_back(400);
		to_cross.push_back(401);
		pl_cg = (fermi)? crossGate(mpo_pl[L-1], -1, to_cross) : mpo_pl[L-1];
		mi_cg = (fermi)? crossGate(mpo_mi[L-1], -1, to_cross) : mpo_mi[L-1];
		net_pl = uni10::contract(mpo_sy[L-2], pl_cg, false);
		net_mi = uni10::contract(mpo_sy[L-2], mi_cg, false);
		to_cross.clear();
		if (fermi) {
			applyCrossGate(net_pl, -1, 301);
			applyCrossGate(net_mi, -1, 301);
		}
		net_pl.permute( contr_mpo_pl[1].label(), contr_mpo_pl[1].inBondNum() );
		net_mi.permute( contr_mpo_mi[1].label(), contr_mpo_mi[1].inBondNum() );
		contr_mpo_pl[1] += net_pl;
		contr_mpo_mi[1] += net_mi;
	}

	// mpoLMMR
	uni10::CUniTensor op = uni10::contract(contr_mpo_sy[0], contr_mpo_sy[1], false);
	op += uni10::contract(contr_mpo_pl[0], contr_mpo_mi[1], false);
	op += uni10::contract(contr_mpo_mi[0], contr_mpo_pl[1], false);
	op.permute( lab_fin, 4 );

	return op;
}

//===============================================

uni10::CUniTensor op2SiteFromMPOQn(
	uni10::CUniTensor mpo_ms, uni10::CUniTensor mpo_mp, uni10::CUniTensor mpo_mm ) {
	/// Uses questionable combineBond but works. Depreciated but sometimes handy
	bool fermi = mpo_ms.bond()[1].Qlist()[0].isFermionic();
	int dim_mpo = mpo_ms.bond()[0].dim();

	uni10::CUniTensor mpo_l = mpoDummy( true, dim_mpo );
	uni10::CUniTensor mpo_r = mpoDummy( false, dim_mpo );

	int lab_l[] = {100, 0, 101};
	int lab_m1[]= {0, 200, 1, 201};
	int lab_m2[]= {1, 300, 2, 301};
	int lab_r[] = {2, 400, 401};

	int lab_mpm1[]= {0, 200, 1, 201, -1};
	int lab_mpm2[]= {1, 300, 2, 301, -1};

	int lab_fin[] = {100, 200, 300, 400, 101, 201, 301, 401};

	std::vector<uni10::CUniTensor> contr_mpo_sy;
	std::vector<uni10::CUniTensor> contr_mpo_pl;
	std::vector<uni10::CUniTensor> contr_mpo_mi;
	for (int i = 0; i < 2; ++i) {
		contr_mpo_sy.push_back( uni10::CUniTensor() );
		contr_mpo_pl.push_back( uni10::CUniTensor() );
		contr_mpo_mi.push_back( uni10::CUniTensor() );
	}

	// mpoLM
	mpo_l.setLabel( lab_l );
	mpo_ms.setLabel( lab_m1 );
	contr_mpo_sy[0] = uni10::contractFm(mpo_l, mpo_ms, false);
	mpo_mp.setLabel( lab_mpm1 );
	contr_mpo_pl[0] = uni10::contractFm(mpo_l, mpo_mp, false);
	mpo_mm.setLabel( lab_mpm1 );
	contr_mpo_mi[0] = uni10::contractFm(mpo_l, mpo_mm, false);

	// mpoMR
	mpo_ms.setLabel( lab_m2 );
	mpo_r.setLabel( lab_r );
	contr_mpo_sy[1] = uni10::contractFm(mpo_ms, mpo_r, false);
	mpo_mp.setLabel( lab_mpm2 );
	contr_mpo_pl[1] = uni10::contractFm(mpo_mp, mpo_r, false);
	mpo_mm.setLabel( lab_mpm2 );
	contr_mpo_mi[1] = uni10::contractFm(mpo_mm, mpo_r, false);

	// mpoLMMR
	uni10::CUniTensor op = uni10::contractFm(contr_mpo_sy[0], contr_mpo_sy[1], false);
	op += uni10::contractFm(contr_mpo_pl[0], contr_mpo_mi[1], false);
	op += uni10::contractFm(contr_mpo_mi[0], contr_mpo_pl[1], false);
	op.permuteFm( lab_fin, 4 );

	std::vector<int> lab_bd0(2), lab_bd1(2), lab_bd2(2), lab_bd3(2);
	lab_bd0[0] = op.label()[0];
	lab_bd0[1] = op.label()[1];
	lab_bd1[0] = op.label()[2];
	lab_bd1[1] = op.label()[3];
	lab_bd2[0] = op.label()[4];
	lab_bd2[1] = op.label()[5];
	lab_bd3[0] = op.label()[6];
	lab_bd3[1] = op.label()[7];
	op.combineBond( lab_bd0 );
	op.combineBond( lab_bd1 );
	op.combineBond( lab_bd2 );
	op.combineBond( lab_bd3 );

	return op;
}

//===============================================

uni10::CUniTensor op2SiteFromMPOQn( std::vector<std::vector<uni10::CUniTensor>>& mpo_m ) {
	/// Uses questionable combineBond but works. Depreciated but sometimes handy
	// mpo_m[i][j]: i-th site; j=0 - symm, j=1 - plus, j=2 - minus
	bool fermi = mpo_m[0][0].bond()[1].Qlist()[0].isFermionic();
	int dim_mpo = mpo_m[0][0].bond()[0].dim();

	uni10::CUniTensor mpo_l = mpoDummy( true, dim_mpo );
	uni10::CUniTensor mpo_r = mpoDummy( false, dim_mpo );

	int lab_l[] = {100, 0, 101};
	int lab_m1[]= {0, 200, 1, 201};
	int lab_m2[]= {1, 300, 2, 301};
	int lab_r[] = {2, 400, 401};

	int lab_mpm1[]= {0, 200, 1, 201, -1};
	int lab_mpm2[]= {1, 300, 2, 301, -1};

	int lab_fin[] = {100, 200, 300, 400, 101, 201, 301, 401};

	std::vector<uni10::CUniTensor> contr_mpo_sy;
	std::vector<uni10::CUniTensor> contr_mpo_pl;
	std::vector<uni10::CUniTensor> contr_mpo_mi;
	for (int i = 0; i < 2; ++i) {
		contr_mpo_sy.push_back( uni10::CUniTensor() );
		contr_mpo_pl.push_back( uni10::CUniTensor() );
		contr_mpo_mi.push_back( uni10::CUniTensor() );
	}

	// mpoLM
	mpo_l.setLabel( lab_l );
	mpo_m[0][0].setLabel( lab_m1 );
	contr_mpo_sy[0] = uni10::contractFm(mpo_l, mpo_m[0][0], false);
	mpo_m[0][1].setLabel( lab_mpm1 );
	contr_mpo_pl[0] = uni10::contractFm(mpo_l, mpo_m[0][1], false);
	mpo_m[0][2].setLabel( lab_mpm1 );
	contr_mpo_mi[0] = uni10::contractFm(mpo_l, mpo_m[0][2], false);

	// mpoMR
	int n = mpo_m.size() - 1;
	mpo_m[n][0].setLabel( lab_m2 );
	mpo_r.setLabel( lab_r );
	contr_mpo_sy[1] = uni10::contractFm(mpo_m[n][0], mpo_r, false);
	mpo_m[n][1].setLabel( lab_mpm2 );
	contr_mpo_pl[1] = uni10::contractFm(mpo_m[n][1], mpo_r, false);
	mpo_m[n][2].setLabel( lab_mpm2 );
	contr_mpo_mi[1] = uni10::contractFm(mpo_m[n][2], mpo_r, false);

	// mpoLMMR
	uni10::CUniTensor op = uni10::contractFm(contr_mpo_sy[0], contr_mpo_sy[1], false);
	op += uni10::contractFm(contr_mpo_pl[0], contr_mpo_mi[1], false);
	op += uni10::contractFm(contr_mpo_mi[0], contr_mpo_pl[1], false);
	op.permuteFm( lab_fin, 4 );

	std::vector<int> lab_bd0(2), lab_bd1(2), lab_bd2(2), lab_bd3(2);
	lab_bd0[0] = op.label()[0];
	lab_bd0[1] = op.label()[1];
	lab_bd1[0] = op.label()[2];
	lab_bd1[1] = op.label()[3];
	lab_bd2[0] = op.label()[4];
	lab_bd2[1] = op.label()[5];
	lab_bd3[0] = op.label()[6];
	lab_bd3[1] = op.label()[7];
	op.combineBond( lab_bd0 );
	op.combineBond( lab_bd1 );
	op.combineBond( lab_bd2 );
	op.combineBond( lab_bd3 );

	return op;
}

//===============================================

uni10::CUniTensor mpoMatVecQn( int size,
	std::vector<uni10::CUniTensor> mpoH_sy,
	std::vector<uni10::CUniTensor> mpoH_pl,
	std::vector<uni10::CUniTensor> mpoH_mi,
	uni10::CUniTensor psi ) {
	///
	// todo: assert size >= 2
	std::vector<uni10::Bond> bd = mpoH_sy[1].bond();
	uni10::Bond bd_phys = bd[ bd.size()-1 ];
	// check Fermionic
	bool fermi = bd_phys.Qlist()[0].isFermionic();
	// check long range mpo
	int dim_phys = bd_phys.dim();
	uni10::CMatrix mat_sy = mpoH_sy[1].getRawElem();
	bool long_range = ( mat_sy.trace() != (dim_phys * 2.) );

	uni10::CUniTensor net, net_sy, net_pl, net_mi, net_pm;
	uni10::CUniTensor net_ps, net_ms;
	uni10::CUniTensor pl_cg, mi_cg;
	std::vector<int> to_cross;

	if (size == 2) {
		// size = actual_update_size for this section
		// hard to incorporate with current algo flow. unused.
		int lab_l[] = {100, 0, 101};
		int lab_r[] = {0, 200, 201};
		int lab_lpm[] = {100, 0, 101, -1};
		int lab_rpm[] = {0, 200, 201, -1};

		int lab_psi[] = {101, 201};
		int lab_net[] = {100, 200};

		mpoH_sy[0].setLabel( lab_l );
		mpoH_sy[size-1].setLabel( lab_r );
		mpoH_pl[0].setLabel( lab_lpm );
		mpoH_pl[size-1].setLabel( lab_rpm );
		mpoH_mi[0].setLabel( lab_lpm );
		mpoH_mi[size-1].setLabel( lab_rpm );

		to_cross.push_back(200);
		to_cross.push_back(201);
		pl_cg = (fermi)? crossGate(mpoH_pl[size-1], -1, to_cross) : mpoH_pl[size-1];
		mi_cg = (fermi)? crossGate(mpoH_mi[size-1], -1, to_cross) : mpoH_mi[size-1];
		to_cross.clear();

		psi.setLabel( lab_psi );
		net = uni10::contract( mpoH_sy[0], psi, true );
		net = uni10::contract( net, mpoH_sy[size-1], true );

		net_pm = uni10::contract( mpoH_pl[0], psi, true );
		net_pm = uni10::contract( net_pm, mi_cg, true );
		net += net_pm;
		net_pm = uni10::contract( mpoH_mi[0], psi, true );
		net_pm = uni10::contract( net_pm, pl_cg, true );
		net += net_pm;

		net.permute( lab_net, size );
	}

	else if (size > 2) {
		// size = actual_update_size + 2 (cuz 2 boundaries)
		// For OBC dmrg, use mpoDummies as boundary MPO. Then we can safely apply this update
		int lab_l[] = {100, 0, 101};
		int lab_r[] = {size-2, size*100, size*100+1};
		int lab_lpm[] = {100, 0, 101, -1};
		int lab_rpm[] = {size-2, size*100, size*100+1, -1};

		std::vector<int> lab_m(4), lab_mpm(5);
		std::vector<int> lab_psi, lab_net;
		for (int i = 1; i <= size; ++i) {
			lab_psi.push_back(i*100+1);
			lab_net.push_back(i*100);
		}
		psi.setLabel( lab_psi );

		mpoH_sy[0].setLabel( lab_l );
		mpoH_pl[0].setLabel( lab_lpm );
		mpoH_mi[0].setLabel( lab_lpm );
		net_sy = uni10::contract( mpoH_sy[0], psi, true );
		net_pl = uni10::contract( mpoH_pl[0], psi, true );
		net_mi = uni10::contract( mpoH_mi[0], psi, true );

		for (int i = 2; i < size; ++i) {
			lab_m[0] = i-2;
			lab_m[1] = i*100;
			lab_m[2] = i-1;
			lab_m[3] = i*100+1;
			lab_mpm[0] = i-2;
			lab_mpm[1] = i*100;
			lab_mpm[2] = i-1;
			lab_mpm[3] = i*100+1;
			lab_mpm[4] = -1;

			mpoH_sy[i-1].setLabel( lab_m );
			mpoH_pl[i-1].setLabel( lab_mpm );
			mpoH_mi[i-1].setLabel( lab_mpm );

			to_cross.push_back( lab_mpm[1] );
			to_cross.push_back( lab_mpm[2] );
			to_cross.push_back( lab_mpm[3] );
			pl_cg = (fermi)? crossGate(mpoH_pl[i-1], -1, to_cross) : mpoH_pl[i-1];
			mi_cg = (fermi)? crossGate(mpoH_mi[i-1], -1, to_cross) : mpoH_mi[i-1];
			to_cross.clear();

			net = uni10::contract( net_sy, mpoH_sy[i-1], true );
			net += uni10::contract( net_pl, mi_cg, true );
			net += uni10::contract( net_mi, pl_cg, true );

			if (long_range) {
				pl_cg = (fermi)? crossGate(net_pl, -1, lab_m[3]) : net_pl;
				mi_cg = (fermi)? crossGate(net_mi, -1, lab_m[3]) : net_mi;
				net_ps = uni10::contract( pl_cg, mpoH_sy[i-1], true );
				net_ms = uni10::contract( mi_cg, mpoH_sy[i-1], true );

				net_pl = uni10::contract( net_sy, mpoH_pl[i-1], true );
				net_mi = uni10::contract( net_sy, mpoH_mi[i-1], true );
				net_ps.permute( net_pl.label(), net_pl.inBondNum() );
				net_ms.permute( net_mi.label(), net_mi.inBondNum() );
				net_pl += net_ps;
				net_mi += net_ms;
			}
			else {
				net_pl = uni10::contract( net_sy, mpoH_pl[i-1], true );
				net_mi = uni10::contract( net_sy, mpoH_mi[i-1], true );
			}

			net_sy = net;
		}

		mpoH_sy[size-1].setLabel( lab_r );
		mpoH_pl[size-1].setLabel( lab_rpm );
		mpoH_mi[size-1].setLabel( lab_rpm );

		// to_cross.push_back( lab_rpm[1] );
		// to_cross.push_back( lab_rpm[2] );
		// pl_cg = (fermi)? crossGate(mpoH_pl[size-1], -1, to_cross) : mpoH_pl[size-1];
		// mi_cg = (fermi)? crossGate(mpoH_mi[size-1], -1, to_cross) : mpoH_mi[size-1];
		// to_cross.clear();
		pl_cg = mpoH_pl[size-1];	// cross gates already applied in previous renormMPOR
		mi_cg = mpoH_mi[size-1];	// even for OBC update. cuz dummy is used instead of mpo_r

		net = uni10::contract( net_sy, mpoH_sy[size-1], true );
		net += uni10::contract( net_pl, mi_cg, true );
		net += uni10::contract( net_mi, pl_cg, true );

		net.permute( lab_net, size );
	}

	else
		std::cerr << "mpoMatVecQn: MPO vec size must be >= 2.\n";

	return net;
}

//===============================================

void renormMPOL(
	std::vector<uni10::CUniTensor>& mpoH_sy,
	std::vector<uni10::CUniTensor>& mpoH_pl,
	std::vector<uni10::CUniTensor>& mpoH_mi,
	uni10::CUniTensor ket, bool initial ) {
	///
	std::vector<uni10::Bond> bd = mpoH_sy[1].bond();
	uni10::Bond bd_phys = bd[ bd.size()-1 ];
	// check Fermionic
	bool fermi = bd_phys.Qlist()[0].isFermionic();
	// check long range mpo
	int dim_phys = bd_phys.dim();
	uni10::CMatrix mat_sy = mpoH_sy[1].getRawElem();
	bool long_range = ( mat_sy.trace() != (dim_phys * 2.) );

	uni10::CUniTensor bra = dag(ket);
	uni10::CUniTensor pl_cg, mi_cg;
	std::vector<int> to_cross;

	int lab_new[] = {0, 1, 2};
	int lab_npm[] = {0, 1, 2, -1};

	if (initial) {
		int lab_bra[] = {0, -10, 200};
		int lab_l[]   = {200, 1, 100};
		int lab_lpm[] = {200, 1, 100, -1};
		int lab_ket[] = {-10, 100, 2};
		bra.setLabel( lab_bra );
		ket.setLabel( lab_ket );

		mpoH_sy[0].setLabel( lab_l );
		mpoH_sy[0] = uni10::contract( bra, mpoH_sy[0], false );
		mpoH_sy[0] = uni10::contract( mpoH_sy[0], ket, false );
		mpoH_sy[0].permute( lab_new, 1 );

		mpoH_pl[0].setLabel( lab_lpm );
		mpoH_pl[0] = uni10::contract( bra, mpoH_pl[0], false );
		mpoH_pl[0] = uni10::contract( mpoH_pl[0], ket, false );
		mpoH_pl[0].permute( lab_npm, 1 );

		mpoH_mi[0].setLabel( lab_lpm );
		mpoH_mi[0] = uni10::contract( bra, mpoH_mi[0], false );
		mpoH_mi[0] = uni10::contract( mpoH_mi[0], ket, false );
		mpoH_mi[0].permute( lab_npm, 1 );
	}
	else {
		int lab_bra[] = {0, 200, 201};
		int lab_l[]   = {200, 10, 100};
		int lab_lpm[] = {200, 10, 100, -1};
		int lab_m[]   = {10, 201, 1, 101};
		int lab_mpm[] = {10, 201, 1, 101, -1};
		int lab_ket[] = {100, 101, 2};
		bra.setLabel( lab_bra );
		ket.setLabel( lab_ket );

		uni10::CUniTensor net_sy, net_pm;
		uni10::CUniTensor net_ps, net_ms;

		net_sy = mpoH_sy[0];
		net_sy.setLabel( lab_l );
		mpoH_pl[0].setLabel( lab_lpm );
		mpoH_mi[0].setLabel( lab_lpm );
		mpoH_sy[1].setLabel( lab_m );
		mpoH_pl[1].setLabel( lab_mpm );
		mpoH_mi[1].setLabel( lab_mpm );

		to_cross.push_back( lab_mpm[1] );
		to_cross.push_back( lab_mpm[2] );
		to_cross.push_back( lab_mpm[3] );
		pl_cg = (fermi)? crossGate(mpoH_pl[1], -1, to_cross) : mpoH_pl[1];
		mi_cg = (fermi)? crossGate(mpoH_mi[1], -1, to_cross) : mpoH_mi[1];
		to_cross.clear();

		mpoH_sy[0] = uni10::contract( bra, net_sy, false );
		mpoH_sy[0] = uni10::contract( mpoH_sy[0], mpoH_sy[1], false );
		mpoH_sy[0] = uni10::contract( mpoH_sy[0], ket, false );

		net_pm = uni10::contract( bra, mpoH_pl[0], false );
		net_pm = uni10::contract( net_pm, mi_cg, false );
		net_pm = uni10::contract( net_pm, ket, false );
		mpoH_sy[0] += net_pm;
		net_pm = uni10::contract( bra, mpoH_mi[0], false );
		net_pm = uni10::contract( net_pm, pl_cg, false );
		net_pm = uni10::contract( net_pm, ket, false );
		mpoH_sy[0] += net_pm;
		mpoH_sy[0].permute( lab_new, 1 );

		if (long_range) {
			net_ps = uni10::contract( bra, mpoH_pl[0], false );
			net_ps = uni10::contract( net_ps, mpoH_sy[1], false );
			net_ms = uni10::contract( bra, mpoH_mi[0], false );
			net_ms = uni10::contract( net_ms, mpoH_sy[1], false );
			if (fermi) {
				applyCrossGate(net_ps, -1, 101);
				applyCrossGate(net_ms, -1, 101);
			}
			net_ps = uni10::contract( net_ps, ket, false );
			net_ms = uni10::contract( net_ms, ket, false );
			net_ps.permute( lab_npm, 1 );
			net_ms.permute( lab_npm, 1 );
		}

		mpoH_pl[0] = uni10::contract( bra, net_sy, false );
		mpoH_pl[0] = uni10::contract( mpoH_pl[0], mpoH_pl[1], false );
		mpoH_pl[0] = uni10::contract( mpoH_pl[0], ket, false );
		mpoH_pl[0].permute( lab_npm, 1 );

		mpoH_mi[0] = uni10::contract( bra, net_sy, false );
		mpoH_mi[0] = uni10::contract( mpoH_mi[0], mpoH_mi[1], false );
		mpoH_mi[0] = uni10::contract( mpoH_mi[0], ket, false );
		mpoH_mi[0].permute( lab_npm, 1 );

		if (long_range) {
			mpoH_pl[0] += net_ps;
			mpoH_mi[0] += net_ms;
		}
	}
}

//===============================================

void renormMPOR(
	std::vector<uni10::CUniTensor>& mpoH_sy,
	std::vector<uni10::CUniTensor>& mpoH_pl,
	std::vector<uni10::CUniTensor>& mpoH_mi,
	uni10::CUniTensor ket, bool initial ) {
	///
	std::vector<uni10::Bond> bd = mpoH_sy[1].bond();
	uni10::Bond bd_phys = bd[ bd.size()-1 ];
	// check Fermionic
	bool fermi = bd_phys.Qlist()[0].isFermionic();
	// check long range mpo
	int dim_phys = bd_phys.dim();
	uni10::CMatrix mat_sy = mpoH_sy[1].getRawElem();
	bool long_range = ( mat_sy.trace() != (dim_phys * 2.) );

	uni10::CUniTensor bra = dag(ket);
	uni10::CUniTensor pl_cg, mi_cg;
	std::vector<int> to_cross;

	int n = (int)mpoH_sy.size()-1;
	int lab_new[] = {1, 0, 2};
	int lab_npm[] = {1, 0, 2, -1};

	if (initial) {
		int lab_bra[] = {-10, 0, 200};
		int lab_r[]   = {1, 200, 100};
		int lab_rpm[] = {1, 200, 100, -1};
		int lab_ket[] = {2, 100, -10};
		bra.setLabel( lab_bra );
		ket.setLabel( lab_ket );

		mpoH_sy[n].setLabel( lab_r );
		mpoH_pl[n].setLabel( lab_rpm );
		mpoH_mi[n].setLabel( lab_rpm );

		mpoH_sy[n] = uni10::contract( bra, mpoH_sy[n], false );
		mpoH_sy[n] = uni10::contract( mpoH_sy[n], ket, false );
		mpoH_sy[n].permute( lab_new, 2 );

		to_cross.push_back( lab_rpm[1] );
		to_cross.push_back( lab_rpm[2] );
		pl_cg = (fermi)? crossGate(mpoH_pl[n], -1, to_cross) : mpoH_pl[n];
		mi_cg = (fermi)? crossGate(mpoH_mi[n], -1, to_cross) : mpoH_mi[n];
		to_cross.clear();

		mpoH_pl[n] = uni10::contract( bra, pl_cg, false );
		mpoH_pl[n] = uni10::contract( mpoH_pl[n], ket, false );
		mpoH_pl[n].permute( lab_npm, 2 );

		mpoH_mi[n] = uni10::contract( bra, mi_cg, false );
		mpoH_mi[n] = uni10::contract( mpoH_mi[n], ket, false );
		mpoH_mi[n].permute( lab_npm, 2 );
	}
	else {
		int lab_bra[] = {200, 0, 201};
		int lab_r[]   = {10, 200, 100};
		int lab_rpm[] = {10, 200, 100, -1};
		int lab_m[]   = {1, 201, 10, 101};
		int lab_mpm[] = {1, 201, 10, 101, -1};
		int lab_ket[] = {2, 101, 100};
		bra.setLabel( lab_bra );
		ket.setLabel( lab_ket );

		uni10::CUniTensor net_sy, net_pm;
		uni10::CUniTensor net_ps, net_ms;

		net_sy = mpoH_sy[n];
		net_sy.setLabel( lab_r );
		mpoH_pl[n].setLabel( lab_rpm );
		mpoH_mi[n].setLabel( lab_rpm );
		mpoH_sy[n-1].setLabel( lab_m );
		mpoH_pl[n-1].setLabel( lab_mpm );
		mpoH_mi[n-1].setLabel( lab_mpm );

		mpoH_sy[n] = uni10::contract( bra, net_sy, false );
		mpoH_sy[n] = uni10::contract( mpoH_sy[n], mpoH_sy[n-1], false );
		mpoH_sy[n] = uni10::contract( mpoH_sy[n], ket, false );
		net_pm = uni10::contract( bra, mpoH_pl[n], false );
		net_pm = uni10::contract( net_pm, mpoH_mi[n-1], false );
		net_pm = uni10::contract( net_pm, ket, false );
		mpoH_sy[n] += net_pm;
		net_pm = uni10::contract( bra, mpoH_mi[n], false );
		net_pm = uni10::contract( net_pm, mpoH_pl[n-1], false );
		net_pm = uni10::contract( net_pm, ket, false );
		mpoH_sy[n] += net_pm;
		mpoH_sy[n].permute( lab_new, 2 );

		if (long_range) {
			net_ps = uni10::contract( bra, mpoH_pl[n], false );
			net_ps = uni10::contract( net_ps, mpoH_sy[n-1], false );
			net_ms = uni10::contract( bra, mpoH_mi[n], false );
			net_ms = uni10::contract( net_ms, mpoH_sy[n-1], false );
			if (fermi) {
				applyCrossGate(net_ps, -1, 101);
				applyCrossGate(net_ms, -1, 101);
			}
			net_ps = uni10::contract( net_ps, ket, false );
			net_ms = uni10::contract( net_ms, ket, false );
			net_ps.permute( lab_npm, 2 );
			net_ms.permute( lab_npm, 2 );
		}

		to_cross.push_back( lab_mpm[1] );
		to_cross.push_back( lab_mpm[2] );
		to_cross.push_back( lab_mpm[3] );
		pl_cg = (fermi)? crossGate(mpoH_pl[n-1], -1, to_cross) : mpoH_pl[n-1];
		mi_cg = (fermi)? crossGate(mpoH_mi[n-1], -1, to_cross) : mpoH_mi[n-1];
		to_cross.clear();

		mpoH_pl[n] = uni10::contract( bra, net_sy, false );
		mpoH_pl[n] = uni10::contract( mpoH_pl[n], pl_cg, false );
		mpoH_pl[n] = uni10::contract( mpoH_pl[n], ket, false );
		mpoH_pl[n].permute( lab_npm, 2 );

		mpoH_mi[n] = uni10::contract( bra, net_sy, false );
		mpoH_mi[n] = uni10::contract( mpoH_mi[n], mi_cg, false );
		mpoH_mi[n] = uni10::contract( mpoH_mi[n], ket, false );
		mpoH_mi[n].permute( lab_npm, 2 );

		if (long_range) {
			mpoH_pl[n] += net_ps;
			mpoH_mi[n] += net_ms;
		}
	}
}

//===============================================

std::vector<uni10::CUniTensor> allLRVecsQn(
	const std::vector<uni10::CUniTensor>& gams,
	const std::vector<uni10::CUniTensor>& lams,
	bool left ) {
  ///
	// assert lam vec size > game vec
	std::vector<uni10::CUniTensor> blk;

	int L = gams.size();
	for (int i = 0; i < L; ++i)
		blk.push_back( uni10::CUniTensor() );

	uni10::CUniTensor lvec;
	uni10::CUniTensor rvec;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	if (left) {

		int lab_lvec[] = {0, 1};
		int lab_bra1[] = {2, 0, 100};
		int lab_ket1[] = {1, 100, 3};

		lvec.assign( lams[0].bond() );
		lvec.identity();
		lvec.setLabel( lab_lvec );

		for (int i = 0; i < L; ++i) {

			ket = netLG( lams[i], gams[i] );
			bra = dag( ket );

			bra.setLabel( lab_bra1 );
			ket.setLabel( lab_ket1 );

			lvec = uni10::contract( bra, lvec, false );
			lvec = uni10::contract( lvec, ket, false );
			lvec.setLabel( lab_lvec );

			blk[i] = lvec;	// top (bra) in bottom (ket) out
		}
	}

	else {
		int lab_rvec[] = {3, 2};
		int lab_bra2[] = {2, 0, 100};
		int lab_ket2[] = {1, 100, 3};

		rvec.assign( lams[L].bond() );
		rvec.identity();
		rvec.setLabel( lab_rvec );

		for (int i = L-1; i >= 0; --i) {

			ket = netGL( gams[i], lams[i+1] );
			bra = dag( ket );

			bra.setLabel( lab_bra2 );
			ket.setLabel( lab_ket2 );

			rvec = uni10::contract( ket, rvec, false );
			rvec = uni10::contract( rvec, bra, false );
			rvec.setLabel( lab_rvec );

			blk[i] = rvec;	// bottom (ket) in top (bra) out
			// For expVal calculation. Don't need to be top in bottom out as MPO blks
		}
	}

  return blk;
}

//===============================================

uni10::CUniTensor buildLRVecQn( int loc,
	const std::vector<uni10::CUniTensor>& gams,
	const std::vector<uni10::CUniTensor>& lams,
	bool left ) {
  ///
	// assert lam vec size > game vec
	int L = gams.size();
	uni10::CUniTensor vec;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	if (left) {

		int lab_lvec[] = {0, 1};
		int lab_bra1[] = {2, 0, 100};
		int lab_ket1[] = {1, 100, 3};

		vec.assign( lams[0].bond() );
		vec.identity();
		vec.setLabel( lab_lvec );

		for (int i = 0; i <= loc; ++i) {

			ket = netLG( lams[i], gams[i] );
			bra = dag( ket );

			bra.setLabel( lab_bra1 );
			ket.setLabel( lab_ket1 );

			vec = uni10::contract( bra, vec, false );
			vec = uni10::contract( vec, ket, false );
			vec.setLabel( lab_lvec );	// top (bra) in bottom (ket) out
		}
	}

	else {
		int lab_rvec[] = {3, 2};
		int lab_bra2[] = {2, 0, 100};
		int lab_ket2[] = {1, 100, 3};

		vec.assign( lams[L].bond() );
		vec.identity();
		vec.setLabel( lab_rvec );

		for (int i = L-1; i >= loc; --i) {

			ket = netGL( gams[i], lams[i+1] );
			bra = dag( ket );

			bra.setLabel( lab_bra2 );
			ket.setLabel( lab_ket2 );

			vec = uni10::contract( ket, vec, false );
			vec = uni10::contract( vec, bra, false );
			vec.setLabel( lab_rvec );	// bottom (ket) in top (bra) out
		}
	}

  return vec;
}

//===============================================

uni10::CUniTensor buildLRVecQn(
	uni10::CUniTensor& vec_last, uni10::CUniTensor ket, bool left ) {
	///
	// assert vec.bondNum = 2 or 3
	// assert vec_last in [top; -1 bot] for left or [-1, bot; top] for right if vec.bondNum = 3
	bool fermi = ket.bond()[0].Qlist()[0].isFermionic();
	uni10::CUniTensor vec = vec_last;
	uni10::CUniTensor bra = dag( ket );
	int bnv = vec.bondNum();
	int bnk = ket.bondNum();

	// ex: if ket is 2-site
	// lab_bra[] = {2, 0, 100, 101};
	// lab_ket[] = {1, 100, 101, 3};
	std::vector<int> lab_bra, lab_ket;
	std::vector<int> to_cross; // fermionc cross gates
	lab_bra.push_back(2);
	lab_bra.push_back(0);
	lab_ket.push_back(1);
	for (int i = 0; i < bnk-2; ++i) {
		lab_bra.push_back(100 + i);
		lab_ket.push_back(100 + i);
		to_cross.push_back(100 + i);
	}
	lab_ket.push_back(3);

	bra.setLabel( lab_bra );
	ket.setLabel( lab_ket );

	if (left) {

		std::vector<int> lab_lvec(bnv);
		if (bnv == 2) {
			lab_lvec[0] = 0;
			lab_lvec[1] = 1;
		}
		else if (bnv == 3) {
			lab_lvec[0] = 0;
			lab_lvec[1] = -1;
			lab_lvec[2] = 1;
		}
		vec.setLabel( lab_lvec );

		vec = uni10::contract( bra, vec, false );
		if (bnv == 3 && fermi)
			applyCrossGate(vec, -1, to_cross);
		vec = uni10::contract( vec, ket, false );
		if (bnv == 3)
			vec.permute(1);
	}

	else {
		std::vector<int> lab_rvec(bnv);
		if (bnv == 2) {
			lab_rvec[0] = 3;
			lab_rvec[1] = 2;
		}
		else if (bnv == 3) {
			lab_rvec[0] = -1;
			lab_rvec[1] = 3;
			lab_rvec[2] = 2;
		}
		vec.setLabel( lab_rvec );

		vec = uni10::contract( ket, vec, false );
		if (bnv == 3 && fermi)
			applyCrossGate(vec, -1, to_cross);
		vec = uni10::contract( vec, bra, false );
		if (bnv == 3) {
			int lab_fin[] = {-1, 1, 0};
			vec.permute( lab_fin, 2 );
		}
	}

  return vec;
}

//===============================================

uni10::CUniTensor buildLRVecQn(
	uni10::CUniTensor& vec_last, uni10::CUniTensor ket,
	uni10::CUniTensor& op, bool left ) {
	///
	// assert vec.bondNum = 2 or 3
	// assert op.bondNum = 2 or 3 (1-site op) or 4 (2-site op)
	// assert vec_last in [top; -1 bot] for left or [-1, bot; top] for right if vec.bondNum = 3
	bool fermi = ket.bond()[0].Qlist()[0].isFermionic();
	uni10::CUniTensor vec = vec_last;
	uni10::CUniTensor bra = dag( ket );
	int bnv = vec.bondNum();
	int bnk = ket.bondNum();
	int bno = op.bondNum();

	// ex: if operator is 2-site
	// lab_bra[] = {2, 0, 200, 201};
	// lab_ket[] = {1, 100, 101, 3};
	std::vector<int> lab_bra, lab_ket;
	lab_bra.push_back(2);
	lab_bra.push_back(0);
	lab_ket.push_back(1);
	for (int i = 0; i < bnk-2; ++i) {
		lab_bra.push_back(200 + i);
		lab_ket.push_back(100 + i);
	}
	lab_ket.push_back(3);

	std::vector<int> lab_op(bno);
	if (bno == 2) {
		lab_op[0] = 200;
		lab_op[1] = 100;
	}
	else if (bno == 3) {
		lab_op[0] = 200;
		lab_op[1] = 100;
		lab_op[2] = -1;
	}
	else if (bno == 4) {
		lab_op[0] = 200;
		lab_op[1] = 201;
		lab_op[2] = 100;
		lab_op[3] = 101;
	}

	bra.setLabel( lab_bra );
	ket.setLabel( lab_ket );
	op.setLabel( lab_op );

	if (left) {

		std::vector<int> lab_lvec(bnv);
		if (bnv == 2) {
			lab_lvec[0] = 0;
			lab_lvec[1] = 1;
		}
		else if (bnv == 3) {
			lab_lvec[0] = 0;
			lab_lvec[1] = -1;
			lab_lvec[2] = 1;

			if (bno == 3) {
				// apply swap gates to the fermionic operator on the right
				std::vector<int> to_cross;
				to_cross.push_back(200);
				to_cross.push_back(100);
				applyCrossGate(op, -1, to_cross);
			}
		}
		vec.setLabel( lab_lvec );

		vec = uni10::contract( bra, vec, false );
		vec = uni10::contract( vec, op, false );
		vec = uni10::contract( vec, ket, false );
		if (bnv == 3)
			vec.permute(1);
	}

	else {
		// currently only contract op from the left. didn't take care swap gates here
		// todo: enable contracting fm op from the right
		std::vector<int> lab_rvec(bnv);
		if (bnv == 2) {
			lab_rvec[0] = 3;
			lab_rvec[1] = 2;

			if (bno == 3) {
				// apply swap gates to the fermionic operator on the right
				std::vector<int> to_cross;
				to_cross.push_back(200);
				to_cross.push_back(100);
				applyCrossGate(op, -1, to_cross);
			}
		}
		else if (bnv == 3) {
			lab_rvec[0] = -1;
			lab_rvec[1] = 3;
			lab_rvec[2] = 2;
		}
		vec.setLabel( lab_rvec );

		vec = uni10::contract( ket, vec, false );
		vec = uni10::contract( op, vec, false );
		vec = uni10::contract( vec, bra, false );
		if (bnv == 3 && bno != 3) {
			int lab_fin[] = {-1, 1, 0};
			vec.permute( lab_fin, 2 );
		}
	}

  return vec;
}

//===============================================

uni10::CUniTensor buildLRVecQn(
	int loc, uni10::CUniTensor& vec_last,
	const std::vector<uni10::CUniTensor>& gams,
	const std::vector<uni10::CUniTensor>& lams,
	bool left ) {
  ///
	// assert lam vec size > game vec
	int L = gams.size();
	uni10::CUniTensor vec;
	uni10::CUniTensor ket;

	if (left) {
		vec.assign( lams[0].bond() );
		vec.identity();

		for (int i = 0; i <= loc; ++i) {
			ket = netLG( lams[i], gams[i] );
			vec = buildLRVecQn( vec, ket, left );
		}
	}

	else {
		vec.assign( lams[L].bond() );
		vec.identity();

		for (int i = L-1; i >= loc; --i) {
			ket = netGL( gams[i], lams[i+1] );
			vec = buildLRVecQn( vec, ket, left );
		}
	}

  return vec;
}

//===============================================

uni10::CUniTensor contrLRVecQn(
	uni10::CUniTensor& lvec, uni10::CUniTensor& rvec ) {
	///
	// todo: assert l/r bonds match
	uni10::CUniTensor net;

	int bnv = lvec.bondNum();

	if (bnv == 2) {
		int lab_lvec[] = {0, 1};
		int lab_rvec[] = {1, 0};
		lvec.setLabel( lab_lvec );
		rvec.setLabel( lab_rvec );
		net = uni10::contract( lvec, rvec, false );
	}
	else if (bnv == 3) {
		int lab_lvec[] = {0, -1, 1};
		int lab_rvec[] = {-1, 1, 0};
		lvec.setLabel( lab_lvec );
		rvec.setLabel( lab_rvec );
		net = uni10::contract( lvec, rvec, false );
	}

	return net;
}

//===============================================

void initDMRGBufferQn( int loc,
	std::vector<uni10::CUniTensor>& gamma,
	std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	std::vector<uni10::CUniTensor>& mpoH_sy,
	std::vector<uni10::CUniTensor>& mpoH_pl,
	std::vector<uni10::CUniTensor>& mpoH_mi,
	std::vector<uni10::CUniTensor>& buff_sy,
	std::vector<uni10::CUniTensor>& buff_pl,
	std::vector<uni10::CUniTensor>& buff_mi ) {
	///
	int L = gamma.size();  // number of mps sites
	int n = (int)mpoH_sy.size()-1;
	buff_sy.clear();
	buff_pl.clear();
	buff_mi.clear();

	for (int i = 0; i < L; ++i) {
		buff_sy.push_back( uni10::CUniTensor() );
		buff_pl.push_back( uni10::CUniTensor() );
		buff_mi.push_back( uni10::CUniTensor() );
	}

	// idx 0~loc: lvec; idx loc+1~L-1: rvec
	for (int i = 0; i <= loc; ++i) {
		renormMPOL( mpoH_sy, mpoH_pl, mpoH_mi, netLG( lambda[i], gamma[i] ), false );
		buff_sy[i] = mpoH_sy[0];
		buff_pl[i] = mpoH_pl[0];
		buff_mi[i] = mpoH_mi[0];
		mpoH_sy[1] = mpo_sy[i+2];  // mpo has 2 more sites than mps: mpo[2] acts on mps[1]
		mpoH_pl[1] = mpo_pl[i+2];
		mpoH_mi[1] = mpo_mi[i+2];
	}
	mpoH_sy[n-1] = mpo_sy[L];
	mpoH_pl[n-1] = mpo_pl[L];
	mpoH_mi[n-1] = mpo_mi[L];
	for (int i = L-1; i > loc; --i) {
		renormMPOR( mpoH_sy, mpoH_pl, mpoH_mi, netGL( gamma[i], lambda[i+1]), false );
		buff_sy[i] = mpoH_sy[n];
		buff_pl[i] = mpoH_pl[n];
		buff_mi[i] = mpoH_mi[n];
		mpoH_sy[n-1] = mpo_sy[i];  // mpo has 2 more sites than mps: mpo[L-1] acts on mps[L-2]
		mpoH_pl[n-1] = mpo_pl[i];
		mpoH_mi[n-1] = mpo_mi[i];
	}
}

//===============================================

void updateDMRGBufferQn( int loc, int update_size,
	std::vector<uni10::CUniTensor>& gamma,
	std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpoH_sy,
	std::vector<uni10::CUniTensor>& mpoH_pl,
	std::vector<uni10::CUniTensor>& mpoH_mi,
	std::vector<uni10::CUniTensor>& buff_sy,
	std::vector<uni10::CUniTensor>& buff_pl,
	std::vector<uni10::CUniTensor>& buff_mi,
	bool sweep_right, int inc_size ) {
	///
	// todo: assert loc in available range
	// todo: assert 1 <= inc_size <= update_size
	int n = (int)mpoH_sy.size()-1;

	if ( sweep_right ) {
		for (int i = loc; i < loc+inc_size; ++i) {
			renormMPOL( mpoH_sy, mpoH_pl, mpoH_mi, netLG( lambda[i], gamma[i] ), false );
			buff_sy[i] = mpoH_sy[0];
			buff_pl[i] = mpoH_pl[0];
			buff_mi[i] = mpoH_mi[0];
		}
	}
	else {
		for (int i = loc+update_size-inc_size; i > loc-1; --i) {
			renormMPOR( mpoH_sy, mpoH_pl, mpoH_mi, netGL( gamma[i], lambda[i+1]), false );
			buff_sy[i] = mpoH_sy[n];
			buff_pl[i] = mpoH_pl[n];
			buff_mi[i] = mpoH_mi[n];
		}
	}
}

//===============================================

void dmrgMPOHQn( int loc, int update_size,
	std::vector<uni10::CUniTensor>& gamma,
	std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	std::vector<uni10::CUniTensor>& mpoH_sy,
	std::vector<uni10::CUniTensor>& mpoH_pl,
	std::vector<uni10::CUniTensor>& mpoH_mi,
	std::vector<uni10::CUniTensor>& buff_sy,
	std::vector<uni10::CUniTensor>& buff_pl,
	std::vector<uni10::CUniTensor>& buff_mi ) {
	///
	// todo: assert update_size >= 1
	// todo: assert loc in available range
	int L = gamma.size();
	int mpoH_size = update_size + 2;

	if ( mpoH_sy.size() != mpoH_size ) {
		mpoH_sy.clear();
		mpoH_pl.clear();
		mpoH_mi.clear();
		mpoH_sy.push_back( mpo_sy[0] );
		mpoH_pl.push_back( mpo_pl[0] );
		mpoH_mi.push_back( mpo_mi[0] );
		for (int i = 1; i <= update_size; ++i) {
			mpoH_sy.push_back( mpo_sy[i] );
			mpoH_pl.push_back( mpo_pl[i] );
			mpoH_mi.push_back( mpo_mi[i] );
		}
		mpoH_sy.push_back( mpo_sy[ mpo_sy.size()-1 ] );
		mpoH_pl.push_back( mpo_pl[ mpo_pl.size()-1 ] );
		mpoH_mi.push_back( mpo_mi[ mpo_mi.size()-1 ] );
	}

	if ( buff_sy.size() == 0 ) {
		initDMRGBufferQn( loc, gamma, lambda, mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
	}

	if ( loc == 0 ) {	// Tip: replace original mpo_l/r with mpoDummy for OBC dmrg's loc=0/L-1 update.
		for (int i = 0; i <= update_size; ++i) {
			mpoH_sy[i] = mpo_sy[i];
			mpoH_pl[i] = mpo_pl[i];
			mpoH_mi[i] = mpo_mi[i];
		}
		mpoH_sy[ mpoH_size-1 ] = buff_sy[ loc+update_size ];
		mpoH_pl[ mpoH_size-1 ] = buff_pl[ loc+update_size ];
		mpoH_mi[ mpoH_size-1 ] = buff_mi[ loc+update_size ];
	}
	else if ( loc == L-update_size ) {
		mpoH_sy[0] = buff_sy[ loc-1 ];
		mpoH_pl[0] = buff_pl[ loc-1 ];
		mpoH_mi[0] = buff_mi[ loc-1 ];
		for (int i = 0; i <= update_size; ++i) {
			mpoH_sy[i+1] = mpo_sy[ loc+i+1 ];
			mpoH_pl[i+1] = mpo_pl[ loc+i+1 ];
			mpoH_mi[i+1] = mpo_mi[ loc+i+1 ];
		}
	}
	else {
		mpoH_sy[0] = buff_sy[ loc-1 ];
		mpoH_pl[0] = buff_pl[ loc-1 ];
		mpoH_mi[0] = buff_mi[ loc-1 ];
		for (int i = 0; i < update_size; ++i) {
			mpoH_sy[i+1] = mpo_sy[ loc+i+1 ];
			mpoH_pl[i+1] = mpo_pl[ loc+i+1 ];
			mpoH_mi[i+1] = mpo_mi[ loc+i+1 ];
		}
		mpoH_sy[ mpoH_size-1 ] = buff_sy[ loc+update_size ];
		mpoH_pl[ mpoH_size-1 ] = buff_pl[ loc+update_size ];
		mpoH_mi[ mpoH_size-1 ] = buff_mi[ loc+update_size ];
	}
}

//===============================================
