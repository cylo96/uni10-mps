#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <time.h>
#include <sys/stat.h>

#include <mps-qn/CanonQnMPS.h>
#include <mps-qn/func_qn_net.h>
#include <mps-qn/func_qn_meas.h>
#include <mps-qn/func_qn_blkops.h>
#include <tns-func/func_evol.h>
#include <tns-func/func_net.h>
#include <tns-func/func_op.h>

//======================================

CanonQnMPS::CanonQnMPS( int L, int X, std::vector<uni10::Qnum>& q_phys, std::vector<uni10::Qnum>& q_virt ) :
	CanonMPS( L, q_phys.size(), X ) {
	/// object constructor
	qphys = q_phys;
	qvirt = q_virt;
}

//======================================

CanonQnMPS::CanonQnMPS( int L, int X, std::vector<uni10::Qnum>& q_phys ) :
	CanonMPS( L, q_phys.size(), X ) {
	/// object constructor
	qphys = q_phys;
}

//======================================

CanonQnMPS::CanonQnMPS( int L, int X ) :
	CanonMPS( L, 2, X ) {
	/// object constructor
}

//======================================

CanonQnMPS::~CanonQnMPS() {
	/// object destructor
}

//======================================

uni10::CUniTensor CanonQnMPS::initGamma(
	std::vector<uni10::Qnum>& qin_virt, std::vector<uni10::Qnum>& qin_phys, std::vector<uni10::Qnum>& qout_virt ) {
	/// initialize a gamma tensor
	std::vector<uni10::Bond> bond_gam;
	bond_gam.push_back( uni10::Bond(uni10::BD_IN, qin_virt) );
	bond_gam.push_back( uni10::Bond(uni10::BD_IN, qin_phys) );
	bond_gam.push_back( uni10::Bond(uni10::BD_OUT, qout_virt) );

	return uni10::CUniTensor( bond_gam );
}

//======================================

uni10::CUniTensor CanonQnMPS::initGamma(
	std::vector<uni10::Qnum>& qin_virt, std::vector<uni10::Qnum>& qin_phys ) {
	/// initialize a gamma tensor
	std::vector<uni10::Qnum> qout_virt;
	for (int i = 0; i < qin_phys.size(); ++i)
		for (int j = 0; j < qin_virt.size(); ++j)
			qout_virt.push_back( qin_phys[i] * qin_virt[j] );

	return initGamma( qin_virt, qin_phys, qout_virt );
}

//======================================

uni10::CUniTensor CanonQnMPS::initLambda( std::vector<uni10::Qnum>& q_virt ) {
	/// initialize a lambda tensor
	std::vector<uni10::Bond> bond_lam;
	bond_lam.push_back( uni10::Bond(uni10::BD_IN, q_virt) );
	bond_lam.push_back( uni10::Bond(uni10::BD_OUT, q_virt) );

	return uni10::CUniTensor( bond_lam );
}

//======================================

void CanonQnMPS::resizeGamma( int idx,
	std::vector<uni10::Qnum>& qin_virt, std::vector<uni10::Qnum>& qout_virt ) {
	/// resize a gamma tensor
	uni10::CUniTensor gam = CanonQnMPS::initGamma( qin_virt, qphys, qout_virt );

	std::map<uni10::Qnum, uni10::CMatrix> blks = gamma[idx].getBlocks();

	for (std::map<uni10::Qnum, uni10::CMatrix>::iterator it = blks.begin(); it != blks.end(); ++it) {
		uni10::Qnum q = it->first;
		uni10::CMatrix blk = it->second;
		try {
			int row = gam.getBlock(q).row();
			int col = gam.getBlock(q).col();
			gam.putBlock( q, blk.resize(row, col) );
		}
		catch(const std::exception& e) {
			continue;
		}
	}

	gamma[idx] = gam;
}

//======================================

void CanonQnMPS::resizeLambda( int idx, std::vector<uni10::Qnum>& q_virt ) {
	/// resize a lambda tensor
	uni10::CUniTensor lam = CanonQnMPS::initLambda( q_virt );

	std::map<uni10::Qnum, uni10::CMatrix> blks = lambda[idx].getBlocks();

	for (std::map<uni10::Qnum, uni10::CMatrix>::iterator it = blks.begin(); it != blks.end(); ++it) {
		uni10::Qnum q = it->first;
		uni10::CMatrix blk = it->second;
		try {
			int dim = lam.getBlock(q).col();
			lam.putBlock( q, blk.resize(dim, dim) );
		}
		catch(const std::exception& e) {
			continue;
		}
	}

	lambda[idx] = lam;
}

//======================================
/*
void CanonMPS::oneSiteOP( uni10::CUniTensor op, int idx ) {
	/// Act one-site operator on a specific site

	// todo: assert op is one-site operator
	int lab_gam[] = {0, 100, 1};
	int lab_op[] = {200, 100};
	int lab_fin[] = {0, 200, 1};

	gamma[idx].setLabel( lab_gam );
	op.setLabel( lab_op );
	gamma[idx] = uni10::contract( op, gamma[idx] );
	gamma[idx].permute( lab_fin, 2 );
}

//======================================
*/
//======================================

void CanonQnMPS::mps2SiteSVD( uni10::CUniTensor& theta,
	uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1, uni10::CUniTensor& lam2,
	bool show_err, const std::vector<uni10::Qnum>& qvec ) {
	///
	if ( theta.bondNum() != theta.inBondNum()*2 )
		theta.permute( theta.bondNum()/2 );

	std::map<uni10::Qnum, std::vector<uni10::CMatrix>> th_svd = svdQn( theta );
	truncateQn( th_svd, chi_max, 0.0, qvec );

	uni10::CUniTensor l0_inv = tenInvQn( lam0 );
	uni10::CUniTensor l2_inv = tenInvQn( lam2 );

	std::vector<uni10::Qnum> l1_qlist;
	for (std::map<uni10::Qnum, std::vector<uni10::CMatrix>>::iterator it = th_svd.begin(); it != th_svd.end(); ++it) {
		uni10::Qnum q = it->first;
		for (int i = 0; i < th_svd.at(q)[1].col(); ++i)
			l1_qlist.push_back(q);
	}

	std::vector<uni10::Bond> bdg0;
	bdg0.push_back( theta.bond()[0] );
	bdg0.push_back( theta.bond()[1] );
	bdg0.push_back( uni10::Bond(uni10::BD_OUT, l1_qlist) );
	std::vector<uni10::Bond> bdl1;
	bdl1.push_back( uni10::Bond(uni10::BD_IN, l1_qlist) );
	bdl1.push_back( uni10::Bond(uni10::BD_OUT, l1_qlist) );
	std::vector<uni10::Bond> bdg1;
	bdg1.push_back( uni10::Bond(uni10::BD_IN, l1_qlist) );
	bdg1.push_back( theta.bond()[2] );
	bdg1.push_back( theta.bond()[3] );

	gam0.assign( bdg0 );
	lam1.assign( bdl1 );
	gam1.assign( bdg1 );

	std::vector<uni10::Qnum> l1_qnums = lam1.blockQnum();
	for (std::vector<uni10::Qnum>::iterator it = l1_qnums.begin(); it != l1_qnums.end(); ++it) {
		uni10::Qnum q = *it;
    gam0.putBlock(q, th_svd.at(q)[0]);
    lam1.putBlock(q, th_svd.at(q)[1]);
    gam1.putBlock(q, th_svd.at(q)[2]);
	}

	gam0 = netLG( l0_inv, gam0 );
	gam1 = netGL( gam1, l2_inv );
}

//======================================

void CanonQnMPS::mps1SiteSVD( uni10::CUniTensor& theta,
	uni10::CUniTensor& lam0, uni10::CUniTensor& gam0, uni10::CUniTensor& lam1,
	uni10::CUniTensor& extra_uv, const std::vector<uni10::Qnum>& qvec ) {
	///
	// theta.bondNum should be 3. theta.inBondNum = 1 or 2
	int ibnt = theta.inBondNum();

	std::map<uni10::Qnum, std::vector<uni10::CMatrix>> th_svd = svdQn( theta );

	if ( theta.bond()[0].dim() > chi_max || theta.bond()[2].dim() > chi_max )
		truncateQn( th_svd, chi_max, 0.0, qvec );

	std::vector<uni10::Qnum> lam_qlist;
	for (std::map<uni10::Qnum, std::vector<uni10::CMatrix>>::iterator it = th_svd.begin(); it != th_svd.end(); ++it) {
		uni10::Qnum q = it->first;
		for (int i = 0; i < th_svd.at(q)[1].col(); ++i)
			lam_qlist.push_back(q);
	}

	std::vector<uni10::Bond> bdg;
	std::vector<uni10::Bond> bdl;
	std::vector<uni10::Bond> bde;
	bdl.push_back( uni10::Bond(uni10::BD_IN, lam_qlist) );
	bdl.push_back( uni10::Bond(uni10::BD_OUT, lam_qlist) );
	std::vector<uni10::Qnum> lam_qnums;

	if (ibnt == 2) {
		bdg.push_back( theta.bond()[0] );
		bdg.push_back( theta.bond()[1] );
		bdg.push_back( uni10::Bond(uni10::BD_OUT, lam_qlist) );
		bde.push_back( uni10::Bond(uni10::BD_IN, lam_qlist) );
		bde.push_back( theta.bond()[2] );
		lam1.assign( bdl );
		lam_qnums = lam1.blockQnum();
	}
	else if (ibnt == 1) {
		bde.push_back( theta.bond()[0] );
		bde.push_back( uni10::Bond(uni10::BD_OUT, lam_qlist) );
		bdg.push_back( uni10::Bond(uni10::BD_IN, lam_qlist) );
		bdg.push_back( theta.bond()[1] );
		bdg.push_back( theta.bond()[2] );
		lam0.assign( bdl );
		lam_qnums = lam0.blockQnum();
	}
	gam0.assign( bdg );
	extra_uv.assign( bde );

	for (std::vector<uni10::Qnum>::iterator it = lam_qnums.begin(); it != lam_qnums.end(); ++it) {
		uni10::Qnum q = *it;
		if (ibnt == 2) {
        	gam0.putBlock(q, th_svd.at(q)[0]);
        	lam1.putBlock(q, th_svd.at(q)[1]);
        	extra_uv.putBlock(q, th_svd.at(q)[2]);
		}
		else if (ibnt == 1) {
        	extra_uv.putBlock(q, th_svd.at(q)[0]);
        	lam0.putBlock(q, th_svd.at(q)[1]);
        	gam0.putBlock(q, th_svd.at(q)[2]);
		}
	}

	if (ibnt == 2) {
		double scale = 1./vecNormQn( lam1 );
		lam1 *= scale;
		gam0 = netLG( tenInvQn(lam0), gam0 );
	}
	else if (ibnt == 1) {
		double scale = 1./vecNormQn( lam0 );
		lam0 *= scale;
		gam0 = netGL( gam0, tenInvQn(lam1) );
	}
}

//======================================

void CanonQnMPS::evol2Site( uni10::CUniTensor& U,
	uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1, uni10::CUniTensor& lam2,
	bool show_err, const std::vector<uni10::Qnum>& qvec ) {
	///
	uni10::CUniTensor theta = netLGLGL( lam0, gam0, lam1, gam1, lam2 );
	theta = tensorOp( theta, U );
	mps2SiteSVD( theta, lam0, gam0, lam1, gam1, lam2, show_err, qvec );
}

//======================================
