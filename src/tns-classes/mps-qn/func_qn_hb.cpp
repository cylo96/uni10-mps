#include <uni10.hpp>
#include <mps-qn/func_qn_hb.h>
#include <mps-qn/func_qn_net.h>

using namespace std;

//============================================

uni10::CUniTensor hbVecQn( bool left,
	uni10::CUniTensor& ket, uni10::CUniTensor& bra,
	uni10::CUniTensor& lambda, uni10::CUniTensor& x ) {
	///
	uni10::CUniTensor id( x.bond() );
	id.permute( id.bondNum()/2 );
	id.identity();
	id.permute( x.inBondNum() );
	uni10::CUniTensor trx = traceRhoVecQn( left, lambda, x )[0] * id;

	return x + (-1.0) * tranMatVecQn( left, ket, bra, x ) + trx;
}

//============================================

uni10::CUniTensor hbResidQn( bool left,
	uni10::CUniTensor& ket, uni10::CUniTensor& bra,
	uni10::CUniTensor& lambda, uni10::CUniTensor& x,
	uni10::CUniTensor& b ) {
	/// Computes the residual R = B-A*X
	return b + (-1.0) * hbVecQn( left, ket, bra, lambda, x );
}

//============================================
/*
uni10::CUniTensor hbCGQn( bool left,
	uni10::CUniTensor& ket, uni10::CUniTensor& bra,
	uni10::CUniTensor& lambda, uni10::CUniTensor& x_trial,
	uni10::CUniTensor& b, bool& early_term ) {
	/// Solve linear equation A*x = b using Conjugate Gradient method
	uni10::CUniTensor x = x_trial;

	// organize column vectors
	if ( b.inBondNum() < b.bondNum() )
		b.permute( b.bondNum() );
	if ( x.inBondNum() < x.bondNum() )
		x.permute( x.bondNum() );

	// get the order of the system
	int n = b.getBlock().row();

	// Initialize
	// AP = A * x,
	// R  = b - A * x,
	// P  = b - A * x.
	uni10::CUniTensor ap = Hb_matvec( left, ket, bra, lambda, x );
	uni10::CUniTensor r = b + (-1.0) * ap;
	uni10::CUniTensor p = b + (-1.0) * ap;
	double pap, pr, rap;
	double alpha, beta;

	int it;
	double diff_min = 1.0;
	uni10::CUniTensor x_min = x;
	early_term = false;

	// Do N steps of the conjugate gradient method.
	for ( it = 1; it <= n; it++ ) {

		// Compute the matrix*vector product AP=A*P.
		ap = Hb_matvec( left, ket, bra, lambda, p );

		// Compute the dot products
		// PAP = P*AP,
		// PR  = P*R
		pap = Hb_overlap( p, ap );
		pr = Hb_overlap( p, r );

		if ( fabs(pap) < diff_min ) {
			diff_min = fabs(pap);
			x_min = x;
		}

		if ( fabs(pap) < 1e-15 ) {
			early_term = true;
			break;
		}

		// Set ALPHA = PR / PAP.
		alpha = pr / pap;

		// X = X + ALPHA * P
		// R = R - ALPHA * AP.
		x = x + alpha * p;
		r = r + (-1.0) * alpha * ap;

		// Compute the vector dot product
		// RAP = R*AP
		rap = Hb_overlap ( r, ap );

		// Set BETA = - RAP / PAP.
		beta = - rap / pap;

		// Update the perturbation vector
		// P = R + BETA * P.
		p = r + beta * p;
	}

	return x_min;
}
*/
//============================================

uni10::CUniTensor hbGMRESQn( bool left,
	uni10::CUniTensor& ket, uni10::CUniTensor& bra,
	uni10::CUniTensor& lambda, uni10::CUniTensor& x_trial,
	uni10::CUniTensor& b, int ar_iter, double ar_tol, bool& early_term ) {
	/// Solve linear equation A*x = b using GMRES method
	uni10::CUniTensor x = x_trial;

	// only qn0 has elems
	uni10::Qnum qn0(0);

	// get the order of the system
	int n = b.getBlock(qn0).row();

	// Initialize
	// ax = A * x,
	// r  = b - A * x,
	// q0  = r / ||r||.
	uni10::CUniTensor r = hbResidQn( left, ket, bra, lambda, x, b );
	std::vector<uni10::CUniTensor> q;
	for (int i = 0; i < n; ++i)
		q.push_back( uni10::CUniTensor() );
	q[0] = r * ( 1./ vecNormQn(r) );

	uni10::CUniTensor y;

	uni10::CMatrix H(ar_iter+1, ar_iter);
	H.set_zero();
	double h_jk;

	uni10::CMatrix Q(n, ar_iter);
	Q.set_zero();

//std::cout << "q[0]\n" << q[0];
	uni10::CMatrix qblk = q[0].getBlock(qn0);

	for ( int i = 0; i < n; ++i )
		Q.at(i,0) = qblk[i];

	uni10::CMatrix r0(2, 1);
	r0.at(0,0) = vecNormQn(r);
	//r0.at(0,0) = vecNormQn(b);

	uni10::CMatrix c(1, 1);
	uni10::CMatrix Hk, Qk;
	double bnorm = vecNormQn(b);

	// Resert early_term flag
	early_term = false;

	// Do ar_iter steps of Arnoldi iteration.
	for ( int k = 0; k < ar_iter; ++k ) {

		y = hbVecQn( left, ket, bra, lambda, q[k] );

		for ( int j = 0; j <= k; ++j ) {
			h_jk = innerProdQn(q[j], y)[0].real();
			y = y + (-1.0) * h_jk * q[j];
			H.at(j,k) = h_jk;
		}

		double ynorm = vecNormQn(y);
		H.at(k+1, k) = ynorm;

		if ( ynorm != 0. && k != ar_iter-1 ) {
			q[k+1] = y * (1./ynorm);

			qblk = q[k+1].getBlock(qn0);
			// Try memcpy here? Probably no. CMatrix is row major
			for ( int i = 0; i < n; ++i )
				Q.at(i, k+1) = qblk[i];
		}

		// least squre
		// find c_k for min|| H * c_k - r0 ||
		Hk = H;
		Hk.resize( k+1+1, k+1 );
		Qk = Q;
		Qk.resize( n, k+1 );
		r0.resize( k+1+1, 1 );

		std::vector<uni10::CMatrix> qrh = Hk.qr();
		// R * c = Qt * r0
		c = qrh[1].inverse() * qrh[0].transpose() * r0;
		x.putBlock( qn0, Qk * c + x_trial.getBlock(qn0) );

		// early termination
		r = hbResidQn( left, ket, bra, lambda, x, b );
		if ( (vecNormQn(r) / bnorm) < ar_tol ) {
			early_term = true;
			break;
		}
	}

	return x;
}

//============================================

uni10::CUniTensor findHbQn( bool left,
	uni10::CUniTensor ket,
	uni10::CUniTensor& lambda, uni10::CUniTensor& x_trial,
	uni10::CUniTensor& b, int ar_iter, double ar_tol ) {
	///
	// only qn0 has elems
	uni10::Qnum qn0(0);
	// force x to column vector
	if ( b.inBondNum() < b.bondNum() )
		b.permute( b.bondNum() );
	if ( x_trial.inBondNum() < x_trial.bondNum() )
		x_trial.permute( x_trial.bondNum() );

	// get the order of the system
	int n = b.getBlock(qn0).elemNum();

	uni10::CUniTensor bra = dag(ket);

	if ( ar_iter < 0 )
		ar_iter = std::min( n, 500 );
		// ar_iter = std::min( lambda.bond()[0].dim()*lambda.bond()[0].dim(), 500 );

	bool early_term = false;

	//uni10::CUniTensor x = hbCGQn( left, ket, bra, lambda, x_trial, b, early_term );
	uni10::CUniTensor x = hbGMRESQn( left, ket, bra, lambda, x_trial, b, ar_iter, ar_tol, early_term );
	uni10::CUniTensor r = hbResidQn( left, ket, bra, lambda, x, b );
	double rnorm = vecNormQn( r );
	double bnorm = vecNormQn( b );

	double tol = 1e-6;
	double maxtol = 1e-2;
	int iter = 0;
	int maxiter = 1e4/sqrt(n);

	double rn_min = rnorm;
	uni10::CUniTensor x_min = x;

	while ( ( rnorm/bnorm > tol && early_term == false ) || rnorm > maxtol || rnorm != rnorm ) {
		// rnorm != rnorm <- to tackle NaN

		if ( iter > maxiter )
			break;

		if ( early_term && rnorm == rnorm )
			//x = hbCGQn( left, ket, bra, lambda, x, b, early_term );
			x = hbGMRESQn( left, ket, bra, lambda, x, b, ar_iter, ar_tol, early_term );
		else {
			x_trial.randomize();
			//x = hbCGQn( left, ket, bra, lambda, x_trial, b, early_term );
			x = hbGMRESQn( left, ket, bra, lambda, x_trial, b, ar_iter, ar_tol, early_term );
		}

		r = hbResidQn( left, ket, bra, lambda, x, b );
		rnorm = vecNormQn( r );

		if ( rnorm < rn_min ) {
			rn_min = rnorm;
			x_min = x;
		}

		iter += 1;
	}

	if ( rn_min > maxtol )
		std::cout << "Linear solver failed to converge. rnorm > " << maxtol << ". iter = " << iter << "\n";
	//else
	//	std::cout << "Linear solving done. rnorm = " << rn_min << ". iter = " << iter << "\n";

	return x_min;
}

//============================================

uni10::CUniTensor findHbQn( bool left,
	uni10::CUniTensor ket,
	uni10::CUniTensor& lambda,
	uni10::CUniTensor& b, int ar_iter, double ar_tol ) {
	///
	uni10::CUniTensor x_trial( b.bond() );
	x_trial.set_zero();

	return findHbQn( left, ket, lambda, x_trial, b, ar_iter, ar_tol );
}
