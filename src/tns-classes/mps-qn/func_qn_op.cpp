#include <mps-qn/func_qn_op.h>
#include <tns-func/func_op.h>
#include <tns-func/func_net.h>
#include <tns-func/tns_const.h>

//======================================

uni10::CUniTensor opU1( std::string name ) {
	///
	// define qnums
	uni10::Qnum q1(1);

	// define bond parameters
	std::vector< uni10::Bond > bd2_diag;
	std::vector< uni10::Bond > bd2_rais;
	std::vector< uni10::Bond > bd2_lowr;

	std::vector< uni10::Qnum > qn2_diag;
	std::vector< uni10::Qnum > qn2_rais;
	std::vector< uni10::Qnum > qn2_lowr;
	qn2_diag.push_back(q1);
	qn2_diag.push_back(-q1);
	qn2_rais.push_back(q1*q1);	// q1 - (-q1)
	qn2_lowr.push_back(-q1*-q1);	// -[ q1 - (-q1) ]

	bd2_diag.push_back( uni10::Bond( uni10::BD_IN, qn2_diag ) );
	bd2_diag.push_back( uni10::Bond( uni10::BD_OUT, qn2_diag ) );

	bd2_rais.push_back( uni10::Bond( uni10::BD_IN, qn2_diag ) );
	bd2_rais.push_back( uni10::Bond( uni10::BD_OUT, qn2_diag ) );
	bd2_rais.push_back( uni10::Bond( uni10::BD_OUT, qn2_rais ) );

	bd2_lowr.push_back( uni10::Bond( uni10::BD_IN, qn2_diag ) );
	bd2_lowr.push_back( uni10::Bond( uni10::BD_OUT, qn2_diag ) );
	bd2_lowr.push_back( uni10::Bond( uni10::BD_OUT, qn2_lowr ) );

	// define elemental matrices
	// spin 1/2; 0.5 h_bar -> 1
	Complex sz_elem[]={ Complex(1.,0.), Complex(0.,0.),\
						Complex(0.,0.), Complex(-1.,0.) };

	Complex sp_elem[]={ Complex(0.,0.), Complex(1.,0.),\
						Complex(0.,0.), Complex(0.,0.) };

	Complex sm_elem[]={ Complex(0.,0.), Complex(0.,0.),\
						Complex(1.,0.), Complex(0.,0.) };

	uni10::CUniTensor op;

	if ( name == "id" ) {
		op.assign( bd2_diag );
		op.identity();
	}
	else if ( name == "sz" ) {
		op.assign( bd2_diag );
		op.setRawElem( sz_elem );
	}
	else if ( name == "sp" ) {
		op.assign( bd2_rais );
		op.setRawElem( sp_elem );
	}
	else if ( name == "sm" ) {
		op.assign( bd2_lowr );
		op.setRawElem( sm_elem );
	}
	else if ( name == "Sz" ) {
		op.assign( bd2_diag );
		op.setRawElem( sz_elem );
		op = 0.5 * op;
	}
	else if ( name == "Sp" ) {
		op.assign( bd2_rais );
		op.setRawElem( sp_elem );
	}
	else if ( name == "Sm" ) {
		op.assign( bd2_lowr );
		op.setRawElem( sm_elem );
	}

	op.setName( name );
	return op;
}

//======================================

uni10::CUniTensor opF( std::string name ) {
	///
	// define qnums
	uni10::Qnum q0(0);
	uni10::Qnum qf0(uni10::PRTF_ODD, 0, uni10::PRT_EVEN);

	// define bond parameters
	std::vector< uni10::Bond > bd2_diag;
	std::vector< uni10::Bond > bd2_rais;
	std::vector< uni10::Bond > bd2_lowr;

	std::vector< uni10::Qnum > qn2_diag;
	std::vector< uni10::Qnum > qn2_rais;
	std::vector< uni10::Qnum > qn2_lowr;
	qn2_diag.push_back(qf0);
	qn2_diag.push_back(q0);
	qn2_rais.push_back(qf0);	// qf0 - q0
	qn2_lowr.push_back(-qf0);	// - (qf0 - q0)

	bd2_diag.push_back( uni10::Bond( uni10::BD_IN, qn2_diag ) );
	bd2_diag.push_back( uni10::Bond( uni10::BD_OUT, qn2_diag ) );

	bd2_rais.push_back( uni10::Bond( uni10::BD_IN, qn2_diag ) );
	bd2_rais.push_back( uni10::Bond( uni10::BD_OUT, qn2_diag ) );
	bd2_rais.push_back( uni10::Bond( uni10::BD_OUT, qn2_rais ) );

	bd2_lowr.push_back( uni10::Bond( uni10::BD_IN, qn2_diag ) );
	bd2_lowr.push_back( uni10::Bond( uni10::BD_OUT, qn2_diag ) );
	bd2_lowr.push_back( uni10::Bond( uni10::BD_OUT, qn2_lowr ) );

	// define elemental matrices
	Complex N_elem[] ={ Complex(1.,0.), Complex(0.,0.),\
						Complex(0.,0.), Complex(0.,0.) };

	Complex cp_elem[]={ Complex(0.,0.), Complex(1.,0.),\
						Complex(0.,0.), Complex(0.,0.) };

	Complex cm_elem[]={ Complex(0.,0.), Complex(0.,0.),\
						Complex(1.,0.), Complex(0.,0.) };

	uni10::CUniTensor op;

	if ( name == "id" ) {
		op.assign( bd2_diag );
		op.identity();
	}
	else if ( name == "N" ) {
		op.assign( bd2_diag );
		op.setRawElem( N_elem );
	}
	else if ( name == "cp" ) {
		op.assign( bd2_rais );
		op.setRawElem( cp_elem );
	}
	else if ( name == "cm" ) {
		op.assign( bd2_lowr );
		op.setRawElem( cm_elem );
	}

	op.setName( name );
	return op;
}

//======================================

uni10::CUniTensor opFU1( std::string name ) {
	///
	// define qnums
	uni10::Qnum q0(0);
	uni10::Qnum qf1(uni10::PRTF_ODD, 1, uni10::PRT_EVEN);

	// define bond parameters
	std::vector< uni10::Bond > bd2_diag;
	std::vector< uni10::Bond > bd2_rais;
	std::vector< uni10::Bond > bd2_lowr;

	std::vector< uni10::Qnum > qn2_diag;
	std::vector< uni10::Qnum > qn2_rais;
	std::vector< uni10::Qnum > qn2_lowr;
	qn2_diag.push_back(qf1);
	qn2_diag.push_back(q0);
	qn2_rais.push_back(qf1);	// qf1 - q0
	qn2_lowr.push_back(-qf1);	// - (qf1 - q0)

	bd2_diag.push_back( uni10::Bond( uni10::BD_IN, qn2_diag ) );
	bd2_diag.push_back( uni10::Bond( uni10::BD_OUT, qn2_diag ) );

	bd2_rais.push_back( uni10::Bond( uni10::BD_IN, qn2_diag ) );
	bd2_rais.push_back( uni10::Bond( uni10::BD_OUT, qn2_diag ) );
	bd2_rais.push_back( uni10::Bond( uni10::BD_OUT, qn2_rais ) );

	bd2_lowr.push_back( uni10::Bond( uni10::BD_IN, qn2_diag ) );
	bd2_lowr.push_back( uni10::Bond( uni10::BD_OUT, qn2_diag ) );
	bd2_lowr.push_back( uni10::Bond( uni10::BD_OUT, qn2_lowr ) );

	// define elemental matrices
	Complex N_elem[] ={ Complex(1.,0.), Complex(0.,0.),\
						Complex(0.,0.), Complex(0.,0.) };

	Complex cp_elem[]={ Complex(0.,0.), Complex(1.,0.),\
						Complex(0.,0.), Complex(0.,0.) };

	Complex cm_elem[]={ Complex(0.,0.), Complex(0.,0.),\
						Complex(1.,0.), Complex(0.,0.) };

	uni10::CUniTensor op;

	if ( name == "id" ) {
		op.assign( bd2_diag );
		op.identity();
	}
	else if ( name == "N" ) {
		op.assign( bd2_diag );
		op.setRawElem( N_elem );
	}
	else if ( name == "cp" ) {
		op.assign( bd2_rais );
		op.setRawElem( cp_elem );
	}
	else if ( name == "cm" ) {
		op.assign( bd2_lowr );
		op.setRawElem( cm_elem );
	}

	op.setName( name );
	return op;
}

//======================================

uni10::CUniTensor opFU1_grd0( std::string name, int num_particle, int size_uc ) {
	///
	// define qnums
	uni10::Qnum q_emp(num_particle);
	uni10::Qnum qf_occ(uni10::PRTF_ODD, size_uc-num_particle, uni10::PRT_EVEN);

	// define bond parameters
	std::vector< uni10::Bond > bd2_diag;
	std::vector< uni10::Bond > bd2_rais;
	std::vector< uni10::Bond > bd2_lowr;

	std::vector< uni10::Qnum > qn2_diag;
	std::vector< uni10::Qnum > qn2_rais;
	std::vector< uni10::Qnum > qn2_lowr;
	qn2_diag.push_back(qf_occ);
	qn2_diag.push_back(-q_emp);
	qn2_rais.push_back(qf_occ * q_emp);	// qf_occ - (-q_emp)
	qn2_lowr.push_back(-qf_occ * -q_emp);	// - [qf_occ - (-q_emp)]

	bd2_diag.push_back( uni10::Bond( uni10::BD_IN, qn2_diag ) );
	bd2_diag.push_back( uni10::Bond( uni10::BD_OUT, qn2_diag ) );

	bd2_rais.push_back( uni10::Bond( uni10::BD_IN, qn2_diag ) );
	bd2_rais.push_back( uni10::Bond( uni10::BD_OUT, qn2_diag ) );
	bd2_rais.push_back( uni10::Bond( uni10::BD_OUT, qn2_rais ) );

	bd2_lowr.push_back( uni10::Bond( uni10::BD_IN, qn2_diag ) );
	bd2_lowr.push_back( uni10::Bond( uni10::BD_OUT, qn2_diag ) );
	bd2_lowr.push_back( uni10::Bond( uni10::BD_OUT, qn2_lowr ) );

	// define elemental matrices
	Complex N_elem[] ={ Complex(1.,0.), Complex(0.,0.),\
						Complex(0.,0.), Complex(0.,0.) };

	Complex cp_elem[]={ Complex(0.,0.), Complex(1.,0.),\
						Complex(0.,0.), Complex(0.,0.) };

	Complex cm_elem[]={ Complex(0.,0.), Complex(0.,0.),\
						Complex(1.,0.), Complex(0.,0.) };

	uni10::CUniTensor op;

	if ( name == "id" ) {
		op.assign( bd2_diag );
		op.identity();
	}
	else if ( name == "N" ) {
		op.assign( bd2_diag );
		op.setRawElem( N_elem );
	}
	else if ( name == "cp" ) {
		op.assign( bd2_rais );
		op.setRawElem( cp_elem );
	}
	else if ( name == "cm" ) {
		op.assign( bd2_lowr );
		op.setRawElem( cm_elem );
	}

	op.setName( name );
	return op;
}

//======================================

uni10::CUniTensor opFU1_spinful( std::string name, int num_particle, int size_uc ) {
	///
	uni10::CUniTensor op;
	uni10::CUniTensor id = opFU1_grd0("id", num_particle, size_uc);
	uni10::CUniTensor ni = opFU1_grd0("N", num_particle, size_uc);
	uni10::CUniTensor cp = opFU1_grd0("cp", num_particle, size_uc);
	uni10::CUniTensor cm = opFU1_grd0("cm", num_particle, size_uc);
	
	if ( name == "id" ) {
		op = uni10::otimes(id, id);
		std::vector<int> labi = {0, 1};
		std::vector<int> labo = {2, 3};
		std::vector<int> lab_op = {0, 1};
		op.combineBond(labi);
		op.combineBond(labo);
		op.setLabel(lab_op);
	}
	else if ( name == "N_up" ) {
		op = uni10::otimes(ni, id);
		std::vector<int> labi = {0, 1};
		std::vector<int> labo = {2, 3};
		std::vector<int> lab_op = {0, 1};
		op.combineBond(labi);
		op.combineBond(labo);
		op.setLabel(lab_op);
	}
	else if ( name == "N_dn" ) {
		op = uni10::otimes(id, ni);
		std::vector<int> labi = {0, 1};
		std::vector<int> labo = {2, 3};
		std::vector<int> lab_op = {0, 1};
		op.combineBond(labi);
		op.combineBond(labo);
		op.setLabel(lab_op);
	}
	else if ( name == "NN" ) {
		op = uni10::otimes(ni, ni);
		std::vector<int> labi = {0, 1};
		std::vector<int> labo = {2, 3};
		std::vector<int> lab_op = {0, 1};
		op.combineBond(labi);
		op.combineBond(labo);
		op.setLabel(lab_op);
	}
	else if ( name == "N" ) {
		op = opFU1_spinful("N_up", num_particle, size_uc)
			+ opFU1_spinful("N_dn", num_particle, size_uc);
	}
	else if ( name == "cp_up" ) {
		op = uni10::otimes(cp, id);
		std::vector<int> lab_cpup = {0, 1, 2, 4, 3};
		op.permuteFm(lab_cpup, 2);
		std::vector<int> labi = {0, 1};
		std::vector<int> labo = {2, 4};
		std::vector<int> lab_op = {0, 1, -1};
		op.combineBond(labi);
		op.combineBond(labo);
		op.setLabel(lab_op);
	}
	else if ( name == "cm_up" ) {
		op = uni10::otimes(cm, id);
		std::vector<int> lab_cmup = {0, 1, 2, 4, 3};
		op.permuteFm(lab_cmup, 2);
		std::vector<int> labi = {0, 1};
		std::vector<int> labo = {2, 4};
		std::vector<int> lab_op = {0, 1, -1};
		op.combineBond(labi);
		op.combineBond(labo);
		op.setLabel(lab_op);
	}
	else if ( name == "cp_dn" ) {
		op = uni10::otimes(id, cp);
		std::vector<int> labi = {0, 1};
		std::vector<int> labo = {2, 3};
		std::vector<int> lab_op = {0, 1, -1};
		op.combineBond(labi);
		op.combineBond(labo);
		op.setLabel(lab_op);
	}
	else if ( name == "cm_dn" ) {
		op = uni10::otimes(id, cm);
		std::vector<int> labi = {0, 1};
		std::vector<int> labo = {2, 3};
		std::vector<int> lab_op = {0, 1, -1};
		op.combineBond(labi);
		op.combineBond(labo);
		op.setLabel(lab_op);
	}

	op.setName( name );
	return op;
}

//======================================

uni10::CUniTensor otimesPM( uni10::CUniTensor op1, uni10::CUniTensor op2, bool swap ) {
	///
	int lab_op1[] = {0, 2, -1};
	int lab_op2[] = {1, 3, -1};
	int lab_out[] = {0, 1, 2, 3};
	if (swap) {
		op1.setLabel(lab_op2);
		op2.setLabel(lab_op1);
	}
	else {
		op1.setLabel(lab_op1);
		op2.setLabel(lab_op2);
	}
	uni10::CUniTensor op = uni10::contractFm(op1, op2, false);
	op.permuteFm( lab_out, 2 );

	return op;
}

//======================================

void importMPOQn( int len, std::string mpo_dir,
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	bool bdry_dummy,
	uni10::CUniTensor bdry_lam0, uni10::CUniTensor bdry_lamN ) {
	///
	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();

	mpo_sy.push_back( uni10::CUniTensor( mpo_dir + "/mpo_ls" ) );
	for (int i = 0; i < len; ++i)
		mpo_sy.push_back( uni10::CUniTensor( mpo_dir + "/mpo_ms" ) );
	mpo_sy.push_back( uni10::CUniTensor( mpo_dir + "/mpo_rs" ) );

	mpo_pl.push_back( uni10::CUniTensor( mpo_dir + "/mpo_lp" ) );
	for (int i = 0; i < len; ++i)
		mpo_pl.push_back( uni10::CUniTensor( mpo_dir + "/mpo_mp" ) );
	mpo_pl.push_back( uni10::CUniTensor( mpo_dir + "/mpo_rp" ) );

	mpo_mi.push_back( uni10::CUniTensor( mpo_dir + "/mpo_lm" ) );
	for (int i = 0; i < len; ++i)
		mpo_mi.push_back( uni10::CUniTensor( mpo_dir + "/mpo_mm" ) );
	mpo_mi.push_back( uni10::CUniTensor( mpo_dir + "/mpo_rm" ) );

	if (bdry_lam0.bondNum() > 0 && bdry_lamN.bondNum() == 0)
		bdry_lamN = bdry_lam0;

	if (bdry_dummy) {
		int dim_mpo = mpo_sy[0].bond()[1].dim();
		uni10::CUniTensor binv_lamN = bondInv(bdry_lamN);
		mpo_sy[0] = mpoDummy( true, dim_mpo, bdry_lam0 );
		mpo_sy[len+1] = mpoDummy( false, dim_mpo, binv_lamN );

		uni10::Bond bp = mpo_pl[0].bond()[3];
		uni10::Bond bm = mpo_mi[0].bond()[3];
		std::vector<uni10::Bond> bdp = bdry_lam0.bond();
		bdp.push_back(bp);
		std::vector<uni10::Bond> bdm = bdry_lam0.bond();
		bdm.push_back(bm);
		mpo_pl[0] = mpoDummy( true, dim_mpo, uni10::CUniTensor(bdp) );
		mpo_mi[0] = mpoDummy( true, dim_mpo, uni10::CUniTensor(bdm) );

		bdp = binv_lamN.bond();
		bdp.push_back(bp);
		bdm = binv_lamN.bond();
		bdm.push_back(bm);
		mpo_pl[len+1] = mpoDummy( false, dim_mpo, uni10::CUniTensor(bdp) );
		mpo_mi[len+1] = mpoDummy( false, dim_mpo, uni10::CUniTensor(bdm) );

		mpo_pl[0].set_zero();
		mpo_mi[0].set_zero();
		mpo_pl[len+1].set_zero();
		mpo_mi[len+1].set_zero();
	}
}

//======================================
