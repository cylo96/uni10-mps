#include <algorithm>
#include <uni10.hpp>
#include <tns-func/tns_const.h>
#include <mps-qn/func_qn_dsum.h>

using std::vector;

//===============================================

uni10::CMatrix matrixSlice(uni10::CMatrix M, vector<int> top_left_idx, vector<int> bot_right_idx) {
	///
	assert(top_left_idx.size() == 2 && bot_right_idx.size() == 2);
	int row = bot_right_idx[0] - top_left_idx[0] + 1;
	int col = bot_right_idx[1] - top_left_idx[1] + 1;
	int mrow = M.row(), mcol = M.col();
	assert(row >= 1 && col >= 1);
	assert(bot_right_idx[0] < mrow && bot_right_idx[1] < mcol);
	
	Complex *ea = M.getElem();
	vector<Complex> elems;
	int start = top_left_idx[0] * mcol + top_left_idx[1];
	for (int i = 0; i < row; ++i) {
		int intm = start+i*mcol;
		elems.insert(elems.end(), ea+intm, ea+intm+col);
	}
	uni10::CMatrix mtx(row, col);
	mtx.setElem(elems);
	return mtx;
}

//===============================================

uni10::CMatrix blocksDirectSum(vector<uni10::CMatrix>& Ms) {
	///
	vector<uni10::Qnum> qni, qno;
	for (int i = 0; i < Ms.size(); ++i) {
		uni10::Qnum q(i);
		qni.insert(qni.end(), Ms[i].row(), q);
		qno.insert(qno.end(), Ms[i].col(), q);
	}

	vector<uni10::Bond> bds = {uni10::Bond(uni10::BD_IN, qni), uni10::Bond(uni10::BD_OUT, qno)};
	uni10::CUniTensor ut(bds);
	for (int i = 0; i < Ms.size(); ++i)
		ut.putBlock(uni10::Qnum(i), Ms[i]);
	
	return ut.getRawElem();
}

//===============================================

uni10::CMatrix blocksConcat(vector<uni10::CMatrix>& Ms, int axis) {
	///
	if (axis != 0 && axis != 1)
		std::cerr << "Wrong axis index." << '\n';
	int row0 = Ms[0].row(), col0 = Ms[0].col();
	bool transpose = (axis == 1 && row0 > 1);

	vector<Complex> elems;
	for (const auto& M: Ms) {
		if (axis == 0)
			assert(M.col() == col0);
		else
			assert(M.row() == row0);
		
		int en = M.elemNum();
		if (transpose) {
			uni10::CMatrix Mt(M);
			Mt.transpose(); 
			Complex *ea = Mt.getElem();
			elems.insert(elems.end(), ea, ea+en);
		}
		else {
			Complex *ea = M.getElem();
			elems.insert(elems.end(), ea, ea+en);
		}
	}
	
	int elem_num = elems.size();
	uni10::CMatrix Bcat;
	if (axis == 0)
		Bcat = uni10::CMatrix(elem_num/col0, col0);
	else if (transpose)
		Bcat = uni10::CMatrix(elem_num/row0, row0);
	else
		Bcat = uni10::CMatrix(row0, elem_num/row0);
	Bcat.setElem(elems);
	if (transpose)
		Bcat.transpose();
		
	return Bcat;
}

//===============================================

uni10::CUniTensor directSum(
	uni10::CUniTensor Ta, uni10::CUniTensor Tb, vector<int> axis, char order_qnum) {
	///
	// check axes for direct sum
	int bn = Ta.bondNum();
	int ibn = Ta.inBondNum();
	int obn = bn - ibn;
	bool easy_dsum = (bn == 2);
	assert(bn == Tb.bondNum() && ibn == Tb.inBondNum());
	if (axis.size() == 0) {
		if (ibn > 0 && obn > 0) {
			axis.push_back(0);
			axis.push_back(ibn);
		}
		else if (bn <= 2)
			for (int i = 0; i < bn; ++i)
				axis.push_back(i);
	}
	if (axis.size() > 2)
		std::cerr << "Cannot perform directSum on more than 2 axes." << '\n';

	// assign qnums and bonds to final tensor
	vector<uni10::Bond> bda = Ta.bond();
	vector<uni10::Bond> bdb = Tb.bond();
	vector<uni10::Bond> bds;
	vector<int> common_ax;
	for (int i = 0; i < bn; ++i) {
		vector<uni10::Qnum> qns = bda[i].Qlist();
		if (std::find(axis.begin(), axis.end(), i) != axis.end()) {
			vector<uni10::Qnum> qnb = bdb[i].Qlist();
			qns.insert(qns.end(), qnb.begin(), qnb.end());
		}
		else
			common_ax.push_back(i);
		bds.push_back(uni10::Bond(bda[i].type(), qns));
	}
	uni10::CUniTensor Tsum(bds);

	// direct sum is no easy task :/
	if (easy_dsum) {
		if (ibn != 1) {
			Ta.permute(1);
			Tb.permute(1);
			Tsum.permute(1);
		}
		std::map<uni10::Qnum, uni10::CMatrix> Ba = Ta.getBlocks();
		std::map<uni10::Qnum, uni10::CMatrix> Bb = Tb.getBlocks();
		vector<uni10::Qnum> blk_qn = Tsum.blockQnum();
		for (const auto& q: blk_qn) {
			vector<uni10::CMatrix> blks;
			if (Ba.count(q))
				blks.push_back(Ba[q]);
			if (Bb.count(q))
				blks.push_back(Bb[q]);
			Tsum.putBlock(q, blocksDirectSum(blks));
		}
		if (ibn != 1)
			Tsum.permute(ibn);
	}
	else if (bn == 1) {
		vector<uni10::CMatrix> blks = {Ta.getRawElem(), Tb.getRawElem()};
		Tsum.setRawElem(blocksConcat(blks, (int)(!ibn)));
	}
	else {
		for (const auto& i: common_ax)
			assert(bda[i] == bdb[i]);
    
		vector<int> lab_a = Ta.label();
		vector<int> lab_b = Tb.label();
		vector<int> lab_t = Tsum.label();
		vector<int> lab_ai, lab_ao;
		vector<int> lab_bi, lab_bo;
		vector<int> lab_ti, lab_to;
		int ln = lab_a.size();
		int cn = common_ax.size();
		for (int i = 0; i < ln; ++i) {
			if (std::find(common_ax.begin(), common_ax.end(), i) != common_ax.end()) {
				lab_ai.push_back(i);
				lab_bi.push_back(i);
				lab_ti.push_back(i);
			}
			else {
				lab_ao.push_back(i);
				lab_bo.push_back(i);
				lab_to.push_back(i);
			}
		}
		lab_ai.insert(lab_ai.end(), lab_ao.begin(), lab_ao.end());
		lab_bi.insert(lab_bi.end(), lab_bo.begin(), lab_bo.end());
		lab_ti.insert(lab_ti.end(), lab_to.begin(), lab_to.end());
		Ta.permute(lab_ai, cn+1);
		Tb.permute(lab_bi, cn+1);
		Tsum.permute(lab_ti, cn+1);
		
		int common_dim = 1;
		for (const auto& i: common_ax)
			common_dim *= bds[i].dim();
		
		uni10::CMatrix Ma = Ta.getRawElem();
		uni10::CMatrix Mb = Tb.getRawElem();
		vector<uni10::CMatrix> Ms;
		int row_ba = bda[axis[0]].dim(), col_ba = Ma.col();
		int row_bb = bdb[axis[0]].dim(), col_bb = Mb.col();
		for (int i = 0; i < common_dim; ++i) {
			uni10::CMatrix Mai = matrixSlice(Ma, vector<int>{i*row_ba, 0}, vector<int>{(i+1)*row_ba-1, col_ba-1});
			uni10::CMatrix Mbi = matrixSlice(Mb, vector<int>{i*row_bb, 0}, vector<int>{(i+1)*row_bb-1, col_bb-1});
			vector<uni10::CMatrix> Msi = {Mai, Mbi};
			if (axis.size() == 1)
				Ms.push_back(blocksConcat(Msi, 0));
			else
				Ms.push_back(blocksDirectSum(Msi));
		}
		uni10::CMatrix Msum = blocksConcat(Ms, 0);
		Tsum.setRawElem(Msum);
		Tsum.permute(lab_t, ibn);
	}

	// order the qnums for clarity
	if (order_qnum == 'a' or order_qnum == 'd') {
		uni10::CUniTensor Ttmp = Tsum;
		vector<uni10::Bond> bdt = Ttmp.bond();
		for (const auto& i: axis) {
			vector<uni10::Qnum> qns = bdt[i].Qlist();
			if (order_qnum == 'a')
				std::sort(qns.begin(), qns.end());
			else
				std::sort(qns.begin(), qns.end(), [](uni10::Qnum a, uni10::Qnum b) { return !(a < b); });
			bds[i] = uni10::Bond(bdt[i].type(), qns);
		}
		Tsum.assign(bds);
		vector<uni10::Qnum> blk_qn = Ttmp.blockQnum();
		for (const auto& q: blk_qn)
			Tsum.putBlock(q, Ttmp.getBlock(q));
	}

	return Tsum;
}
