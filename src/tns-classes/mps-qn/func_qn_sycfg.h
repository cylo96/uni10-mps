#ifndef FUNC_QN_SYCFG_H
#define FUNC_QN_SYCFG_H

bool verify(std::vector<int>& ten_qns, std::vector<std::vector<int>>& adjs);
void findCfgRecur(std::vector<std::vector<int>>& cfgs, int idx_site,
	std::vector<int>& ten_qns, std::vector<std::vector<int>>& adjs,
	std::vector<std::vector<int>>& given);
void findCfgs(std::vector<std::vector<int>>& cfgs, int idx_site,
	std::vector<std::vector<int>>& adjs, std::vector<std::vector<int>>& given);
void findCfgs(std::vector<std::vector<int>>& cfgs, int idx_site,
	std::vector<std::vector<int>>& adjs);
void findLink(std::vector<std::string>& links, std::vector<int>& ten_qns,
	std::vector<std::vector<int>>& adjs, std::vector<int>& order);
void findLink(std::vector<std::string>& links, std::vector<int>& ten_qns,
	std::vector<std::vector<int>>& adjs);

#endif
