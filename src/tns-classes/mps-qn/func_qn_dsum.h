#ifndef FUNC_QN_DSUM_H
#define FUNC_QN_DSUM_H
#include <uni10.hpp>
using std::vector;

uni10::CMatrix matrixSlice(uni10::CMatrix M, vector<int> top_left_idx, vector<int> bot_right_idx);
uni10::CMatrix blocksDirectSum(vector<uni10::CMatrix>& Ms);
uni10::CMatrix blocksConcat(vector<uni10::CMatrix>& Ms, int axis = 0);
uni10::CUniTensor directSum(
	uni10::CUniTensor Ta, uni10::CUniTensor Tb, vector<int> axis = vector<int>(), char order_qnum = NULL);

#endif
