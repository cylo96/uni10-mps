#ifndef CHAINQNIBC_H
#define CHAINQNIBC_H
#include "CanonQnMPS.h"
#include "ChainQnInf.h"

/// ref: PHYSICAL REVIEW B 86, 245107 (2012)
class ChainQnIBC: public CanonQnMPS {

public:
	/// constructor
	ChainQnIBC( int L, int X, std::vector<uni10::Qnum>& q_phys, std::vector<uni10::Qnum>& q_virt );
	ChainQnIBC( int L, int X, std::vector<uni10::Qnum>& q_phys );
	ChainQnIBC();

	void init();
	void randomize();
	void expand(int add_left, int add_right, std::string dirname, int unit_cell = 2);

	void importMPS( std::string dirname, int unit_cell = 2 );
	void import2Semi( std::string dirname, int unit_cell = 2 );
	void import2Semi( std::string dir_l, std::string dir_r, int unit_cell = 2 );
	void breakLink( int imp_loc );

	void loadNetIBC( std::string net_dir = "." );
	void tebdImp( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
		uni10::CUniTensor hwr, uni10::CUniTensor hr, uni10::CUniTensor himp, int idx,
		Complex dt, int steps, int orderTS = 1 );
	void tebd( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
		uni10::CUniTensor hwr, uni10::CUniTensor hr, Complex dt, int steps, int orderTS = 1 );
	void tebd( uni10::CUniTensor hl, uni10::CUniTensor hlw,
		std::vector<uni10::CUniTensor>& hw,
		uni10::CUniTensor hwr, uni10::CUniTensor hr,
		Complex dt, int steps, int orderTS = 1 );
	void tebd( std::vector<uni10::CUniTensor>& hw, uni10::CUniTensor hbl, uni10::CUniTensor hbr,
		ChainQnInf& mps_bkl, ChainQnInf& mps_bkr, Complex dt, int steps, int orderTS = 1 );

	void dmrgImp( int start_loc, int imp_loc,
		std::vector<uni10::CUniTensor>& mpo_sy,
		std::vector<uni10::CUniTensor>& mpo_pl,
		std::vector<uni10::CUniTensor>& mpo_mi,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgImp( int start_loc, int imp_loc, std::string mpo_dir,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgImp( int imp_loc, std::string mpo_dir,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrg( std::string mpo_dir,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgImpU2( int start_loc, int imp_loc,
		std::vector<uni10::CUniTensor>& mpo_sy,
		std::vector<uni10::CUniTensor>& mpo_pl,
		std::vector<uni10::CUniTensor>& mpo_mi,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgImpU2( int start_loc, int imp_loc, std::string mpo_dir,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgImpU2( int imp_loc, std::string mpo_dir,
		int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgU2( std::string mpo_dir,
		int sweeps, int iter_max, double tolerance, bool verbose = false );

	uni10::CUniTensor expVal( uni10::CUniTensor op, int idx );

	uni10::CUniTensor correlation(
		uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 );

	friend class YJuncQnIBC;

private:
	uni10::CNetwork net_evo_lb, net_evo_rb;
	bool net_ibc_loaded = false;
	std::string net_ibc_dir = ".";

	void updateCorner( bool left, uni10::CUniTensor Ub, uni10::CUniTensor Ubw, std::string net_dir = "." );

};

#endif
