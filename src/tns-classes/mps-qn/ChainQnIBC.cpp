#include <iostream>
#include <cstdlib>
#include <time.h>

#include <mps-qn/ChainQnIBC.h>
#include <mps-qn/func_qn_la.h>
#include <mps-qn/func_qn_op.h>
#include <mps-qn/func_qn_net.h>
#include <mps-qn/func_qn_meas.h>
#include <mps-qn/func_qn_blkops.h>
#include <tns-func/func_net.h>

//======================================

ChainQnIBC::ChainQnIBC( int L, int X, std::vector<uni10::Qnum>& q_phys, std::vector<uni10::Qnum>& q_virt ) :
	CanonQnMPS( L, X, q_phys, q_virt ) {
	/// object constructor
	// todo: assert L even
}

//======================================

ChainQnIBC::ChainQnIBC( int L, int X, std::vector<uni10::Qnum>& q_phys ) :
	CanonQnMPS( L, X, q_phys ) {
	/// object constructor
	qvirt.push_back( uni10::Qnum(0) );
}

//======================================

ChainQnIBC::ChainQnIBC() : CanonQnMPS( 1, 1 ) {
	/// object constructor
}

//======================================

void ChainQnIBC::init() {
	///
	if (gamma.size() > 0)
		gamma.clear();
	if (lambda.size() > 0)
		lambda.clear();

	gamma.push_back( initGamma(qvirt, qphys) );
	lambda.push_back( initLambda(qvirt) );

	bool fermi = qphys[0].isFermionic();
	bool u1 = ((qphys[0].U1() != 0) || (qphys[qphys.size()-1].U1() != 0 ));

	if (fermi && u1) {
		// assert lat_size >= 4
		std::vector<uni10::Qnum> qin1 = gamma[0].bond()[2].Qlist();
		gamma.push_back( initGamma(qin1, qphys) );
		lambda.push_back( initLambda(qin1) );
		std::vector<uni10::Qnum> qin2 = gamma[1].bond()[2].Qlist();
		gamma.push_back( initGamma(qin2, qphys) );
		lambda.push_back( initLambda(qin2) );
		std::vector<uni10::Qnum> qin3 = gamma[2].bond()[2].Qlist();

		for (int i = 3; i < lat_size; ++i) {
			if (i%4 == 1) {
				gamma.push_back( initGamma(qin1, qphys, qin2) );
				lambda.push_back( initLambda(qin1) );
			}
			else if (i%4 == 2) {
				gamma.push_back( initGamma(qin2, qphys, qin3) );
				lambda.push_back( initLambda(qin2) );
			}
			else if (i%4 == 3) {
				gamma.push_back( initGamma(qin3, qphys, qvirt) );
				lambda.push_back( initLambda(qin3) );
			}
			else {
				gamma.push_back( initGamma(qvirt, qphys, qin1) );
				lambda.push_back( initLambda(qvirt) );
			}
		}
		// lambda vec is one elem longer than gam
		lambda.push_back( initLambda(qvirt) );
	}

	else {
		std::vector<uni10::Qnum> qin = gamma[0].bond()[2].Qlist();

		for (int i = 1; i < lat_size; ++i) {
			if (i%2) {
				gamma.push_back( initGamma(qin, qphys, qvirt) );
				lambda.push_back( initLambda(qin) );
			}
			else {
				gamma.push_back( initGamma(qvirt, qphys, qin) );
				lambda.push_back( initLambda(qvirt) );
			}
		}
		// lambda vec is one elem longer than gam
		lambda.push_back( initLambda(qvirt) );
	}
}

//======================================

void ChainQnIBC::randomize() {
	/// randomize a complex MPS having only real part
	ChainQnIBC::init();

	std::srand( time(NULL) );

	for (int i = 0; i < lat_size; ++i)
		gamma[i] = randTQn( gamma[i] );
	for (int i = 0; i < lat_size+1; ++i)
		lambda[i] = randTQn( lambda[i] );
}

//======================================

void ChainQnIBC::expand(int add_left, int add_right, std::string dirname, int unit_cell) {
	/// expand the MPS with add_left sites on the left, add_right sites on the right
	// todo: raise exception if gam/lam vectors empty
	if (gamma.size() == 0 || lambda.size() == 0)
		setSize( lat_size + add_left + add_right );
	else {
		std::vector< uni10::CUniTensor > ext_gam;
		std::vector< uni10::CUniTensor > ext_lam;
		for (int idx = 0; idx < unit_cell; ++idx) {
			ext_gam.push_back( uni10::CUniTensor( dirname + "/gamma_" + std::to_string((long long)idx ) ) );
			ext_lam.push_back( uni10::CUniTensor( dirname + "/lambda_" + std::to_string((long long)idx ) ) );
		}

		for (int i = 0; i < add_left; i+=unit_cell) {
			for (int j = 0; j < unit_cell; ++j) {
				gamma.insert( gamma.begin(), ext_gam[ unit_cell - j - 1 ] );
				lambda.insert( lambda.begin(), ext_lam[ unit_cell - j - 1 ] );
			}
		}
		for (int i = 0; i < add_right; i+=unit_cell) {
			for (int j = 0; j < unit_cell; ++j) {
				gamma.push_back( ext_gam[ j ] );
				lambda.push_back( ext_lam[ (j+1)%unit_cell ] );
			}
		}
		setSize( lat_size + add_left + add_right );

		ext_gam.clear();
		ext_lam.clear();
	}
}

//======================================

void ChainQnIBC::importMPS( std::string dirname, int unit_cell ) {
	///
	// fit_chi in CanonMPS doesn't work for Qn classes => set false
	CanonMPS::importMPS( dirname, unit_cell, true, false );
}

//======================================

void ChainQnIBC::import2Semi( std::string dirname, int unit_cell ) {
	///
	// presume both L and L/2 are even
	int L = lat_size;
	setSize( L/2 );
	CanonMPS::importMPS( dirname, unit_cell, true, false );

	uni10::CUniTensor lam_last = lambda[ L/2 ];
	lambda.pop_back();

	for (int i = 0; i < L/2; ++i) {
		putGamma( gamma[i] );
		putLambda( lambda[i] );
	}
	putLambda( lam_last );

	for (int i = 0; i < L/2; ++i) {
		putGamma( bondInvGamU1( gamma[L-1-i] ), i );
		putLambda( lambda[L-i], i );
	}

	setSize(L);
}

//======================================

void ChainQnIBC::import2Semi( std::string dir_l, std::string dir_r, int unit_cell ) {
	///
	int L = lat_size;
	setSize( L/2 );
	CanonMPS::importMPS( dir_l, unit_cell, true, false );

	CanonMPS mps_right( L/2, 1, 1 );
	mps_right.importMPS( dir_r, unit_cell, true, false );

	for (int i = 0; i < L/2; ++i) {
		putGamma( mps_right.getGamma(i) );
		putLambda( mps_right.getLambda(i+1) );
	}

	setSize(L);
	mps_right.clear();
}

//======================================

void ChainQnIBC::breakLink( int imp_loc ) {
	///
	SchmidtVal sv_l0 = entangleSpecQn( lambda[imp_loc], 1 )[0];

	uni10::CMatrix uni = lambda[imp_loc].getBlock( sv_l0.qn );
	uni.set_zero();
	uni.at(0, 0) = Complex(1.0, 0.0);
	lambda[imp_loc].set_zero();
	lambda[imp_loc].putBlock( sv_l0.qn, uni );
}

//======================================

void ChainQnIBC::loadNetIBC( std::string net_dir ) {
	///
	net_ibc_dir = net_dir;
	net_evo_lb = uni10::CNetwork( net_ibc_dir + "/evo_lb.net" );
	net_evo_rb = uni10::CNetwork( net_ibc_dir + "/evo_rb.net" );
  net_ibc_loaded = true;
}

//======================================

uni10::CUniTensor ChainQnIBC::expVal( uni10::CUniTensor op, int idx ) {
	/// return expectation value (a contracted uni10) of an 1-site/2-site operator on site idx
	uni10::CUniTensor exp_val;

	uni10::CUniTensor lvec( lambda[0].bond() );
	uni10::CUniTensor rvec( lambda[lat_size].bond() );
	lvec.identity();
	rvec.identity();

	int bno = op.bondNum();
	if ( bno < 2 || bno > 4 ) {
		std::cerr << "In ChainQnIBC::expVal : Unsupported Operator.\n";
		return exp_val;
	}
	if ( bno%2 && op.bond()[bno-1].dim() == 1 ) {
		std::cerr << "In ChainQnIBC::expVal : Non-symmetric operator.\n";
		return exp_val;
	}

	uni10::CUniTensor onsite;
	if (bno == 2)
		onsite = netLGL( lambda[idx], gamma[idx], lambda[idx+1] );
	else if (bno == 4)
		onsite = netLGLGL( lambda[idx], gamma[idx], lambda[idx+1], gamma[idx+1], lambda[idx+2] );

	if (idx > 0)
		lvec = buildLRVecQn( idx-1, lvec, gamma, lambda, true );
	lvec = buildLRVecQn( lvec, onsite, op, true );
	if (idx < lat_size-(bno/2))
		rvec = buildLRVecQn( idx+(bno/2), rvec, gamma, lambda, false );
	exp_val = contrLRVecQn( lvec, rvec );

	return exp_val;
}

//======================================

uni10::CUniTensor ChainQnIBC::correlation(
	uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 ) {
	///
	// todo: assert idx2 - idx1 >= op.bondNum
	// todo: assert lat_size - idx2 >= op.bondNum
	uni10::CUniTensor corr;

	uni10::CUniTensor lvec( lambda[0].bond() );
	uni10::CUniTensor rvec( lambda[lat_size].bond() );
	lvec.identity();
	rvec.identity();

	int bno1 = op1.bondNum();
	int bno2 = op2.bondNum();
	if ( bno1 != bno2 ) {
		std::cerr << "In ChainQnIBC::correlation : Bonds of two operators do not match.\n";
		return corr;
	}
	if ( bno1 < 2 || bno1 > 4 ) {
		std::cerr << "In ChainQnIBC::correlation : Unsupported Operator.\n";
		return corr;
	}

	uni10::CUniTensor onsite1, onsite2;
	if (bno1 == 2 || bno1 == 3) {
		onsite1 = netLG( lambda[idx1], gamma[idx1] );
		onsite2 = netLGL( lambda[idx2], gamma[idx2], lambda[idx2+1] );
	}
	else if (bno1 == 4) {
		onsite1 = netLGLG( lambda[idx1], gamma[idx1], lambda[idx1+1], gamma[idx1+1] );
		onsite2 = netLGLGL( lambda[idx2], gamma[idx2], lambda[idx2+1], gamma[idx2+1], lambda[idx2+2] );
	}

	if (idx1 > 0)
		lvec = buildLRVecQn( idx1-1, lvec, gamma, lambda, true );

	lvec = buildLRVecQn( lvec, onsite1, op1, true );

	for (int i = idx1+bno1/2; i < idx2; ++i) {
		uni10::CUniTensor cell = netLG( lambda[i], gamma[i] );
		lvec = buildLRVecQn( lvec, cell, true );
	}

	lvec = buildLRVecQn( lvec, onsite2, op2, true );

	if (idx2 < lat_size-(bno2/2))
		rvec = buildLRVecQn( idx2+(bno2/2), rvec, gamma, lambda, false );

	corr = contrLRVecQn( lvec, rvec );

	return corr;
}

//======================================

void ChainQnIBC::updateCorner( bool left, uni10::CUniTensor Ub, uni10::CUniTensor Ubw, std::string net_dir ) {
	/// evolve and update corner tensor
	uni10::CNetwork net;
	if (!net_ibc_loaded)
		loadNetIBC(net_dir);

	if (left) {
		net = net_evo_lb;
		net.putTensor("lamL", lambda[0]);
		net.putTensor("gamL", gamma[0]);
		net.putTensor("Ub", Ub);
		net.putTensor("Ubw", Ubw);
		uni10::CUniTensor theta = net.launch(false);
		gamma[0] = netLG( tenInvQn(lambda[0]), theta );
	}
	else {
		net = net_evo_rb;
		net.putTensor("lamW", lambda[lat_size-1]);
		net.putTensor("gamR", gamma[lat_size-1]);
		net.putTensor("lamR", lambda[lat_size]);
		net.putTensor("Ub", Ub);
		net.putTensor("Ubw", Ubw);
		uni10::CUniTensor theta = net.launch(false);
		gamma[lat_size-1] = netGL( netLG( tenInvQn(lambda[lat_size-1]), theta.permute(2) ), tenInvQn(lambda[lat_size]) );
	}
}

//======================================

void ChainQnIBC::tebdImp( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
	uni10::CUniTensor hwr, uni10::CUniTensor hr, uni10::CUniTensor himp, int idx,
	Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS, involving multipications of lambda^-1
	// todo: handle the dimension trncation/expansion to fit chi_max
	// time evolution operator
	uni10::CUniTensor Ul   = takeExpQn( I * dt, hl );
	uni10::CUniTensor Ulw  = takeExpQn( I * dt, hlw );
	uni10::CUniTensor Uw   = takeExpQn( I * dt, hw );
	uni10::CUniTensor Uwr  = takeExpQn( I * dt, hwr );
	uni10::CUniTensor Ur   = takeExpQn( I * dt, hr );
	uni10::CUniTensor Uimp = takeExpQn( I * dt, himp );

	if (orderTS == 1) {
		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				if (n == idx)
					evol2Site(Uimp, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
				else
					evol2Site(Uw, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}

			// evolve and update corners
			updateCorner( true, Ul, Ulw, net_ibc_dir );
			updateCorner( false, Ur, Uwr, net_ibc_dir );

			// evolve and update even sites
			// sweep left to right
			for (int n = 1; n < lat_size-1; n+=2) {
				if (n == idx)
					evol2Site(Uimp, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
				else
					evol2Site(Uw, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}
		}
	}
	else if (orderTS == 2) {
		uni10::CUniTensor Ul2   = takeExpQn( 0.5 * I * dt, hl );
		uni10::CUniTensor Ulw2  = takeExpQn( 0.5 * I * dt, hlw );
		uni10::CUniTensor Uw2   = takeExpQn( 0.5 * I * dt, hw );
		uni10::CUniTensor Uwr2  = takeExpQn( 0.5 * I * dt, hwr );
		uni10::CUniTensor Ur2   = takeExpQn( 0.5 * I * dt, hr );
		uni10::CUniTensor Uimp2 = takeExpQn( 0.5 * I * dt, himp );

		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				if (n == idx)
					evol2Site(Uimp2, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
				else
					evol2Site(Uw2, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}

			// evolve and update corners
			updateCorner( true, Ul2, Ulw2, net_ibc_dir );
			updateCorner( false, Ur2, Uwr2, net_ibc_dir );

			// evolve and update even sites
			for (int n = 1; n < lat_size-1; n+=2) {
				if (n == idx)
					evol2Site(Uimp, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
				else
					evol2Site(Uw, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}

			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				if (n == idx)
					evol2Site(Uimp2, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
				else
					evol2Site(Uw2, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}

			// evolve and update corners
			updateCorner( true, Ul2, Ulw2, net_ibc_dir );
			updateCorner( false, Ur2, Uwr2, net_ibc_dir );
		}
	}
}

//======================================

void ChainQnIBC::tebd( uni10::CUniTensor hl, uni10::CUniTensor hlw, uni10::CUniTensor hw,
	uni10::CUniTensor hwr, uni10::CUniTensor hr, Complex dt, int steps, int orderTS ) {
	///
	tebdImp( hl, hlw, hw, hwr, hr, hw, 0, dt, steps, orderTS );
}

//======================================

void ChainQnIBC::tebd( uni10::CUniTensor hl, uni10::CUniTensor hlw,
	std::vector<uni10::CUniTensor>& hw, uni10::CUniTensor hwr, uni10::CUniTensor hr,
	Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS, involving multipications of lambda^-1
	// todo: handle the dimension trncation/expansion to fit chi_max
	// time evolution operator
	uni10::CUniTensor Ul   = takeExpQn( I * dt, hl );
	uni10::CUniTensor Ulw  = takeExpQn( I * dt, hlw );
	uni10::CUniTensor Uw;
	uni10::CUniTensor Uwr  = takeExpQn( I * dt, hwr );
	uni10::CUniTensor Ur   = takeExpQn( I * dt, hr );

	if (orderTS == 1) {
		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				Uw = takeExpQn( I * dt, hw[n] );
				evol2Site(Uw, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}

			// evolve and update corners
			updateCorner( true, Ul, Ulw, net_ibc_dir );
			updateCorner( false, Ur, Uwr, net_ibc_dir );

			// evolve and update even sites
			// sweep left to right
			for (int n = 1; n < lat_size-1; n+=2) {
				Uw = takeExpQn( I * dt, hw[n] );
				evol2Site(Uw, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}
		}
	}
	else if (orderTS == 2) {
		uni10::CUniTensor Ul2   = takeExpQn( 0.5 * I * dt, hl );
		uni10::CUniTensor Ulw2  = takeExpQn( 0.5 * I * dt, hlw );
		uni10::CUniTensor Uw2;
		uni10::CUniTensor Uwr2  = takeExpQn( 0.5 * I * dt, hwr );
		uni10::CUniTensor Ur2   = takeExpQn( 0.5 * I * dt, hr );

		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				Uw2 = takeExpQn( 0.5 * I * dt, hw[n] );
				evol2Site(Uw2, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}

			// evolve and update corners
			updateCorner( true, Ul2, Ulw2, net_ibc_dir );
			updateCorner( false, Ur2, Uwr2, net_ibc_dir );

			// evolve and update even sites
			for (int n = 1; n < lat_size-1; n+=2) {
				Uw = takeExpQn( I * dt, hw[n] );
				evol2Site(Uw, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}

			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				Uw2 = takeExpQn( 0.5 * I * dt, hw[n] );
				evol2Site(Uw2, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}

			// evolve and update corners
			updateCorner( true, Ul2, Ulw2, net_ibc_dir );
			updateCorner( false, Ur2, Uwr2, net_ibc_dir );
		}
	}
}

//======================================

void ChainQnIBC::tebd( std::vector<uni10::CUniTensor>& hw, uni10::CUniTensor hbl, uni10::CUniTensor hbr,
	ChainQnInf& mps_bkl, ChainQnInf& mps_bkr, Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS, involving multipications of lambda^-1
	// todo: handle the dimension trncation/expansion to fit chi_max
	// time evolution operator
	uni10::CUniTensor Uw;
	uni10::CUniTensor Ubl = takeExpQn( I * dt, hbl );
	uni10::CUniTensor Ubr = takeExpQn( I * dt, hbr );
	uni10::CUniTensor gtmp, ltmp, thl, thr;

	if (orderTS == 1) {
		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				Uw = takeExpQn( I * dt, hw[n] );
				evol2Site(Uw, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}

			// evolve and update bdry -- Nishino 2015 paper Fig.A2
			// A2.1
			evol2Site(Ubl, mps_bkl.lambda[0], mps_bkl.gamma[0], mps_bkl.lambda[1], mps_bkl.gamma[1], mps_bkl.lambda[0]);
			evol2Site(Ubr, mps_bkr.lambda[0], mps_bkr.gamma[0], mps_bkr.lambda[1], mps_bkr.gamma[1], mps_bkr.lambda[0]);
			// A2.2 ~ A2.3
			ltmp = mps_bkl.lambda[1]; gtmp = mps_bkl.gamma[1];
			thl = tensorOp(netLGLGL(ltmp, gtmp, lambda[0], gamma[0], lambda[1]), Ubl);
			gtmp = mps_bkr.gamma[0]; ltmp = mps_bkr.lambda[1];
			thr = tensorOp(netLGLGL(lambda[lat_size-1], gamma[lat_size-1], lambda[lat_size], gtmp, ltmp), Ubr);
			// A2.4
			evol2Site(Ubl, mps_bkl.lambda[1], mps_bkl.gamma[1], mps_bkl.lambda[0], mps_bkl.gamma[0], mps_bkl.lambda[1]);
			evol2Site(Ubr, mps_bkr.lambda[1], mps_bkr.gamma[1], mps_bkr.lambda[0], mps_bkr.gamma[0], mps_bkr.lambda[1]);
			// A2.5
			gtmp = netLG(mps_bkl.lambda[1], mps_bkl.gamma[1]); gtmp.cTranspose();
			thl.setLabel(std::vector<int> {0, 100, 101, 1}); gtmp.setLabel(std::vector<int> {-1, 0, 100});
			thl = gtmp * thl; thl.permute(2);
			gtmp = netGL(mps_bkr.gamma[0], mps_bkr.lambda[1]); gtmp.cTranspose();
			thr.setLabel(std::vector<int> {0, 100, 101, 1}); gtmp.setLabel(std::vector<int> {1, 2, 101});
			thr = thr * gtmp;
			// back to gamma lambda form
			lambda[0] = mps_bkl.lambda[0]; lambda[lat_size] = mps_bkr.lambda[0];
			gamma[0] = netGL( netLG( tenInvQn(lambda[0]), thl ), tenInvQn(lambda[1]) );
			gamma[lat_size-1] = netGL( netLG( tenInvQn(lambda[lat_size-1]), thr ), tenInvQn(lambda[lat_size]) );

			// evolve and update even sites
			for (int n = 1; n < lat_size-1; n+=2) {
				Uw = takeExpQn( I * dt, hw[n] );
				evol2Site(Uw, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}
		}
	}
	else if (orderTS == 2) {
		uni10::CUniTensor Uw2;
		uni10::CUniTensor Ubl2 = takeExpQn( 0.5 * I * dt, hbl );
		uni10::CUniTensor Ubr2 = takeExpQn( 0.5 * I * dt, hbr );

		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				Uw2 = takeExpQn( 0.5 * I * dt, hw[n] );
				evol2Site(Uw2, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}

			// evolve and update bdry -- Nishino 2015 paper Fig.A2
			// A2.1
			evol2Site(Ubl2, mps_bkl.lambda[0], mps_bkl.gamma[0], mps_bkl.lambda[1], mps_bkl.gamma[1], mps_bkl.lambda[0]);
			evol2Site(Ubr2, mps_bkr.lambda[0], mps_bkr.gamma[0], mps_bkr.lambda[1], mps_bkr.gamma[1], mps_bkr.lambda[0]);
			// A2.2 ~ A2.3
			ltmp = mps_bkl.lambda[1]; gtmp = mps_bkl.gamma[1];
			thl = tensorOp(netLGLGL(ltmp, gtmp, lambda[0], gamma[0], lambda[1]), Ubl);
			gtmp = mps_bkr.gamma[0]; ltmp = mps_bkr.lambda[1];
			thr = tensorOp(netLGLGL(lambda[lat_size-1], gamma[lat_size-1], lambda[lat_size], gtmp, ltmp), Ubr);
			// A2.4
			evol2Site(Ubl, mps_bkl.lambda[1], mps_bkl.gamma[1], mps_bkl.lambda[0], mps_bkl.gamma[0], mps_bkl.lambda[1]);
			evol2Site(Ubr, mps_bkr.lambda[1], mps_bkr.gamma[1], mps_bkr.lambda[0], mps_bkr.gamma[0], mps_bkr.lambda[1]);
			// A2.5
			gtmp = netLG(mps_bkl.lambda[1], mps_bkl.gamma[1]); gtmp.cTranspose();
			thl.setLabel(std::vector<int> {0, 100, 101, 1}); gtmp.setLabel(std::vector<int> {-1, 0, 100});
			thl = gtmp * thl; thl.permute(2);
			gtmp = netGL(mps_bkr.gamma[0], mps_bkr.lambda[1]); gtmp.cTranspose();
			thr.setLabel(std::vector<int> {0, 100, 101, 1}); gtmp.setLabel(std::vector<int> {1, 2, 101});
			thr = thr * gtmp;
			// back to gamma lambda form
			lambda[0] = mps_bkl.lambda[0]; lambda[lat_size] = mps_bkr.lambda[0];
			gamma[0] = netGL( netLG( tenInvQn(lambda[0]), thl ), tenInvQn(lambda[1]) );
			gamma[lat_size-1] = netGL( netLG( tenInvQn(lambda[lat_size-1]), thr ), tenInvQn(lambda[lat_size]) );
			// A2.1
			evol2Site(Ubl2, mps_bkl.lambda[0], mps_bkl.gamma[0], mps_bkl.lambda[1], mps_bkl.gamma[1], mps_bkl.lambda[0]);
			evol2Site(Ubr2, mps_bkr.lambda[0], mps_bkr.gamma[0], mps_bkr.lambda[1], mps_bkr.gamma[1], mps_bkr.lambda[0]);

			// evolve and update even sites
			for (int n = 1; n < lat_size-1; n+=2) {
				Uw = takeExpQn( I * dt, hw[n] );
				evol2Site(Uw, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}

			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {
				Uw2 = takeExpQn( 0.5 * I * dt, hw[n] );
				evol2Site(Uw2, lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2]);
			}
		}
	}
}

//======================================

void ChainQnIBC::dmrgImp( int start_loc, int imp_loc,
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	int update_size = 1;
	uni10::CUniTensor psi;
	double eng = 0.0;

	std::vector<uni10::CUniTensor> mpoH_sy;
	std::vector<uni10::CUniTensor> mpoH_pl;
	std::vector<uni10::CUniTensor> mpoH_mi;
	std::vector<uni10::CUniTensor> buff_sy;
	std::vector<uni10::CUniTensor> buff_pl;
	std::vector<uni10::CUniTensor> buff_mi;

	for (int s = 0; s < sweeps; ++s) {
		//
		if (s%2 == 0) {
			// sweep from left to right
			int start = (s == 0)? start_loc : 0;
			for (int i = start; i <= lat_size-update_size; ++i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.bondNum() );
				int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );
				psi.permute(2);

				if (i == lat_size-1)
					gamma[i] = netLG( tenInvQn(lambda[i]), netGL( psi, tenInvQn(lambda[i+1]) ) );
				else {
					uni10::CUniTensor vi;
					mps1SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], vi );
					gamma[i+1] = netLG( vi, gamma[i+1] );
				}

				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, true );
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size-update_size; i >= 0; --i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.bondNum() );
				int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );
				psi.permute(1);

				if (i == 0)
					gamma[i] = netGL( netLG( tenInvQn(lambda[i]), psi ), tenInvQn(lambda[i+1]) );
				else {
					uni10::CUniTensor ui;
					mps1SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], ui );
					gamma[i-1] = netGL( gamma[i-1], ui );
				}

				if (verbose) {
					std::cout << i << '\t' << lat_size-i << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( lambda[i] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, false );
			}
		}
	}

	mpoH_sy.clear();
	mpoH_pl.clear();
	mpoH_mi.clear();
	buff_sy.clear();
	buff_pl.clear();
	buff_mi.clear();
}

//======================================

void ChainQnIBC::dmrgImp( int start_loc, int imp_loc, std::string mpo_dir,
	int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( lat_size, mpo_dir, mpo_sy, mpo_pl, mpo_mi );

	if (imp_loc > 0) {
		mpo_sy[imp_loc+1] = uni10::CUniTensor( mpo_dir + "/mpo_imp_s" );
		mpo_pl[imp_loc+1] = uni10::CUniTensor( mpo_dir + "/mpo_imp_p" );
		mpo_mi[imp_loc+1] = uni10::CUniTensor( mpo_dir + "/mpo_imp_m" );
	}

	dmrgImp( start_loc, imp_loc, mpo_sy, mpo_pl, mpo_mi, sweeps, iter_max, tolerance, verbose );

	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();
}

//======================================

void ChainQnIBC::dmrgImp( int imp_loc, std::string mpo_dir,
	int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	dmrgImp( 0, imp_loc, mpo_dir, sweeps, iter_max, tolerance, verbose );
}

//======================================

void ChainQnIBC::dmrg( std::string mpo_dir,
	int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	dmrgImp( -10, mpo_dir, sweeps, iter_max, tolerance, verbose );
}

//======================================

void ChainQnIBC::dmrgImpU2( int start_loc, int imp_loc,
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	int update_size = 2;
	uni10::CUniTensor psi;
	double eng = 0.0;

	std::vector<uni10::CUniTensor> mpoH_sy;
	std::vector<uni10::CUniTensor> mpoH_pl;
	std::vector<uni10::CUniTensor> mpoH_mi;
	std::vector<uni10::CUniTensor> buff_sy;
	std::vector<uni10::CUniTensor> buff_pl;
	std::vector<uni10::CUniTensor> buff_mi;

	for (int s = 0; s < sweeps; ++s) {
		//
		if (s%2 == 0) {
			// sweep from left to right
			int start = (s == 0)? start_loc : 0;
			for (int i = start; i <= lat_size-update_size; ++i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.bondNum() );
				int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

				mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, true );
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size-update_size; i >= 0; --i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.bondNum() );
				int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

				mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, false );
			}
		}
	}

	mpoH_sy.clear();
	mpoH_pl.clear();
	mpoH_mi.clear();
	buff_sy.clear();
	buff_pl.clear();
	buff_mi.clear();
}

//======================================

void ChainQnIBC::dmrgImpU2( int start_loc, int imp_loc, std::string mpo_dir,
	int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( lat_size, mpo_dir, mpo_sy, mpo_pl, mpo_mi );

	if (imp_loc > 0) {
		mpo_sy[imp_loc+1] = uni10::CUniTensor( mpo_dir + "/mpo_imp_s" );
		mpo_pl[imp_loc+1] = uni10::CUniTensor( mpo_dir + "/mpo_imp_p" );
		mpo_mi[imp_loc+1] = uni10::CUniTensor( mpo_dir + "/mpo_imp_m" );
	}

	dmrgImpU2( start_loc, imp_loc, mpo_sy, mpo_pl, mpo_mi, sweeps, iter_max, tolerance, verbose );

	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();
}

//======================================

void ChainQnIBC::dmrgImpU2( int imp_loc, std::string mpo_dir,
	int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	dmrgImpU2( 0, imp_loc, mpo_dir, sweeps, iter_max, tolerance, verbose );
}

//======================================

void ChainQnIBC::dmrgU2( std::string mpo_dir,
	int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	dmrgImpU2( -10, mpo_dir, sweeps, iter_max, tolerance, verbose );
}

//======================================
