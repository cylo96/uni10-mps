#include <iostream>
#include <cstdlib>
#include <time.h>

#include <mps-qn/ChainQnSemiInf.h>
#include <mps-qn/func_qn_la.h>
#include <mps-qn/func_qn_op.h>
#include <mps-qn/func_qn_net.h>
#include <mps-qn/func_qn_meas.h>
#include <mps-qn/func_qn_blkops.h>
#include <tns-func/func_net.h>

//======================================

ChainQnSemiInf::ChainQnSemiInf( int L, int X,
	std::vector<uni10::Qnum>& q_phys, uni10::Qnum q_vin, std::vector<uni10::Qnum>& q_vout ) :
	CanonQnMPS( L, X, q_phys ) {
	/// object constructor
	// Why bond dim at boundary >= 3, not 1? Because of the stupid only-symm-block-survive design !!
	uni10::Qnum q1 = q_phys[0];
	uni10::Qnum q2 = q_phys[q_phys.size()-1];

	qvirt.push_back( q1 * (-q2) * q_vin );
	qvirt.push_back( q_vin );
	qvirt.push_back( -q1 * q2 * q_vin );

	qvout = q_vout;
}

//======================================

ChainQnSemiInf::ChainQnSemiInf( int L, int X, std::vector<uni10::Qnum>& q_phys, uni10::Qnum q_vin ) :
	CanonQnMPS( L, X, q_phys ) {
	/// object constructor
	// Why bond dim at boundary >= 3, not 1? Because of the stupid only-symm-block-survive design !!
	uni10::Qnum q_vout = q_vin;

	uni10::Qnum q1 = q_phys[0];
	uni10::Qnum q2 = q_phys[q_phys.size()-1];

	qvirt.push_back( q1 * (-q2) * q_vin );
	qvirt.push_back( q_vin );
	qvirt.push_back( -q1 * q2 * q_vin );

	qvout.push_back( q1 * (-q2) * q_vout );
	qvout.push_back( q_vout );
	qvout.push_back( -q1 * q2 * q_vout );
}

//======================================

ChainQnSemiInf::ChainQnSemiInf( int L, int X, std::vector<uni10::Qnum>& q_phys ) :
	CanonQnMPS( L, X, q_phys ) {
	/// object constructor
	// Why bond dim at boundary >= 3, not 1? Because of the stupid only-symm-block-survive design !!
	uni10::Qnum q1 = q_phys[0];
	uni10::Qnum q2 = q_phys[q_phys.size()-1];

	qvirt.push_back( q1 * (-q2) );
	qvirt.push_back( uni10::Qnum(0) );
	qvirt.push_back( -q1 * q2 );

	qvout.push_back( q1 * (-q2) );
	qvout.push_back( uni10::Qnum(0) );
	qvout.push_back( -q1 * q2 );
}

//======================================

void ChainQnSemiInf::reverse() { reverse_bdry = true; }

//======================================

void ChainQnSemiInf::init() {
	///
	if (gamma.size() > 0)
		gamma.clear();
	if (lambda.size() > 0)
		lambda.clear();

	gamma.push_back( initGamma(qvirt, qphys) );
	lambda.push_back( initLambda(qvirt) );

	std::vector<uni10::Qnum> qin = gamma[0].bond()[2].Qlist();

	for (int i = 1; i < lat_size-1; ++i) {
		if (i%2) {
			gamma.push_back( initGamma(qin, qphys, qvirt) );
			lambda.push_back( initLambda(qin) );
		}
		else {
			gamma.push_back( initGamma(qvirt, qphys, qin) );
			lambda.push_back( initLambda(qvirt) );
		}
	}

	// assert lat_size even
	lambda.push_back( initLambda(qin) );
	gamma.push_back( initGamma(qin, qphys, qvout) );
	// lambda vec is one elem longer than gam
	lambda.push_back( initLambda(qvout) );

	if (reverse_bdry) {
		std::vector<uni10::CUniTensor> gams, lams;
		for (int i = 0; i < lat_size; ++i) {
			gams.push_back(gamma[i]);
			lams.push_back(lambda[i]);
		}
		lams.push_back(lambda[lat_size]);
		for (int i = 0; i < lat_size; ++i) {
			gamma[i] = bondInvGamU1( gams[lat_size-1-i] );
			lambda[i] = lams[lat_size-i];
		}
		lambda[lat_size] = lams[0];
	}
}

//======================================

void ChainQnSemiInf::randomize() {
	/// randomize a complex MPS having only real part
	ChainQnSemiInf::init();

	std::srand( time(NULL) );

	for (int i = 0; i < lat_size; ++i)
		gamma[i] = randTQn( gamma[i] );
	for (int i = 0; i < lat_size+1; ++i)
		lambda[i] = randTQn( lambda[i] );

	int bdry = (reverse_bdry)? lat_size : 0;
	uni10::CMatrix uni = lambda[bdry].getBlock( qvirt[1] );
	uni.set_zero();
	uni.at(0, 0) = Complex(1.0, 0.0);
	lambda[bdry].set_zero();
	lambda[bdry].putBlock( qvirt[1], uni );
}

//======================================

void ChainQnSemiInf::expand( int add_tail, std::string dirname, int unit_cell ) {
	/// expand the MPS with add_tail sites on the tail
	// todo: raise exception if gam/lam vectors empty
	if (gamma.size() == 0 || lambda.size() == 0)
		setSize( lat_size + add_tail );
	else {
		std::vector< uni10::CUniTensor > ext_gam;
		std::vector< uni10::CUniTensor > ext_lam;
		for (int idx = 0; idx < unit_cell; ++idx) {
			ext_gam.push_back( uni10::CUniTensor( dirname + "/gamma_" + std::to_string((long long)idx ) ) );
			ext_lam.push_back( uni10::CUniTensor( dirname + "/lambda_" + std::to_string((long long)idx ) ) );
		}

		if (reverse_bdry) {
			for (int i = 0; i < add_tail; i+=unit_cell) {
				for (int j = 0; j < unit_cell; ++j) {
					gamma.insert( gamma.begin(), ext_gam[ unit_cell - j - 1 ] );
					lambda.insert( lambda.begin(), ext_lam[ unit_cell - j - 1 ] );
				}
			}
		}
		else {
			for (int i = 0; i < add_tail; i+=unit_cell) {
				for (int j = 0; j < unit_cell; ++j) {
					gamma.push_back( ext_gam[ j ] );
					lambda.push_back( ext_lam[ (j+1)%unit_cell ] );
				}
			}
		}

		setSize( lat_size + add_tail );

		ext_gam.clear();
		ext_lam.clear();
	}
}

//======================================

void ChainQnSemiInf::importMPS( std::string dirname, int unit_cell ) {
	///
	// fit_chi in CanonMPS doesn't work for Qn classes => set false
	CanonMPS::importMPS( dirname, unit_cell, true, false );

	SchmidtVal sv_l0 = entangleSpecQn( lambda[0], 1 )[0];

	int bdry = (reverse_bdry)? lat_size : 0;
	uni10::CMatrix uni = lambda[bdry].getBlock( sv_l0.qn );
	uni.set_zero();
	uni.at(0, 0) = Complex(1.0, 0.0);
	lambda[bdry].set_zero();
	lambda[bdry].putBlock( sv_l0.qn, uni );
}

//======================================

uni10::CUniTensor ChainQnSemiInf::expVal( uni10::CUniTensor op, int idx ) {
	/// return expectation value (a contracted uni10) of an 1-site/2-site operator on site idx
	uni10::CUniTensor exp_val;

	uni10::CUniTensor lvec( lambda[0].bond() );
	uni10::CUniTensor rvec( lambda[lat_size].bond() );
	lvec.identity();
	rvec.identity();

	int bno = op.bondNum();
	if ( bno < 2 || bno > 4 ) {
		std::cerr << "In ChainQnSemiInf::expVal : Unsupported Operator.\n";
		return exp_val;
	}
	if ( bno%2 && op.bond()[bno-1].dim() == 1 ) {
		std::cerr << "In ChainQnSemiInf::expVal : Non-symmetric operator.\n";
		return exp_val;
	}

	uni10::CUniTensor onsite;
	if (bno == 2)
		onsite = netLGL( lambda[idx], gamma[idx], lambda[idx+1] );
	else if (bno == 4)
		onsite = netLGLGL( lambda[idx], gamma[idx], lambda[idx+1], gamma[idx+1], lambda[idx+2] );

	if (idx > 0)
		lvec = buildLRVecQn( idx-1, lvec, gamma, lambda, true );
	lvec = buildLRVecQn( lvec, onsite, op, true );
	if (idx < lat_size-(bno/2))
		rvec = buildLRVecQn( idx+(bno/2), rvec, gamma, lambda, false );
	exp_val = contrLRVecQn( lvec, rvec );

	return exp_val;
}

//======================================

uni10::CUniTensor ChainQnSemiInf::correlation(
	uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 ) {
	///
	// todo: assert idx2 - idx1 >= op.bondNum
	// todo: assert lat_size - idx2 >= op.bondNum
	uni10::CUniTensor corr;

	uni10::CUniTensor lvec( lambda[0].bond() );
	uni10::CUniTensor rvec( lambda[lat_size].bond() );
	lvec.identity();
	rvec.identity();

	int bno1 = op1.bondNum();
	int bno2 = op2.bondNum();
	if ( bno1 != bno2 ) {
		std::cerr << "In ChainQnSemiInf::correlation : Bonds of two operators do not match.\n";
		return corr;
	}
	if ( bno1 < 2 || bno1 > 4 ) {
		std::cerr << "In ChainQnSemiInf::correlation : Unsupported Operator.\n";
		return corr;
	}

	uni10::CUniTensor onsite1, onsite2;
	if (bno1 == 2 || bno1 == 3) {
		onsite1 = netLG( lambda[idx1], gamma[idx1] );
		onsite2 = netLGL( lambda[idx2], gamma[idx2], lambda[idx2+1] );
	}
	else if (bno1 == 4) {
		onsite1 = netLGLG( lambda[idx1], gamma[idx1], lambda[idx1+1], gamma[idx1+1] );
		onsite2 = netLGLGL( lambda[idx2], gamma[idx2], lambda[idx2+1], gamma[idx2+1], lambda[idx2+2] );
	}

	if (idx1 > 0)
		lvec = buildLRVecQn( idx1-1, lvec, gamma, lambda, true );

	lvec = buildLRVecQn( lvec, onsite1, op1, true );

	for (int i = idx1+bno1/2; i < idx2; ++i) {
		uni10::CUniTensor cell = netLG( lambda[i], gamma[i] );
		lvec = buildLRVecQn( lvec, cell, true );
	}

	lvec = buildLRVecQn( lvec, onsite2, op2, true );

	if (idx2 < lat_size-(bno2/2))
		rvec = buildLRVecQn( idx2+(bno2/2), rvec, gamma, lambda, false );

	corr = contrLRVecQn( lvec, rvec );

	return corr;
}

//======================================
/*
void ChainQnSemiInf::tebd( uni10::CUniTensor hlw, uni10::CUniTensor hw,
	uni10::CUniTensor hwr, uni10::CUniTensor hr, Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS, involving multipications of lambda^-1
}
*/
//======================================

void ChainQnSemiInf::dmrg( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	int update_size = 1;

	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( lat_size, mpo_dir, mpo_sy, mpo_pl, mpo_mi );

	int n = (reverse_bdry)? 0 : mpo_sy.size()-1;
	uni10::CUniTensor last_sy = mpo_sy[n];
	uni10::CUniTensor last_pl = mpo_pl[n];
	uni10::CUniTensor last_mi = mpo_mi[n];
	importMPOQn( lat_size, mpo_dir, mpo_sy, mpo_pl, mpo_mi, true, lambda[0] );
	mpo_sy[n] = last_sy;
	mpo_pl[n] = last_pl;
	mpo_mi[n] = last_mi;

	uni10::CUniTensor psi;
	double eng = 0.0;

	std::vector<uni10::CUniTensor> mpoH_sy;
	std::vector<uni10::CUniTensor> mpoH_pl;
	std::vector<uni10::CUniTensor> mpoH_mi;
	std::vector<uni10::CUniTensor> buff_sy;
	std::vector<uni10::CUniTensor> buff_pl;
	std::vector<uni10::CUniTensor> buff_mi;

	for (int s = 0; s < sweeps; ++s) {
		//
		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 0; i <= lat_size-update_size; ++i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.bondNum() );
				int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );
				psi.permute(2);

				if (i == lat_size-1)
					gamma[i] = netLG( tenInvQn(lambda[i]), netGL( psi, tenInvQn(lambda[i+1]) ) );
				else {
					uni10::CUniTensor vi;
					mps1SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], vi );
					gamma[i+1] = netLG( vi, gamma[i+1] );
				}

				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, true );
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size-update_size; i >= 0; --i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.bondNum() );
				int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );
				psi.permute(1);

				if (i == 0)
					gamma[i] = netGL( netLG( tenInvQn(lambda[i]), psi ), tenInvQn(lambda[i+1]) );
				else {
					uni10::CUniTensor ui;
					mps1SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], ui );
					gamma[i-1] = netGL( gamma[i-1], ui );
				}

				if (verbose) {
					std::cout << i << '\t' << lat_size-i << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( lambda[i] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, false );
			}
		}
	}

	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();
	mpoH_sy.clear();
	mpoH_pl.clear();
	mpoH_mi.clear();
	buff_sy.clear();
	buff_pl.clear();
	buff_mi.clear();
}

//======================================

void ChainQnSemiInf::dmrgU2( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose ) {
	///
	int update_size = 2;

	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( lat_size, mpo_dir, mpo_sy, mpo_pl, mpo_mi );

	int n = (reverse_bdry)? 0 : mpo_sy.size()-1;
	uni10::CUniTensor last_sy = mpo_sy[n];
	uni10::CUniTensor last_pl = mpo_pl[n];
	uni10::CUniTensor last_mi = mpo_mi[n];
	importMPOQn( lat_size, mpo_dir, mpo_sy, mpo_pl, mpo_mi, true, lambda[0] );
	mpo_sy[n] = last_sy;
	mpo_pl[n] = last_pl;
	mpo_mi[n] = last_mi;

	uni10::CUniTensor psi;
	double eng = 0.0;

	std::vector<uni10::CUniTensor> mpoH_sy;
	std::vector<uni10::CUniTensor> mpoH_pl;
	std::vector<uni10::CUniTensor> mpoH_mi;
	std::vector<uni10::CUniTensor> buff_sy;
	std::vector<uni10::CUniTensor> buff_pl;
	std::vector<uni10::CUniTensor> buff_mi;

	for (int s = 0; s < sweeps; ++s) {
		//
		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 0; i <= lat_size-update_size; ++i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.bondNum() );
				int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

				mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, true );
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size-update_size; i >= 0; --i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.bondNum() );
				int status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

				mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

				if (verbose) {
					std::cout << i+1 << '\t' << lat_size-i-1 << '\t';
					std::cout << std::setprecision(12) << entangleEntropyQn( lambda[i+1] ) << '\t'
						<< eng << '\n';
				}
				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, false );
			}
		}
	}

	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();
	mpoH_sy.clear();
	mpoH_pl.clear();
	mpoH_mi.clear();
	buff_sy.clear();
	buff_pl.clear();
	buff_mi.clear();
}

//======================================
