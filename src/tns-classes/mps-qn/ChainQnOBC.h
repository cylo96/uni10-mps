#ifndef CHAINQNOBC_H
#define CHAINQNOBC_H
#include <mps/ChainOBC.h>
#include "CanonQnMPS.h"

class ChainQnOBC: public CanonQnMPS {

public:
	/// constructor
	ChainQnOBC( int L, int X, std::vector<uni10::Qnum>& q_phys,
		uni10::Qnum q_vin, uni10::Qnum q_vout, uni10::Qnum qp_pl, uni10::Qnum qp_mi );
	ChainQnOBC( int L, int X, std::vector<uni10::Qnum>& q_phys, uni10::Qnum q_vin, uni10::Qnum q_vout );
	ChainQnOBC( int L, int X, std::vector<uni10::Qnum>& q_phys, uni10::Qnum q_virt );
	ChainQnOBC( int L, int X, std::vector<uni10::Qnum>& q_phys );

	void init();
	void init( std::vector<uni10::Qnum>& qv_vec );
	void randomize();

	void importMPS( std::string dirname, int unit_cell = 2, bool remake_bdry = true );
	void setFixQn( const std::vector<std::vector<uni10::Qnum>>& qvecs );

	void tebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS = 1,
		bool show_err = false, bool fix_qn = false );
	void tebd( uni10::CUniTensor Uevol, int steps,
		bool show_err = false, bool fix_qn = false );
	void tebdImp( std::vector<uni10::CUniTensor>& hams, Complex dt, int steps, int orderTS = 1,
		bool show_err = false, bool fix_qn = false );
	void tebdImp( std::vector<uni10::CUniTensor>& Uevols, int steps,
		bool show_err = false, bool fix_qn = false );

	void dmrg( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose = false );
	void dmrgU2( std::string mpo_dir, int sweeps, int iter_max, double tolerance, bool verbose = false );

	void dmrgImp(
		std::vector<uni10::CUniTensor>& mpo_sy,
		std::vector<uni10::CUniTensor>& mpo_pl,
		std::vector<uni10::CUniTensor>& mpo_mi,
		int sweeps, int iter_max, double tolerance, bool verbose = false, bool fix_qn = false );
	void dmrgImpU2(
		std::vector<uni10::CUniTensor>& mpo_sy,
		std::vector<uni10::CUniTensor>& mpo_pl,
		std::vector<uni10::CUniTensor>& mpo_mi,
		int sweeps, int iter_max, double tolerance, bool verbose = false, bool fix_qn = false );

	uni10::CUniTensor expVal( uni10::CUniTensor op, int idx );
	uni10::CUniTensor correlation( 
		uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 );

	ChainOBC toNoQ();

private:
	bool grd0 = true;
	std::vector<uni10::Qnum> qvout;
	std::vector<std::vector<uni10::Qnum>> qfix;
	void rmRedundantBlk();

};

#endif
