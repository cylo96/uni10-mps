#include <uni10/numeric/uni10_lapack.h>
#include <tns-func/uni10_arpack_wrapper.h>
#include <mps-qn/func_qn_la.h>

#include <stdexcept>
#include <uni10/tools/uni10_tools.h>
#ifdef MKL
	#define MKL_Complex16 std::complex<double>
	#include <mkl.h>
#else
	#include <uni10/numeric/uni10_lapack_wrapper.h>
#endif
#include <tns-func/func_net.h>
#include <mps-qn/func_qn_net.h>

//=================================================

uni10::CUniTensor matVec( uni10::CUniTensor mat, uni10::CUniTensor vec ) {
	///
	int bnm = mat.bondNum();
	int bnv = vec.bondNum();
	int bnc = bnm/2;
	std::vector<int> lab_mat = mat.label();
	std::vector<int> lab_vec;
	uni10::CUniTensor matvec;

	if (bnv == bnc) {
		for (int i = 0; i < bnc; ++i)
			lab_vec.push_back( lab_mat[ bnm-bnc+i ] );
		vec.setLabel( lab_vec );
		matvec = mat * vec;
	}
	else if (bnv == bnc+2) {
		std::vector<int> lab_mv;

		lab_vec.push_back( lab_mat[0] + 1000 );
		lab_mv.push_back( lab_mat[0] + 1000 );
		for (int i = 0; i < bnc; ++i) {
			lab_vec.push_back( lab_mat[ bnm-bnc+i ] );
			lab_mv.push_back( lab_mat[i] );
		}
		lab_vec.push_back( lab_mat[bnm-1] + 1000 );
		lab_mv.push_back( lab_mat[bnm-1] + 1000 );

		vec.setLabel( lab_vec );
		matvec = mat * vec;
		matvec.permute( lab_mv, bnv );
	}

	return matvec;
}

//=================================================
//=================================================

bool myQnLanczosEV(
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	uni10::CUniTensor& psi,
	size_t& max_iter, double err_tol,
	double& eigVal, uni10::CUniTensor& eigVec ) {
	///
	const int min_iter = 2;
	const double beta_err = 1E-15;
	if(!(max_iter > min_iter)){
		std::ostringstream err;
		err<<"Maximum iteration number should be set greater than 2.";
		throw std::runtime_error(uni10::exception_msg(err.str()));
	}
	std::complex<double> a = 1.0;
	std::complex<double> alpha_tmp = 0.;
	double alpha;
	double beta = 1.;
	int inc = 1;
	size_t M = max_iter;

	// phi's
	std::vector<uni10::CUniTensor> phi;
	for (int i = 0; i < M+1; ++i)
		phi.push_back( uni10::CUniTensor() );

	double *As = (double*)malloc(M * sizeof(double));
	double *Bs = (double*)malloc(M * sizeof(double));
	double *d = (double*)malloc(M * sizeof(double));
	double *e = (double*)malloc(M * sizeof(double));
	int it = 0;

	// normalize
	psi *= ( 1./vecNormQn(psi) );

	// init phi0 = psi
	phi[0] = psi;
	// init phi1 = 0
	phi[1].assign( psi.bond() );
	phi[1].set_zero();

	memset(As, 0, M * sizeof(double));
	memset(Bs, 0, M * sizeof(double));

	double e_diff = 1;
	double e0_old = 0;
	bool converged = false;
	int bn = psi.bondNum();

	while((((e_diff > err_tol) && it < max_iter) || it < min_iter) && beta > beta_err) {
		double minus_beta = -beta;

		// |phi[it+1]> = H|phi[it]> - b * |phi[it-1]>
		phi[it+1] *= minus_beta;
		phi[it+1] += a * mpoMatVecQn( bn, mpo_sy, mpo_pl, mpo_mi, phi[it] );

		// <phi[it]|phi[it+1]>
		alpha_tmp = innerProdQn( phi[it], phi[it+1] )[0];
		alpha = alpha_tmp.real();
		double minus_alpha = -alpha;

		//
		phi[it+1] += minus_alpha * phi[it];
		beta = vecNormQn( phi[it+1] );
		if(it < max_iter - 1)
			phi[it+2] = phi[it];
		As[it] = alpha;
		if(beta > beta_err) {
			phi[it+1] *= (1./beta);
			if(it < max_iter - 1)
				Bs[it] = beta;
		}
		else
			converged = true;
		it++;
		if(it > 1) {
			double* z = NULL;
			double* work = NULL;
			int info;
			memcpy(d, As, it * sizeof(double));
			memcpy(e, Bs, it * sizeof(double));
			dstev((char*)"N", &it, d, e, z, &it, work, &info);
			if(info != 0) {
				std::ostringstream err;
				err<<"Error in Lapack function 'dstev': Lapack INFO = "<<info;
				throw std::runtime_error(uni10::exception_msg(err.str()));
			}
			double base = std::abs(d[0]) > 1 ? std::abs(d[0]) : 1;
			e_diff = std::abs(d[0] - e0_old) / base;
			e0_old = d[0];
			if(e_diff <= err_tol)
				converged = true;
		}
	}
	if(it > 1) {
		memcpy(d, As, it * sizeof(double));
		memcpy(e, Bs, it * sizeof(double));
		double* z = (double*)malloc(it * it * sizeof(double));
		double* work = (double*)malloc(4 * it * sizeof(double));
		int info;
		dstev((char*)"V", &it, d, e, z, &it, work, &info);
		if(info != 0) {
			std::ostringstream err;
			err<<"Error in Lapack function 'dstev': Lapack INFO = "<<info;
			throw std::runtime_error(uni10::exception_msg(err.str()));
		}
		eigVec.set_zero();
		std::complex<double> cz;
		for(int k = 0; k < it; k++) {
			cz = z[k];
			eigVec += cz * phi[k];
		}
		max_iter = it;
		eigVal = d[0];
		free(z), free(work);
	}
	else {
		max_iter = 1;
		eigVal = 0;
	}
	free(As), free(Bs), free(d), free(e);
	return converged;
}

//=================================================

size_t myQnLanczosEigh(
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	uni10::CUniTensor& psi,
	double& E0, size_t max_iter, double err_tol ) {
	///
	psi.permute( psi.bondNum() );
	uni10::CUniTensor eigVec = psi;

	try {
		int retry = 0;
		size_t iter = max_iter;
		while (retry < 4) {
			if( !myQnLanczosEV(mpo_sy, mpo_pl, mpo_mi, psi, iter, err_tol, E0, eigVec) ) {
				if (retry < 3) {
					err_tol *= 10.;
					std::cerr << "Lanczos algorithm fails in converging. Tuning up err_tol to " << err_tol << '\n';
					retry += 1;
				}
				else {
					std::ostringstream err;
					err << "Lanczos algorithm fails in converging.";
					throw std::runtime_error(uni10::exception_msg(err.str()));
				}
			}
			else {
				psi = eigVec;
				return iter;
			}
		}
	}
	catch(const std::exception& e) {
		uni10::propogate_exception(e, "In function myQnLanczosEigh: ");
		return 0;
	}
}

//=================================================
//=================================================

bool myQnLanczosEV(
	std::vector<uni10::CUniTensor>& mpo, uni10::CUniTensor& psi,
	size_t& max_iter, double err_tol,
	double& eigVal, uni10::CUniTensor& eigVec ) {
	///
	const int min_iter = 2;
	const double beta_err = 1E-15;
	if(!(max_iter > min_iter)){
		std::ostringstream err;
		err<<"Maximum iteration number should be set greater than 2.";
		throw std::runtime_error(uni10::exception_msg(err.str()));
	}
	std::complex<double> a = 1.0;
	std::complex<double> alpha_tmp = 0.;
	double alpha;
	double beta = 1.;
	int inc = 1;
	size_t M = max_iter;

	// phi's
	std::vector<uni10::CUniTensor> phi;
	for (int i = 0; i < M+1; ++i)
		phi.push_back( uni10::CUniTensor() );

	double *As = (double*)malloc(M * sizeof(double));
	double *Bs = (double*)malloc(M * sizeof(double));
	double *d = (double*)malloc(M * sizeof(double));
	double *e = (double*)malloc(M * sizeof(double));
	int it = 0;

	// normalize
	psi *= ( 1./vecNormQn(psi) );

	// init phi0 = psi
	phi[0] = psi;
	// init phi1 = 0
	phi[1].assign( psi.bond() );
	phi[1].set_zero();

	memset(As, 0, M * sizeof(double));
	memset(Bs, 0, M * sizeof(double));

	double e_diff = 1;
	double e0_old = 0;
	bool converged = false;
	int bn = psi.bondNum();

	while((((e_diff > err_tol) && it < max_iter) || it < min_iter) && beta > beta_err) {
		double minus_beta = -beta;

		// |phi[it+1]> = H|phi[it]> - b * |phi[it-1]>
		phi[it+1] *= minus_beta;
		phi[it+1] += a * mpoMatVec( bn, mpo, phi[it] );

		// <phi[it]|phi[it+1]>
		alpha_tmp = innerProdQn( phi[it], phi[it+1] )[0];
		alpha = alpha_tmp.real();
		double minus_alpha = -alpha;

		//
		phi[it+1] += minus_alpha * phi[it];
		beta = vecNormQn( phi[it+1] );
		if(it < max_iter - 1)
			phi[it+2] = phi[it];
		As[it] = alpha;
		if(beta > beta_err) {
			phi[it+1] *= (1./beta);
			if(it < max_iter - 1)
				Bs[it] = beta;
		}
		else
			converged = true;
		it++;
		if(it > 1) {
			double* z = NULL;
			double* work = NULL;
			int info;
			memcpy(d, As, it * sizeof(double));
			memcpy(e, Bs, it * sizeof(double));
			dstev((char*)"N", &it, d, e, z, &it, work, &info);
			if(info != 0) {
				std::ostringstream err;
				err<<"Error in Lapack function 'dstev': Lapack INFO = "<<info;
				throw std::runtime_error(uni10::exception_msg(err.str()));
			}
			double base = std::abs(d[0]) > 1 ? std::abs(d[0]) : 1;
			e_diff = std::abs(d[0] - e0_old) / base;
			e0_old = d[0];
			if(e_diff <= err_tol)
				converged = true;
		}
	}
	if(it > 1) {
		memcpy(d, As, it * sizeof(double));
		memcpy(e, Bs, it * sizeof(double));
		double* z = (double*)malloc(it * it * sizeof(double));
		double* work = (double*)malloc(4 * it * sizeof(double));
		int info;
		dstev((char*)"V", &it, d, e, z, &it, work, &info);
		if(info != 0) {
			std::ostringstream err;
			err<<"Error in Lapack function 'dstev': Lapack INFO = "<<info;
			throw std::runtime_error(uni10::exception_msg(err.str()));
		}
		eigVec.set_zero();
		std::complex<double> cz;
		for(int k = 0; k < it; k++) {
			cz = z[k];
			eigVec += cz * phi[k];
		}
		max_iter = it;
		eigVal = d[0];
		free(z), free(work);
	}
	else {
		max_iter = 1;
		eigVal = 0;
	}
	free(As), free(Bs), free(d), free(e);
	return converged;
}

//=================================================

size_t myQnLanczosEigh( std::vector<uni10::CUniTensor>& mpo, uni10::CUniTensor& psi,
	double& E0, size_t max_iter, double err_tol ) {
	///
	psi.permute( psi.bondNum() );
	uni10::CUniTensor eigVec = psi;

	try {
		int retry = 0;
		size_t iter = max_iter;
		while (retry < 4) {
			if( !myQnLanczosEV(mpo, psi, iter, err_tol, E0, eigVec) ) {
				if (retry < 3) {
					err_tol *= 10.;
					std::cerr << "Lanczos algorithm fails in converging. Tuning up err_tol to " << err_tol << '\n';
					retry += 1;
				}
				else {
					std::ostringstream err;
					err << "Lanczos algorithm fails in converging.";
					throw std::runtime_error(uni10::exception_msg(err.str()));
				}
			}
			else {
				psi = eigVec;
				return iter;
			}
		}
	}
	catch(const std::exception& e) {
		uni10::propogate_exception(e, "In function myQnLanczosEigh: ");
		return 0;
	}
}

//=================================================
//=================================================

bool myQnLanczosEV(
	uni10::CUniTensor& op, uni10::CUniTensor& psi,
	size_t& max_iter, double err_tol,
	double& eigVal, uni10::CUniTensor& eigVec ) {
	///
	const int min_iter = 2;
	const double beta_err = 1E-15;
	if(!(max_iter > min_iter)){
		std::ostringstream err;
		err<<"Maximum iteration number should be set greater than 2.";
		throw std::runtime_error(uni10::exception_msg(err.str()));
	}
	std::complex<double> a = 1.0;
	std::complex<double> alpha_tmp= 0.;
	double alpha;
	double beta = 1.;
	int inc = 1;
	size_t M = max_iter;

	// phi's
	std::vector<uni10::CUniTensor> phi;
	for (int i = 0; i < M+1; ++i)
		phi.push_back( uni10::CUniTensor() );

	double *As = (double*)malloc(M * sizeof(double));
	double *Bs = (double*)malloc(M * sizeof(double));
	double *d = (double*)malloc(M * sizeof(double));
	double *e = (double*)malloc(M * sizeof(double));
	int it = 0;

	// normalize
	psi *= ( 1./vecNormQn(psi) );

	// init phi0 = psi
	phi[0] = psi;
	// init phi1 = 0
	phi[1].assign( psi.bond() );
	phi[1].set_zero();

	memset(As, 0, M * sizeof(double));
	memset(Bs, 0, M * sizeof(double));

	double e_diff = 1;
	double e0_old = 0;
	bool converged = false;
	int bn = psi.bondNum();

	while((((e_diff > err_tol) && it < max_iter) || it < min_iter) && beta > beta_err) {
		double minus_beta = -beta;

		// |phi[it+1]> = H|phi[it]> - b * |phi[it-1]>
		phi[it+1] *= minus_beta;
		phi[it+1] += a * matVec( op, phi[it] );

		// <phi[it]|phi[it+1]>
		alpha_tmp = innerProdQn( phi[it], phi[it+1] )[0];
		alpha = alpha_tmp.real();
		double minus_alpha = -alpha;

		//
		phi[it+1] += minus_alpha * phi[it];
		beta = vecNormQn( phi[it+1] );
		if(it < max_iter - 1)
			phi[it+2] = phi[it];
		As[it] = alpha;
		if(beta > beta_err) {
			phi[it+1] *= (1./beta);
			if(it < max_iter - 1)
				Bs[it] = beta;
		}
		else
			converged = true;
		it++;
		if(it > 1) {
			double* z = NULL;
			double* work = NULL;
			int info;
			memcpy(d, As, it * sizeof(double));
			memcpy(e, Bs, it * sizeof(double));
			dstev((char*)"N", &it, d, e, z, &it, work, &info);
			if(info != 0) {
				std::ostringstream err;
				err<<"Error in Lapack function 'dstev': Lapack INFO = "<<info;
				throw std::runtime_error(uni10::exception_msg(err.str()));
			}
			double base = std::abs(d[0]) > 1 ? std::abs(d[0]) : 1;
			e_diff = std::abs(d[0] - e0_old) / base;
			e0_old = d[0];
			if(e_diff <= err_tol)
				converged = true;
		}
	}
	if(it > 1) {
		memcpy(d, As, it * sizeof(double));
		memcpy(e, Bs, it * sizeof(double));
		double* z = (double*)malloc(it * it * sizeof(double));
		double* work = (double*)malloc(4 * it * sizeof(double));
		int info;
		dstev((char*)"V", &it, d, e, z, &it, work, &info);
		if(info != 0) {
			std::ostringstream err;
			err<<"Error in Lapack function 'dstev': Lapack INFO = "<<info;
			throw std::runtime_error(uni10::exception_msg(err.str()));
		}
		eigVec.set_zero();
		std::complex<double> cz;
		for(int k = 0; k < it; k++) {
			cz = z[k];
			eigVec += cz * phi[k];
		}
		max_iter = it;
		eigVal = d[0];
		free(z), free(work);
	}
	else {
		max_iter = 1;
		eigVal = 0;
	}
	free(As), free(Bs), free(d), free(e);
	return converged;
}

//=================================================

size_t myQnLanczosEigh( uni10::CUniTensor& op, uni10::CUniTensor& psi,
	double& E0, size_t max_iter, double err_tol ) {
	///
	psi.permute( psi.bondNum() );
	uni10::CUniTensor eigVec = psi;

	try {
		int retry = 0;
		size_t iter = max_iter;
		while (retry < 4) {
			if( !myQnLanczosEV(op, psi, iter, err_tol, E0, eigVec) ) {
				if (retry < 3) {
					err_tol *= 10.;
					std::cerr << "Lanczos algorithm fails in converging. Tuning up err_tol to " << err_tol << '\n';
					retry += 1;
				}
				else {
					std::ostringstream err;
					err << "Lanczos algorithm fails in converging.";
					throw std::runtime_error(uni10::exception_msg(err.str()));
				}
			}
			else {
				psi = eigVec;
				return iter;
			}
		}
	}
	catch(const std::exception& e) {
		uni10::propogate_exception(e, "In function myQnLanczosEigh: ");
		return 0;
	}
}

//=================================================
//=================================================

bool myQnArpackDomEig(
	uni10::CUniTensor& ket, uni10::CUniTensor& bra, uni10::CUniTensor& psi,
	uni10::Qnum& qn, size_t n,
	size_t& max_iter, double& eigVal, std::complex<double>* eigVec,
	bool ongpu, double err_tol, int nev, bool left ) {
	///
	int dim = n;
	int ido = 0;
	char bmat = 'I';
	char which[] = {'L','R'};	// largest real part

	std::complex<double> *resid = new std::complex<double>[dim];
	memcpy(resid, psi.getBlock(qn).getElem(), dim * sizeof(std::complex<double>));

	int ncv = 42;
	if ( dim < ncv )
		ncv = dim;
	int ldv = dim;
	std::complex<double> *v = new std::complex<double>[ldv*ncv];
	int *iparam = new int[11];
	iparam[0] = 1;
	iparam[2] = max_iter;
	iparam[6] = 1;
	int *ipntr = new int[14];	// Different from real version
	std::complex<double> *workd = new std::complex<double>[3*dim];
	int lworkl = 3*ncv*(ncv+2);	// LWORKL must be at least 3*NCV**2 + 5*NCV.
	std::complex<double> *workl = new std::complex<double>[lworkl];
	double *rwork = new double[ncv];
	int info = 1;
	// Parameters for matvec
	std::complex<double> alpha(1.0e0, 0.0e0);
	std::complex<double> beta(0.0e0, 0.0e0);
	int inc = 1;

	uni10::CUniTensor trial( psi.bond() );

	znaupd_(&ido, &bmat, &dim, &which[0], &nev, &err_tol, resid, &ncv, v, &ldv,
		iparam, ipntr, workd, workl, &lworkl, rwork, &info);

	while ( ido != 99 ) {

		psi.setElem( workd+ipntr[0]-1 );
		trial.setElem( workd+ipntr[1]-1 );
		// mat * vec
		trial *= beta;
		trial += alpha * tranMatVecQn( left, ket, bra, psi );
		memcpy( workd+ipntr[1]-1, trial.getBlock(qn).getElem(), dim * sizeof(std::complex<double>) );

		znaupd_(&ido, &bmat, &dim, &which[0], &nev, &err_tol, resid, &ncv, v, &ldv,
			iparam, ipntr, workd, workl, &lworkl, rwork, &info);
	}

	if ( info < 0 )
		std::cerr << "Error with znaupd, info = " << info << std::endl;
	else if ( info == 1 )
		std::cerr << "Maximum number of Lanczos iterations reached." << std::endl;
	else if ( info == 3 )
		std::cerr << "No shifts could be applied during implicit Arnoldi update,"
				<< " try increasing NCV." << std::endl;

	// zneupd Parameters
	int rvec = 1;
	char howmny = 'A';
	int *select = new int[ncv];
	std::complex<double> *d = new std::complex<double>[nev+1];
	std::complex<double> *z = new std::complex<double>[dim*nev];
	std::complex<double> sigma;
	std::complex<double> *workev = new std::complex<double>[2*ncv];
	zneupd_(&rvec, &howmny, select, d, z, &ldv, &sigma, workev,
		&bmat, &dim, &which[0], &nev, &err_tol, resid, &ncv, v, &ldv,
		iparam, ipntr, workd, workl, &lworkl, rwork, &info);
	if ( info != 0 )
		std::cerr << "Error with dneupd, info = " << info << std::endl;
	eigVal = d[nev-1].real();
	memcpy(eigVec, z, dim * sizeof(std::complex<double>));

	delete [] workev;
	delete [] z;
	delete [] d;
	delete [] select;
	delete [] rwork;
	delete [] workl;
	delete [] workd;
	delete [] ipntr;
	delete [] iparam;
	delete [] v;
	delete [] resid;

	return (info == 0);
}

//=================================================

size_t myQnArpLanczosDomEig(
	uni10::CUniTensor& ket, uni10::CUniTensor& psi,
	double& E0, size_t max_iter, double err_tol, int nev, bool left ) {
	///
	uni10::CUniTensor bra = dag(ket);

	psi.permute( psi.bondNum() );
	uni10::Qnum q0(0);	// only q0 survives in column vec
	uni10::CMatrix psi_blk = psi.getBlock(q0);
	size_t Rnum = psi_blk.elemNum();
	std::complex<double> *psi_elem = psi_blk.getElem();

	try {
		size_t iter = max_iter;
		if( !myQnArpackDomEig(ket, bra, psi, q0, Rnum, iter, E0, psi_elem, false, err_tol, nev, left) ) {
			std::ostringstream err;
			err<<"Lanczos algorithm fails in converging.";
			throw std::runtime_error(uni10::exception_msg(err.str()));
		}
		psi.putBlock( q0, psi_blk );
		return iter;
	}
	catch(const std::exception& e) {
		uni10::propogate_exception(e, "In function myQnArpLanczosDomEig: ");
		return 0;
	}
}

//=================================================
