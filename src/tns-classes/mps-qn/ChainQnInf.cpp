#include <iostream>
#include <string>
#include <algorithm>    // std::swap, std::sort
#include <tuple>

#include <mps-qn/ChainQnInf.h>
#include <mps-qn/func_qn_la.h>
#include <mps-qn/func_qn_op.h>
#include <mps-qn/func_qn_net.h>
#include <mps-qn/func_qn_blkops.h>
#include <mps-qn/func_qn_meas.h>
// #include <tns-func/tns_const.h>
#include <tns-func/func_op.h>
#include <tns-func/func_net.h>

//======================================

ChainQnInf::ChainQnInf( int L, int X, std::vector<uni10::Qnum>& q_phys, std::vector<uni10::Qnum>& q_virt ) :
	CanonQnMPS( L, X, q_phys, q_virt ) {
	/// object constructor
}

//======================================

ChainQnInf::ChainQnInf( int L, int X, std::vector<uni10::Qnum>& q_phys ) :
	CanonQnMPS( L, X, q_phys ) {
	/// object constructor
	bool fermi = q_phys[0].isFermionic();
	bool u1 = ((q_phys[0].U1() != 0) || (q_phys[q_phys.size()-1].U1() != 0 ));
	bool spinful = (fermi && std::abs(q_phys[0].U1() - q_phys[1].U1()) > 1);

	if ((fermi || u1) && !spinful) {	// q_rais, q0, q_lowr. To meet the requirement of bdry dummy mpo
		// only works on spin-1/2
		uni10::Qnum q1 = q_phys[0];
		uni10::Qnum q2 = q_phys[q_phys.size()-1];

		qvirt.push_back( q1 * (-q2) );
		qvirt.push_back( uni10::Qnum(0) );
		qvirt.push_back( -q1 * q2 );
	}
	else if (spinful) {
		// only works on spin-1/2
		uni10::Qnum q1 = q_phys[0];
		uni10::Qnum q2 = q_phys[q_phys.size()-1];
		if (q1.prtF() == 0) {
			uni10::Qnum qf(0);
			qf.assign(uni10::PRTF_ODD, 0, uni10::PRT_EVEN);
			q1 = q1 * qf;
			q2 = q2 * qf;
		}
		qvirt.push_back( q1 );
		qvirt.push_back( uni10::Qnum(0) );
		qvirt.push_back( q2 );
	}
	else
		qvirt.push_back( uni10::Qnum(0) );
}

//======================================

ChainQnInf::ChainQnInf( int X, std::vector<uni10::Qnum>& q_phys, std::vector<uni10::Qnum>& q_virt ) :
	CanonQnMPS( 2, X, q_phys, q_virt ) {
	/// object constructor
}

//======================================

ChainQnInf::ChainQnInf( int X, std::vector<uni10::Qnum>& q_phys ) :
	CanonQnMPS( 2, X, q_phys ) {
	/// object constructor
	bool fermi = q_phys[0].isFermionic();

	if (fermi) {
		qvirt = q_phys;
		for (int i = 0; i < qvirt.size(); ++i)
			qvirt[i].assign( qvirt[i].prtF(), 0, qvirt[i].prt() );	// qf0, q0
		// qvirt.push_back( uni10::Qnum(0) );
	}
	else
		qvirt.push_back( uni10::Qnum(0) );
}

//======================================

void ChainQnInf::init() {
	///
	if (gamma.size() > 0)
		gamma.clear();
	if (lambda.size() > 0)
		lambda.clear();

	gamma.push_back( initGamma(qvirt, qphys) );
	lambda.push_back( initLambda(qvirt) );

	bool fermi = qphys[0].isFermionic();
	bool u1 = ((qphys[0].U1() != 0) || (qphys[qphys.size()-1].U1() != 0 ));

	if (fermi && u1 && lat_size >= 4) {
		std::vector<uni10::Qnum> qin1 = gamma[0].bond()[2].Qlist();
		gamma.push_back( initGamma(qin1, qphys) );
		lambda.push_back( initLambda(qin1) );
		std::vector<uni10::Qnum> qin2 = gamma[1].bond()[2].Qlist();
		gamma.push_back( initGamma(qin2, qphys) );
		lambda.push_back( initLambda(qin2) );
		std::vector<uni10::Qnum> qin3 = gamma[2].bond()[2].Qlist();

		for (int i = 3; i < lat_size; ++i) {
			if (i%4 == 1) {
				gamma.push_back( initGamma(qin1, qphys, qin2) );
				lambda.push_back( initLambda(qin1) );
			}
			else if (i%4 == 2) {
				gamma.push_back( initGamma(qin2, qphys, qin3) );
				lambda.push_back( initLambda(qin2) );
			}
			else if (i%4 == 3) {
				gamma.push_back( initGamma(qin3, qphys, qvirt) );
				lambda.push_back( initLambda(qin3) );
			}
			else {
				gamma.push_back( initGamma(qvirt, qphys, qin1) );
				lambda.push_back( initLambda(qvirt) );
			}
		}
	}
	else {
		std::vector<uni10::Qnum> qin = gamma[0].bond()[2].Qlist();

		for (int i = 1; i < lat_size; ++i) {
			if (i%2) {
				gamma.push_back( initGamma(qin, qphys, qvirt) );
				lambda.push_back( initLambda(qin) );
			}
			else {
				gamma.push_back( initGamma(qvirt, qphys, qin) );
				lambda.push_back( initLambda(qvirt) );
			}
		}
	}
}

//======================================

void ChainQnInf::randomize() {
	/// randomize a complex MPS having only real part
	ChainQnInf::init();

	std::srand( time(NULL) );

	for (int i = 0; i < lat_size; ++i) {
		gamma[i] = randTQn( gamma[i] );
		lambda[i] = randTQn( lambda[i] );
	}

	if ( !(lat_size == 2 && qphys[0].isFermionic()) ) {
		uni10::CMatrix uni = lambda[0].getBlock( uni10::Qnum(0) );
		uni.set_zero();
		uni.at(0, 0) = Complex(1.0, 0.0);
		lambda[0].set_zero();
		lambda[0].putBlock( uni10::Qnum(0), uni );
	}
}

//======================================

void ChainQnInf::setFixQn( const std::vector<std::vector<uni10::Qnum>>& qvecs ) { qfix = qvecs; }

//======================================

void ChainQnInf::itebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS ) {
	/// perform iTEBD on iMPS using 2-site evolution operator

	if (orderTS == 1) {
		uni10::CUniTensor U = takeExpQn( I * dt, ham );       // generate exp(iHdt)
		uni10::CUniTensor theta;

		for (int iter = 0; iter < steps; ++iter) {
			//====== evolve and update ======
			theta = tensorOp( netLGLGL(lambda[0], gamma[0], lambda[1], gamma[1], lambda[0]), U );
			CanonQnMPS::mps2SiteSVD( theta, lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );
			theta = tensorOp( netLGLGL(lambda[1], gamma[1], lambda[0], gamma[0], lambda[1]), U );
			CanonQnMPS::mps2SiteSVD( theta, lambda[1], gamma[1], lambda[0], gamma[0], lambda[1] );
		}
	}
	else {
		// raise exception
	}
}

//======================================

void ChainQnInf::itebdU2( std::vector<uni10::CUniTensor>& ham, Complex dt, int steps, int orderTS, bool fix_qn ) {
	/// perform iTEBD on iMPS using 2-site evolution operator

	if (orderTS == 1) {
		uni10::CUniTensor Ua = takeExpQn( I * dt, ham[0] );       // generate exp(iHdt)
		uni10::CUniTensor Ub = takeExpQn( I * dt, ham[1] );       // generate exp(iHdt)
		uni10::CUniTensor theta;

		for (int iter = 0; iter < steps; ++iter) {
			//====== evolve and update ======
			theta = tensorOp( netLGLGL(lambda[0], gamma[0], lambda[1], gamma[1], lambda[0]), Ua );
			if (fix_qn)
				CanonQnMPS::mps2SiteSVD( theta, lambda[0], gamma[0], lambda[1], gamma[1], lambda[0], false, qfix[0] );
			else
				CanonQnMPS::mps2SiteSVD( theta, lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );

			theta = tensorOp( netLGLGL(lambda[1], gamma[1], lambda[0], gamma[0], lambda[1]), Ub );
			if (fix_qn)
				CanonQnMPS::mps2SiteSVD( theta, lambda[1], gamma[1], lambda[0], gamma[0], lambda[1], false, qfix[1] );
			else
				CanonQnMPS::mps2SiteSVD( theta, lambda[1], gamma[1], lambda[0], gamma[0], lambda[1] );
		}
	}
	else {
		// raise exception
	}
}

//======================================

void ChainQnInf::itebdUC( uni10::CUniTensor ham, Complex dt, int steps, int orderTS ) {
	/// perform iTEBD on iMPS using 2-site evolution operator
	// assert lat_size even
	if (orderTS == 1) {
		uni10::CUniTensor U = takeExpQn( I * dt, ham );       // generate exp(iHdt)
		uni10::CUniTensor theta;

		for (int iter = 0; iter < steps; ++iter) {
			//====== evolve and update ======
			for (int i = 0; i < lat_size; i+=2) {	// odd bonds
				theta = tensorOp(
					netLGLGL(lambda[i%lat_size], gamma[i%lat_size], lambda[(i+1)%lat_size], gamma[(i+1)%lat_size], lambda[(i+2)%lat_size]),
					U );
				CanonQnMPS::mps2SiteSVD( theta,
					lambda[i%lat_size], gamma[i%lat_size], lambda[(i+1)%lat_size], gamma[(i+1)%lat_size], lambda[(i+2)%lat_size] );
			}
			for (int i = 1; i < lat_size; i+=2) {	// even bonds
				theta = tensorOp(
					netLGLGL(lambda[i%lat_size], gamma[i%lat_size], lambda[(i+1)%lat_size], gamma[(i+1)%lat_size], lambda[(i+2)%lat_size]),
					U );
				CanonQnMPS::mps2SiteSVD( theta,
					lambda[i%lat_size], gamma[i%lat_size], lambda[(i+1)%lat_size], gamma[(i+1)%lat_size], lambda[(i+2)%lat_size] );
			}
		}
	}
	else {
		// raise exception
	}
}


//======================================

uni10::CUniTensor ChainQnInf::expVal( uni10::CUniTensor op, int loc ) {
	/// return expectation value (a contracted uni10) of an 1-site/2-site operator
	uni10::CUniTensor ket, eval_sum;

	if (loc >= 0 && loc < lat_size) {
		ket = netLGLGL(lambda[loc%lat_size], gamma[loc%lat_size],
			lambda[(loc+1)%lat_size], gamma[(loc+1)%lat_size], lambda[(loc+2)%lat_size]);
		return expValQn(ket, op) * (1./vecNormQn(ket));
	}

	for (int i = 0; i < lat_size; ++i) {
		ket = netLGLGL(lambda[i%lat_size], gamma[i%lat_size],
			lambda[(i+1)%lat_size], gamma[(i+1)%lat_size], lambda[(i+2)%lat_size]);
		eval_sum += expValQn(ket, op) * (1./vecNormQn(ket));
	}

	return eval_sum * (1./lat_size);
}

//======================================

void offsetMPOL( uni10::CUniTensor& mpo_ls, uni10::CUniTensor& lam ) {
	///
	MPO idl( mpo_ls.bond()[1].dim(), 'l' );
	uni10::CUniTensor Id( lam.bond() );
	Id.identity();
	idl.putTensor( Id, 0, 0 );

	uni10::CUniTensor lam_dag = dag(lam);

	int lab_mpol[] = {0, 10, 1};
	int lab_ltop[] = {2, 0};
	int lab_lbot[] = {1, 2};
	mpo_ls.setLabel( lab_mpol );
	lam_dag.setLabel( lab_ltop );
	lam.setLabel( lab_lbot );
	uni10::CUniTensor traces = uni10::contract( lam_dag, mpo_ls, false );
	traces = uni10::contract( traces, lam, false );

	Complex e0 = traces[0];
	mpo_ls = mpo_ls + (-1.) * e0 * idl.launch();
}

//======================================

void offsetMPOR( uni10::CUniTensor& mpo_rs, uni10::CUniTensor& lam ) {
	///
	int dim_mpo = mpo_rs.bond()[0].dim();
	MPO idr( dim_mpo, 'r' );
	uni10::CUniTensor Id( bondInv(lam).bond() );
	Id.identity();
	idr.putTensor( Id, dim_mpo-1, 0 );

	uni10::CUniTensor lam_dag = dag(lam);

	int lab_mpor[] = {10, 2, 3};
	int lab_ltop[] = {2, 0};
	int lab_lbot[] = {0, 3};
	mpo_rs.setLabel( lab_mpor );
	lam_dag.setLabel( lab_ltop );
	lam.setLabel( lab_lbot );
	uni10::CUniTensor traces = uni10::contract( mpo_rs, lam, false );
	traces = uni10::contract( traces, lam_dag, false );

	Complex e0 = traces[ dim_mpo-1 ];
	mpo_rs = mpo_rs + (-1.) * e0 * idr.launch();
}

//======================================

void ChainQnInf::idmrg( std::string mpo_dir, int steps, int iter_max, double tolerance,
	std::string sav_dir, bool offset_hb ) {
	///
	int uc = 2;
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( uc, mpo_dir, mpo_sy, mpo_pl, mpo_mi );

	uni10::CUniTensor psi;
	double eng = 0.0;

	uni10::CUniTensor ham2s = mpoLRQn( mpo_sy, mpo_pl, mpo_mi );	// for energy output

	// initialize trial state
	bool has_init_trial = ( gamma.size() >= 2 && lambda.size() >= 2 );

	if ( ! has_init_trial ) {
		// create dummy state
		ChainQnInf::randomize();
	}

	int status;

	for (int st = 0; st < steps; ++st) {

		if (st > 0) {
			std::swap( gamma[0], gamma[1] );
			std::swap( lambda[0], lambda[1] );
		}

		psi = netLGLGL( lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );
		psi.permute( psi.bondNum() );

		if (st == 0)
			status = myQnLanczosEigh( ham2s, psi, eng, iter_max, tolerance );
		else
			status = myQnLanczosEigh( mpo_sy, mpo_pl, mpo_mi, psi, eng, iter_max, tolerance );
			//status = myArpLanczosEigh( mpo, psi, eng, iter_max, tolerance );

		CanonQnMPS::mps2SiteSVD( psi, lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );

		// update mpo_l mpo_r
		renormMPOL( mpo_sy, mpo_pl, mpo_mi, netLG( lambda[0], gamma[0] ), (st == 0) );
		renormMPOR( mpo_sy, mpo_pl, mpo_mi, netGL( gamma[1], lambda[0] ), (st == 0) );

		if ( offset_hb && (st > 0) ) {
			// offset HL/HR diagonal elems
			offsetMPOL( mpo_sy[0], lambda[1] );
			offsetMPOR( mpo_sy[ mpo_sy.size()-1 ], lambda[1] );
		}

		eng = expVal( ham2s )[0].real();
		std::vector< SchmidtVal > sv = entangleSpecQn( lambda[1], std::min( lambda[1].bond()[0].dim(), 4 ) );
		std::cout << st << "\t" << std::setprecision(12) << eng;
		for (int i = 0; i < sv.size(); ++i)
			std::cout << '\t' << sv[i].val;
		std::cout << '\n';
	}

	if ( sav_dir != "" ) {
		int n = mpo_sy.size()-1;
		mpo_sy[0].save( sav_dir + "/mpo_ls" );
		mpo_sy[n].save( sav_dir + "/mpo_rs" );
		mpo_pl[0].save( sav_dir + "/mpo_lp" );
		mpo_pl[n].save( sav_dir + "/mpo_rp" );
		mpo_mi[0].save( sav_dir + "/mpo_lm" );
		mpo_mi[n].save( sav_dir + "/mpo_rm" );
	}

	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();
}

//======================================

void ChainQnInf::idmrgResume( std::string mpo_dir, int steps, int iter_max, double tolerance,
	std::string sav_dir, bool offset_hb ) {
	///
	int uc = 2;
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( uc, mpo_dir, mpo_sy, mpo_pl, mpo_mi );

	int n = mpo_sy.size()-1;
	mpo_sy[0] = uni10::CUniTensor( sav_dir + "/mpo_ls" );
	mpo_sy[n] = uni10::CUniTensor( sav_dir + "/mpo_rs" );
	mpo_pl[0] = uni10::CUniTensor( sav_dir + "/mpo_lp" );
	mpo_pl[n] = uni10::CUniTensor( sav_dir + "/mpo_rp" );
	mpo_mi[0] = uni10::CUniTensor( sav_dir + "/mpo_lm" );
	mpo_mi[n] = uni10::CUniTensor( sav_dir + "/mpo_rm" );

	uni10::CUniTensor psi;
	double eng = 0.0;

	uni10::CUniTensor ham2s = op2SiteFromMPOQn( mpo_sy[1], mpo_pl[1], mpo_mi[1] );	// for energy output
	int status;

	for (int st = 0; st < steps; ++st) {

		std::swap( gamma[0], gamma[1] );
		std::swap( lambda[0], lambda[1] );

		psi = netLGLGL( lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );
		psi.permute( psi.bondNum() );

		status = myQnLanczosEigh( mpo_sy, mpo_pl, mpo_mi, psi, eng, iter_max, tolerance );
		//status = myArpLanczosEigh( mpo, psi, eng, iter_max, tolerance );

		CanonQnMPS::mps2SiteSVD( psi, lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );

		// update mpo_l mpo_r
		renormMPOL( mpo_sy, mpo_pl, mpo_mi, netLG( lambda[0], gamma[0] ), false );
		renormMPOR( mpo_sy, mpo_pl, mpo_mi, netGL( gamma[1], lambda[0] ), false );

		if ( offset_hb && (st > 0) ) {
			// offset HL/HR diagonal elems
			offsetMPOL( mpo_sy[0], lambda[1] );
			offsetMPOR( mpo_sy[ mpo_sy.size()-1 ], lambda[1] );
		}

		eng = expVal( ham2s )[0].real();
		std::vector< SchmidtVal > sv = entangleSpecQn( lambda[1], std::min( lambda[1].bond()[0].dim(), 4 ) );
		std::cout << st << "\t" << std::setprecision(12) << eng;
		for (int i = 0; i < sv.size(); ++i)
			std::cout << '\t' << sv[i].val;
		std::cout << '\n';
	}

	mpo_sy[0].save( sav_dir + "/mpo_ls" );
	mpo_sy[n].save( sav_dir + "/mpo_rs" );
	mpo_pl[0].save( sav_dir + "/mpo_lp" );
	mpo_pl[n].save( sav_dir + "/mpo_rp" );
	mpo_mi[0].save( sav_dir + "/mpo_lm" );
	mpo_mi[n].save( sav_dir + "/mpo_rm" );

	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();
}

//======================================

void ChainQnInf::idmrgU2(
	std::vector<std::vector<uni10::CUniTensor>>& mpo_lr,
	std::vector<std::vector<uni10::CUniTensor>>& mpo_m,
	int steps, int iter_max, double tolerance,
	std::string sav_dir, bool offset_hb, bool fix_qn ) {
	///
	int uc = 2;
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	mpo_sy.push_back(mpo_lr[0][0]);
	mpo_pl.push_back(mpo_lr[0][1]);
	mpo_mi.push_back(mpo_lr[0][2]);
	for (int i = 0; i < 2; ++i) {
		mpo_sy.push_back(mpo_m[i][0]);
		mpo_pl.push_back(mpo_m[i][1]);
		mpo_mi.push_back(mpo_m[i][2]);
	}
	mpo_sy.push_back(mpo_lr[1][0]);
	mpo_pl.push_back(mpo_lr[1][1]);
	mpo_mi.push_back(mpo_lr[1][2]);

	uni10::CUniTensor psi;
	double eng = 0.0;

	uni10::CUniTensor ham2b = mpoLRQn( mpo_sy, mpo_pl, mpo_mi );	// initial H_2site
	uni10::CUniTensor ham2o = op2SiteFromMPOQn(mpo_m);	// H_2site for odd iter
	std::swap(mpo_m[0], mpo_m[1]);
	uni10::CUniTensor ham2e = op2SiteFromMPOQn(mpo_m);	// H_2site for even iter
	std::swap(mpo_m[0], mpo_m[1]);

	// initialize trial state
	bool has_init_trial = ( gamma.size() >= 2 && lambda.size() >= 2 );

	if ( ! has_init_trial ) {
		// create dummy state
		ChainQnInf::randomize();
	}

	int status;

	for (int st = 0; st < steps; ++st) {

		if (st > 0) {
			std::swap( gamma[0], gamma[1] );
			std::swap( lambda[0], lambda[1] );
		}
		if (st > 1) {
			std::swap( mpo_sy[1], mpo_sy[2] );
			std::swap( mpo_pl[1], mpo_pl[2] );
			std::swap( mpo_mi[1], mpo_mi[2] );
		}

		psi = netLGLGL( lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );
		psi.permute( psi.bondNum() );

		if (st == 0)
			status = myQnLanczosEigh( ham2b, psi, eng, iter_max, tolerance );
		else
			status = myQnLanczosEigh( mpo_sy, mpo_pl, mpo_mi, psi, eng, iter_max, tolerance );

		if (fix_qn)
			CanonQnMPS::mps2SiteSVD( psi, lambda[0], gamma[0], lambda[1], gamma[1], lambda[0], false, qfix[st%2] );
		else
			CanonQnMPS::mps2SiteSVD( psi, lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );

		// update mpo_l mpo_r
		renormMPOL( mpo_sy, mpo_pl, mpo_mi, netLG( lambda[0], gamma[0] ), (st == 0) );
		renormMPOR( mpo_sy, mpo_pl, mpo_mi, netGL( gamma[1], lambda[0] ), (st == 0) );

		if ( offset_hb && (st > 0) ) {
			// offset HL/HR diagonal elems
			offsetMPOL( mpo_sy[0], lambda[1] );
			offsetMPOR( mpo_sy[ mpo_sy.size()-1 ], lambda[1] );
		}

		eng = (st%2)? expVal(ham2o, 0)[0].real() : expVal(ham2e, 0)[0].real();
		std::vector< SchmidtVal > sv = entangleSpecQn( lambda[1], std::min( lambda[1].bond()[0].dim(), 4 ) );
		std::cout << st << "\t" << std::setprecision(12) << eng;
		for (int i = 0; i < sv.size(); ++i)
			std::cout << '\t' << sv[i].val;
		std::cout << '\n';
	}

	if ( sav_dir != "" ) {
		int n = mpo_sy.size()-1;
		mpo_sy[0].save( sav_dir + "/mpo_ls" );
		mpo_sy[n].save( sav_dir + "/mpo_rs" );
		mpo_pl[0].save( sav_dir + "/mpo_lp" );
		mpo_pl[n].save( sav_dir + "/mpo_rp" );
		mpo_mi[0].save( sav_dir + "/mpo_lm" );
		mpo_mi[n].save( sav_dir + "/mpo_rm" );
	}

	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();
}

//======================================

void ChainQnInf::idmrgU2Resume(
	std::vector<std::vector<uni10::CUniTensor>>& mpo_lr,
	std::vector<std::vector<uni10::CUniTensor>>& mpo_m,
	int steps, int iter_max, double tolerance,
	std::string sav_dir, bool offset_hb, bool fix_qn, bool first_swap ) {
	///
	if (first_swap)
		std::swap(mpo_m[0], mpo_m[1]);
	
	int uc = 2;
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	mpo_sy.push_back(mpo_lr[0][0]);
	mpo_pl.push_back(mpo_lr[0][1]);
	mpo_mi.push_back(mpo_lr[0][2]);
	for (int i = 0; i < 2; ++i) {
		mpo_sy.push_back(mpo_m[i][0]);
		mpo_pl.push_back(mpo_m[i][1]);
		mpo_mi.push_back(mpo_m[i][2]);
	}
	mpo_sy.push_back(mpo_lr[1][0]);
	mpo_pl.push_back(mpo_lr[1][1]);
	mpo_mi.push_back(mpo_lr[1][2]);

	int n = mpo_sy.size()-1;
	mpo_sy[0] = uni10::CUniTensor( sav_dir + "/mpo_ls" );
	mpo_sy[n] = uni10::CUniTensor( sav_dir + "/mpo_rs" );
	mpo_pl[0] = uni10::CUniTensor( sav_dir + "/mpo_lp" );
	mpo_pl[n] = uni10::CUniTensor( sav_dir + "/mpo_rp" );
	mpo_mi[0] = uni10::CUniTensor( sav_dir + "/mpo_lm" );
	mpo_mi[n] = uni10::CUniTensor( sav_dir + "/mpo_rm" );

	uni10::CUniTensor psi;
	double eng = 0.0;

	uni10::CUniTensor ham2e = op2SiteFromMPOQn(mpo_m);	// H_2site for even iter
	std::swap(mpo_m[0], mpo_m[1]);
	uni10::CUniTensor ham2o = op2SiteFromMPOQn(mpo_m);	// H_2site for odd iter
	if (!first_swap)
		std::swap(mpo_m[0], mpo_m[1]);

	int status;

	for (int st = 0; st < steps; ++st) {

		std::swap( gamma[0], gamma[1] );
		std::swap( lambda[0], lambda[1] );
		if (st > 0) {
			std::swap( mpo_sy[1], mpo_sy[2] );
			std::swap( mpo_pl[1], mpo_pl[2] );
			std::swap( mpo_mi[1], mpo_mi[2] );
		}

		psi = netLGLGL( lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );
		psi.permute( psi.bondNum() );

		status = myQnLanczosEigh( mpo_sy, mpo_pl, mpo_mi, psi, eng, iter_max, tolerance );

		if (fix_qn)
			CanonQnMPS::mps2SiteSVD( psi, lambda[0], gamma[0], lambda[1], gamma[1], lambda[0], false, qfix[st%2] );
		else
			CanonQnMPS::mps2SiteSVD( psi, lambda[0], gamma[0], lambda[1], gamma[1], lambda[0] );

		// update mpo_l mpo_r
		renormMPOL( mpo_sy, mpo_pl, mpo_mi, netLG( lambda[0], gamma[0] ) );
		renormMPOR( mpo_sy, mpo_pl, mpo_mi, netGL( gamma[1], lambda[0] ) );

		if ( offset_hb && (st > 0) ) {
			// offset HL/HR diagonal elems
			offsetMPOL( mpo_sy[0], lambda[1] );
			offsetMPOR( mpo_sy[ mpo_sy.size()-1 ], lambda[1] );
		}

		eng = (st%2)? expVal(ham2o, 0)[0].real() : expVal(ham2e, 0)[0].real();
		std::vector< SchmidtVal > sv = entangleSpecQn( lambda[1], std::min( lambda[1].bond()[0].dim(), 4 ) );
		std::cout << st << "\t" << std::setprecision(12) << eng;
		for (int i = 0; i < sv.size(); ++i)
			std::cout << '\t' << sv[i].val;
		std::cout << '\n';
	}

	if ( sav_dir != "" ) {
		int n = mpo_sy.size()-1;
		mpo_sy[0].save( sav_dir + "/mpo_ls" );
		mpo_sy[n].save( sav_dir + "/mpo_rs" );
		mpo_pl[0].save( sav_dir + "/mpo_lp" );
		mpo_pl[n].save( sav_dir + "/mpo_rp" );
		mpo_mi[0].save( sav_dir + "/mpo_lm" );
		mpo_mi[n].save( sav_dir + "/mpo_rm" );
	}

	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();
}

//=============================================

bool compare(std::tuple<int, Complex> svx, std::tuple<int, Complex> svy) {
	///
	return (std::get<1>(svx).real() > std::get<1>(svy).real());
}

//======================================

void ChainQnInf::idmrgUC( std::string mpo_dir, int steps, int iter_max, double tolerance,
	std::string sav_dir, bool offset_hb, int sweep_rt ) {
	///
	// assert lat_size even
	int uc = lat_size;
	int update_size = 2;
	lambda.push_back( lambda[0] );	// one more lambda for easier dmrg update

	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( uc, mpo_dir, mpo_sy, mpo_pl, mpo_mi, true, lambda[0] );

	std::vector<uni10::CUniTensor> mpoH_sy;
	std::vector<uni10::CUniTensor> mpoH_pl;
	std::vector<uni10::CUniTensor> mpoH_mi;
	std::vector<uni10::CUniTensor> buff_sy;
	std::vector<uni10::CUniTensor> buff_pl;
	std::vector<uni10::CUniTensor> buff_mi;

	uni10::CUniTensor psi;
	double eng = 0.0;

	uni10::CUniTensor ham2s = op2SiteFromMPOQn( mpo_sy[1], mpo_pl[1], mpo_mi[1] );	// for energy output

	// initialize trial state
	bool has_init_trial = ( gamma.size() >= lat_size && lambda.size() >= lat_size );

	if ( ! has_init_trial )
		ChainQnInf::randomize();

	int n = mpo_sy.size()-1;
	int mid = lat_size/2;
	int status;

	for (int st = 0; st < steps; ++st) {

		if (st > 0) {
			for (int i = 0; i < mid; ++ i) {
				std::swap( gamma[i], gamma[mid+i] );
				std::swap( lambda[i], lambda[mid+i] );
			}
			lambda[lat_size] = lambda[0];
		}

		// sweep_rt : sweep round trips
		for (int sw = 0; sw < sweep_rt; ++sw) {
			// sweep from left to right
			for (int i = 0; i <= lat_size-update_size; ++i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.bondNum() );
				status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

				mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, true );
			}
			// sweep from right to left
			for (int i = lat_size-update_size; i >= 0; --i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.bondNum() );
				status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

				mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, false );
			}
		}

		// update blk from left to center
		for (int i = 0; i < mid; ++i) {
			//
			dmrgMPOHQn( i, update_size, gamma, lambda,
				mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
			psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
			mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

			updateDMRGBufferQn( i, update_size, gamma, lambda,
				mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, true );
		}

		// update mpo_l mpo_r
		mpo_sy[0] = buff_sy[ mid-1 ];
		mpo_pl[0] = buff_pl[ mid-1 ];
		mpo_mi[0] = buff_mi[ mid-1 ];
		mpo_sy[n] = buff_sy[ mid ];
		mpo_pl[n] = buff_pl[ mid ];
		mpo_mi[n] = buff_mi[ mid ];

		if ( offset_hb && (st > 0) ) {
			// offset HL/HR diagonal elems
			offsetMPOL( mpo_sy[0], lambda[mid] );
			offsetMPOR( mpo_sy[n], lambda[mid] );
		}

		eng = expVal( ham2s, mid-1 )[0].real();
		std::vector< SchmidtVal > sv = entangleSpecQn( lambda[mid], std::min( lambda[mid].bond()[0].dim(), 4 ) );
		std::cout << st << '\t' << std::setprecision(12) << eng;
		for (int i = 0; i < sv.size(); ++i)
			std::cout << '\t' << sv[i].val;
		std::cout << '\n';

		mpoH_sy.clear();
		mpoH_pl.clear();
		mpoH_mi.clear();
		buff_sy.clear();
		buff_pl.clear();
		buff_mi.clear();
	}

	if ( sav_dir != "" ) {
		int n = mpo_sy.size()-1;
		mpo_sy[0].save( sav_dir + "/mpo_ls" );
		mpo_sy[n].save( sav_dir + "/mpo_rs" );
		mpo_pl[0].save( sav_dir + "/mpo_lp" );
		mpo_pl[n].save( sav_dir + "/mpo_rp" );
		mpo_mi[0].save( sav_dir + "/mpo_lm" );
		mpo_mi[n].save( sav_dir + "/mpo_rm" );
	}

	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();
	lambda.pop_back();
}

//======================================

void ChainQnInf::idmrgUCResume( std::string mpo_dir, int steps, int iter_max, double tolerance,
	std::string sav_dir, bool offset_hb, int sweep_rt ) {
	///
	// assert lat_size even
	int uc = lat_size;
	int update_size = 2;
	lambda.push_back( lambda[0] );	// one more lambda for easier dmrg update

	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( uc, mpo_dir, mpo_sy, mpo_pl, mpo_mi );

	int n = mpo_sy.size()-1;
	mpo_sy[0] = uni10::CUniTensor( sav_dir + "/mpo_ls" );
	mpo_sy[n] = uni10::CUniTensor( sav_dir + "/mpo_rs" );
	mpo_pl[0] = uni10::CUniTensor( sav_dir + "/mpo_lp" );
	mpo_pl[n] = uni10::CUniTensor( sav_dir + "/mpo_rp" );
	mpo_mi[0] = uni10::CUniTensor( sav_dir + "/mpo_lm" );
	mpo_mi[n] = uni10::CUniTensor( sav_dir + "/mpo_rm" );

	std::vector<uni10::CUniTensor> mpoH_sy;
	std::vector<uni10::CUniTensor> mpoH_pl;
	std::vector<uni10::CUniTensor> mpoH_mi;
	std::vector<uni10::CUniTensor> buff_sy;
	std::vector<uni10::CUniTensor> buff_pl;
	std::vector<uni10::CUniTensor> buff_mi;

	uni10::CUniTensor psi;
	double eng = 0.0;

	uni10::CUniTensor ham2s = op2SiteFromMPOQn( mpo_sy[1], mpo_pl[1], mpo_mi[1] );	// for energy output

	int mid = lat_size/2;
	int status;

	for (int st = 0; st < steps; ++st) {

		for (int i = 0; i < mid; ++ i) {
			std::swap( gamma[i], gamma[mid+i] );
			std::swap( lambda[i], lambda[mid+i] );
		}
		lambda[lat_size] = lambda[0];

		for (int sw = 0; sw < sweep_rt; ++sw) {
			// sweep from left to right
			for (int i = 0; i <= lat_size-update_size; ++i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.bondNum() );
				status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

				mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, true );
			}
			// sweep from right to left
			for (int i = lat_size-update_size; i >= 0; --i) {
				//
				dmrgMPOHQn( i, update_size, gamma, lambda,
					mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
				psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				psi.permute( psi.bondNum() );
				status = myQnLanczosEigh( mpoH_sy, mpoH_pl, mpoH_mi, psi, eng, iter_max, tolerance );

				mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

				updateDMRGBufferQn( i, update_size, gamma, lambda,
					mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, false );
			}
		}

		// update blk from left to center
		for (int i = 0; i < mid; ++i) {
			//
			dmrgMPOHQn( i, update_size, gamma, lambda,
				mpo_sy, mpo_pl, mpo_mi, mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi );
			psi = netLGLGL( lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
			mps2SiteSVD( psi, lambda[i], gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );

			updateDMRGBufferQn( i, update_size, gamma, lambda,
				mpoH_sy, mpoH_pl, mpoH_mi, buff_sy, buff_pl, buff_mi, true );
		}

		// update mpo_l mpo_r
		mpo_sy[0] = buff_sy[ mid-1 ];
		mpo_pl[0] = buff_pl[ mid-1 ];
		mpo_mi[0] = buff_mi[ mid-1 ];
		mpo_sy[n] = buff_sy[ mid ];
		mpo_pl[n] = buff_pl[ mid ];
		mpo_mi[n] = buff_mi[ mid ];

		if ( offset_hb && (st > 0) ) {
			// offset HL/HR diagonal elems
			offsetMPOL( mpo_sy[0], lambda[mid] );
			offsetMPOR( mpo_sy[n], lambda[mid] );
		}

		eng = expVal( ham2s )[0].real();
		std::vector< SchmidtVal > sv = entangleSpecQn( lambda[mid], std::min( lambda[mid].bond()[0].dim(), 4 ) );
		std::cout << st << '\t' << std::setprecision(12) << eng;
		for (int i = 0; i < sv.size(); ++i)
			std::cout << '\t' << sv[i].val;
		std::cout << '\n';

		mpoH_sy.clear();
		mpoH_pl.clear();
		mpoH_mi.clear();
		buff_sy.clear();
		buff_pl.clear();
		buff_mi.clear();
	}

	if ( sav_dir != "" ) {
		mpo_sy[0].save( sav_dir + "/mpo_ls" );
		mpo_sy[n].save( sav_dir + "/mpo_rs" );
		mpo_pl[0].save( sav_dir + "/mpo_lp" );
		mpo_pl[n].save( sav_dir + "/mpo_rp" );
		mpo_mi[0].save( sav_dir + "/mpo_lm" );
		mpo_mi[n].save( sav_dir + "/mpo_rm" );
	}

	mpo_sy.clear();
	mpo_pl.clear();
	mpo_mi.clear();
	lambda.pop_back();
}

//======================================

ChainInf ChainQnInf::toNoQ() {
	///
	ChainInf mpsNQ( dim_phys, chi_max );

	for (int i = 0; i < lat_size; ++i) {
		std::vector<uni10::Bond> bdg = gamma[i].bond();
		std::vector<uni10::Bond> bdl = lambda[i].bond();

		uni10::CUniTensor gam = CanonMPS::initGamma( bdg[0].dim(), bdg[2].dim(), bdg[1].dim() );
		uni10::CUniTensor lam = CanonMPS::initLambda( bdl[0].dim() );

		gam.setRawElem( gamma[i].getRawElem() );
		lam.setRawElem( lambda[i].getRawElem() );

		mpsNQ.putGamma( gam );
		mpsNQ.putLambda( lam );
	}

	for (int i = 0; i < lat_size; ++i) {
		int idxL = (std::abs(i-1))%lat_size;
		uni10::CUniTensor gamL = mpsNQ.getGamma(idxL);
		uni10::CUniTensor lamC = mpsNQ.getLambda(i);
		uni10::CUniTensor gamR = mpsNQ.getGamma(i);

		gamR.permute(1);
		uni10::CMatrix gamL_elem = gamL.getRawElem();
		uni10::CMatrix lamC_elem = lamC.getRawElem();
		uni10::CMatrix gamR_elem = gamR.getRawElem();

		// sort Schmidt vals
		int virt_dim = lamC.bond()[0].dim();
		std::vector< std::tuple<int, Complex> > sch_vals;
		for (int n = 0; n < virt_dim; ++n) {
			std::tuple<int, Complex> sv( n, lamC_elem.at(n, n) );
			sch_vals.push_back( sv );
		}
		std::sort( sch_vals.begin(), sch_vals.end(), compare );

		uni10::CMatrix gamL_sort = gamL_elem;
		uni10::CMatrix lamC_sort = lamC_elem;
		uni10::CMatrix gamR_sort = gamR_elem;
		for (int m = 0; m < gamL_elem.row(); ++m)
			for (int n = 0; n < gamL_elem.col(); ++n)
				gamL_sort.at(m, n) = gamL_elem.at( m, std::get<0>(sch_vals[n]) );
		for (int n = 0; n < virt_dim; ++n)
			lamC_sort.at(n, n) = lamC_elem.at( std::get<0>(sch_vals[n]), std::get<0>(sch_vals[n]) );
		for (int m = 0; m < gamR_elem.row(); ++m)
			for (int n = 0; n < gamR_elem.col(); ++n)
				gamR_sort.at(m, n) = gamR_elem.at( std::get<0>(sch_vals[m]), n );

		gamL.setRawElem( gamL_sort );
		lamC.setRawElem( lamC_sort );
		gamR.setRawElem( gamR_sort );
		gamR.permute(2);

		mpsNQ.putGamma( gamL, idxL );
		mpsNQ.putLambda( lamC, i );
		mpsNQ.putGamma( gamR, i );
	}

	return mpsNQ;
}

//======================================
