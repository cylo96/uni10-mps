#ifndef FUNC_QN_HB_H
#define FUNC_QN_HB_H
#include <uni10.hpp>

uni10::CUniTensor hbVecQn( bool left,
	uni10::CUniTensor& ket, uni10::CUniTensor& bra,
	uni10::CUniTensor& lambda, uni10::CUniTensor& x );

uni10::CUniTensor hbResidQn( bool left, 
	uni10::CUniTensor& ket, uni10::CUniTensor& bra,
	uni10::CUniTensor& lambda, uni10::CUniTensor& x,
	uni10::CUniTensor& b );

uni10::CUniTensor hbCGQn( bool left, 
	uni10::CUniTensor& ket, uni10::CUniTensor& bra,
	uni10::CUniTensor& lambda, uni10::CUniTensor& x_trial,
	uni10::CUniTensor& b, bool& early_term );

uni10::CUniTensor hbGMRESQn( bool left, 
	uni10::CUniTensor& ket, uni10::CUniTensor& bra,
	uni10::CUniTensor& lambda, uni10::CUniTensor& x_trial,
	uni10::CUniTensor& b, int ar_iter, double ar_tol, bool& early_term );

uni10::CUniTensor findHbQn( bool left,
	uni10::CUniTensor ket,
	uni10::CUniTensor& lambda, uni10::CUniTensor& x_trial,
	uni10::CUniTensor& b, int ar_iter = -1, double ar_tol = 1e-15 );

uni10::CUniTensor findHbQn( bool left, 
	uni10::CUniTensor ket,
	uni10::CUniTensor& lambda,
	uni10::CUniTensor& b, int ar_iter = -1, double ar_tol = 1e-15 );

#endif
