#include <uni10/numeric/uni10_lapack.h>
#include <tns-func/uni10_arpack_wrapper.h>

#include <stdexcept>
#include <uni10/tools/uni10_tools.h>
#ifdef MKL
	#define MKL_Complex16 std::complex<double>
	#include <mkl.h>
#else
	#include <uni10/numeric/uni10_lapack_wrapper.h>
#endif

#include <tns-func/func_net.h>
#include <mps-qn/func_qn_net.h>
#include <mps-qn/YJuncQnIBC.h>

//=================================================

bool YJuncQnIBC::lanczosEVJT( std::string loc, uni10::CUniTensor& psi,
	size_t& max_iter, double err_tol, double& eigVal, uni10::CUniTensor& eigVec ) {
	///
	const int min_iter = 2;
	const double beta_err = 1E-15;
	if(!(max_iter > min_iter)){
		std::ostringstream err;
		err<<"Maximum iteration number should be set greater than 2.";
		throw std::runtime_error(uni10::exception_msg(err.str()));
	}
	std::complex<double> a = 1.0;
	std::complex<double> alpha_tmp = 0.;
	double alpha;
	double beta = 1.;
	int inc = 1;
	size_t M = max_iter;

	// phi's
	std::vector<uni10::CUniTensor> phi;
	for (int i = 0; i < M+1; ++i)
		phi.push_back( uni10::CUniTensor() );

	double *As = (double*)malloc(M * sizeof(double));
	double *Bs = (double*)malloc(M * sizeof(double));
	double *d = (double*)malloc(M * sizeof(double));
	double *e = (double*)malloc(M * sizeof(double));
	int it = 0;

	// normalize
	psi *= ( 1./vecNormQn(psi) );

	// init phi0 = psi
	phi[0] = psi;
	// init phi1 = 0
	phi[1].assign( psi.bond() );
	phi[1].set_zero();

	memset(As, 0, M * sizeof(double));
	memset(Bs, 0, M * sizeof(double));

	double e_diff = 1;
	double e0_old = 0;
	bool converged = false;

	while((((e_diff > err_tol) && it < max_iter) || it < min_iter) && beta > beta_err) {
		double minus_beta = -beta;

		// |phi[it+1]> = H|phi[it]> - b * |phi[it-1]>
		phi[it+1] *= minus_beta;
		phi[it+1] += a * mpoMatVecJT( loc, phi[it] );

		// <phi[it]|phi[it+1]>
		alpha_tmp = innerProdQn( phi[it], phi[it+1] )[0];
		alpha = alpha_tmp.real();
		double minus_alpha = -alpha;

		//
		phi[it+1] += minus_alpha * phi[it];
		beta = vecNormQn( phi[it+1] );
		if(it < max_iter - 1)
			phi[it+2] = phi[it];
		As[it] = alpha;
		if(beta > beta_err) {
			phi[it+1] *= (1./beta);
			if(it < max_iter - 1)
				Bs[it] = beta;
		}
		else
			converged = true;
		it++;
		if(it > 1) {
			double* z = NULL;
			double* work = NULL;
			int info;
			memcpy(d, As, it * sizeof(double));
			memcpy(e, Bs, it * sizeof(double));
			dstev((char*)"N", &it, d, e, z, &it, work, &info);
			if(info != 0) {
				std::ostringstream err;
				err<<"Error in Lapack function 'dstev': Lapack INFO = "<<info;
				throw std::runtime_error(uni10::exception_msg(err.str()));
			}
			double base = std::abs(d[0]) > 1 ? std::abs(d[0]) : 1;
			e_diff = std::abs(d[0] - e0_old) / base;
			e0_old = d[0];
			if(e_diff <= err_tol)
				converged = true;
		}
	}
	if(it > 1) {
		memcpy(d, As, it * sizeof(double));
		memcpy(e, Bs, it * sizeof(double));
		double* z = (double*)malloc(it * it * sizeof(double));
		double* work = (double*)malloc(4 * it * sizeof(double));
		int info;
		dstev((char*)"V", &it, d, e, z, &it, work, &info);
		if(info != 0) {
			std::ostringstream err;
			err<<"Error in Lapack function 'dstev': Lapack INFO = "<<info;
			throw std::runtime_error(uni10::exception_msg(err.str()));
		}
		eigVec.set_zero();
		std::complex<double> cz;
		for(int k = 0; k < it; k++) {
			cz = z[k];
			eigVec += cz * phi[k];
		}
		max_iter = it;
		eigVal = d[0];
		free(z), free(work);
	}
	else {
		max_iter = 1;
		eigVal = 0;
	}
	free(As), free(Bs), free(d), free(e);
	return converged;
}

//=================================================

size_t YJuncQnIBC::lanczosEighJT( std::string loc, uni10::CUniTensor& psi,
	double& E0, size_t max_iter, double err_tol ) {
	///
	// psi.permute(psi.bondNum());	// seems good even psi is not column vec
	uni10::CUniTensor eigVec = psi;

	try {
		int retry = 0;
		size_t iter = max_iter;
		while (retry < 4) {
			if( !lanczosEVJT(loc, psi, iter, err_tol, E0, eigVec) ) {
				if (retry < 3) {
					err_tol *= 10.;
					std::cerr << "lanczosEVJT fails in converging. Tuning up err_tol to " << err_tol << '\n';
					retry += 1;
				}
				else {
					std::ostringstream err;
					err << "Lanczos algorithm fails in converging.";
					throw std::runtime_error(uni10::exception_msg(err.str()));
				}
			}
			else {
				psi = eigVec;
				return iter;
			}
		}
	}
	catch(const std::exception& e) {
		uni10::propogate_exception(e, "In YJuncQnIBC::lanczosEighJT : ");
		return 0;
	}
}

//=================================================

bool YJuncQnIBC::lanczosEVWJT( std::string loc, uni10::CUniTensor& psi,
	size_t& max_iter, double err_tol, double& eigVal, uni10::CUniTensor& eigVec ) {
	///
	const int min_iter = 2;
	const double beta_err = 1E-15;
	if(!(max_iter > min_iter)){
		std::ostringstream err;
		err<<"Maximum iteration number should be set greater than 2.";
		throw std::runtime_error(uni10::exception_msg(err.str()));
	}
	std::complex<double> a = 1.0;
	std::complex<double> alpha_tmp = 0.;
	double alpha;
	double beta = 1.;
	int inc = 1;
	size_t M = max_iter;

	// phi's
	std::vector<uni10::CUniTensor> phi;
	for (int i = 0; i < M+1; ++i)
		phi.push_back( uni10::CUniTensor() );

	double *As = (double*)malloc(M * sizeof(double));
	double *Bs = (double*)malloc(M * sizeof(double));
	double *d = (double*)malloc(M * sizeof(double));
	double *e = (double*)malloc(M * sizeof(double));
	int it = 0;

	// normalize
	psi *= ( 1./vecNormQn(psi) );

	// init phi0 = psi
	phi[0] = psi;
	// init phi1 = 0
	phi[1].assign( psi.bond() );
	phi[1].set_zero();

	memset(As, 0, M * sizeof(double));
	memset(Bs, 0, M * sizeof(double));

	double e_diff = 1;
	double e0_old = 0;
	bool converged = false;

	while((((e_diff > err_tol) && it < max_iter) || it < min_iter) && beta > beta_err) {
		double minus_beta = -beta;

		// |phi[it+1]> = H|phi[it]> - b * |phi[it-1]>
		phi[it+1] *= minus_beta;
		phi[it+1] += a * mpoMatVecWJT( loc, phi[it] );

		// <phi[it]|phi[it+1]>
		alpha_tmp = innerProdQn( phi[it], phi[it+1] )[0];
		alpha = alpha_tmp.real();
		double minus_alpha = -alpha;

		//
		phi[it+1] += minus_alpha * phi[it];
		beta = vecNormQn( phi[it+1] );
		if(it < max_iter - 1)
			phi[it+2] = phi[it];
		As[it] = alpha;
		if(beta > beta_err) {
			phi[it+1] *= (1./beta);
			if(it < max_iter - 1)
				Bs[it] = beta;
		}
		else
			converged = true;
		it++;
		if(it > 1) {
			double* z = NULL;
			double* work = NULL;
			int info;
			memcpy(d, As, it * sizeof(double));
			memcpy(e, Bs, it * sizeof(double));
			dstev((char*)"N", &it, d, e, z, &it, work, &info);
			if(info != 0) {
				std::ostringstream err;
				err<<"Error in Lapack function 'dstev': Lapack INFO = "<<info;
				throw std::runtime_error(uni10::exception_msg(err.str()));
			}
			double base = std::abs(d[0]) > 1 ? std::abs(d[0]) : 1;
			e_diff = std::abs(d[0] - e0_old) / base;
			e0_old = d[0];
			if(e_diff <= err_tol)
				converged = true;
		}
	}
	if(it > 1) {
		memcpy(d, As, it * sizeof(double));
		memcpy(e, Bs, it * sizeof(double));
		double* z = (double*)malloc(it * it * sizeof(double));
		double* work = (double*)malloc(4 * it * sizeof(double));
		int info;
		dstev((char*)"V", &it, d, e, z, &it, work, &info);
		if(info != 0) {
			std::ostringstream err;
			err<<"Error in Lapack function 'dstev': Lapack INFO = "<<info;
			throw std::runtime_error(uni10::exception_msg(err.str()));
		}
		eigVec.set_zero();
		std::complex<double> cz;
		for(int k = 0; k < it; k++) {
			cz = z[k];
			eigVec += cz * phi[k];
		}
		max_iter = it;
		eigVal = d[0];
		free(z), free(work);
	}
	else {
		max_iter = 1;
		eigVal = 0;
	}
	free(As), free(Bs), free(d), free(e);
	return converged;
}

//=================================================

size_t YJuncQnIBC::lanczosEighWJT( std::string loc, uni10::CUniTensor& psi,
	double& E0, size_t max_iter, double err_tol ) {
	///
	// psi.permute(psi.bondNum());	// seems good even psi is not column vec
	uni10::CUniTensor eigVec = psi;

	try {
		int retry = 0;
		size_t iter = max_iter;
		while (retry < 4) {
			if( !lanczosEVWJT(loc, psi, iter, err_tol, E0, eigVec) ) {
				if (retry < 3) {
					err_tol *= 10.;
					std::cerr << "lanczosEVWJT fails in converging. Tuning up err_tol to " << err_tol << '\n';
					retry += 1;
				}
				else {
					std::ostringstream err;
					err << "Lanczos algorithm fails in converging.";
					throw std::runtime_error(uni10::exception_msg(err.str()));
				}
			}
			else {
				psi = eigVec;
				return iter;
			}
		}
	}
	catch(const std::exception& e) {
		uni10::propogate_exception(e, "In YJuncQnIBC::lanczosEighWJT : ");
		return 0;
	}
}
