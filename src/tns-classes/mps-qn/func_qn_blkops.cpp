#include <map>
#include <stdexcept>
#include <algorithm>    // std::sort, std::find
#include <complex>

#include <tns-func/func_net.h>
#include <tns-func/func_convert.h>
#include <mps-qn/func_qn_net.h>
#include <mps-qn/func_qn_blkops.h>

//=============================================

uni10::CUniTensor real2ComplexQn( uni10::UniTensor& ten ) {
	///
	uni10::CUniTensor cten = uni10::CUniTensor( ten.bond() );
	std::map<uni10::Qnum, uni10::Matrix> blks = ten.getBlocks();

	for (std::map<uni10::Qnum, uni10::Matrix>::iterator it = blks.begin(); it != blks.end(); ++it) {
		uni10::Qnum q = it->first;
		uni10::Matrix blk = it->second;
		cten.putBlock( q, real2Complex(blk) );
	}

	return cten;
}

//======================================

uni10::CUniTensor randTQn( uni10::CUniTensor& cten ) {
	/// randomize a complex tensor having only real part
	uni10::UniTensor rten( cten.bond() );
	rten.randomize();

	return real2ComplexQn( rten );
}

//=============================================

uni10::CMatrix diagMatInv( uni10::CMatrix mat ) {
	///
	int dim = mat.col();

	uni10::CMatrix inv_mat(dim, dim, true);
	for (int i = 0; i < dim; ++i) {
		if ( std::fabs(mat.at(i, i).real()) < 1e-16 )
			inv_mat.at(i, i) = Complex(0.0, 0.0);
		else
			inv_mat.at(i, i) = Complex(1.0/mat.at(i, i).real(), 0.0);
	}

	return inv_mat;
}

//=============================================

uni10::CUniTensor tenInvQn( uni10::CUniTensor& ten ) {
	///
	uni10::CUniTensor inv_ten = uni10::CUniTensor( ten.bond() );
	std::map<uni10::Qnum, uni10::CMatrix> blks = ten.getBlocks();

	for (std::map<uni10::Qnum, uni10::CMatrix>::iterator it = blks.begin(); it != blks.end(); ++it) {
		uni10::Qnum q = it->first;
		uni10::CMatrix blk = it->second;
		try {
			inv_ten.putBlock( q, blk.inverse() );
		}
		catch(const std::exception& e) {
			// zero division error
			inv_ten.putBlock( q, diagMatInv(blk) );
		}
	}

	return inv_ten;
}

//=============================================

uni10::CUniTensor takeExpQn( Complex beta, uni10::CUniTensor& op ) {
	///
	uni10::CUniTensor U( op.bond() );
	std::map<uni10::Qnum, uni10::CMatrix> blks = op.getBlocks();

	for (std::map<uni10::Qnum, uni10::CMatrix>::iterator it = blks.begin(); it != blks.end(); ++it) {
		uni10::Qnum q = it->first;
		uni10::CMatrix blk = it->second;
		U.putBlock( q, uni10::exp( beta, blk ) );
	}

	return U;
}

//=============================================

std::map<uni10::Qnum, std::vector<uni10::CMatrix>> svdQn( uni10::CUniTensor& theta ) {
	///
	std::map<uni10::Qnum, std::vector<uni10::CMatrix>> svd_map;
	std::map<uni10::Qnum, uni10::CMatrix> blks = theta.getBlocks();

	for (std::map<uni10::Qnum, uni10::CMatrix>::iterator it = blks.begin(); it != blks.end(); ++it) {
		uni10::Qnum q = it->first;
		uni10::CMatrix blk = it->second;
		svd_map.insert( std::pair<uni10::Qnum, std::vector<uni10::CMatrix>>(q, blk.svd()) );
	}

	return svd_map;
}

//=============================================

bool compare(double x, double y) { return (x > y); }

//=============================================

bool inQnList(uni10::Qnum q, const std::vector<uni10::Qnum>& qvec) {
	///
	if (qvec.size() == 0)
		return true;
	if (std::find(qvec.begin(), qvec.end(), q) != qvec.end())
		return true;
	return false;
}

//=============================================

void truncateQn( std::map<uni10::Qnum, std::vector<uni10::CMatrix>>& svd_map,
	int chi, double tol, const std::vector<uni10::Qnum>& qvec ) {
	///
	std::vector<double> sch_vals;

	for (std::map<uni10::Qnum, std::vector<uni10::CMatrix>>::iterator it = svd_map.begin(); it != svd_map.end(); ++it) {
		uni10::Qnum q = it->first;
		if (inQnList(q, qvec)) {
			std::vector<uni10::CMatrix> svd = it->second;
			for (int i = 0; i < svd[1].col(); ++i) {
				double sch_val = svd[1][i].real();
				if ( sch_val > tol )
					sch_vals.push_back( sch_val );
			}
		}
	}

	std::sort( sch_vals.begin(), sch_vals.end(), compare );
	chi = std::min( chi, (int)sch_vals.size() );
	double baseline = sch_vals[ chi-1 ];
	double norm = 0.0;
	for (int i = 0; i < chi; ++i)
		norm += sch_vals[i] * sch_vals[i];
	norm = std::sqrt( norm );
	double scale = 1./norm;

	for (std::map<uni10::Qnum, std::vector<uni10::CMatrix>>::iterator it = svd_map.begin(); it != svd_map.end(); ++it) {
		uni10::Qnum q = it->first;
		int chi_q = 0;
		int i = 0;
		if (inQnList(q, qvec)) {
			while ( (i < svd_map.at(q)[1].col()) && !(svd_map.at(q)[1][i].real() < baseline) ) {
				chi_q += 1;
				i += 1;
			}
		}
		svd_map.at(q)[1].resize( chi_q, chi_q );
		if (chi_q > 0)
			svd_map.at(q)[1] *= scale;
		svd_map.at(q)[0].resize( svd_map.at(q)[0].row(), chi_q );
		svd_map.at(q)[2].resize( chi_q, svd_map.at(q)[2].col() );
	}
}

//=============================================

void resturctQn( uni10::CUniTensor& before, uni10::CUniTensor& after ) {
	///
	std::map<uni10::Qnum, uni10::CMatrix> blks = before.getBlocks();

	for (std::map<uni10::Qnum, uni10::CMatrix>::iterator it = blks.begin(); it != blks.end(); ++it) {
		uni10::Qnum q = it->first;
		uni10::CMatrix blk = it->second;
		uni10::CMatrix dest = after.getBlock(q);
		int dest_col = (int)dest.col();
		int dest_row = (int)dest.row();
		after.putBlock( q, blk.resize( dest_row, dest_col ) );
	}
}

//=============================================

uni10::CUniTensor lamMix( uni10::CUniTensor lam, double rnd_scale ) {
	///
	std::srand( time(NULL) );
	uni10::CUniTensor rnd(lam.bond());
	rnd = randTQn(rnd);
	rnd *= rnd_scale;

	uni10::CUniTensor lam_mix = lam + rnd;
	uni10::CUniTensor lam_cpy = lam_mix;
	std::vector<int> lab_lam = {0, 1};
	lam_mix.setLabel(lab_lam);
	lab_lam = {1, 0};
	lam_cpy.setLabel(lab_lam);
	uni10::CUniTensor norm = uni10::contract(lam_mix, lam_cpy, false);
	lam_mix *= 1.0/std::sqrt(norm[0]);

	return lam_mix;
}
