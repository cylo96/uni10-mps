#ifndef FUNC_QN_LA_H
#define FUNC_QN_LA_H
#include <uni10.hpp>

bool myQnLanczosEV(
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	uni10::CUniTensor& psi,
	size_t& max_iter, double err_tol, double& eigVal, uni10::CUniTensor& eigVec );

size_t myQnLanczosEigh( 
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	uni10::CUniTensor& psi,
	double& E0, size_t max_iter, double err_tol );

bool myQnLanczosEV(
	std::vector<uni10::CUniTensor>& mpo, uni10::CUniTensor& psi,
	size_t& max_iter, double err_tol, double& eigVal, uni10::CUniTensor& eigVec );

size_t myQnLanczosEigh( std::vector<uni10::CUniTensor>& mpo, uni10::CUniTensor& psi, 
	double& E0, size_t max_iter, double err_tol );

bool myQnLanczosEV(
	uni10::CUniTensor& op, uni10::CUniTensor& psi,
	size_t& max_iter, double err_tol, double& eigVal, uni10::CUniTensor& eigVec );

size_t myQnLanczosEigh( uni10::CUniTensor& op, uni10::CUniTensor& psi, 
	double& E0, size_t max_iter, double err_tol );

bool myQnArpackDomEig(
	uni10::CUniTensor& ket, uni10::CUniTensor& bra, uni10::CUniTensor& psi,
	uni10::Qnum& qn, size_t n,
	size_t& max_iter, double& eigVal, std::complex<double>* eigVec,
	bool ongpu, double err_tol=0.0e0, int nev=1, bool left=false );

size_t myQnArpLanczosDomEig(
	uni10::CUniTensor& ket, uni10::CUniTensor& psi,
	double& E0, size_t max_iter, double err_tol=1.0e-15, int nev=1, bool left=false );

#endif
