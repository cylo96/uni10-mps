#ifndef FUNC_QN_OP_H
#define FUNC_QN_OP_H
#include <string>
#include <uni10.hpp>

//======================================

uni10::CUniTensor opU1( std::string name );
uni10::CUniTensor opF( std::string name );
uni10::CUniTensor opFU1( std::string name );
uni10::CUniTensor opFU1_grd0( std::string name, int num_particle, int size_uc );
uni10::CUniTensor opFU1_spinful( std::string name, int num_particle, int size_uc );
uni10::CUniTensor otimesPM( uni10::CUniTensor op1, uni10::CUniTensor op2, bool swap = false );
void importMPOQn( int len, std::string mpo_dir,
	std::vector<uni10::CUniTensor>& mpo_sy,
	std::vector<uni10::CUniTensor>& mpo_pl,
	std::vector<uni10::CUniTensor>& mpo_mi,
	bool bdry_dummy = false,
	uni10::CUniTensor bdry_lam0 = uni10::CUniTensor(), uni10::CUniTensor bdry_lamN = uni10::CUniTensor() );

#endif
