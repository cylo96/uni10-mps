#ifndef FUNC_MEAS_H
#define FUNC_MEAS_H
#include <uni10.hpp>

double entanglementEntropy( const uni10::CUniTensor& lam );
uni10::Matrix entanglementSpec( const uni10::CUniTensor& lam, int N );
double correlationEig( std::vector<uni10::CUniTensor>& gam, std::vector<uni10::CUniTensor>& lam );
#endif
