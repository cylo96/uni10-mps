#include <tns-func/func_meas.h>
#include <tns-func/func_net.h>
#include <tns-func/func_la.h>

//======================================

double entanglementEntropy( const uni10::CUniTensor& lam ) {

	uni10::CMatrix mat = lam.getBlock();
	int D = mat.row();

	double S = 0.0, sch_val = 0.0;
	for (int i = 0; i < D; i++) {
		sch_val = mat.at(i, i).real();
		if ( sch_val > 1e-16 )
			S -= ( sch_val * sch_val ) * log( sch_val * sch_val );
	}

	return S;
}

//======================================

uni10::Matrix entanglementSpec( const uni10::CUniTensor& lam, int N ) {

	uni10::Matrix spec(N, N, true);
	spec.set_zero();
	uni10::CMatrix mat = lam.getBlock();

	for (int i = 0; i < N; ++i)
		spec.at(i, i) = mat.at(i, i).real();

	return spec;
}

//======================================

double correlationEig( std::vector<uni10::CUniTensor>& gam, std::vector<uni10::CUniTensor>& lam ) {

	std::vector<double> corrL;

	for (int n = 0; n < 2; ++n){

		int p = (n+1)%2;
		uni10::CUniTensor ket = netLGLG(lam[p], gam[n], lam[n], gam[p]);
		uni10::CUniTensor transM = tranMtx(ket);
		corrL.push_back( (double)-1.0 / std::log( eigenVal( transM.getBlock(), 2 ) ) );
	}

	return 0.5 * (corrL[0] + corrL[1]);
}
