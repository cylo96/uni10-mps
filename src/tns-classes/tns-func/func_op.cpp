#include <tns-func/func_op.h>
#include <tns-func/func_convert.h>
#include <tns-func/tns_const.h>

//======================================

uni10::CUniTensor OP( std::string name ) {

	// define bond parameters
	std::vector< uni10::Bond > bonds_d2;
	std::vector< uni10::Bond > bonds_d3;

	bonds_d2.push_back( uni10::Bond( uni10::BD_IN, 2 ) );
	bonds_d2.push_back( uni10::Bond( uni10::BD_OUT, 2 ) );

	bonds_d3.push_back( uni10::Bond( uni10::BD_IN, 3 ) );
	bonds_d3.push_back( uni10::Bond( uni10::BD_OUT, 3 ) );

	// define elemental matrices
	// spin 1/2; 0.5 h_bar -> 1
	double id_elem[] = {1., 0.,\
						0., 1.};

	double sz_elem[] = {1., 0.,\
						0.,-1.};

	double sx_elem[] = {0., 1.,\
						1., 0.};

	double sp_elem[] = {0., 1.,\
						0., 0.};

	double sm_elem[] = {0., 0.,\
						1., 0.};

	double _isy_elem[]={0., -1.,\
						1., 0.};

	double sig_elem[]= {0., 1.,\
						-1., 0.};

	// spin-1; h_bar -> 1
	double id1_elem[]= {1., 0., 0.,\
						0., 1., 0.,\
						0., 0., 1.};

	double sz1_elem[]= {1., 0., 0.,\
						0., 0., 0.,\
						0., 0., -1.};

	double sx1_elem[]= {0., 1./sqrt(2), 0.,\
						1./sqrt(2), 0., 1./sqrt(2),\
						0., 1./sqrt(2), 0.};

	double sp1_elem[]= {0., sqrt(2), 0.,\
						0., 0., sqrt(2),\
						0., 0., 0.};

	double sm1_elem[]= {0., 0., 0.,\
						sqrt(2), 0., 0.,\
						0., sqrt(2), 0.};

	double _isy1_elem[] = {0., 1./sqrt(2), 0.,\
						-1./sqrt(2), 0., 1./sqrt(2),\
						0., -1./sqrt(2), 0.};

	double szz1_elem[]={1., 0., 0.,\
		 	 			0., 0., 0.,\
			 			0., 0., 1.};

	double sxx1_elem[]={0.5, 0.0, 0.5,\
		 	 		 	0.0, 1.0, 0.0,\
					 	0.5, 0.0, 0.5};

	uni10::UniTensor op;

	if ( name == "id" ) {
		op.assign( bonds_d2 );
		op.identity();
	}
	else if ( name == "sz" ) {
		op.assign( bonds_d2 );
		op.setElem( sz_elem );
	}
	else if ( name == "sx" ) {
		op.assign( bonds_d2 );
		op.setElem( sx_elem );
	}
	else if ( name == "sp" ) {
		op.assign( bonds_d2 );
		op.setElem( sp_elem );
	}
	else if ( name == "sm" ) {
		op.assign( bonds_d2 );
		op.setElem( sm_elem );
	}
	else if ( name == "-isy" ) {
		op.assign( bonds_d2 );
		op.setElem( _isy_elem );
	}
	else if ( name == "sy" ) {
		op.assign( bonds_d2 );
		op.setElem( _isy_elem );
		return I * real2Complex( op );
	}
	else if ( name == "Sz" ) {
		op.assign( bonds_d2 );
		op.setElem( sz_elem );
		op *= 0.5;
	}
	else if ( name == "Sx" ) {
		op.assign( bonds_d2 );
		op.setElem( sx_elem );
		op *= 0.5;
	}
	else if ( name == "Sy" ) {
		op.assign( bonds_d2 );
		op.setElem( _isy_elem );
		op *= 0.5;
		return I * real2Complex( op );
	}
	else if ( name == "Sp" ) {
		op.assign( bonds_d2 );
		op.setElem( sp_elem );
	}
	else if ( name == "Sm" ) {
		op.assign( bonds_d2 );
		op.setElem( sm_elem );
	}
	else if ( name == "sigma" ) {
		op.assign( bonds_d2 );
		op.setElem( sig_elem );
	}
	else if ( name == "id1" ) {
		op.assign( bonds_d3 );
		op.identity();
	}
	else if ( name == "sz1" ) {
		op.assign( bonds_d3 );
		op.setElem( sz1_elem );
	}
	else if ( name == "sx1" ) {
		op.assign( bonds_d3 );
		op.setElem( sx1_elem );
	}
	else if ( name == "sp1" ) {
		op.assign( bonds_d3 );
		op.setElem( sp1_elem );
	}
	else if ( name == "sm1" ) {
		op.assign( bonds_d3 );
		op.setElem( sm1_elem );
	}
	else if ( name == "-isy1" ) {
		op.assign( bonds_d3 );
		op.setElem( _isy1_elem );
	}
	else if ( name == "sy1" ) {
		op.assign( bonds_d3 );
		op.setElem( _isy1_elem );
		return I * real2Complex( op );
	}
	else if ( name == "szz1" ) {
		op.assign( bonds_d3 );
		op.setElem( szz1_elem );
	}
	else if ( name == "sxx1" ) {
		op.assign( bonds_d3 );
		op.setElem( sxx1_elem );
	}

	return real2Complex( op );
}

//======================================

MPO::MPO( int dim, char loc ) {
	///
	virt_dim = dim;
	mpo_loc = loc;

	if ( loc == 'l' ) {
		std::vector< uni10::Bond > bonds_l;
		bonds_l.push_back( uni10::Bond( uni10::BD_OUT, virt_dim ) );
		mpo_frame.assign( bonds_l );
	}
	else if ( loc == 'r' ) {
		std::vector< uni10::Bond > bonds_r;
		bonds_r.push_back( uni10::Bond( uni10::BD_IN, virt_dim ) );
		mpo_frame.assign( bonds_r );
	}
	else {
		std::vector< uni10::Bond > bonds_m;
		bonds_m.push_back( uni10::Bond( uni10::BD_IN, virt_dim ) );
		bonds_m.push_back( uni10::Bond( uni10::BD_OUT, virt_dim ) );
		mpo_frame.assign( bonds_m );
	}

	mpo_frame.set_zero();
}

//======================================

MPO::MPO( std::vector<int>& dim_l, std::vector<int>& dim_r ) {
	///
	std::vector< uni10::Bond > bonds;
	for (int i = 0; i < dim_l.size(); ++i)
		bonds.push_back( uni10::Bond( uni10::BD_IN, dim_l[i] ) );
	for (int i = 0; i < dim_r.size(); ++i)
		bonds.push_back( uni10::Bond( uni10::BD_OUT, dim_r[i] ) );
	mpo_frame.assign( bonds );

	mpo_frame.set_zero();
}

//======================================

void MPO::putTensor( uni10::CUniTensor op, int row_idx, int col_idx ) {
	///
	uni10::CUniTensor location( mpo_frame.bond() );
	location.set_zero();
	uni10::CMatrix temp = location.getBlock();
	temp.at(row_idx, col_idx) = 1.0;
	location.putBlock( temp );

	if ( mpo.bondNum() == 0 )
		mpo = uni10::otimes( location, op );
	else
		mpo = mpo + uni10::otimes( location, op );
}

//======================================

uni10::CUniTensor MPO::launch() {
	///
	if ( mpo.bondNum() == 0 )
		return mpo_frame;
	else
		return mpo;
}

//======================================

uni10::CUniTensor mpoDummy( bool left, int dim_mpo, uni10::CUniTensor bdry_lam ) {
	///
	std::vector<uni10::Bond> bd;

	if ( bdry_lam.bondNum() == 0 ) {
		bd.push_back( uni10::Bond( uni10::BD_IN, 1 ) );
		bd.push_back( uni10::Bond( uni10::BD_OUT, 1 ) );
	}
	else
		bd = bdry_lam.bond();

	uni10::CUniTensor id(bd);
	id.identity();

	if (left) {
		MPO mpo_l(dim_mpo, 'l');
		mpo_l.putTensor( id, 0, dim_mpo-1 );
		return mpo_l.launch();
	}
	else {
		MPO mpo_r(dim_mpo, 'r');
		mpo_r.putTensor( id, 0, 0 );
		return mpo_r.launch();
	}
}

//======================================

void importMPO( int len, std::string mpo_dir, std::vector<uni10::CUniTensor>& mpo, std::string bc ) {
	///
	mpo.clear();
	int mpo_m_size;
	if (bc == "obc")
		mpo_m_size = len-2;
	else if (bc == "semiinf")
		mpo_m_size = len-1;
	else if (bc == "ibc")
		mpo_m_size = len;
	else {
		std::cerr << "In importMPO : Unsupported boundary condition." << '\n';
		return;
	}

	mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_l" ) );
	for (int i = 0; i < mpo_m_size; ++i)
		mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_r" ) );
}
