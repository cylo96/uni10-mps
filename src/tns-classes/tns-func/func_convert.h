#ifndef FUNC_CONVERT_H
#define FUNC_CONVERT_H
#include <uni10.hpp>

uni10::CMatrix real2Complex( uni10::Matrix rM );
uni10::CUniTensor real2Complex( uni10::UniTensor rT );
uni10::Matrix complex2Real( uni10::CMatrix cM );
uni10::UniTensor complex2Real( uni10::CUniTensor cT );
uni10::CUniTensor randT( uni10::CUniTensor cT );
#endif
