#include <uni10.hpp>
#include <tns-func/tns_const.h>

void ibcXXZ( double J, double delta,
    std::vector<uni10::CUniTensor>& AL, std::vector<uni10::CUniTensor>& AR, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& HBL, std::vector<uni10::CUniTensor>& HBR, int at_iter = -1 );

//void ibcXXZ( double J, double delta,
//	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda );

