#include <algorithm>
#include <tns-func/func_op.h>
#include <tns-func/func_net.h>

// todo: rewrite these functions using Network class -- or not?
//======================================

uni10::CUniTensor bondInvGam( uni10::CUniTensor gam ) {

	int lab_old[] = {0, 100, 1};
	int lab_new[] = {1, 100, 0};

	gam.setLabel( lab_old );
	gam.permute( lab_new, 2 );

	return gam;
}

//=============================================

uni10::CUniTensor bondInv( uni10::CUniTensor ten ) {
	///
	int lab_old[] = {0, 1};
	int lab_new[] = {1, 0};
	ten.setLabel( lab_old );
	ten.permute( lab_new, 1 );

	return ten;
}

//=============================================

uni10::CUniTensor netLGLGL(
	uni10::CUniTensor ll, uni10::CUniTensor ga,
	uni10::CUniTensor la, uni10::CUniTensor gb, uni10::CUniTensor lb ) {

	int lab_ll[] = {0, 1};
	int lab_ga[] = {1, 100, 2};
	int lab_la[] = {2, 3};
	int lab_gb[] = {3, 101, 4};
	int lab_lb[] = {4, 5};

	ll.setLabel( lab_ll );
	ga.setLabel( lab_ga );
	la.setLabel( lab_la );
	gb.setLabel( lab_gb );
	lb.setLabel( lab_lb );

	uni10::CUniTensor net = uni10::contract( ll, ga );
	net = uni10::contract( net, la );
	net = uni10::contract( net, gb );
	net = uni10::contract( net, lb );
	net.permute( net.label(), 3 );

	return net;
}

//======================================

uni10::CUniTensor netLGLGL(
	uni10::CUniTensor ll, uni10::CUniTensor gala, uni10::CUniTensor gblb ) {

	int lab_ll[] = {0, 1};
	int lab_gala[] = {1, 100, 3};
	int lab_gblb[] = {3, 101, 5};

	ll.setLabel( lab_ll );
	gala.setLabel( lab_gala );
	gblb.setLabel( lab_gblb );

	uni10::CUniTensor net = uni10::contract( ll, gala );
	net = uni10::contract( gala, gblb );
	net.permute( net.label(), 3 );

	return net;
}

//======================================

uni10::CUniTensor netLG( uni10::CUniTensor la, uni10::CUniTensor ga ) {

	int lab_la[] = {0, 1};
	int lab_ga[] = {1, 100, 2};

	la.setLabel( lab_la );
	ga.setLabel( lab_ga );

	uni10::CUniTensor net = uni10::contract( la, ga );
	net.permute( net.label(), 2 );

	return net;
}

//=====================================

uni10::CUniTensor netGL( uni10::CUniTensor ga, uni10::CUniTensor la ) {

	int lab_ga[] = {0, 100, 1};
	int lab_la[] = {1, 2};

	ga.setLabel( lab_ga );
	la.setLabel( lab_la );

	uni10::CUniTensor net = uni10::contract( ga, la );

	return net;
}

//======================================

uni10::CUniTensor netLL( uni10::CUniTensor la, uni10::CUniTensor lb ) {

	int lab_la[] = {0, 1};
	int lab_lb[] = {1, 2};

	la.setLabel( lab_la );
	lb.setLabel( lab_lb );

	uni10::CUniTensor net = uni10::contract( la, lb );

	return net;
}

//======================================

uni10::CUniTensor netLGLG(
	uni10::CUniTensor ll, uni10::CUniTensor ga,
	uni10::CUniTensor la, uni10::CUniTensor gb ) {

	int lab_ll[] = {0, 1};
	int lab_ga[] = {1, 100, 2};
	int lab_la[] = {2, 3};
	int lab_gb[] = {3, 101, 4};

	ll.setLabel( lab_ll );
	ga.setLabel( lab_ga );
	la.setLabel( lab_la );
	gb.setLabel( lab_gb );

	uni10::CUniTensor net = uni10::contract( ll, ga );
	net = uni10::contract( net, la );
	net = uni10::contract( net, gb );
	net.permute( net.label(), 3 );

	return net;
}

//================================================

uni10::CUniTensor netGLGL(
	uni10::CUniTensor ga, uni10::CUniTensor la,
	uni10::CUniTensor gb, uni10::CUniTensor lb ) {

	int lab_ga[] = {0, 100, 1};
	int lab_la[] = {1, 2};
	int lab_gb[] = {2, 101, 3};
	int lab_lb[] = {3, 4};

	ga.setLabel( lab_ga );
	la.setLabel( lab_la );
	gb.setLabel( lab_gb );
	lb.setLabel( lab_lb );

	uni10::CUniTensor net = uni10::contract( ga, la );
	net = uni10::contract( net, gb );
	net = uni10::contract( net, lb );
	net.permute( net.label(), 3 );

	return net;
}

//===============================================

uni10::CUniTensor netGLGL( uni10::CUniTensor gala, uni10::CUniTensor gblb ) {

	int lab_gala[] = {0, 100, 2};
	int lab_gblb[] = {2, 101, 4};

	gala.setLabel( lab_gala );
	gblb.setLabel( lab_gblb );

	uni10::CUniTensor net = uni10::contract( gala, gblb );
	net.permute( net.label(), 3 );

	return net;
}

//================================================

uni10::CUniTensor netGLG(
	uni10::CUniTensor ga, uni10::CUniTensor la, uni10::CUniTensor gb ) {

	int lab_ga[] = {0, 100, 1};
	int lab_la[] = {1, 2};
	int lab_gb[] = {2, 101, 3};

	ga.setLabel( lab_ga );
	la.setLabel( lab_la );
	gb.setLabel( lab_gb );

	uni10::CUniTensor net = uni10::contract( ga, la );
	net = uni10::contract( net, gb );
	net.permute( net.label(), 3 );

	return net;
}

//================================================

uni10::CUniTensor netLGL(
	uni10::CUniTensor ll, uni10::CUniTensor ga, uni10::CUniTensor lr ) {

	int lab_ll[] = {0, 1};
	int lab_ga[] = {1, 100, 2};
	int lab_lr[] = {2, 3};

	ll.setLabel( lab_ll );
	ga.setLabel( lab_ga );
	lr.setLabel( lab_lr );

	uni10::CUniTensor net = uni10::contract( ll, ga );
	net = uni10::contract( net, lr );

	return net;
}

//================================================

uni10::CUniTensor netLLL(
	uni10::CUniTensor ll, uni10::CUniTensor la, uni10::CUniTensor lr ) {

	int lab_ll[] = {0, 1};
	int lab_la[] = {1, 2};
	int lab_lr[] = {2, 3};

	ll.setLabel( lab_ll );
	la.setLabel( lab_la );
	lr.setLabel( lab_lr );

	uni10::CUniTensor net = uni10::contract( ll, la );
	net = uni10::contract( net, lr );

	return net;
}

//================================================

uni10::CUniTensor net3Site(
	uni10::CUniTensor lam0, uni10::CUniTensor gam0,
	uni10::CUniTensor lam1, uni10::CUniTensor gam1,
	uni10::CUniTensor lam2, uni10::CUniTensor gam2, uni10::CUniTensor lam3 ) {
	///
	// suppose mps2s in netLGLGL form
	uni10::CUniTensor mps2s_l = netLGLG( lam0, gam0, lam1, gam1 );
	uni10::CUniTensor mps2s_r = netLGL( lam2, gam2, lam3 );

	int lab_mpsl[] = {0, 100, 101, 1};
	int lab_mpsr[] = {1, 102, 2};

	mps2s_l.setLabel(lab_mpsl);
	mps2s_r.setLabel(lab_mpsr);

	uni10::CUniTensor net = uni10::contract(mps2s_l, mps2s_r, true);
	net.permute( net.bondNum()-1 );

	return net;
}

//================================================

uni10::CUniTensor net4Site(
	uni10::CUniTensor lam0, uni10::CUniTensor gam0,
	uni10::CUniTensor lam1, uni10::CUniTensor gam1,
	uni10::CUniTensor lam2, uni10::CUniTensor gam2,
	uni10::CUniTensor lam3, uni10::CUniTensor gam3, uni10::CUniTensor lam4 ) {
	///
	// suppose mps2s in netLGLGL form
	uni10::CUniTensor mps2s_l = netLGLG( lam0, gam0, lam1, gam1 );
	uni10::CUniTensor mps2s_r = netLGLGL( lam2, gam2, lam3, gam3, lam4 );

	int lab_mpsl[] = {0, 100, 101, 1};
	int lab_mpsr[] = {1, 102, 103, 2};

	mps2s_l.setLabel(lab_mpsl);
	mps2s_r.setLabel(lab_mpsr);

	uni10::CUniTensor net = uni10::contract(mps2s_l, mps2s_r, true);
	net.permute( net.bondNum()-1 );

	return net;
}

//================================================

uni10::CUniTensor netAA( uni10::CUniTensor A0, uni10::CUniTensor A1 ) {

	int lab_A0[] = {0, 100, 1};
	int lab_A1[] = {1, 101, 2};

	A0.setLabel( lab_A0 );
	A1.setLabel( lab_A1 );

	uni10::CUniTensor net = uni10::contract( A0, A1 );
	net.permute( net.label(), 3 );

	return net;
}

//================================================

uni10::CUniTensor netBB( uni10::CUniTensor B0, uni10::CUniTensor B1 ) {

	int lab_B0[] = {1, 100, 0};
	int lab_B1[] = {2, 101, 1};

	B0.setLabel( lab_B0 );
	B1.setLabel( lab_B1 );

	uni10::CUniTensor net = uni10::contract( B1, B0 );
	net.permute( net.label(), 3 );

	return net;
}

//================================================

uni10::CUniTensor netLdAs( uni10::CUniTensor& L, uni10::CUniTensor& As ) {
	///
	// todo: assert L's bondNum < As
	uni10::CUniTensor Ld = L;
	Ld.conj();

	std::vector<int> lab_Ld;
	for (int i = 0; i < Ld.label().size(); ++i) {
		if (i == 0)
			lab_Ld.push_back(0);
		else if (i == Ld.label().size()-1)
			lab_Ld.push_back(1);
		else
			lab_Ld.push_back(99+i);
	}

	std::vector<int> lab_As;
	for (int i = 0; i < As.label().size(); ++i) {
		if (i == 0)
			lab_As.push_back(0);
		else if (i == As.label().size()-1)
			lab_As.push_back(2);
		else
			lab_As.push_back(99+i);
	}

	Ld.setLabel( lab_Ld );
	As.setLabel( lab_As );

	uni10::CUniTensor net = uni10::contract( Ld, As );
	net.permute( net.label(), net.label().size()-1 );

	return net;
}

//================================================

uni10::CUniTensor netBsRd( uni10::CUniTensor& Bs, uni10::CUniTensor& R ) {
	///
	// todo: assert R's bondNum < Bs
	uni10::CUniTensor Rd = R;
	Rd.conj();

	std::vector<int> lab_Rd;
	for (int i = 0; i < Rd.label().size(); ++i) {
		if (i == 0)
			lab_Rd.push_back(2);
		else if (i == Rd.label().size()-1)
			lab_Rd.push_back(0);
		else
			lab_Rd.push_back(98+Rd.label().size()-i);
	}

	std::vector<int> lab_Bs;
	for (int i = 0; i < Bs.label().size(); ++i) {
		if (i == 0)
			lab_Bs.push_back(1);
		else if (i == Bs.label().size()-1)
			lab_Bs.push_back(0);
		else
			lab_Bs.push_back(98+Bs.label().size()-i);
	}

	Rd.setLabel( lab_Rd );
	Bs.setLabel( lab_Bs );

	uni10::CUniTensor net = uni10::contract( Bs, Rd );
	net.permute( net.label(), net.label().size()-1 );

	return net;
}

//===============================================

uni10::CUniTensor netDMRGH( int loc,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo ) {
	///
	uni10::CUniTensor net;
	uni10::CUniTensor lvec;
	uni10::CUniTensor rvec;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	int L = gamma.size();

	int lab_mpol[] = {100, 10, 101};
	int lab_mpom[] = {10, 100, 11, 101};
	int lab_mpor[] = {11, 100, 101};

	int lab_lv0[] = {0, 1};
	int lab_lvec[]= {0, 10, 1};
	int lab_bra1[]= {0, 100, 2};
	int lab_ket1[]= {1, 101, 3};

	lvec.assign( lambda[0].bond() );
	lvec.identity();
	lvec.permute( lvec.label(), 0 );
	lvec.setLabel( lab_lv0 );

	for (int i = 0; i < loc; ++i) {

		ket = netLG( lambda[i], gamma[i] );
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra1 );
		ket.setLabel( lab_ket1 );
		if (i == 0)
			mpo[i].setLabel( lab_mpol );
		else
			mpo[i].setLabel( lab_mpom );

		lvec = uni10::contract( bra, lvec );
		lvec = uni10::contract( lvec, mpo[i] );
		lvec = uni10::contract( lvec, ket );
		lvec.setLabel( lab_lvec );
	}

	int lab_rv0[] = {2, 3};
	int lab_rvec[]= {2, 11, 3};
	int lab_bra2[]= {0, 100, 2};
	int lab_ket2[]= {1, 101, 3};

	rvec.assign( lambda[L].bond() );
	rvec.identity();
	rvec.permute( rvec.label(), 2 );
	rvec.setLabel( lab_rv0 );

	for (int i = L-1; i > loc; --i) {

		ket = netGL( gamma[i], lambda[i+1] );
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra2 );
		ket.setLabel( lab_ket2 );
		if (i == L-1)
			mpo[i].setLabel( lab_mpor );
		else
			mpo[i].setLabel( lab_mpom );

		rvec = uni10::contract( bra, rvec );
		rvec = uni10::contract( rvec, mpo[i] );
		rvec = uni10::contract( rvec, ket );
		rvec.setLabel( lab_rvec );
	}

	int lab_mpol2[] = {100, 11, 101};
	int lab_mpor2[] = {10, 100, 101};
	if ( loc == 0 )
		mpo[loc].setLabel( lab_mpol2 );
	else if ( loc == L-1 )
		mpo[loc].setLabel( lab_mpor2 );
	else
		mpo[loc].setLabel( lab_mpom );

	net = lvec * mpo[loc] * rvec;
	int lab_net[] = {0, 100, 2, 1, 101, 3};
	net.permute( lab_net, 3 );

	return net;
}

//================================================

uni10::CUniTensor mpoLR( uni10::CUniTensor mpo_l, uni10::CUniTensor mpo_r ) {
	///
	int lab_l[] = {100, 0, 101};
	int lab_r[] = {0, 200, 201};
	int lab_fin[] = {100, 200, 101, 201};

	mpo_l.setLabel( lab_l );
	mpo_r.setLabel( lab_r );

	uni10::CUniTensor op = mpo_l * mpo_r;
	op.permute( lab_fin, 2 );

	return op;
}

//===============================================

uni10::CUniTensor mpoLM( uni10::CUniTensor mpo_l, uni10::CUniTensor mpo_m ) {
	///
	int lab_l[] = {100, 0, 101};
	int lab_m[] = {0, 200, 1, 201};
	int lab_fin[] = {100, 200, 1, 101, 201};

	mpo_l.setLabel( lab_l );
	mpo_m.setLabel( lab_m );

	uni10::CUniTensor op = mpo_l * mpo_m;
	op.permute( lab_fin, 2 );

	return op;
}

//===============================================

uni10::CUniTensor mpoMR( uni10::CUniTensor mpo_m, uni10::CUniTensor mpo_r ) {
	///
	int lab_m[] = {0, 100, 1, 101};
	int lab_r[] = {1, 200, 201};
	int lab_fin[] = {0, 100, 200, 101, 201};

	mpo_m.setLabel( lab_m );
	mpo_r.setLabel( lab_r );

	uni10::CUniTensor op = mpo_m * mpo_r;
	op.permute( lab_fin, 3 );

	return op;
}

//===============================================

uni10::CUniTensor mpoLMMR(
	uni10::CUniTensor mpo_l, uni10::CUniTensor mpo_m, uni10::CUniTensor mpo_r ) {
	///
	int lab_lm[] = {100, 200, 0, 101, 201};
	int lab_mr[] = {0, 300, 400, 301, 401};
	int lab_fin[] = {100, 200, 300, 400, 101, 201, 301, 401};

	uni10::CUniTensor mpo_lm = mpoLM( mpo_l, mpo_m );
	uni10::CUniTensor mpo_mr = mpoMR( mpo_m, mpo_r );

	mpo_lm.setLabel( lab_lm );
	mpo_mr.setLabel( lab_mr );

	uni10::CUniTensor op = mpo_lm * mpo_mr;
	op.permute( lab_fin, 4 );

	return op;
}

//===============================================

uni10::CUniTensor op2SiteFromMPO( uni10::CUniTensor mpo_m ) {
	///
	int dim_mpo = mpo_m.bond()[0].dim();
	uni10::CUniTensor mpo_l = mpoDummy( true, dim_mpo );
	uni10::CUniTensor mpo_r = mpoDummy( false, dim_mpo );
	uni10::CUniTensor op = mpoLMMR( mpo_l, mpo_m, mpo_r );
	std::vector<int> lab_bd0(2), lab_bd1(2), lab_bd2(2), lab_bd3(2);
	lab_bd0[0] = op.label()[0];
	lab_bd0[1] = op.label()[1];
	lab_bd1[0] = op.label()[2];
	lab_bd1[1] = op.label()[3];
	lab_bd2[0] = op.label()[4];
	lab_bd2[1] = op.label()[5];
	lab_bd3[0] = op.label()[6];
	lab_bd3[1] = op.label()[7];
	op.combineBond( lab_bd0 );
	op.combineBond( lab_bd1 );
	op.combineBond( lab_bd2 );
	op.combineBond( lab_bd3 );

	return op;
}

//===============================================

void initDMRGBlk( int loc,
	std::vector<uni10::CUniTensor>& AL, std::vector<uni10::CUniTensor>& AR, uni10::CUniTensor& Cs,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& blk ) {
	///
	int L = AL.size();
	blk.clear();
	for (int i = 0; i < L; ++i)
		blk.push_back( uni10::CUniTensor() );

	uni10::CUniTensor lvec;
	uni10::CUniTensor rvec;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	int lab_mpol1[] = {100, 10, 101};
	int lab_mpor1[] = {10, 100, 101};

	int lab_mpom[] = {10, 100, 11, 101};

	int lab_mpol2[] = {100, 11, 101};
	int lab_mpor2[] = {11, 100, 101};

	int lab_lv0[] = {0, 1};
	int lab_lvec[]= {0, 10, 1};
	int lab_bra1[]= {0, 100, 2};
	int lab_ket1[]= {1, 101, 3};

	std::vector<uni10::Bond> bdi;
	bdi.push_back( uni10::Bond( uni10::BD_IN, 1 ) );
	bdi.push_back( uni10::Bond( uni10::BD_OUT, 1 ) );

	lvec.assign( bdi );
	lvec.identity();
	lvec.permute( lvec.label(), 0 );
	lvec.setLabel( lab_lv0 );

	for (int i = 0; i < loc; ++i) {

		ket = AL[i];
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra1 );
		ket.setLabel( lab_ket1 );
		if (i == 0)
			mpo[i].setLabel( lab_mpol1 );
		else if (i == L-1)
			mpo[i].setLabel( lab_mpor1 );
		else
			mpo[i].setLabel( lab_mpom );

		lvec = uni10::contract( bra, lvec );
		lvec = uni10::contract( lvec, mpo[i] );
		lvec = uni10::contract( lvec, ket );
		lvec.setLabel( lab_lvec );

		blk[i] = lvec;
		blk[i].permute(1);
	}

	int lab_rv0[] = {2, 3};
	int lab_rvec[]= {2, 11, 3};
	int lab_bra2[]= {0, 100, 2};
	int lab_ket2[]= {1, 101, 3};
	int lab_rblk[]= {11, 2, 3};

	rvec.assign( bdi );
	rvec.identity();
	rvec.permute( rvec.label(), 2 );
	rvec.setLabel( lab_rv0 );

	for (int i = L-1; i >= loc; --i) {

		ket = AR[L-1-i];
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra2 );
		ket.setLabel( lab_ket2 );
		if (i == L-1)
			mpo[i].setLabel( lab_mpor2 );
		else if (i == 0)
			mpo[i].setLabel( lab_mpol2 );
		else
			mpo[i].setLabel( lab_mpom );

		rvec = uni10::contract( bra, rvec );
		rvec = uni10::contract( rvec, mpo[i] );
		rvec = uni10::contract( rvec, ket );
		rvec.setLabel( lab_rvec );

		blk[i] = rvec;
		blk[i].permute( lab_rblk, 2 );
	}
}

//===============================================

void initDMRGBlk( int loc,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& blk ) {
	///
	int L = gamma.size();
	blk.clear();
	for (int i = 0; i < L; ++i)
		blk.push_back( uni10::CUniTensor() );

	uni10::CUniTensor lvec;
	uni10::CUniTensor rvec;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	int lab_mpol1[] = {100, 10, 101};
	int lab_mpor1[] = {10, 100, 101};

	int lab_mpom[] = {10, 100, 11, 101};

	int lab_mpol2[] = {100, 11, 101};
	int lab_mpor2[] = {11, 100, 101};

	int lab_lv0[] = {0, 1};
	int lab_lvec[]= {0, 10, 1};
	int lab_bra1[]= {0, 100, 2};
	int lab_ket1[]= {1, 101, 3};

	lvec.assign( lambda[0].bond() );
	lvec.identity();
	lvec.permute( lvec.label(), 0 );
	lvec.setLabel( lab_lv0 );

	int loc_ = std::max(1,loc);

	for (int i = 0; i < loc_; ++i) {

		ket = netLG( lambda[i], gamma[i] );
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra1 );
		ket.setLabel( lab_ket1 );
		if (i == 0)
			mpo[i].setLabel( lab_mpol1 );
		else if (i == L-1)
			mpo[i].setLabel( lab_mpor1 );
		else
			mpo[i].setLabel( lab_mpom );

		lvec = uni10::contract( bra, lvec );
		lvec = uni10::contract( lvec, mpo[i] );
		lvec = uni10::contract( lvec, ket );
		lvec.setLabel( lab_lvec );

		blk[i] = lvec;
		blk[i].permute(1);
	}

	int lab_rv0[] = {2, 3};
	int lab_rvec[]= {2, 11, 3};
	int lab_bra2[]= {0, 100, 2};
	int lab_ket2[]= {1, 101, 3};
	int lab_rblk[]= {11, 2, 3};

	rvec.assign( lambda[L].bond() );
	rvec.identity();
	rvec.permute( rvec.label(), 2 );
	rvec.setLabel( lab_rv0 );

	for (int i = L-1; i >= loc_; --i) {

		ket = netGL( gamma[i], lambda[i+1] );
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra2 );
		ket.setLabel( lab_ket2 );
		if (i == L-1)
			mpo[i].setLabel( lab_mpor2 );
		else if (i == 0)
			mpo[i].setLabel( lab_mpol2 );
		else
			mpo[i].setLabel( lab_mpom );

		rvec = uni10::contract( bra, rvec );
		rvec = uni10::contract( rvec, mpo[i] );
		rvec = uni10::contract( rvec, ket );
		rvec.setLabel( lab_rvec );

		blk[i] = rvec;
		blk[i].permute( lab_rblk, 2 );
	}
}

//===============================================

void refreshDMRGBlk( int loc,
	std::vector<uni10::CUniTensor>& AL, std::vector<uni10::CUniTensor>& AR, uni10::CUniTensor& Cs,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& blk ) {
	///
	// assume boundary blk has been updated
	int L = AL.size();

	uni10::CUniTensor lvec;
	uni10::CUniTensor rvec;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	int lab_mpol1[] = {100, 10, 101};
	int lab_mpor1[] = {10, 100, 101};

	int lab_mpom[] = {10, 100, 11, 101};

	int lab_mpol2[] = {100, 11, 101};
	int lab_mpor2[] = {11, 100, 101};

	int lab_lv0[] = {0, 1};
	int lab_lvec[]= {0, 10, 1};
	int lab_bra1[]= {0, 100, 2};
	int lab_ket1[]= {1, 101, 3};

	std::vector<uni10::Bond> bdi;
	bdi.push_back( uni10::Bond( uni10::BD_IN, 1 ) );
	bdi.push_back( uni10::Bond( uni10::BD_OUT, 1 ) );

	lvec.assign( bdi );
	lvec.identity();
	lvec.permute( lvec.label(), 0 );
	lvec.setLabel( lab_lv0 );

	for (int i = 0; i < loc; ++i) {

		ket = AL[i];
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra1 );
		ket.setLabel( lab_ket1 );
		if (i == 0)
			mpo[i].setLabel( lab_mpol1 );
		else if (i == L-1)
			mpo[i].setLabel( lab_mpor1 );
		else
			mpo[i].setLabel( lab_mpom );

		lvec = uni10::contract( bra, lvec );
		lvec = uni10::contract( lvec, mpo[i] );
		lvec = uni10::contract( lvec, ket );
		lvec.setLabel( lab_lvec );

		blk[i] = lvec;
		blk[i].permute(1);
	}

	int lab_rv0[] = {2, 3};
	int lab_rvec[]= {2, 11, 3};
	int lab_bra2[]= {0, 100, 2};
	int lab_ket2[]= {1, 101, 3};
	int lab_rblk[]= {11, 2, 3};

	rvec.assign( bdi );
	rvec.identity();
	rvec.permute( rvec.label(), 2 );
	rvec.setLabel( lab_rv0 );

	for (int i = L-1; i >= loc; --i) {

		ket = AR[L-1-i];
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra2 );
		ket.setLabel( lab_ket2 );
		if (i == L-1)
			mpo[i].setLabel( lab_mpor2 );
		else if (i == 0)
			mpo[i].setLabel( lab_mpol2 );
		else
			mpo[i].setLabel( lab_mpom );

		rvec = uni10::contract( bra, rvec );
		rvec = uni10::contract( rvec, mpo[i] );
		rvec = uni10::contract( rvec, ket );
		rvec.setLabel( lab_rvec );

		blk[i] = rvec;
		blk[i].permute( lab_rblk, 2 );
	}
}

//===============================================

void refreshDMRGBlk( int loc,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& blk ) {
	///
	// assume boundary blk has been updated
	int L = gamma.size();

	uni10::CUniTensor lvec;
	uni10::CUniTensor rvec;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	int lab_mpol1[] = {100, 10, 101};
	int lab_mpor1[] = {10, 100, 101};

	int lab_mpom[] = {10, 100, 11, 101};

	int lab_mpol2[] = {100, 11, 101};
	int lab_mpor2[] = {11, 100, 101};

	int lab_lv0[] = {0, 1};
	int lab_lvec[]= {0, 10, 1};
	int lab_bra1[]= {0, 100, 2};
	int lab_ket1[]= {1, 101, 3};

	lvec.assign( lambda[0].bond() );
	lvec.identity();
	lvec.permute( lvec.label(), 0 );
	lvec.setLabel( lab_lv0 );

	for (int i = 0; i < loc; ++i) {

		ket = netLG( lambda[i], gamma[i] );
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra1 );
		ket.setLabel( lab_ket1 );
		if (i == 0)
			mpo[i].setLabel( lab_mpol1 );
		else if (i == L-1)
			mpo[i].setLabel( lab_mpor1 );
		else
			mpo[i].setLabel( lab_mpom );

		lvec = uni10::contract( bra, lvec );
		lvec = uni10::contract( lvec, mpo[i] );
		lvec = uni10::contract( lvec, ket );
		lvec.setLabel( lab_lvec );

		blk[i] = lvec;
		blk[i].permute(1);
	}

	int lab_rv0[] = {2, 3};
	int lab_rvec[]= {2, 11, 3};
	int lab_bra2[]= {0, 100, 2};
	int lab_ket2[]= {1, 101, 3};
	int lab_rblk[]= {11, 2, 3};

	rvec.assign( lambda[L].bond() );
	rvec.identity();
	rvec.permute( rvec.label(), 2 );
	rvec.setLabel( lab_rv0 );

	for (int i = L-1; i >= loc; --i) {

		ket = netGL( gamma[i], lambda[i+1] );
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra2 );
		ket.setLabel( lab_ket2 );
		if (i == L-1)
			mpo[i].setLabel( lab_mpor2 );
		else if (i == 0)
			mpo[i].setLabel( lab_mpol2 );
		else
			mpo[i].setLabel( lab_mpom );

		rvec = uni10::contract( bra, rvec );
		rvec = uni10::contract( rvec, mpo[i] );
		rvec = uni10::contract( rvec, ket );
		rvec.setLabel( lab_rvec );

		blk[i] = rvec;
		blk[i].permute( lab_rblk, 2 );
	}
}

//===============================================

void updateDMRGBlk( int loc, bool swp_right,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& blk ) {
	///
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	int lab_mpol1[] = {100, 10, 101};
	int lab_mpor1[] = {10, 100, 101};

	int lab_mpom[] = {10, 100, 11, 101};

	int lab_mpol2[] = {100, 11, 101};
	int lab_mpor2[] = {11, 100, 101};

	int lab_lvec[]= {0, 10, 1};
	int lab_bra1[]= {0, 100, 2};
	int lab_ket1[]= {1, 101, 3};

	int lab_rvec[]= {2, 11, 3};
	int lab_bra2[]= {0, 100, 2};
	int lab_ket2[]= {1, 101, 3};
	int lab_rblk[]= {11, 2, 3};

	if (swp_right) {

		ket = netLG( lambda[loc], gamma[loc] );
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra1 );
		ket.setLabel( lab_ket1 );
		if (loc == 0)
			mpo[loc].setLabel( lab_mpol1 );
		else if (loc == gamma.size()-1)
			mpo[loc].setLabel( lab_mpor1 );
		else
			mpo[loc].setLabel( lab_mpom );

		blk[loc] = uni10::contract( bra, blk[loc-1] );
		blk[loc] = uni10::contract( blk[loc], mpo[loc] );
		blk[loc] = uni10::contract( blk[loc], ket );
		blk[loc].setLabel( lab_lvec );
		blk[loc].permute(1);
	}
	else {

		ket = netGL( gamma[loc], lambda[loc+1] );
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra2 );
		ket.setLabel( lab_ket2 );
		if (loc == gamma.size()-1)
			mpo[loc].setLabel( lab_mpor2 );
		else if (loc == 0)
			mpo[loc].setLabel( lab_mpol2 );
		else
			mpo[loc].setLabel( lab_mpom );

		blk[loc] = uni10::contract( bra, blk[loc+1] );
		blk[loc] = uni10::contract( blk[loc], mpo[loc] );
		blk[loc] = uni10::contract( blk[loc], ket );
		blk[loc].setLabel( lab_rvec );
		blk[loc].permute( lab_rblk, 2 );
	}
}

//===============================================

void updateDMRGBlk( int loc, bool swp_right, int unit_cell,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& blk ) {
	///
	int L = gamma.size();

	uni10::CUniTensor lvec;
	uni10::CUniTensor rvec;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	int lab_mpol1[] = {100, 10, 101};
	int lab_mpor1[] = {10, 100, 101};

	int lab_mpom[] = {10, 100, 11, 101};

	int lab_mpol2[] = {100, 11, 101};
	int lab_mpor2[] = {11, 100, 101};

	int lab_lv0[] = {0, 1};
	int lab_lvec[]= {0, 10, 1};
	int lab_bra1[]= {0, 100, 2};
	int lab_ket1[]= {1, 101, 3};

	lvec.assign( lambda[0].bond() );
	lvec.identity();
	lvec.permute( lvec.label(), 0 );
	lvec.setLabel( lab_lv0 );

	int lab_rv0[] = {2, 3};
	int lab_rvec[]= {2, 11, 3};
	int lab_bra2[]= {0, 100, 2};
	int lab_ket2[]= {1, 101, 3};
	int lab_rblk[]= {11, 2, 3};

	rvec.assign( lambda[L].bond() );
	rvec.identity();
	rvec.permute( rvec.label(), 2 );
	rvec.setLabel( lab_rv0 );

	if (swp_right) {

		ket = netLG( lambda[loc], gamma[loc] );
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra1 );
		ket.setLabel( lab_ket1 );
		if (loc == 0)
			mpo[loc].setLabel( lab_mpol1 );
		else if (loc == L-1)
			mpo[loc].setLabel( lab_mpor1 );
		else
			mpo[loc].setLabel( lab_mpom );

		if (loc == 0)
			blk[loc] = uni10::contract( bra, lvec );
		else
			blk[loc] = uni10::contract( bra, blk[loc-1] );

		blk[loc] = uni10::contract( blk[loc], mpo[loc] );	// fail at loc == 0
		blk[loc] = uni10::contract( blk[loc], ket );
		blk[loc].setLabel( lab_lvec );
		blk[loc].permute(1);

		for (int i = loc+unit_cell-1; i > loc; --i) {
			ket = netGL( gamma[i], lambda[i+1] );
			bra = ket;
			bra.conj();

			bra.setLabel( lab_bra2 );
			ket.setLabel( lab_ket2 );
			if (i == L-1)
				mpo[i].setLabel( lab_mpor2 );
			else if (i == 0)
				mpo[i].setLabel( lab_mpol2 );
			else
				mpo[i].setLabel( lab_mpom );

			if (i == L-1) {
				blk[i] = uni10::contract( bra, rvec );
				blk[i] = uni10::contract( blk[i], mpo[i] );	// fail at boundary
				blk[i] = uni10::contract( blk[i], ket );
				blk[i].setLabel( lab_rvec );
				blk[i].permute( lab_rblk, 2 );
			}
			else {
				blk[i] = uni10::contract( bra, blk[i+1] );
				blk[i] = uni10::contract( blk[i], mpo[i] );
				blk[i] = uni10::contract( blk[i], ket );
				blk[i].setLabel( lab_rvec );
				blk[i].permute( lab_rblk, 2 );
			}
		}
	}
	else {

		ket = netGL( gamma[loc], lambda[loc+1] );
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra2 );
		ket.setLabel( lab_ket2 );
		if (loc == L-1)
			mpo[loc].setLabel( lab_mpor2 );
		else if (loc == 0)
			mpo[loc].setLabel( lab_mpol2 );
		else
			mpo[loc].setLabel( lab_mpom );

		if ( loc == L-1 )
		{
			blk[loc] = uni10::contract( bra, rvec );
			blk[loc] = uni10::contract( blk[loc], mpo[loc] );
			blk[loc] = uni10::contract( blk[loc], ket );
			blk[loc].setLabel( lab_rvec );
			blk[loc].permute( lab_rblk, 2 );
		}
		else {
			blk[loc] = uni10::contract( bra, blk[loc+1] );
			blk[loc] = uni10::contract( blk[loc], mpo[loc] );
			blk[loc] = uni10::contract( blk[loc], ket );
			blk[loc].setLabel( lab_rvec );
			blk[loc].permute( lab_rblk, 2 );
		}

		for (int i = loc-unit_cell+1; i < loc; ++i) {
			ket = netLG( lambda[i], gamma[i] );
			bra = ket;
			bra.conj();

			bra.setLabel( lab_bra1 );
			ket.setLabel( lab_ket1 );
			if (i == 0)
				mpo[i].setLabel( lab_mpol1 );
			else if (i == L-1)
				mpo[i].setLabel( lab_mpor1 );
			else
				mpo[i].setLabel( lab_mpom );

			if (i == 0) {
				blk[i] = uni10::contract( bra, lvec );
				blk[i] = uni10::contract( blk[i], mpo[i] );
				blk[i] = uni10::contract( blk[i], ket );
				blk[i].setLabel( lab_lvec );
				blk[i].permute(1);
			}
			else {
				blk[i] = uni10::contract( bra, blk[i-1] );
				blk[i] = uni10::contract( blk[i], mpo[i] );
				blk[i] = uni10::contract( blk[i], ket );
				blk[i].setLabel( lab_lvec );
				blk[i].permute(1);
			}
		}
	}
}

//===============================================

void mpoDMRGH( int loc, bool swp_right, int unit_cell,
	std::vector<uni10::CUniTensor>& AL, std::vector<uni10::CUniTensor>& AR, uni10::CUniTensor& Cs,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& mpoH,
	std::vector<uni10::CUniTensor>& blk ) {
	///
	int L = AL.size();

	if ( mpoH.size() != (2+unit_cell) ) {
		mpoH.clear();
		mpoH.push_back( mpo[0] );
		for (int i = 0; i < unit_cell; ++i)
			mpoH.push_back( mpo[1] );
		mpoH.push_back( mpo[ mpo.size()-1 ] );
	}

	if (blk.size() == 0) {
		initDMRGBlk( loc, AL, AR, Cs, mpo, blk );	// <--
	}

	uni10::CUniTensor lvec;
	uni10::CUniTensor rvec;

	std::vector<uni10::Bond> bdi;
	bdi.push_back( uni10::Bond( uni10::BD_IN, 1 ) );
	bdi.push_back( uni10::Bond( uni10::BD_OUT, 1 ) );

	int lab_lv0[] = {0, 1};
	lvec.assign( bdi );
	lvec.identity();
	lvec.permute( lvec.label(), 1 );
	lvec.setLabel( lab_lv0 );

	int lab_rv0[] = {2, 3};
	rvec.assign( bdi );
	rvec.identity();
	rvec.permute( rvec.label(), 1 );
	rvec.setLabel( lab_rv0 );

	if (swp_right) {
		if ( loc == 0 ) {
			mpoH[0] = lvec;
			for (int i = 0; i < unit_cell; ++i)
				mpoH[i+1] = mpo[loc+i];
			mpoH[ mpoH.size()-1 ] = blk[loc+unit_cell];
		}
		else if ( loc == L-unit_cell ) {
			mpoH[0] = blk[loc-1];
			for (int i = 0; i < unit_cell; ++i)
				mpoH[i+1] = mpo[loc+i];
			mpoH[ mpoH.size()-1 ] = rvec;
		}
		else {
			mpoH[0] = blk[loc-1];
			for (int i = 0; i < unit_cell; ++i)
				mpoH[i+1] = mpo[loc+i];
			mpoH[ mpoH.size()-1 ] = blk[loc+unit_cell];
		}
	}
	else {
		if ( loc == unit_cell-1 ) {
			mpoH[0] = lvec;
			for (int i = 1; i <= unit_cell; ++i)
				mpoH[i] = mpo[loc-unit_cell+i];
			mpoH[ mpoH.size()-1 ] = blk[loc+1];
		}
		else if ( loc == L-1 ) {
			mpoH[0] = blk[loc-unit_cell];
			for (int i = 1; i <= unit_cell; ++i)
				mpoH[i] = mpo[loc-unit_cell+i];
			mpoH[ mpoH.size()-1 ] = rvec;
		}
		else {
			mpoH[0] = blk[loc-unit_cell];
			for (int i = 1; i <= unit_cell; ++i)
				mpoH[i] = mpo[loc-unit_cell+i];
			mpoH[ mpoH.size()-1 ] = blk[loc+1];
		}
	}
}

//===============================================

void mpoDMRGH( int loc, bool swp_right, int unit_cell,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& mpoH,
	std::vector<uni10::CUniTensor>& blk ) {
	///
	int L = gamma.size();

	if ( mpoH.size() != (2+unit_cell) ) {
		mpoH.clear();
		mpoH.push_back( mpo[0] );
		for (int i = 0; i < unit_cell; ++i)
			mpoH.push_back( mpo[1] );
		mpoH.push_back( mpo[ mpo.size()-1 ] );
	}

	if (blk.size() == 0) {
		initDMRGBlk( loc, gamma, lambda, mpo, blk );
	}

	uni10::CUniTensor lvec;
	uni10::CUniTensor rvec;

	int lab_lv0[] = {0, 1};
	lvec.assign( lambda[0].bond() );
	lvec.identity();
	lvec.permute( lvec.label(), 1 );
	lvec.setLabel( lab_lv0 );

	int lab_rv0[] = {2, 3};
	rvec.assign( lambda[L].bond() );
	rvec.identity();
	rvec.permute( rvec.label(), 1 );
	rvec.setLabel( lab_rv0 );

	if (swp_right) {
		if ( loc == 0 ) {
			mpoH[0] = lvec;	// dummy mpo_l for OBC dmrg's first site update. IBC update starts from loc=1
			for (int i = 0; i < unit_cell; ++i)
				mpoH[i+1] = mpo[loc+i];
			mpoH[ mpoH.size()-1 ] = blk[loc+unit_cell];
		}
		else if ( loc == L-unit_cell ) {
			mpoH[0] = blk[loc-1];
			for (int i = 0; i < unit_cell; ++i)
				mpoH[i+1] = mpo[loc+i];
			mpoH[ mpoH.size()-1 ] = rvec;
		}
		else {
			mpoH[0] = blk[loc-1];
			for (int i = 0; i < unit_cell; ++i)
				mpoH[i+1] = mpo[loc+i];
			mpoH[ mpoH.size()-1 ] = blk[loc+unit_cell];
		}
	}
	else {
		if ( loc == unit_cell-1 ) {
			mpoH[0] = lvec;
			for (int i = 1; i <= unit_cell; ++i)
				mpoH[i] = mpo[loc-unit_cell+i];
			mpoH[ mpoH.size()-1 ] = blk[loc+1];
		}
		else if ( loc == L-1 ) {
			mpoH[0] = blk[loc-unit_cell];
			for (int i = 1; i <= unit_cell; ++i)
				mpoH[i] = mpo[loc-unit_cell+i];
			mpoH[ mpoH.size()-1 ] = rvec;
		}
		else {
			mpoH[0] = blk[loc-unit_cell];
			for (int i = 1; i <= unit_cell; ++i)
				mpoH[i] = mpo[loc-unit_cell+i];
			mpoH[ mpoH.size()-1 ] = blk[loc+1];
		}
	}
}

//===============================================

void mpoDMRGH( int loc,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& mpoH ) {
	///
	if (mpoH.size() != 3) {
		mpoH.clear();
		mpoH.push_back( mpo[0] );
		mpoH.push_back( mpo[1] );
		mpoH.push_back( mpo[ mpo.size()-1 ] );
	}

	uni10::CUniTensor lvec;
	uni10::CUniTensor rvec;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	int L = gamma.size();

	int lab_mpol[] = {100, 10, 101};
	int lab_mpom[] = {10, 100, 11, 101};
	int lab_mpor[] = {11, 100, 101};

	int lab_lv0[] = {0, 1};
	int lab_lvec[]= {0, 10, 1};
	int lab_bra1[]= {0, 100, 2};
	int lab_ket1[]= {1, 101, 3};

	lvec.assign( lambda[0].bond() );
	lvec.identity();
	lvec.permute( lvec.label(), 0 );
	lvec.setLabel( lab_lv0 );

	for (int i = 0; i < loc; ++i) {

		ket = netLG( lambda[i], gamma[i] );
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra1 );
		ket.setLabel( lab_ket1 );
		if (i == 0)
			mpo[i].setLabel( lab_mpol );
		else
			mpo[i].setLabel( lab_mpom );

		lvec = uni10::contract( bra, lvec );
		lvec = uni10::contract( lvec, mpo[i] );
		lvec = uni10::contract( lvec, ket );
		lvec.setLabel( lab_lvec );
	}

	int lab_rv0[] = {2, 3};
	int lab_rvec[]= {2, 11, 3};
	int lab_bra2[]= {0, 100, 2};
	int lab_ket2[]= {1, 101, 3};

	rvec.assign( lambda[L].bond() );
	rvec.identity();
	rvec.permute( rvec.label(), 2 );
	rvec.setLabel( lab_rv0 );

	for (int i = L-1; i > loc; --i) {

		ket = netGL( gamma[i], lambda[i+1] );
		bra = ket;
		bra.conj();

		bra.setLabel( lab_bra2 );
		ket.setLabel( lab_ket2 );
		if (i == L-1)
			mpo[i].setLabel( lab_mpor );
		else
			mpo[i].setLabel( lab_mpom );

		rvec = uni10::contract( bra, rvec );
		rvec = uni10::contract( rvec, mpo[i] );
		rvec = uni10::contract( rvec, ket );
		rvec.setLabel( lab_rvec );
	}

	lvec.permute( lvec.label(), 1 );
	if ( rvec.label().size() > 2 ) {
		int lab_rvec2[] = {11, 2, 3};
		rvec.permute( lab_rvec2, 2 );
	}

	mpoH[0] = lvec;
	mpoH[1] = mpo[loc];
	mpoH[2] = rvec;
}

//===============================================

uni10::CUniTensor mpoMatVec( int size,
	std::vector<uni10::CUniTensor>& mpoH, uni10::CUniTensor& psi ) {
	///
	// todo: assert size = 2, 3, 4, 5, 6
	uni10::CUniTensor net;

	if (size == 2) {  // mpo[0]=mpo_l mpo[1]=mpo_r, both have 3 bonds

		int lab_l[] = {100, 0, 101};
		int lab_r[] = {0, 200, 201};
		mpoH[0].setLabel( lab_l );
		mpoH[1].setLabel( lab_r );

		int lab_psi[] = {101, 201};
		int lab_net[] = {100, 200};
		psi.setLabel( lab_psi );
		net = uni10::contract( mpoH[0], psi );
		net = uni10::contract( mpoH[1], net );
		net.permute( lab_net, 2 );
	}
	else if (size == 3) {	// <-- fails when both mpo[0] mpo[2] are both 2 bonds

		if (mpoH[0].bondNum() == 2) {
			int lab_l[] = {100, 101};
			int lab_m[] = {200, 1, 201};
			int lab_r[] = {1, 300, 301};
			mpoH[0].setLabel( lab_l );
			mpoH[1].setLabel( lab_m );
			mpoH[2].setLabel( lab_r );
		}

		else if (mpoH[2].bondNum() == 2) {
			int lab_l[] = {100, 0, 101};
			int lab_m[] = {0, 200, 201};
			int lab_r[] = {300, 301};
			mpoH[0].setLabel( lab_l );
			mpoH[1].setLabel( lab_m );
			mpoH[2].setLabel( lab_r );
		}

		else {
			int lab_l[] = {100, 0, 101};
			int lab_m[] = {0, 200, 1, 201};
			int lab_r[] = {1, 300, 301};
			mpoH[0].setLabel( lab_l );
			mpoH[1].setLabel( lab_m );
			mpoH[2].setLabel( lab_r );
		}

		int lab_psi[] = {101, 201, 301};
		int lab_net[] = {100, 200, 300};
		psi.setLabel( lab_psi );
		net = uni10::contract( mpoH[0], psi );
		net = uni10::contract( mpoH[1], net );
		net = uni10::contract( mpoH[2], net );
		net.permute( lab_net, 3 );
	}

	else if (size == 4) {	// <-- fails when both mpo[0] mpo[3] are both 2 bonds

		if (mpoH[0].bondNum() == 2) {
			int lab_l[] = {100, 101};
			int lab_m1[] = {200, 1, 201};
			int lab_m2[] = {1, 300, 2, 301};
			int lab_r[] = {2, 400, 401};
			mpoH[0].setLabel( lab_l );
			mpoH[1].setLabel( lab_m1 );
			mpoH[2].setLabel( lab_m2 );
			mpoH[3].setLabel( lab_r );
		}

		else if (mpoH[3].bondNum() == 2) {
			int lab_l[] = {100, 0, 101};
			int lab_m1[] = {0, 200, 1, 201};
			int lab_m2[] = {1, 300, 301};
			int lab_r[] = {400, 401};
			mpoH[0].setLabel( lab_l );
			mpoH[1].setLabel( lab_m1 );
			mpoH[2].setLabel( lab_m2 );
			mpoH[3].setLabel( lab_r );
		}

		else {
			int lab_l[] = {100, 0, 101};
			int lab_m1[] = {0, 200, 1, 201};
			int lab_m2[] = {1, 300, 2, 301};
			int lab_r[] = {2, 400, 401};
			mpoH[0].setLabel( lab_l );
			mpoH[1].setLabel( lab_m1 );
			mpoH[2].setLabel( lab_m2 );
			mpoH[3].setLabel( lab_r );
		}

		int lab_psi[] = {101, 201, 301, 401};
		int lab_net[] = {100, 200, 300, 400};
		psi.setLabel( lab_psi );
		net = uni10::contract( mpoH[0], psi );
		net = uni10::contract( mpoH[1], net );
		net = uni10::contract( mpoH[2], net );
		net = uni10::contract( mpoH[3], net );
		net.permute( lab_net, 4 );
	}

	else if (size == 5) {	// <-- fails when both mpo[0] mpo[4] are both 2 bonds

		if (mpoH[0].bondNum() == 2) {
			int lab_l[] = {100, 101};
			int lab_m1[] = {200, 1, 201};
			int lab_m2[] = {1, 300, 2, 301};
			int lab_m3[] = {2, 400, 3, 401};
			int lab_r[] = {3, 500, 501};
			mpoH[0].setLabel( lab_l );
			mpoH[1].setLabel( lab_m1 );
			mpoH[2].setLabel( lab_m2 );
			mpoH[3].setLabel( lab_m3 );
			mpoH[4].setLabel( lab_r );
		}

		else if (mpoH[4].bondNum() == 2) {
			int lab_l[] = {100, 0, 101};
			int lab_m1[] = {0, 200, 1, 201};
			int lab_m2[] = {1, 300, 2, 301};
			int lab_m3[] = {2, 400, 401};
			int lab_r[] = {500, 501};
			mpoH[0].setLabel( lab_l );
			mpoH[1].setLabel( lab_m1 );
			mpoH[2].setLabel( lab_m2 );
			mpoH[3].setLabel( lab_m3 );
			mpoH[4].setLabel( lab_r );
		}

		else {
			int lab_l[] = {100, 0, 101};
			int lab_m1[] = {0, 200, 1, 201};
			int lab_m2[] = {1, 300, 2, 301};
			int lab_m3[] = {2, 400, 3, 401};
			int lab_r[] = {3, 500, 501};
			mpoH[0].setLabel( lab_l );
			mpoH[1].setLabel( lab_m1 );
			mpoH[2].setLabel( lab_m2 );
			mpoH[3].setLabel( lab_m3 );
			mpoH[4].setLabel( lab_r );
		}

		int lab_psi[] = {101, 201, 301, 401, 501};
		int lab_net[] = {100, 200, 300, 400, 500};
		psi.setLabel( lab_psi );
		net = uni10::contract( mpoH[0], psi );
		net = uni10::contract( mpoH[1], net );
		net = uni10::contract( mpoH[2], net );
		net = uni10::contract( mpoH[3], net );
		net = uni10::contract( mpoH[4], net );
		net.permute( lab_net, 5 );
  }

	else if (size == 6) {	// <-- fails when both mpo[0] mpo[5] are both 2 bonds

		if (mpoH[0].bondNum() == 2) {
			int lab_l[] = {100, 101};
			int lab_m1[] = {200, 1, 201};
			int lab_m2[] = {1, 300, 2, 301};
			int lab_m3[] = {2, 400, 3, 401};
			int lab_m4[] = {3, 500, 4, 501};
			int lab_r[] = {4, 600, 601};
			mpoH[0].setLabel( lab_l );
			mpoH[1].setLabel( lab_m1 );
			mpoH[2].setLabel( lab_m2 );
			mpoH[3].setLabel( lab_m3 );
			mpoH[4].setLabel( lab_m4 );
			mpoH[5].setLabel( lab_r );
		}

		else if (mpoH[5].bondNum() == 2) {
			int lab_l[] = {100, 0, 101};
			int lab_m1[] = {0, 200, 1, 201};
			int lab_m2[] = {1, 300, 2, 301};
			int lab_m3[] = {2, 400, 3, 401};
			int lab_m4[] = {3, 500, 501};
			int lab_r[] = {600, 601};
			mpoH[0].setLabel( lab_l );
			mpoH[1].setLabel( lab_m1 );
			mpoH[2].setLabel( lab_m2 );
			mpoH[3].setLabel( lab_m3 );
			mpoH[4].setLabel( lab_m4 );
			mpoH[5].setLabel( lab_r );
		}

		else {
			int lab_l[] = {100, 0, 101};
			int lab_m1[] = {0, 200, 1, 201};
			int lab_m2[] = {1, 300, 2, 301};
			int lab_m3[] = {2, 400, 3, 401};
			int lab_m4[] = {3, 500, 4, 501};
			int lab_r[] = {4, 600, 601};
			mpoH[0].setLabel( lab_l );
			mpoH[1].setLabel( lab_m1 );
			mpoH[2].setLabel( lab_m2 );
			mpoH[3].setLabel( lab_m3 );
			mpoH[4].setLabel( lab_m4 );
			mpoH[5].setLabel( lab_r );
		}

		int lab_psi[] = {101, 201, 301, 401, 501, 601};
		int lab_net[] = {100, 200, 300, 400, 500, 600};
		psi.setLabel( lab_psi );
		net = uni10::contract( mpoH[0], psi );
		net = uni10::contract( mpoH[1], net );
		net = uni10::contract( mpoH[2], net );
		net = uni10::contract( mpoH[3], net );
		net = uni10::contract( mpoH[4], net );
		net = uni10::contract( mpoH[5], net );
		net.permute( lab_net, 6 );
  }

	else
		std::cerr << "mpoMatVec: lattice size should be 2, 3, 4, 5 or 6\n";

	return net;
}

//===============================================

uni10::CUniTensor tranMatVec(
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	uni10::CUniTensor& vec, bool left ) {
	///
	int lat_size = gamma.size();	// let it be <= 4
	uni10::CUniTensor net;

	std::vector<uni10::CUniTensor> gam_d;
	std::vector<uni10::CUniTensor> lam_d;
	for (int n = 0; n < lat_size; ++n) {
		gam_d.push_back( gamma[n] );
		lam_d.push_back( lambda[n] );
		gam_d[n].conj();
	}

	if (left) {
		for (int i = 0; i < lat_size; ++i) {
			int lab_gam[] = {2*i, 100+i, 2*i+1};
			int lab_lam[] = {2*i-1, 2*i};
			int lab_gamd[] = {2*i+10, 100+i, 2*i+1+10};
			int lab_lamd[] = {2*i-1+10, 2*i+10};
			gamma[i].setLabel( lab_gam );
			gam_d[i].setLabel( lab_gamd );
			lambda[i].setLabel( lab_lam );
			lam_d[i].setLabel( lab_lamd );
		}

		int lab_vec[] = {9, -1};
		vec.setLabel( lab_vec );
		net = uni10::contract( lambda[0], vec, false );
		net = uni10::contract( net, lam_d[0], false );
		for (int j = 1; j < lat_size; ++j) {
			net = uni10::contract( net, gam_d[j-1], false );
			net = uni10::contract( net, gamma[j-1], false );
			net = uni10::contract( lambda[j], net, false );
			net = uni10::contract( net, lam_d[j], false );
		}
		net = uni10::contract( net, gam_d[lat_size-1], false );
		net = uni10::contract( net, gamma[lat_size-1], false );

		net.permute( net.bondNum() );
	}
	else {
		int lab_lam0[] = {2*lat_size-1, 2*lat_size};
		int lab_lamd0[] = {2*lat_size-1+10, 2*lat_size+10};
		for (int i = 0; i < lat_size; ++i) {
			int lab_gam[] = {2*i, 100+i, 2*i+1};
			int lab_lam[] = {2*i-1, 2*i};
			int lab_gamd[] = {2*i+10, 100+i, 2*i+1+10};
			int lab_lamd[] = {2*i-1+10, 2*i+10};
			gamma[i].setLabel( lab_gam );
			gam_d[i].setLabel( lab_gamd );
			if (i == 0) {
				lambda[i].setLabel( lab_lam0 );
				lam_d[i].setLabel( lab_lamd0 );
			}
			else {
				lambda[i].setLabel( lab_lam );
				lam_d[i].setLabel( lab_lamd );
			}
		}

		int lab_vec[] = {2*lat_size+10, 2*lat_size};
		vec.setLabel( lab_vec );
		net = uni10::contract( lam_d[0], vec, false );
		net = uni10::contract( net, lambda[0], false );
		for (int j = 1; j < lat_size; ++j) {
			net = uni10::contract( gam_d[lat_size-j], net, false );
			net = uni10::contract( net, gamma[lat_size-j], false );
			net = uni10::contract( lam_d[lat_size-j], net, false );
			net = uni10::contract( net, lambda[lat_size-j], false );
		}
		net = uni10::contract( gam_d[0], net, false );
		net = uni10::contract( net, gamma[0], false );

		net.permute( net.bondNum() );
	}

	return net;
}

//===============================================

uni10::CUniTensor tranMtx( uni10::CUniTensor ket ) {

	// todo: assert bondNum >= 3
	uni10::CUniTensor bra = ket;
	bra.permute(ket.label(), 1).conj();

	int bn = ket.bondNum();
	std::vector<int> lab_ket;
	std::vector<int> lab_bra;

	for (int i = 0; i < bn; ++i) {

		if (i == 0 || i == bn-1) {
			lab_ket.push_back(i);
			lab_bra.push_back(i+1);
		}
		else {
			lab_ket.push_back(i+100);
			lab_bra.push_back(i+100);
		}
	}

	ket.setLabel( lab_ket );
	bra.setLabel( lab_bra );

	uni10::CUniTensor net = uni10::contract( bra, ket );

	int lab_net[4];
	lab_net[0] = net.label()[0];
	lab_net[1] = net.label()[2];
	lab_net[2] = net.label()[1];
	lab_net[3] = net.label()[3];
	net.permute( lab_net, 2 );

	return net;
}

//===============================================

uni10::CUniTensor tranMtx( uni10::CUniTensor ket, uni10::CUniTensor op ) {

	// todo: assert bondNum >= 3
	// todo: assert op is one-site operator
	uni10::CUniTensor bra = ket;
	bra.permute(ket.label(), 1).conj();

	int bn = ket.bondNum();
	std::vector<int> lab_ket;
	std::vector<int> lab_bra;
	std::vector<int> lab_op;

	for (int i = 0; i < bn; ++i) {

		if ( op.bondNum() == 2 ) {
			if (i == 0 || i == bn-1) {
				lab_ket.push_back(i);
				lab_bra.push_back(i+1);
			}
			else if (i == 1) {
				lab_ket.push_back(i+100);
				lab_bra.push_back(i+200);
				lab_op.push_back(i+200);
				lab_op.push_back(i+100);
			}
			else {
				lab_ket.push_back(i+100);
				lab_bra.push_back(i+100);
			}
		}

		else if ( op.bondNum() == 4 ) {
			if (i == 0 || i == bn-1) {
				lab_ket.push_back(i);
				lab_bra.push_back(i+1);
			}
			else if (i == 1 || i == 2) {
				lab_ket.push_back(i+100);
				lab_bra.push_back(i+200);
				lab_op.push_back(i+200);
				lab_op.push_back(i+100);
			}
			else {
				lab_ket.push_back(i+100);
				lab_bra.push_back(i+100);
			}
		}
	}

	if ( op.bondNum() == 4 ) {
		std::swap( lab_op[1], lab_op[2] );
	}

	ket.setLabel( lab_ket );
	bra.setLabel( lab_bra );
	op.setLabel( lab_op );

	uni10::CUniTensor net = uni10::contract(op, ket);
	net = uni10::contract(bra, net);

	int lab_net[4];
	lab_net[0] = net.label()[0];
	lab_net[1] = net.label()[2];
	lab_net[2] = net.label()[1];
	lab_net[3] = net.label()[3];
	net.permute( lab_net, 2 );

	return net;
}

//===============================================

/// Construct the transfer matrix which length = 2^n unit cells
uni10::CUniTensor tranMtxChain( uni10::CUniTensor ket, int n ) {

	uni10::CUniTensor net = tranMtx( ket );
	uni10::CUniTensor cell = tranMtx( ket );

	int lab1[] = {0, 1, 2, 3};
	int lab2[] = {2, 3, 4, 5};

	for (int i = 0; i < n; ++i){

		net.setLabel( lab1 );
		cell.setLabel( lab2 );

		net = uni10::contract( net, cell, false );
		cell = net;
	}

	return net;
}

//===============================================

uni10::CUniTensor tensorOp( uni10::CUniTensor ket, uni10::CUniTensor op ) {

	// todo: assert ket.bondNum() == 3 or 4
	// todo: assert op is one-site or two-site operator
	int lab_3b[] = {0, 100, 2};
	int lab_4b[] = {0, 100, 101, 5};
	int lab_op1[] = {200, 100};
	int lab_op2[] = {200, 201, 100, 101};
	int lab_net_3b[] = {0, 200, 2};
	int lab_net_4b1[] = {0, 200, 101, 5};
	int lab_net_4b2[] = {0, 200, 201, 5};

	uni10::CUniTensor net;

	if (op.bondNum() == 4 && ket.bondNum() == 4) {

		op.setLabel( lab_op2 );
		ket.setLabel( lab_4b );
		net = uni10::contract( op, ket );
		net.permute( lab_net_4b2, 3 );
	}
	else if (op.bondNum() == 2) {

		if (ket.bondNum() == 3) {
			op.setLabel( lab_op1 );
			ket.setLabel( lab_3b );
			net = uni10::contract( op, ket );
			net.permute( lab_net_3b, 2 );
		}
		else if (ket.bondNum() == 4) {
			op.setLabel( lab_op1 );
			ket.setLabel( lab_4b );
			net = uni10::contract( op, ket );
			net.permute( lab_net_4b1, 3 );
		}
	}

	return net;
}

//=============================================

uni10::CUniTensor norm( uni10::CUniTensor ket ) {

	// todo: assert 3 <= ket.bondNum() <= ?
	uni10::CUniTensor bra = ket;
	bra.permute(ket.label(), 1).conj();

	uni10::CUniTensor net = uni10::contract(bra, ket);

	return net;
}

//=============================================

uni10::CUniTensor traceRhoVec( uni10::CUniTensor vec, uni10::CUniTensor lam, bool left ) {
	///
	int lab_vec[] = {0, 1};
	int lab_ll0[] = {0, 2};
	int lab_ll1[] = {1, 2};
	int lab_lr0[] = {2, 0};
	int lab_lr1[] = {2, 1};

	uni10::CUniTensor net;

	if (left) {
		vec.setLabel( lab_vec );
		lam.setLabel( lab_ll0 );
		net = uni10::contract( lam, vec, false );
		lam.setLabel( lab_ll1 );
		net = uni10::contract( net, lam, false );
		return net;
	}
	else {
		vec.setLabel( lab_vec );
		lam.setLabel( lab_lr0 );
		net = uni10::contract( lam, vec, false );
		lam.setLabel( lab_lr1 );
		net = uni10::contract( net, lam, false );
		return net;
	}
}

//===============================================

uni10::CUniTensor expect( uni10::CUniTensor ket, uni10::CUniTensor op ) {

	// todo: assert ket.bondNum() == 3 or 4
	uni10::CUniTensor bra = ket;
	bra.permute(ket.label(), 1).conj();

	uni10::CUniTensor net = tensorOp( ket, op );
	bra.setLabel( net.label() );
	net = uni10::contract( bra, net );

	return net;
}

//=============================================

uni10::CUniTensor expectOBCLongContract(
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	uni10::CUniTensor op, int op_location ) {

	uni10::CUniTensor net;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	int lab_vec[]= {0, 1};
	int lab_bra1[]= {0, 100, 2};
	int lab_ket1[]= {1, 100, 3};
	int lab_bra2[]= {0, 100, 101, 2};
	int lab_ket2[]= {1, 100, 101, 3};

	net = netLL( lambda[0], lambda[0] );
	net.permute( net.label(), 0 );
	net.setLabel( lab_vec );

	int L = gamma.size();

	for (int i = 0; i < L; ++i) {

		if ( i == op_location ) {

			if ( op.bondNum() == 2 ) {
				ket = netGL( gamma[i], lambda[i+1] );
				bra = ket;
				bra.permute( bra.label(), 1 ).conj();

				ket = tensorOp( ket, op );
				ket.setLabel( lab_ket1 );
				bra.setLabel( lab_bra1 );
			}
			else if ( op.bondNum() == 4 ) {
				ket = netGLGL( gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				bra = ket;
				bra.permute( bra.label(), 1 ).conj();

				ket = tensorOp( ket, op );
				ket.setLabel( lab_ket2 );
				bra.setLabel( lab_bra2 );
				i += 1;
			}
		}
		else {
			ket = netGL( gamma[i], lambda[i+1] );
			bra = ket;
			bra.permute( bra.label(), 1 ).conj();
			ket.setLabel( lab_ket1 );
			bra.setLabel( lab_bra1 );
		}

		net = uni10::contract( net, ket );
		net = uni10::contract( bra, net );
		net.setLabel( lab_vec );
	}

	uni10::CUniTensor id = lambda[L];
	id.identity();
	id.permute( id.label(), 2 );

	net.setLabel( lab_vec );
	id.setLabel( lab_vec );
	net = uni10::contract( net, id );

	return net;
}

//=================================================

uni10::CUniTensor expectPBCLongContract(
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	uni10::CUniTensor op, int op_location ) {

	uni10::CUniTensor net;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	int lab_vec[]= {0, 1};
	int lab_bra1[]= {0, 100, 2};
	int lab_ket1[]= {1, 100, 3};
	int lab_bra2[]= {0, 100, 101, 2};
	int lab_ket2[]= {1, 100, 101, 3};

	int L = gamma.size();

	net = netLL( lambda[L-1], lambda[L-1] );	// need conj ?
	net.permute( net.label(), 0 );
	net.setLabel( lab_vec );

	for (int i = 0; i < L; ++i) {

		if ( i == op_location ) {

			if ( op.bondNum() == 2 ) {
				ket = netGL( gamma[i], lambda[i+1] );
				bra = ket;
				bra.permute( bra.label(), 1 ).conj();

				ket = tensorOp( ket, op );
				ket.setLabel( lab_ket1 );
				bra.setLabel( lab_bra1 );
			}
			else if ( op.bondNum() == 4 ) {
				ket = netGLGL( gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				bra = ket;
				bra.permute( bra.label(), 1 ).conj();

				ket = tensorOp( ket, op );
				ket.setLabel( lab_ket2 );
				bra.setLabel( lab_bra2 );
				i += 1;
			}
		}
		else {
			ket = netGL( gamma[i], lambda[i+1] );
			bra = ket;
			bra.permute( bra.label(), 1 ).conj();
			ket.setLabel( lab_ket1 );
			bra.setLabel( lab_bra1 );
		}

		net = uni10::contract( net, ket );
		net = uni10::contract( bra, net );
		net.setLabel( lab_vec );
	}

	uni10::CUniTensor id = lambda[L-1];
	id.identity();
	id.permute( id.label(), 2 );

	net.setLabel( lab_vec );
	id.setLabel( lab_vec );
	net = uni10::contract( net, id );

	return net;
}

//=================================================

uni10::CUniTensor correlationOBC(
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	uni10::CUniTensor op1, uni10::CUniTensor op2, int x1, int x2 ) {

	uni10::CUniTensor net;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	int lab_vec[]= {0, 1};
	int lab_bra1[]= {0, 100, 2};
	int lab_ket1[]= {1, 100, 3};
	int lab_bra2[]= {0, 100, 101, 2};
	int lab_ket2[]= {1, 100, 101, 3};

	net = netLL( lambda[0], lambda[0] );
	net.permute( net.label(), 0 );
	net.setLabel( lab_vec );

	int L = gamma.size();

	for (int i = 0; i < L; ++i) {

		if ( i == x1 ) {

			if ( op1.bondNum() == 2 ) {
				ket = netGL( gamma[i], lambda[i+1] );
				bra = ket;
				bra.permute( bra.label(), 1 ).conj();

				ket = tensorOp( ket, op1 );
				ket.setLabel( lab_ket1 );
				bra.setLabel( lab_bra1 );
			}
			else if ( op1.bondNum() == 4 ) {
				ket = netGLGL( gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				bra = ket;
				bra.permute( bra.label(), 1 ).conj();

				ket = tensorOp( ket, op1 );
				ket.setLabel( lab_ket2 );
				bra.setLabel( lab_bra2 );
				i += 1;
			}
		}
		else if ( i == x2 ) {

			if ( op2.bondNum() == 2 ) {
				ket = netGL( gamma[i], lambda[i+1] );
				bra = ket;
				bra.permute( bra.label(), 1 ).conj();

				ket = tensorOp( ket, op2 );
				ket.setLabel( lab_ket1 );
				bra.setLabel( lab_bra1 );
			}
			else if ( op2.bondNum() == 4 ) {
				ket = netGLGL( gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				bra = ket;
				bra.permute( bra.label(), 1 ).conj();

				ket = tensorOp( ket, op2 );
				ket.setLabel( lab_ket2 );
				bra.setLabel( lab_bra2 );
				i += 1;
			}
		}
		else {
			ket = netGL( gamma[i], lambda[i+1] );
			bra = ket;
			bra.permute( bra.label(), 1 ).conj();
			ket.setLabel( lab_ket1 );
			bra.setLabel( lab_bra1 );
		}

		net = uni10::contract( net, ket );
		net = uni10::contract( bra, net );
		net.setLabel( lab_vec );
	}

	uni10::CUniTensor id = lambda[L];
	id.identity();
	id.permute( id.label(), 2 );

	net.setLabel( lab_vec );
	id.setLabel( lab_vec );
	net = uni10::contract( net, id );

	return net;
}

//=================================================

std::tuple<uni10::CUniTensor, uni10::CUniTensor, uni10::CUniTensor> thetaSVD(
	uni10::CUniTensor& theta, int chi_max, int ibn, double sv_cutoff, bool fix_chi) {
	///
	if (ibn > 0)
		theta.permute(ibn);
	std::vector<uni10::CMatrix> svd = theta.getBlock().svd();

	std::vector<double> sch_vals;
	for (int i = 0; i < svd[1].col(); ++i) {
		double sch_val = svd[1][i].real();
		if ( sch_val > sv_cutoff )
			sch_vals.push_back( sch_val );
	}
	int chi = (fix_chi)? chi_max : std::min( (int)sch_vals.size(), chi_max );

	svd[0].resize(svd[0].row(), chi);
	svd[1].resize(chi, chi);
	svd[1] *= (1.0/svd[1].norm());
	svd[2].resize(chi, svd[2].col());

	uni10::CUniTensor U, S, V;
	int bn = theta.bondNum(); ibn = theta.inBondNum();
	std::vector<uni10::Bond> bdth = theta.bond();
	std::vector<uni10::Bond> bdu, bds, bdv;
	for (int i = 0; i < ibn; ++i)
		bdu.push_back(bdth[i]);
	bdu.push_back(uni10::Bond(uni10::BD_OUT, chi));
	bds.push_back(uni10::Bond(uni10::BD_IN, chi));
	bds.push_back(uni10::Bond(uni10::BD_OUT, chi));
	bdv.push_back(uni10::Bond(uni10::BD_IN, chi));
	for (int i = ibn; i < bn; ++i)
		bdv.push_back(bdth[i]);
	U.assign(bdu); S.assign(bds); V.assign(bdv);
	U.putBlock(svd[0]); S.putBlock(svd[1]); V.putBlock(svd[2]);

	return std::make_tuple(U, S, V);
}
