#ifndef FUNC_LA_H
#define FUNC_LA_H
#include <uni10.hpp>

double eigenVal( const uni10::CMatrix& mat, int nth );
void mtxEighDecomp( uni10::CUniTensor hM,
    uni10::CUniTensor& V, uni10::CUniTensor& D, uni10::CUniTensor& Vi );

bool myLanczosEV(
	std::vector<uni10::CUniTensor>& mpo, uni10::CUniTensor& psi_t,
	size_t dim, size_t& max_iter, double err_tol,
	double& eigVal, std::complex<double>* eigVec );

size_t myLanczosEigh( std::vector<uni10::CUniTensor>& mpo, uni10::CUniTensor& psi_t, 
	double& E0, size_t max_iter, double err_tol );

bool myArpackEigh( std::vector<uni10::CUniTensor>& mpo, uni10::CUniTensor& psi_t, size_t n, 
	size_t& max_iter, double& eigVal, std::complex<double>* eigVec, bool ongpu, 
	double err_tol=0.0e0, int nev=1 );

size_t myArpLanczosEigh( std::vector<uni10::CUniTensor>& mpo, uni10::CUniTensor& psi_t, 
	double& E0, size_t max_iter, double err_tol );

bool myArpackDomEig( std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	uni10::CUniTensor& psi_t, size_t n, 
	size_t& max_iter, double& eigVal, std::complex<double>* eigVec, bool ongpu, 
	double err_tol=0.0e0, int nev=1, bool left = false );

size_t myArpLanczosDomEig( std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	uni10::CUniTensor& psi_t, double& E0, size_t max_iter,
	double err_tol=1.0e-15, int nev=1, bool left = false );

#endif
