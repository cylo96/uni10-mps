#include <uni10/numeric/uni10_lapack.h>
#include <tns-func/uni10_arpack_wrapper.h>
#include <tns-func/func_la.h>

#include <stdexcept>
#include <uni10/tools/uni10_tools.h>
#ifdef MKL
	#define MKL_Complex16 std::complex<double>
	#include <mkl.h>
#else
	#include <uni10/numeric/uni10_lapack_wrapper.h>
#endif

#include <tns-func/func_net.h>

//=================================================

/* for qsort */
int compare_function(const void *a, const void *b) {
	double *x = (double *) a;
	double *y = (double *) b;
	if (*x > *y) return -1;
	else if (*x < *y) return 1;
	else return 0;
}

//=================================================

double eigenVal( const uni10::CMatrix& mat, int nth ) {

	std::vector<uni10::CMatrix> rets = mat.eig();

	int N = rets[0].row();
	double *tmp = new double[N];

	for (int j=0; j<N; ++j)
		tmp[j] = sqrt( rets[0].at(j,j).real()*rets[0].at(j,j).real()
					 + rets[0].at(j,j).imag()*rets[0].at(j,j).imag() );

	qsort(tmp, N, sizeof(double), compare_function);

	int i = nth - 1;
	double eigenV = tmp[i];

	return eigenV;
}

//=================================================

void mtxEighDecomp( uni10::CUniTensor hM,
	uni10::CUniTensor& V, uni10::CUniTensor& D, uni10::CUniTensor& Vi ) {
	///
	std::vector<uni10::CMatrix> DU = hM.getBlock().eigh();

	V.assign( hM.bond() );
	D.assign( hM.bond() );
	Vi.assign( hM.bond() );

	D.putBlock( DU[0] );
	Vi.putBlock( DU[1] );
	V.putBlock( DU[1].cTranspose() );
}

//=================================================

bool myLanczosEV(
	std::vector<uni10::CUniTensor>& mpo, uni10::CUniTensor& psi_t,
	size_t dim, size_t& max_iter, double err_tol,
	double& eigVal, std::complex<double>* eigVec ) {
	///
	int N = (int) dim;
	const int min_iter = 2;
	const double beta_err = 1E-15;
	if(!(max_iter > min_iter)){
		std::ostringstream err;
		err<<"Maximum iteration number should be set greater than 2.";
		throw std::runtime_error(uni10::exception_msg(err.str()));
	}
	std::complex<double> a = 1.0;
	std::complex<double> alpha_tmp=0.;
	double alpha;
	double beta = 1;
	int inc = 1;
	size_t M = max_iter;
	std::complex<double> *Vm = (std::complex<double>*)malloc((M + 1) * N * sizeof(std::complex<double>));
	double *As = (double*)malloc(M * sizeof(double));
	double *Bs = (double*)malloc(M * sizeof(double));
	double *d = (double*)malloc(M * sizeof(double));
	double *e = (double*)malloc(M * sizeof(double));
	int it = 0;

	//std::complex<double>* psi = psi_t.getBlock().getElem();	// <--
	//memcpy(Vm, psi, N * sizeof(std::complex<double>));
	//uni10::vectorScal(1 / uni10::vectorNorm(psi, N, 1, false), Vm, N, false);
	memcpy(Vm, psi_t.getBlock().getElem(), N * sizeof(std::complex<double>));
	uni10::vectorScal(1 / uni10::vectorNorm(psi_t.getBlock().getElem(), N, 1, false), Vm, N, false);
	memset(&Vm[(it+1) * N], 0, N * sizeof(std::complex<double>));
	memset(As, 0, M * sizeof(double));
	memset(Bs, 0, M * sizeof(double));

	uni10::CUniTensor trial( psi_t.bond() );	// <--

	double e_diff = 1;
	double e0_old = 0;
	bool converged = false;
	while((((e_diff > err_tol) && it < max_iter) || it < min_iter) && beta > beta_err) {
		std::complex<double> minus_beta = -beta;

//		zgemv((char*)"T", &N, &N, &a, A, &N, &Vm[it * N], &inc, &minus_beta, &Vm[(it+1) * N], &inc); // <-- optimize this
// <--
		psi_t.setElem( &Vm[it * N] );
		trial.setElem( &Vm[(it+1) * N] );
		trial *= minus_beta;
		trial += a * mpoMatVec( psi_t.bondNum(), mpo, psi_t );
		memcpy( &Vm[(it+1) * N], trial.getBlock().getElem(), N * sizeof(std::complex<double>) );
// <--
		zdotc(&alpha_tmp, &N, &Vm[it*N], &inc, &Vm[(it+1) * N], &inc);
		alpha=alpha_tmp.real();
		std::complex<double> minus_alpha = -alpha;
		zaxpy(&N, &minus_alpha, &Vm[it * N], &inc, &Vm[(it+1) * N], &inc);
		beta = uni10::vectorNorm(&Vm[(it+1) * N], N, 1, false);
		if(it < max_iter - 1)
			memcpy(&Vm[(it + 2) * N], &Vm[it * N], N * sizeof(std::complex<double>));
		As[it] = alpha;
		if(beta > beta_err) {
			uni10::vectorScal(1/beta, &Vm[(it+1) * N], N, false);
			if(it < max_iter - 1)
				Bs[it] = beta;
		}
		else
			converged = true;
		it++;
		if(it > 1) {
			double* z = NULL;
			double* work = NULL;
			int info;
			memcpy(d, As, it * sizeof(double));
			memcpy(e, Bs, it * sizeof(double));
			dstev((char*)"N", &it, d, e, z, &it, work, &info);
			if(info != 0) {
				std::ostringstream err;
				err<<"Error in Lapack function 'dstev': Lapack INFO = "<<info;
				throw std::runtime_error(uni10::exception_msg(err.str()));
			}
			double base = std::abs(d[0]) > 1 ? std::abs(d[0]) : 1;
			e_diff = std::abs(d[0] - e0_old) / base;
			e0_old = d[0];
			if(e_diff <= err_tol)
				converged = true;
		}
	}
	if(it > 1) {
		memcpy(d, As, it * sizeof(double));
		memcpy(e, Bs, it * sizeof(double));
		double* z = (double*)malloc(it * it * sizeof(double));
		double* work = (double*)malloc(4 * it * sizeof(double));
		int info;
		dstev((char*)"V", &it, d, e, z, &it, work, &info);
		if(info != 0) {
			std::ostringstream err;
			err<<"Error in Lapack function 'dstev': Lapack INFO = "<<info;
			throw std::runtime_error(uni10::exception_msg(err.str()));
		}
		memset(eigVec, 0, N * sizeof(std::complex<double>));
		std::complex<double> cz;
		for(int k = 0; k < it; k++) {
			cz = z[k];
			zaxpy(&N, &cz, &Vm[k * N], &inc, eigVec, &inc);
		}
		max_iter = it;
		eigVal = d[0];
		free(z), free(work);
	}
	else {
		max_iter = 1;
		eigVal = 0;
	}
	free(Vm), free(As), free(Bs), free(d), free(e);
	return converged;
}

//=================================================

size_t myLanczosEigh( std::vector<uni10::CUniTensor>& mpo, uni10::CUniTensor& psi_t, double& E0,
	size_t max_iter, double err_tol ) {
	///
	psi_t.permute( psi_t.label(), psi_t.bondNum() );
	uni10::CMatrix psi = psi_t.getBlock();
	size_t Rnum = psi.elemNum();
	std::complex<double> *psi_elem = psi.getElem();

	try {
		size_t iter = max_iter;
		if( !myLanczosEV(mpo, psi_t, Rnum, iter, err_tol, E0, psi_elem) ) {
			std::ostringstream err;
			err<<"Lanczos algorithm fails in converging.";;
			throw std::runtime_error(uni10::exception_msg(err.str()));
		}
		//psi.setElem( psi_elem );
		psi_t.putBlock( psi );
		return iter;
	}
	catch(const std::exception& e) {
		uni10::propogate_exception(e, "In function myLanczosEigh: ");
		return 0;
	}
}

//=================================================

bool myArpackEigh( std::vector<uni10::CUniTensor>& mpo, uni10::CUniTensor& psi_t, size_t n,
	size_t& max_iter, double& eigVal, std::complex<double>* eigVec, bool ongpu,
	double err_tol, int nev ) {
	///
	int dim = n;
	int ido = 0;
	char bmat = 'I';
	char which[] = {'S','R'};// smallest real part

	//std::complex<double>* psi = psi_t.getBlock().getElem(); // <--

	std::complex<double> *resid = new std::complex<double>[dim];
	//memcpy(resid, psi, dim * sizeof(std::complex<double>));
	memcpy(resid, psi_t.getBlock().getElem(), dim * sizeof(std::complex<double>));

	int ncv = 42;
	if ( dim < ncv )
		ncv = dim;
	int ldv = dim;
	std::complex<double> *v = new std::complex<double>[ldv*ncv];
	int *iparam = new int[11];
	iparam[0] = 1;
	iparam[2] = max_iter;
	iparam[6] = 1;
	int *ipntr = new int[14];// Different from real version
	std::complex<double> *workd = new std::complex<double>[3*dim];
	int lworkl = 3*ncv*(ncv+2);// LWORKL must be at least 3*NCV**2 + 5*NCV.*/
	std::complex<double> *workl = new std::complex<double>[lworkl];
	double *rwork = new double[ncv];
	int info = 1;
	// Parameters for zgemv
	std::complex<double> alpha(1.0e0, 0.0e0);
	std::complex<double> beta(0.0e0, 0.0e0);
	int inc = 1;

	uni10::CUniTensor trial( psi_t.bond() );	// <--

	znaupd_(&ido, &bmat, &dim, &which[0], &nev, &err_tol, resid, &ncv, v, &ldv,
		iparam, ipntr, workd, workl, &lworkl, rwork, &info);

	while ( ido != 99 ) {
		//zgemv((char*)"T", &dim, &dim, &alpha, A, &dim, workd+ipntr[0]-1, &inc, &beta,
		//	workd+ipntr[1]-1, &inc);
// <--
		psi_t.setElem( workd+ipntr[0]-1 );
		trial.setElem( workd+ipntr[1]-1 );
		trial *= beta;
		trial += alpha * mpoMatVec( psi_t.bondNum(), mpo, psi_t );
		memcpy( workd+ipntr[1]-1, trial.getBlock().getElem(), dim * sizeof(std::complex<double>) );
// <--
		znaupd_(&ido, &bmat, &dim, &which[0], &nev, &err_tol, resid, &ncv, v, &ldv,
			iparam, ipntr, workd, workl, &lworkl, rwork, &info);
	}

	if ( info < 0 )
		std::cerr << "Error with znaupd, info = " << info << std::endl;
	else if ( info == 1 )
		std::cerr << "Maximum number of Lanczos iterations reached." << std::endl;
	else if ( info == 3 )
		std::cerr << "No shifts could be applied during implicit Arnoldi update,"
				<< " try increasing NCV." << std::endl;

	// zneupd Parameters
	int rvec = 1;
	char howmny = 'A';
	int *select = new int[ncv];
	std::complex<double> *d = new std::complex<double>[nev+1];
	std::complex<double> *z = new std::complex<double>[dim*nev];
	std::complex<double> sigma;
	std::complex<double> *workev = new std::complex<double>[2*ncv];
	zneupd_(&rvec, &howmny, select, d, z, &ldv, &sigma, workev,
		&bmat, &dim, &which[0], &nev, &err_tol, resid, &ncv, v, &ldv,
		iparam, ipntr, workd, workl, &lworkl, rwork, &info);
	if ( info != 0 )
		std::cerr << "Error with dneupd, info = " << info << std::endl;
	eigVal = d[0].real();
	memcpy(eigVec, z, dim * sizeof(std::complex<double>));

	delete [] workev;
	delete [] z;
	delete [] d;
	delete [] select;
	delete [] rwork;
	delete [] workl;
	delete [] workd;
	delete [] ipntr;
	delete [] iparam;
	delete [] v;
	delete [] resid;

	return (info == 0);
}

//=================================================

size_t myArpLanczosEigh( std::vector<uni10::CUniTensor>& mpo, uni10::CUniTensor& psi_t,
	double& E0, size_t max_iter, double err_tol ) {
	///
	psi_t.permute( psi_t.label(), psi_t.bondNum() );
	uni10::CMatrix psi = psi_t.getBlock();
	size_t Rnum = psi.elemNum();
	std::complex<double> *psi_elem = psi.getElem();

	try {
		size_t iter = max_iter;
		if( !myArpackEigh(mpo, psi_t, Rnum, iter, E0, psi_elem, false, err_tol) ) {
			std::ostringstream err;
			err<<"Lanczos algorithm fails in converging.";
			throw std::runtime_error(uni10::exception_msg(err.str()));
		}
		//psi.setElem( psi_elem );
		psi_t.putBlock( psi );
		return iter;
	}
	catch(const std::exception& e) {
		uni10::propogate_exception(e, "In function myArpLanczosEigh: ");
		return 0;
	}
}

//=================================================

bool myArpackDomEig( std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	uni10::CUniTensor& psi_t, size_t n,
	size_t& max_iter, double& eigVal, std::complex<double>* eigVec, bool ongpu,
	double err_tol, int nev, bool left ) {
	///
	int dim = n;
	int ido = 0;
	char bmat = 'I';
	char which[] = {'L','R'};// smallest real part

	//std::complex<double>* psi = psi_t.getBlock().getElem(); // <--

	std::complex<double> *resid = new std::complex<double>[dim];
	//memcpy(resid, psi, dim * sizeof(std::complex<double>));
	memcpy(resid, psi_t.getBlock().getElem(), dim * sizeof(std::complex<double>));

	int ncv = 42;
	if ( dim < ncv )
		ncv = dim;
	int ldv = dim;
	std::complex<double> *v = new std::complex<double>[ldv*ncv];
	int *iparam = new int[11];
	iparam[0] = 1;
	iparam[2] = max_iter;
	iparam[6] = 1;
	int *ipntr = new int[14];// Different from real version
	std::complex<double> *workd = new std::complex<double>[3*dim];
	int lworkl = 3*ncv*(ncv+2);// LWORKL must be at least 3*NCV**2 + 5*NCV.*/
	std::complex<double> *workl = new std::complex<double>[lworkl];
	double *rwork = new double[ncv];
	int info = 1;
	// Parameters for zgemv
	std::complex<double> alpha(1.0e0, 0.0e0);
	std::complex<double> beta(0.0e0, 0.0e0);
	int inc = 1;

	uni10::CUniTensor trial( psi_t.bond() );	// <--

	znaupd_(&ido, &bmat, &dim, &which[0], &nev, &err_tol, resid, &ncv, v, &ldv,
		iparam, ipntr, workd, workl, &lworkl, rwork, &info);

	while ( ido != 99 ) {
		//zgemv((char*)"T", &dim, &dim, &alpha, A, &dim, workd+ipntr[0]-1, &inc, &beta,
		//	workd+ipntr[1]-1, &inc);
// <--
		psi_t.setElem( workd+ipntr[0]-1 );
		trial.setElem( workd+ipntr[1]-1 );
		trial *= beta;
		trial += alpha * tranMatVec( gamma, lambda, psi_t, left );
		memcpy( workd+ipntr[1]-1, trial.getBlock().getElem(), dim * sizeof(std::complex<double>) );
// <--
		znaupd_(&ido, &bmat, &dim, &which[0], &nev, &err_tol, resid, &ncv, v, &ldv,
			iparam, ipntr, workd, workl, &lworkl, rwork, &info);
	}

	if ( info < 0 )
		std::cerr << "Error with znaupd, info = " << info << std::endl;
	else if ( info == 1 )
		std::cerr << "Maximum number of Lanczos iterations reached." << std::endl;
	else if ( info == 3 )
		std::cerr << "No shifts could be applied during implicit Arnoldi update,"
				<< " try increasing NCV." << std::endl;

	// zneupd Parameters
	int rvec = 1;
	char howmny = 'A';
	int *select = new int[ncv];
	std::complex<double> *d = new std::complex<double>[nev+1];
	std::complex<double> *z = new std::complex<double>[dim*nev];
	std::complex<double> sigma;
	std::complex<double> *workev = new std::complex<double>[2*ncv];
	zneupd_(&rvec, &howmny, select, d, z, &ldv, &sigma, workev,
		&bmat, &dim, &which[0], &nev, &err_tol, resid, &ncv, v, &ldv,
		iparam, ipntr, workd, workl, &lworkl, rwork, &info);
	if ( info != 0 )
		std::cerr << "Error with dneupd, info = " << info << std::endl;
	eigVal = d[nev-1].real();
	memcpy(eigVec, z, dim * sizeof(std::complex<double>));

	delete [] workev;
	delete [] z;
	delete [] d;
	delete [] select;
	delete [] rwork;
	delete [] workl;
	delete [] workd;
	delete [] ipntr;
	delete [] iparam;
	delete [] v;
	delete [] resid;

	return (info == 0);
}

//=================================================

size_t myArpLanczosDomEig( std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	uni10::CUniTensor& psi_t, double& E0, size_t max_iter, double err_tol, int nev, bool left ) {
	///
	psi_t.permute( psi_t.label(), psi_t.bondNum() );
	uni10::CMatrix psi = psi_t.getBlock();
	size_t Rnum = psi.elemNum();
	std::complex<double> *psi_elem = psi.getElem();

	try {
		size_t iter = max_iter;
		if( !myArpackDomEig(gamma, lambda, psi_t, Rnum, iter, E0, psi_elem, false, err_tol, nev, left) ) {
			std::ostringstream err;
			err<<"Lanczos algorithm fails in converging.";
			throw std::runtime_error(uni10::exception_msg(err.str()));
		}
		//psi.setElem( psi_elem );
		psi_t.putBlock( psi );
		return iter;
	}
	catch(const std::exception& e) {
		uni10::propogate_exception(e, "In function myArpLanczosDomEig: ");
		return 0;
	}
}

//=================================================
