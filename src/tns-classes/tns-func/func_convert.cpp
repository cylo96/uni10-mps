#include <tns-func/func_convert.h>
#include <tns-func/tns_const.h>

//======================================

uni10::CMatrix real2Complex( uni10::Matrix rM ) {

	int num_elem = rM.isDiag()? rM.row() : rM.row() * rM.col();
	//int num_elem = rM.row() * rM.col();
	double *rM_elem = rM.getElem();

	uni10::CMatrix cM( rM.row(), rM.col(), rM.isDiag() );
	//cM.set_zero();
	Complex *cM_elem = new Complex[ num_elem ];
	for (int i = 0; i < num_elem; ++i)
		cM_elem[i] = Complex( rM_elem[i], 0.0 );

	cM.setElem( cM_elem );
	return cM;
}

//======================================

uni10::CUniTensor real2Complex( uni10::UniTensor rT ) {

	uni10::Matrix rT_mat = rT.getBlock();

	uni10::CUniTensor cT( rT.bond() );
	cT.putBlock( real2Complex( rT_mat ) );

	return cT;
}

//======================================

uni10::Matrix complex2Real( uni10::CMatrix cM ) {

	int num_elem = cM.isDiag()? cM.row() : cM.row() * cM.col();
	Complex *cM_elem = cM.getElem();

	uni10::Matrix rM( cM.row(), cM.col(), cM.isDiag() );
	double *rM_elem = new double[ num_elem ];
	for (int i = 0; i < num_elem; ++i)
		rM_elem[i] = cM_elem[i].real();

	rM.setElem( rM_elem );
	return rM;
}

//======================================

uni10::UniTensor complex2Real( uni10::CUniTensor cT ) {

	uni10::CMatrix cT_mat = cT.getBlock();

	uni10::UniTensor rT( cT.bond() );
	rT.putBlock( complex2Real( cT_mat ) );

	return rT;
}

//======================================

uni10::CUniTensor randT( uni10::CUniTensor cT ) {
	/// randomize a complex tensor having only real part
	uni10::UniTensor rT( cT.bond() );
	rT.randomize();

	return real2Complex( rT );
}

