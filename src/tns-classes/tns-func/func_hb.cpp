#include <uni10.hpp>
#include <tns-func/func_convert.h>
#include <tns-func/func_net.h>
#include "func_hb.h"

using namespace std;

//============================================

uni10::UniTensor trRhoVec( bool left, uni10::UniTensor& lambda, uni10::UniTensor& x ) {
	///
	uni10::UniTensor trace;
	int lab_lv[]  = {0, 2};
	int lab_rv[]  = {1, 3};

	if (left) {
		int lab_lup[] = {0, 1};
		int lab_ldn[] = {2, 1};
		x.setLabel( lab_lv );
		lambda.setLabel( lab_lup );
		trace = uni10::contract( x, lambda );
		lambda.setLabel( lab_ldn );
		trace = uni10::contract( trace, lambda );
	}
	else {
		int lab_lup[] = {0, 1};
		int lab_ldn[] = {0, 3};
		x.setLabel( lab_rv );
		lambda.setLabel( lab_lup );
		trace = uni10::contract( lambda, x );
		lambda.setLabel( lab_ldn );
		trace = uni10::contract( trace, lambda );
	}

	return trace;
}

//============================================

uni10::UniTensor Hb_matvec( bool left, 
	uni10::UniTensor& cell, uni10::UniTensor& cell_dag, uni10::UniTensor& lambda, uni10::UniTensor& x ) {
	///
	uni10::UniTensor vec;
	uni10::UniTensor id( x.bond() );
	id.permute( id.bondNum()/2 );
	id.identity();
	id.permute( x.inBondNum() );

	int lab_bra[] = {0, 100, 101, 1};
	int lab_ket[] = {2, 100, 101, 3};
	int lab_lv[]  = {0, 2};
	int lab_rv[]  = {1, 3};

	cell_dag.setLabel( lab_bra );
	cell.setLabel( lab_ket );
	if (left)
		x.setLabel( lab_lv );
	else
		x.setLabel( lab_rv );

	vec = uni10::contract( cell_dag, x );
	vec = uni10::contract( vec, cell );
	vec.permute( vec.bondNum() );

	uni10::UniTensor edg = trRhoVec(left, lambda, x);
	edg = edg[0] * id;

	vec = x + (-1.0) * vec + edg;

	return vec;
}

//============================================

uni10::UniTensor Hb_resid( bool left, 
	uni10::UniTensor& cell, uni10::UniTensor& cell_dag, uni10::UniTensor& lambda,
	uni10::UniTensor& x, uni10::UniTensor& b ) {	
	/// Computes the residual R = B-A*X
	uni10::UniTensor vec = b + (-1.0) * Hb_matvec( left, cell, cell_dag, lambda, x );
	return vec;
}

//============================================

double Hb_overlap( uni10::UniTensor bra, uni10::UniTensor ket ) {
	///
	int lab[] = {0, 1};

	bra.setLabel( lab );
	ket.setLabel( lab );

	uni10::UniTensor overlap = bra * ket;
	return overlap[0];
}

//============================================

double Hb_vnorm( uni10::UniTensor vec ) {
	///
	double l2_norm = sqrt( Hb_overlap( vec, vec ) );
	return l2_norm;
}

//============================================

uni10::UniTensor Hb_cg( bool left, 
	uni10::UniTensor& cell, uni10::UniTensor& cell_dag, uni10::UniTensor& lambda,
	uni10::UniTensor& b, uni10::UniTensor& x_trial, bool& early_term ) {
	/// Solve linear equation A*x = b using Conjugate Gradient method
	uni10::UniTensor x = x_trial;

	// organize column vectors
	if ( b.inBondNum() < b.bondNum() )
		b.permute( b.bondNum() );
	if ( x.inBondNum() < x.bondNum() )
		x.permute( x.bondNum() );

	// get the order of the system
	int n = b.getBlock().row();

	// Initialize
	// AP = A * x,
	// R  = b - A * x,
	// P  = b - A * x.
	uni10::UniTensor ap = Hb_matvec( left, cell, cell_dag, lambda, x );
	uni10::UniTensor r = b + (-1.0) * ap;
	uni10::UniTensor p = b + (-1.0) * ap;
	double pap, pr, rap;
	double alpha, beta;

	int it;
	double diff_min = 1.0;
	uni10::UniTensor x_min = x;
	early_term = false;

	// Do N steps of the conjugate gradient method.
	for ( it = 1; it <= n; it++ ) {

		// Compute the matrix*vector product AP=A*P.
		ap = Hb_matvec( left, cell, cell_dag, lambda, p );

		// Compute the dot products
		// PAP = P*AP,
		// PR  = P*R
		pap = Hb_overlap( p, ap );
		pr = Hb_overlap( p, r );

		if ( fabs(pap) < diff_min ) {
			diff_min = fabs(pap);
			x_min = x;
		}

		if ( fabs(pap) < 1e-15 ) {
			early_term = true;
			break;
		}

		// Set ALPHA = PR / PAP.
		alpha = pr / pap;

		// X = X + ALPHA * P
		// R = R - ALPHA * AP.
		x = x + alpha * p;
		r = r + (-1.0) * alpha * ap;

		// Compute the vector dot product
		// RAP = R*AP
		rap = Hb_overlap ( r, ap );

		// Set BETA = - RAP / PAP.
		beta = - rap / pap;

		// Update the perturbation vector
		// P = R + BETA * P.
		p = r + beta * p;
	}

	return x_min;
}

//============================================

uni10::UniTensor Hb_gmres( bool left, 
	uni10::UniTensor& cell, uni10::UniTensor& cell_dag, uni10::UniTensor& lambda,
	uni10::UniTensor& b, uni10::UniTensor& x_trial, int ar_iter, bool& early_term ) {
	/// Solve linear equation A*x = b using GMRES method
	uni10::UniTensor x = x_trial;

	// organize column vectors
	if ( b.inBondNum() < b.bondNum() )
		b.permute( b.bondNum() );
	if ( x.inBondNum() < x.bondNum() )
		x.permute( x.bondNum() );

	// get the order of the system
	int n = b.getBlock().row();

	// Initialize
	// ax = A * x,
	// r  = b - A * x,
	// q0  = r / ||r||.
	uni10::UniTensor ax = Hb_matvec( left, cell, cell_dag, lambda, x );
	uni10::UniTensor r = b + (-1.0) * ax;
	std::vector<uni10::UniTensor> q;
	for (int i = 0; i < n; ++i)
		q.push_back( uni10::UniTensor() );
	q[0] = r * ( 1./ Hb_vnorm(r) );

	early_term = false;

	uni10::UniTensor y;
	double h_jk;

	uni10::Matrix H(ar_iter+1, ar_iter);
	H.set_zero();

	uni10::Matrix Q(n, ar_iter);
	Q.set_zero();
	for ( int i = 0; i < n; ++i )
		Q.at(i,0) = q[0][i];

	uni10::Matrix r0(2, 1);
	r0.at(0,0) = Hb_vnorm(r);
	//r0.at(0,0) = Hb_vnorm(b);

	uni10::Matrix c(1, 1);
	uni10::Matrix Hk, Qk;
	double bnorm = Hb_vnorm(b);

	// Do ar_iter steps of Arnoldi iteration.
	for ( int k = 0; k < ar_iter; ++k ) {

		y = Hb_matvec( left, cell, cell_dag, lambda, q[k] );

		for ( int j = 0; j <= k; ++j ) {
			h_jk = Hb_overlap(q[j], y);
			y = y + (-1.0) * h_jk * q[j];
			H.at(j,k) = h_jk;
		}

		double ynorm = Hb_vnorm(y);
		H.at(k+1, k) = ynorm;

		if ( ynorm != 0. && k != ar_iter-1 ) {
			q[k+1] = y * (1./ynorm);

			// try memcpy here? nope. uni10::Matrix is row major
			for ( int i = 0; i < n; ++i )
				Q.at(i, k+1) = q[k+1][i];
		}

		// least squre
		// find c_k for min|| H * c_k - r0 ||
		Hk = H;
		Hk.resize( k+1+1, k+1 );
		Qk = Q;
		Qk.resize( n, k+1 );
		r0.resize( k+1+1, 1 );

		std::vector<uni10::Matrix> qrh = Hk.qr();
		// R * c = Qt * r0
		c = qrh[1].inverse() * qrh[0].transpose() * r0; 
		x.putBlock( Qk * c + x_trial.getBlock() );
		//x.putBlock( Qk * c );

		// early termination
		ax = Hb_matvec( left, cell, cell_dag, lambda, x );
		r = b + (-1.0) * ax;
		if ( (Hb_vnorm(r) / bnorm) < 1e-8 ) {
			early_term = true;
			break;
		}
	}

	return x;
}

//============================================

uni10::CUniTensor findHb( bool left, 
	uni10::CUniTensor zcell, uni10::CUniTensor zcell_dag, uni10::CUniTensor& zlambda, 
	uni10::CUniTensor zb, uni10::CUniTensor zx_trial, int ar_iter ) {
	///
	uni10::UniTensor cell = complex2Real( zcell );
	uni10::UniTensor cell_dag = complex2Real( zcell_dag );
	uni10::UniTensor lambda = complex2Real( zlambda );
	uni10::UniTensor b = complex2Real( zb );
	uni10::UniTensor x_trial = complex2Real( zx_trial );

	int n = b.getBlock().elemNum();
	bool early_term = false;

	if ( ar_iter < 0 )
		ar_iter = std::min( zlambda.bond()[0].dim()*zlambda.bond()[0].dim(), 500 );
	//uni10::UniTensor x = Hb_cg( left, cell, cell_dag, lambda, b, x_trial, early_term );
	uni10::UniTensor x = Hb_gmres( left, cell, cell_dag, lambda, b, x_trial, ar_iter, early_term );
	uni10::UniTensor r = Hb_resid( left, cell, cell_dag, lambda, x, b );
	double r_norm = Hb_vnorm( r );
	double b_norm = Hb_vnorm( b );

	double tol = 1e-6;
	double maxtol = 1e-2;
	int iter = 0;
	int maxiter = 1e4/sqrt(n);

	double rn_min = r_norm;
	uni10::UniTensor x_min = x;

	while ( ( r_norm/b_norm > tol && early_term == false ) || r_norm > maxtol || r_norm != r_norm ) {
		// r_norm != r_norm <- to tackle NaN

		if ( iter > maxiter )
			break;

		if ( early_term && r_norm == r_norm )
			//x = Hb_cg( left, cell, cell_dag, lambda, b, x, early_term );
			x = Hb_gmres( left, cell, cell_dag, lambda, b, x, ar_iter, early_term );
		else {
			x_trial.randomize();
			//x = Hb_cg( left, cell, cell_dag, lambda, b, x_trial, early_term );
			x = Hb_gmres( left, cell, cell_dag, lambda, b, x_trial, ar_iter, early_term );
		}

		r = Hb_resid( left, cell, cell_dag, lambda, x, b );
		r_norm = Hb_vnorm( r );

		if ( r_norm < rn_min ) {
			rn_min = r_norm;
			x_min = x;
		}

		iter += 1;
	}

	if ( rn_min > maxtol )
		std::cout << "Linear solver failed to converge. r_norm > " << maxtol << ". iter = " << iter << "\n";
	//else
	//	std::cout << "Linear solving done. r_norm = " << rn_min << ". iter = " << iter << "\n";

	return real2Complex( x_min );
}

//============================================

uni10::CUniTensor findHb( bool left, 
	uni10::CUniTensor zcell, uni10::CUniTensor zcell_dag, uni10::CUniTensor& zlambda,
	uni10::CUniTensor zb, int ar_iter ) {
	///
	uni10::CUniTensor zx_trial( zb.bond() );
	zx_trial.set_zero();

	if ( ar_iter < 0 )
		ar_iter = zlambda.bond()[0].dim();
	return findHb( left, zcell, zcell_dag, zlambda, zb, zx_trial, ar_iter );
}

