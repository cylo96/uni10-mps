#ifndef TNS_CONST_H
#define TNS_CONST_H
#include <complex>
#include <string>

typedef std::complex<double> Complex;

const Complex I (0.0, 1.0);

struct monitor {
	std::string name;
	int idx;

	monitor( std::string fn, int i = 0 ) {
		name = fn;
		idx = i;
	}
};

#endif
