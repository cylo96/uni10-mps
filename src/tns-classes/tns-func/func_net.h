#ifndef FUNC_NET_H
#define FUNC_NET_H
#include <uni10.hpp>
#include <tuple>

uni10::CUniTensor bondInvGam( uni10::CUniTensor gam );
uni10::CUniTensor bondInv( uni10::CUniTensor ten );

uni10::CUniTensor netLGLGL(
	uni10::CUniTensor ll, uni10::CUniTensor ga,
	uni10::CUniTensor la, uni10::CUniTensor gb, uni10::CUniTensor lb );

uni10::CUniTensor netLGLGL(
	uni10::CUniTensor ll, uni10::CUniTensor gala, uni10::CUniTensor gblb );

uni10::CUniTensor netLG( uni10::CUniTensor la, uni10::CUniTensor ga );
uni10::CUniTensor netGL( uni10::CUniTensor ga, uni10::CUniTensor la );
uni10::CUniTensor netLL( uni10::CUniTensor la, uni10::CUniTensor lb );

uni10::CUniTensor netLGLG(
	uni10::CUniTensor ll, uni10::CUniTensor ga,
	uni10::CUniTensor la, uni10::CUniTensor gb );

uni10::CUniTensor netGLGL(
	uni10::CUniTensor ga, uni10::CUniTensor la,
	uni10::CUniTensor gb, uni10::CUniTensor lb );

uni10::CUniTensor netGLGL( uni10::CUniTensor gala, uni10::CUniTensor gblb );

uni10::CUniTensor netGLG(
	uni10::CUniTensor ga, uni10::CUniTensor la, uni10::CUniTensor gb );

uni10::CUniTensor netLGL(
	uni10::CUniTensor ll, uni10::CUniTensor ga, uni10::CUniTensor lr );

uni10::CUniTensor netLLL(
	uni10::CUniTensor ll, uni10::CUniTensor la, uni10::CUniTensor lr );

uni10::CUniTensor net3Site(
	uni10::CUniTensor lam0, uni10::CUniTensor gam0,
	uni10::CUniTensor lam1, uni10::CUniTensor gam1,
	uni10::CUniTensor lam2, uni10::CUniTensor gam2, uni10::CUniTensor lam3 );

uni10::CUniTensor net4Site(
	uni10::CUniTensor lam0, uni10::CUniTensor gam0,
	uni10::CUniTensor lam1, uni10::CUniTensor gam1,
	uni10::CUniTensor lam2, uni10::CUniTensor gam2,
	uni10::CUniTensor lam3, uni10::CUniTensor gam3, uni10::CUniTensor lam4 );

uni10::CUniTensor netAA( uni10::CUniTensor A0, uni10::CUniTensor A1 );
uni10::CUniTensor netBB( uni10::CUniTensor B0, uni10::CUniTensor B1 );
uni10::CUniTensor netLdAs( uni10::CUniTensor& L, uni10::CUniTensor& As );
uni10::CUniTensor netBsRd( uni10::CUniTensor& Bs, uni10::CUniTensor& R );

uni10::CUniTensor netDMRGH( int loc,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo );

uni10::CUniTensor mpoLR( uni10::CUniTensor mpo_l, uni10::CUniTensor mpo_r );
uni10::CUniTensor mpoLM( uni10::CUniTensor mpo_l, uni10::CUniTensor mpo_m );
uni10::CUniTensor mpoMR( uni10::CUniTensor mpo_m, uni10::CUniTensor mpo_r );
uni10::CUniTensor mpoLMMR(
	uni10::CUniTensor mpo_l, uni10::CUniTensor mpo_m, uni10::CUniTensor mpo_r );
uni10::CUniTensor op2SiteFromMPO( uni10::CUniTensor mpo_m );

void initDMRGBlk( int loc,
	std::vector<uni10::CUniTensor>& AL, std::vector<uni10::CUniTensor>& AR, uni10::CUniTensor& Cs,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& blk );

void initDMRGBlk( int loc,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& blk );

void refreshDMRGBlk( int loc,
	std::vector<uni10::CUniTensor>& AL, std::vector<uni10::CUniTensor>& AR, uni10::CUniTensor& Cs,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& blk );

void refreshDMRGBlk( int loc,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& blk );

void updateDMRGBlk( int loc, bool swp_right, int unit_cell,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& blk );

void mpoDMRGH( int loc, bool swp_right, int unit_cell,
	std::vector<uni10::CUniTensor>& AL, std::vector<uni10::CUniTensor>& AR, uni10::CUniTensor& Cs,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& mpoH,
	std::vector<uni10::CUniTensor>& blk );

void mpoDMRGH( int loc, bool swp_right, int unit_cell,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& mpoH,
	std::vector<uni10::CUniTensor>& blk );

void mpoDMRGH( int loc,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& mpo, std::vector<uni10::CUniTensor>& mpoH );

uni10::CUniTensor mpoMatVec( int size,
	std::vector<uni10::CUniTensor>& mpoH, uni10::CUniTensor& psi );

uni10::CUniTensor tranMatVec(
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	uni10::CUniTensor& vec, bool left = false);

uni10::CUniTensor tranMtx( uni10::CUniTensor ket );
uni10::CUniTensor tranMtx( uni10::CUniTensor ket, uni10::CUniTensor op );
uni10::CUniTensor tranMtxChain( uni10::CUniTensor ket, int n );

uni10::CUniTensor tensorOp( uni10::CUniTensor ket, uni10::CUniTensor op );
uni10::CUniTensor norm( uni10::CUniTensor ket );
uni10::CUniTensor traceRhoVec( uni10::CUniTensor vec, uni10::CUniTensor lam, bool left = true );
uni10::CUniTensor expect( uni10::CUniTensor ket, uni10::CUniTensor op );

uni10::CUniTensor expectOBCLongContract(
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	uni10::CUniTensor op, int op_location);

uni10::CUniTensor expectPBCLongContract(
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	uni10::CUniTensor op, int op_location);

uni10::CUniTensor correlationOBC(
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda,
	uni10::CUniTensor op1, uni10::CUniTensor op2, int x1, int x2);

std::tuple<uni10::CUniTensor, uni10::CUniTensor, uni10::CUniTensor> thetaSVD(
	uni10::CUniTensor& theta, int chi_max, int ibn = -1,
	double sv_cutoff = 1e-10, bool fix_chi = false);
#endif
