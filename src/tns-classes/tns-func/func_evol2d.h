#ifndef FUNC_EVOL2D_H
#define FUNC_EVOL2D_H
#include <uni10.hpp>

void updateRect2Site( uni10::CUniTensor& theta,
	uni10::CUniTensor& li, uni10::CUniTensor& la1, uni10::CUniTensor& la2, uni10::CUniTensor& ga,
	uni10::CUniTensor& lc, uni10::CUniTensor& lb1, uni10::CUniTensor& lb2, uni10::CUniTensor& gb,
	uni10::CUniTensor& lf, bool vertical = false);

void evolRect2Site( uni10::CUniTensor U,
	uni10::CUniTensor& li, uni10::CUniTensor& la1, uni10::CUniTensor& la2, uni10::CUniTensor& ga,
	uni10::CUniTensor& lc, uni10::CUniTensor& lb1, uni10::CUniTensor& lb2, uni10::CUniTensor& gb,
	uni10::CUniTensor& lf, bool vertical = false);

std::vector<uni10::CUniTensor> trgSVD( uni10::CUniTensor node, int chi_trg );
#endif
