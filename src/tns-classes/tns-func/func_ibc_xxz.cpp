/**************************************************************************

Effective Infinite Boundary Condition for spin-1/2 XXZ model

Ref: PHYSICAL REVIEW B 86, 245107 (2012)
Uni10 library: http://uni10.org/

@author: cylo
@affiliation: NTHU Physics

***************************************************************************/

#include <iostream>
#include <uni10.hpp>

#include <mps.hpp>

#include "func_hb.h"

//============================================

uni10::CUniTensor net2Site1Op (
	uni10::CNetwork net, uni10::CUniTensor A1, uni10::CUniTensor A2,
	uni10::CUniTensor A1d, uni10::CUniTensor A2d, uni10::CUniTensor op ) {

	net.putTensor( "A1", A1 );
	net.putTensor( "A2", A2 );
	net.putTensor( "op", op );
	net.putTensor( "A1dag", A1d );
	net.putTensor( "A2dag", A2d );

	uni10::CUniTensor T = net.launch();
	return T;
}

//============================================

void ibcXXZ( double J, double delta,
	std::vector<uni10::CUniTensor>& AL, std::vector<uni10::CUniTensor>& AR, std::vector<uni10::CUniTensor>& lambda,
	std::vector<uni10::CUniTensor>& HBL, std::vector<uni10::CUniTensor>& HBR, int ar_iter ) {

	J /= 4.0;
	delta /= 4.0;

	if ( HBL.size() < 6 || HBR.size() < 6 ) {
		HBL.clear();
		HBR.clear();
		for (int i = 0; i < 6; ++i) {
			HBL.push_back( uni10::CUniTensor() );
			HBR.push_back( uni10::CUniTensor() );
		}
	}

//	uni10::CUniTensor As = netLG( lambda[0], gamma[0] );
//	uni10::CUniTensor A2s = netLG( lambda[1], gamma[1] );

//	uni10::CUniTensor Bs = netGL( gamma[1], lambda[0] );
//	uni10::CUniTensor B2s = netGL( gamma[0], lambda[1] );

	uni10::CUniTensor As = AL[1];
	uni10::CUniTensor A2s = AL[2];

	uni10::CUniTensor Bs = AR[1];
	uni10::CUniTensor B2s = AR[2];

	uni10::CUniTensor As_dag = As;
	uni10::CUniTensor A2s_dag = A2s;
	As_dag.permute( As_dag.label(), 1 );
	A2s_dag.permute( A2s_dag.label(), 1 );
	As_dag.conj();
	A2s_dag.conj();

	uni10::CUniTensor Bs_dag = Bs;
	uni10::CUniTensor B2s_dag = B2s;
	Bs_dag.permute( Bs_dag.label(), 1 );
	B2s_dag.permute( B2s_dag.label(), 1 );
	Bs_dag.conj();
	B2s_dag.conj();

	// define E5 and E5r
	int D = AL[1].bond()[0].dim();

	std::vector<uni10::Bond> bonds;
	bonds.push_back( uni10::Bond( uni10::BD_IN, D) );
	bonds.push_back( uni10::Bond( uni10::BD_OUT, D) );

	uni10::CUniTensor E5(bonds, "E5");
	E5.identity();
	uni10::CUniTensor E5r = E5;

	// define E4 and E4r
	uni10::CUniTensor op("sz");
	uni10::CNetwork net("network_left");
	uni10::CUniTensor E4 = net2Site1Op(net, As, A2s, As_dag, A2s_dag, op);
	net = uni10::CNetwork("network_right");
	uni10::CUniTensor E4r = net2Site1Op(net, Bs, B2s, Bs_dag, B2s_dag, op);

	// define E3 and E3r
	op = uni10::CUniTensor("_isy");
	net = uni10::CNetwork("network_left");
	uni10::CUniTensor E3 = net2Site1Op(net, As, A2s, As_dag, A2s_dag, op);
	net = uni10::CNetwork("network_right");
	uni10::CUniTensor E3r = net2Site1Op(net, Bs, B2s, Bs_dag, B2s_dag, op);

	// define E2 and E2r
	op = uni10::CUniTensor("sx");
	net = uni10::CNetwork("network_left");
	uni10::CUniTensor E2 = net2Site1Op(net, As, A2s, As_dag, A2s_dag, op);
	net = uni10::CNetwork("network_right");
	uni10::CUniTensor E2r = net2Site1Op(net, Bs, B2s, Bs_dag, B2s_dag, op);

	// define C
	//uni10::UniTensor C = Tsx_E2 + Tsy_E3 + Tsz_E4;
	op = uni10::CUniTensor("ham_xxz");
	net = uni10::CNetwork("network_ham_odd_left");
	uni10::CUniTensor C1 = net2Site1Op(net, As, A2s, As_dag, A2s_dag, op);

	net = uni10::CNetwork("network_ham_even_left");
	net.putTensor( "A1", As );
	net.putTensor( "A2", A2s );
	net.putTensor( "A3", As );
	net.putTensor( "A4", A2s );
	net.putTensor( "op", op );
	net.putTensor( "A1dag", As_dag );
	net.putTensor( "A2dag", A2s_dag );
	net.putTensor( "A3dag", As_dag );
	net.putTensor( "A4dag", A2s_dag );
	uni10::CUniTensor C2 = net.launch();

	uni10::CUniTensor C = C1 + C2;

	// define Cr
	net = uni10::CNetwork("network_ham_odd_right");
	uni10::CUniTensor C1r = net2Site1Op(net, Bs, B2s, Bs_dag, B2s_dag, op);

	net = uni10::CNetwork("network_ham_even_right");
	net.putTensor( "A1", Bs );
	net.putTensor( "A2", B2s );
	net.putTensor( "A3", Bs );
	net.putTensor( "A4", B2s );
	net.putTensor( "op", op );
	net.putTensor( "A1dag", Bs_dag );
	net.putTensor( "A2dag", B2s_dag );
	net.putTensor( "A3dag", Bs_dag );
	net.putTensor( "A4dag", B2s_dag );
	uni10::CUniTensor C2r = net.launch();

	uni10::CUniTensor Cr = C1r + C2r;

	// calculate e0
	uni10::CNetwork netTrace("trace_rho_lvec");
	netTrace.putTensor( "lam1", lambda[0] );
	netTrace.putTensor( "vec", C );
	netTrace.putTensor( "lam2", lambda[0] );

	uni10::CUniTensor traceRhoC = netTrace.launch();
	double e0 = traceRhoC[0].real();
//	std::cout << std::setprecision(10) << e0 << "\n";

	// find HL
	uni10::CUniTensor I2 = E5;
	uni10::CUniTensor rhs_l = C + (-1.0 * e0) * I2;
	rhs_l.permute( rhs_l.bondNum() );

	uni10::CUniTensor HL = findHb( true, netAA(As, A2s), netAA(As_dag, A2s_dag), lambda[0], rhs_l, ar_iter );
	uni10::CUniTensor tmp_soln = HL;	// save for HR solver's trial func
	HL.permute(1);
	HL.setName("HL");
/*
	netTrace = uni10::CNetwork("trace_rho_lvec");
	netTrace.putTensor( "lam1", lambda[0] );
	netTrace.putTensor( "vec", HL );
	netTrace.putTensor( "lam2", lambda[0] );
	uni10::CUniTensor traceRhoHL = netTrace.launch();
	double H0 = traceRhoHL[0].real();

	uni10::CUniTensor ground( HL.bond() );
	ground.identity();
	ground *= H0;
	HL = HL + (-1.0) * ground;
*/
//	HL.save("HL");
	HBL[0] = HL;


	// calculate e0
	netTrace = uni10::CNetwork("trace_rho_rvec");
	netTrace.putTensor( "lam1", lambda[1] );
	netTrace.putTensor( "vec", Cr );
	netTrace.putTensor( "lam2", lambda[1] );

	traceRhoC = netTrace.launch();
	e0 = traceRhoC[0].real();

	// find HR
	uni10::CUniTensor rhs_r = Cr + (-1.0 * e0) * I2;
	rhs_r.permute( rhs_r.bondNum() );

	uni10::CUniTensor HR = findHb( false, netBB(Bs, B2s), netBB(Bs_dag, B2s_dag), lambda[1], rhs_r, tmp_soln, ar_iter );
	HR.permute(1);
	HR.setName("HR");
/*
	netTrace = uni10::CNetwork("trace_rho_rvec");
	//netTrace.putTensor( "lam1", lambda[0] );
	netTrace.putTensor( "lam1", lambda[1] );	// lambda[1] = Cs[1] in CARAR
	netTrace.putTensor( "vec", HR );
	//netTrace.putTensor( "lam2", lambda[0] );
	netTrace.putTensor( "lam2", lambda[1] );
	uni10::CUniTensor traceRhoHR = netTrace.launch();
	H0 = traceRhoHR[0].real();

	ground.assign( HR.bond() );
	ground.identity();
	ground *= H0;
	HR = HR + (-1.0) * ground;
*/
//	HR.save("HR");
	HBR[0] = HR;

	// define HLW
	uni10::CUniTensor HLW =
		J * uni10::otimes( E2, uni10::CUniTensor("sx") )
		+ (-1.0) * J * uni10::otimes( E3, uni10::CUniTensor("_isy") )
		+ delta * uni10::otimes( E4, uni10::CUniTensor("sz") );
//	HLW.save("HLW");
	HBL[1] = HLW;

	// define HWR
	uni10::CUniTensor HWR =
		J * uni10::otimes( uni10::CUniTensor("sx"), E2r )
		+ (-1.0) * J * uni10::otimes( uni10::CUniTensor("_isy"), E3r )
		+ delta * uni10::otimes( uni10::CUniTensor("sz"), E4r );
//	HWR.save("HWR");
	HBR[1] = HWR;

	// output effective boundary one-site operators
//	E2.save("Sx_L");
//	E3.save("_iSy_L");
//	E4.save("Sz_L");
//	E5.save("Id_L");
//	E2r.save("Sx_R");
//	E3r.save("_iSy_R");
//	E4r.save("Sz_R");
//	E5r.save("Id_R");
	HBL[2] = E2;
	HBL[3] = E3;
	HBL[4] = E4;
	HBL[5] = E5;
	HBR[2] = E2r;
	HBR[3] = E3r;
	HBR[4] = E4r;
	HBR[5] = E5r;
}

//============================================
/*
void ibcXXZ( double J, double delta,
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda ) {

	J /= 4.0;
	delta /= 4.0;

	uni10::CUniTensor As = netLG( lambda[1], gamma[1] );
	uni10::CUniTensor A2s = netLG( lambda[2], gamma[2] );

	uni10::CUniTensor Bs = netGL( gamma[2], lambda[1] );
	uni10::CUniTensor B2s = netGL( gamma[1], lambda[2] );

	uni10::CUniTensor As_dag = As;
	uni10::CUniTensor A2s_dag = A2s;
	As_dag.permute( As_dag.label(), 1 );
	A2s_dag.permute( A2s_dag.label(), 1 );

	uni10::CUniTensor Bs_dag = Bs;
	uni10::CUniTensor B2s_dag = B2s;
	Bs_dag.permute( Bs_dag.label(), 1 );
	B2s_dag.permute( B2s_dag.label(), 1 );

	// define E5 and E5r
	int D = (lambda[1].bond())[0].dim();

	std::vector<uni10::Bond> bonds;
	bonds.push_back( uni10::Bond( uni10::BD_IN, D) );
	bonds.push_back( uni10::Bond( uni10::BD_OUT, D) );

	uni10::CUniTensor E5(bonds, "E5");
	E5.identity();
	uni10::CUniTensor E5r = E5;

	// define E4 and E4r
	uni10::CUniTensor op("sz");
	uni10::CNetwork net("network_left");
	uni10::CUniTensor E4 = net2Site1Op(net, As, A2s, As_dag, A2s_dag, op);
	net = uni10::CNetwork("network_right");
	uni10::CUniTensor E4r = net2Site1Op(net, Bs, B2s, Bs_dag, B2s_dag, op);

	// define E3 and E3r
	op = uni10::CUniTensor("_isy");
	net = uni10::CNetwork("network_left");
	uni10::CUniTensor E3 = net2Site1Op(net, As, A2s, As_dag, A2s_dag, op);
	net = uni10::CNetwork("network_right");
	uni10::CUniTensor E3r = net2Site1Op(net, Bs, B2s, Bs_dag, B2s_dag, op);

	// define E2 and E2r
	op = uni10::CUniTensor("sx");
	net = uni10::CNetwork("network_left");
	uni10::CUniTensor E2 = net2Site1Op(net, As, A2s, As_dag, A2s_dag, op);
	net = uni10::CNetwork("network_right");
	uni10::CUniTensor E2r = net2Site1Op(net, Bs, B2s, Bs_dag, B2s_dag, op);

	// define C
	//uni10::UniTensor C = Tsx_E2 + Tsy_E3 + Tsz_E4;
	op = uni10::CUniTensor("ham_xxz");
	net = uni10::CNetwork("network_ham_odd_left");
	uni10::CUniTensor C1 = net2Site1Op(net, As, A2s, As_dag, A2s_dag, op);

	net = uni10::CNetwork("network_ham_even_left");
	net.putTensor( "A1", As );
	net.putTensor( "A2", A2s );
	net.putTensor( "A3", As );
	net.putTensor( "A4", A2s );
	net.putTensor( "op", op );
	net.putTensor( "A1dag", As_dag );
	net.putTensor( "A2dag", A2s_dag );
	net.putTensor( "A3dag", As_dag );
	net.putTensor( "A4dag", A2s_dag );
	uni10::CUniTensor C2 = net.launch();

	uni10::CUniTensor C = C1 + C2;
	//uni10::CUniTensor C = C1;

	// define Cr
	net = uni10::CNetwork("network_ham_odd_right");
	uni10::CUniTensor C1r = net2Site1Op(net, Bs, B2s, Bs_dag, B2s_dag, op);

	net = uni10::CNetwork("network_ham_even_right");
	net.putTensor( "A1", Bs );
	net.putTensor( "A2", B2s );
	net.putTensor( "A3", Bs );
	net.putTensor( "A4", B2s );
	net.putTensor( "op", op );
	net.putTensor( "A1dag", Bs_dag );
	net.putTensor( "A2dag", B2s_dag );
	net.putTensor( "A3dag", Bs_dag );
	net.putTensor( "A4dag", B2s_dag );
	uni10::CUniTensor C2r = net.launch();

	uni10::CUniTensor Cr = C1r + C2r;
	//uni10::CUniTensor Cr = C1r;

	// calculate e0
	uni10::CNetwork netTrace("trace_rho_lvec");
	netTrace.putTensor( "lam1", lambda[1] );
	netTrace.putTensor( "vec", C );
	netTrace.putTensor( "lam2", lambda[1] );

	uni10::CUniTensor traceRhoC = netTrace.launch();
	double e0 = traceRhoC[0].real();
	std::cout << std::setprecision(10) << e0 << "\n";

	// find HL
	uni10::CUniTensor I2 = E5;
	uni10::CUniTensor rhs_l = C + (-1.0 * e0) * I2;
	rhs_l.permute( rhs_l.bondNum() );

	//uni10::CUniTensor pcu = netLGL( lambda[0], gamma[0], lambda[0] );
	//uni10::CUniTensor HL = findHbp( true, netAA(As, A2s), netAA(As_dag, A2s_dag), pcu, rhs_l );
	uni10::CUniTensor HL = findHb( true, netAA(As, A2s), netAA(As_dag, A2s_dag), lambda[1], rhs_l );
	uni10::CUniTensor tmp_soln = HL;	// save for HR solver's trial func
	HL.permute(1);
	HL.setName("HL");

	netTrace = uni10::CNetwork("trace_rho_lvec");
	netTrace.putTensor( "lam1", lambda[1] );
	netTrace.putTensor( "vec", HL );
	netTrace.putTensor( "lam2", lambda[1] );
	uni10::CUniTensor traceRhoHL = netTrace.launch();
	double H0 = traceRhoHL[0].real();

	uni10::CUniTensor ground( HL.bond() );
	ground.identity();
	ground *= H0;
	HL = HL + (-1.0) * ground;
	HL.save("HL");

	// find HR
	uni10::CUniTensor rhs_r = Cr + (-1.0 * e0) * I2;
	rhs_r.permute( rhs_r.bondNum() );

	//uni10::CUniTensor HR = findHbp( false, netBB(Bs, B2s), netBB(Bs_dag, B2s_dag), pcu, rhs_r, tmp_soln );
	uni10::CUniTensor HR = findHb( false, netBB(Bs, B2s), netBB(Bs_dag, B2s_dag), lambda[1], rhs_r, tmp_soln );
	HR.permute(1);
	HR.setName("HR");

	netTrace = uni10::CNetwork("trace_rho_rvec");
	netTrace.putTensor( "lam1", lambda[1] );
	netTrace.putTensor( "vec", HR );
	netTrace.putTensor( "lam2", lambda[1] );
	uni10::CUniTensor traceRhoHR = netTrace.launch();
	H0 = traceRhoHR[0].real();

	ground.assign( HR.bond() );
	ground.identity();
	ground *= H0;
	HR = HR + (-1.0) * ground;
	HR.save("HR");

	// define HLW
	uni10::CUniTensor HLW =
		J * uni10::otimes( E2, uni10::CUniTensor("sx") )
		+ (-1.0) * J * uni10::otimes( E3, uni10::CUniTensor("_isy") )
		+ delta * uni10::otimes( E4, uni10::CUniTensor("sz") );
	HLW.save("HLW");

	// define HWR
	uni10::CUniTensor HWR =
		J * uni10::otimes( uni10::CUniTensor("sx"), E2r )
		+ (-1.0) * J * uni10::otimes( uni10::CUniTensor("_isy"), E3r )
		+ delta * uni10::otimes( uni10::CUniTensor("sz"), E4r );
	HWR.save("HWR");

	// output effective boundary one-site operators
	E2.save("Sx_L");
	E3.save("_iSy_L");
	E4.save("Sz_L");
	E5.save("Id_L");
	E2r.save("Sx_R");
	E3r.save("_iSy_R");
	E4r.save("Sz_R");
	E5r.save("Id_R");
}
*/
