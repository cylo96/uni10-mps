#include <string>
#include <tns-func/func_net2d.h>

// todo: rewrite these functions using Network class -- or not?
//===================================

uni10::CUniTensor netRect2Site( 
	uni10::CUniTensor li, uni10::CUniTensor la1, uni10::CUniTensor la2, uni10::CUniTensor ga, 
	uni10::CUniTensor lc, uni10::CUniTensor lb1, uni10::CUniTensor lb2, uni10::CUniTensor gb, 
	uni10::CUniTensor lf, bool vertical ) {

	uni10::CNetwork net( "net_rect_2site_h" );

	if (vertical)
		net = uni10::CNetwork( "net_rect_2site_v" );

	net.putTensor( "lamI", li );
	net.putTensor( "lam1", la1 );
	net.putTensor( "lam2", la2 );
	net.putTensor( "lamC", lc );
	net.putTensor( "lam3", lb1 );
	net.putTensor( "lam4", lb2 );
	net.putTensor( "lamF", lf );
	net.putTensor( "gamA", ga );
	net.putTensor( "gamB", gb );

	return net.launch();
}

//=============================================

uni10::CUniTensor netRectTheta( uni10::CUniTensor op, 
	uni10::CUniTensor li, uni10::CUniTensor la1, uni10::CUniTensor la2, uni10::CUniTensor ga,
	uni10::CUniTensor lc, uni10::CUniTensor lb1, uni10::CUniTensor lb2, uni10::CUniTensor gb,
	uni10::CUniTensor lf, bool vertical ) {

	uni10::CNetwork net( "net_rect_theta_h" );

	if (vertical)
		net = uni10::CNetwork( "net_rect_theta_v" );

	net.putTensor( "lamI", li );
	net.putTensor( "lam1", la1 );
	net.putTensor( "lam2", la2 );
	net.putTensor( "lamC", lc );
	net.putTensor( "lam3", lb1 );
	net.putTensor( "lam4", lb2 );
	net.putTensor( "lamF", lf );
	net.putTensor( "gamA", ga );
	net.putTensor( "gamB", gb );
	net.putTensor( "op", op );

	return net.launch();
}

//===============================================

uni10::CUniTensor netRectOp( uni10::CUniTensor ket, uni10::CUniTensor op ) {

	// todo: assert ket.bondNum() == 5 or 8
	// todo: assert op is one-site or two-site operator
	int lab_5b[] = {0, 1, 100, 2, 3};
	int lab_8b[] = {0, 1, 2, 100, 101, 3, 4, 5};
	int lab_op1[] = {200, 100};
	int lab_op2[] = {200, 201, 100, 101};
	int lab_net_5b[] = {0, 1, 200, 2, 3};
	int lab_net_8b1[] = {0, 1, 2, 200, 101, 3, 4, 5};
	int lab_net_8b2[] = {0, 1, 2, 200, 201, 3, 4, 5};

	uni10::CUniTensor net;

	if (op.bondNum() == 4 && ket.bondNum() == 8) {

		op.setLabel( lab_op2 );
		ket.setLabel( lab_8b );
		net = uni10::contract( op, ket );
		net.permute( lab_net_8b2, 5 );
	}
	else if (op.bondNum() == 2) {

		if (ket.bondNum() == 5) {
			op.setLabel( lab_op1 );
			ket.setLabel( lab_5b );
			net = uni10::contract( op, ket );
			net.permute( lab_net_5b, 3 );
		}
		else if (ket.bondNum() == 8) {
			op.setLabel( lab_op1 );
			ket.setLabel( lab_8b );
			net = uni10::contract( op, ket );
			net.permute( lab_net_8b1, 5 );
		}
	}

	return net;	
}

//===============================================

uni10::CUniTensor netRectExpect( uni10::CUniTensor ket, uni10::CUniTensor op ) {
	///
	// todo: assert ket is netRect2Site
	int lab_ket[] = {0, 1, 2, 100, 101, 3, 4, 5};

	uni10::CUniTensor bra = ket;
	bra.permute( bra.label(), 3 );
	bra.conj();

	ket = netRectOp( ket, op );
	ket.setLabel( lab_ket );

	uni10::CUniTensor net = uni10::contract(bra, ket);

	return net;
}

//===============================================

uni10::CUniTensor netRectLGL( 
	uni10::CUniTensor lam0, uni10::CUniTensor gam, uni10::CUniTensor lam1,
	std::string pos ) {
	///
	// todo: available strings: "tl" "tr" "lr" "bl" "br" "tb"
	int lab_gam[] = {1, 3, 100, 5, 7};
	gam.setLabel( lab_gam );

	uni10::CUniTensor net;

	if (pos == "tl") {
		int lab_lam0[] = {2, 3};
		int lab_lam1[] = {0, 1};
		int lab_net[]  = {0, 2, 100, 5, 7};
		lam0.setLabel( lab_lam0 );
		lam1.setLabel( lab_lam1 );
		net = gam * lam0 * lam1;
		net.permute( lab_net, 3 );
	}
	else if (pos == "tr") {
		int lab_lam0[] = {2, 3};
		int lab_lam1[] = {5, 4};
		int lab_net[]  = {1, 2, 100, 4, 7};
		lam0.setLabel( lab_lam0 );
		lam1.setLabel( lab_lam1 );
		net = gam * lam0 * lam1;
		net.permute( lab_net, 3 );
	}
	else if (pos == "lr") {
		int lab_lam0[] = {0, 1};
		int lab_lam1[] = {5, 4};
		int lab_net[]  = {0, 3, 100, 4, 7};
		lam0.setLabel( lab_lam0 );
		lam1.setLabel( lab_lam1 );
		net = gam * lam0 * lam1;
		net.permute( lab_net, 3 );
	}
	else if (pos == "bl") {
		int lab_lam0[] = {7, 6};
		int lab_lam1[] = {0, 1};
		int lab_net[]  = {0, 3, 100, 5, 6};
		lam0.setLabel( lab_lam0 );
		lam1.setLabel( lab_lam1 );
		net = gam * lam0 * lam1;
		net.permute( lab_net, 3 );
	}
	else if (pos == "br") {
		int lab_lam0[] = {7, 6};
		int lab_lam1[] = {5, 4};
		int lab_net[]  = {1, 3, 100, 4, 6};
		lam0.setLabel( lab_lam0 );
		lam1.setLabel( lab_lam1 );
		net = gam * lam0 * lam1;
		net.permute( lab_net, 3 );
	}
	else if (pos == "tb") {
		int lab_lam0[] = {2, 3};
		int lab_lam1[] = {7, 6};
		int lab_net[]  = {1, 2, 100, 3, 6};
		lam0.setLabel( lab_lam0 );
		lam1.setLabel( lab_lam1 );
		net = gam * lam0 * lam1;
		net.permute( lab_net, 3 );
	}
	else {
		// raise exception
	}

	return net;
}

//===============================================

uni10::CUniTensor netRectNode( uni10::CUniTensor ket ) {
	///
	// todo: assert ket = 1-site gamma
	uni10::CUniTensor bra = ket;
	bra.permute( bra.label(), 2 );
	bra.conj();

	uni10::CUniTensor net;

	int lab_ket[] = {0, 2, 100, 4, 6};
	int lab_bra[] = {1, 3, 100, 5, 7};
	std::vector<int> cb_0;
	cb_0.push_back(0);
	cb_0.push_back(1);
	std::vector<int> cb_1;
	cb_1.push_back(2);
	cb_1.push_back(3);
	std::vector<int> cb_2;
	cb_2.push_back(4);
	cb_2.push_back(5);
	std::vector<int> cb_3;
	cb_3.push_back(6);
	cb_3.push_back(7);

	ket.setLabel( lab_ket );
	bra.setLabel( lab_bra );
	net = uni10::contract( bra, ket );

	net.combineBond( cb_0 );
	net.combineBond( cb_1 );
	net.combineBond( cb_2 );
	net.combineBond( cb_3 );

	net.permute( net.label(), 2 );

	return net;
}

//===============================================

uni10::CUniTensor netRectNode( uni10::CUniTensor ket, uni10::CUniTensor op ) {
	///
	// todo: assert ket = 1-site gamma; assert op = 1-site operator
	uni10::CUniTensor bra = ket;
	bra.permute( bra.label(), 2 );
	bra.conj();

	uni10::CUniTensor net;

	int lab_ket[] = {0, 2, 100, 4, 6};
	int lab_bra[] = {1, 3, 200, 5, 7};
	int lab_op[]  = {200, 100};
	std::vector<int> cb_0;
	cb_0.push_back(0);
	cb_0.push_back(1);
	std::vector<int> cb_1;
	cb_1.push_back(2);
	cb_1.push_back(3);
	std::vector<int> cb_2;
	cb_2.push_back(4);
	cb_2.push_back(5);
	std::vector<int> cb_3;
	cb_3.push_back(6);
	cb_3.push_back(7);

	ket.setLabel( lab_ket );
	bra.setLabel( lab_bra );
	op.setLabel( lab_op );
	net = uni10::contract( op, ket );
	net = uni10::contract( bra, net );

	net.combineBond( cb_0 );
	net.combineBond( cb_1 );
	net.combineBond( cb_2 );
	net.combineBond( cb_3 );

	net.permute( net.label(), 2 );

	return net;
}

//===============================================

uni10::CUniTensor netRectTRG( 
	uni10::CUniTensor Ttl, uni10::CUniTensor Ttr, 
	uni10::CUniTensor Tbl, uni10::CUniTensor Tbr ) {
	///
	// todo: assert bond types -- Ttl (1 in 2 out), Ttr (2 in 1 out), etc.
	uni10::CNetwork net( "net_rect_trg" );
	
	net.putTensor( "Ttl", Ttl );
	net.putTensor( "Ttr", Ttr );
	net.putTensor( "Tbl", Tbl );
	net.putTensor( "Tbr", Tbr );

	return net.launch();
}

//===============================================

uni10::CUniTensor netRectTRGExpect( 
	uni10::CUniTensor Ttl, uni10::CUniTensor Ttr,
	uni10::CUniTensor Tbl, uni10::CUniTensor Tbr ) {

	uni10::CNetwork net( "net_rect_expect" );

	net.putTensor( "Ttl", Ttl );
	net.putTensor( "Ttr", Ttr );
	net.putTensor( "Tbl", Tbl );
	net.putTensor( "Tbr", Tbr );

	return net.launch();
}
