#ifndef FUNC_OP_H
#define FUNC_OP_H
#include <string>
#include <uni10.hpp>

//======================================

class MPO {

public:
	/// constructor
	MPO( int dim, char loc );
	MPO( std::vector<int>& dim_l, std::vector<int>& dim_r );

	void putTensor( uni10::CUniTensor op, int row_idx, int col_idx );
	uni10::CUniTensor launch();

private:
	int virt_dim;
	char mpo_loc;
	uni10::CUniTensor mpo_frame;
	uni10::CUniTensor mpo;

};

//======================================

uni10::CUniTensor OP( std::string name );
uni10::CUniTensor mpoDummy( bool left, int dim_mpo, uni10::CUniTensor bdry_lam = uni10::CUniTensor() );
void importMPO( int len, std::string mpo_dir, std::vector<uni10::CUniTensor>& mpo, std::string bc );

#endif
