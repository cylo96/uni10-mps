#ifndef FUNC_EVOL_H
#define FUNC_EVOL_H
#include <uni10.hpp>
#include <tns-func/tns_const.h>

uni10::CUniTensor expBO( Complex beta, uni10::CUniTensor op );
uni10::CUniTensor expBO2( Complex beta, uni10::CUniTensor op );
//uni10::CUniTensor expBO3( Complex beta, uni10::CUniTensor op );
uni10::UniTensor expBO4( double beta, uni10::UniTensor op );

uni10::CUniTensor lamInv( uni10::CUniTensor lam );

void truncUpdate( uni10::CUniTensor& theta, uni10::CUniTensor& lbl, uni10::CUniTensor& ga,
	uni10::CUniTensor& la, uni10::CUniTensor& gb, uni10::CUniTensor& lbr );

void evol2Site( uni10::CUniTensor& lam0, uni10::CUniTensor& gam0, uni10::CUniTensor& lam1,
	uni10::CUniTensor& gam1, uni10::CUniTensor& lam2, uni10::CUniTensor op );

void evolHasting( uni10::CUniTensor& gala, uni10::CUniTensor& gblb, 
	uni10::CUniTensor& la, uni10::CUniTensor lb, 
	uni10::CUniTensor op, bool n, int maxBD );

void trotterSuzuki( uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1,
	uni10::CUniTensor op );

void trotterSuzuki( uni10::CUniTensor& gala, uni10::CUniTensor& gblb, 
	uni10::CUniTensor& la, uni10::CUniTensor& lb, 
	uni10::CUniTensor op,  bool n, int maxBD );

void trotterSuzuki2( uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1,
	uni10::CUniTensor op1, uni10::CUniTensor op2 );

void trotterSuzuki2( uni10::CUniTensor& gala, uni10::CUniTensor& gblb, 
	uni10::CUniTensor& la, uni10::CUniTensor& lb, 
	uni10::CUniTensor op1, uni10::CUniTensor op2, bool n, int maxBD );

void trotterSuzuki4( uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1,
	uni10::CUniTensor op1, uni10::CUniTensor op2, uni10::CUniTensor op3, uni10::CUniTensor op4 );

void trotterSuzuki4( uni10::CUniTensor& gala, uni10::CUniTensor& gblb, 
	uni10::CUniTensor& la, uni10::CUniTensor& lb, 
	uni10::CUniTensor op1, uni10::CUniTensor op2, 
	uni10::CUniTensor op3, uni10::CUniTensor op4, 
	bool n, int maxBD );

uni10::CUniTensor extractGamma( uni10::CUniTensor gala, uni10::CUniTensor la );
uni10::CUniTensor extractGamma2( uni10::CUniTensor laga, uni10::CUniTensor la );
#endif
