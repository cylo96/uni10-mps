#ifndef FUNC_NET2D_H
#define FUNC_NET2D_H
#include <uni10.hpp>

uni10::CUniTensor netRect2Site( 
	uni10::CUniTensor li, uni10::CUniTensor la1, uni10::CUniTensor la2, uni10::CUniTensor ga,
	uni10::CUniTensor lc, uni10::CUniTensor lb1, uni10::CUniTensor lb2, uni10::CUniTensor gb,
	uni10::CUniTensor lf, bool vertical = false );

uni10::CUniTensor netRectTheta( uni10::CUniTensor op,
	uni10::CUniTensor li, uni10::CUniTensor la1, uni10::CUniTensor la2, uni10::CUniTensor ga,
	uni10::CUniTensor lc, uni10::CUniTensor lb1, uni10::CUniTensor lb2, uni10::CUniTensor gb,
	uni10::CUniTensor lf, bool vertical = false );

uni10::CUniTensor netRectOp( uni10::CUniTensor ket, uni10::CUniTensor op );
uni10::CUniTensor netRectExpect( uni10::CUniTensor ket, uni10::CUniTensor op );

uni10::CUniTensor netRectLGL( 
	uni10::CUniTensor lam0, uni10::CUniTensor gam, uni10::CUniTensor lam1, 
	std::string pos = "tl" );

uni10::CUniTensor netRectNode( uni10::CUniTensor ket );
uni10::CUniTensor netRectNode( uni10::CUniTensor ket, uni10::CUniTensor op );

uni10::CUniTensor netRectTRG( 
	uni10::CUniTensor Ttl, uni10::CUniTensor Ttr,
	uni10::CUniTensor Tbl, uni10::CUniTensor Tbr );

uni10::CUniTensor netRectTRGExpect( 
	uni10::CUniTensor Ttl, uni10::CUniTensor Ttr,
	uni10::CUniTensor Tbl, uni10::CUniTensor Tbr );
#endif
