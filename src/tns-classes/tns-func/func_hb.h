#include <uni10.hpp>
#include <tns-func/tns_const.h>

uni10::UniTensor Hb_matvec( bool left, 
    uni10::UniTensor& cell, uni10::UniTensor& cell_dag, uni10::UniTensor& lambda, uni10::UniTensor& x );

uni10::UniTensor Hb_resid( bool left, 
	uni10::UniTensor& cell, uni10::UniTensor& cell_dag, uni10::UniTensor& lambda,
    uni10::UniTensor& x, uni10::UniTensor& b );

double Hb_overlap( uni10::UniTensor bra, uni10::UniTensor ket );
double Hb_vnorm( uni10::UniTensor vec );

uni10::UniTensor Hb_cg( bool left, 
	uni10::UniTensor& cell, uni10::UniTensor& cell_dag, uni10::UniTensor& lambda,
    uni10::UniTensor& b, uni10::UniTensor& x_trial, bool& early_term );

uni10::UniTensor Hb_gmres( bool left, 
	uni10::UniTensor& cell, uni10::UniTensor& cell_dag, uni10::UniTensor& lambda,
	uni10::UniTensor& b, uni10::UniTensor& x_trial, int ar_iter, bool& early_term );

uni10::CUniTensor findHb( bool left,
	uni10::CUniTensor zcell, uni10::CUniTensor zcell_dag, uni10::CUniTensor& zlambda,
	uni10::CUniTensor zb, uni10::CUniTensor zx_trial, int ar_iter = -1 );

uni10::CUniTensor findHb( bool left,
	uni10::CUniTensor zcell, uni10::CUniTensor zcell_dag, uni10::CUniTensor& zlambda,
	uni10::CUniTensor zb, int ar_iter = -1 );

