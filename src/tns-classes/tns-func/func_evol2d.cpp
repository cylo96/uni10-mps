#include <uni10/numeric/uni10_lapack.h>

#include <tns-func/func_evol2d.h>
#include <tns-func/func_evol.h>
#include <tns-func/func_net2d.h>

//============================

void updateRect2Site( uni10::CUniTensor& theta, 
	uni10::CUniTensor& li, uni10::CUniTensor& la1, uni10::CUniTensor& la2, uni10::CUniTensor& ga,
	uni10::CUniTensor& lc, uni10::CUniTensor& lb1, uni10::CUniTensor& lb2, uni10::CUniTensor& gb,
	uni10::CUniTensor& lf, bool vertical ) {

	if (theta.bondNum() != theta.inBondNum()*2)
		theta.permute(theta.label(), theta.bondNum()/2);	// permute theta to square matrix

	std::vector<uni10::CMatrix> theta_svd = theta.getBlock().svd();

	// truncate and update ga
	int lab_li[]  = {0, 1};
	int lab_la1[] = {2, 3};
	int lab_la2[] = {5, 4};
	int lab_gah[] = {1, 3, 100, 7, 5};
	int lab_gav[] = {3, 1, 100, 5, 7};
	int lab_ga2[] = {1, 3, 5, 100, 7};
	int lab_ga3h[]= {0, 2, 100, 7, 4};
	int lab_ga3v[]= {2, 0, 100, 4, 7};
	int dim_lc = (vertical)? ga.bond()[4].dim() : ga.bond()[3].dim();

	ga.setLabel( (vertical)? lab_gav : lab_gah );
	ga.permute( lab_ga2, 4 );
	ga.putBlock( theta_svd[0].resize(theta_svd[0].row(), dim_lc) );

	uni10::CUniTensor li_inv  = lamInv( li );
	uni10::CUniTensor la1_inv = lamInv( la1 );
	uni10::CUniTensor la2_inv = lamInv( la2 );
	li_inv.setLabel( lab_li );
	la1_inv.setLabel( lab_la1 );
	la2_inv.setLabel( lab_la2 );

	ga = ga * li_inv * la1_inv * la2_inv;
	ga.permute( (vertical)? lab_ga3v : lab_ga3h, 3 );
		
	// truncate and update lambda
	theta_svd[1].resize(dim_lc, dim_lc);
	theta_svd[1] *= ( 1.0 / theta_svd[1].norm() );	// rescale/normalize lambda
	lc.putBlock( theta_svd[1] );

	// truncate and update gb
	int lab_lb1[] = {9, 8};
	int lab_lb2[] = {10, 11};
	int lab_lf[]  = {12, 13};
	int lab_gbh[] = {6, 8, 101, 12, 10};
	int lab_gbv[] = {8, 6, 101, 10, 12};
	int lab_gb2[] = {6, 101, 8, 10, 12};
	int lab_gb3h[]= {6, 9, 101, 13, 11};
	int lab_gb3v[]= {9, 6, 101, 11, 13};

	gb.setLabel( (vertical)? lab_gbv : lab_gbh );
	gb.permute( lab_gb2, 1 );
	gb.putBlock( theta_svd[2].resize(dim_lc, theta_svd[2].col()) );

	uni10::CUniTensor lb1_inv = lamInv( lb1 );
	uni10::CUniTensor lb2_inv = lamInv( lb2 );
	uni10::CUniTensor lf_inv  = lamInv( lf );
	lb1_inv.setLabel( lab_lb1 );
	lb2_inv.setLabel( lab_lb2 );
	lf_inv.setLabel( lab_lf );

	gb = gb * lb1_inv * lb2_inv * lf_inv;
	gb.permute( (vertical)? lab_gb3v : lab_gb3h, 3 );

	theta_svd.clear();
}

//===========================================

void evolRect2Site( uni10::CUniTensor U, 
	uni10::CUniTensor& li, uni10::CUniTensor& la1, uni10::CUniTensor& la2, uni10::CUniTensor& ga,
	uni10::CUniTensor& lc, uni10::CUniTensor& lb1, uni10::CUniTensor& lb2, uni10::CUniTensor& gb,
	uni10::CUniTensor& lf, bool vertical ) {

	uni10::CUniTensor theta = netRectTheta(U, li, la1, la2, ga, lc, lb1, lb2, gb, lf, vertical);

	updateRect2Site(theta, li, la1, la2, ga, lc, lb1, lb2, gb, lf, vertical);
}

//===========================================

std::vector<uni10::CUniTensor> trgSVD( uni10::CUniTensor node, int chi_trg ) {

	std::vector<uni10::CMatrix> node_svd = node.getBlock().svd();

	int chi = (node_svd[1].row() < chi_trg)? node_svd[1].row() : chi_trg;

	node_svd[1].resize(chi, chi);
	//node_svd[1] *= ( 1.0 / node_svd[1].norm() );
	for (int i = 0; i < chi; ++i)
		node_svd[1].at(i, i) = std::sqrt( node_svd[1].at(i, i) );

	std::vector<uni10::Bond> bdn = node.bond();

	std::vector<uni10::Bond> bdu;
	bdu.push_back( bdn[0] );
	bdu.push_back( bdn[1] );
	bdu.push_back( uni10::Bond( uni10::BD_OUT, chi ) );

	std::vector<uni10::Bond> bdv;
	bdv.push_back( uni10::Bond( uni10::BD_IN, chi ) );
	bdv.push_back( bdn[2] );
	bdv.push_back( bdn[3] );

	uni10::CUniTensor nodeU( bdu );
	uni10::CUniTensor nodeV( bdv );


	nodeU.putBlock( node_svd[0].resize( node_svd[0].row(), chi ) * node_svd[1] );
	nodeV.putBlock( node_svd[1] * node_svd[2].resize( chi, node_svd[2].col() ) );

	std::vector<uni10::CUniTensor> two_nodes;
	two_nodes.push_back( nodeU );
	two_nodes.push_back( nodeV );

	return two_nodes;
}

