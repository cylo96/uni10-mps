#include <iostream>
#include <string>

#include <thermal/ChainThInf.h>
#include <thermal/func_th_net.h>
#include <thermal/func_th_evol.h>
#include <tns-func/func_convert.h>

//======================================

ChainThInf::ChainThInf(int d, int X) : ChainThPBC(2, d, X) {
	/// object constructor
}

//======================================

void ChainThInf::itebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS ) {
	/// perform iTEBD on iMPS using 2-site evolution operator
	std::vector<uni10::CUniTensor> gl;
	gl.push_back( tnetGL(gamma[0], lambda[1]) );
	gl.push_back( tnetGL(gamma[1], lambda[0]) );

	if (orderTS == 1) {
		uni10::CUniTensor U = texpBO3( I * dt, ham );       // generate exp(Hdt)

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			ttrotterSuzuki(gl[0], gl[1], lambda[1], lambda[0], U, true, chi_max);
			//trotterSuzuki(lambda[0], gamma[0], lambda[1], gamma[1], lambda[0], U);
		}
	}
	else if (orderTS == 2) {
		uni10::CUniTensor U1 = texpBO3( I * dt, ham );
		uni10::CUniTensor U2 = texpBO3( 0.5 * I * dt, ham );

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			ttrotterSuzuki2(gl[0], gl[1], lambda[1], lambda[0], U2, U1, true, chi_max);	
		}
	}
	else if (orderTS == 4) {
		double th = 1.0 / (2.0 - std::pow(2.0, 1./3.));
		uni10::CUniTensor U1 = texpBO3( I * dt * th * 0.5, ham );
		uni10::CUniTensor U2 = texpBO3( I * dt * th, ham );
		uni10::CUniTensor U3 = texpBO3( I * dt * (1.0-th) * 0.5, ham );
		uni10::CUniTensor U4 = texpBO3( I * dt * (1.0 - 2*th), ham );

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			ttrotterSuzuki4(gl[0], gl[1], lambda[1], lambda[0], U1, U2, U3, U4, true, chi_max);
		}
	}
	else {
		// raise exception
	}

	gamma[0] = textractGamma(gl[0], lambda[1]);
	gamma[1] = textractGamma(gl[1], lambda[0]);

	gl.clear();
}

//======================================

uni10::CUniTensor ChainThInf::expVal( uni10::CUniTensor op ) {
    /// return expectation value (a contracted uni10) of an 1-site/2-site operator
	std::vector<uni10::CUniTensor> expV;
	expV.push_back( texpect( tnetLGLGL(lambda[0], gamma[0], lambda[1], gamma[1], lambda[0]), op) );	
	expV.push_back( texpect( tnetLGLGL(lambda[1], gamma[1], lambda[0], gamma[0], lambda[1]), op) );	

    return 0.5 * (expV[0] + expV[1]);
}

//======================================

uni10::CUniTensor ChainThInf::expValStagger( uni10::CUniTensor op ) {
    /// return expectation value (a contracted uni10) of an 1-site/2-site operator
	std::vector<uni10::CUniTensor> expV;
	expV.push_back( texpect( tnetLGLGL(lambda[0], gamma[0], lambda[1], gamma[1], lambda[0]), op) );	
	expV.push_back( texpect( tnetLGLGL(lambda[1], gamma[1], lambda[0], gamma[0], lambda[1]), op) );	

    return 0.5 * (expV[0] + (-1.0) * expV[1]);
}

//======================================

