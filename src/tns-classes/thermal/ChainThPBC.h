#ifndef CHAINTHPBC_H
#define CHAINTHPBC_H
#include <thermal/CanonThMPS.h>

class ChainThPBC: public CanonThMPS {

public:
	/// constructor
	ChainThPBC(int L, int d, int X);

	void randomize();

private:

};

#endif
