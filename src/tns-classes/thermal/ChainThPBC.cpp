#include <iostream>
#include <cstdlib>
#include <time.h>

#include <thermal/ChainThPBC.h>
#include <tns-func/func_convert.h>

//======================================

ChainThPBC::ChainThPBC(int L, int d, int X) : CanonThMPS(L, d, X) {
	/// object constructor
}

//======================================

void ChainThPBC::randomize() {
	/// randomize a complex MPS having only real part
	std::srand( time(NULL) );

	if (gamma.size() > 0)
		gamma.clear();
	if (lambda.size() > 0)
		lambda.clear();

	for (int i = 0; i < lat_size; ++i) {

		gamma.push_back( initGamma(chi_max, chi_max, dim_phys) );
		gamma[i] = randT( gamma[i] );

		lambda.push_back( initLambda(chi_max) );
		lambda[i] = randT( lambda[i] );
	}
}

