#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <time.h>
#include <sys/stat.h>

#include <thermal/CanonThMPS.h>
#include <thermal/func_th_evol.h>

//======================================

CanonThMPS::CanonThMPS(int L, int d, int X) {
	/// object constructor
	lat_size = L;	// lattice size
	dim_phys = d;	// physical dimension
	chi_max = X;	// maximum bond dimension
}

//======================================

CanonThMPS::~CanonThMPS() {
	/// object destructor
	clear();
}

//======================================

void CanonThMPS::setSize(int L) {
	/// set lattice size
	// todo: if L < current lat_size => do slice
	//if ( L < lat_size )
	//	slice(L, 0);

	lat_size = L;
}

void CanonThMPS::setPhysD(int d) {
	/// set physical dimension
	dim_phys = d;
}

void CanonThMPS::setMaxBD(int X) {
	/// set maximum bond dimension
	// todo: if X < current chi_max => do truncation
	chi_max = X;
}

//======================================

int CanonThMPS::getSize() {
	///
	return lat_size;
}

int CanonThMPS::getPhysD() {
	///
	return dim_phys;
}

int CanonThMPS::getMaxBD() {
	///
	return chi_max;
}

//======================================

void CanonThMPS::clear() {
	/// clear tensors in current MPS; keep size and bond dim settings
	gamma.clear();
	lambda.clear();
}

//======================================

void CanonThMPS::vecSlice( std::vector<uni10::CUniTensor>& vec, int length, int start ) {
	/// slice vec into desired chunk
	// todo: assert start >= 0
	if (start == 0) {
		vec.resize(length);
	}
	else {
		vec.erase( vec.begin(), vec.begin()+start );
		vec.erase( vec.begin()+length, vec.end() );
	}
}

//======================================

void CanonThMPS::slice(int length, int start) {
	/// slice MPS into desired chunk
	// todo: assert start >= 0
	vecSlice( gamma, length, start );
	vecSlice( lambda, length, start );
}

//======================================

uni10::CUniTensor CanonThMPS::getGamma(int idx) {
	/// return the idx'th gamma
	return gamma[idx];
	// todo: raise exception if idx out of range
}

uni10::CUniTensor CanonThMPS::getLambda(int idx) {
	/// return the idx'th lambda
	return lambda[idx];
	// todo: raise exception if idx out of range
}

//======================================

uni10::CUniTensor CanonThMPS::initGamma(int chi1, int chi2, int d) {
	/// initialize a gamma tensor
	std::vector<uni10::Bond> bond_gam;
	bond_gam.push_back( uni10::Bond(uni10::BD_IN, chi1) );
	bond_gam.push_back( uni10::Bond(uni10::BD_IN, d) );
	bond_gam.push_back( uni10::Bond(uni10::BD_IN, d) );
	bond_gam.push_back( uni10::Bond(uni10::BD_OUT, chi2) );

	uni10::CUniTensor gam(bond_gam);

	return gam;
}

//======================================

uni10::CUniTensor CanonThMPS::initLambda(int chi) {
	/// initialize a lambda tensor
	std::vector<uni10::Bond> bond_lam;
	bond_lam.push_back( uni10::Bond(uni10::BD_IN, chi) );
	bond_lam.push_back( uni10::Bond(uni10::BD_OUT, chi) );

	uni10::CUniTensor lam(bond_lam);

	return lam;
}

//======================================

uni10::CUniTensor CanonThMPS::resizeGamma( uni10::CUniTensor gam0, int chi_l, int chi_r ) {
	/// resize a gamma tensor 
	std::vector<uni10::Bond> bd = gam0.bond();
	int chi1 = bd[0].dim();
	int d    = bd[1].dim();
	int chi2 = bd[3].dim();

	uni10::CUniTensor gam;

	if ( chi_l == chi1 && chi_r == chi2 ) {
		gam = gam0;
	}
	else {
		int dim1 = std::min(chi1, chi_l);
		int dim2 = std::min(chi2, chi_r);
		gam = initGamma( chi_l, chi_r, d );

		uni10::CMatrix gam0_blk = gam0.getBlock();
		uni10::CMatrix gam_blk = gam.getBlock();

		for (int i = 0; i < dim1 * d * d; ++i)
			for (int j = 0; j < dim2; ++j)
				gam_blk.at(i, j) = gam0_blk.at(i, j);

		gam.putBlock( gam_blk );
	}

	return gam;
}

//======================================

uni10::CUniTensor CanonThMPS::resizeLambda( uni10::CUniTensor lam0, int chi_new ) {
	/// resize a lambda tensor
	int chi = lam0.bond()[0].dim();
	uni10::CUniTensor lam;

	if ( chi_new == chi ) {
		lam = lam0;
	}
	else {
		int dim = std::min(chi, chi_new);
		lam = initLambda( chi_new );

		uni10::CMatrix lam0_blk = lam0.getBlock(); 
		uni10::CMatrix lam_blk = lam.getBlock(); 

		for (int i = 0; i < dim; ++i)
			lam_blk.at(i, i) = lam0_blk.at(i, i);

		lam.putBlock( lam_blk );
	}

	return lam;
}

//======================================

void CanonThMPS::putGamma(uni10::CUniTensor gam, int idx) {
	/// put a gamma tensor at gamma[idx]
	// todo: assert -1 <= idx < gamma.size(); 
	if (idx == -1)
		gamma.push_back( gam );
	else
		gamma[idx] = gam;
}

//======================================

void CanonThMPS::putLambda(uni10::CUniTensor lam, int idx) {
	/// put a lambda tensor at lambda[idx]
	// todo: assert -1 <= idx < lambda.size(); 
	if (idx == -1)
		lambda.push_back( lam );
	else
		lambda[idx] = lam;
}

//======================================

void CanonThMPS::exportGamma(int idx, const std::string& fname) {
	/// export a gamma tensor to file
	if ( fname == "" )
		gamma[idx].save( "gamma_" + std::to_string((long long)idx) );
	else
		gamma[idx].save( fname );
}

//======================================

void CanonThMPS::exportLambda(int idx, const std::string& fname) {
	/// export a gamma tensor to file
	if ( fname == "" )
		lambda[idx].save( "lambda_" + std::to_string((long long)idx) );
	else
		lambda[idx].save( fname );
}

//======================================

void CanonThMPS::importGamma(int idx, const std::string &fname, bool fit_chi) {
	/// import a gamma tensor from file. decide to fit chi_max or not
	uni10::CUniTensor gam0( fname );

	std::vector<uni10::Bond> bd = gam0.bond();
	int chi1 = bd[0].dim();
	int d    = bd[1].dim();
	int chi2 = bd[3].dim();

	if ( fit_chi && chi_max < std::max(chi1, chi2) ) {
		/// truncate bond dimension to fit chi_max
		int dim1 = std::min(chi1, chi_max);
		int dim2 = std::min(chi2, chi_max);
		uni10::CUniTensor gam = initGamma( dim1, dim2, d );

		uni10::CMatrix gam0_blk = gam0.getBlock();
		uni10::CMatrix gam_blk = gam.getBlock();

		for (int i = 0; i < dim1 * d * d; ++i)
			for (int j = 0; j < dim2; ++j)
				gam_blk.at(i, j) = gam0_blk.at(i, j);

		gam.putBlock( gam_blk );
		putGamma( gam, idx );
	}
	else {
		putGamma( gam0, idx );
	}
}

//======================================

void CanonThMPS::importLambda(int idx, const std::string &fname, bool fit_chi) {
	/// import a lambda tensor from file. decide to fit chi_max or not
	uni10::CUniTensor lam0( fname );

	int chi = lam0.bond()[0].dim();

	if ( fit_chi && chi_max < chi ) {
		/// truncate bond dimension to fit chi_max
		uni10::CUniTensor lam = initLambda( chi_max );

		uni10::CMatrix lam0_blk = lam0.getBlock(); 
		uni10::CMatrix lam_blk = lam.getBlock(); 

		for (int i = 0; i < chi_max; ++i)
			lam_blk.at(i, i) = lam0_blk.at(i, i);

		lam.putBlock( lam_blk );
		putLambda( lam, idx );
	}
	else {
		putLambda( lam0, idx );
	}
}

//======================================

void CanonThMPS::exportMPS( std::string dirname ) {
	/// export MPS to a directory
	// http://stackoverflow.com/questions/3828192/checking-if-a-directory-exists-in-unix-system-call
	// http://pubs.opengroup.org/onlinepubs/009695399/basedefs/sys/stat.h.html
	struct stat info;

	if ( stat( dirname.c_str(), &info ) != 0 )
		mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

	std::string fname;
	for (int i = 0; i < gamma.size(); ++i) {
		fname = dirname + "/gamma_" + std::to_string((long long)i);
		gamma[i].save( fname );
	}
	for (int i = 0; i < lambda.size(); ++i) {
		fname = dirname + "/lambda_" + std::to_string((long long)i);
		lambda[i].save( fname );
	}
}

//======================================

void CanonThMPS::importMPS( std::string dirname, int unit_cell, bool end_lam, bool fit_chi ) {
	/// import MPS from a directory
	// todo: raise exception if num of gam/lam files in folder < lat_size
	struct stat info;

	if ( stat( dirname.c_str(), &info ) == 0 && S_ISDIR(info.st_mode) ) {

		gamma.clear();
		lambda.clear();
		std::string fname;
		int last = 0;

		for (int i = 0; i < lat_size; ++i) {

			try {
				fname = dirname + "/gamma_" + std::to_string((long long)i);
				importGamma( -1, fname, fit_chi );
				last = i;
			}
			catch( const std::logic_error& e ) {
				if ( i > unit_cell - 1 ) {
					fname = dirname + "/gamma_" + std::to_string((long long) last-unit_cell+1+(i%unit_cell) );
					importGamma( -1, fname, fit_chi );
				}
			}
		}
		for (int i = 0; i < lat_size + (int)end_lam; ++i) {

			try {
				fname = dirname + "/lambda_" + std::to_string((long long)i);
				importLambda( -1, fname, fit_chi );
			}
			catch( const std::logic_error& e ) {
				if ( i > unit_cell - 1 ) {
					fname = dirname + "/lambda_" + std::to_string((long long) last-unit_cell+1+(i%unit_cell) );
					importLambda( -1, fname, fit_chi );
				}
			}
		}
	}
	else {
		std::cout << "CanonThMPS::importMPS : path name error.\n";
	}
}

//======================================

void CanonThMPS::importMPS( std::string dirname, bool end_lam ) {
	/// import MPS from a directory
	importMPS( dirname, 2, end_lam, false );
}

//======================================

void CanonThMPS::oneSiteOP( uni10::CUniTensor op, int idx ) {
	/// Act one-site operator on a specific site

	// todo: assert op is one-site operator
	int lab_gam[] = {0, 100, 101, 1};
	int lab_op[] = {200, 100};
	int lab_fin[] = {0, 200, 101, 1};

	gamma[idx].setLabel( lab_gam );
	op.setLabel( lab_op );
	gamma[idx] = uni10::contract( op, gamma[idx] );
	gamma[idx].permute( lab_fin, 3 );
}

//======================================

void CanonThMPS::mps2SiteSVD( uni10::CUniTensor& psi, 
	uni10::CUniTensor& lam0, uni10::CUniTensor& gam0, 
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1, uni10::CUniTensor& lam2 ) {
	///
	psi.permute( psi.label(), psi.bondNum()/2 );
	std::vector<uni10::CMatrix> svda = psi.getBlock().svd();

	int dim_l = gam0.bond()[0].dim();
	int dim_m = std::min( (int)svda[1].col(), chi_max );
	int dim_r = gam1.bond()[3].dim();

	if ( dim_m != gam0.bond()[3].dim() )
		gam0 = initGamma(dim_l, dim_m, dim_phys );
	if ( dim_m != lam1.bond()[0].dim() )
		lam1 = initLambda(dim_m);
	if ( dim_m != gam1.bond()[0].dim() )
		gam1 = initGamma(dim_m, dim_r, dim_phys );

	gam0.putBlock( svda[0].resize( dim_l * dim_phys * dim_phys, dim_m ) );
	gam0 = textractGamma2( gam0, lam0 );

	svda[1].resize(dim_m, dim_m);
	svda[1] *= ( 1.0 / svda[1].norm() );
	lam1.putBlock( svda[1] );

	gam1.permute( gam1.label(), 1 );
	gam1.putBlock( svda[2].resize( dim_m, dim_phys * dim_phys * dim_r ) );
	gam1.permute( gam1.label(), 3 );
	gam1 = textractGamma( gam1, lam2 );
}

//======================================


