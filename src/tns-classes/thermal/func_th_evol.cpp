#include <uni10/numeric/uni10_lapack.h>

#include <thermal/func_th_evol.h>
#include <thermal/func_th_net.h>
#include <tns-func/func_convert.h>

//#define MKL_Complex16 std::complex<double>
//#include <mkl.h>

// ======================================================

uni10::CMatrix texpd( const Complex& a, const uni10::Matrix& mat ) {

    std::vector<uni10::Matrix> rets = mat.eigh();

	uni10::Matrix UT(rets[1]);
	UT.cTranspose();
	std::vector<uni10::CMatrix> crets;
	for (int i = 0; i < rets.size(); i++) {
		crets.push_back( real2Complex(rets[i]) );
	}

	uni10::CMatrix cUT = real2Complex(UT);
    uni10::vectorExp(a, crets[0].getElem(), crets[0].row(), crets[0].isOngpu());

    return cUT * (crets[0] * crets[1]);
}

// ======================================================

uni10::CMatrix texpz( const Complex& a, const uni10::CMatrix& mat ) {

    std::vector<uni10::CMatrix> crets = mat.eig();

	uni10::CMatrix cUT(crets[1]);
	cUT.cTranspose();

    uni10::vectorExp(a, crets[0].getElem(), crets[0].row(), crets[0].isOngpu());

    return cUT * (crets[0] * crets[1]);
}

//============================
/* return the exp(beta * OP) in CUniTensor form */
uni10::CUniTensor texpBO( Complex beta, uni10::CUniTensor op ) {

	uni10::CUniTensor eBO( op.bond() );
	eBO.putBlock( uni10::exp(beta, op.getBlock()) );

	return eBO;
}

//============================
/* return the exp(beta * OP) in CUniTensor form */
uni10::CUniTensor texpBO2( Complex beta, uni10::CUniTensor op ) {

	uni10::CUniTensor eBO( op.bond() );
	uni10::UniTensor opr = complex2Real( op );
	eBO.putBlock( texpd(beta, opr.getBlock()) );

	return eBO;
}

//============================
/* return the exp(beta * OP) in CUniTensor form */
uni10::CUniTensor texpBO3( Complex beta, uni10::CUniTensor op ) {

	uni10::CUniTensor eBO( op.bond() );
	eBO.putBlock( texpz(beta, op.getBlock()) );

	return eBO;
}

//============================

uni10::CUniTensor tlamInv( uni10::CUniTensor lam ) {

	int dim = lam.bond()[0].dim();

	uni10::CMatrix lam_inv_mat(dim, dim, true);
	for (int i = 0; i < dim; ++i) {
		if ( lam.getBlock().at(i, i).real() < 1e-64 )
			lam_inv_mat.at(i, i) = Complex(0.0, 0.0);
		else
			lam_inv_mat.at(i, i) = Complex(1.0/lam.getBlock().at(i, i).real(), 0.0);
	}

	uni10::CUniTensor lam_inv( lam.bond() );
	lam_inv.putBlock( lam_inv_mat );

	return lam_inv;
}

//============================

void ttruncUpdate( uni10::CUniTensor& theta, uni10::CUniTensor& lbl, uni10::CUniTensor& ga, 
	uni10::CUniTensor& la, uni10::CUniTensor& gb, uni10::CUniTensor& lbr ) {

	std::vector<uni10::Bond> bd_a = ga.bond();
	int dim_l = bd_a[0].dim();    // get bond dimension
	int d     = bd_a[1].dim();
	int dim_m = bd_a[3].dim();

	std::vector<uni10::Bond> bd_b = gb.bond();
	int dim_r = bd_b[3].dim();

	if (theta.bondNum() != theta.inBondNum()*2)
		theta.permute(theta.label(), theta.bondNum()/2);	// permute theta to square matrix

	std::vector<uni10::CMatrix> theta_svd = theta.getBlock().svd();

	// truncate and update gamma
	ga.putBlock( theta_svd[0].resize(dim_l*d*d, dim_m) );

	uni10::CMatrix inv_lbl(dim_l, dim_l, true);
	for (int i = 0; i < dim_l; ++i) {
		if ( lbl.getBlock().at(i, i).real() < 1e-64 )
			inv_lbl.at(i, i) = Complex(0.0, 0.0);
		else
			inv_lbl.at(i, i) = Complex(1.0 / lbl.getBlock().at(i, i).real(), 0.0);
	}

	uni10::CMatrix inv_lbr(dim_r, dim_r, true);
	for (int i = 0; i < dim_r; ++i) {
		if ( lbr.getBlock().at(i, i).real() < 1e-64 )
			inv_lbr.at(i, i) = Complex(0.0, 0.0);
		else
			inv_lbr.at(i, i) = Complex(1.0 / lbr.getBlock().at(i, i).real(), 0.0);
	}

	ga.permute(ga.label(), 1);
	ga.putBlock( inv_lbl * ga.getBlock() );	// for corner gamma, set lbl = 1 (rank 0 tensor)
	ga.permute(ga.label(), 3);

	// truncate and update lambda
	theta_svd[1].resize(dim_m, dim_m);
	theta_svd[1] *= ( 1.0 / theta_svd[1].norm() );	// rescale/normalize lambda
	la.putBlock( theta_svd[1] );

	// truncate and update gamma2
	gb.permute(gb.label(), 1);
	gb.putBlock( theta_svd[2].resize(dim_m, d*d*dim_r) );
	gb.permute(gb.label(), 3);
	gb.putBlock( gb.getBlock() * inv_lbr );

	theta_svd.clear();
}

//===========================================

void tevol2Site( uni10::CUniTensor& lam0, uni10::CUniTensor& gam0, uni10::CUniTensor& lam1, 
	uni10::CUniTensor& gam1, uni10::CUniTensor& lam2, uni10::CUniTensor op ) {

	uni10::CUniTensor ket = tnetLGLGL( lam0, gam0, lam1, gam1, lam2 );
	ket = ttensorOp(ket, op);
	ttruncUpdate( ket, lam0, gam0, lam1, gam1, lam2 );
}

//===========================================

void tevolHasting( uni10::CUniTensor& gala, uni10::CUniTensor& gblb, uni10::CUniTensor& la, 
	uni10::CUniTensor lb, uni10::CUniTensor op, bool n, int maxBD ) {

	std::vector<uni10::Bond> bd = gala.bond();
	int D = bd[0].dim();    // get bond dimension
	int d = bd[1].dim();

	uni10::CUniTensor cell = tnetGLGL(gala, gblb);
	cell = ttensorOp(cell, op);

	int lab_lb[] = {0, 1};
	int lab_cell[] = {1, 100, 101, 200, 201, 5};
	lb.setLabel(lab_lb);
	cell.setLabel(lab_cell);
	uni10::CUniTensor theta = uni10::contract(lb, cell, false);

	//if (theta.bondNum() != theta.inBondNum()*2)
	//	theta.permute(theta.label(), theta.bondNum()/2); // permute theta to square matrix

	int lab_theta[] = {0, 100, 101, 5, 200, 201};
	theta.permute( lab_theta, 3 );

	std::vector<uni10::CMatrix> theta_svd = theta.getBlock().svd();

	int BD = (maxBD >= d*D)? d*D : maxBD;	// gradually grow BD with evolution
	// truncate and update lambda
	theta_svd[1].resize(BD, BD);
	double scale = theta_svd[1].norm();
	theta_svd[1] *= (n)? ( 1.0 / scale ) : 1.0;     // rescale/normalize lambda if n is true

	std::vector<uni10::Bond> la_bd;
	la_bd.push_back( uni10::Bond(uni10::BD_IN, BD) );
	la_bd.push_back( uni10::Bond(uni10::BD_OUT, BD) );
	la.assign(la_bd);
	la.putBlock( theta_svd[1] );

	// truncate and update gblb
	std::vector<uni10::Bond> gblb_bd;
	gblb_bd.push_back( uni10::Bond(uni10::BD_IN, BD) );
	gblb_bd.push_back( uni10::Bond(uni10::BD_OUT, D) );
	gblb_bd.push_back( uni10::Bond(uni10::BD_OUT, d) );
	gblb_bd.push_back( uni10::Bond(uni10::BD_OUT, d) );
	gblb.assign( gblb_bd );
	gblb.putBlock( theta_svd[2].resize(BD, D*d*d) );
	int lab_gblb0[] = {2, 5, 200, 201};
	int lab_gblb1[] = {2, 200, 201, 5};
	gblb.setLabel( lab_gblb0 );
	gblb.permute( lab_gblb1, 3 );

	// undate gala
	//int lab_gblb[] = {2, 200, 201, 5};
	//gblb.setLabel(lab_gblb);
	uni10::CUniTensor cgblb = gblb;
	cgblb.conj();

	gala = uni10::contract(cell, cgblb) * (1.0 / scale );
	gala.permute( gala.label(), 3 );

	theta_svd.clear();
}

//===========================================

void ttrotterSuzuki( uni10::CUniTensor& lam0, uni10::CUniTensor& gam0, 
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1, 
	uni10::CUniTensor op ) {
	/// for infinite system only
	tevol2Site( lam0, gam0, lam1, gam1, lam0, op );
	tevol2Site( lam1, gam1, lam0, gam0, lam1, op );
}

//===========================================

void ttrotterSuzuki( uni10::CUniTensor& gala, uni10::CUniTensor& gblb, 
	uni10::CUniTensor& la, uni10::CUniTensor& lb, 
	uni10::CUniTensor op,  bool n, int maxBD ) {
	/// for infinite system only
	tevolHasting( gala, gblb, la, lb, op, n, maxBD );
	tevolHasting( gblb, gala, lb, la, op, n, maxBD );
}

//===========================================

void ttrotterSuzuki2( uni10::CUniTensor& lam0, uni10::CUniTensor& gam0, 
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1, 
	uni10::CUniTensor op1, uni10::CUniTensor op2 ) {
	/// for infinite system only
	tevol2Site( lam0, gam0, lam1, gam1, lam0, op1 );
	tevol2Site( lam1, gam1, lam0, gam0, lam1, op2 );
	tevol2Site( lam0, gam0, lam1, gam1, lam0, op1 );
}

//===========================================

void ttrotterSuzuki2( uni10::CUniTensor& gala, uni10::CUniTensor& gblb, 
	uni10::CUniTensor& la, uni10::CUniTensor& lb,
	uni10::CUniTensor op1, uni10::CUniTensor op2, 
	bool n, int maxBD ) {
	/// for infinite system only
	tevolHasting( gala, gblb, la, lb, op1, n, maxBD );
	tevolHasting( gblb, gala, lb, la, op2, n, maxBD );
	tevolHasting( gala, gblb, la, lb, op1, n, maxBD );
}

//===========================================

void ttrotterSuzuki4( uni10::CUniTensor& lam0, uni10::CUniTensor& gam0, 
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1, 
	uni10::CUniTensor op1, uni10::CUniTensor op2, 
	uni10::CUniTensor op3, uni10::CUniTensor op4 ) {
	/// for infinite system only
	tevol2Site( lam0, gam0, lam1, gam1, lam0, op1 );
	tevol2Site( lam1, gam1, lam0, gam0, lam1, op2 );
	tevol2Site( lam0, gam0, lam1, gam1, lam0, op3 );
	tevol2Site( lam1, gam1, lam0, gam0, lam1, op4 );
	tevol2Site( lam0, gam0, lam1, gam1, lam0, op3 );
	tevol2Site( lam1, gam1, lam0, gam0, lam1, op2 );
	tevol2Site( lam0, gam0, lam1, gam1, lam0, op1 );
}

//===========================================

void ttrotterSuzuki4( uni10::CUniTensor& gala, uni10::CUniTensor& gblb, 
	uni10::CUniTensor& la, uni10::CUniTensor& lb,
	uni10::CUniTensor op1, uni10::CUniTensor op2, uni10::CUniTensor op3, uni10::CUniTensor op4, 
	bool n, int maxBD ) {
	/// for infinite system only
	tevolHasting( gala, gblb, la, lb, op1, n, maxBD );
	tevolHasting( gblb, gala, lb, la, op2, n, maxBD );
	tevolHasting( gala, gblb, la, lb, op3, n, maxBD );
	tevolHasting( gblb, gala, lb, la, op4, n, maxBD );
	tevolHasting( gala, gblb, la, lb, op3, n, maxBD );
	tevolHasting( gblb, gala, lb, la, op2, n, maxBD );
	tevolHasting( gala, gblb, la, lb, op1, n, maxBD );
}

//===========================================

uni10::CUniTensor textractGamma( uni10::CUniTensor gala, uni10::CUniTensor la ) {

	int D = la.getBlock().col();

	uni10::CMatrix inv_lambda(D, D, true);
	for (int i=0; i<D; i++) {
		if (la.getBlock().at(i, i).real() < 1e-64)
			inv_lambda.at(i, i) = Complex(0.0, 0.0);
		else
			inv_lambda.at(i, i) = 1.0 / la.getBlock().at(i, i);
	}

	gala.permute(gala.label(), 3);
	gala.putBlock( gala.getBlock() * inv_lambda );

	uni10::CUniTensor gam = gala;
	return gam;
}

//===========================================

uni10::CUniTensor textractGamma2( uni10::CUniTensor laga, uni10::CUniTensor la ) {

	int D = la.getBlock().col();

	uni10::CMatrix inv_lambda(D, D, true);
	for (int i=0; i<D; i++) {
		if (la.getBlock().at(i, i).real() < 1e-64)
			inv_lambda.at(i, i) = Complex(0.0, 0.0);
		else
			inv_lambda.at(i, i) = 1.0 / la.getBlock().at(i, i);
	}

	laga.permute(laga.label(), 1);
	laga.putBlock( inv_lambda * laga.getBlock() );
	laga.permute(laga.label(), 3);

	uni10::CUniTensor gam = laga;
	return gam;
}

//===========================================


