#ifndef CHAINTHSEMIINF_H
#define CHAINTHSEMIINF_H
#include "CanonThMPS.h"

/// ref: PHYSICAL REVIEW B 86, 245107 (2012)
class ChainThSemiInf: public CanonThMPS {

public:
	/// constructor
	ChainThSemiInf(int L, int d, int X);
	~ChainThSemiInf();

	uni10::CUniTensor getAs(int idx);
	void putAs(uni10::CUniTensor A, int idx = -1);
	void refresh();
	void randomize();
	void importMPS( std::string dirname, int unit_cell = 2 );

	void oneSiteOP( uni10::CUniTensor op, int idx = 0 );

	void tebd( uni10::CUniTensor hlw, uni10::CUniTensor hw,
		uni10::CUniTensor hwr, uni10::CUniTensor hr, Complex dt, int steps, int orderTS = 1 );

	uni10::CUniTensor expVal( uni10::CUniTensor op, int idx );

private:
	std::vector<uni10::CUniTensor> As;
	uni10::CUniTensor idl;
	uni10::CUniTensor idr;
	uni10::CUniTensor dummy;

	void updateCorner( uni10::CNetwork net, uni10::CUniTensor Ub, uni10::CUniTensor Ubw );

	uni10::CNetwork net_r;
	uni10::CNetwork net_l;
	uni10::CNetwork net_contr;
	uni10::CNetwork net_contr_o1;
	uni10::CNetwork net_contr_o2;
	uni10::CNetwork net_contr_last;
	uni10::CNetwork net_rcorner;

};

#endif
