#ifndef FUNC_TH_NET_H
#define FUNC_TH_NET_H
#include <uni10.hpp>

uni10::CUniTensor tbondInvGam( uni10::CUniTensor gam );

uni10::CUniTensor tnetLGLGL( 
	uni10::CUniTensor ll, uni10::CUniTensor ga, 
	uni10::CUniTensor la, uni10::CUniTensor gb, uni10::CUniTensor lb );

uni10::CUniTensor tnetLGLGL( 
	uni10::CUniTensor ll, uni10::CUniTensor gala, uni10::CUniTensor gblb );

uni10::CUniTensor tnetLG( uni10::CUniTensor la, uni10::CUniTensor ga );
uni10::CUniTensor tnetGL( uni10::CUniTensor ga, uni10::CUniTensor la );
uni10::CUniTensor tnetLL( uni10::CUniTensor la, uni10::CUniTensor lb );

uni10::CUniTensor tnetLGLG( 
	uni10::CUniTensor ll, uni10::CUniTensor ga, 
	uni10::CUniTensor la, uni10::CUniTensor gb );

uni10::CUniTensor tnetGLGL( 
	uni10::CUniTensor ga, uni10::CUniTensor la, 
	uni10::CUniTensor gb, uni10::CUniTensor lb );

uni10::CUniTensor tnetGLGL( uni10::CUniTensor gala, uni10::CUniTensor gblb );

uni10::CUniTensor tnetGLG( 
	uni10::CUniTensor ga, uni10::CUniTensor la, uni10::CUniTensor gb );

uni10::CUniTensor tnetLGL( 
	uni10::CUniTensor ll, uni10::CUniTensor ga, uni10::CUniTensor lr );

uni10::CUniTensor tnetLLL( 
	uni10::CUniTensor ll, uni10::CUniTensor la, uni10::CUniTensor lr );

uni10::CUniTensor tnetAA( uni10::CUniTensor A0, uni10::CUniTensor A1 );
uni10::CUniTensor tnetBB( uni10::CUniTensor B0, uni10::CUniTensor B1 );

uni10::CUniTensor ttranMtx( uni10::CUniTensor ket );
uni10::CUniTensor ttranMtx( uni10::CUniTensor ket, uni10::CUniTensor op );
uni10::CUniTensor ttranMtxChain( uni10::CUniTensor ket, int n );

uni10::CUniTensor ttensorOp( uni10::CUniTensor ket, uni10::CUniTensor op );
uni10::CUniTensor tnorm( uni10::CUniTensor ket );
uni10::CUniTensor ttraceRhoVec( uni10::CUniTensor vec, uni10::CUniTensor lam, bool left = true );
uni10::CUniTensor texpect( uni10::CUniTensor ket, uni10::CUniTensor op );

uni10::CUniTensor texpectOBCLongContract( 
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda, 
	uni10::CUniTensor op, int op_location);

#endif

