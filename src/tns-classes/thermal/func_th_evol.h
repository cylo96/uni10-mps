#ifndef FUNC_TH_EVOL_H
#define FUNC_TH_EVOL_H
#include <uni10.hpp>
#include <tns-func/tns_const.h>

uni10::CUniTensor texpBO( Complex beta, uni10::CUniTensor op );
uni10::CUniTensor texpBO2( Complex beta, uni10::CUniTensor op );
uni10::CUniTensor texpBO3( Complex beta, uni10::CUniTensor op );

uni10::CUniTensor tlamInv( uni10::CUniTensor lam );

void ttruncUpdate( uni10::CUniTensor& theta, uni10::CUniTensor& lbl, uni10::CUniTensor& ga,
	uni10::CUniTensor& la, uni10::CUniTensor& gb, uni10::CUniTensor& lbr );

void tevol2Site( uni10::CUniTensor& lam0, uni10::CUniTensor& gam0, uni10::CUniTensor& lam1,
	uni10::CUniTensor& gam1, uni10::CUniTensor& lam2, uni10::CUniTensor op );

void tevolHasting( uni10::CUniTensor& gala, uni10::CUniTensor& gblb, 
	uni10::CUniTensor& la, uni10::CUniTensor lb, 
	uni10::CUniTensor op, bool n, int maxBD );

void ttrotterSuzuki( uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1,
	uni10::CUniTensor op );

void ttrotterSuzuki( uni10::CUniTensor& gala, uni10::CUniTensor& gblb, 
	uni10::CUniTensor& la, uni10::CUniTensor& lb, 
	uni10::CUniTensor op,  bool n, int maxBD );

void ttrotterSuzuki2( uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1,
	uni10::CUniTensor op1, uni10::CUniTensor op2 );

void ttrotterSuzuki2( uni10::CUniTensor& gala, uni10::CUniTensor& gblb, 
	uni10::CUniTensor& la, uni10::CUniTensor& lb, 
	uni10::CUniTensor op1, uni10::CUniTensor op2, bool n, int maxBD );

void ttrotterSuzuki4( uni10::CUniTensor& lam0, uni10::CUniTensor& gam0,
	uni10::CUniTensor& lam1, uni10::CUniTensor& gam1,
	uni10::CUniTensor op1, uni10::CUniTensor op2, uni10::CUniTensor op3, uni10::CUniTensor op4 );

void ttrotterSuzuki4( uni10::CUniTensor& gala, uni10::CUniTensor& gblb, 
	uni10::CUniTensor& la, uni10::CUniTensor& lb, 
	uni10::CUniTensor op1, uni10::CUniTensor op2, 
	uni10::CUniTensor op3, uni10::CUniTensor op4, 
	bool n, int maxBD );

uni10::CUniTensor textractGamma( uni10::CUniTensor gala, uni10::CUniTensor la );
uni10::CUniTensor textractGamma2( uni10::CUniTensor laga, uni10::CUniTensor la );

#endif
