#include <iostream>
#include <cstdlib>
#include <time.h>

#include <thermal/ChainThOBC.h>
#include <thermal/func_th_net.h>
#include <thermal/func_th_evol.h>
#include <tns-func/func_convert.h>

//======================================

ChainThOBC::ChainThOBC(int L, int d, int X) : CanonThMPS(L, d, X) {
	/// object constructor

	// dummy tensor
	dummy = initLambda(1);
	dummy.identity();
}

//======================================

void ChainThOBC::randomize() {
	/// randomize a complex MPS having only real part
	std::srand( time(NULL) );

	if (gamma.size() > 0)
		gamma.clear();
	if (lambda.size() > 0)
		lambda.clear();

	lambda.push_back(dummy);

	for (int i = 0; i < lat_size; ++i) {

		int chi1 = (int) std::min( std::min( pow(dim_phys, i), pow(dim_phys, lat_size-i) ), (double) chi_max );
		int chi2 = (int) std::min( std::min( pow(dim_phys, i+1), pow(dim_phys, lat_size-i-1) ), (double) chi_max );

		gamma.push_back( initGamma(chi1, chi2, dim_phys) );
		gamma[i] = randT( gamma[i] );

		lambda.push_back( initLambda(chi2) );
		lambda[i+1] = randT( lambda[i+1] );
	}

	// replace last lambda with dummy (since it's at boundary)
	lambda.pop_back();
	lambda.push_back(dummy);
}

//======================================

void ChainThOBC::importMPS( std::string dirname, int unit_cell ) {
	///
	CanonThMPS::importMPS( dirname, unit_cell, true, true );

	int chi = lambda[0].bond()[0].dim();
	uni10::CMatrix uni(chi, chi);
	uni.set_zero();
	uni.at(0, 0) = Complex(1.0, 0.0);

	lambda[0] = initLambda(chi);
	lambda[0].putBlock(uni);
	lambda.pop_back();
	lambda.push_back( lambda[0] );
}

//======================================

void ChainThOBC::tebd( uni10::CUniTensor ham, uni10::CUniTensor hbl, uni10::CUniTensor hbr, 
	Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS
	// todo: handle the dimension trncation/expansion to fit chi_max

	// generate time evolution operator
	uni10::CUniTensor U   = texpBO3( I * dt, ham );
	uni10::CUniTensor Ubl = texpBO3( I * dt, hbl );
	uni10::CUniTensor Ubr = texpBO3( I * dt, hbr );

	if (orderTS == 1) {
		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update
			// update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if ( n == 0 )
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubl);
				else if ( n == lat_size-2 )
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubr);
				else
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], U);
			}
			// update even sites
			for (int n = 1; n < lat_size-1; n+=2) {

				if ( n == lat_size-2 )
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubr);
				else
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], U);
			}
		}
	}
	else if (orderTS == 2) {
		uni10::CUniTensor U2   = texpBO3( 0.5 * I * dt, ham );
		uni10::CUniTensor Ubl2 = texpBO3( 0.5 * I * dt, hbl );
		uni10::CUniTensor Ubr2 = texpBO3( 0.5 * I * dt, hbr );

		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update
			// update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if ( n == 0 )
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubl2);
				else if ( n == lat_size-2 )
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubr2);
				else
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], U2);
			}
			// update even sites
			for (int n = 1; n < lat_size-1; n+=2) {

				if ( n == lat_size-2 )
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubr);
				else
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], U);
			}
			// update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if ( n == 0 )
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubl2);
				else if ( n == lat_size-2 )
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ubr2);
				else
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], U2);
			}
		}
	}
}

//======================================
/*
void ChainThOBC::dmrg( std::string mpo_dir, int sweeps, int iter_max, double tolerance ) {
	///
	std::vector<uni10::CUniTensor> mpo;
	for (int i = 0; i < lat_size; ++i) {
		if (i == 0)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_l" ) );
		else if (i == lat_size-1)
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_r" ) );
		else
			mpo.push_back( uni10::CUniTensor( mpo_dir + "/mpo_m" ) );
	}

	uni10::CUniTensor ham;
	uni10::CUniTensor psi;
	double eng;

	for (int s = 0; s < sweeps; ++s) {
		//
		if (s%2 == 0) {
			// sweep from left to right
			for (int i = 0; i < lat_size; ++i) {
				//
				ham = netDMRGH( i, gamma, lambda, mpo );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), 3 );

				uni10::CMatrix ham_mtx = ham.getBlock();
				uni10::CMatrix psi_mtx = psi.getBlock();
				int s1 = ham_mtx.lanczosEigh( eng, psi_mtx, iter_max, tolerance );

				psi.putBlock( psi_mtx );
				psi.permute( psi.label(), 2 );

				if (i == lat_size-1) {
					gamma[i] = extractGamma( extractGamma2( psi, lambda[i] ), lambda[i+1] );
				}
				else {
					std::vector<uni10::CMatrix> svda = psi.getBlock().svd();

					int dim_l = gamma[i].bond()[0].dim();
					int dim_m = gamma[i].bond()[2].dim();
					gamma[i].putBlock( svda[0].resize( dim_l * dim_phys, dim_m ) );
					gamma[i] = extractGamma2( gamma[i], lambda[i] );

					svda[1].resize(dim_m, dim_m);
					svda[1] *= ( 1.0 / svda[1].norm() );
					lambda[i+1].putBlock( svda[1] );

					uni10::CUniTensor vi( lambda[i+1].bond() );
					vi.putBlock( svda[2].resize(dim_m, dim_m) );	
					gamma[i+1] = netLG( vi, gamma[i+1] );
				}
			}
		}
		else {
			// sweep from right to left
			for (int i = lat_size-1; i >= 0; --i) {
				//
				ham = netDMRGH( i, gamma, lambda, mpo );
				psi = netLGL( lambda[i], gamma[i], lambda[i+1] );
				psi.permute( psi.label(), 3 );

				uni10::CMatrix ham_mtx = ham.getBlock();
				uni10::CMatrix psi_mtx = psi.getBlock();
				int s1 = ham_mtx.lanczosEigh( eng, psi_mtx, iter_max, tolerance );

				psi.putBlock( psi_mtx );
				psi.permute( psi.label(), 1 );

				if (i == 0) {
					psi.permute( psi.label(), 2 );
					gamma[i] = extractGamma( extractGamma2( psi, lambda[i] ), lambda[i+1] );
				}
				else {
					std::vector<uni10::CMatrix> svdb = psi.getBlock().svd();

					int dim_m = gamma[i].bond()[0].dim();
					int dim_r = gamma[i].bond()[2].dim();
					uni10::CUniTensor vi( lambda[i].bond() );
					vi.putBlock( svdb[0].resize(dim_m, dim_m) );	
					gamma[i-1] = netGL( gamma[i-1], vi );

					svdb[1].resize(dim_m, dim_m);
					svdb[1] *= ( 1.0 / svdb[1].norm() );
					lambda[i].putBlock( svdb[1] );

					gamma[i].permute( gamma[i].label(), 1 );
					gamma[i].putBlock( svdb[2].resize( dim_m, dim_phys * dim_r ) );
					gamma[i].permute( gamma[i].label(), 2 );
					gamma[i] = extractGamma( gamma[i], lambda[i+1] );
				}
			}
		}
	}
} 
*/
//======================================

uni10::CUniTensor ChainThOBC::expVal( uni10::CUniTensor op, int idx, bool constBD ) {
	/// return expectation value (a contracted uni10) of an one-site operator on site idx
	return texpectOBCLongContract(gamma, lambda, op, idx);
}

//======================================
/*
uni10::CUniTensor ChainThOBC::correlation( uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 ) {
	/// 
	return correlationOBC( gamma, lambda, op1, op2, idx1, idx2 );
}
*/
