#ifndef CHAINTHOBC_H
#define CHAINTHOBC_H
#include "CanonThMPS.h"

class ChainThOBC: public CanonThMPS {

public:
	/// constructor
	ChainThOBC(int L, int d, int X);

	void randomize();

	void importMPS( std::string dirname, int unit_cell = 2 );

	void tebd( uni10::CUniTensor ham, uni10::CUniTensor hbl, uni10::CUniTensor hbr, 
		Complex dt, int steps, int orderTS = 1 );

	//void dmrg( std::string mpo_dir, int sweeps, int iter_max, double tolerance );

	uni10::CUniTensor expVal( uni10::CUniTensor op, int idx, bool constBD = false );
	//uni10::CUniTensor correlation( uni10::CUniTensor op1, uni10::CUniTensor op2, int idx1, int idx2 );

private:
	uni10::CUniTensor dummy;

};

#endif
