#include <algorithm>
#include <thermal/func_th_net.h>

// todo: rewrite these functions using Network class -- or not?
//======================================

uni10::CUniTensor tbondInvGam( uni10::CUniTensor gam ) {

	int lab_old[] = {0, 100, 101, 1};
	int lab_new[] = {1, 100, 101, 0};

	gam.setLabel( lab_old );
	gam.permute( lab_new, 2 );

	return gam;
}

//===================================

uni10::CUniTensor tnetLGLGL( 
	uni10::CUniTensor ll, uni10::CUniTensor ga, 
	uni10::CUniTensor la, uni10::CUniTensor gb, uni10::CUniTensor lb ) {

	int lab_ll[] = {0, 1};
	int lab_ga[] = {1, 100, 101, 2};
	int lab_la[] = {2, 3};
	int lab_gb[] = {3, 200, 201, 4};
	int lab_lb[] = {4, 5};

	ll.setLabel( lab_ll );
	ga.setLabel( lab_ga );
	la.setLabel( lab_la );
	gb.setLabel( lab_gb );
	lb.setLabel( lab_lb );

	uni10::CUniTensor net = uni10::contract( ll, ga );
	net = uni10::contract( net, la );
	net = uni10::contract( net, gb );
	net = uni10::contract( net, lb );
	net.permute( net.label(), 5 );

	return net;
}

//======================================

uni10::CUniTensor tnetLGLGL( 
	uni10::CUniTensor ll, uni10::CUniTensor gala, uni10::CUniTensor gblb ) {

	int lab_ll[] = {0, 1};
	int lab_gala[] = {1, 100, 101, 3};
	int lab_gblb[] = {3, 200, 201, 5};

	ll.setLabel( lab_ll );
	gala.setLabel( lab_gala );
	gblb.setLabel( lab_gblb );

	uni10::CUniTensor net = uni10::contract( ll, gala );
	net = uni10::contract( gala, gblb );
	net.permute( net.label(), 5 );

	return net;
}

//======================================

uni10::CUniTensor tnetLG( uni10::CUniTensor la, uni10::CUniTensor ga ) {

	int lab_la[] = {0, 1};
	int lab_ga[] = {1, 100, 101, 2};

	la.setLabel( lab_la );
	ga.setLabel( lab_ga );

	uni10::CUniTensor net = uni10::contract( la, ga );
	net.permute( net.label(), 3 );

	return net;
}

//=====================================

uni10::CUniTensor tnetGL( uni10::CUniTensor ga, uni10::CUniTensor la ) {

	int lab_ga[] = {0, 100, 101, 1};
	int lab_la[] = {1, 2};

	ga.setLabel( lab_ga );
	la.setLabel( lab_la );

	uni10::CUniTensor net = uni10::contract( ga, la );

	return net;
}

//======================================

uni10::CUniTensor tnetLL( uni10::CUniTensor la, uni10::CUniTensor lb ) {

	int lab_la[] = {0, 1};
	int lab_lb[] = {1, 2};

	la.setLabel( lab_la );
	lb.setLabel( lab_lb );

	uni10::CUniTensor net = uni10::contract( la, lb );

	return net;
}

//======================================

uni10::CUniTensor tnetLGLG( 
	uni10::CUniTensor ll, uni10::CUniTensor ga, 
	uni10::CUniTensor la, uni10::CUniTensor gb ) {

	int lab_ll[] = {0, 1};
	int lab_ga[] = {1, 100, 101, 2};
	int lab_la[] = {2, 3};
	int lab_gb[] = {3, 200, 201, 4};

	ll.setLabel( lab_ll );
	ga.setLabel( lab_ga );
	la.setLabel( lab_la );
	gb.setLabel( lab_gb );

	uni10::CUniTensor net = uni10::contract( ll, ga );
	net = uni10::contract( net, la );
	net = uni10::contract( net, gb );
	net.permute( net.label(), 5 );

	return net;
}

//================================================

uni10::CUniTensor tnetGLGL( 
	uni10::CUniTensor ga, uni10::CUniTensor la, 
	uni10::CUniTensor gb, uni10::CUniTensor lb ) {

	int lab_ga[] = {0, 100, 101, 1};
	int lab_la[] = {1, 2};
	int lab_gb[] = {2, 200, 201, 3};
	int lab_lb[] = {3, 4};

	ga.setLabel( lab_ga );
	la.setLabel( lab_la );
	gb.setLabel( lab_gb );
	lb.setLabel( lab_lb );

	uni10::CUniTensor net = uni10::contract( ga, la );
	net = uni10::contract( net, gb );
	net = uni10::contract( net, lb );
	net.permute( net.label(), 5 );

	return net;
}

//===============================================

uni10::CUniTensor tnetGLGL( uni10::CUniTensor gala, uni10::CUniTensor gblb ) {

	int lab_gala[] = {0, 100, 101, 2};
	int lab_gblb[] = {2, 200, 201, 4};

	gala.setLabel( lab_gala );
	gblb.setLabel( lab_gblb );

	uni10::CUniTensor net = uni10::contract( gala, gblb );
	net.permute( net.label(), 5 );

	return net;
}

//================================================

uni10::CUniTensor tnetGLG( 
	uni10::CUniTensor ga, uni10::CUniTensor la, uni10::CUniTensor gb ) {

	int lab_ga[] = {0, 100, 101, 1};
	int lab_la[] = {1, 2};
	int lab_gb[] = {2, 200, 201, 3};

	ga.setLabel( lab_ga );
	la.setLabel( lab_la );
	gb.setLabel( lab_gb );

	uni10::CUniTensor net = uni10::contract( ga, la );
	net = uni10::contract( net, gb );
	net.permute( net.label(), 5 );

	return net;
}

//================================================

uni10::CUniTensor tnetLGL( 
	uni10::CUniTensor ll, uni10::CUniTensor ga, uni10::CUniTensor lr ) {

	int lab_ll[] = {0, 1};
	int lab_ga[] = {1, 100, 101, 2};
	int lab_lr[] = {2, 3};

	ll.setLabel( lab_ll );
	ga.setLabel( lab_ga );
	lr.setLabel( lab_lr );

	uni10::CUniTensor net = uni10::contract( ll, ga );
	net = uni10::contract( net, lr );

	return net;
}

//================================================

uni10::CUniTensor tnetLLL( 
	uni10::CUniTensor ll, uni10::CUniTensor la, uni10::CUniTensor lr ) {

	int lab_ll[] = {0, 1};
	int lab_la[] = {1, 2};
	int lab_lr[] = {2, 3};

	ll.setLabel( lab_ll );
	la.setLabel( lab_la );
	lr.setLabel( lab_lr );

	uni10::CUniTensor net = uni10::contract( ll, la );
	net = uni10::contract( net, lr );

	return net;
}

//================================================

uni10::CUniTensor tnetAA( uni10::CUniTensor A0, uni10::CUniTensor A1 ) {

	int lab_A0[] = {0, 100, 101, 1};
	int lab_A1[] = {1, 200, 201, 2};

	A0.setLabel( lab_A0 );
	A1.setLabel( lab_A1 );

	uni10::CUniTensor net = uni10::contract( A0, A1 );
	net.permute( net.label(), 5 );

	return net;
}

//================================================

uni10::CUniTensor tnetBB( uni10::CUniTensor B0, uni10::CUniTensor B1 ) {

	int lab_B0[] = {1, 100, 101, 0};
	int lab_B1[] = {2, 200, 201, 1};

	B0.setLabel( lab_B0 );
	B1.setLabel( lab_B1 );

	uni10::CUniTensor net = uni10::contract( B1, B0 );
	net.permute( net.label(), 5 );

	return net;
}

//================================================

uni10::CUniTensor ttranMtx( uni10::CUniTensor ket ) {

	// todo: assert bondNum >= 3
	uni10::CUniTensor bra = ket;
	bra.permute(ket.label(), 1).conj();

	int bn = ket.bondNum();
	std::vector<int> lab_ket;
	std::vector<int> lab_bra;

	for (int i = 0; i < bn; ++i) {

		if (i == 0 || i == bn-1) {
			lab_ket.push_back(i);
			lab_bra.push_back(i+1);
		}
		else {
			lab_ket.push_back(i+100);
			lab_bra.push_back(i+100);
		}
	}

	ket.setLabel( lab_ket );
	bra.setLabel( lab_bra );

	uni10::CUniTensor net = uni10::contract( bra, ket );

	int lab_net[4];
	lab_net[0] = net.label()[0];
	lab_net[1] = net.label()[2];
	lab_net[2] = net.label()[1];
	lab_net[3] = net.label()[3];
	net.permute( lab_net, 2 );

	return net;
}

//===============================================

uni10::CUniTensor ttranMtx( uni10::CUniTensor ket, uni10::CUniTensor op ) { 

	// todo: assert bondNum >= 3
	// todo: assert op is one-site operator
	uni10::CUniTensor bra = ket;
	bra.permute(ket.label(), 1).conj();

	int bn = ket.bondNum();
	std::vector<int> lab_ket;
	std::vector<int> lab_bra;
	std::vector<int> lab_op;

	for (int i = 0; i < bn; ++i) {

		if ( op.bondNum() == 2 ) {
			if (i == 0 || i == bn-1) {
				lab_ket.push_back(i);
				lab_bra.push_back(i+1);
			}
			else if (i == 1) {
				lab_bra.push_back(i+200);
				lab_ket.push_back(i+100);
				lab_op.push_back(i+200);
				lab_op.push_back(i+100);	
			}
			else {
				lab_ket.push_back(i+100);
				lab_bra.push_back(i+100);
			}
		}

		else if ( op.bondNum() == 4 ) {
			if (i == 0 || i == bn-1) {
				lab_ket.push_back(i);
				lab_bra.push_back(i+1);
			}
			else if (i == 1 || i == 3) {
				lab_bra.push_back(i+200);
				lab_ket.push_back(i+100);
				lab_op.push_back(i+200);
				lab_op.push_back(i+100);	
			}
			else {
				lab_ket.push_back(i+100);
				lab_bra.push_back(i+100);
			}
		}
	}

	if ( op.bondNum() == 4 ) {
		std::swap( lab_op[1], lab_op[2] );
	}

	ket.setLabel( lab_ket );
	bra.setLabel( lab_bra );
	op.setLabel( lab_op );

	uni10::CUniTensor net = uni10::contract(op, ket);
	net = uni10::contract(bra, net);

	int lab_net[4];
	lab_net[0] = net.label()[0];
	lab_net[1] = net.label()[2];
	lab_net[2] = net.label()[1];
	lab_net[3] = net.label()[3];
	net.permute( lab_net, 2 );

	return net;
}

//===============================================

/// Construct the transfer matrix which length = 2^n unit cells
uni10::CUniTensor ttranMtxChain( uni10::CUniTensor ket, int n ) {

	uni10::CUniTensor net = ttranMtx( ket );
	uni10::CUniTensor cell = ttranMtx( ket );

	int lab1[] = {0, 1, 2, 3};
	int lab2[] = {2, 3, 4, 5};

	for (int i = 0; i < n; ++i){

		net.setLabel( lab1 );
		cell.setLabel( lab2 );

		net = uni10::contract( net, cell, false );
		cell = net;
	}

	return net;
}

//===============================================

uni10::CUniTensor ttensorOp( uni10::CUniTensor ket, uni10::CUniTensor op ) {

	// todo: assert ket.bondNum() == 4 or 6
	// todo: assert op is one-site or two-site operator
	int lab_1s[] = {0, 100, 101, 2};
	int lab_2s[] = {0, 100, 101, 102, 103, 5};
	int lab_op1[] = {200, 100};
	int lab_op2[] = {200, 202, 100, 102};
	int lab_net_1s[] = {0, 200, 101, 2};
	int lab_net_2s1[] = {0, 200, 101, 102, 103, 5};
	int lab_net_2s2[] = {0, 200, 101, 202, 103, 5};

	uni10::CUniTensor net;

	if (op.bondNum() == 4 && ket.bondNum() == 6) {

		op.setLabel( lab_op2 );
		ket.setLabel( lab_2s );
		net = uni10::contract( op, ket );
		net.permute( lab_net_2s2, 5 );
	}
	else if (op.bondNum() == 2) {

		if (ket.bondNum() == 4) {
			op.setLabel( lab_op1 );
			ket.setLabel( lab_1s );
			net = uni10::contract( op, ket );
			net.permute( lab_net_1s, 3 );
		}
		else if (ket.bondNum() == 6) {
			op.setLabel( lab_op1 );
			ket.setLabel( lab_2s );
			net = uni10::contract( op, ket );
			net.permute( lab_net_2s1, 5 );
		}
	}

	return net;	
}

//=============================================

uni10::CUniTensor tnorm( uni10::CUniTensor ket ) {

	// todo: assert 3 <= ket.bonNum() <= ?
	uni10::CUniTensor bra = ket;
	bra.permute(ket.label(), 1).conj();

	uni10::CUniTensor net = uni10::contract(bra, ket);

	return net;
}

//=============================================

uni10::CUniTensor ttraceRhoVec( uni10::CUniTensor vec, uni10::CUniTensor lam, bool left ) {
	///
	int lab_vec[] = {0, 1};
	int lab_ll0[] = {0, 2};
	int lab_ll1[] = {1, 2};
	int lab_lr0[] = {2, 0};
	int lab_lr1[] = {2, 1};

	uni10::CUniTensor net;

	if (left) {
		vec.setLabel( lab_vec );
		lam.setLabel( lab_ll0 );
		net = uni10::contract( lam, vec, false );
		lam.setLabel( lab_ll1 );
		net = uni10::contract( net, lam, false );
		return net;
	}
	else {
		vec.setLabel( lab_vec );
		lam.setLabel( lab_lr0 );
		net = uni10::contract( lam, vec, false );
		lam.setLabel( lab_lr1 );
		net = uni10::contract( net, lam, false );
		return net;
	}	
}

//===============================================

uni10::CUniTensor texpect( uni10::CUniTensor ket, uni10::CUniTensor op ) {

	// todo: assert ket.bondNum() == 3 or 4
	uni10::CUniTensor bra = ket;
	bra.permute(ket.label(), 1).conj();

	uni10::CUniTensor net = ttensorOp( ket, op );
	bra.setLabel( net.label() );
	net = uni10::contract( bra, net );

	return net;
}

//=============================================

uni10::CUniTensor texpectOBCLongContract( 
	std::vector<uni10::CUniTensor>& gamma, std::vector<uni10::CUniTensor>& lambda, 
	uni10::CUniTensor op, int op_location ) {

	uni10::CUniTensor net;
	uni10::CUniTensor ket;
	uni10::CUniTensor bra;

	int lab_vec[] = {0, 1};
	int lab_bra1[]= {0, 100, 101, 2};
	int lab_ket1[]= {1, 100, 101, 3};
	int lab_bra2[]= {0, 100, 101, 200, 201, 2};
	int lab_ket2[]= {1, 100, 101, 200, 201, 3};

	net = tnetLL( lambda[0], lambda[0] );
	net.permute( net.label(), 0 );
	net.setLabel( lab_vec );

	int L = gamma.size();

	for (int i = 0; i < L; ++i) {

		if ( i == op_location ) {

			if ( op.bondNum() == 2 ) {
				ket = tnetGL( gamma[i], lambda[i+1] );
				bra = ket;
				bra.permute( bra.label(), 1 ).conj();

				ket = ttensorOp( ket, op );
				ket.setLabel( lab_ket1 );
				bra.setLabel( lab_bra1 );
			}
			else if ( op.bondNum() == 4 ) {
				ket = tnetGLGL( gamma[i], lambda[i+1], gamma[i+1], lambda[i+2] );
				bra = ket;
				bra.permute( bra.label(), 1 ).conj();

				ket = ttensorOp( ket, op );
				ket.setLabel( lab_ket2 );
				bra.setLabel( lab_bra2 );
				i += 1;
			}
		}
		else {
			ket = tnetGL( gamma[i], lambda[i+1] );
			bra = ket;
			bra.permute( bra.label(), 1 ).conj();
			ket.setLabel( lab_ket1 );
			bra.setLabel( lab_bra1 );
		}

		net = uni10::contract( net, ket );
		net = uni10::contract( bra, net );
		net.setLabel( lab_vec );
	}

	uni10::CUniTensor id = lambda[L];
	id.identity();
	id.permute( id.label(), 2 );

	net.setLabel( lab_vec );
	id.setLabel( lab_vec );
	net = uni10::contract( net, id );
	
	return net;
}

//=================================================


