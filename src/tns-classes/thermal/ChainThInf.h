#ifndef CHAINTHINF_H
#define CHAINTHINF_H
#include <thermal/ChainThPBC.h>

class ChainThInf: public ChainThPBC {

public:
	/// constructor
	ChainThInf(int d, int X);

	void itebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS = 1 );

	uni10::CUniTensor expVal( uni10::CUniTensor op );
	uni10::CUniTensor expValStagger( uni10::CUniTensor op );

private:

};

#endif
