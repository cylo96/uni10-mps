#include <iostream>
#include <cstdlib>
#include <time.h>

#include <thermal/ChainThSemiInf.h>
#include <thermal/func_th_net.h>
#include <thermal/func_th_evol.h>
#include <tns-func/func_convert.h>

//======================================

ChainThSemiInf::ChainThSemiInf(int L, int d, int X) : CanonThMPS(L, d, X) {
	/// object constructor

	// todo: assert L even

	// dummy tensor
	dummy = initLambda(1);
	dummy.identity();

	// left corner identity
	idl = dummy;
	// right corner identity
	idr = initLambda(chi_max);
	idr.identity();
	idr.permute(idr.label(), 2);

	net_r = uni10::CNetwork("exp_th_rvec");
	net_l = uni10::CNetwork("exp_th_lvec");
	net_contr = uni10::CNetwork("exp_th_contr");
	net_contr_o1 = uni10::CNetwork("exp_th_contr_op");
	net_contr_o2 = uni10::CNetwork("exp_th_contr_op2");
	net_contr_last = uni10::CNetwork("exp_th_contr_last");
	net_rcorner = uni10::CNetwork("net_th_rcorner");
}

//======================================

ChainThSemiInf::~ChainThSemiInf() {
	/// object destructor
	As.clear();
	gamma.clear();
	lambda.clear();
}

//======================================

uni10::CUniTensor ChainThSemiInf::getAs(int idx) {
	/// return the idx'th As
	return As[idx];
}

//======================================

void ChainThSemiInf::putAs(uni10::CUniTensor A, int idx) {
	/// put a As tensor at As[idx]
	// todo: assert -1 <= idx < gamma.size();
	if (idx == -1)
		As.push_back( A );
	else
		As[idx] = A;
}

//======================================

void ChainThSemiInf::refresh() {
	/// refresh As vector using gamma lambda vectors
	// todo: assert gamma.size() lambda.size() > sth
	if (As.size() == 0) {
		for (int n = 0; n < lat_size; ++n) {
			if (n == lat_size-1)
				putAs( tnetGL( tnetLG( lambda[n], gamma[n] ), lambda[n+1] ) );
			else
				putAs( tnetLG( lambda[n], gamma[n] ) );
		}
	}
	else if (As.size() == lat_size) {
		for (int n = 0; n < lat_size; ++n) {
			if (n == lat_size-1)
				As[n] = tnetGL( tnetLG( lambda[n], gamma[n] ), lambda[n+1] );
			else
				As[n] = tnetLG( lambda[n], gamma[n] );
		}
	}
}

//======================================

void ChainThSemiInf::randomize() {
	/// randomize a complex MPS having only real part
	std::srand( time(NULL) );

	if (As.size() > 0)
		As.clear();
	if (gamma.size() > 0)
		gamma.clear();
	if (lambda.size() > 0)
		lambda.clear();

	lambda.push_back( dummy );	// lambda[0]

	for (int i = 0; i < lat_size; ++i) {

		int chi1 = (int) std::min( pow(dim_phys, i), (double) chi_max );
		int chi2 = (int) std::min( pow(dim_phys, i+1), (double) chi_max );

		As.push_back( initGamma(chi1, chi2, dim_phys) );
		As[i] = randT( As[i] );
		gamma.push_back( As[i] );

		lambda.push_back( initLambda(chi2) );
		lambda[i+1].identity();
	}
}

//======================================

void ChainThSemiInf::importMPS( std::string dirname, int unit_cell ) {
	///
	CanonThMPS::importMPS( dirname, unit_cell, true, true );

	int chi = lambda[0].bond()[0].dim();
	uni10::CMatrix uni(chi, chi);
	uni.set_zero();
	uni.at(0, 0) = Complex(1.0, 0.0);

	lambda[0] = initLambda(chi);
	lambda[0].putBlock(uni);

	refresh();
}

//======================================

void ChainThSemiInf::oneSiteOP( uni10::CUniTensor op, int idx ) {
	/// Act one-site operator on a specific site
	CanonThMPS::oneSiteOP( op, idx );
	As[idx] = tnetLG( lambda[idx], gamma[idx] );
}

//======================================

uni10::CUniTensor ChainThSemiInf::expVal( uni10::CUniTensor op, int idx ) {
	/// return expectation value (a contracted uni10) of an one-site operator on site idx

	// left vector: corner id * corner id
	// loop over sites
	// if not idx, transfer matrix = As * As_dagger
	// if at idx, transfer matrix = As * op * As_dagger
	// right vector: id * id
	uni10::CUniTensor idL;
	int BD = lambda[0].bond()[0].dim();

	if ( BD > 1 ) {
		idL = initLambda( BD );
		idL.identity();
	}
	else {
		idL = idl;
	}

	net_r.putTensor( "bot", idr );
	net_r.putTensor( "top", idr.permute(idr.label(), 1) );
	uni10::CUniTensor rvec = net_r.launch();
	idr.permute(idr.label(), 2);

	net_l.putTensor( "bot", idL );
	net_l.putTensor( "top", idL.permute(idL.label(), 0) );
	uni10::CUniTensor lvec = net_l.launch();
	idL.permute(idL.label(), 1);

	for (int i = 0; i < lat_size; ++i) {

		if (i == idx) {
			if ( op.bondNum() == 2 ) {
				net_contr_o1.putTensor( "left", lvec );
				net_contr_o1.putTensor( "bot", As[i] );
				net_contr_o1.putTensor( "top", As[i].permute(As[i].label(), 1).conj() );
				net_contr_o1.putTensor( "op", op );
				lvec = net_contr_o1.launch();

				As[i].permute(As[i].label(), 3).conj();
			}
			else if ( op.bondNum() == 4 ) {
				net_contr_o2.putTensor( "left", lvec );
				net_contr_o2.putTensor( "bot1", As[i] );
				net_contr_o2.putTensor( "bot2", As[i+1] );
				net_contr_o2.putTensor( "top1", As[i].permute(As[i].label(), 1).conj() );
				net_contr_o2.putTensor( "top2", As[i+1].permute(As[i+1].label(), 1).conj() );
				net_contr_o2.putTensor( "op", op );
				lvec = net_contr_o2.launch();

				As[i].permute(As[i].label(), 3).conj();
				As[i+1].permute(As[i+1].label(), 3).conj();
				i += 1;
			}
		}
		else {
			net_contr.putTensor( "left", lvec );
			net_contr.putTensor( "bot", As[i] );
			net_contr.putTensor( "top", As[i].permute(As[i].label(), 1).conj() );
			lvec = net_contr.launch();

			As[i].permute(As[i].label(), 3).conj();
		}
	}

	net_contr_last.putTensor( "left", lvec );
	net_contr_last.putTensor( "right", rvec );
	uni10::CUniTensor expect = net_contr_last.launch();

	return expect;
}

//======================================

void ChainThSemiInf::updateCorner( uni10::CNetwork net, uni10::CUniTensor Ub, uni10::CUniTensor Ubw ) {
	/// evolve and update corner tensor
	net.putTensor("Id", idr);
	net.putTensor("As", As[lat_size-1]);
	net.putTensor("Ub", Ub);
	net.putTensor("Ubw", Ubw);
	uni10::CUniTensor theta = net.launch();
	As[lat_size-1] = theta.permute( theta.label(), 3 );
}

//======================================

void ChainThSemiInf::tebd( uni10::CUniTensor hlw, uni10::CUniTensor hw,
	uni10::CUniTensor hwr, uni10::CUniTensor hr, Complex dt, int steps, int orderTS ) {
	/// perform TEBD on MPS, involving multipications of lambda^-1
	// todo: handle the dimension trncation/expansion to fit chi_max

	// time evolution operator
	uni10::CUniTensor Ulw = texpBO3( I * dt, hlw );
	uni10::CUniTensor Uw  = texpBO3( I * dt, hw );
	uni10::CUniTensor Uwr = texpBO3( I * dt, hwr );
	uni10::CUniTensor Ur  = texpBO3( I * dt, hr );

	if (orderTS == 1) {
		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if ( n == 0 )
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ulw);
				else
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);
			}
			refresh();

			// evolve and update corners
			updateCorner( net_rcorner, Ur, Uwr );
			gamma[lat_size-1] = textractGamma2( textractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );


			// evolve and update even sites
			// sweep left to right
			for (int n = 1; n < lat_size-1; n+=2)
				tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);

			refresh();
		}
	}
	else if (orderTS == 2) {
		uni10::CUniTensor Ulw2 = texpBO3( 0.5 * I * dt, hlw );
		uni10::CUniTensor Uw2  = texpBO3( 0.5 * I * dt, hw );
		uni10::CUniTensor Uwr2 = texpBO3( 0.5 * I * dt, hwr );
		uni10::CUniTensor Ur2  = texpBO3( 0.5 * I * dt, hr );

		for (int ite = 0; ite < steps; ++ite) {
			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if ( n == 0 )
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ulw2);
				else
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw2);
			}
			refresh();

			// evolve and update corners
			updateCorner( net_rcorner, Ur2, Uwr2 );
			gamma[lat_size-1] = textractGamma2( textractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );


			// evolve and update even sites
			// sweep left to right
			for (int n = 1; n < lat_size-1; n+=2)
				tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw);

			refresh();

			// evolve and update odd sites
			for (int n = 0; n < lat_size-1; n+=2) {

				if ( n == 0 )
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Ulw2);
				else
					tevol2Site(lambda[n], gamma[n], lambda[n+1], gamma[n+1], lambda[n+2], Uw2);
			}
			refresh();

			// evolve and update corners
			updateCorner( net_rcorner, Ur2, Uwr2 );
			gamma[lat_size-1] = textractGamma2( textractGamma(As[lat_size-1], lambda[lat_size]), lambda[lat_size-1] );
		}
	}
}

//======================================
