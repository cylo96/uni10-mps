#ifndef NETINF_H
#define NETINF_H
#include "NetPBC.h"

class NetInf: public NetPBC {

public:
	/// constructor
	NetInf(int d, int X);

	void itebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS = 1 );
	void itebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS, 
		std::vector<monitor> mon_in, int interval = 10);

	Complex expVal( uni10::CUniTensor op );
	Complex expValStagger( uni10::CUniTensor op );

	Complex expValTRG( uni10::CUniTensor op, int chi_trg, int trg_steps );

private:

	std::vector<uni10::CUniTensor> trg_base_nodes;
	std::vector<uni10::CUniTensor> trg_expv_nodes;

	void updateTRG( int chi_trg, int steps = 1 );
	void updateTRG( uni10::CUniTensor op, int site, int chi_trg, int steps = 1 );

	std::vector<std::string> mon_avail;
	void monitorOutput( std::vector<monitor> monitors, uni10::CUniTensor ham );
};

#endif
