#include <iostream>
#include <string>

#include <peps/NetInf.h>
#include <tns-func/func_net2d.h>
#include <tns-func/func_evol.h>
#include <tns-func/func_evol2d.h>
#include <tns-func/func_meas.h>
#include <tns-func/func_convert.h>

//======================================

NetInf::NetInf(int d, int X) : NetPBC(2, 1, d, X) {
	/// object constructor

	mon_avail.push_back( "energy" );
	mon_avail.push_back( "entropy" );
	mon_avail.push_back( "spec" );
}

//======================================

void NetInf::monitorOutput( std::vector<monitor> monitors, uni10::CUniTensor ham ) {
	/// set output format of monitoring
	for (int m = 0; m < monitors.size(); ++m) {
		if (monitors[m].name == "energy")
			std::cout << std::setprecision(10) << expVal( ham ).real() << "\t";
		else if (monitors[m].name == "entropy")
			std::cout << std::setprecision(10) << entanglementEntropy( lambda_x[0][1] ) << "\t";
		else if (monitors[m].name == "spec") {
			uni10::Matrix spec = entanglementSpec( lambda_x[0][1], monitors[m].idx );
			for (int i = 0; i < monitors[m].idx; ++i)
				std::cout << std::setprecision(10) << spec.at(i, i) << "\t";
		}
		else
			std::cout << std::setprecision(10) << expVal( uni10::CUniTensor(monitors[m].name) ).real() << "\t";
	}
	std::cout << "\n";
}

//======================================

void NetInf::itebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS ) {
	/// perform iTEBD on iPEPS using 2-site evolution operator
	if (orderTS == 1) {
		uni10::CUniTensor U = expBO2( I * dt, ham );

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			evolRect2Site( U, 
				lambda_x[0][0], lambda_y[0][0], lambda_y[0][1], gamma[0][0],
				lambda_x[0][1], lambda_y[0][1], lambda_y[0][0], gamma[0][1], 
				lambda_x[0][0], false );
			evolRect2Site( U, 
				lambda_y[0][1], lambda_x[0][1], lambda_x[0][0], gamma[0][1],
				lambda_y[0][0], lambda_x[0][0], lambda_x[0][1], gamma[0][0], 
				lambda_y[0][1], true );
			evolRect2Site( U, 
				lambda_x[0][1], lambda_y[0][1], lambda_y[0][0], gamma[0][1],
				lambda_x[0][0], lambda_y[0][0], lambda_y[0][1], gamma[0][0], 
				lambda_x[0][1], false );
			evolRect2Site( U, 
				lambda_y[0][0], lambda_x[0][0], lambda_x[0][1], gamma[0][0],
				lambda_y[0][1], lambda_x[0][1], lambda_x[0][0], gamma[0][1], 
				lambda_y[0][0], true );
		}
	}
/*
	else if (orderTS == 2) {
		uni10::CUniTensor U1 = expBO2( I * dt, ham );
		uni10::CUniTensor U2 = expBO2( 0.5 * I * dt, ham );

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			trotterSuzuki2(gl[0], gl[1], lambda[0], lambda[1], U2, U1, true, chi_max);	
		}
	}
	else if (orderTS == 4) {
		double th = 1.0 / (2.0 - std::pow(2.0, 1./3.));
		uni10::CUniTensor U1 = expBO2( I * dt * th * 0.5, ham );
		uni10::CUniTensor U2 = expBO2( I * dt * th, ham );
		uni10::CUniTensor U3 = expBO2( I * dt * (1.0-th) * 0.5, ham );
		uni10::CUniTensor U4 = expBO2( I * dt * (1.0 - 2*th), ham );

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			trotterSuzuki4(gl[0], gl[1], lambda[0], lambda[1], U1, U2, U3, U4, true, chi_max);
		}
	}
*/
	else {
		// raise exception
	}
}

//======================================

void NetInf::itebd( uni10::CUniTensor ham, Complex dt, int steps, int orderTS,
	std::vector<monitor> mon_in, int interval ) {
	/// perform iTEBD on iMPS using 2-site evolution operator
	// todo: available monitor list
	std::vector<monitor> mon_out = availMonitors( mon_in, mon_avail );

	if (orderTS == 1) {
		uni10::CUniTensor U = expBO2( I * dt, ham );       // generate exp(Hdt)

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			evolRect2Site( U, 
				lambda_x[0][0], lambda_y[0][0], lambda_y[0][1], gamma[0][0],
				lambda_x[0][1], lambda_y[0][1], lambda_y[0][0], gamma[0][1], 
				lambda_x[0][0], false );
			evolRect2Site( U, 
				lambda_y[0][1], lambda_x[0][1], lambda_x[0][0], gamma[0][1],
				lambda_y[0][0], lambda_x[0][0], lambda_x[0][1], gamma[0][0], 
				lambda_y[0][1], true );
			evolRect2Site( U, 
				lambda_x[0][1], lambda_y[0][1], lambda_y[0][0], gamma[0][1],
				lambda_x[0][0], lambda_y[0][0], lambda_y[0][1], gamma[0][0], 
				lambda_x[0][1], false );
			evolRect2Site( U, 
				lambda_y[0][0], lambda_x[0][0], lambda_x[0][1], gamma[0][0],
				lambda_y[0][1], lambda_x[0][1], lambda_x[0][0], gamma[0][1], 
				lambda_y[0][0], true );

			if (ite % interval == 0)
				monitorOutput( mon_out, ham );
		}
	}
/*
	else if (orderTS == 2) {
		uni10::CUniTensor U1 = expBO2( I * dt, ham );
		uni10::CUniTensor U2 = expBO2( 0.5 * I * dt, ham );

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			trotterSuzuki2(gl[0], gl[1], lambda[0], lambda[1], U2, U1, true, chi_max);	

			if (ite % interval == 0)
				monitorOutput( mon_out, ham );
		}
	}
	else if (orderTS == 4) {
		double th = 1.0 / (2.0 - std::pow(2.0, 1./3.));
		uni10::CUniTensor U1 = expBO2( I * dt * th * 0.5, ham );
		uni10::CUniTensor U2 = expBO2( I * dt * th, ham );
		uni10::CUniTensor U3 = expBO2( I * dt * (1.0-th) * 0.5, ham );
		uni10::CUniTensor U4 = expBO2( I * dt * (1.0 - 2*th), ham );

		for (int ite = 0; ite < steps; ++ite) {
			//====== evolve and update ======
			trotterSuzuki4(gl[0], gl[1], lambda[0], lambda[1], U1, U2, U3, U4, true, chi_max);

			if (ite % interval == 0)
				monitorOutput( mon_out, ham );
		}
	}
*/
	else {
		// raise exception
	}
}

//======================================

Complex NetInf::expVal( uni10::CUniTensor op ) {
    /// return expectation value of an 1-site/2-site operator
	uni10::CUniTensor id( op.bond() );
	id.identity();

	std::vector<uni10::CUniTensor> expV;
	std::vector<uni10::CUniTensor> norm;

	uni10::CUniTensor ket = netRect2Site( 
		lambda_x[0][0], lambda_y[0][0], lambda_y[0][1], gamma[0][0],
		lambda_x[0][1], lambda_y[0][1], lambda_y[0][0], gamma[0][1], 
		lambda_x[0][0], false );

	expV.push_back( netRectExpect( ket, op ) );	
	norm.push_back( netRectExpect( ket, id ) );	

	ket = netRect2Site( 
		lambda_x[0][1], lambda_y[0][1], lambda_y[0][0], gamma[0][1],
		lambda_x[0][0], lambda_y[0][0], lambda_y[0][1], gamma[0][0], 
		lambda_x[0][1], false );

	expV.push_back( netRectExpect( ket, op ) );	
	norm.push_back( netRectExpect( ket, id ) );	

	return 0.5 * (expV[0][0]/norm[0][0] + expV[1][0]/norm[1][0]);
}

//======================================

Complex NetInf::expValStagger( uni10::CUniTensor op ) {
    /// return staggered expectation value of an 1-site operator
	uni10::CUniTensor id( op.bond() );
	id.identity();

	std::vector<uni10::CUniTensor> expV;
	std::vector<uni10::CUniTensor> norm;

	uni10::CUniTensor ket = netRect2Site( 
		lambda_x[0][0], lambda_y[0][0], lambda_y[0][1], gamma[0][0],
		lambda_x[0][1], lambda_y[0][1], lambda_y[0][0], gamma[0][1], 
		lambda_x[0][0], false );

	expV.push_back( netRectExpect( ket, op ) );	
	norm.push_back( netRectExpect( ket, id ) );	

	ket = netRect2Site( 
		lambda_x[0][1], lambda_y[0][1], lambda_y[0][0], gamma[0][1],
		lambda_x[0][0], lambda_y[0][0], lambda_y[0][1], gamma[0][0], 
		lambda_x[0][1], false );

	expV.push_back( netRectExpect( ket, op ) );	
	norm.push_back( netRectExpect( ket, id ) );	

    return 0.5 * (expV[0][0]/norm[0][0] + (-1.0) * expV[1][0]/norm[1][0]);
}

//======================================

void NetInf::updateTRG( int chi_trg, int steps ) {
	///
	// todo: assert gamma lambda not empty
	if (trg_base_nodes.size() == 0) {
		for (int i = 0; i < gamma[0].size(); ++i)
			trg_base_nodes.push_back( netRectNode( netRectLGL( lambda_y[0][i], gamma[0][i], lambda_x[0][i] ) ) );
	}

	std::vector< std::vector<uni10::CUniTensor> > frags;

	for (int s = 0; s < steps; ++s) {
		// permute node0 (gammaA) to meet trgSVD's default orientation
		int lab_no[] = {0, 1, 2, 3};
		int lab_nn[] = {3, 0, 1, 2};
		trg_base_nodes[0].setLabel( lab_no );
		trg_base_nodes[0].permute( lab_nn, 2 );

		frags.push_back( trgSVD( trg_base_nodes[0], chi_trg ) );
		frags.push_back( trgSVD( trg_base_nodes[1], chi_trg ) );

		trg_base_nodes[0] = netRectTRG( frags[1][1], frags[0][0], frags[0][1], frags[1][0] );
		trg_base_nodes[1] = netRectTRG( frags[1][1], frags[0][0], frags[0][1], frags[1][0] );

		frags.clear();
	}
}

//======================================

void NetInf::updateTRG( uni10::CUniTensor op, int site, int chi_trg, int steps ) {
	///
	// todo: assert gamma lambda not empty; assert site = 0 or 1
	if (trg_base_nodes.size() == 0) {
		for (int i = 0; i < gamma[0].size(); ++i)
			trg_base_nodes.push_back( netRectNode( netRectLGL( lambda_y[0][i], gamma[0][i], lambda_x[0][i] ) ) );
	}

	if (trg_expv_nodes.size() == 0) {
		for (int i = 0; i < gamma[0].size(); ++i) {
			if (i == site)
				trg_expv_nodes.push_back( netRectNode( netRectLGL( lambda_y[0][i], gamma[0][i], lambda_x[0][i] ), op ) );
			else
				trg_expv_nodes.push_back( netRectNode( netRectLGL( lambda_y[0][i], gamma[0][i], lambda_x[0][i] ) ) );
		}
		for (int i = 0; i < gamma[0].size(); ++i) {
			int j = (i+1)%2;
			trg_expv_nodes.push_back( netRectNode( netRectLGL( lambda_y[0][j], gamma[0][j], lambda_x[0][j] ) ) );
		}
	}

	std::vector< std::vector<uni10::CUniTensor> > base_frags;
	std::vector< std::vector<uni10::CUniTensor> > expv_frags;

	for (int s = 0; s < steps; ++s) {
		// permute node0 (gammaA) to meet trgSVD's default orientation
		int lab_no[] = {0, 1, 2, 3};
		int lab_nn[] = {3, 0, 1, 2};
		trg_base_nodes[0].setLabel( lab_no );
		trg_base_nodes[0].permute( lab_nn, 2 );
		trg_expv_nodes[0].setLabel( lab_no );
		trg_expv_nodes[0].permute( lab_nn, 2 );
		trg_expv_nodes[3].setLabel( lab_no );
		trg_expv_nodes[3].permute( lab_nn, 2 );

		base_frags.push_back( trgSVD( trg_base_nodes[0], chi_trg ) );
		base_frags.push_back( trgSVD( trg_base_nodes[1], chi_trg ) );
		expv_frags.push_back( trgSVD( trg_expv_nodes[0], chi_trg ) );
		expv_frags.push_back( trgSVD( trg_expv_nodes[1], chi_trg ) );
		expv_frags.push_back( trgSVD( trg_expv_nodes[2], chi_trg ) );
		expv_frags.push_back( trgSVD( trg_expv_nodes[3], chi_trg ) );

		trg_base_nodes[0] = netRectTRG( base_frags[1][1], base_frags[0][0], base_frags[0][1], base_frags[1][0] );
		double max_elem = 0.0;
		for (int i = 0; i < trg_base_nodes[0].elemNum(); ++i) {
			if ( trg_base_nodes[0][i].real() > max_elem )
				max_elem = trg_base_nodes[0][i].real();
		}
		trg_base_nodes[0] *= ( 1./max_elem );
		trg_base_nodes[1] = trg_base_nodes[0];
		trg_expv_nodes[0] = netRectTRG( base_frags[1][1], expv_frags[0][0], base_frags[0][1], expv_frags[2][0] ) * ( 1./max_elem );
		trg_expv_nodes[1] = netRectTRG( base_frags[1][1], base_frags[0][0], expv_frags[0][1], expv_frags[1][0] ) * ( 1./max_elem );
		trg_expv_nodes[2] = netRectTRG( expv_frags[2][1], expv_frags[3][0], base_frags[0][1], base_frags[1][0] ) * ( 1./max_elem );
		trg_expv_nodes[3] = netRectTRG( expv_frags[1][1], base_frags[0][0], expv_frags[3][1], base_frags[1][0] ) * ( 1./max_elem );

		base_frags.clear();
		expv_frags.clear();
	}
}

//======================================

Complex NetInf::expValTRG( uni10::CUniTensor op, int chi_trg, int trg_steps ) {
    /// return expectation value of an 1-site operator
	std::vector<uni10::CUniTensor> expV;
	std::vector<uni10::CUniTensor> norm;

	updateTRG( op, 0, chi_trg, trg_steps );
	expV.push_back( netRectTRGExpect( trg_expv_nodes[0], trg_expv_nodes[1], trg_expv_nodes[2], trg_expv_nodes[3] ) );	
	norm.push_back( netRectTRGExpect( trg_base_nodes[0], trg_base_nodes[1], trg_base_nodes[1], trg_base_nodes[0] ) );	

	return expV[0][0]/norm[0][0];
}

