#include <iostream>
#include <cstdlib>
#include <time.h>

#include <peps/NetPBC.h>
#include <tns-func/func_convert.h>

//======================================

NetPBC::NetPBC(int L, int W, int d, int X) : CanonPEPS(L, W, d, X) {
	/// object constructor
}

//======================================

void NetPBC::randomize() {
	/// randomize a complex MPS having only real part
	std::srand( time(NULL) );

	if (gamma.size() > 0)
		gamma.clear();
	if (lambda_x.size() > 0)
		lambda_x.clear();
	if (lambda_y.size() > 0)
		lambda_y.clear();

	std::vector<uni10::CUniTensor> temp;

	for (int i = 0; i < lat_wid; ++i) {

		for (int j = 0; j < lat_len; ++j) {
			temp.push_back( initGamma(chi_max, chi_max, chi_max, chi_max, dim_phys) );
			temp[j] = randT( temp[j] );
		}
		gamma.push_back( temp );
		temp.clear();

		for (int j = 0; j < lat_len; ++j) {
			temp.push_back( initLambda(chi_max) );
			temp[j] = randT( temp[j] );
		}
		lambda_x.push_back( temp );
		temp.clear();

		for (int j = 0; j < lat_len; ++j) {
			temp.push_back( initLambda(chi_max) );
			temp[j] = randT( temp[j] );
		}
		lambda_y.push_back( temp );
		temp.clear();
	}
}

