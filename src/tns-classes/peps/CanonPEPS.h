#ifndef CANONPEPS_H
#define CANONPEPS_H
#include <string>
#include <uni10.hpp>
#include <tns-func/tns_const.h>

class CanonPEPS {

public:
	/// constructor
	CanonPEPS(int L, int W, int d, int X);
	/// destructor
	~CanonPEPS();

	void setSize(int L, int W);
	void setPhysD(int d);
	void setMaxBD(int X);

	uni10::CUniTensor getGamma( int y_idx, int x_idx );
	uni10::CUniTensor getLambdaX( int y_idx, int x_idx );
	uni10::CUniTensor getLambdaY( int y_idx, int x_idx );
	void putGamma( uni10::CUniTensor gam, int y_idx, int x_idx );
	void putLambdaX( uni10::CUniTensor lam, int y_idx, int x_idx );
	void putLambdaY( uni10::CUniTensor lam, int y_idx, int x_idx );

	void oneSiteOP( uni10::CUniTensor op, int y_idx, int x_idx );

	void slice( int length, int width, int y_idx, int x_idx );
	void clear();

protected:
	int lat_len;
	int lat_wid;
	int dim_phys;
	int chi_max;
	std::vector< std::vector<uni10::CUniTensor> > gamma;
	std::vector< std::vector<uni10::CUniTensor> > lambda_x;
	std::vector< std::vector<uni10::CUniTensor> > lambda_y;

	void vecSlice( std::vector<uni10::CUniTensor>& vec, int length, int start = 0);
	void vecSlice( std::vector< std::vector<uni10::CUniTensor> >& vec, int length, int start = 0);
	uni10::CUniTensor initGamma(int x_chi1, int x_chi2, int y_chi1, int y_chi2, int d);
	uni10::CUniTensor initLambda(int chi);

	std::vector<monitor> availMonitors( std::vector<monitor> monitors, std::vector<std::string> avil_list );
};

#endif
