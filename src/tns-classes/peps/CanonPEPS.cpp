#include <iostream>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <time.h>

#include <peps/CanonPEPS.h>

//======================================

CanonPEPS::CanonPEPS( int L, int W, int d, int X ) {
	/// object constructor
	lat_len = L;	// lattice length (x dimension)
	lat_wid = W;	// lattice width (y dimension)
	dim_phys = d;	// physical dimension
	chi_max = X;	// maximum bond dimension
}

//======================================

CanonPEPS::~CanonPEPS() {
	/// object destructor
	clear();
}

//======================================

void CanonPEPS::setSize( int L, int W ) {
	/// set lattice size
	// todo: if L, W < current lat_size => do slice; else do nothing

	lat_len = L;
	lat_wid = W;
}

void CanonPEPS::setPhysD( int d ) {
	/// set physical dimension
	dim_phys = d;
}

void CanonPEPS::setMaxBD( int X ) {
	/// set maximum bond dimension
	// todo: if X < current chi_max => do truncation
	chi_max = X;
}

//======================================

void CanonPEPS::clear() {
	/// clear tensors in current MPS; keep size and bond dim settings
	gamma.clear();
	lambda_x.clear();
	lambda_y.clear();
}

//======================================

void CanonPEPS::vecSlice( std::vector<uni10::CUniTensor>& vec, int length, int start ) {
	/// slice vec into desired chunk
	// todo: assert start >= 0
	if (start == 0) {
		vec.resize(length);
	}
	else {
		vec.erase( vec.begin(), vec.begin()+start );
		vec.erase( vec.begin()+length, vec.end() );
	}
}

//======================================

void CanonPEPS::vecSlice( std::vector< std::vector<uni10::CUniTensor> >& vec, int length, int start ) {
	/// slice vec into desired chunk
	// todo: assert start >= 0
	if (start == 0) {
		vec.resize(length);
	}
	else {
		vec.erase( vec.begin(), vec.begin()+start );
		vec.erase( vec.begin()+length, vec.end() );
	}
}

//======================================

void CanonPEPS::slice(int length, int width, int y_idx, int x_idx) {
	/// slice MPS into desired chunk
	// todo: assert starting x and y index >= 0
	vecSlice( gamma, width, y_idx );
	vecSlice( lambda_x, width, y_idx );
	vecSlice( lambda_y, width, y_idx );

	for (int i = 0; i < gamma.size(); ++i)
		vecSlice( gamma[i], length, x_idx );
	for (int i = 0; i < lambda_x.size(); ++i)
		vecSlice( lambda_x[i], length, x_idx );
	for (int i = 0; i < lambda_y.size(); ++i)
		vecSlice( lambda_y[i], length, x_idx );
}

//======================================

uni10::CUniTensor CanonPEPS::getGamma( int y_idx, int x_idx ) {
	/// return the idx'th gamma
	return gamma[y_idx][x_idx];
	// todo: raise exception if idx out of range
}

uni10::CUniTensor CanonPEPS::getLambdaX( int y_idx, int x_idx ) {
	/// return the idx'th lambda_x
	return lambda_x[y_idx][x_idx];
	// todo: raise exception if idx out of range
}

uni10::CUniTensor CanonPEPS::getLambdaY( int y_idx, int x_idx ) {
	/// return the idx'th lambda_x
	return lambda_y[y_idx][x_idx];
	// todo: raise exception if idx out of range
}

//======================================

uni10::CUniTensor CanonPEPS::initGamma( int x_chi1, int x_chi2, int y_chi1, int y_chi2, int d ) {
	/// initialize a gamma tensor
	std::vector<uni10::Bond> bond_gam;
	bond_gam.push_back( uni10::Bond(uni10::BD_IN, x_chi1) );
	bond_gam.push_back( uni10::Bond(uni10::BD_IN, y_chi1) );
	bond_gam.push_back( uni10::Bond(uni10::BD_IN, d) );
	bond_gam.push_back( uni10::Bond(uni10::BD_OUT, x_chi2) );
	bond_gam.push_back( uni10::Bond(uni10::BD_OUT, y_chi2) );

	uni10::CUniTensor gam(bond_gam);

	return gam;
}

//======================================

uni10::CUniTensor CanonPEPS::initLambda(int chi) {
	/// initialize a lambda tensor
	std::vector<uni10::Bond> bond_lam;
	bond_lam.push_back( uni10::Bond(uni10::BD_IN, chi) );
	bond_lam.push_back( uni10::Bond(uni10::BD_OUT, chi) );

	uni10::CUniTensor lam(bond_lam);

	return lam;
}

//======================================

void CanonPEPS::putGamma( uni10::CUniTensor gam, int y_idx, int x_idx ) {
	/// put a gamma tensor at gamma[y_idx][x_idx]
	// todo: assert there is already a tensor at the spot; 
	gamma[y_idx][x_idx] = gam;
}

void CanonPEPS::putLambdaX( uni10::CUniTensor lam, int y_idx, int x_idx ) {
	/// put a lambda tensor at lambda[y_idx][x_idx]
	// todo: assert there is already a tensor at the spot; 
	lambda_x[y_idx][x_idx] = lam;
}

void CanonPEPS::putLambdaY( uni10::CUniTensor lam, int y_idx, int x_idx ) {
	/// put a lambda tensor at lambda[y_idx][x_idx]
	// todo: assert there is already a tensor at the spot; 
	lambda_y[y_idx][x_idx] = lam;
}

//======================================

void CanonPEPS::oneSiteOP( uni10::CUniTensor op, int y_idx, int x_idx ) {
	/// Act one-site operator on a specific site

	// todo: assert op is one-site operator
	int lab_gam[] = {0, 1, 100, 2, 3};
	int lab_op[]  = {200, 100};
	int lab_fin[] = {0, 1, 200, 2, 3};

	gamma[y_idx][x_idx].setLabel( lab_gam );
	op.setLabel( lab_op );
	gamma[y_idx][x_idx] = uni10::contract( op, gamma[y_idx][x_idx] );
	gamma[y_idx][x_idx].permute( lab_fin, 3 );
}

//======================================

bool is_file_exist( std::string fileName ) {

    std::ifstream infile(fileName);
    return infile.good();
}

//======================================

std::vector< monitor > CanonPEPS::availMonitors( 
				std::vector< monitor > monitors, std::vector< std::string > avil_list ) {
	/// Check availability of monitor list
	std::vector< monitor > my_list;

	for (int i = 0; i < monitors.size(); ++i) {
		if (std::find( avil_list.begin(), avil_list.end(), monitors[i].name) != avil_list.end() )
			my_list.push_back( monitors[i] );
		else if ( is_file_exist( monitors[i].name ) )
			my_list.push_back( monitors[i] );
	}

	return my_list;
}
