#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>	// std::pow
#include <sys/stat.h>

#include <tns-func/tns_const.h>
#include <tns-func/func_convert.h>
#include <tns-func/func_op.h>

using namespace std;

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H itf <args>  construct Hamiltonian of spin 1/2 Ising model with args = Jz, hx" << std::endl;
	std::cerr << "-H itf-alt <args>  construct Hamiltonian of spin 1/2 Ising model with args = Jx, hz" << std::endl;
	std::cerr << "-H itf-alt-exp <args>  construct Hamiltonian of spin 1/2 Ising model with args = Jx, hz" << std::endl;
	std::cerr << "-H itf1-alt <args>  construct Hamiltonian of spin 1 Ising model with args = Jx, hz" << std::endl;
	std::cerr << "-H xxz <args>  construct Hamiltonian of spin 1/2 xxz model with args = J, Jz" << std::endl;
	std::cerr << "-H xxz-alt <args>  construct Hamiltonian of spin 1/2 xxz model with args = J, Jz" << std::endl;
	std::cerr << "-H xxz-lr <args>  construct Hamiltonian of spin 1/2 xxz model with args = J, Jz, long_range_param_file" << std::endl;
	std::cerr << "-H xxz1 <args>  construct Hamiltonian of spin 1 xxz model with args = J, Jz" << std::endl;
	std::cerr << "-l <arg>  Length of the lattice (for pbc xxz-lr model)" << std::endl;
	std::cerr << "-pbc  mimic Periodic Boundary Condition (WARNING: in fact it is OBC with first-last interact.)" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	int pbc = 0;
	int lat_size = -1;

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-pbc")
			pbc = 1;
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> lat_size;
			}
			else { // Uh-oh, there was no argument to the -m option.
				std::cerr << "-l option requires a positive integer lattice length." << std::endl;
				return 1;
			}
		}
	}

	// construct operators
	if (std::string(argv[1]) == "-H") {

		if (std::string(argv[2]) == "itf") {

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itf [J] [h]" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}

			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor id = OP( "id" );

			MPO mpo_l(3+pbc, 'l');
			MPO mpo_m(3+pbc, 'm');
			MPO mpo_r(3+pbc, 'r');

			if (pbc) {
				mpo_l.putTensor( h*sx, 0, 0 );
				mpo_l.putTensor( J*sz, 0, 1 );
				mpo_l.putTensor( J*sz, 0, 2 );
				mpo_l.putTensor(   id, 0, 3 );

				mpo_m.putTensor(   id, 0, 0 );
				mpo_m.putTensor(   sz, 1, 0 );
				mpo_m.putTensor(   id, 2, 2 );
				mpo_m.putTensor( h*sx, 3, 0 );
				mpo_m.putTensor( J*sz, 3, 1 );
				mpo_m.putTensor(   id, 3, 3 );

				mpo_r.putTensor(   id, 0, 0 );
				mpo_r.putTensor(   sz, 1, 0 );
				mpo_r.putTensor(   sz, 2, 0 );
				mpo_r.putTensor( h*sx, 3, 0 );
			}
			else {
				mpo_l.putTensor( h*sx, 0, 0 );
				mpo_l.putTensor( J*sz, 0, 1 );
				mpo_l.putTensor(   id, 0, 2 );

				mpo_m.putTensor(   id, 0, 0 );
				mpo_m.putTensor(   sz, 1, 0 );
				mpo_m.putTensor( h*sx, 2, 0 );
				mpo_m.putTensor( J*sz, 2, 1 );
				mpo_m.putTensor(   id, 2, 2 );

				mpo_r.putTensor(   id, 0, 0 );
				mpo_r.putTensor(   sz, 1, 0 );
				mpo_r.putTensor( h*sx, 2, 0 );
			}

			struct stat info;
			std::string dirname = "mpo-ham-itf";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );
		}

		else if (std::string(argv[2]) == "itf-alt") {

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itf-alt [J] [h]" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}

			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor id = OP( "id" );

			MPO mpo_l(3+pbc, 'l');
			MPO mpo_m(3+pbc, 'm');
			MPO mpo_r(3+pbc, 'r');

			if (pbc) {
				mpo_l.putTensor( h*sz, 0, 0 );
				mpo_l.putTensor( J*sx, 0, 1 );
				mpo_l.putTensor( J*sx, 0, 2 );
				mpo_l.putTensor(   id, 0, 3 );

				mpo_m.putTensor(   id, 0, 0 );
				mpo_m.putTensor(   sx, 1, 0 );
				mpo_m.putTensor(   id, 2, 2 );
				mpo_m.putTensor( h*sz, 3, 0 );
				mpo_m.putTensor( J*sx, 3, 1 );
				mpo_m.putTensor(   id, 3, 3 );

				mpo_r.putTensor(   id, 0, 0 );
				mpo_r.putTensor(   sx, 1, 0 );
				mpo_r.putTensor(   sx, 2, 0 );
				mpo_r.putTensor( h*sz, 3, 0 );
			}
			else {
				mpo_l.putTensor( h*sz, 0, 0 );
				mpo_l.putTensor( J*sx, 0, 1 );
				mpo_l.putTensor(   id, 0, 2 );

				mpo_m.putTensor(   id, 0, 0 );
				mpo_m.putTensor(   sx, 1, 0 );
				mpo_m.putTensor( h*sz, 2, 0 );
				mpo_m.putTensor( J*sx, 2, 1 );
				mpo_m.putTensor(   id, 2, 2 );

				mpo_r.putTensor(   id, 0, 0 );
				mpo_r.putTensor(   sx, 1, 0 );
				mpo_r.putTensor( h*sz, 2, 0 );
			}

			struct stat info;
			std::string dirname = "mpo-ham-itf";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );
		}

		else if (std::string(argv[2]) == "itf-alt-exp") {

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itf-alt-exp [J] [h]" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}

			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor id = OP( "id" );

			MPO mpo_l(5, 'l');
			MPO mpo_m(5, 'm');
			MPO mpo_r(5, 'r');

			mpo_l.putTensor( h*sz, 0, 0 );
			mpo_l.putTensor( J*sx, 0, 1 );
			mpo_l.putTensor(   id, 0, 4 );

			mpo_m.putTensor(   id, 0, 0 );
			mpo_m.putTensor(   sx, 1, 0 );
			mpo_m.putTensor( h*sz, 4, 0 );
			mpo_m.putTensor( J*sx, 4, 1 );
			mpo_m.putTensor(   id, 4, 4 );

			mpo_r.putTensor(   id, 0, 0 );
			mpo_r.putTensor(   sx, 1, 0 );
			mpo_r.putTensor( h*sz, 4, 0 );

			struct stat info;
			std::string dirname = "mpo-ham-itf";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );
		}

		else if (std::string(argv[2]) == "itf1-alt") {

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itf1-alt [J] [h]" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}

			uni10::CUniTensor sz = OP( "sz1" );
			uni10::CUniTensor sx = OP( "sx1" );
			uni10::CUniTensor id = OP( "id1" );

			MPO mpo_l(3+pbc, 'l');
			MPO mpo_m(3+pbc, 'm');
			MPO mpo_r(3+pbc, 'r');

			if (pbc) {
				mpo_l.putTensor( h*sz, 0, 0 );
				mpo_l.putTensor( J*sx, 0, 1 );
				mpo_l.putTensor( J*sx, 0, 2 );
				mpo_l.putTensor(   id, 0, 3 );

				mpo_m.putTensor(   id, 0, 0 );
				mpo_m.putTensor(   sx, 1, 0 );
				mpo_m.putTensor(   id, 2, 2 );
				mpo_m.putTensor( h*sz, 3, 0 );
				mpo_m.putTensor( J*sx, 3, 1 );
				mpo_m.putTensor(   id, 3, 3 );

				mpo_r.putTensor(   id, 0, 0 );
				mpo_r.putTensor(   sx, 1, 0 );
				mpo_r.putTensor(   sx, 2, 0 );
				mpo_r.putTensor( h*sz, 3, 0 );
			}
			else {
				mpo_l.putTensor( h*sz, 0, 0 );
				mpo_l.putTensor( J*sx, 0, 1 );
				mpo_l.putTensor(   id, 0, 2 );

				mpo_m.putTensor(   id, 0, 0 );
				mpo_m.putTensor(   sx, 1, 0 );
				mpo_m.putTensor( h*sz, 2, 0 );
				mpo_m.putTensor( J*sx, 2, 1 );
				mpo_m.putTensor(   id, 2, 2 );

				mpo_r.putTensor(   id, 0, 0 );
				mpo_r.putTensor(   sx, 1, 0 );
				mpo_r.putTensor( h*sz, 2, 0 );
			}

			struct stat info;
			std::string dirname = "mpo-ham-itf";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );
		}

		else if (std::string(argv[2]) == "xxz") {

			double J = 1.0;
			double Jz = 1.0;
			double hz = 0.0;	// perturbation to break symm

			if (argc > 4) {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> Jz;
			}
			if (argc > 5) {
				std::stringstream(argv[5]) >> hz;
			}

			Jz *= 0.25;
			hz *= 0.5;

			uni10::CUniTensor sp = OP( "sp" );
			uni10::CUniTensor sm = OP( "sm" );
			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor id = OP( "id" );

			MPO mpo_l(5+pbc*3, 'l');
			MPO mpo_m(5+pbc*3, 'm');
			MPO mpo_r(5+pbc*3, 'r');

			if (pbc) {
				mpo_l.putTensor(    hz*sz, 0, 0 );
				mpo_l.putTensor( 0.5*J*sm, 0, 1 );
				mpo_l.putTensor( 0.5*J*sp, 0, 2 );
				mpo_l.putTensor(    Jz*sz, 0, 3 );
				mpo_l.putTensor( 0.5*J*sm, 0, 4 );
				mpo_l.putTensor( 0.5*J*sp, 0, 5 );
				mpo_l.putTensor(    Jz*sz, 0, 6 );
				mpo_l.putTensor(       id, 0, 7 );

				mpo_m.putTensor(       id, 0, 0 );
				mpo_m.putTensor(       sp, 1, 0 );
				mpo_m.putTensor(       sm, 2, 0 );
				mpo_m.putTensor(       sz, 3, 0 );
				mpo_m.putTensor(       id, 4, 4 );
				mpo_m.putTensor(       id, 5, 5 );
				mpo_m.putTensor(       id, 6, 6 );
				mpo_m.putTensor(    hz*sz, 7, 0 );
				mpo_m.putTensor( 0.5*J*sm, 7, 1 );
				mpo_m.putTensor( 0.5*J*sp, 7, 2 );
				mpo_m.putTensor(    Jz*sz, 7, 3 );
				mpo_m.putTensor(       id, 7, 7 );

				mpo_r.putTensor(       id, 0, 0 );
				mpo_r.putTensor(       sp, 1, 0 );
				mpo_r.putTensor(       sm, 2, 0 );
				mpo_r.putTensor(       sz, 3, 0 );
				mpo_r.putTensor(       sp, 4, 0 );
				mpo_r.putTensor(       sm, 5, 0 );
				mpo_r.putTensor(       sz, 6, 0 );
				mpo_r.putTensor(    hz*sz, 7, 0 );
			}
			else {
				mpo_l.putTensor(    hz*sz, 0, 0 );
				mpo_l.putTensor( 0.5*J*sm, 0, 1 );
				mpo_l.putTensor( 0.5*J*sp, 0, 2 );
				mpo_l.putTensor(    Jz*sz, 0, 3 );
				mpo_l.putTensor(       id, 0, 4 );

				mpo_m.putTensor(       id, 0, 0 );
				mpo_m.putTensor(       sp, 1, 0 );
				mpo_m.putTensor(       sm, 2, 0 );
				mpo_m.putTensor(       sz, 3, 0 );
				mpo_m.putTensor(    hz*sz, 4, 0 );
				mpo_m.putTensor( 0.5*J*sm, 4, 1 );
				mpo_m.putTensor( 0.5*J*sp, 4, 2 );
				mpo_m.putTensor(    Jz*sz, 4, 3 );
				mpo_m.putTensor(       id, 4, 4 );

				mpo_r.putTensor(       id, 0, 0 );
				mpo_r.putTensor(       sp, 1, 0 );
				mpo_r.putTensor(       sm, 2, 0 );
				mpo_r.putTensor(       sz, 3, 0 );
				mpo_r.putTensor(    hz*sz, 4, 0 );
			}

			struct stat info;
			std::string dirname = "mpo-ham-xxz";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );
		}

		else if (std::string(argv[2]) == "xxz-alt") {

			double J = 1.0;
			double Jz = 1.0;
			double hz = 0.0;	// perturbation to break symm

			if (argc > 4) {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> Jz;
			}
			if (argc > 5) {
				std::stringstream(argv[5]) >> hz;
			}

			// // comment out to match itfx-alt
			// J *= 0.25;
			// Jz *= 0.25;
			// hz *= 0.5;

			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor sy = OP( "sy" );
			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor id = OP( "id" );

			MPO mpo_l(5+pbc*3, 'l');
			MPO mpo_m(5+pbc*3, 'm');
			MPO mpo_r(5+pbc*3, 'r');

			if (pbc) {
				mpo_l.putTensor( hz*sz, 0, 0 );
				mpo_l.putTensor(  J*sx, 0, 1 );
				mpo_l.putTensor(  J*sy, 0, 2 );
				mpo_l.putTensor( Jz*sz, 0, 3 );
				mpo_l.putTensor(  J*sx, 0, 4 );
				mpo_l.putTensor(  J*sy, 0, 5 );
				mpo_l.putTensor( Jz*sz, 0, 6 );
				mpo_l.putTensor(    id, 0, 7 );

				mpo_m.putTensor(    id, 0, 0 );
				mpo_m.putTensor(    sx, 1, 0 );
				mpo_m.putTensor(    sy, 2, 0 );
				mpo_m.putTensor(    sz, 3, 0 );
				mpo_m.putTensor(    id, 4, 4 );
				mpo_m.putTensor(    id, 5, 5 );
				mpo_m.putTensor(    id, 6, 6 );
				mpo_m.putTensor( hz*sz, 7, 0 );
				mpo_m.putTensor(  J*sx, 7, 1 );
				mpo_m.putTensor(  J*sy, 7, 2 );
				mpo_m.putTensor( Jz*sz, 7, 3 );
				mpo_m.putTensor(    id, 7, 7 );

				mpo_r.putTensor(    id, 0, 0 );
				mpo_r.putTensor(    sx, 1, 0 );
				mpo_r.putTensor(    sy, 2, 0 );
				mpo_r.putTensor(    sz, 3, 0 );
				mpo_r.putTensor(    sx, 4, 0 );
				mpo_r.putTensor(    sy, 5, 0 );
				mpo_r.putTensor(    sz, 6, 0 );
				mpo_r.putTensor( hz*sz, 7, 0 );
			}
			else {
				mpo_l.putTensor( hz*sz, 0, 0 );
				mpo_l.putTensor(  J*sx, 0, 1 );
				mpo_l.putTensor(  J*sy, 0, 2 );
				mpo_l.putTensor( Jz*sz, 0, 3 );
				mpo_l.putTensor(    id, 0, 4 );

				mpo_m.putTensor(    id, 0, 0 );
				mpo_m.putTensor(    sx, 1, 0 );
				mpo_m.putTensor(    sy, 2, 0 );
				mpo_m.putTensor(    sz, 3, 0 );
				mpo_m.putTensor( hz*sz, 4, 0 );
				mpo_m.putTensor(  J*sx, 4, 1 );
				mpo_m.putTensor(  J*sy, 4, 2 );
				mpo_m.putTensor( Jz*sz, 4, 3 );
				mpo_m.putTensor(    id, 4, 4 );

				mpo_r.putTensor(    id, 0, 0 );
				mpo_r.putTensor(    sx, 1, 0 );
				mpo_r.putTensor(    sy, 2, 0 );
				mpo_r.putTensor(    sz, 3, 0 );
				mpo_r.putTensor( hz*sz, 4, 0 );
			}

			struct stat info;
			std::string dirname = "mpo-ham-xxz";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );
		}

		else if (std::string(argv[2]) == "xxz1") {

			double J = 1.0;
			double Jz = 1.0;
			double hz = 0.0;	// perturbation to break symm

			if (argc > 4) {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> Jz;
			}
			if (argc > 5) {
				std::stringstream(argv[5]) >> hz;
			}

			uni10::CUniTensor sx = OP( "sx1" );
			uni10::CUniTensor sy = OP( "sy1" );
			uni10::CUniTensor sz = OP( "sz1" );
			uni10::CUniTensor id = OP( "id1" );

			MPO mpo_l(5+pbc*3, 'l');
			MPO mpo_m(5+pbc*3, 'm');
			MPO mpo_r(5+pbc*3, 'r');

			if (pbc) {
				mpo_l.putTensor( hz*sz, 0, 0 );
				mpo_l.putTensor(  J*sx, 0, 1 );
				mpo_l.putTensor(  J*sy, 0, 2 );
				mpo_l.putTensor( Jz*sz, 0, 3 );
				mpo_l.putTensor(  J*sx, 0, 4 );
				mpo_l.putTensor(  J*sy, 0, 5 );
				mpo_l.putTensor( Jz*sz, 0, 6 );
				mpo_l.putTensor(    id, 0, 7 );

				mpo_m.putTensor(    id, 0, 0 );
				mpo_m.putTensor(    sx, 1, 0 );
				mpo_m.putTensor(    sy, 2, 0 );
				mpo_m.putTensor(    sz, 3, 0 );
				mpo_m.putTensor(    id, 4, 4 );
				mpo_m.putTensor(    id, 5, 5 );
				mpo_m.putTensor(    id, 6, 6 );
				mpo_m.putTensor( hz*sz, 7, 0 );
				mpo_m.putTensor(  J*sx, 7, 1 );
				mpo_m.putTensor(  J*sy, 7, 2 );
				mpo_m.putTensor( Jz*sz, 7, 3 );
				mpo_m.putTensor(    id, 7, 7 );

				mpo_r.putTensor(    id, 0, 0 );
				mpo_r.putTensor(    sx, 1, 0 );
				mpo_r.putTensor(    sy, 2, 0 );
				mpo_r.putTensor(    sz, 3, 0 );
				mpo_r.putTensor(    sx, 4, 0 );
				mpo_r.putTensor(    sy, 5, 0 );
				mpo_r.putTensor(    sz, 6, 0 );
				mpo_r.putTensor( hz*sz, 7, 0 );
			}
			else {
				mpo_l.putTensor( hz*sz, 0, 0 );
				mpo_l.putTensor(  J*sx, 0, 1 );
				mpo_l.putTensor(  J*sy, 0, 2 );
				mpo_l.putTensor( Jz*sz, 0, 3 );
				mpo_l.putTensor(    id, 0, 4 );

				mpo_m.putTensor(    id, 0, 0 );
				mpo_m.putTensor(    sx, 1, 0 );
				mpo_m.putTensor(    sy, 2, 0 );
				mpo_m.putTensor(    sz, 3, 0 );
				mpo_m.putTensor( hz*sz, 4, 0 );
				mpo_m.putTensor(  J*sx, 4, 1 );
				mpo_m.putTensor(  J*sy, 4, 2 );
				mpo_m.putTensor( Jz*sz, 4, 3 );
				mpo_m.putTensor(    id, 4, 4 );

				mpo_r.putTensor(    id, 0, 0 );
				mpo_r.putTensor(    sx, 1, 0 );
				mpo_r.putTensor(    sy, 2, 0 );
				mpo_r.putTensor(    sz, 3, 0 );
				mpo_r.putTensor( hz*sz, 4, 0 );
			}

			struct stat info;
			std::string dirname = "mpo-ham-xxz";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );
		}

		else if (std::string(argv[2]) == "xxz-lr") {

			double J = 1.0;
			double Jz = 1.0;
			double hz = 0.0;	// perturbation to break symm
			std::vector<double> lam;
			std::vector<double> w;

			if (argc < 6) {
				errMsg( argv[0] );
				return 1;
			}

			std::stringstream(argv[3]) >> J;
			std::stringstream(argv[4]) >> Jz;
			std::ifstream params(argv[5]);
			if ( !params ) {
				errMsg( argv[0] );
				return 1;
			}

			int nu = 0;
			double value;
			std::string line;
			while( !params.eof() ) {
				std::getline(params, line);
				std::stringstream vin(line);
				if (nu == 0)
					while ( vin >> value )
						lam.push_back(value);
				else if (nu == 1)
					while ( vin >> value )
						w.push_back(-2.*value);
				nu += 1;
			}

			int len_params = lam.size();

			double sum_w = 0.0;
			for (int i = 0; i < len_params; ++i)
				sum_w += w[i];

			Jz *= 0.25;
			J = J - sum_w;

			uni10::CUniTensor sp = OP( "sp" );
			uni10::CUniTensor sm = OP( "sm" );
			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor id = OP( "id" );

			int dim_w = 5 + 3*pbc + (2 + 2*pbc) * len_params;
			MPO mpo_l(dim_w, 'l');
			MPO mpo_m(dim_w, 'm');
			MPO mpo_r(dim_w, 'r');

			if (pbc) {
				std::vector<double> lam_pow;
				if (lat_size < 2) {
					errMsg( argv[0] );
					return 1;
				}
				for (int i = 0; i < len_params; ++i)
					lam_pow.push_back(std::pow(lam[i], lat_size-2));

				mpo_l.putTensor( hz*sz, 0, 0 );
				mpo_l.putTensor( 0.5*J*sm, 0, 1 );
				mpo_l.putTensor( 0.5*J*sp, 0, 2 );
				mpo_l.putTensor( Jz*sz, 0, 3 );
				mpo_l.putTensor( 0.5*J*sm, 0, 4 );
				mpo_l.putTensor( 0.5*J*sp, 0, 5 );
				mpo_l.putTensor( Jz*sz, 0, 6 );
				for (int i = 0; i < len_params; ++i) {
					mpo_l.putTensor( 0.5*w[i]*sm, 0, 7+i*4 );
					mpo_l.putTensor( 0.5*w[i]*sp, 0, 8+i*4 );
					mpo_l.putTensor( 0.5*lam_pow[i]*w[i]*sm, 0, 9+i*4 );
					mpo_l.putTensor( 0.5*lam_pow[i]*w[i]*sp, 0, 10+i*4 );
				}
				mpo_l.putTensor( id, 0, dim_w-1 );

				mpo_m.putTensor( id, 0, 0 );
				mpo_m.putTensor( sp, 1, 0 );
				mpo_m.putTensor( sm, 2, 0 );
				mpo_m.putTensor( sz, 3, 0 );
				for (int i = 0; i < len_params; ++i) {
					mpo_m.putTensor( sp, 7+i*4, 0 );
					mpo_m.putTensor( sm, 8+i*4, 0 );
					mpo_m.putTensor( sp, 9+i*4, 0 );
					mpo_m.putTensor( sm, 10+i*4, 0 );
				}
				mpo_m.putTensor( hz*sz, dim_w-1, 0 );
				mpo_m.putTensor( 0.5*J*sm, dim_w-1, 1 );
				mpo_m.putTensor( 0.5*J*sp, dim_w-1, 2 );
				mpo_m.putTensor( Jz*sz, dim_w-1, 3 );
				for (int i = 0; i < len_params; ++i) {
					mpo_m.putTensor( 0.5*w[i]*sm, dim_w-1, 7+i*4 );
					mpo_m.putTensor( 0.5*w[i]*sp, dim_w-1, 8+i*4 );
					mpo_m.putTensor( 0.5*lam_pow[i]*w[i]*sm, dim_w-1, 9+i*4 );
					mpo_m.putTensor( 0.5*lam_pow[i]*w[i]*sp, dim_w-1, 10+i*4 );
				}
				mpo_m.putTensor( id, dim_w-1, dim_w-1 );

				mpo_m.putTensor( id, 4, 4 );
				mpo_m.putTensor( id, 5, 5 );
				mpo_m.putTensor( id, 6, 6 );
				for (int i = 0; i < len_params; ++i) {
					mpo_m.putTensor( lam[i]*id, 7+i*4, 7+i*4 );
					mpo_m.putTensor( lam[i]*id, 8+i*4, 8+i*4 );
					mpo_m.putTensor( (1./lam[i])*id, 9+i*4, 9+i*4 );
					mpo_m.putTensor( (1./lam[i])*id, 10+i*4, 10+i*4 );
				}

				mpo_r.putTensor( id, 0, 0 );
				mpo_r.putTensor( sp, 1, 0 );
				mpo_r.putTensor( sm, 2, 0 );
				mpo_r.putTensor( sz, 3, 0 );
				mpo_r.putTensor( sp, 4, 0 );
				mpo_r.putTensor( sm, 5, 0 );
				mpo_r.putTensor( sz, 6, 0 );
				for (int i = 0; i < len_params; ++i) {
					mpo_r.putTensor( sp, 7+i*4, 0 );
					mpo_r.putTensor( sm, 8+i*4, 0 );
					mpo_r.putTensor( sp, 9+i*4, 0 );
					mpo_r.putTensor( sm, 10+i*4, 0 );
				}
				mpo_r.putTensor( hz*sz, dim_w-1, 0 );
			}

			else {
				mpo_l.putTensor( hz*sz, 0, 0 );
				mpo_l.putTensor( 0.5*J*sm, 0, 1 );
				mpo_l.putTensor( 0.5*J*sp, 0, 2 );
				mpo_l.putTensor( Jz*sz, 0, 3 );
				for (int i = 0; i < len_params; ++i) {
					mpo_l.putTensor( 0.5*w[i]*sm, 0, 4+i*2 );
					mpo_l.putTensor( 0.5*w[i]*sp, 0, 5+i*2 );
				}
				mpo_l.putTensor( id, 0, dim_w-1 );

				mpo_m.putTensor( id, 0, 0 );
				mpo_m.putTensor( sp, 1, 0 );
				mpo_m.putTensor( sm, 2, 0 );
				mpo_m.putTensor( sz, 3, 0 );
				for (int i = 0; i < len_params; ++i) {
					mpo_m.putTensor( sp, 4+i*2, 0 );
					mpo_m.putTensor( sm, 5+i*2, 0 );
				}
				mpo_m.putTensor( hz*sz, dim_w-1, 0 );
				mpo_m.putTensor( 0.5*J*sm, dim_w-1, 1 );
				mpo_m.putTensor( 0.5*J*sp, dim_w-1, 2 );
				mpo_m.putTensor( Jz*sz, dim_w-1, 3 );
				for (int i = 0; i < len_params; ++i) {
					mpo_m.putTensor( 0.5*w[i]*sm, dim_w-1, 4+i*2 );
					mpo_m.putTensor( 0.5*w[i]*sp, dim_w-1, 5+i*2 );
				}
				mpo_m.putTensor( id, dim_w-1, dim_w-1 );

				for (int i = 0; i < len_params; ++i) {
					mpo_m.putTensor( lam[i]*id, 4+i*2, 4+i*2 );
					mpo_m.putTensor( lam[i]*id, 5+i*2, 5+i*2 );
				}

				mpo_r.putTensor( id, 0, 0 );
				mpo_r.putTensor( sp, 1, 0 );
				mpo_r.putTensor( sm, 2, 0 );
				mpo_r.putTensor( sz, 3, 0 );
				for (int i = 0; i < len_params; ++i) {
					mpo_r.putTensor( sp, 4+i*2, 0 );
					mpo_r.putTensor( sm, 5+i*2, 0 );
				}
				mpo_r.putTensor( hz*sz, dim_w-1, 0 );
			}

			struct stat info;
			std::string dirname = "mpo-ham-xxz";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );
		}

		else { // Uh-oh, there was no argument to the -H option.
			std::cerr << "-H model Hamiltonian. Valid choices: itf, xxz, xxz-lr" << std::endl;
			return 1;
		}
	}

	else {

		errMsg( argv[0] );
		return 1;
	}

	return 0;
}
