#include <iostream>
#include <sstream>
#include <sys/stat.h>

#include <tns-func/func_op.h>
#include <mps-qn/func_qn_op.h>

using namespace std;

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H hub <t> <V> (-w <hb_dir>) (-imp <t_imp> <V_imp>) (-df <dest_folder>)" << std::endl;
	std::cerr << "-occu <args>  set OCCUpation params num_particles, unit_cell_size" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 3) {

		errMsg( argv[0] );
		return 1;
	}

	int occu = 1;
	int unit = 2;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-occu") {
			if (i + 2 < argc) {
				std::istringstream(argv[i+1]) >> occu;
				std::istringstream(argv[i+2]) >> unit;
			}
			else {
				std::cerr << "-occu option requires arguments (int) number of particles, (int) unit-cell size." << std::endl;
				return 1;
			}
		}
	}

	// construct operators
	if (std::string(argv[1]) == "-H") {

		if (std::string(argv[2]) == "hub") {

			double t = 1.0;
			double V = 1.0;
			double t_imp = 1.0;
			double V_imp = 1.0;
			bool imp = false;
			std::string hb_dir = ".";
			std::string dirname = "mpo-ham-hub";

			if (argc > 4) {
				std::stringstream(argv[3]) >> t;
				std::stringstream(argv[4]) >> V;
			}

			if (argc > 5) {
				for (int i = 5; i < argc; ++i) {

					if (std::string(argv[i]) == "-w") {
						if (i + 1 < argc) {
							hb_dir = std::string(argv[i+1]);
						}
						else {
							std::cerr << "Usage: -w <hb_dir>" << std::endl;
							return 1;
						}
					}
					else if (std::string(argv[i]) == "-imp") {
						if (i + 2 < argc) {
							std::stringstream(argv[i+1]) >> t_imp;
							std::stringstream(argv[i+2]) >> V_imp;
							imp = true;
						}
						else {
							std::cerr << "Usage: -imp <t_imp> <V_imp>" << std::endl;
							return 1;
						}
					}
					else if (std::string(argv[i]) == "-df") {
						if (i + 1 < argc) {
							dirname = std::string(argv[i+1]);
						}
						else {
							std::cerr << "Usage: -df <dest_folder>" << std::endl;
							return 1;
						}
					}
				}
			}

			uni10::CUniTensor id = opFU1_grd0( "id", occu, unit );
			uni10::CUniTensor cp = opFU1_grd0( "cp", occu, unit );
			uni10::CUniTensor cm = opFU1_grd0( "cm", occu, unit );
			uni10::CUniTensor Ni = opFU1_grd0(  "N", occu, unit ) + (-0.5) * id;

			uni10::CUniTensor hl   = uni10::CUniTensor( hb_dir + "/HL" );
			uni10::CUniTensor cp_l = uni10::CUniTensor( hb_dir + "/Cp_L" );
			uni10::CUniTensor cm_l = uni10::CUniTensor( hb_dir + "/Cm_L" );
			uni10::CUniTensor Ni_l = uni10::CUniTensor( hb_dir + "/Ni_L" );
			uni10::CUniTensor id_l = uni10::CUniTensor( hb_dir + "/Id_L" );

			uni10::CUniTensor hr   = uni10::CUniTensor( hb_dir + "/HR" );
			uni10::CUniTensor cp_r = uni10::CUniTensor( hb_dir + "/Cp_R" );
			uni10::CUniTensor cm_r = uni10::CUniTensor( hb_dir + "/Cm_R" );
			uni10::CUniTensor Ni_r = uni10::CUniTensor( hb_dir + "/Ni_R" );
			uni10::CUniTensor id_r = uni10::CUniTensor( hb_dir + "/Id_R" );

			// symmetric blocks
			MPO mpo_ls(5, 'l');
			MPO mpo_ms(5, 'm');
			MPO mpo_rs(5, 'r');

			// raising blocks
			MPO mpo_lp(5, 'l');
			MPO mpo_mp(5, 'm');
			MPO mpo_rp(5, 'r');

			// lowering blocks
			MPO mpo_lm(5, 'l');
			MPO mpo_mm(5, 'm');
			MPO mpo_rm(5, 'r');

			mpo_ls.putTensor(          hl, 0, 0 );
			mpo_lm.putTensor(  1.0*t*cm_l, 0, 1 );
			mpo_lp.putTensor( -1.0*t*cp_l, 0, 2 );
			mpo_ls.putTensor(      V*Ni_l, 0, 3 );
			mpo_ls.putTensor(        id_l, 0, 4 );

			mpo_ms.putTensor(          id, 0, 0 );
			mpo_mp.putTensor(          cp, 1, 0 );
			mpo_mm.putTensor(          cm, 2, 0 );
			mpo_ms.putTensor(          Ni, 3, 0 );
			mpo_mm.putTensor(    1.0*t*cm, 4, 1 );
			mpo_mp.putTensor(   -1.0*t*cp, 4, 2 );
			mpo_ms.putTensor(        V*Ni, 4, 3 );
			mpo_ms.putTensor(          id, 4, 4 );

			mpo_rs.putTensor(        id_r, 0, 0 );
			mpo_rp.putTensor(        cp_r, 1, 0 );
			mpo_rm.putTensor(        cm_r, 2, 0 );
			mpo_rs.putTensor(        Ni_r, 3, 0 );
			mpo_rs.putTensor(          hr, 4, 0 );

			struct stat info;
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_ls.launch().save( dirname + "/mpo_ls" );
			mpo_ms.launch().save( dirname + "/mpo_ms" );
			mpo_rs.launch().save( dirname + "/mpo_rs" );
			mpo_lp.launch().save( dirname + "/mpo_lp" );
			mpo_mp.launch().save( dirname + "/mpo_mp" );
			mpo_rp.launch().save( dirname + "/mpo_rp" );
			mpo_lm.launch().save( dirname + "/mpo_lm" );
			mpo_mm.launch().save( dirname + "/mpo_mm" );
			mpo_rm.launch().save( dirname + "/mpo_rm" );

			if (imp) {
				// symmetric blocks
				MPO mpo_imp_s(5, 'm');
				// raising blocks
				MPO mpo_imp_p(5, 'm');
				// lowering blocks
				MPO mpo_imp_m(5, 'm');

				mpo_imp_s.putTensor(            id, 0, 0 );
				mpo_imp_p.putTensor(            cp, 1, 0 );
				mpo_imp_m.putTensor(            cm, 2, 0 );
				mpo_imp_s.putTensor(            Ni, 3, 0 );
				mpo_imp_m.putTensor(  1.0*t_imp*cm, 4, 1 );
				mpo_imp_p.putTensor( -1.0*t_imp*cp, 4, 2 );
				mpo_imp_s.putTensor(      V_imp*Ni, 4, 3 );
				mpo_imp_s.putTensor(            id, 4, 4 );

				mpo_imp_s.launch().save( dirname + "/mpo_imp_s" );
				mpo_imp_p.launch().save( dirname + "/mpo_imp_p" );
				mpo_imp_m.launch().save( dirname + "/mpo_imp_m" );
			}
		}

		else {
			errMsg( argv[0] );
			return 1;
		}
	}

	else {
		errMsg( argv[0] );
		return 1;
	}

	return 0;
}
