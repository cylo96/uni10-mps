/**************************************************************************

Effective Infinite Boundary Condition for Fermion Hubbard model

Ref: PHYSICAL REVIEW B 86, 245107 (2012)
Uni10 library: http://uni10.org/

@author: cylo
@affiliation: NTHU Physics

***************************************************************************/

#include <iostream>
#include <sstream>
#include <uni10.hpp>

#include <mps.hpp>

//============================================

uni10::CUniTensor netMPS( bool left, std::vector<uni10::CUniTensor>& T ) {
	///
	int len = T.size();
	uni10::CUniTensor net = T[0];
	int lab_nl[] = {0, 100, 1};
	int lab_nr[] = {len-1, 100+len-1, len};

	if (left) {
		net.setLabel(lab_nl);
		for (int i = 1; i < len; ++i) {
			int lab_t[] = {i, 100+i, i+1};
			T[i].setLabel(lab_t);
			net = uni10::contract(net, T[i], false);
		}
	}
	else {
		net.setLabel(lab_nr);
		for (int i = 1; i < len; ++i) {
			int lab_t[] = {len-1-i, 100+len-1-i, len-i};
			T[i].setLabel(lab_t);
			net = uni10::contract(T[i], net, false);
		}
	}

	net.permute(len+1);
	return net;
}

//============================================

uni10::CUniTensor netContract( bool left,
	std::vector<uni10::CUniTensor>& T, uni10::CUniTensor op, int loc ) {
	///
	int len = T.size();
	uni10::CUniTensor net;
	uni10::CUniTensor ket = netMPS(left, T);
	uni10::CUniTensor bra = dag(ket);

	std::vector<int> lab_ket = ket.label();
	std::vector<int> lab_bra = bra.label();
	lab_ket[0] = 0;
	lab_ket[len+1] = 1;
	lab_bra[0] = 1+(int)left;
	lab_bra[1] = 0+2*(int)(!left);
	ket.setLabel(lab_ket);

	if (op.bondNum() == 2) {
		int lab_op[] = { 200+loc, 100+loc };
		lab_bra[2+loc] = 200+loc;
		op.setLabel(lab_op);
		ket = uni10::contract(op, ket);
		bra.setLabel(lab_bra);
	}
	else if (op.bondNum() == 4) {
		int lab_op[] = { 200+loc, 201+loc, 100+loc, 101+loc };
		lab_bra[2+loc] = 200+loc;
		lab_bra[3+loc] = 201+loc;
		op.setLabel(lab_op);
		ket = uni10::contract(op, ket);
		bra.setLabel(lab_bra);
	}
	else if (op.bondNum() == 3) {
		int lab_op[] = { 200+loc, 100+loc, -1 };
		lab_bra[2+loc] = 200+loc;

		op.setLabel(lab_op);
		// apply swap gates to cp/cm operator on the right side
		if (!left) {
			std::vector<int> to_cross;
			to_cross.push_back(200+loc);
			to_cross.push_back(100+loc);
			applyCrossGate(op, -1, to_cross);
		}

		ket = uni10::contract(op, ket);
		std::vector<int> lab_ko;
		for (int i = 0; i < len+3; ++i) {
			int lab = ket.label()[i];
			if (lab != -1)
				lab_ko.push_back(lab);
		}
		lab_ko.push_back(-1);
		ket.permute(lab_ko, 1);
		bra.setLabel(lab_bra);
	}

	net = uni10::contract(bra, ket, false);
	return net;
}

//============================================

uni10::CUniTensor netContract( bool left,
	std::vector<uni10::CUniTensor>& T, uni10::CUniTensor op ) {
	///
	int loc = (left)? T.size()-op.inBondNum() : 0;
	return netContract(left, T, op, loc);
}

//============================================

uni10::CUniTensor otimesRenormPM( bool left,
	uni10::CUniTensor op1, uni10::CUniTensor op2 ) {
	/// left: renormalized operator at left (op1)?
	int lab_op1[] = {0, 2, -1};
	int lab_op2[] = {1, 3, -1};
	int lab_out[] = {0, 1, 2, 3};

	op1.setLabel(lab_op1);
	op2.setLabel(lab_op2);
	uni10::CUniTensor op;

	if (left) {
		std::vector<int> to_cross;
		to_cross.push_back(1);
		to_cross.push_back(3);
		applyCrossGate(op2, -1, to_cross);
	}

	op = uni10::contract(op1, op2, false);
	op.permute( lab_out, 2 );
	return op;
}

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H arg  load Hamiltonian from file arg" << std::endl;
	std::cerr << "-w arg  inf mps Wavefunction (gamma and lambda tensors) folder arg" << std::endl;
	std::cerr << "-n arg  Network file folder arg" << std::endl;
	std::cerr << "-df arg  Destination Folder arg" << std::endl;
	std::cerr << "-l arg  system Length (unit cell size)" << std::endl;
	std::cerr << "-t arg  set t = arg" << std::endl;
	std::cerr << "-V arg  set V = arg" << std::endl;
	std::cerr << "-occu arg arg  set OCCUpation params num_particles, unit_cell_size" << std::endl;
	std::cerr << "-ite arg  set max Arnoldi iteration = arg" << std::endl;
	std::cerr << "-tol arg  set tolerance for GMRES/CG algorithm = arg" << std::endl;
}

//============================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian
	std::string ham_file = "";
	// wavefunction directory
	std::string wf_dir = "mps-inf";
	// Network file directory
	std::string net_dir = ".";
	// Hb destination directory
	std::string hb_dir = ".";
	// system length or unit cell size
	int len = 4;
	// coupling strengths
	double t = 1.0;
	double V = 1.0;
	// occupation number
	int occu = 1;
	int unit = 2;
	// max Arnoldi iteration
	int ar_iter = -1;
	// tolerance for GMRES/CG algorithm
	double tolerance = 1e-15;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				ham_file = std::string(argv[i+1]);
			}
			else { // there was no argument to the -H option.
				std::cerr << "-H option requires the filename of the Hamiltonian." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			if (i + 1 < argc) {
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-n") {
			if (i + 1 < argc) {
				net_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc) {
				hb_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l option requires a positive integer system length (unit cell size)." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-t") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> t;
			}
			else {
				std::cerr << "-t option requires a float number of coupling strength." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-V") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> V;
			}
			else {
				std::cerr << "-Jz option requires a float number of coupling strength." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-occu") {
			if (i + 2 < argc) {
				std::istringstream(argv[i+1]) >> occu;
				std::istringstream(argv[i+2]) >> unit;
			}
			else {
				std::cerr << "-occu option requires arguments (int) number of particles, (int) unit-cell size." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> ar_iter;
			}
			else {
				std::cerr << "-ite option requires a positive integer number of Arnoldi iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> tolerance;
			}
			else {
				std::cerr << "-tol option requires a positive number of error tolerance for Hb solvers." << std::endl;
				return 1;
			}
		}
	}

	/// main function body
	// define operators
	uni10::CUniTensor cp = opFU1_grd0( "cp", occu, unit );
	uni10::CUniTensor cm = opFU1_grd0( "cm", occu, unit );
	uni10::CUniTensor Ni = opFU1_grd0( "N", occu, unit ) + (-0.5) * opFU1_grd0( "id", occu, unit );

	uni10::CUniTensor ham;
	if ( ham_file == "" )
			ham = (-1.) * t * ( otimesPM(cp, cm) + otimesPM(cp, cm, true) )
				+ V * uni10::otimes(Ni, Ni);
	else
		ham = uni10::CUniTensor( ham_file );

	// import imps
	std::vector<uni10::Qnum> qphys = ham.bond()[0].Qlist();
	int bd_dim = uni10::CUniTensor( wf_dir + "/lambda_0" ).bond()[0].dim();
	ChainQnInf imps(len, bd_dim, qphys);
	imps.importMPS( wf_dir );

	// A, B tensors
	const std::vector<uni10::CUniTensor>& gamma = imps.getGamVec();
	const std::vector<uni10::CUniTensor>& lambda = imps.getLamVec();
	std::vector<uni10::CUniTensor> A, B;
	for (int i = 0; i < len; ++i) {
		A.push_back( netLG(lambda[i%len], gamma[i%len]) );
		B.push_back( netGL(gamma[(len-1-i)%len], lambda[(len-i)%len]) );
	}

	// define E5 and E5r
	uni10::CUniTensor E5( lambda[0].bond(), "E5" );
	E5.identity();
	uni10::CUniTensor E5r = bondInv(E5);

	// define E4 and E4r
	uni10::CUniTensor E4 = netContract( true, A, Ni );
	uni10::CUniTensor E4r = netContract( false, B, Ni );

	// define E3 and E3r
	uni10::CUniTensor E3 = netContract( true, A, cm );
	uni10::CUniTensor E3r = netContract( false, B, cm );

	// define E2 and E2r
	uni10::CUniTensor E2 = netContract( true, A, cp );
	uni10::CUniTensor E2r = netContract( false, B, cp );

	// define C;
	std::vector<uni10::CUniTensor> A2, B2;
	for (int i = 0; i < len*2; ++i) {
		A2.push_back( A[i%len] );
		B2.push_back( B[i%len] );
	}
	uni10::CUniTensor C = netContract( true, A2, ham, len-1 );	// lvec
	for (int i = 0; i < len-1; ++i)
		C += netContract( true, A, ham, i );

	// define Cr
	uni10::CUniTensor Cr = netContract( false, B2, ham, len-1 );	// rvec
	for (int i = 0; i < len-1; ++i)
		Cr += netContract( false, B, ham, i );

	// calculate e0
	uni10::CUniTensor lam = lambda[0];
	uni10::CUniTensor traceRhoC = traceRhoVecQn( true, lam, C );
	double e0 = traceRhoC[0].real();
	std::cout << "e0 = " << std::setprecision(10) << e0 << '\n';

	// find HL
	uni10::CUniTensor Idn = E5;
	uni10::CUniTensor rhs_l = C + (-1.0 * e0) * Idn;
	uni10::CUniTensor trial = rhs_l;

	uni10::CUniTensor HL = findHbQn( true, netMPS(true, A), lam, trial, rhs_l, ar_iter, tolerance );
	HL.permute(1);
	HL.setName("HL");
	HL.save( hb_dir + "/HL" );

	// find HR
	traceRhoC = traceRhoVecQn( false, lam, Cr );
	e0 = traceRhoC[0].real();
	std::cout << "e0 = " << std::setprecision(10) << e0 << '\n';
	Idn = E5r;
	uni10::CUniTensor rhs_r = Cr + (-1.0 * e0) * Idn;
	trial = bondInv(HL);

	uni10::CUniTensor HR = findHbQn( false, netMPS(false, B), lam, trial, rhs_r, ar_iter, tolerance );
	HR.permute(1);
	HR.setName("HR");
	HR.save( hb_dir + "/HR" );

	// define HLW
	uni10::CUniTensor HLW
		= (-1.) * t * ( otimesRenormPM( true, E2, cm ) + (-1.) * otimesRenormPM( true, E3, cp ) )
		+ V * uni10::otimes( E4, Ni );
	HLW.save( hb_dir + "/HLW" );

	// define HWR
	uni10::CUniTensor HWR
		= (-1.) * t * ( otimesRenormPM( false, cp, E3r ) + (-1.) * otimesRenormPM( false, cm, E2r ) )
		+ V * uni10::otimes( Ni, E4r );
	HWR.save( hb_dir + "/HWR" );

	// output effective boundary one-site operators
	E2.save( hb_dir + "/Cp_L" );
	E3.save( hb_dir + "/Cm_L" );
	E4.save( hb_dir + "/Ni_L" );
	E5.save( hb_dir + "/Id_L" );
	E2r.save( hb_dir + "/Cp_R" );
	E3r.save( hb_dir + "/Cm_R" );
	E4r.save( hb_dir + "/Ni_R" );
	E5r.save( hb_dir + "/Id_R" );

	return 0;
}
