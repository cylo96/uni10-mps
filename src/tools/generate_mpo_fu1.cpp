#include <iostream>
#include <sstream>
#include <fstream>
#include <sys/stat.h>

#include <tns-func/func_op.h>
#include <mps-qn/func_qn_op.h>

using namespace std;

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H hub <args>  construct Hamiltonian of Hubbard model with args = t, V" << std::endl;
	std::cerr << "-H hub-lr <args>  construct Hamiltonian of Hubbard model with args = t, V, long_range_param_file" << std::endl;
	std::cerr << "-H hub-nn <args>  construct Hamiltonian of Hubbard model with args = t, V" << std::endl;
	std::cerr << "-occu <args>  set OCCUpation params num_particles, unit_cell_size" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	int occu = 1;
	int unit = 2;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-occu") {
			if (i + 2 < argc) {
				std::istringstream(argv[i+1]) >> occu;
				std::istringstream(argv[i+2]) >> unit;
			}
			else {
				std::cerr << "-occu option requires arguments (int) number of particles, (int) unit-cell size." << std::endl;
				return 1;
			}
		}
	}

	// construct operators
	if (std::string(argv[1]) == "-H") {

		if (std::string(argv[2]) == "hub") {

			double t = 1.0;
			double V = 1.0;

			if (argc > 4) {
				std::stringstream(argv[3]) >> t;
				std::stringstream(argv[4]) >> V;
			}

			uni10::CUniTensor id = opFU1_grd0( "id", occu, unit );
			uni10::CUniTensor cp = opFU1_grd0( "cp", occu, unit );
			uni10::CUniTensor cm = opFU1_grd0( "cm", occu, unit );
			uni10::CUniTensor Ni = opFU1_grd0(  "N", occu, unit ) + (-0.5) * id;

			// symmetric blocks
			MPO mpo_ls(5, 'l');
			MPO mpo_ms(5, 'm');
			MPO mpo_rs(5, 'r');

			// raising blocks
			MPO mpo_lp(5, 'l');
			MPO mpo_mp(5, 'm');
			MPO mpo_rp(5, 'r');

			// lowering blocks
			MPO mpo_lm(5, 'l');
			MPO mpo_mm(5, 'm');
			MPO mpo_rm(5, 'r');

			mpo_lm.putTensor(  1.0*t*cm, 0, 1 );
			mpo_lp.putTensor( -1.0*t*cp, 0, 2 );
			mpo_ls.putTensor(      V*Ni, 0, 3 );
			mpo_ls.putTensor(        id, 0, 4 );

			mpo_ms.putTensor(        id, 0, 0 );
			mpo_mp.putTensor(        cp, 1, 0 );
			mpo_mm.putTensor(        cm, 2, 0 );
			mpo_ms.putTensor(        Ni, 3, 0 );
			mpo_mm.putTensor(  1.0*t*cm, 4, 1 );
			mpo_mp.putTensor( -1.0*t*cp, 4, 2 );
			mpo_ms.putTensor(      V*Ni, 4, 3 );
			mpo_ms.putTensor(        id, 4, 4 );

			mpo_rs.putTensor(        id, 0, 0 );
			mpo_rp.putTensor(        cp, 1, 0 );
			mpo_rm.putTensor(        cm, 2, 0 );
			mpo_rs.putTensor(        Ni, 3, 0 );

			struct stat info;
			std::string dirname = "mpo-ham-hub";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_ls.launch().save( dirname + "/mpo_ls" );
			mpo_ms.launch().save( dirname + "/mpo_ms" );
			mpo_rs.launch().save( dirname + "/mpo_rs" );
			mpo_lp.launch().save( dirname + "/mpo_lp" );
			mpo_mp.launch().save( dirname + "/mpo_mp" );
			mpo_rp.launch().save( dirname + "/mpo_rp" );
			mpo_lm.launch().save( dirname + "/mpo_lm" );
			mpo_mm.launch().save( dirname + "/mpo_mm" );
			mpo_rm.launch().save( dirname + "/mpo_rm" );
		}

		else if (std::string(argv[2]) == "hub-nn") {

			double t = 1.0;
			double V = 1.0;

			if (argc > 4) {
				std::stringstream(argv[3]) >> t;
				std::stringstream(argv[4]) >> V;
			}

			uni10::CUniTensor id = opFU1_grd0( "id", occu, unit );
			uni10::CUniTensor cp = opFU1_grd0( "cp", occu, unit );
			uni10::CUniTensor cm = opFU1_grd0( "cm", occu, unit );
			uni10::CUniTensor Ni = opFU1_grd0(  "N", occu, unit );

			// symmetric blocks
			MPO mpo_ls(5, 'l');
			MPO mpo_ms(5, 'm');
			MPO mpo_rs(5, 'r');

			// raising blocks
			MPO mpo_lp(5, 'l');
			MPO mpo_mp(5, 'm');
			MPO mpo_rp(5, 'r');

			// lowering blocks
			MPO mpo_lm(5, 'l');
			MPO mpo_mm(5, 'm');
			MPO mpo_rm(5, 'r');

			mpo_lm.putTensor(  1.0*t*cm, 0, 1 );
			mpo_lp.putTensor( -1.0*t*cp, 0, 2 );
			mpo_ls.putTensor(      V*Ni, 0, 3 );
			mpo_ls.putTensor(        id, 0, 4 );

			mpo_ms.putTensor(        id, 0, 0 );
			mpo_mp.putTensor(        cp, 1, 0 );
			mpo_mm.putTensor(        cm, 2, 0 );
			mpo_ms.putTensor(        Ni, 3, 0 );
			mpo_mm.putTensor(  1.0*t*cm, 4, 1 );
			mpo_mp.putTensor( -1.0*t*cp, 4, 2 );
			mpo_ms.putTensor(      V*Ni, 4, 3 );
			mpo_ms.putTensor(        id, 4, 4 );

			mpo_rs.putTensor(        id, 0, 0 );
			mpo_rp.putTensor(        cp, 1, 0 );
			mpo_rm.putTensor(        cm, 2, 0 );
			mpo_rs.putTensor(        Ni, 3, 0 );

			struct stat info;
			std::string dirname = "mpo-ham-hub";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_ls.launch().save( dirname + "/mpo_ls" );
			mpo_ms.launch().save( dirname + "/mpo_ms" );
			mpo_rs.launch().save( dirname + "/mpo_rs" );
			mpo_lp.launch().save( dirname + "/mpo_lp" );
			mpo_mp.launch().save( dirname + "/mpo_mp" );
			mpo_rp.launch().save( dirname + "/mpo_rp" );
			mpo_lm.launch().save( dirname + "/mpo_lm" );
			mpo_mm.launch().save( dirname + "/mpo_mm" );
			mpo_rm.launch().save( dirname + "/mpo_rm" );
		}

		else if (std::string(argv[2]) == "hub-lr") {

			double t = 1.0;
			double V = 1.0;
			std::vector<double> lam;
			std::vector<double> w;

			if (argc < 6) {
				errMsg( argv[0] );
				return 1;
			}

			std::stringstream(argv[3]) >> t;
			std::stringstream(argv[4]) >> V;
			std::ifstream params(argv[5]);
			if ( !params ) {
				errMsg( argv[0] );
				return 1;
			}

			int nu = 0;
			double value;
			std::string line;
			while( !params.eof() ) {
				std::getline(params, line);
				std::stringstream vin(line);
				if (nu == 0)
					while ( vin >> value )
						lam.push_back(value);
				else if (nu == 1)
					while ( vin >> value )
						w.push_back(value);
				nu += 1;
			}

			int len_params = lam.size();

			double sum_w = 0.0;
			for (int i = 0; i < len_params; ++i)
				sum_w += w[i];

			t = t - sum_w;

			uni10::CUniTensor id = opFU1_grd0( "id", occu, unit );
			uni10::CUniTensor cp = opFU1_grd0( "cp", occu, unit );
			uni10::CUniTensor cm = opFU1_grd0( "cm", occu, unit );
			uni10::CUniTensor Ni = opFU1_grd0(  "N", occu, unit ) + (-0.5) * id;
			int dim_w = 5 + 2 * len_params;

			// symmetric blocks
			MPO mpo_ls(dim_w, 'l');
			MPO mpo_ms(dim_w, 'm');
			MPO mpo_rs(dim_w, 'r');

			// raising blocks
			MPO mpo_lp(dim_w, 'l');
			MPO mpo_mp(dim_w, 'm');
			MPO mpo_rp(dim_w, 'r');

			// lowering blocks
			MPO mpo_lm(dim_w, 'l');
			MPO mpo_mm(dim_w, 'm');
			MPO mpo_rm(dim_w, 'r');

			mpo_lm.putTensor(  1.0*t*cm, 0, 1 );
			mpo_lp.putTensor( -1.0*t*cp, 0, 2 );
			mpo_ls.putTensor(      V*Ni, 0, 3 );
			for (int i = 0; i < len_params; ++i) {
				mpo_lm.putTensor(  1.0*w[i]*cm, 0, 4+i*2 );
				mpo_lp.putTensor( -1.0*w[i]*cp, 0, 5+i*2 );
			}
			mpo_ls.putTensor( id, 0, dim_w-1 );

			mpo_ms.putTensor( id, 0, 0 );
			mpo_mp.putTensor( cp, 1, 0 );
			mpo_mm.putTensor( cm, 2, 0 );
			mpo_ms.putTensor( Ni, 3, 0 );
			for (int i = 0; i < len_params; ++i) {
				mpo_mp.putTensor( cp, 4+i*2, 0 );
				mpo_mm.putTensor( cm, 5+i*2, 0 );
			}
			mpo_mm.putTensor(  1.0*t*cm, dim_w-1, 1 );
			mpo_mp.putTensor( -1.0*t*cp, dim_w-1, 2 );
			mpo_ms.putTensor(      V*Ni, dim_w-1, 3 );
			for (int i = 0; i < len_params; ++i) {
				mpo_mm.putTensor(  1.0*w[i]*cm, dim_w-1, 4+i*2 );
				mpo_mp.putTensor( -1.0*w[i]*cp, dim_w-1, 5+i*2 );
			}
			mpo_ms.putTensor( id, dim_w-1, dim_w-1 );

			for (int i = 0; i < len_params; ++i) {
				mpo_ms.putTensor( lam[i]*id, 4+i*2, 4+i*2 );
				mpo_ms.putTensor( lam[i]*id, 5+i*2, 5+i*2 );
			}

			mpo_rs.putTensor( id, 0, 0 );
			mpo_rp.putTensor( cp, 1, 0 );
			mpo_rm.putTensor( cm, 2, 0 );
			mpo_rs.putTensor( Ni, 3, 0 );
			for (int i = 0; i < len_params; ++i) {
				mpo_rp.putTensor( cp, 4+i*2, 0 );
				mpo_rm.putTensor( cm, 5+i*2, 0 );
			}

			struct stat info;
			std::string dirname = "mpo-ham-hub";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_ls.launch().save( dirname + "/mpo_ls" );
			mpo_ms.launch().save( dirname + "/mpo_ms" );
			mpo_rs.launch().save( dirname + "/mpo_rs" );
			mpo_lp.launch().save( dirname + "/mpo_lp" );
			mpo_mp.launch().save( dirname + "/mpo_mp" );
			mpo_rp.launch().save( dirname + "/mpo_rp" );
			mpo_lm.launch().save( dirname + "/mpo_lm" );
			mpo_mm.launch().save( dirname + "/mpo_mm" );
			mpo_rm.launch().save( dirname + "/mpo_rm" );
		}

		else {
			std::cerr << "-H model Hamiltonian. Models supported: hub, hub-lr" << std::endl;
			return 1;
		}
	}

	else {

		errMsg( argv[0] );
		return 1;
	}

	return 0;
}
