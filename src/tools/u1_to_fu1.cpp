#include <iostream>
#include <sstream>
#include <mps.hpp>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-l arg  Length of U1 mps" << std::endl;
	std::cerr << "-w arg  directory of U1 Wavefunc mps" << std::endl;
	std::cerr << "-df arg  Destination Folder of FU1 mps" << std::endl;
	std::cerr << "-fo  set lowest positive or zero U1 qnum block to prtf Odd (defult: even)" << std::endl;
	std::cerr << "-shift arg  SHIFT the unit cell with arg sites" << std::endl;
	std::cerr << "-obc  for OBC/IBC/PBC mps" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	std::string wf_dir = "mps-inf";
	std::string df_dir = "mps-inf";
	bool fo = false;
	bool obc = false;
	int len_u1 = 2;
	int len_fu1 = 4;
	int chi_max = 2;
	int shift = 0;

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-w") {
			if (i + 1 < argc)
				wf_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-w <(str) directory name of the source U1 mps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				df_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-df <(str) directory name of the destination FU1 mps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len_u1;
			else {
				std::cerr << "-l <(int) system length>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-shift") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> shift;
			else {
				std::cerr << "-shift <(int) num of sites to shift>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-fo")
			fo = true;
		else if (std::string(argv[i]) == "-obc")
			obc = true;
	}

	/// main function body
	uni10::Bond pbd_u1 = uni10::CUniTensor( wf_dir + "/gamma_0" ).bond()[1];
	std::vector<uni10::Qnum> qn_u1 = pbd_u1.Qlist();
	std::vector<uni10::Qnum> qn_fu1 = pbd_u1.Qlist();
	uni10::Qnum qf(uni10::PRTF_ODD, 0, uni10::PRT_EVEN);
	for (int i = 0; i < qn_fu1.size(); ++i) {
		if (qn_fu1[i].U1() == 1)
			qn_fu1[i] = qn_fu1[i] * qf;
	}
	uni10::Bond pbd_fu1(uni10::BD_IN, qn_fu1);

	if (obc) {
		std::cerr << "Not Implemented Error." << '\n';
		return 1;
	}

	else {
		len_fu1 = len_u1 * 2;
		ChainQnInf mpsU1(len_u1, chi_max, qn_u1);
		ChainQnInf mpsFU1(len_fu1, chi_max, qn_fu1);
		mpsU1.importMPS(wf_dir);
		std::vector<uni10::CUniTensor> gams, lams;
		for (int i = 0; i < len_u1; ++i) {
			gams.push_back( mpsU1.getGamma( (i+shift)%len_u1 ) );
			lams.push_back( mpsU1.getLambda( (i+shift)%len_u1 ) );
		}

		bool lfo = false; // lam's first block (lowest U1) ferm odd?
		for (int i = 0; i < len_fu1; ++i) {
			std::vector<uni10::Bond> bdl = lams[i%len_u1].bond();
			std::vector<uni10::Bond> bdg = gams[i%len_u1].bond();

			std::vector<uni10::Qnum> qn_lam;
			std::vector<uni10::Qnum> qn_list = lams[i%len_u1].blockQnum();
			if (i == 0) {
				int n = 0;
				while (qn_list[n].U1() < 0) n++; // find idx of the lowest positive or zero U1 qnum
				if (n%2 == (int)(!fo)) lfo = true;
			}

			std::map<uni10::Qnum, int> deg_lam = bdl[0].degeneracy();
			int j = 0;
			for (std::map<uni10::Qnum, int>::iterator it = deg_lam.begin(); it != deg_lam.end(); ++it) {
				uni10::Qnum q = it->first;
				int deg = it->second;
				if ((j+1)%2 == (int)lfo) q = q * qf;
				for (int k = 0; k < deg; ++k)
					qn_lam.push_back(q);
				qn_list[j] = q;
				j++;
			}
			bdl[0] = uni10::Bond(uni10::BD_IN, qn_lam);
			bdl[1] = uni10::Bond(uni10::BD_OUT, qn_lam);

			bdg[0] = bdl[0];
			bdg[1] = pbd_fu1;
			std::vector<uni10::Qnum> qn_gam;
			std::map<uni10::Qnum, int> deg_gam = bdg[2].degeneracy();
			j = 0;
			bool gfo = false;
			for (std::map<uni10::Qnum, int>::iterator it = deg_gam.begin(); it != deg_gam.end(); ++it) {
				uni10::Qnum q = it->first;
				int deg = it->second;
				if (j == 0) gfo = ((q.U1() - qn_list[0].U1() == 1 && !lfo) || (q.U1() - qn_list[0].U1() == -1 && lfo));
				if ((j+1)%2 == (int)gfo) q = q * qf;
				for (int k = 0; k < deg; ++k)
					qn_gam.push_back(q);
				j++;
			}
			bdg[2] = uni10::Bond(uni10::BD_OUT, qn_gam);

			uni10::CUniTensor lam_fu1(bdl);
			uni10::CUniTensor gam_fu1(bdg);

			std::map<uni10::Qnum, uni10::CMatrix> blks_lam = lams[i%len_u1].getBlocks();
			std::map<uni10::Qnum, uni10::CMatrix> blks_gam = gams[i%len_u1].getBlocks();
			for (std::map<uni10::Qnum, uni10::CMatrix>::iterator it = blks_lam.begin(); it != blks_lam.end(); ++it) {
				uni10::Qnum q = it->first;
				uni10::CMatrix blk = it->second;
				try {
					lam_fu1.putBlock( q, blk );
				}
				catch (const std::exception& e) {
					lam_fu1.putBlock( q*qf, blk );
				}
			}
			for (std::map<uni10::Qnum, uni10::CMatrix>::iterator it = blks_gam.begin(); it != blks_gam.end(); ++it) {
				uni10::Qnum q = it->first;
				uni10::CMatrix blk = it->second;
				try {
					gam_fu1.putBlock( q, blk );
				}
				catch (const std::exception& e) {
					gam_fu1.putBlock( q*qf, blk );
				}
			}

			mpsFU1.putLambda(lam_fu1);
			mpsFU1.putGamma(gam_fu1);
			lfo = gfo;
		}

		mpsFU1.exportMPS(df_dir);
	}

	return 0;
}
