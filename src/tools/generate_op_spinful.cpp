#include <iostream>
#include <sstream>

#include <tns-func/tns_const.h>
#include <mps-qn/func_qn_op.h>

using namespace std;

//======================================

void errMsg( char* arg ) {
	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H hub <t> <U> (<V> <mu>)     : Hamiltonian of Hubbard model" << std::endl;
	std::cerr << "-cp_up  : cp_up (c^+_up) operator" << std::endl;
	std::cerr << "-cp_dn  : cp_dn (c^+_dn) operator" << std::endl;
	std::cerr << "-cm_up  : cm_up (c_up) operator" << std::endl;
	std::cerr << "-cm_dn  : cm_dn (c_dn) operator" << std::endl;
	std::cerr << "-N_up   : N_up operator" << std::endl;
	std::cerr << "-N_dn   : N_dn operator" << std::endl;
	std::cerr << "-N      : N operator" << std::endl;
	std::cerr << "-id     : identity operator" << std::endl;
	std::cerr << "-occu <num_ptcl> <unit_cell>  : set OCCUpation params" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {
		errMsg( argv[0] );
		return 1;
	}

	int occu = 1;
	int unit = 2;

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-occu") {
			if (i + 2 < argc) {
				std::istringstream(argv[i+1]) >> occu;
				std::istringstream(argv[i+2]) >> unit;
			}
			else {
				std::cerr << "-occu option requires arguments (int) number of particles, (int) unit-cell size." << std::endl;
				return 1;
			}
		}
	}


	// construct operators
	if (std::string(argv[1]) == "-H") {
		if ( (argc > 2) && (std::string(argv[2]) == "hub") ) {
			double t = 1.0;
			double U = 1.0;
			double V = 0.0;
			double mu = 0.0;
			if (argc > 4) {
				std::stringstream(argv[3]) >> t;
				std::stringstream(argv[4]) >> U;
			}
			if (argc > 5)
				std::stringstream(argv[5]) >> V;
			if (argc > 6)
				std::stringstream(argv[6]) >> mu;

			uni10::CUniTensor id = opFU1_spinful( "id", occu, unit );
			uni10::CUniTensor NN = opFU1_spinful( "NN", occu, unit );
			uni10::CUniTensor cp_up = opFU1_spinful( "cp_up", occu, unit );
			uni10::CUniTensor cm_up = opFU1_spinful( "cm_up", occu, unit );
			uni10::CUniTensor cp_dn = opFU1_spinful( "cp_dn", occu, unit );
			uni10::CUniTensor cm_dn = opFU1_spinful( "cm_dn", occu, unit );
			uni10::CUniTensor N_up = opFU1_spinful( "N_up", occu, unit );
			uni10::CUniTensor N_dn = opFU1_spinful( "N_dn", occu, unit );
			uni10::CUniTensor N = opFU1_spinful( "N", occu, unit );
			// uni10::CUniTensor Nu = N_up + (-0.5) * id;
			// uni10::CUniTensor Nd = N_dn + (-0.5) * id;

			uni10::CUniTensor ham
				= (-1.) * t * ( otimesPM(cp_up, cm_up) + otimesPM(cp_up, cm_up, true) 
					+ otimesPM(cp_dn, cm_dn) + otimesPM(cp_dn, cm_dn, true) )
				+ (0.5) * U * ( uni10::otimes(NN, id) + uni10::otimes(id, NN) )
				+ (1.0) * V * uni10::otimes(N, N)
				+ (-.5) * mu * ( uni10::otimes(N, id) + uni10::otimes(id, N) );
				// + (1.0) * V * ( uni10::otimes(Nu, Nu) + uni10::otimes(Nd, Nd) )

			ham.save( "ham_hub" );
			std::cout << ham;
		}
		else {
			std::cerr << "-H model Hamiltonian. Valid choices: hub" << std::endl;
			return 1;
		}
	}
	else if (std::string(argv[1]) == "-id") {
		uni10::CUniTensor op = opFU1_spinful( "id", occu, unit );
		op.save( "id" );
		std::cout << op;
	}
	else if (std::string(argv[1]) == "-N_up") {
		uni10::CUniTensor op = opFU1_spinful( "N_up", occu, unit );
		op.save( "N_up" );
		std::cout << op;
	}
	else if (std::string(argv[1]) == "-N_dn") {
		uni10::CUniTensor op = opFU1_spinful( "N_dn", occu, unit );
		op.save( "N_dn" );
		std::cout << op;
	}
	else if (std::string(argv[1]) == "-N") {
		uni10::CUniTensor op = opFU1_spinful( "N", occu, unit );
		op.save( "N" );
		std::cout << op;
	}
	else if (std::string(argv[1]) == "-NN") {
		uni10::CUniTensor op = opFU1_spinful( "NN", occu, unit );
		op.save( "NN" );
		std::cout << op;
	}
	else if (std::string(argv[1]) == "-cp_up") {
		uni10::CUniTensor op = opFU1_spinful( "cp_up", occu, unit );
		op.save( "cp_up" );
		std::cout << op;
	}
	else if (std::string(argv[1]) == "-cm_up") {
		uni10::CUniTensor op = opFU1_spinful( "cm_up", occu, unit );
		op.save( "cm_up" );
		std::cout << op;
	}
	else if (std::string(argv[1]) == "-cp_dn") {
		uni10::CUniTensor op = opFU1_spinful( "cp_dn", occu, unit );
		op.save( "cp_dn" );
		std::cout << op;
	}
	else if (std::string(argv[1]) == "-cm_dn") {
		uni10::CUniTensor op = opFU1_spinful( "cm_dn", occu, unit );
		op.save( "cm_dn" );
		std::cout << op;
	}
	else {
		errMsg( argv[0] );
		return 1;
	}

	return 0;
}
