#include <iostream>
#include <sstream>

#include <tns-func/tns_const.h>
#include <mps-qn/func_qn_op.h>

using namespace std;

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H xxz args  construct Hamiltonian of spin 1/2 xxz model with J, Jz, hz = args" << std::endl;
	std::cerr << "-sz  construct sz (sigma z) operator" << std::endl;
	std::cerr << "-sp  construct sp (sigma +) operator" << std::endl;
	std::cerr << "-sm  construct sm (sigma -) operator" << std::endl;
	std::cerr << "-Sz  construct Sz operator" << std::endl;
	std::cerr << "-Sp  construct Sp (S+) operator" << std::endl;
	std::cerr << "-Sm  construct Sm (S-) operator" << std::endl;
	std::cerr << "-id  construct identity operator" << std::endl;
	std::cerr << "-Jl  construct Jl operator" << std::endl;
	std::cerr << "-Jr  construct Jr operator" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// construct operators
	if (std::string(argv[1]) == "-H") {

		if (std::string(argv[2]) == "xxz") {

			double J = 1.0;
			double Jz = 1.0;
			double hz1 = 0.0;
			double hz2 = 0.0;

			if (argc > 4) {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> Jz;
			}
			if (argc > 5) {
				std::stringstream(argv[5]) >> hz1;
				hz2 = hz1;
			}
			if (argc > 6)
				std::stringstream(argv[6]) >> hz2;

			uni10::CUniTensor Sp = opU1( "Sp" );
			uni10::CUniTensor Sm = opU1( "Sm" );
			uni10::CUniTensor Sz = opU1( "Sz" );
			uni10::CUniTensor Id = opU1( "id" );
			
			uni10::CUniTensor ham 
				= J * 0.5 * ( otimesPM(Sp, Sm) + otimesPM(Sm, Sp) )
				+ Jz * uni10::otimes(Sz, Sz)
				+ hz1 * 0.5 * otimes(Sz, Id) + hz2 * 0.5 * otimes(Id, Sz);

			ham.save( "ham_xxz" );
			std::cout << ham;
		}

		else { // Uh-oh, there was no argument to the -H option.
			std::cerr << "-H model Hamiltonian. Valid choices: xxz" << std::endl;
			return 1;
		}
	}

	else if (std::string(argv[1]) == "-id") {

		uni10::CUniTensor op = opU1( "id" );
		op.save( "id" );
		std::cout << op;	
	}

	else if (std::string(argv[1]) == "-sz") {

		uni10::CUniTensor op = opU1( "sz" );
		op.save( "sz" );
		std::cout << op;	
	}

	else if (std::string(argv[1]) == "-sp") {

		uni10::CUniTensor op = opU1( "sp" );
		op.save( "sp" );
		std::cout << op;	
	}

	else if (std::string(argv[1]) == "-sm") {

		uni10::CUniTensor op = opU1( "sm" );
		op.save( "sm" );
		std::cout << op;	
	}

	else if (std::string(argv[1]) == "-Sz") {

		uni10::CUniTensor op = opU1( "Sz" );
		op.save( "Sz" );
		std::cout << op;	
	}

	else if (std::string(argv[1]) == "-Sp") {

		uni10::CUniTensor op = opU1( "Sp" );
		op.save( "Sp" );
		std::cout << op;	
	}

	else if (std::string(argv[1]) == "-Sm") {

		uni10::CUniTensor op = opU1( "Sm" );
		op.save( "Sm" );
		std::cout << op;	
	}

	else if (std::string(argv[1]) == "-Jl") {

		uni10::CUniTensor Sp = opU1( "Sp" );
		uni10::CUniTensor Sm = opU1( "Sm" );
		uni10::CUniTensor op = I * ( otimesPM(Sp, Sm) + (-1.0) * otimesPM(Sm, Sp) );

		op.setName( "Jl" );
		op.save( "Jl" );
		std::cout << op;	
	}

	else if (std::string(argv[1]) == "-Jr") {

		uni10::CUniTensor Sp = opU1( "Sp" );
		uni10::CUniTensor Sm = opU1( "Sm" );
		uni10::CUniTensor op = I * ( otimesPM(Sm, Sp) + (-1.0) * otimesPM(Sp, Sm) );

		op.setName( "Jr" );
		op.save( "Jr" );
		std::cout << op;	
	}

	else {

		errMsg( argv[0] );
		return 1;
	}

	return 0;
}
