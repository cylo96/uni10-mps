#!/usr/bin/env python

""" approx power """
from __future__ import print_function
import sys
import numpy as np
from scipy import linalg

def approx_power(N, n, alpha, f):
    """
    OBJECTIVE:
    Generate a N-by-n (N > n) numpy array F, whose elements are

    [ f(1)     f(2)     .... f(n)   ]
    [ f(2)     f(3)     .... f(n+1) ]
    .
    .
    [ f(N-n+1) f(N-n+2) .... f(N)   ]

    for arbitray function f(r).

    Our function of interest is const./r**alpha, you may want to implement it
    """
    m = N - n + 1
    if m < n: m, n = n, m
    def fn(r):
        return f / r**(alpha)

    F = np.array([[fn(i+j) for j in range(n)] for i in range(1, m + 1)])

    #print("Function matrix:")
    #print(F)
    #print()

    ## ===================================================== ##

    """
    Find lambda
    """

    #Q, R = np.linalg.qr(F)
    U, _ = linalg.qr(F, mode='economic')
    U1 = U[:-1]
    U2 = U[1:]
    U1i = linalg.pinv(U1)
    Lam = linalg.eigvals(np.dot(U1i, U2))

    #print("Fitted bases (lambda):")
    #print(Lam)
    #print()

    ## ===================================================== ##

    """
    Find x
    """

    Lmtx = np.array([[Lam[j]**i for j in range(n)] for i in range(1, N+1)])
    fvec = np.array([fn(i) for i in range(1, N+1)])
    x = linalg.lstsq(Lmtx, fvec)[0]

    ## ===================================================== ##

    """
    Calculate error
    """
    err = []
    for i in range(1, N+1):
        F_i = fn(i)
        f_i = 0
        for j in range(n):
            f_i += x[j] * (Lam[j] ** (i))
        err.append(np.abs(F_i - f_i))

    return Lam, x, err

if __name__ == "__main__":
    if len(sys.argv) < 5:
        print('uasge: {} N n alpha f [output_file]'.format(sys.argv[0].split('/')[-1]))
        print()
        print('    fn(r) = f / r**(alpha)')
        exit()
    N = int(sys.argv[1])
    n = int(sys.argv[2])
    alpha = float(sys.argv[3])
    f = float(sys.argv[4])

    lmb, x, err = approx_power(N, n, alpha, f)

    if len(sys.argv) == 5:
        print('Lambda:')
        print(lmb.real)
        print()
        print('x:')
        print(x.real)
        print()
        print('Sum of error:', end=' ')
        print(np.sum(err))
    else:
        ofile = sys.argv[5]
        with open(ofile, 'w') as file:
            file.write(' '.join(str(lmbi) for lmbi in lmb.real))
            file.write('\n')
            file.write(' '.join(str(xi) for xi in x.real))
