#include <iostream>
#include <sstream>
#include <mps.hpp>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-w <(str) Wavefunction mps directory>" << std::endl;
	std::cerr << "-ww3 <(str) Wavefunc Wire1> <(str) Wavefunc Wire2> <(str) Wavefunc Wire3>" << std::endl;
	std::cerr << "-l <(int) system Length>" << std::endl;
	std::cerr << "-uc <(int) Unit Cell size>" << std::endl;
	std::cerr << "-m <(int) Max bond dimension>" << std::endl;
	std::cerr << "-df <(str) Destination Folder for final mps>" << std::endl;
}

//======================================

int main( int argc, char* argv[] ) {

	if (argc < 2) {
		errMsg( argv[0] );
		return 1;
	}

	/// main function body
	// wavefunction directory
	std::string wf_dir = "mps-ibc-yj";
	std::string ww1_dir = "mps-inf";
	std::string ww2_dir = "mps-inf";
	std::string ww3_dir = "mps-inf";
	// final mps directory
	std::string mps_dir = "";
	// length of each wire
	int len = 16;
	// unit-cell size
	int uc = 4;
	// bond dimension
	int chi = 8;
	// separate junction gams
	bool sep_gj = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-w") {
			if (i + 1 < argc) {
				wf_dir = std::string(argv[i+1]);
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ww3") {
			if (i + 3 < argc) {
				ww1_dir = std::string(argv[i+1]);
				ww2_dir = std::string(argv[i+2]);
				ww3_dir = std::string(argv[i+3]);
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-uc") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> uc;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> chi;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
	}

	// initialize YJ
	uni10::CUniTensor gam0;
	try {
		gam0 = uni10::CUniTensor(wf_dir + "/wire1/gamma_0");
	}
	catch(const std::exception& e) {
		try {
			gam0 = uni10::CUniTensor(wf_dir + "/gamma_0");
		}
		catch(const std::exception& e) {
			errMsg(argv[0]);
			return 1;
		}
	}
	std::vector<uni10::Qnum> qphys = gam0.bond()[1].Qlist();
	std::vector<uni10::Qnum> qvirt = gam0.bond()[0].Qlist();
	chi = std::max(chi, gam0.bond()[0].dim());
	YJuncQnIBC mpsYJ(len, chi, qphys, qvirt);

	// import MPS
	mpsYJ.importMPS(wf_dir, uc, sep_gj);
	
	// match qnum at IBC
	mpsYJ.matchQnIBC(ww1_dir, ww2_dir, ww3_dir);

	// export MPS
	if (mps_dir == "")
		mps_dir = wf_dir;
	mpsYJ.exportMPS(mps_dir);

	return 0;
}
