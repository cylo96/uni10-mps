#include <iostream>
#include <sstream>
#include <sys/stat.h>

#include <tns-func/tns_const.h>
#include <tns-func/func_op.h>
#include <mps-qn/func_qn_op.h>

#define _USE_MATH_DEFINES
#include <cmath>

using namespace std;

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H hub <(num) t> <(num) V> (-imp <t_imp> <V_imp>) (-phase <(deg) ph_t_imp> <(deg) ph_V_imp>)"
		<< "\n\t\t\tHamiltonian settings" << std::endl;
	std::cerr << "-w <(str) hb_dir>\n\t\t\tload Hb from directory" << std::endl;
	std::cerr << "-df <(str) mpo_dir>\n\t\t\tset the Destination Folder for MPO" << std::endl;
	std::cerr << "-occu <(int) num_particles> <(int) unit_cell_size>\n\t\t\tset OCCUpation density" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	/// common settings
	// Hb directory
	std::string hb_dir = ".";
	// mpo directory
	std::string mpo_dir = "mpo-ham-hub";
	// occupation number
	int occu = 1;
	int unit = 2;

	if (argc < 3) {
		errMsg( argv[0] );
		return 1;
	}

	int H_idx;
	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc)
				H_idx = i+1; // for later use
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			if (i + 1 < argc) {
				hb_dir = std::string(argv[i+1]);
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc) {
				mpo_dir = std::string(argv[i+1]);
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-occu") {
			if (i + 2 < argc) {
				std::istringstream(argv[i+1]) >> occu;
				std::istringstream(argv[i+2]) >> unit;
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
	}

	// construct operators
	if (std::string(argv[H_idx]) == "hub") {

		double t = 1.0;
		double V = 1.0;
		double t_imp = 1.0;
		double V_imp = 1.0;
		double ph_t_imp = 0.0;
		double ph_V_imp = 0.0;
		bool imp = false;

		if (H_idx + 2 < argc) {
			std::stringstream(argv[H_idx+1]) >> t;
			std::stringstream(argv[H_idx+2]) >> V;
		}

		for (int i = H_idx+3; i < argc; ++i) {
			if (std::string(argv[i]) == "-imp") {
				if (i + 2 < argc) {
					std::stringstream(argv[i+1]) >> t_imp;
					std::stringstream(argv[i+2]) >> V_imp;
					imp = true;
				}
				else {
					errMsg( argv[0] );
					return 1;
				}
			}
			else if (std::string(argv[i]) == "-phase") {
				if (i + 2 < argc) {
					std::stringstream(argv[i+1]) >> ph_t_imp;
					std::stringstream(argv[i+2]) >> ph_V_imp;
				}
				else {
					errMsg( argv[0] );
					return 1;
				}
			}
		}

		uni10::CUniTensor id = opFU1_grd0( "id", occu, unit );
		uni10::CUniTensor cp = opFU1_grd0( "cp", occu, unit );
		uni10::CUniTensor cm = opFU1_grd0( "cm", occu, unit );
		uni10::CUniTensor Ni = opFU1_grd0(  "N", occu, unit ) + (-0.5) * id;

		uni10::CUniTensor hl   = uni10::CUniTensor( hb_dir + "/HL" );
		uni10::CUniTensor cp_l = uni10::CUniTensor( hb_dir + "/Cp_L" );
		uni10::CUniTensor cm_l = uni10::CUniTensor( hb_dir + "/Cm_L" );
		uni10::CUniTensor Ni_l = uni10::CUniTensor( hb_dir + "/Ni_L" );
		uni10::CUniTensor id_l = uni10::CUniTensor( hb_dir + "/Id_L" );

		uni10::CUniTensor hr   = uni10::CUniTensor( hb_dir + "/HR" );
		uni10::CUniTensor cp_r = uni10::CUniTensor( hb_dir + "/Cp_R" );
		uni10::CUniTensor cm_r = uni10::CUniTensor( hb_dir + "/Cm_R" );
		uni10::CUniTensor Ni_r = uni10::CUniTensor( hb_dir + "/Ni_R" );
		uni10::CUniTensor id_r = uni10::CUniTensor( hb_dir + "/Id_R" );

		std::vector<int> dim_yj_l, dim_yj_r;
		dim_yj_l.push_back(5);
		dim_yj_r.push_back(5);
		dim_yj_r.push_back(5);

		// network A: wire1 at left, wire2 + wire3 at right
		// network B: wire1 + wire3 at left, wire2 at right
		// network C: wire1 + wire2 at left, wire3 at right
		// symmetric blocks
		MPO mpo_ls(5, 'l');
		MPO mpo_ms(5, 'm');
		MPO mpo_rs(5, 'r');
		MPO mpo_1as(dim_yj_l, dim_yj_r);	// junct 1 in network A
		MPO mpo_1bs(dim_yj_l, dim_yj_r);	// junct 1 in network B
		MPO mpo_2as(dim_yj_l, dim_yj_r);	// junct 2 in network A
		MPO mpo_2bs(dim_yj_l, dim_yj_r);	// junct 2 in network B
		MPO mpo_2cs(dim_yj_l, dim_yj_r);	// junct 2 in network C
		MPO mpo_3as(dim_yj_l, dim_yj_r);	// junct 3 in network A
		MPO mpo_3bs(dim_yj_l, dim_yj_r);	// junct 3 in network A

		// raising blocks
		MPO mpo_lp(5, 'l');
		MPO mpo_mp(5, 'm');
		MPO mpo_rp(5, 'r');
		MPO mpo_1ap(dim_yj_l, dim_yj_r);
		MPO mpo_1bp(dim_yj_l, dim_yj_r);
		MPO mpo_2ap(dim_yj_l, dim_yj_r);
		MPO mpo_2bp(dim_yj_l, dim_yj_r);
		MPO mpo_2cp(dim_yj_l, dim_yj_r);
		MPO mpo_3ap(dim_yj_l, dim_yj_r);
		MPO mpo_3bp(dim_yj_l, dim_yj_r);

		// lowering blocks
		MPO mpo_lm(5, 'l');
		MPO mpo_mm(5, 'm');
		MPO mpo_rm(5, 'r');
		MPO mpo_1am(dim_yj_l, dim_yj_r);
		MPO mpo_1bm(dim_yj_l, dim_yj_r);
		MPO mpo_2am(dim_yj_l, dim_yj_r);
		MPO mpo_2bm(dim_yj_l, dim_yj_r);
		MPO mpo_2cm(dim_yj_l, dim_yj_r);
		MPO mpo_3am(dim_yj_l, dim_yj_r);
		MPO mpo_3bm(dim_yj_l, dim_yj_r);

		mpo_ls.putTensor(          hl, 0, 0 );
		mpo_lm.putTensor(  1.0*t*cm_l, 0, 1 );
		mpo_lp.putTensor( -1.0*t*cp_l, 0, 2 );
		mpo_ls.putTensor(      V*Ni_l, 0, 3 );
		mpo_ls.putTensor(        id_l, 0, 4 );

		mpo_ms.putTensor(          id, 0, 0 );
		mpo_mp.putTensor(          cp, 1, 0 );
		mpo_mm.putTensor(          cm, 2, 0 );
		mpo_ms.putTensor(          Ni, 3, 0 );
		mpo_mm.putTensor(    1.0*t*cm, 4, 1 );
		mpo_mp.putTensor(   -1.0*t*cp, 4, 2 );
		mpo_ms.putTensor(        V*Ni, 4, 3 );
		mpo_ms.putTensor(          id, 4, 4 );

		mpo_rs.putTensor(        id_r, 0, 0 );
		mpo_rp.putTensor(        cp_r, 1, 0 );
		mpo_rm.putTensor(        cm_r, 2, 0 );
		mpo_rs.putTensor(        Ni_r, 3, 0 );
		mpo_rs.putTensor(          hr, 4, 0 );

		if (!imp) {
			t_imp = t;
			V_imp = V;
		}
		ph_t_imp *= (M_PI/180.);
		ph_V_imp *= (M_PI/180.);
		Complex ct_imp = t_imp * Complex(cos(ph_t_imp), sin(ph_t_imp));
		Complex cV_imp = V_imp * Complex(cos(ph_V_imp), sin(ph_V_imp));
		Complex cct_imp = conj(ct_imp);
		Complex ccV_imp = conj(cV_imp);

		mpo_1as.putTensor(             id, 4, 0 );
		mpo_1ap.putTensor( -1.0*ct_imp*cp, 4, 5 );
		mpo_1am.putTensor( 1.0*cct_imp*cm, 4,10 );
		mpo_1as.putTensor(      cV_imp*Ni, 4,15 );
		mpo_1as.putTensor(             id, 0,20 );
		mpo_1ap.putTensor(             cp, 1,20 );
		mpo_1am.putTensor(             cm, 2,20 );
		mpo_1as.putTensor(             Ni, 3,20 );
		mpo_1am.putTensor( 1.0*cct_imp*cm, 4,21 );
		mpo_1ap.putTensor( -1.0*ct_imp*cp, 4,22 );
		mpo_1as.putTensor(      cV_imp*Ni, 4,23 );
		mpo_1as.putTensor(             id, 4,24 );

		mpo_1bs.putTensor(             id, 0, 0 );
		mpo_1bp.putTensor(             cp, 1, 0 );
		mpo_1bm.putTensor(             cm, 2, 0 );
		mpo_1bs.putTensor(             Ni, 3, 0 );
		mpo_1bm.putTensor( 1.0*cct_imp*cm, 4, 1 );
		mpo_1bp.putTensor( -1.0*ct_imp*cp, 4, 2 );
		mpo_1bs.putTensor(      cV_imp*Ni, 4, 3 );
		mpo_1bs.putTensor(             id, 4, 4 );
		mpo_1bm.putTensor( 1.0*cct_imp*cm, 4, 5 );
		mpo_1bp.putTensor( -1.0*ct_imp*cp, 4,10 );
		mpo_1bs.putTensor(      cV_imp*Ni, 4,15 );
		mpo_1bs.putTensor(             id, 4,20 );

		mpo_2am.putTensor(       1.0*t*cm, 0, 1 );
		mpo_2ap.putTensor(      -1.0*t*cp, 0, 2 );
		mpo_2as.putTensor(           V*Ni, 0, 3 );
		mpo_2as.putTensor(             id, 0, 4 );
		mpo_2am.putTensor(             cm, 1, 0 );
		mpo_2ap.putTensor(             cp, 2, 0 );
		mpo_2as.putTensor(             Ni, 3, 0 );
		mpo_2as.putTensor(             id, 4, 0 );
		mpo_2am.putTensor( 1.0*cct_imp*cm, 4, 5 );
		mpo_2ap.putTensor( -1.0*ct_imp*cp, 4,10 );
		mpo_2as.putTensor(      cV_imp*Ni, 4,15 );
		mpo_2as.putTensor(             id, 4,20 );

		mpo_2bs.putTensor(             id, 0, 0 );
		mpo_2bp.putTensor(             cp, 1, 0 );
		mpo_2bm.putTensor(             cm, 2, 0 );
		mpo_2bs.putTensor(             Ni, 3, 0 );
		mpo_2bm.putTensor(       1.0*t*cm, 4, 1 );
		mpo_2bp.putTensor(      -1.0*t*cp, 4, 2 );
		mpo_2bs.putTensor(           V*Ni, 4, 3 );
		mpo_2bs.putTensor(             id, 4, 4 );
		mpo_2bp.putTensor(             cp, 0, 5 );
		mpo_2bm.putTensor(             cm, 0,10 );
		mpo_2bs.putTensor(             Ni, 0,15 );

		mpo_2cs.putTensor(             id, 0, 0 );
		mpo_2cp.putTensor(             cp, 1, 0 );
		mpo_2cm.putTensor(             cm, 2, 0 );
		mpo_2cs.putTensor(             Ni, 3, 0 );
		mpo_2cm.putTensor(       1.0*t*cm, 4, 1 );
		mpo_2cp.putTensor(      -1.0*t*cp, 4, 2 );
		mpo_2cs.putTensor(           V*Ni, 4, 3 );
		mpo_2cs.putTensor(             id, 4, 4 );
		mpo_2cm.putTensor( 1.0*cct_imp*cm, 4, 5 );
		mpo_2cp.putTensor( -1.0*ct_imp*cp, 4,10 );
		mpo_2cs.putTensor(      cV_imp*Ni, 4,15 );
		mpo_2cs.putTensor(             id, 4,20 );

		mpo_3as.putTensor(             id, 0, 0 );
		mpo_3ap.putTensor(             cp, 4, 5 );
		mpo_3am.putTensor(             cm, 4,10 );
		mpo_3as.putTensor(             Ni, 4,15 );
		mpo_3ap.putTensor(             cp, 1,20 );
		mpo_3am.putTensor(             cm, 2,20 );
		mpo_3as.putTensor(             Ni, 3,20 );
		mpo_3am.putTensor(       1.0*t*cm, 4,21 );
		mpo_3ap.putTensor(      -1.0*t*cp, 4,22 );
		mpo_3as.putTensor(           V*Ni, 4,23 );
		mpo_3as.putTensor(             id, 4,24 );

		mpo_3bs.putTensor(             id, 0, 0 );
		mpo_3bp.putTensor(             cp, 1, 0 );
		mpo_3bm.putTensor(             cm, 2, 0 );
		mpo_3bs.putTensor(             Ni, 3, 0 );
		mpo_3bm.putTensor(       1.0*t*cm, 4, 1 );
		mpo_3bp.putTensor(      -1.0*t*cp, 4, 2 );
		mpo_3bs.putTensor(           V*Ni, 4, 3 );
		mpo_3bs.putTensor(             id, 4, 4 );
		mpo_3bm.putTensor( -1.0*ct_imp*cm, 4, 5 );	// -1* what in mpo_2c
		mpo_3bp.putTensor( 1.0*cct_imp*cp, 4,10 );	// -1* what in mpo_2c
		mpo_3bs.putTensor(      cV_imp*Ni, 4,15 );
		mpo_3bs.putTensor(             id, 4,20 );

		struct stat info;
		if ( stat( mpo_dir.c_str(), &info ) != 0 )
			mkdir( mpo_dir.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

		mpo_ls.launch().save( mpo_dir + "/mpo_ls" );
		mpo_ms.launch().save( mpo_dir + "/mpo_ms" );
		mpo_rs.launch().save( mpo_dir + "/mpo_rs" );
		mpo_lp.launch().save( mpo_dir + "/mpo_lp" );
		mpo_mp.launch().save( mpo_dir + "/mpo_mp" );
		mpo_rp.launch().save( mpo_dir + "/mpo_rp" );
		mpo_lm.launch().save( mpo_dir + "/mpo_lm" );
		mpo_mm.launch().save( mpo_dir + "/mpo_mm" );
		mpo_rm.launch().save( mpo_dir + "/mpo_rm" );

		mpo_1as.launch().save( mpo_dir + "/mpo_1as" );
		mpo_1ap.launch().save( mpo_dir + "/mpo_1ap" );
		mpo_1am.launch().save( mpo_dir + "/mpo_1am" );
		mpo_1bs.launch().save( mpo_dir + "/mpo_1bs" );
		mpo_1bp.launch().save( mpo_dir + "/mpo_1bp" );
		mpo_1bm.launch().save( mpo_dir + "/mpo_1bm" );
		mpo_1bs.launch().save( mpo_dir + "/mpo_1cs" );
		mpo_1bp.launch().save( mpo_dir + "/mpo_1cp" );
		mpo_1bm.launch().save( mpo_dir + "/mpo_1cm" );

		mpo_2as.launch().save( mpo_dir + "/mpo_2as" );
		mpo_2ap.launch().save( mpo_dir + "/mpo_2ap" );
		mpo_2am.launch().save( mpo_dir + "/mpo_2am" );
		mpo_2bs.launch().save( mpo_dir + "/mpo_2bs" );
		mpo_2bp.launch().save( mpo_dir + "/mpo_2bp" );
		mpo_2bm.launch().save( mpo_dir + "/mpo_2bm" );
		mpo_2cs.launch().save( mpo_dir + "/mpo_2cs" );
		mpo_2cp.launch().save( mpo_dir + "/mpo_2cp" );
		mpo_2cm.launch().save( mpo_dir + "/mpo_2cm" );

		// mpo_2c = mpo_3b
		// mpo_2b = mpo_3c
		mpo_3as.launch().save( mpo_dir + "/mpo_3as" );
		mpo_3ap.launch().save( mpo_dir + "/mpo_3ap" );
		mpo_3am.launch().save( mpo_dir + "/mpo_3am" );
		mpo_3bs.launch().save( mpo_dir + "/mpo_3bs" );
		mpo_3bp.launch().save( mpo_dir + "/mpo_3bp" );
		mpo_3bm.launch().save( mpo_dir + "/mpo_3bm" );
		mpo_2bs.launch().save( mpo_dir + "/mpo_3cs" );
		mpo_2bp.launch().save( mpo_dir + "/mpo_3cp" );
		mpo_2bm.launch().save( mpo_dir + "/mpo_3cm" );
	}

	else {
		errMsg(argv[0]);
		return 1;
	}

	return 0;
}
