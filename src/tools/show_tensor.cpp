#include <iostream>
#include <string>

#include <uni10.hpp>

//======================================

int main(int argc, char* argv[]){

	if (argc < 2) {
		std::cerr << "Usage: " << argv[0] << " [option] <filename of Tensor>" << std::endl;
		std::cerr << "Allowed options:" << std::endl;
		std::cerr << "-r  if the Tensor only has real part (UniTensor)" << std::endl;
		return 1;
	}

	if ( std::string(argv[1]) == "-r" ) {
		if (argc < 3) {
			std::cerr << "Usage: " << argv[0] << " -r <filename of UniTensor>" << std::endl;
			return 1;
		}
		else {	
			std::cout << uni10::UniTensor(argv[2]);
		}
	}
	else {
		std::cout << uni10::CUniTensor(argv[1]);
	}

	return 0;
}


