/*******************************************************************

Boundary Hamiltonian generator for Ising model H = Sum(-SzSz + h*Sx)

********************************************************************/

#include <iostream>
#include <uni10.hpp>

#include <mps.hpp>

//============================================

uni10::CUniTensor netContract( uni10::CNetwork net,
	uni10::CUniTensor T1, uni10::CUniTensor T2,
	uni10::CUniTensor T1d, uni10::CUniTensor T2d,
	uni10::CUniTensor op ) {

	net.putTensor( "T1", T1 );
	net.putTensor( "T2", T2 );
	net.putTensor( "op", op );
	net.putTensor( "T1dag", T1d );
	net.putTensor( "T2dag", T2d );

	return net.launch();
}

//============================================

uni10::CUniTensor netContract( uni10::CNetwork net,
	uni10::CUniTensor T1, uni10::CUniTensor T2, uni10::CUniTensor T3, uni10::CUniTensor T4,
	uni10::CUniTensor T1d, uni10::CUniTensor T2d, uni10::CUniTensor T3d, uni10::CUniTensor T4d,
	uni10::CUniTensor op ) {

	net.putTensor( "T1", T1 );
	net.putTensor( "T2", T2 );
	net.putTensor( "T3", T3 );
	net.putTensor( "T4", T4 );
	net.putTensor( "op", op );
	net.putTensor( "T1dag", T1d );
	net.putTensor( "T2dag", T2d );
	net.putTensor( "T3dag", T3d );
	net.putTensor( "T4dag", T4d );

	return net.launch();
}

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-w arg  inf mps Wavefunction (gamma and lambda tensors) folder arg" << std::endl;
	std::cerr << "-J arg  set J = arg" << std::endl;
	std::cerr << "-h arg  set h = arg" << std::endl;
	std::cerr << "-ite arg  set max Arnoldi iteration = arg" << std::endl;
	std::cerr << "-tol arg  set tolerance for GMRES/CG algorithm = arg" << std::endl;
	std::cerr << "-n arg  Network file folder arg" << std::endl;
	std::cerr << "-H arg  load Hamiltonian from file arg" << std::endl;
	std::cerr << "-df arg  Destination Folder arg" << std::endl;
	std::cerr << "-alt  ALTernative Hamiltonian. here means swap Sx and Sz in Hamiltonian" << std::endl;
	std::cerr << "-obc  make an OBC boundary instead of IBC (useful when inf state is prod state)" << std::endl;
	std::cerr << "-spin1  spin-1 Ising model" << std::endl;
}

//============================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian
	std::string ham_file = "";
	// wavefunction directory
	std::string wf_dir = "mps-inf";
	// Network file directory
	std::string net_dir = ".";
	// Hb destination directory
	std::string hb_dir = ".";
	// coupling strengths
	double J = -1.0;
	double h = 1.0;
	// max Arnoldi iteration
	int ar_iter = -1;
	// tolerance for GMRES/CG algorithm
	double tolerance = 1e-15;
	// swap Sx and Sz in Hamiltonian
	bool alter = false;
	// OBC
	bool obc = false;
	// spin-1 ?
	bool spin1 = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				ham_file = std::string(argv[i+1]);
			}
			else { // there was no argument to the -H option.
				std::cerr << "-H option requires the filename of the Hamiltonian." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			if (i + 1 < argc) {
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-n") {
			if (i + 1 < argc) {
				net_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc) {
				hb_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-J") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> J;
			}
			else {
				std::cerr << "-J option requires a float number of coupling strength." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-h") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> h;
			}
			else {
				std::cerr << "-h option requires a float number of external field." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> ar_iter;
			}
			else {
				std::cerr << "-ite option requires a positive integer number of Arnoldi iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> tolerance;
			}
			else {
				std::cerr << "-tol option requires a positive number of error tolerance for Hb solvers." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-alt") {
			alter = true;
		}
		else if (std::string(argv[i]) == "-obc") {
			obc = true;
		}
		else if (std::string(argv[i]) == "-spin1") {
			spin1 = true;
		}
	}

	std::vector<uni10::CUniTensor> gamma;
	std::vector<uni10::CUniTensor> lambda;

	gamma.push_back( uni10::CUniTensor(wf_dir + "/gamma_0") );
	gamma.push_back( uni10::CUniTensor(wf_dir + "/gamma_1") );
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_0") );
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_1") );

	uni10::CUniTensor A1 = netLG( lambda[0], gamma[0] );
	uni10::CUniTensor A2 = netLG( lambda[1], gamma[1] );
	uni10::CUniTensor B1 = netGL( gamma[1], lambda[0] );
	uni10::CUniTensor B2 = netGL( gamma[0], lambda[1] );
	uni10::CUniTensor A1d = dag(A1);
	uni10::CUniTensor A2d = dag(A2);
	uni10::CUniTensor B1d = dag(B1);
	uni10::CUniTensor B2d = dag(B2);

	// define operators
	uni10::CUniTensor sx = (spin1)? OP( "sx1" ) : OP( "sx" );
	uni10::CUniTensor sz = (spin1)? OP( "sz1" ) : OP( "sz" );
	uni10::CUniTensor id = (spin1)? OP( "id1" ) : OP( "id" );
	uni10::CUniTensor op1 = (alter)? sx : sz;
	uni10::CUniTensor op2 = (alter)? sz : sx;

	uni10::CUniTensor ham;
	if ( ham_file == "" )
		ham = J * uni10::otimes(op1, op1)
			+ (0.5) * h * ( uni10::otimes(op2, id) + uni10::otimes(id, op2) );
	else
		ham = uni10::CUniTensor( ham_file );

	// import network files
	uni10::CNetwork net_l1( net_dir + "/ibc_lb_op1.net" );
	uni10::CNetwork net_r1( net_dir + "/ibc_rb_op1.net" );
	uni10::CNetwork net_l2o( net_dir + "/ibc_lb_op2_odd.net" );
	uni10::CNetwork net_l2e( net_dir + "/ibc_lb_op2_even.net" );
	uni10::CNetwork net_r2o( net_dir + "/ibc_rb_op2_odd.net" );
	uni10::CNetwork net_r2e( net_dir + "/ibc_rb_op2_even.net" );

	// define E3 and E3r
	uni10::CUniTensor E3( lambda[0].bond(), "E3" );
	E3.identity();
	uni10::CUniTensor E3r = bondInv(E3);

	// define E2 and E2r
	uni10::CUniTensor E2 = netContract( net_l1, A1, A2, A1d, A2d, op1 );
	uni10::CUniTensor E2r = netContract( net_r1, B1, B2, B1d, B2d, op1 );

	// define Ex and Exr
	uni10::CUniTensor Ex = netContract( net_l1, A1, A2, A1d, A2d, op2 );
	uni10::CUniTensor Exr = netContract( net_r1, B1, B2, B1d, B2d, op2 );

	// define C
	uni10::CUniTensor C1 = netContract( net_l2o, A1, A2, A1d, A2d, ham );
	uni10::CUniTensor C2 = netContract( net_l2e, A1, A2, A1, A2, A1d, A2d, A1d, A2d, ham );
	uni10::CUniTensor C = C1 + C2;	// lvec

	// define Cr
	uni10::CUniTensor C1r = netContract( net_r2o, B1, B2, B1d, B2d, ham );
	uni10::CUniTensor C2r = netContract( net_r2e, B1, B2, B1, B2, B1d, B2d, B1d, B2d, ham );
	uni10::CUniTensor Cr = C1r + C2r;	// rvec

	// calculate e0
	uni10::CUniTensor traceRhoC = traceRhoVecQn( true, lambda[0], C );
	double e0 = traceRhoC[0].real();
	std::cout << "e0 = " << std::setprecision(10) << e0 << '\n';

	// find HL
	uni10::CUniTensor I2 = E3;
	uni10::CUniTensor rhs_l = C + (-1.0 * e0) * I2;
	uni10::CUniTensor trial = rhs_l;

	uni10::CUniTensor HL;
	if (obc) {
		HL = E3;
		HL.set_zero();
	}
	else {
		HL = findHbQn( true, netAA(A1, A2), lambda[0], trial, rhs_l, ar_iter, tolerance );
		HL.permute(1);
	}
	HL.setName("HL");
	HL.save( hb_dir + "/HL" );

	// find HR
	I2 = E3r;
	uni10::CUniTensor rhs_r = Cr + (-1.0 * e0) * I2;
	trial = bondInv(HL);

	uni10::CUniTensor HR;
	if (obc) {
		HR = E3r;
		HR.set_zero();
	}
	else {
		HR = findHbQn( false, netBB(B1, B2), lambda[0], trial, rhs_r, ar_iter, tolerance );
		HR.permute(1);
	}
	HR.setName("HR");
	HR.save( hb_dir + "/HR" );

	// define HLW
	uni10::CUniTensor HLW = (alter)?
		J * uni10::otimes( E2, sx )
			+ (0.5) * h * uni10::otimes( Ex, id )
			+ (0.5) * h * uni10::otimes( E3, sz ):
		J * uni10::otimes( E2, sz )
			+ (0.5) * h * uni10::otimes( Ex, id )
			+ (0.5) * h * uni10::otimes( E3, sx );
	HLW.save( hb_dir + "/HLW" );

	// define HWR
	uni10::CUniTensor HWR = (alter)?
		J * uni10::otimes( sx, E2r )
			+ (0.5) * h * uni10::otimes( id, Exr )
			+ (0.5) * h * uni10::otimes( sz, E3r ):
		J * uni10::otimes( sz, E2r )
			+ (0.5) * h * uni10::otimes( id, Exr )
			+ (0.5) * h * uni10::otimes( sx, E3r );
	HWR.save( hb_dir + "/HWR" );

	// output effective boundary one-site operators
	if (alter) {
		Ex.save( hb_dir + "/Sz_L" );
		E2.save( hb_dir + "/Sx_L" );
		E3.save( hb_dir + "/Id_L" );
		Exr.save( hb_dir + "/Sz_R" );
		E2r.save( hb_dir + "/Sx_R" );
		E3r.save( hb_dir + "/Id_R" );
	}
	else {
		Ex.save( hb_dir + "/Sx_L" );
		E2.save( hb_dir + "/Sz_L" );
		E3.save( hb_dir + "/Id_L" );
		Exr.save( hb_dir + "/Sx_R" );
		E2r.save( hb_dir + "/Sz_R" );
		E3r.save( hb_dir + "/Id_R" );
	}

	return 0;
}
