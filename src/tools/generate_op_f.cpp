#include <iostream>
#include <sstream>

#include <tns-func/tns_const.h>
#include <mps-qn/func_qn_op.h>

using namespace std;

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H hub arg arg  construct Hamiltonian of Hubbard model with t, V = args" << std::endl;
	std::cerr << "-N  construct N operator" << std::endl;
	std::cerr << "-cp  construct cp ( c^+ ) operator" << std::endl;
	std::cerr << "-cm  construct cm ( c ) operator" << std::endl;
	std::cerr << "-id  construct identity operator" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// construct operators
	if (std::string(argv[1]) == "-H") {

		if (std::string(argv[2]) == "hub") {

			double t = 1.0;
			double V = 1.0;

			if (argc > 4) {
				std::stringstream(argv[3]) >> t;
				std::stringstream(argv[4]) >> V;
			}

			uni10::CUniTensor Ni = opF( "N" ) + (-0.5) * opF( "id" );
			uni10::CUniTensor cp = opF( "cp" );
			uni10::CUniTensor cm = opF( "cm" );
			
			uni10::CUniTensor ham 
				= (-1.) * t * ( otimesPM(cp, cm) + otimesPM(cp, cm, true) )
				+ V * uni10::otimes(Ni, Ni);

			ham.save( "ham_hub" );
			std::cout << ham;
		}

		else { // Uh-oh, there was no argument to the -H option.
			std::cerr << "-H model Hamiltonian. Valid choices: hub" << std::endl;
			return 1;
		}
	}

	else if (std::string(argv[1]) == "-id") {

		uni10::CUniTensor op = opF( "id" );
		op.save( "id" );
		std::cout << op;	
	}

	else if (std::string(argv[1]) == "-N") {

		uni10::CUniTensor op = opF( "N" );
		op.save( "N" );
		std::cout << op;	
	}

	else if (std::string(argv[1]) == "-cp") {

		uni10::CUniTensor op = opF( "cp" );
		op.save( "cp" );
		std::cout << op;	
	}

	else if (std::string(argv[1]) == "-cm") {

		uni10::CUniTensor op = opF( "cm" );
		op.save( "cm" );
		std::cout << op;	
	}

	else {

		errMsg( argv[0] );
		return 1;
	}

	return 0;
}

