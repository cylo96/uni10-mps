#include <iostream>
#include <sstream>

#include <tns-func/func_convert.h>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " <state> <bond dimension>" << std::endl;
	std::cerr << "Available states:" << std::endl;
	std::cerr << "sz+  \"|1>\" state, spin 1/2" << std::endl;
	std::cerr << "sz-  \"|0>\" state, spin 1/2" << std::endl;
	std::cerr << "sx+  \"|+>\" state, spin 1/2" << std::endl;
	std::cerr << "sx-  \"|->\" state, spin 1/2" << std::endl;
	std::cerr << "sz1+  \"|1>\" state, spin 1" << std::endl;
	std::cerr << "sz10  \"|0>\" state, spin 1" << std::endl;
	std::cerr << "sz1-  \"|-1>\" state, spin 1" << std::endl;
	std::cerr << "rnd arg  randomized state. arg: physical dimension" << std::endl;	
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 3) {

		errMsg( argv[0] );
		return 1;
	}

	int d, D;	// physical dimension, bond dimension
	std::istringstream(argv[2]) >> D;
	
	double up[] = {1.0, 0.0};
	double dn[] = {0.0, 1.0};
	double ps[] = {1./sqrt(2), 1./sqrt(2)};
	double ms[] = {1./sqrt(2), -1./sqrt(2)};

	double up1[] = {1.0, 0.0, 0.0};
	double zo1[] = {0.0, 1.0, 0.0};
	double dn1[] = {0.0, 0.0, 1.0};

	uni10::UniTensor state;

	if (std::string(argv[1]) == "sz+") {
		std::vector<uni10::Bond> bd;
		bd.push_back( uni10::Bond(uni10::BD_IN, 2) );
		state.assign(bd);
		state.setElem(up);
	}
	else if (std::string(argv[1]) == "sz-") {
		std::vector<uni10::Bond> bd;
		bd.push_back( uni10::Bond(uni10::BD_IN, 2) );
		state.assign(bd);
		state.setElem(dn);
	}
	else if (std::string(argv[1]) == "sx+") {
		std::vector<uni10::Bond> bd;
		bd.push_back( uni10::Bond(uni10::BD_IN, 2) );
		state.assign(bd);
		state.setElem(ps);
	}
	else if (std::string(argv[1]) == "sx-") {
		std::vector<uni10::Bond> bd;
		bd.push_back( uni10::Bond(uni10::BD_IN, 2) );
		state.assign(bd);
		state.setElem(ms);
	}
	else if (std::string(argv[1]) == "sz1+") {
		std::vector<uni10::Bond> bd;
		bd.push_back( uni10::Bond(uni10::BD_IN, 3) );
		state.assign(bd);
		state.setElem(up1);
	}
	else if (std::string(argv[1]) == "sz10") {
		std::vector<uni10::Bond> bd;
		bd.push_back( uni10::Bond(uni10::BD_IN, 3) );
		state.assign(bd);
		state.setElem(zo1);
	}
	else if (std::string(argv[1]) == "sz1-") {
		std::vector<uni10::Bond> bd;
		bd.push_back( uni10::Bond(uni10::BD_IN, 3) );
		state.assign(bd);
		state.setElem(dn1);
	}
	else if (std::string(argv[1]) == "rnd") {
		std::istringstream(argv[2]) >> d;
		std::istringstream(argv[3]) >> D;
		std::vector<uni10::Bond> bd;
		bd.push_back( uni10::Bond(uni10::BD_IN, d) );
		state.assign(bd);
		state.randomize();
	}
	else {

		errMsg( argv[0] );
		return 1;
	}

	uni10::Matrix uni(D, D);
	uni.set_zero();
	uni.at(0, 0) = 1.0;

	std::vector< uni10::Bond > bdl;
	bdl.push_back( uni10::Bond( uni10::BD_IN, D ) );
	bdl.push_back( uni10::Bond( uni10::BD_OUT, D ) );
	uni10::UniTensor lambda( bdl );
	lambda.putBlock( uni );

	uni10::UniTensor gamma = uni10::otimes(lambda, state);

	uni10::CUniTensor gam = real2Complex( gamma );
	uni10::CUniTensor lam = real2Complex( lambda );

	gam.save("gamma");
	lam.save("lambda");

	return 0;
}

