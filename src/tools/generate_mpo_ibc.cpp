#include <iostream>
#include <sstream>
#include <sys/stat.h>

#include <tns-func/tns_const.h>
#include <tns-func/func_convert.h>
#include <tns-func/func_op.h>

using namespace std;

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H itf <J> <h> (-w <hb_dir>) (-imp <J_imp> <h_imp>)" << std::endl;
	std::cerr << "-H itf-alt <J> <h> (-w <hb_dir>) (-imp <J_imp> <h_imp>)" << std::endl;
	std::cerr << "-H itf-alt-exp <J> <h> (-w <hb_dir>) (-imp <J_imp> <h_imp>)" << std::endl;
	std::cerr << "-H itf1-alt <J> <h> (-w <hb_dir>) (-imp <J_imp> <h_imp>)" << std::endl;
	std::cerr << "-H xxz <J> <delta> (-w <hb_dir>) (-imp <J_imp> <delta_imp>) (-hz <hz>) (-alt)" << std::endl;
	std::cerr << "-H xxz1 <J> <delta> (-w <hb_dir>) (-imp <J_imp> <delta_imp>)" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 3) {

		errMsg( argv[0] );
		return 1;
	}

	// construct operators
	if (std::string(argv[1]) == "-H") {

		if (std::string(argv[2]) == "itf") { // Make sure we aren't at the end of argv!

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itf <J> <h>" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			double J_imp = -1.0;
			double h_imp = 1.0;
			bool imp = false;
			std::string hb_dir = ".";

			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}

			if (argc > 5) {
				for (int i = 5; i < argc; ++i) {

					if (std::string(argv[i]) == "-w") {
						if (i + 1 < argc) {
							hb_dir = std::string(argv[i+1]);
						}
						else {
							std::cerr << "Usage: -w <hb_dir>" << std::endl;
							return 1;
						}
					}
					else if (std::string(argv[i]) == "-imp") {
						if (i + 2 < argc) {
							std::stringstream(argv[i+1]) >> J_imp;
							std::stringstream(argv[i+2]) >> h_imp;
							imp = true;
						}
						else {
							std::cerr << "Usage: -imp <J_imp> <h_imp>" << std::endl;
							return 1;
						}
					}
				}
			}

			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor id = OP( "id" );

			uni10::CUniTensor hl = uni10::CUniTensor( hb_dir + "/HL" );
			uni10::CUniTensor sx_l = uni10::CUniTensor( hb_dir + "/Sx_L" );
			uni10::CUniTensor sz_l = uni10::CUniTensor( hb_dir + "/Sz_L" );
			uni10::CUniTensor id_l = uni10::CUniTensor( hb_dir + "/Id_L" );

			uni10::CUniTensor hr = uni10::CUniTensor( hb_dir + "/HR" );
			uni10::CUniTensor sx_r = uni10::CUniTensor( hb_dir + "/Sx_R" );
			uni10::CUniTensor sz_r = uni10::CUniTensor( hb_dir + "/Sz_R" );
			uni10::CUniTensor id_r = uni10::CUniTensor( hb_dir + "/Id_R" );

			MPO mpo_l(3, 'l');
			MPO mpo_m(3, 'm');
			MPO mpo_r(3, 'r');

			mpo_l.putTensor( hl + 0.5*h*sx_l, 0, 0 );
			mpo_l.putTensor( J*sz_l, 0, 1 );
			mpo_l.putTensor( id_l, 0, 2 );

			mpo_m.putTensor( id, 0, 0 );
			mpo_m.putTensor( sz, 1, 0 );
			mpo_m.putTensor( h*sx, 2, 0 );
			mpo_m.putTensor( J*sz, 2, 1 );
			mpo_m.putTensor( id, 2, 2 );

			mpo_r.putTensor( id_r, 0, 0 );
			mpo_r.putTensor( sz_r, 1, 0 );
			mpo_r.putTensor( 0.5*h*sx_r + hr, 2, 0 );

			struct stat info;
			std::string dirname = "mpo-ham-itf";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );

			if (imp) {
				MPO mpo_imp(3, 'm');
				mpo_imp.putTensor( id, 0, 0 );
				mpo_imp.putTensor( sz, 1, 0 );
				mpo_imp.putTensor( h_imp*sx, 2, 0 );
				mpo_imp.putTensor( J_imp*sz, 2, 1 );
				mpo_imp.putTensor( id, 2, 2 );

				mpo_imp.launch().save( dirname + "/mpo_imp" );
			}
		}

		else if (std::string(argv[2]) == "itf-alt") { // Make sure we aren't at the end of argv!

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itf-alt <J> <h>" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			double J_imp = -1.0;
			double h_imp = 1.0;
			bool imp = false;
			std::string hb_dir = ".";

			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}

			if (argc > 5) {
				for (int i = 5; i < argc; ++i) {

					if (std::string(argv[i]) == "-w") {
						if (i + 1 < argc) {
							hb_dir = std::string(argv[i+1]);
						}
						else {
							std::cerr << "Usage: -w <hb_dir>" << std::endl;
							return 1;
						}
					}
					else if (std::string(argv[i]) == "-imp") {
						if (i + 2 < argc) {
							std::stringstream(argv[i+1]) >> J_imp;
							std::stringstream(argv[i+2]) >> h_imp;
							imp = true;
						}
						else {
							std::cerr << "Usage: -imp <J_imp> <h_imp>" << std::endl;
							return 1;
						}
					}
				}
			}

			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor id = OP( "id" );

			uni10::CUniTensor hl = uni10::CUniTensor( hb_dir + "/HL" );
			uni10::CUniTensor sx_l = uni10::CUniTensor( hb_dir + "/Sx_L" );
			uni10::CUniTensor sz_l = uni10::CUniTensor( hb_dir + "/Sz_L" );
			uni10::CUniTensor id_l = uni10::CUniTensor( hb_dir + "/Id_L" );

			uni10::CUniTensor hr = uni10::CUniTensor( hb_dir + "/HR" );
			uni10::CUniTensor sx_r = uni10::CUniTensor( hb_dir + "/Sx_R" );
			uni10::CUniTensor sz_r = uni10::CUniTensor( hb_dir + "/Sz_R" );
			uni10::CUniTensor id_r = uni10::CUniTensor( hb_dir + "/Id_R" );

			MPO mpo_l(3, 'l');
			MPO mpo_m(3, 'm');
			MPO mpo_r(3, 'r');

			mpo_l.putTensor( hl + 0.5*h*sz_l, 0, 0 );
			mpo_l.putTensor( J*sx_l, 0, 1 );
			mpo_l.putTensor( id_l, 0, 2 );

			mpo_m.putTensor( id, 0, 0 );
			mpo_m.putTensor( sx, 1, 0 );
			mpo_m.putTensor( h*sz, 2, 0 );
			mpo_m.putTensor( J*sx, 2, 1 );
			mpo_m.putTensor( id, 2, 2 );

			mpo_r.putTensor( id_r, 0, 0 );
			mpo_r.putTensor( sx_r, 1, 0 );
			mpo_r.putTensor( 0.5*h*sz_r + hr, 2, 0 );

			struct stat info;
			std::string dirname = "mpo-ham-itf";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );

			if (imp) {
				MPO mpo_imp(3, 'm');
				mpo_imp.putTensor( id, 0, 0 );
				mpo_imp.putTensor( sx, 1, 0 );
				mpo_imp.putTensor( h_imp*sz, 2, 0 );
				mpo_imp.putTensor( J_imp*sx, 2, 1 );
				mpo_imp.putTensor( id, 2, 2 );

				mpo_imp.launch().save( dirname + "/mpo_imp" );
			}
		}

		else if (std::string(argv[2]) == "itf-alt-exp") { // Make sure we aren't at the end of argv!

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itf-alt-exp <J> <h>" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			double J_imp = -1.0;
			double h_imp = 1.0;
			bool imp = false;
			std::string hb_dir = ".";

			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}

			if (argc > 5) {
				for (int i = 5; i < argc; ++i) {

					if (std::string(argv[i]) == "-w") {
						if (i + 1 < argc) {
							hb_dir = std::string(argv[i+1]);
						}
						else {
							std::cerr << "Usage: -w <hb_dir>" << std::endl;
							return 1;
						}
					}
					else if (std::string(argv[i]) == "-imp") {
						if (i + 2 < argc) {
							std::stringstream(argv[i+1]) >> J_imp;
							std::stringstream(argv[i+2]) >> h_imp;
							imp = true;
						}
						else {
							std::cerr << "Usage: -imp <J_imp> <h_imp>" << std::endl;
							return 1;
						}
					}
				}
			}

			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor sy = OP( "sy" );
			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor id = OP( "id" );

			uni10::CUniTensor hl = uni10::CUniTensor( hb_dir + "/HL" );
			uni10::CUniTensor sx_l = uni10::CUniTensor( hb_dir + "/Sx_L" );
			uni10::CUniTensor sz_l = uni10::CUniTensor( hb_dir + "/Sz_L" );
			uni10::CUniTensor id_l = uni10::CUniTensor( hb_dir + "/Id_L" );

			uni10::CUniTensor hr = uni10::CUniTensor( hb_dir + "/HR" );
			uni10::CUniTensor sx_r = uni10::CUniTensor( hb_dir + "/Sx_R" );
			uni10::CUniTensor sz_r = uni10::CUniTensor( hb_dir + "/Sz_R" );
			uni10::CUniTensor id_r = uni10::CUniTensor( hb_dir + "/Id_R" );

			MPO mpo_l(5, 'l');
			MPO mpo_m(5, 'm');
			MPO mpo_r(5, 'r');

			mpo_l.putTensor( hl + 0.5*h*sz_l, 0, 0 );
			mpo_l.putTensor( J*sx_l, 0, 1 );
			mpo_l.putTensor( id_l, 0, 4 );

			mpo_m.putTensor( id, 0, 0 );
			mpo_m.putTensor( sx, 1, 0 );
			mpo_m.putTensor( h*sz, 4, 0 );
			mpo_m.putTensor( J*sx, 4, 1 );
			mpo_m.putTensor( id, 4, 4 );

			mpo_r.putTensor( id_r, 0, 0 );
			mpo_r.putTensor( sx_r, 1, 0 );
			mpo_r.putTensor( 0.5*h*sz_r + hr, 4, 0 );

			struct stat info;
			std::string dirname = "mpo-ham-itf";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );

			if (imp) {
				MPO mpo_imp(5, 'm');
				mpo_imp.putTensor( id, 0, 0 );
				mpo_imp.putTensor( sx, 1, 0 );
				mpo_imp.putTensor( sy, 2, 0 );
				mpo_imp.putTensor( sz, 3, 0 );
				mpo_imp.putTensor( h_imp*sz, 4, 0 );
				mpo_imp.putTensor( J_imp*sx, 4, 1 );
				mpo_imp.putTensor( id, 4, 4 );

				mpo_imp.launch().save( dirname + "/mpo_imp" );
			}
		}

		else if (std::string(argv[2]) == "itf1-alt") { // Make sure we aren't at the end of argv!

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itf1-alt <J> <h>" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			double J_imp = -1.0;
			double h_imp = 1.0;
			bool imp = false;
			std::string hb_dir = ".";

			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}

			if (argc > 5) {
				for (int i = 5; i < argc; ++i) {

					if (std::string(argv[i]) == "-w") {
						if (i + 1 < argc) {
							hb_dir = std::string(argv[i+1]);
						}
						else {
							std::cerr << "Usage: -w <hb_dir>" << std::endl;
							return 1;
						}
					}
					else if (std::string(argv[i]) == "-imp") {
						if (i + 2 < argc) {
							std::stringstream(argv[i+1]) >> J_imp;
							std::stringstream(argv[i+2]) >> h_imp;
							imp = true;
						}
						else {
							std::cerr << "Usage: -imp <J_imp> <h_imp>" << std::endl;
							return 1;
						}
					}
				}
			}

			uni10::CUniTensor sz = OP( "sz1" );
			uni10::CUniTensor sx = OP( "sx1" );
			uni10::CUniTensor id = OP( "id1" );

			uni10::CUniTensor hl = uni10::CUniTensor( hb_dir + "/HL" );
			uni10::CUniTensor sx_l = uni10::CUniTensor( hb_dir + "/Sx_L" );
			uni10::CUniTensor sz_l = uni10::CUniTensor( hb_dir + "/Sz_L" );
			uni10::CUniTensor id_l = uni10::CUniTensor( hb_dir + "/Id_L" );

			uni10::CUniTensor hr = uni10::CUniTensor( hb_dir + "/HR" );
			uni10::CUniTensor sx_r = uni10::CUniTensor( hb_dir + "/Sx_R" );
			uni10::CUniTensor sz_r = uni10::CUniTensor( hb_dir + "/Sz_R" );
			uni10::CUniTensor id_r = uni10::CUniTensor( hb_dir + "/Id_R" );

			MPO mpo_l(3, 'l');
			MPO mpo_m(3, 'm');
			MPO mpo_r(3, 'r');

			mpo_l.putTensor( hl + 0.5*h*sz_l, 0, 0 );
			mpo_l.putTensor( J*sx_l, 0, 1 );
			mpo_l.putTensor( id_l, 0, 2 );

			mpo_m.putTensor( id, 0, 0 );
			mpo_m.putTensor( sx, 1, 0 );
			mpo_m.putTensor( h*sz, 2, 0 );
			mpo_m.putTensor( J*sx, 2, 1 );
			mpo_m.putTensor( id, 2, 2 );

			mpo_r.putTensor( id_r, 0, 0 );
			mpo_r.putTensor( sx_r, 1, 0 );
			mpo_r.putTensor( 0.5*h*sz_r + hr, 2, 0 );

			struct stat info;
			std::string dirname = "mpo-ham-itf";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );

			if (imp) {
				MPO mpo_imp(3, 'm');
				mpo_imp.putTensor( id, 0, 0 );
				mpo_imp.putTensor( sx, 1, 0 );
				mpo_imp.putTensor( h_imp*sz, 2, 0 );
				mpo_imp.putTensor( J_imp*sx, 2, 1 );
				mpo_imp.putTensor( id, 2, 2 );

				mpo_imp.launch().save( dirname + "/mpo_imp" );
			}
		}

		else if (std::string(argv[2]) == "xxz") {

			double J = 1.0;
			double delta = 1.0;
			double J_imp = 1.0;
			double delta_imp = 1.0;
			double hz = 0.0;
			bool imp = false;
			std::string hb_dir = ".";
			bool alter = false;

			if (argc > 4) {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> delta;
			}

			if (argc > 5) {
				for (int i = 5; i < argc; ++i) {

					if (std::string(argv[i]) == "-w") {
						if (i + 1 < argc) {
							hb_dir = std::string(argv[i+1]);
						}
						else {
							std::cerr << "Usage: -w <hb_dir>" << std::endl;
							return 1;
						}
					}
					else if (std::string(argv[i]) == "-imp") {
						if (i + 2 < argc) {
							std::stringstream(argv[i+1]) >> J_imp;
							std::stringstream(argv[i+2]) >> delta_imp;
							imp = true;
						}
						else {
							std::cerr << "Usage: -imp <J_imp> <delta_imp>" << std::endl;
							return 1;
						}
					}
					else if (std::string(argv[i]) == "-hz") {
						if (i + 1 < argc) {
							std::stringstream(argv[i+1]) >> hz;
						}
						else {
							std::cerr << "Usage: -hz <hz>" << std::endl;
							return 1;
						}
					}
					else if (std::string(argv[i]) == "-alt") {
						alter = true;
					}
				}
			}

			if (!alter) {
				J *= 0.25;
				delta *= 0.25;
				J_imp *= 0.25;
				delta_imp *= 0.25;
				hz *= 0.5;
			}

			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor sy = OP( "sy" );
			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor id = OP( "id" );

			uni10::CUniTensor hl = uni10::CUniTensor( hb_dir + "/HL" );
			uni10::CUniTensor sx_l = uni10::CUniTensor( hb_dir + "/Sx_L" );
			uni10::CUniTensor sy_l = I * uni10::CUniTensor( hb_dir + "/_iSy_L" );
			uni10::CUniTensor sz_l = uni10::CUniTensor( hb_dir + "/Sz_L" );
			uni10::CUniTensor id_l = uni10::CUniTensor( hb_dir + "/Id_L" );

			uni10::CUniTensor hr = uni10::CUniTensor( hb_dir + "/HR" );
			uni10::CUniTensor sx_r = uni10::CUniTensor( hb_dir + "/Sx_R" );
			uni10::CUniTensor sy_r = I * uni10::CUniTensor( hb_dir + "/_iSy_R" );
			uni10::CUniTensor sz_r = uni10::CUniTensor( hb_dir + "/Sz_R" );
			uni10::CUniTensor id_r = uni10::CUniTensor( hb_dir + "/Id_R" );

			MPO mpo_l(5, 'l');
			MPO mpo_m(5, 'm');
			MPO mpo_r(5, 'r');

			mpo_l.putTensor( hl + hz*sz_l, 0, 0 );
			mpo_l.putTensor( J*sx_l, 0, 1 );
			mpo_l.putTensor( J*sy_l, 0, 2 );
			mpo_l.putTensor( delta*sz_l, 0, 3 );
			mpo_l.putTensor( id_l, 0, 4 );

			mpo_m.putTensor( id, 0, 0 );
			mpo_m.putTensor( sx, 1, 0 );
			mpo_m.putTensor( sy, 2, 0 );
			mpo_m.putTensor( sz, 3, 0 );
			mpo_m.putTensor( hz*sz, 4, 0 );
			mpo_m.putTensor( J*sx, 4, 1 );
			mpo_m.putTensor( J*sy, 4, 2 );
			mpo_m.putTensor( delta*sz, 4, 3 );
			mpo_m.putTensor( id, 4, 4 );

			mpo_r.putTensor( id_r, 0, 0 );
			mpo_r.putTensor( sx_r, 1, 0 );
			mpo_r.putTensor( sy_r, 2, 0 );
			mpo_r.putTensor( sz_r, 3, 0 );
			mpo_r.putTensor( hr + hz*sz_r, 4, 0 );

			struct stat info;
			std::string dirname = "mpo-ham-xxz";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );

			if (imp) {
				MPO mpo_imp(5, 'm');
				mpo_imp.putTensor( id, 0, 0 );
				mpo_imp.putTensor( sx, 1, 0 );
				mpo_imp.putTensor( sy, 2, 0 );
				mpo_imp.putTensor( sz, 3, 0 );
				mpo_imp.putTensor( hz*sz, 4, 0 );
				mpo_imp.putTensor( J_imp*sx, 4, 1 );
				mpo_imp.putTensor( J_imp*sy, 4, 2 );
				mpo_imp.putTensor( delta_imp*sz, 4, 3 );
				mpo_imp.putTensor( id, 4, 4 );

				mpo_imp.launch().save( dirname + "/mpo_imp" );
			}
		}

		else if (std::string(argv[2]) == "xxz1") {

			double J = 1.0;
			double delta = 1.0;
			double J_imp = 1.0;
			double delta_imp = 1.0;
			bool imp = false;
			std::string hb_dir = ".";

			if (argc > 4) {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> delta;
			}

			if (argc > 5) {
				for (int i = 5; i < argc; ++i) {

					if (std::string(argv[i]) == "-w") {
						if (i + 1 < argc) {
							hb_dir = std::string(argv[i+1]);
						}
						else {
							std::cerr << "Usage: -w <hb_dir>" << std::endl;
							return 1;
						}
					}
					else if (std::string(argv[i]) == "-imp") {
						if (i + 2 < argc) {
							std::stringstream(argv[i+1]) >> J_imp;
							std::stringstream(argv[i+2]) >> delta_imp;
							imp = true;
						}
						else {
							std::cerr << "Usage: -imp <J_imp> <delta_imp>" << std::endl;
							return 1;
						}
					}
				}
			}

			uni10::CUniTensor sx = OP( "sx1" );
			uni10::CUniTensor sy = OP( "sy1" );
			uni10::CUniTensor sz = OP( "sz1" );
			uni10::CUniTensor id = OP( "id1" );

			uni10::CUniTensor hl = uni10::CUniTensor( hb_dir + "/HL" );
			uni10::CUniTensor sx_l = uni10::CUniTensor( hb_dir + "/Sx_L" );
			uni10::CUniTensor sy_l = I * uni10::CUniTensor( hb_dir + "/_iSy_L" );
			uni10::CUniTensor sz_l = uni10::CUniTensor( hb_dir + "/Sz_L" );
			uni10::CUniTensor id_l = uni10::CUniTensor( hb_dir + "/Id_L" );

			uni10::CUniTensor hr = uni10::CUniTensor( hb_dir + "/HR" );
			uni10::CUniTensor sx_r = uni10::CUniTensor( hb_dir + "/Sx_R" );
			uni10::CUniTensor sy_r = I * uni10::CUniTensor( hb_dir + "/_iSy_R" );
			uni10::CUniTensor sz_r = uni10::CUniTensor( hb_dir + "/Sz_R" );
			uni10::CUniTensor id_r = uni10::CUniTensor( hb_dir + "/Id_R" );

			MPO mpo_l(5, 'l');
			MPO mpo_m(5, 'm');
			MPO mpo_r(5, 'r');

			mpo_l.putTensor( hl, 0, 0 );
			mpo_l.putTensor( J*sx_l, 0, 1 );
			mpo_l.putTensor( J*sy_l, 0, 2 );
			mpo_l.putTensor( delta*sz_l, 0, 3 );
			mpo_l.putTensor( id_l, 0, 4 );

			mpo_m.putTensor( id, 0, 0 );
			mpo_m.putTensor( sx, 1, 0 );
			mpo_m.putTensor( sy, 2, 0 );
			mpo_m.putTensor( sz, 3, 0 );
			mpo_m.putTensor( J*sx, 4, 1 );
			mpo_m.putTensor( J*sy, 4, 2 );
			mpo_m.putTensor( delta*sz, 4, 3 );
			mpo_m.putTensor( id, 4, 4 );

			mpo_r.putTensor( id_r, 0, 0 );
			mpo_r.putTensor( sx_r, 1, 0 );
			mpo_r.putTensor( sy_r, 2, 0 );
			mpo_r.putTensor( sz_r, 3, 0 );
			mpo_r.putTensor( hr, 4, 0 );

			struct stat info;
			std::string dirname = "mpo-ham-xxz";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_l.launch().save( dirname + "/mpo_l" );
			mpo_m.launch().save( dirname + "/mpo_m" );
			mpo_r.launch().save( dirname + "/mpo_r" );

			if (imp) {
				MPO mpo_imp(5, 'm');
				mpo_imp.putTensor( id, 0, 0 );
				mpo_imp.putTensor( sx, 1, 0 );
				mpo_imp.putTensor( sy, 2, 0 );
				mpo_imp.putTensor( sz, 3, 0 );
				mpo_imp.putTensor( J_imp*sx, 4, 1 );
				mpo_imp.putTensor( J_imp*sy, 4, 2 );
				mpo_imp.putTensor( delta_imp*sz, 4, 3 );
				mpo_imp.putTensor( id, 4, 4 );

				mpo_imp.launch().save( dirname + "/mpo_imp" );
			}
		}

		else {
			errMsg( argv[0] );
			return 1;
		}
	}

	else {
		errMsg( argv[0] );
		return 1;
	}

	return 0;
}
