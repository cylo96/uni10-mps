#include <iostream>
#include <sstream>

#include <tns-func/tns_const.h>
#include <tns-func/func_convert.h>
#include <tns-func/func_op.h>

using namespace std;

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H itf [Jz] [hx]  construct Hamiltonian of spin 1/2 Ising model with args Jz, hx" << std::endl;
	std::cerr << "-H itf-alt [Jx] [hz]  construct Hamiltonian of spin 1/2 Ising model with args Jx, hz" << std::endl;
	std::cerr << "-H itf1-alt [Jx] [hz]  construct Hamiltonian of spin 1 Ising model with args Jx, hz" << std::endl;
	std::cerr << "-H xxz [J] [Jz]  construct Hamiltonian of spin 1/2 xxz model with args J, Jz" << std::endl;
	std::cerr << "-H xxz-alt [J] [Jz]  construct Hamiltonian of spin 1/2 xxz model with args J, Jz" << std::endl;
	std::cerr << "-H hsb1  construct Hamiltonian of spin-1 Heisenberg model" << std::endl;
	std::cerr << "-H fp2010 [Bx] [Uzz] ([Bz] [Uxy])  construct Hamiltonian of PRB 81, 064439" << std::endl;
	std::cerr << "-sx  construct Sx (sigma x) operator" << std::endl;
	std::cerr << "-sz  construct Sz (sigma z) operator" << std::endl;
	std::cerr << "-id  construct identity operator" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// construct operators
	if (std::string(argv[1]) == "-H") {

		if (std::string(argv[2]) == "itf") { // Make sure we aren't at the end of argv!

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itf [Jz] [hx]" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}

			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor id = OP( "id" );

			uni10::CUniTensor ham
				= J * uni10::otimes(sz, sz)
				+ (0.5) * h * ( uni10::otimes(sx, id) + uni10::otimes(id, sx) );

			ham.save("ham_itf");
			std::cout << ham;
		}

		else if (std::string(argv[2]) == "itfb") { // Make sure we aren't at the end of argv!

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itfb [Jz] [hx]" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}

			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor id = OP( "id" );

			uni10::CUniTensor ham
				= J * uni10::otimes(sz, sz)
				+ (1.0) * h * uni10::otimes(sx, id) + (0.5) * h * uni10::otimes(id, sx);

			ham.save("ham_itfb");
			std::cout << ham;
		}

		else if (std::string(argv[2]) == "itfbr") { // Make sure we aren't at the end of argv!

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itfbr [Jz] [hx]" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}

			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor id = OP( "id" );

			uni10::CUniTensor ham
				= J * uni10::otimes(sz, sz)
				+ (0.5) * h * uni10::otimes(sx, id) + (1.0) * h * uni10::otimes(id, sx);

			ham.save("ham_itfbr");
			std::cout << ham;
		}

		else if (std::string(argv[2]) == "itfdrv") { // Make sure we aren't at the end of argv!

			if (argc < 5) {
				std::cerr << "Usage: " << argv[0] << " -H itfdrv [J] [hx] [hz]" << std::endl;
				return 1;
			}

			double J = -1.0;
			double hx = 1.0;
			double hz = 0.0;
			if (argc == 5) {
				std::stringstream(argv[3]) >> hx;
				std::stringstream(argv[4]) >> hz;
			}
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> hx;
				std::stringstream(argv[5]) >> hz;
			}

			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor id = OP( "id" );

			uni10::CUniTensor ham
				= J * uni10::otimes(sz, sz)
				+ (-0.5) * hx * ( uni10::otimes(sx, id) + uni10::otimes(id, sx) )
				+ (-0.5) * I * hz * ( uni10::otimes(sz, id) + uni10::otimes(id, sz) );

			ham.save("ham_itf");
			std::cout << ham;
		}

		else if (std::string(argv[2]) == "itf-alt") { // Make sure we aren't at the end of argv!

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itf-alt [Jx] [hz]" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}

			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor id = OP( "id" );

			uni10::CUniTensor ham
				= J * uni10::otimes(sx, sx)
				+ (0.5) * h * ( uni10::otimes(sz, id) + uni10::otimes(id, sz) );

			ham.save("ham_itf");
			std::cout << ham;
		}

		else if (std::string(argv[2]) == "itf-alt-intf") { // Make sure we aren't at the end of argv!

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itf-alt-intf [Jx1] [hz1] [hz2]" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			double h2 = h;
			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else if (argc == 5) {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
				std::stringstream(argv[5]) >> h2;
			}

			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor sx = OP( "sx" );
			uni10::CUniTensor id = OP( "id" );

			uni10::CUniTensor ham
				= J * uni10::otimes(sx, sx)
				+ 0.5 * h * uni10::otimes(sz, id)
				+ 0.5 * h2 * uni10::otimes(id, sz);

			ham.save("ham_itf");
			std::cout << ham;
		}

		else if (std::string(argv[2]) == "itf1-alt") { // Make sure we aren't at the end of argv!

			if (argc < 4) {
				std::cerr << "Usage: " << argv[0] << " -H itf1-alt [Jx] [hz]" << std::endl;
				return 1;
			}

			double J = -1.0;
			double h = 1.0;	// transverse field
			if (argc == 4)
				std::stringstream(argv[3]) >> h;
			else {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> h;
			}

			uni10::CUniTensor sz = OP( "sz1" );
			uni10::CUniTensor sx = OP( "sx1" );
			uni10::CUniTensor id = OP( "id1" );

			uni10::CUniTensor ham
				= J * uni10::otimes(sx, sx)
				+ (0.5) * h * ( uni10::otimes(sz, id) + uni10::otimes(id, sz) );

			ham.save("ham_itf");
			std::cout << ham;
		}

		else if (std::string(argv[2]) == "xxz") {

			double J = 1.0;
			double delta = 1.0;
			double hz = 0.0;	// perturbation to break symm

			if (argc > 4) {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> delta;
			}
			if (argc > 5) {
				std::stringstream(argv[5]) >> hz;
			}

			uni10::CUniTensor sp = OP( "sp" );
			uni10::CUniTensor sm = OP( "sm" );
			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor id = OP( "id" );

			uni10::CUniTensor ham
				= J * 0.5 * ( uni10::otimes(sp, sm) + uni10::otimes(sm, sp) )
				+ delta * 0.25 * uni10::otimes(sz, sz)
				+ hz * 0.25 * ( uni10::otimes(id, sz) + uni10::otimes(sz, id) );

			ham.save("ham_xxz");
			std::cout << ham;
		}

		else if (std::string(argv[2]) == "xxz-alt") {

			double J = 1.0;
			double delta = 1.0;
			double hz = 0.0;	// perturbation to break symm

			if (argc > 4) {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> delta;
			}
			if (argc > 5) {
				std::stringstream(argv[5]) >> hz;
			}

			uni10::CUniTensor sp = OP( "sp" );
			uni10::CUniTensor sm = OP( "sm" );
			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor id = OP( "id" );

			uni10::CUniTensor ham
				= J * 2. * ( uni10::otimes(sp, sm) + uni10::otimes(sm, sp) )
				+ delta * uni10::otimes(sz, sz)
				+ hz * 0.5 * ( uni10::otimes(id, sz) + uni10::otimes(sz, id) );

			ham.save("ham_xxz");
			std::cout << ham;
		}

		else if (std::string(argv[2]) == "xxz-hz") {

			double J = 1.0;
			double delta = 1.0;
			double hz1 = 0.0;	// perturbation to break symm
			double hz2 = 0.0;	// perturbation to break symm

			if (argc > 4) {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> delta;
			}
			if (argc > 6) {
				std::stringstream(argv[5]) >> hz1;
				std::stringstream(argv[6]) >> hz2;
			}

			uni10::CUniTensor sp = OP( "sp" );
			uni10::CUniTensor sm = OP( "sm" );
			uni10::CUniTensor sz = OP( "sz" );
			uni10::CUniTensor id = OP( "id" );

			uni10::CUniTensor ham
				= J * 0.5 * ( uni10::otimes(sp, sm) + uni10::otimes(sm, sp) )
				+ delta * 0.25 * uni10::otimes(sz, sz)
				+ hz1 * 0.25 * uni10::otimes(sz, id)
				+ hz2 * 0.25 * uni10::otimes(id, sz);

			ham.save("ham_xxz");
			std::cout << ham;
		}

		else if (std::string(argv[2]) == "hsb1") {

			uni10::CUniTensor sp1 = OP( "sp1" );
			uni10::CUniTensor sm1 = OP( "sm1" );
			uni10::CUniTensor sz1 = OP( "sz1" );

			double del;
			std::stringstream(argv[3]) >> del;
			uni10::CUniTensor ham
				= 0.5 * ( uni10::otimes(sp1, sm1) + uni10::otimes(sm1, sp1) )
				+ del * uni10::otimes(sz1, sz1);

			ham.save("ham_hsb1");
			std::cout << ham;
		}

		else if (std::string(argv[2]) == "fp2010") {

			if (argc < 5) {
				std::cerr << "Usage: " << argv[0] << " -H fp2010 [Bx] [Uzz] [Bz] [Uxy]" << std::endl;
				return 1;
			}

			double Bx, Uzz;
			std::stringstream(argv[3]) >> Bx;
			std::stringstream(argv[4]) >> Uzz;

			double Bz = 0.0;
			double Uxy = 0.0;
			if (argc > 6) {
				std::stringstream(argv[5]) >> Bz;
				std::stringstream(argv[5]) >> Uxy;
			}

			uni10::CUniTensor sp1 = OP( "sp1" );
			uni10::CUniTensor sm1 = OP( "sm1" );
			uni10::CUniTensor sz1 = OP( "sz1" );
			uni10::CUniTensor sx1 = OP( "sx1" );
			uni10::CUniTensor id1 = OP( "id1" );
			uni10::CUniTensor szz1 = OP( "szz1" );

			uni10::CUniTensor ham
				= 0.5 * ( uni10::otimes(sp1, sm1) + uni10::otimes(sm1, sp1) )
				+ uni10::otimes(sz1, sz1)
				+ 0.5 * Bx * (uni10::otimes(sx1, id1) + uni10::otimes(id1, sx1))
				+ 0.5 * Uzz * (uni10::otimes(szz1, id1) + uni10::otimes(id1, szz1))
				+ 0.5 * Bz * (uni10::otimes(sz1, id1) + uni10::otimes(id1, sz1))
				+ 0.5 * Uxy * (uni10::otimes(sp1, sm1) + uni10::otimes(sm1, sp1));

			ham.save("ham_fp2010");
			std::cout << ham;
		}

		else { // Uh-oh, there was no argument to the -H option.
			std::cerr << "-H model Hamiltonian. Valid choices: itf, xxz, hsb1, fp2010" << std::endl;
			return 1;
		}
	}

	else if (std::string(argv[1]) == "-id") {

		uni10::CUniTensor op = OP( "id" );
		op.save( "id" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-sz") {

		uni10::CUniTensor op = OP( "sz" );
		op.save( "sz" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-sx") {

		uni10::CUniTensor op = OP( "sx" );
		op.save( "sx" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-sigma") {

		uni10::CUniTensor op = OP( "sigma" );
		op.save( "sigma" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "--isy") {

		uni10::CUniTensor op = OP( "-isy" );
		op.save( "_isy" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-sy") {

		uni10::CUniTensor op = OP( "sy" );
		op.save( "sy" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-sm") {

		uni10::CUniTensor op = OP( "sm" );
		op.save( "sm" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-sp") {

		uni10::CUniTensor op = OP( "sp" );
		op.save( "sp" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-id1") {

		uni10::CUniTensor op = OP( "id1" );
		op.save( "id1" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-sz1") {

		uni10::CUniTensor op = OP( "sz1" );
		op.save( "sz1" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-sx1") {

		uni10::CUniTensor op = OP( "sx1" );
		op.save( "sx1" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-sy1") {

		uni10::CUniTensor op = OP( "sy1" );
		op.save( "sy1" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "--isy1") {

		uni10::CUniTensor op = OP( "-isy1" );
		op.save( "_isy1" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-sxx1") {

		uni10::CUniTensor op = OP( "sxx1" );
		op.save( "sxx1" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-sp1") {

		uni10::CUniTensor op = OP( "sp1" );
		op.save( "sp1" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-sm1") {

		uni10::CUniTensor op = OP( "sm1" );
		op.save( "sm1" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-long_sigma") {

		int x;
		std::istringstream(argv[2]) >> x;

		uni10::CUniTensor sigma = OP( "-isy" );

		std::vector< uni10::Bond > bdi;
		bdi.push_back( uni10::Bond(uni10::BD_IN, x/2) );
		bdi.push_back( uni10::Bond(uni10::BD_OUT, x/2) );
		uni10::CUniTensor id( bdi );
		id.identity();

		uni10::CUniTensor op = uni10::otimes( id, sigma );
		std::vector<int> lab1;
		lab1.push_back(0);
		lab1.push_back(1);
		std::vector<int> lab2;
		lab2.push_back(2);
		lab2.push_back(3);

		op.combineBond(lab1);
		op.combineBond(lab2);

		op.save("long_sigma");
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-Jr") {

		uni10::CUniTensor op
			= I * ( uni10::otimes( OP("sm"), OP("sp") )
			+ (-1.0) * uni10::otimes( OP("sp"), OP("sm") ) );

		op.save( "Jr" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-Jl") {

		uni10::CUniTensor op
			= I * ( uni10::otimes( OP("sp"), OP("sm") )
			+ (-1.0) * uni10::otimes( OP("sm"), OP("sp") ) );

		op.save( "Jl" );
		std::cout << op;
	}

	else {

		errMsg( argv[0] );
		return 1;
	}

	return 0;
}
