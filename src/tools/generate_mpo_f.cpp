#include <iostream>
#include <sstream>
#include <fstream>
#include <sys/stat.h>

#include <tns-func/func_op.h>
#include <mps-qn/func_qn_op.h>

using namespace std;

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H hub arg arg  construct Hamiltonian of Hubbard model with t, V = args" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// construct operators
	if (std::string(argv[1]) == "-H") {

		if (std::string(argv[2]) == "hub") {

			double t = 1.0;
			double V = 1.0;

			if (argc > 4) {
				std::stringstream(argv[3]) >> t;
				std::stringstream(argv[4]) >> V;
			}

			uni10::CUniTensor Ni = opF( "N" ) + (-0.5) * opF( "id" );
			uni10::CUniTensor cp = opF( "cp" );
			uni10::CUniTensor cm = opF( "cm" );
			uni10::CUniTensor id = opF( "id" );

			// symmetric blocks
			MPO mpo_ls(5, 'l');
			MPO mpo_ms(5, 'm');
			MPO mpo_rs(5, 'r');

			// raising blocks
			MPO mpo_lp(5, 'l');
			MPO mpo_mp(5, 'm');
			MPO mpo_rp(5, 'r');

			// lowering blocks
			MPO mpo_lm(5, 'l');
			MPO mpo_mm(5, 'm');
			MPO mpo_rm(5, 'r');

			mpo_lm.putTensor(  1.0*t*cm, 0, 1 );
			mpo_lp.putTensor( -1.0*t*cp, 0, 2 );
			mpo_ls.putTensor(      V*Ni, 0, 3 );
			mpo_ls.putTensor(        id, 0, 4 );

			mpo_ms.putTensor(        id, 0, 0 );
			mpo_mp.putTensor(        cp, 1, 0 );
			mpo_mm.putTensor(        cm, 2, 0 );
			mpo_ms.putTensor(        Ni, 3, 0 );
			mpo_mm.putTensor(  1.0*t*cm, 4, 1 );
			mpo_mp.putTensor( -1.0*t*cp, 4, 2 );
			mpo_ms.putTensor(      V*Ni, 4, 3 );
			mpo_ms.putTensor(        id, 4, 4 );

			mpo_rs.putTensor(        id, 0, 0 );
			mpo_rp.putTensor(        cp, 1, 0 );
			mpo_rm.putTensor(        cm, 2, 0 );
			mpo_rs.putTensor(        Ni, 3, 0 );

			struct stat info;
			std::string dirname = "mpo-ham-hub";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_ls.launch().save( dirname + "/mpo_ls" );
			mpo_ms.launch().save( dirname + "/mpo_ms" );
			mpo_rs.launch().save( dirname + "/mpo_rs" );
			mpo_lp.launch().save( dirname + "/mpo_lp" );
			mpo_mp.launch().save( dirname + "/mpo_mp" );
			mpo_rp.launch().save( dirname + "/mpo_rp" );
			mpo_lm.launch().save( dirname + "/mpo_lm" );
			mpo_mm.launch().save( dirname + "/mpo_mm" );
			mpo_rm.launch().save( dirname + "/mpo_rm" );
		}

		else {
			std::cerr << "-H model Hamiltonian. Models supported: hub" << std::endl;
			return 1;
		}
	}

	else {

		errMsg( argv[0] );
		return 1;
	}

	return 0;
}
