/**************************************************************************

Effective Infinite Boundary Condition for spin-1/2 XXZ model

Ref: PHYSICAL REVIEW B 86, 245107 (2012)
Uni10 library: http://uni10.org/

@author: cylo
@affiliation: NTHU Physics

***************************************************************************/

#include <iostream>
#include <sstream>
#include <uni10.hpp>

#include <mps.hpp>

//============================================

uni10::CUniTensor netContract( uni10::CNetwork net,
	uni10::CUniTensor T1, uni10::CUniTensor T2,
	uni10::CUniTensor T1d, uni10::CUniTensor T2d,
	uni10::CUniTensor op ) {

	net.putTensor( "T1", T1 );
	net.putTensor( "T2", T2 );
	net.putTensor( "op", op );
	net.putTensor( "T1dag", T1d );
	net.putTensor( "T2dag", T2d );

	return net.launch();
}

//============================================

uni10::CUniTensor netContract( uni10::CNetwork net,
	uni10::CUniTensor T1, uni10::CUniTensor T2, uni10::CUniTensor T3, uni10::CUniTensor T4,
	uni10::CUniTensor T1d, uni10::CUniTensor T2d, uni10::CUniTensor T3d, uni10::CUniTensor T4d,
	uni10::CUniTensor op ) {

	net.putTensor( "T1", T1 );
	net.putTensor( "T2", T2 );
	net.putTensor( "T3", T3 );
	net.putTensor( "T4", T4 );
	net.putTensor( "op", op );
	net.putTensor( "T1dag", T1d );
	net.putTensor( "T2dag", T2d );
	net.putTensor( "T3dag", T3d );
	net.putTensor( "T4dag", T4d );

	return net.launch();
}

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-w arg  inf mps Wavefunction (gamma and lambda tensors) folder arg" << std::endl;
	std::cerr << "-J arg  set J = arg" << std::endl;
	std::cerr << "-Jz arg  set Jz = arg" << std::endl;
	std::cerr << "-hz arg  set hz = arg" << std::endl;
	std::cerr << "-ite arg  set max Arnoldi iteration = arg" << std::endl;
	std::cerr << "-tol arg  set tolerance for GMRES/CG algorithm = arg" << std::endl;
	std::cerr << "-n arg  Network file folder arg" << std::endl;
	std::cerr << "-H arg  load Hamiltonian from file arg" << std::endl;
	std::cerr << "-df arg  Destination Folder arg" << std::endl;
	std::cerr << "-alt  ALTernative Hamiltonian. here means coupling str. based on pauli mat. instead of spin-1/2 operators" << std::endl;
	std::cerr << "-spin1  spin-1 XXZ model" << std::endl;
	std::cerr << "-obc  make an OBC boundary instead of IBC (useful when inf state is prod state)" << std::endl;
}

//============================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian
	std::string ham_file = "";
	// wavefunction directory
	std::string wf_dir = "mps-inf";
	// Network file directory
	std::string net_dir = ".";
	// Hb destination directory
	std::string hb_dir = ".";
	// coupling strengths
	double J = 1.0;
	double Jz = 1.0;
	double hz = 0.0;
	// max Arnoldi iteration
	int ar_iter = -1;
	// tolerance for GMRES/CG algorithm
	double tolerance = 1e-15;
	// xxz-alt: coupling constants based on pauli matrices
	bool alter = false;
	// spin-1 ?
	bool spin1 = false;
	// OBC
	bool obc = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				ham_file = std::string(argv[i+1]);
			}
			else { // there was no argument to the -H option.
				std::cerr << "-H option requires the filename of the Hamiltonian." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			if (i + 1 < argc) {
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-n") {
			if (i + 1 < argc) {
				net_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc) {
				hb_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-J") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> J;
			}
			else {
				std::cerr << "-J option requires a float number of coupling strength." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-Jz") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> Jz;
			}
			else {
				std::cerr << "-Jz option requires a float number of coupling strength." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-hz") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> hz;
			}
			else {
				std::cerr << "-hz option requires a float number of external field." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> ar_iter;
			}
			else {
				std::cerr << "-ite option requires a positive integer number of Arnoldi iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> tolerance;
			}
			else {
				std::cerr << "-tol option requires a positive number of error tolerance for Hb solvers." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-alt") {
			alter = true;
		}
		else if (std::string(argv[i]) == "-spin1") {
			spin1 = true;
		}
		else if (std::string(argv[i]) == "-obc") {
			obc = true;
		}
	}

	/// main function body
	// import imps
	std::vector<uni10::CUniTensor> gamma;
	std::vector<uni10::CUniTensor> lambda;
	gamma.push_back( uni10::CUniTensor( wf_dir + "/gamma_0" ) );
	gamma.push_back( uni10::CUniTensor( wf_dir + "/gamma_1" ) );
	lambda.push_back( uni10::CUniTensor( wf_dir + "/lambda_0" ) );
	lambda.push_back( uni10::CUniTensor( wf_dir + "/lambda_1" ) );

	uni10::CUniTensor A1 = netLG( lambda[0], gamma[0] );
	uni10::CUniTensor A2 = netLG( lambda[1], gamma[1] );
	uni10::CUniTensor B1 = netGL( gamma[1], lambda[0] );
	uni10::CUniTensor B2 = netGL( gamma[0], lambda[1] );
	uni10::CUniTensor A1d = dag(A1);
	uni10::CUniTensor A2d = dag(A2);
	uni10::CUniTensor B1d = dag(B1);
	uni10::CUniTensor B2d = dag(B2);

	// define operators
	uni10::CUniTensor sx = (spin1)? OP( "sx1" ) : OP( "sx" );
	uni10::CUniTensor sy = (spin1)? OP( "sy1" ) : OP( "sy" );
	uni10::CUniTensor sz = (spin1)? OP( "sz1" ) : OP( "sz" );
	uni10::CUniTensor Id = (spin1)? OP( "id1" ) : OP( "id" );

	uni10::CUniTensor ham;
	if ( ham_file == "" )
			ham = (alter || spin1)?
				J * ( uni10::otimes(sx, sx) + uni10::otimes(sy, sy) ) + Jz * uni10::otimes(sz, sz):
				0.25 * ( J * ( uni10::otimes(sx, sx) + uni10::otimes(sy, sy) ) + Jz * uni10::otimes(sz, sz) );
	else
		ham = uni10::CUniTensor( ham_file );

	uni10::CUniTensor hml, hmr;
	if (hz != 0.0) {
		double fct = (alter || spin1)? 1.0: 0.5;
		hml = ham + fct * hz * uni10::otimes(sz, Id);
		hmr = ham + fct * hz * uni10::otimes(Id, sz);
	}
	else {
		hml = ham;
		hmr = ham;
	}

	// import network files
	uni10::CNetwork net_l1( net_dir + "/ibc_lb_op1.net" );
	uni10::CNetwork net_r1( net_dir + "/ibc_rb_op1.net" );
	uni10::CNetwork net_l2o( net_dir + "/ibc_lb_op2_odd.net" );
	uni10::CNetwork net_l2e( net_dir + "/ibc_lb_op2_even.net" );
	uni10::CNetwork net_r2o( net_dir + "/ibc_rb_op2_odd.net" );
	uni10::CNetwork net_r2e( net_dir + "/ibc_rb_op2_even.net" );

	// define E5 and E5r
	uni10::CUniTensor E5( lambda[0].bond(), "E5" );
	E5.identity();
	uni10::CUniTensor E5r = bondInv(E5);

	// define E4 and E4r
	uni10::CUniTensor E4 = netContract( net_l1, A1, A2, A1d, A2d, sz );
	uni10::CUniTensor E4r = netContract( net_r1, B1, B2, B1d, B2d, sz );

	// define E3 and E3r
	uni10::CUniTensor E3 = netContract( net_l1, A1, A2, A1d, A2d, sy );
	uni10::CUniTensor E3r = netContract( net_r1, B1, B2, B1d, B2d, sy );

	// define E2 and E2r
	uni10::CUniTensor E2 = netContract( net_l1, A1, A2, A1d, A2d, sx );
	uni10::CUniTensor E2r = netContract( net_r1, B1, B2, B1d, B2d, sx );

	// define C = Tsx_E2 + Tsy_E3 + Tsz_E4;
	uni10::CUniTensor C1 = netContract( net_l2o, A1, A2, A1d, A2d, hml );
	uni10::CUniTensor C2 = netContract( net_l2e, A1, A2, A1, A2, A1d, A2d, A1d, A2d, hml );
	uni10::CUniTensor C = C1 + C2;	// lvec

	// define Cr
	uni10::CUniTensor C1r = netContract( net_r2o, B1, B2, B1d, B2d, hmr );
	uni10::CUniTensor C2r = netContract( net_r2e, B1, B2, B1, B2, B1d, B2d, B1d, B2d, hmr );
	uni10::CUniTensor Cr = C1r + C2r;	// rvec

	// calculate e0
	uni10::CUniTensor traceRhoC = traceRhoVecQn( true, lambda[0], C );
	double e0 = traceRhoC[0].real();
	std::cout << "e0 = " << std::setprecision(10) << e0 << '\n';

	// find HL
	uni10::CUniTensor I2 = E5;
	uni10::CUniTensor rhs_l = C + (-1.0 * e0) * I2;
	uni10::CUniTensor trial = rhs_l;

	uni10::CUniTensor HL;
	if (obc) {
		HL = E5;
		HL.set_zero();
		E2.set_zero();
	}
	else {
		HL = findHbQn( true, netAA(A1, A2), lambda[0], trial, rhs_l, ar_iter, tolerance );
		HL.permute(1);
	}
	HL.setName("HL");
	HL.save( hb_dir + "/HL" );

	// find HR
	traceRhoC = traceRhoVecQn( false, lambda[0], Cr );
	e0 = traceRhoC[0].real();
	std::cout << "e0 = " << std::setprecision(10) << e0 << '\n';
	I2 = E5r;
	uni10::CUniTensor rhs_r = Cr + (-1.0 * e0) * I2;
	trial = bondInv(HL);

	uni10::CUniTensor HR;
	if (obc) {
		HR = E5r;
		HR.set_zero();
		E2r.set_zero();
	}
	else {
		HR = findHbQn( false, netBB(B1, B2), lambda[0], trial, rhs_r, ar_iter, tolerance );
		HR.permute(1);
	}
	HR.setName("HR");
	HR.save( hb_dir + "/HR" );

	// define HLW
	uni10::CUniTensor HLW = (alter || spin1)?
		J * ( uni10::otimes( E2, sx ) + uni10::otimes( E3, sy ) )
			+ Jz * uni10::otimes( E4, sz )
			+ hz * uni10::otimes( E4, Id ):
		0.25 * ( J * ( otimes( E2, sx ) + otimes( E3, sy ) )
			+ Jz * uni10::otimes( E4, sz ) )
			+ 0.5 * hz * uni10::otimes( E4, Id );
	HLW.save( hb_dir + "/HLW" );

	// define HWR
	uni10::CUniTensor HWR = (alter || spin1)?
		J * ( uni10::otimes( sx, E2r ) + uni10::otimes( sy, E3r ) )
			+ Jz * uni10::otimes( sz, E4r )
			+ hz * uni10::otimes( Id, E4r ):
		0.25 * ( J * ( uni10::otimes( sx, E2r ) + uni10::otimes( sy, E3r ) )
			+ Jz * uni10::otimes( sz, E4r ) )
			+ 0.5 * hz * uni10::otimes( Id, E4r );
	HWR.save( hb_dir + "/HWR" );

	// output effective boundary one-site operators
	E2.save( hb_dir + "/Sx_L" );
	E3.save( hb_dir + "/Sy_L" );
	E4.save( hb_dir + "/Sz_L" );
	E5.save( hb_dir + "/Id_L" );
	E2r.save( hb_dir + "/Sx_R" );
	E3r.save( hb_dir + "/Sy_R" );
	E4r.save( hb_dir + "/Sz_R" );
	E5r.save( hb_dir + "/Id_R" );

	// back support real-num-only programs
	E3 *= (-1.*I);
	E3r *= (-1.*I);
	E3.save( hb_dir + "/_iSy_L" );
	E3r.save( hb_dir + "/_iSy_R" );

	return 0;
}
