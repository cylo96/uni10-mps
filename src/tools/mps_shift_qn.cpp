#include <iostream>
#include <sstream>
#include <algorithm>	// std::find
#include <mps.hpp>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-l arg  Length of mps" << std::endl;
	std::cerr << "-w arg  source directory of Wavefunc mps" << std::endl;
	std::cerr << "-q arg  Qnum shift (example: 1, 2f, ...)" << std::endl;
	std::cerr << "-df arg  Destination Folder of mps" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	std::string wf_dir = "mps-inf";
	std::string df_dir = "mps-inf";
	std::string qn_str = "0";
	int len = 4;

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-w") {
			if (i + 1 < argc)
				wf_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-w <(str) directory name of the source mps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				df_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-df <(str) directory name of the destination mps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-q") {
			if (i + 1 < argc)
				qn_str = std::string(argv[i+1]);
			else {
				std::cerr << "-q <(str) qnum shift (example: 1, 2f, ...)>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l <(int) system length>" << std::endl;
				return 1;
			}
		}
	}

	std::stringstream ss(qn_str);
	std::string segment;
	std::vector<std::string> qn_toks;
	while(std::getline(ss, segment, 'f')) {
  	qn_toks.push_back(segment);
	}
	int u1 = std::stoi(qn_toks[0]);
	bool ferm = (std::find(qn_str.begin(), qn_str.end(), 'f') != qn_str.end());

	/// main function body
	uni10::CUniTensor gam0 = uni10::CUniTensor(wf_dir + "/gamma_0");
	std::vector<uni10::Qnum> qphys = gam0.bond()[1].Qlist();
	int chi = gam0.bond()[0].dim();
	ChainQnInf mpsFU1(len, chi, qphys);
	mpsFU1.importMPS(wf_dir);

	uni10::Qnum qn_shift;
	if (ferm)
		qn_shift.assign(uni10::PRTF_ODD, u1, uni10::PRT_EVEN);
	else
		qn_shift.assign(u1);

	std::vector<std::vector<uni10::Qnum>> qvirt;
	std::vector<std::vector<uni10::Qnum>> qvsft;
	for (int i = 0; i < len; ++i) {
		std::vector<uni10::Qnum> qi = mpsFU1.getLambda(i).bond()[0].Qlist();
		qvirt.push_back(qi);
		for (std::vector<uni10::Qnum>::iterator it = qi.begin(); it != qi.end(); ++it)
			*it = *it * qn_shift;
		qvsft.push_back(qi);
	}

	std::vector<uni10::Bond> bdl, bdg;
	for (int i = 0; i < len; ++i) {
		bdl = {uni10::Bond(uni10::BD_IN, qvsft[i]), uni10::Bond(uni10::BD_OUT, qvsft[i])};
		bdg = {uni10::Bond(uni10::BD_IN, qvsft[i]), uni10::Bond(uni10::BD_IN, qphys), uni10::Bond(uni10::BD_OUT, qvsft[(i+1)%len])};
		uni10::CUniTensor lam(bdl);
		uni10::CUniTensor gam(bdg);
		
		std::map<uni10::Qnum, uni10::CMatrix> blks_l = mpsFU1.getLambda(i).getBlocks();
		for (std::map<uni10::Qnum, uni10::CMatrix>::iterator it = blks_l.begin(); it != blks_l.end(); ++it) {
			uni10::Qnum q = it->first;
			uni10::CMatrix blk = it->second;
			lam.putBlock(q*qn_shift, blk);
		}
		
		std::map<uni10::Qnum, uni10::CMatrix> blks_g = mpsFU1.getGamma(i).getBlocks();
		for (std::map<uni10::Qnum, uni10::CMatrix>::iterator it = blks_g.begin(); it != blks_g.end(); ++it) {
			uni10::Qnum q = it->first;
			uni10::CMatrix blk = it->second;
			gam.putBlock(q*qn_shift, blk);
		}
		
		mpsFU1.putLambda(lam, i);
		mpsFU1.putGamma(gam, i);
	}
	
	mpsFU1.exportMPS(df_dir);
	return 0;
}
