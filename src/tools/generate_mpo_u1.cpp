#include <iostream>
#include <sstream>
#include <fstream>
#include <sys/stat.h>

#include <tns-func/func_op.h>
#include <mps-qn/func_qn_op.h>

using namespace std;

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H xxz <args>  construct Hamiltonian of spin 1/2 xxz model with args = J, Jz, (hz)" << std::endl;
	std::cerr << "-H xxz-lr <args>  construct Hamiltonian of spin 1/2 xxz model with args = J, Jz, long_range_param_file" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// construct operators
	if (std::string(argv[1]) == "-H") {

		if (std::string(argv[2]) == "xxz") {

			double J = 1.0;
			double Jz = 1.0;
			double hz = 0.0;

			if (argc > 4) {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> Jz;
			}
			if (argc > 5) {
				std::stringstream(argv[5]) >> hz;
			}

			uni10::CUniTensor Sp = opU1( "Sp" );
			uni10::CUniTensor Sm = opU1( "Sm" );
			uni10::CUniTensor Sz = opU1( "Sz" );
			uni10::CUniTensor id = opU1( "id" );

			// symmetric blocks
			MPO mpo_ls(5, 'l');
			MPO mpo_ms(5, 'm');
			MPO mpo_rs(5, 'r');

			// raising blocks
			MPO mpo_lp(5, 'l');
			MPO mpo_mp(5, 'm');
			MPO mpo_rp(5, 'r');

			// lowering blocks
			MPO mpo_lm(5, 'l');
			MPO mpo_mm(5, 'm');
			MPO mpo_rm(5, 'r');

			mpo_ls.putTensor(    hz*Sz, 0, 0 );
			mpo_lm.putTensor( 0.5*J*Sm, 0, 1 );
			mpo_lp.putTensor( 0.5*J*Sp, 0, 2 );
			mpo_ls.putTensor(    Jz*Sz, 0, 3 );
			mpo_ls.putTensor(       id, 0, 4 );

			mpo_ms.putTensor(       id, 0, 0 );
			mpo_mp.putTensor(       Sp, 1, 0 );
			mpo_mm.putTensor(       Sm, 2, 0 );
			mpo_ms.putTensor(       Sz, 3, 0 );
			mpo_ms.putTensor(    hz*Sz, 4, 0 );
			mpo_mm.putTensor( 0.5*J*Sm, 4, 1 );
			mpo_mp.putTensor( 0.5*J*Sp, 4, 2 );
			mpo_ms.putTensor(    Jz*Sz, 4, 3 );
			mpo_ms.putTensor(       id, 4, 4 );

			mpo_rs.putTensor(       id, 0, 0 );
			mpo_rp.putTensor(       Sp, 1, 0 );
			mpo_rm.putTensor(       Sm, 2, 0 );
			mpo_rs.putTensor(       Sz, 3, 0 );
			mpo_rs.putTensor(    hz*Sz, 4, 0 );

			struct stat info;
			std::string dirname = "mpo-ham-xxz";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			std::string fname;
			fname = dirname + "/mpo_ls";
			mpo_ls.launch().save( fname );
			fname = dirname + "/mpo_ms";
			mpo_ms.launch().save( fname );
			fname = dirname + "/mpo_rs";
			mpo_rs.launch().save( fname );
			fname = dirname + "/mpo_lp";
			mpo_lp.launch().save( fname );
			fname = dirname + "/mpo_mp";
			mpo_mp.launch().save( fname );
			fname = dirname + "/mpo_rp";
			mpo_rp.launch().save( fname );
			fname = dirname + "/mpo_lm";
			mpo_lm.launch().save( fname );
			fname = dirname + "/mpo_mm";
			mpo_mm.launch().save( fname );
			fname = dirname + "/mpo_rm";
			mpo_rm.launch().save( fname );
		}

		else if (std::string(argv[2]) == "xxz-lr") {

			double J = 1.0;
			double Jz = 1.0;
			std::vector<double> lam;
			std::vector<double> w;

			if (argc < 6) {
				errMsg( argv[0] );
				return 1;
			}

			std::stringstream(argv[3]) >> J;
			std::stringstream(argv[4]) >> Jz;
			std::ifstream params(argv[5]);
			if ( !params ) {
				errMsg( argv[0] );
				return 1;
			}

			int nu = 0;
			double value;
			std::string line;
			while( !params.eof() ) {
				std::getline(params, line);
				std::stringstream vin(line);
				if (nu == 0)
					while ( vin >> value )
						lam.push_back(value);
				else if (nu == 1)
					while ( vin >> value )
						w.push_back(-2.*value);	// cuz J <--> -2t ?
				nu += 1;
			}

			int len_params = lam.size();

			double sum_w = 0.0;
			for (int i = 0; i < len_params; ++i)
				sum_w += w[i];

			J = J - sum_w;

			uni10::CUniTensor Sp = opU1( "Sp" );
			uni10::CUniTensor Sm = opU1( "Sm" );
			uni10::CUniTensor Sz = opU1( "Sz" );
			uni10::CUniTensor id = opU1( "id" );
			int dim_w = 5 + 2 * len_params;

			// symmetric blocks
			MPO mpo_ls(dim_w, 'l');
			MPO mpo_ms(dim_w, 'm');
			MPO mpo_rs(dim_w, 'r');

			// raising blocks
			MPO mpo_lp(dim_w, 'l');
			MPO mpo_mp(dim_w, 'm');
			MPO mpo_rp(dim_w, 'r');

			// lowering blocks
			MPO mpo_lm(dim_w, 'l');
			MPO mpo_mm(dim_w, 'm');
			MPO mpo_rm(dim_w, 'r');

			mpo_lm.putTensor( 0.5*J*Sm, 0, 1 );
			mpo_lp.putTensor( 0.5*J*Sp, 0, 2 );
			mpo_ls.putTensor( Jz*Sz, 0, 3 );
			for (int i = 0; i < len_params; ++i) {
				mpo_lm.putTensor( 0.5*w[i]*Sm, 0, 4+i*2 );
				mpo_lp.putTensor( 0.5*w[i]*Sp, 0, 5+i*2 );
			}
			mpo_ls.putTensor( id, 0, dim_w-1 );

			mpo_ms.putTensor( id, 0, 0 );
			mpo_mp.putTensor( Sp, 1, 0 );
			mpo_mm.putTensor( Sm, 2, 0 );
			mpo_ms.putTensor( Sz, 3, 0 );
			for (int i = 0; i < len_params; ++i) {
				mpo_mp.putTensor( Sp, 4+i*2, 0 );
				mpo_mm.putTensor( Sm, 5+i*2, 0 );
			}
			mpo_mm.putTensor( 0.5*J*Sm, dim_w-1, 1 );
			mpo_mp.putTensor( 0.5*J*Sp, dim_w-1, 2 );
			mpo_ms.putTensor( Jz*Sz, dim_w-1, 3 );
			for (int i = 0; i < len_params; ++i) {
				mpo_mm.putTensor( 0.5*w[i]*Sm, dim_w-1, 4+i*2 );
				mpo_mp.putTensor( 0.5*w[i]*Sp, dim_w-1, 5+i*2 );
			}
			mpo_ms.putTensor( id, dim_w-1, dim_w-1 );

			for (int i = 0; i < len_params; ++i) {
				mpo_ms.putTensor( lam[i]*id, 4+i*2, 4+i*2 );
				mpo_ms.putTensor( lam[i]*id, 5+i*2, 5+i*2 );
			}

			mpo_rs.putTensor( id, 0, 0 );
			mpo_rp.putTensor( Sp, 1, 0 );
			mpo_rm.putTensor( Sm, 2, 0 );
			mpo_rs.putTensor( Sz, 3, 0 );
			for (int i = 0; i < len_params; ++i) {
				mpo_rp.putTensor( Sp, 4+i*2, 0 );
				mpo_rm.putTensor( Sm, 5+i*2, 0 );
			}

			struct stat info;
			std::string dirname = "mpo-ham-xxz";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_ls.launch().save( dirname + "/mpo_ls" );
			mpo_ms.launch().save( dirname + "/mpo_ms" );
			mpo_rs.launch().save( dirname + "/mpo_rs" );

			mpo_lp.launch().save( dirname + "/mpo_lp" );
			mpo_mp.launch().save( dirname + "/mpo_mp" );
			mpo_rp.launch().save( dirname + "/mpo_rp" );

			mpo_lm.launch().save( dirname + "/mpo_lm" );
			mpo_mm.launch().save( dirname + "/mpo_mm" );
			mpo_rm.launch().save( dirname + "/mpo_rm" );
		}

		else {
			std::cerr << "-H model Hamiltonian. Models supported: xxz" << std::endl;
			return 1;
		}
	}

	else {

		errMsg( argv[0] );
		return 1;
	}

	return 0;
}
