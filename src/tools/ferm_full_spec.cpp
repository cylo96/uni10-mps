#include <iostream>
#include <sstream>
#include <mps.hpp>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-l arg  Length of mps" << std::endl;
	std::cerr << "-w arg  directory of mps" << std::endl;
	std::cerr << "-df arg  Destination Folder of mps" << std::endl;
	std::cerr << "-occu arg arg  set OCCUpation params num_particles, unit_cell_size" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	std::string wf_dir = "mps-inf";
	int len = 2;
	int occu = 1;
	int unit = 2;

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-w") {
			if (i + 1 < argc)
				wf_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-w <(str) directory name of mps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				wf_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-df <(str) directory name of the destination mps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l <(int) system length>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-occu") {
			if (i + 2 < argc) {
				std::istringstream(argv[i+1]) >> occu;
				std::istringstream(argv[i+2]) >> unit;
			}
			else {
				std::cerr << "-occu <(int) number of particles> <(int) unit-cell size>" << std::endl;
				return 1;
			}
		}
	}

	/// main function body
	uni10::CUniTensor id0 = opFU1_grd0( "id", 0, 0 );
	uni10::CUniTensor id1 = opFU1_grd0( "id", occu, unit );

	std::vector<uni10::Qnum> qn0 = id0.bond()[0].Qlist();
	std::vector<uni10::Qnum> qn1 = id1.bond()[0].Qlist();
	uni10::Qnum qf(uni10::PRTF_ODD, 0, uni10::PRT_EVEN);

	std::vector<uni10::Qnum> qphys;
	std::vector<uni10::Qnum> qvirt;

	bool fu1 = false;
	for (int i = 0; i < qn1.size(); ++i)
		fu1 += (qn1[i].U1() != 0);

	if (fu1) {
		qphys = {qn0[0], qn0[1]};
		qvirt = {qn1[0], qn1[0]*qf, qn0[0], qn0[1], qn1[1]*qf, qn1[1]};
	}
	else {
		qphys = {qn0[0], qn0[1]};
		qvirt = {qn0[0], qn0[1]};
	}

	std::vector<uni10::Bond> bdl, bdg;
	bdl = {uni10::Bond(uni10::BD_IN, qvirt), uni10::Bond(uni10::BD_OUT, qvirt)};
	bdg = {uni10::Bond(uni10::BD_IN, qvirt), uni10::Bond(uni10::BD_IN, qphys), uni10::Bond(uni10::BD_OUT, qvirt)};
	uni10::CUniTensor lam(bdl);
	uni10::CUniTensor gam(bdg);
	lam.identity();
	gam.eye();

	for (int i = 0; i < len; ++i) {
		lam.save( wf_dir + "/lambda_" + std::to_string((long long) i) );
		gam.save( wf_dir + "/gamma_" + std::to_string((long long) i) );
	}

	return 0;
}
