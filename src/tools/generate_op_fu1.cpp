#include <iostream>
#include <sstream>

#include <tns-func/tns_const.h>
#include <mps-qn/func_qn_op.h>

using namespace std;

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H hub arg arg  construct Hamiltonian of Hubbard model with t, V = args" << std::endl;
	std::cerr << "-N  construct N operator" << std::endl;
	std::cerr << "-cp  construct cp ( c^+ ) operator" << std::endl;
	std::cerr << "-cm  construct cm ( c ) operator" << std::endl;
	std::cerr << "-id  construct identity operator" << std::endl;
	std::cerr << "-Jl  construct Jl operator" << std::endl;
	std::cerr << "-Jr  construct Jr operator" << std::endl;
	std::cerr << "-occu arg arg  set OCCUpation params num_particles, unit_cell_size" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	int occu = 1;
	int unit = 2;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-occu") {
			if (i + 2 < argc) {
				std::istringstream(argv[i+1]) >> occu;
				std::istringstream(argv[i+2]) >> unit;
			}
			else {
				std::cerr << "-occu option requires arguments (int) number of particles, (int) unit-cell size." << std::endl;
				return 1;
			}
		}
	}


	// construct operators
	if (std::string(argv[1]) == "-H") {

		if ( (argc > 2) && (std::string(argv[2]) == "hub") ) {

			double t = 1.0;
			double V = 1.0;

			if (argc > 4) {
				std::stringstream(argv[3]) >> t;
				std::stringstream(argv[4]) >> V;
			}

			uni10::CUniTensor Ni = opFU1_grd0( "N", occu, unit ) + (-0.5) * opFU1_grd0( "id", occu, unit );
			uni10::CUniTensor cp = opFU1_grd0( "cp", occu, unit );
			uni10::CUniTensor cm = opFU1_grd0( "cm", occu, unit );

			uni10::CUniTensor ham
				= (-1.) * t * ( otimesPM(cp, cm) + otimesPM(cp, cm, true) )
				+ V * uni10::otimes(Ni, Ni);

			ham.save( "ham_hub" );
			std::cout << ham;
		}

		else {
			std::cerr << "-H model Hamiltonian. Valid choices: hub" << std::endl;
			return 1;
		}
	}

	else if (std::string(argv[1]) == "-id") {

		uni10::CUniTensor op = opFU1_grd0( "id", occu, unit );
		op.save( "id" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-N") {

		uni10::CUniTensor op = opFU1_grd0( "N", occu, unit );
		op.save( "N" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-cp") {

		uni10::CUniTensor op = opFU1_grd0( "cp", occu, unit );
		op.save( "cp" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-cm") {

		uni10::CUniTensor op = opFU1_grd0( "cm", occu, unit );
		op.save( "cm" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-Jl") {

		uni10::CUniTensor cp = opFU1_grd0( "cp", occu, unit );
		uni10::CUniTensor cm = opFU1_grd0( "cm", occu, unit );
		uni10::CUniTensor op = I * ( otimesPM(cp, cm) + otimesPM(cm, cp) );

		op.setName( "Jl" );
		op.save( "Jl" );
		std::cout << op;
	}

	else if (std::string(argv[1]) == "-Jr") {

		uni10::CUniTensor cp = opFU1_grd0( "cp", occu, unit );
		uni10::CUniTensor cm = opFU1_grd0( "cm", occu, unit );
		uni10::CUniTensor op = I * ( (-1.0) * otimesPM(cm, cp) + (-1.0) * otimesPM(cp, cm) );

		op.setName( "Jr" );
		op.save( "Jr" );
		std::cout << op;
	}

	else {

		errMsg( argv[0] );
		return 1;
	}

	return 0;
}
