#include <iostream>

#include <tns-func/func_convert.h>

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		std::cerr << "Usage: " << argv[0] << " < filename of CUniTensor > " << std::endl;
		return 1;
	}

	uni10::UniTensor rT = complex2Real( uni10::CUniTensor( argv[1] ) );

	rT.save( argv[1] );

	return 0;
}

