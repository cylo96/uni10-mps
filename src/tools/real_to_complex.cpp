#include <iostream>

#include <tns-func/func_convert.h>

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		std::cerr << "Usage: " << argv[0] << " < filename of UniTensor > " << std::endl;
		return 1;
	}

	uni10::CUniTensor cT = real2Complex( uni10::UniTensor( argv[1] ) );

	cT.save( argv[1] );

	return 0;
}

