#include <iostream>
#include <sstream>

#include <tns-func/func_convert.h>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " <state> <bond dimension>" << std::endl;
	std::cerr << "Available states:" << std::endl;
	std::cerr << "sing  \"SINGlet\" max entangled state, spin 1/2" << std::endl;
	std::cerr << "trip  \"TRIPlet\" max entangled state, spin 1/2" << std::endl;
	std::cerr << "rnd arg  randomized state. arg: physical dimension" << std::endl;	
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 3) {
		errMsg( argv[0] );
		return 1;
	}

	int d, D;	// physical dimension, bond dimension
	std::istringstream(argv[2]) >> D;
	
	double up[] = {1.0, 0.0};
	double dn[] = {0.0, 1.0};
	double ps[] = {1./sqrt(2), 1./sqrt(2)};
	double ms[] = {1./sqrt(2), -1./sqrt(2)};

	double up1[] = {1.0, 0.0, 0.0};
	double zo1[] = {0.0, 1.0, 0.0};
	double dn1[] = {0.0, 0.0, 1.0};

	uni10::UniTensor sys, env, state;

	if (std::string(argv[1]) == "sing") {
		std::vector<uni10::Bond> bd;
		bd.push_back( uni10::Bond(uni10::BD_IN, 2) );
		sys.assign(bd);
		sys.setElem(up);
		env.assign(bd);
		env.setElem(dn);
		state = uni10::otimes( sys, env );
		sys.setElem(dn);
		env.setElem(up);
		state += ( (-1.0)*uni10::otimes( sys, env ) );
		state *= sqrt(1./2.);
	}
	else if (std::string(argv[1]) == "trip") {
		std::vector<uni10::Bond> bd;
		bd.push_back( uni10::Bond(uni10::BD_IN, 2) );
		sys.assign(bd);
		sys.setElem(up);
		env.assign(bd);
		env.setElem(dn);
		state = uni10::otimes( sys, env );
		sys.setElem(dn);
		env.setElem(up);
		state += uni10::otimes( sys, env );
		state *= sqrt(1./2.);
	}
	else if (std::string(argv[1]) == "rnd") {
		std::istringstream(argv[2]) >> d;
		std::istringstream(argv[3]) >> D;
		std::vector<uni10::Bond> bd;
		bd.push_back( uni10::Bond(uni10::BD_IN, d) );
		bd.push_back( uni10::Bond(uni10::BD_IN, d) );
		state.assign(bd);
		state.randomize();
	}
	else {
		errMsg( argv[0] );
		return 1;
	}

	uni10::Matrix uni(D, D);
	uni.set_zero();
	uni.at(0, 0) = 1.0;

	std::vector< uni10::Bond > bdl;
	bdl.push_back( uni10::Bond( uni10::BD_IN, D ) );
	bdl.push_back( uni10::Bond( uni10::BD_OUT, D ) );
	uni10::UniTensor lambda( bdl );
	lambda.putBlock( uni );

	uni10::UniTensor gamma = uni10::otimes(lambda, state);

	uni10::CUniTensor gam = real2Complex( gamma );
	uni10::CUniTensor lam = real2Complex( lambda );

	gam.save("gamma");
	lam.save("lambda");

	return 0;
}

