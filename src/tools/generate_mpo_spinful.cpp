#include <iostream>
#include <sstream>
#include <fstream>
#include <sys/stat.h>

#include <tns-func/func_op.h>
#include <mps-qn/func_qn_op.h>

using namespace std;

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H hub <t> <U> (<V> <mu>)     : Hamiltonian of Hubbard model" << std::endl;
	std::cerr << "-occu <num_ptcl> <unit_cell>  : set OCCUpation params" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {
		errMsg( argv[0] );
		return 1;
	}

	int occu = 1;
	int unit = 2;

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-occu") {
			if (i + 2 < argc) {
				std::istringstream(argv[i+1]) >> occu;
				std::istringstream(argv[i+2]) >> unit;
			}
			else {
				std::cerr << "-occu option requires arguments (int) number of particles, (int) unit-cell size." << std::endl;
				return 1;
			}
		}
	}

	// construct operators
	if (std::string(argv[1]) == "-H") {
		if (std::string(argv[2]) == "hub") {
			double t = 1.0;
			double U = 1.0;
			double V = 0.0;
			double mu = 0.0;
			if (argc > 4) {
				std::stringstream(argv[3]) >> t;
				std::stringstream(argv[4]) >> U;
			}
			if (argc > 5)
				std::stringstream(argv[5]) >> V;
			if (argc > 6)
				std::stringstream(argv[6]) >> mu;

			uni10::CUniTensor id = opFU1_spinful( "id", occu, unit );
			uni10::CUniTensor NN = opFU1_spinful( "NN", occu, unit );
			uni10::CUniTensor cp_up = opFU1_spinful( "cp_up", occu, unit );
			uni10::CUniTensor cm_up = opFU1_spinful( "cm_up", occu, unit );
			uni10::CUniTensor cp_dn = opFU1_spinful( "cp_dn", occu, unit );
			uni10::CUniTensor cm_dn = opFU1_spinful( "cm_dn", occu, unit );
			uni10::CUniTensor N_up = opFU1_spinful( "N_up", occu, unit );
			uni10::CUniTensor N_dn = opFU1_spinful( "N_dn", occu, unit );
			uni10::CUniTensor N = opFU1_spinful( "N", occu, unit );
			// uni10::CUniTensor Nu = N_up + (-0.5) * id;
			// uni10::CUniTensor Nd = N_dn + (-0.5) * id;

			// symmetric blocks
			MPO mpo_ls(7, 'l');
			MPO mpo_ms(7, 'm');
			MPO mpo_rs(7, 'r');

			// raising blocks
			MPO mpo_lp(7, 'l');
			MPO mpo_mp(7, 'm');
			MPO mpo_rp(7, 'r');

			// lowering blocks
			MPO mpo_lm(7, 'l');
			MPO mpo_mm(7, 'm');
			MPO mpo_rm(7, 'r');

			mpo_ls.putTensor(  U*NN + mu*N, 0, 0 );
			mpo_lp.putTensor( -1.0*t*cp_up, 0, 1 );
			mpo_lm.putTensor(  1.0*t*cm_up, 0, 2 );
			mpo_lp.putTensor( -1.0*t*cp_dn, 0, 3 );
			mpo_lm.putTensor(  1.0*t*cm_dn, 0, 4 );
			mpo_ls.putTensor(          V*N, 0, 5 );
			mpo_ls.putTensor(           id, 0, 6 );

			mpo_ms.putTensor(           id, 0, 0 );
			mpo_mm.putTensor(        cm_up, 1, 0 );
			mpo_mp.putTensor(        cp_up, 2, 0 );
			mpo_mm.putTensor(        cm_dn, 3, 0 );
			mpo_mp.putTensor(        cp_dn, 4, 0 );
			mpo_ms.putTensor(            N, 5, 0 );
			mpo_ms.putTensor(  U*NN + mu*N, 6, 0 );
			mpo_mp.putTensor( -1.0*t*cp_up, 6, 1 );
			mpo_mm.putTensor(  1.0*t*cm_up, 6, 2 );
			mpo_mp.putTensor( -1.0*t*cp_dn, 6, 3 );
			mpo_mm.putTensor(  1.0*t*cm_dn, 6, 4 );
			mpo_ms.putTensor(          V*N, 6, 5 );
			mpo_ms.putTensor(           id, 6, 6 );

			mpo_rs.putTensor(           id, 0, 0 );
			mpo_rm.putTensor(        cm_up, 1, 0 );
			mpo_rp.putTensor(        cp_up, 2, 0 );
			mpo_rm.putTensor(        cm_dn, 3, 0 );
			mpo_rp.putTensor(        cp_dn, 4, 0 );
			mpo_rs.putTensor(            N, 5, 0 );
			mpo_rs.putTensor(  U*NN + mu*N, 6, 0 );

			struct stat info;
			std::string dirname = "mpo-ham-hub";
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_ls.launch().save( dirname + "/mpo_ls" );
			mpo_ms.launch().save( dirname + "/mpo_ms" );
			mpo_rs.launch().save( dirname + "/mpo_rs" );
			mpo_lp.launch().save( dirname + "/mpo_lp" );
			mpo_mp.launch().save( dirname + "/mpo_mp" );
			mpo_rp.launch().save( dirname + "/mpo_rp" );
			mpo_lm.launch().save( dirname + "/mpo_lm" );
			mpo_mm.launch().save( dirname + "/mpo_mm" );
			mpo_rm.launch().save( dirname + "/mpo_rm" );
		}
		else {
			std::cerr << "-H model Hamiltonian. Models supported: hub" << std::endl;
			return 1;
		}
	}
	else {
		errMsg( argv[0] );
		return 1;
	}

	return 0;
}
