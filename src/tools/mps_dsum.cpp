#include <iostream>
#include <sstream>
#include <algorithm>	// std::sort
#include <mps.hpp>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-l arg  Length of mps" << std::endl;
	std::cerr << "-w arg1, arg2, ...  source directories of Wavefunc mps" << std::endl;
	std::cerr << "-df arg  Destination Folder of mps" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	std::vector<std::string> wf_dir;
	std::string df_dir = "mps-inf";
	int len = 4;
	bool sort = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-w") {
			if (i + 1 < argc) {
				int j = i+1;
				std::string arg_str = std::string(argv[j]);
				bool stop = (arg_str == "-l" || arg_str == "-df" || arg_str == "-w" || arg_str == "-sort");
				while (j < argc && !stop) {
					wf_dir.push_back(arg_str);
					j += 1;
					arg_str = std::string(argv[j]);
					stop = (arg_str == "-l" || arg_str == "-df" || arg_str == "-w" || arg_str == "-sort");
				}
			}
			else {
				std::cerr << "-w <(strings) directory names of the source mps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				df_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-df <(str) directory name of the destination mps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l <(int) system length>" << std::endl;
				return 1;
			}
		}
	}

	/// main function body
	uni10::CUniTensor gam0 = uni10::CUniTensor(wf_dir[0] + "/gamma_0");
	std::vector<uni10::Qnum> qphys = gam0.bond()[1].Qlist();
	int chi = gam0.bond()[0].dim();
	int num_mps = wf_dir.size();
	std::vector<ChainQnInf> mps;
	for (int i = 0; i < num_mps; ++i) {
		mps.push_back(ChainQnInf(len, chi, qphys));
		mps[i].importMPS(wf_dir[i]);
	}

	std::vector<int> lab_l = {0, 1};
	std::vector<int> lab_g = {0, 1, 2};
	for (int i = 1; i < num_mps; ++i) {
		for (int j = 0; j < len; ++j) {
			uni10::CUniTensor lam0 = mps[0].getLambda(j);
			uni10::CUniTensor lami = mps[i].getLambda(j);
			uni10::CUniTensor gam0 = mps[0].getGamma(j);
			uni10::CUniTensor gami = mps[i].getGamma(j);
			lam0.setLabel(lab_l); lami.setLabel(lab_l);
			gam0.setLabel(lab_g); gami.setLabel(lab_g);
			uni10::CUniTensor lamd = directSum(lam0, lami, std::vector<int>{0, 1});
			uni10::CUniTensor gamd = directSum(gam0, gami, std::vector<int>{0, 2});
			mps[0].putLambda(lamd, j);
			mps[0].putGamma(gamd, j);
		}
	}
	
	for (int j = 0; j < len; ++j) {
		mps[0].putLambda(mps[0].getLambda(j) * (1./sqrt((double)num_mps)), j);
		mps[0].putGamma(mps[0].getGamma(j) * sqrt((double)num_mps), j);
	}
	mps[0].setMaxBD(chi*num_mps);
	
	mps[0].exportMPS(df_dir);
	return 0;
}
