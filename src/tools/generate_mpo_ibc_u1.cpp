#include <iostream>
#include <sstream>
#include <sys/stat.h>

#include <tns-func/func_op.h>
#include <mps-qn/func_qn_op.h>

using namespace std;

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H xxz <J> <Jz> (-w <hb_dir>) (-imp <J_imp> <Jz_imp>) (-df <dest_folder>)" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 3) {

		errMsg( argv[0] );
		return 1;
	}

	// construct operators
	if (std::string(argv[1]) == "-H") {

		if (std::string(argv[2]) == "xxz") {

			double J = 1.0;
			double Jz = 1.0;
			double J_imp = 1.0;
			double Jz_imp = 1.0;
			double hz = 0.0;
			bool imp = false;
			std::string hb_dir = ".";
			std::string dirname = "mpo-ham-xxz";

			if (argc > 4) {
				std::stringstream(argv[3]) >> J;
				std::stringstream(argv[4]) >> Jz;
			}

			if (argc > 5) {
				for (int i = 5; i < argc; ++i) {

					if (std::string(argv[i]) == "-w") {
						if (i + 1 < argc) {
							hb_dir = std::string(argv[i+1]);
						}
						else {
							std::cerr << "Usage: -w <hb_dir>" << std::endl;
							return 1;
						}
					}
					else if (std::string(argv[i]) == "-imp") {
						if (i + 2 < argc) {
							std::stringstream(argv[i+1]) >> J_imp;
							std::stringstream(argv[i+2]) >> Jz_imp;
							imp = true;
						}
						else {
							std::cerr << "Usage: -imp <J_imp> <Jz_imp>" << std::endl;
							return 1;
						}
					}
					else if (std::string(argv[i]) == "-df") {
						if (i + 1 < argc) {
							dirname = std::string(argv[i+1]);
						}
						else {
							std::cerr << "Usage: -df <dest_folder>" << std::endl;
							return 1;
						}
					}
					else if (std::string(argv[i]) == "-hz") {
						if (i + 1 < argc) {
							std::stringstream(argv[i+1]) >> hz;
						}
						else {
							std::cerr << "Usage: -hz <ext_field>" << std::endl;
							return 1;
						}
					}
				}
			}

			uni10::CUniTensor Sp = opU1( "Sp" );
			uni10::CUniTensor Sm = opU1( "Sm" );
			uni10::CUniTensor Sz = opU1( "Sz" );
			uni10::CUniTensor id = opU1( "id" );

			uni10::CUniTensor hl   = uni10::CUniTensor( hb_dir + "/HL" );
			uni10::CUniTensor Sp_l = uni10::CUniTensor( hb_dir + "/Sp_L" );
			uni10::CUniTensor Sm_l = uni10::CUniTensor( hb_dir + "/Sm_L" );
			uni10::CUniTensor Sz_l = uni10::CUniTensor( hb_dir + "/Sz_L" );
			uni10::CUniTensor id_l = uni10::CUniTensor( hb_dir + "/Id_L" );

			uni10::CUniTensor hr   = uni10::CUniTensor( hb_dir + "/HR" );
			uni10::CUniTensor Sp_r = uni10::CUniTensor( hb_dir + "/Sp_R" );
			uni10::CUniTensor Sm_r = uni10::CUniTensor( hb_dir + "/Sm_R" );
			uni10::CUniTensor Sz_r = uni10::CUniTensor( hb_dir + "/Sz_R" );
			uni10::CUniTensor id_r = uni10::CUniTensor( hb_dir + "/Id_R" );

			// symmetric blocks
			MPO mpo_ls(5, 'l');
			MPO mpo_ms(5, 'm');
			MPO mpo_rs(5, 'r');

			// raising blocks
			MPO mpo_lp(5, 'l');
			MPO mpo_mp(5, 'm');
			MPO mpo_rp(5, 'r');

			// lowering blocks
			MPO mpo_lm(5, 'l');
			MPO mpo_mm(5, 'm');
			MPO mpo_rm(5, 'r');

			mpo_ls.putTensor( hl+hz*Sz_l, 0, 0 );
			mpo_lm.putTensor( 0.5*J*Sm_l, 0, 1 );
			mpo_lp.putTensor( 0.5*J*Sp_l, 0, 2 );
			mpo_ls.putTensor(    Jz*Sz_l, 0, 3 );
			mpo_ls.putTensor(       id_l, 0, 4 );

			mpo_ms.putTensor(         id, 0, 0 );
			mpo_mp.putTensor(         Sp, 1, 0 );
			mpo_mm.putTensor(         Sm, 2, 0 );
			mpo_ms.putTensor(         Sz, 3, 0 );
			mpo_ms.putTensor(      hz*Sz, 4, 0 );
			mpo_mm.putTensor(   0.5*J*Sm, 4, 1 );
			mpo_mp.putTensor(   0.5*J*Sp, 4, 2 );
			mpo_ms.putTensor(      Jz*Sz, 4, 3 );
			mpo_ms.putTensor(         id, 4, 4 );

			mpo_rs.putTensor(       id_r, 0, 0 );
			mpo_rp.putTensor(       Sp_r, 1, 0 );
			mpo_rm.putTensor(       Sm_r, 2, 0 );
			mpo_rs.putTensor(       Sz_r, 3, 0 );
			mpo_rs.putTensor( hr+hz*Sz_r, 4, 0 );

			struct stat info;
			if ( stat( dirname.c_str(), &info ) != 0 )
				mkdir( dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

			mpo_ls.launch().save( dirname + "/mpo_ls" );
			mpo_ms.launch().save( dirname + "/mpo_ms" );
			mpo_rs.launch().save( dirname + "/mpo_rs" );
			mpo_lp.launch().save( dirname + "/mpo_lp" );
			mpo_mp.launch().save( dirname + "/mpo_mp" );
			mpo_rp.launch().save( dirname + "/mpo_rp" );
			mpo_lm.launch().save( dirname + "/mpo_lm" );
			mpo_mm.launch().save( dirname + "/mpo_mm" );
			mpo_rm.launch().save( dirname + "/mpo_rm" );

			if (imp) {
				// symmetric blocks
				MPO mpo_imp_s(5, 'm');
				// raising blocks
				MPO mpo_imp_p(5, 'm');
				// lowering blocks
				MPO mpo_imp_m(5, 'm');

				mpo_imp_s.putTensor(           id, 0, 0 );
				mpo_imp_p.putTensor(           Sp, 1, 0 );
				mpo_imp_m.putTensor(           Sm, 2, 0 );
				mpo_imp_s.putTensor(           Sz, 3, 0 );
				mpo_imp_s.putTensor(        hz*Sz, 4, 0 );
				mpo_imp_m.putTensor( 0.5*J_imp*Sm, 4, 1 );
				mpo_imp_p.putTensor( 0.5*J_imp*Sp, 4, 2 );
				mpo_imp_s.putTensor(    Jz_imp*Sz, 4, 3 );
				mpo_imp_s.putTensor(           id, 4, 4 );

				mpo_imp_s.launch().save( dirname + "/mpo_imp_s" );
				mpo_imp_p.launch().save( dirname + "/mpo_imp_p" );
				mpo_imp_m.launch().save( dirname + "/mpo_imp_m" );
			}
		}

		else {
			errMsg( argv[0] );
			return 1;
		}
	}

	else {
		errMsg( argv[0] );
		return 1;
	}

	return 0;
}
