#include <iostream>
#include <sstream>
#include <mps.hpp>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-l arg  Length of mps" << std::endl;
	std::cerr << "-w arg  source directory of Wavefunc mps" << std::endl;
	std::cerr << "-df arg  Destination Folder of mps" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	std::string wf_dir = "mps-inf";
	std::string df_dir = "mps-inf";
	int len = 4;

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-w") {
			if (i + 1 < argc)
				wf_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-w <(str) directory name of the source mps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				df_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-df <(str) directory name of the destination mps>" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l <(int) system length>" << std::endl;
				return 1;
			}
		}
	}

	/// main function body
	uni10::CUniTensor gam0 = uni10::CUniTensor(wf_dir + "/gamma_0");
	std::vector<uni10::Qnum> qphys = gam0.bond()[1].Qlist();
	int chi = gam0.bond()[0].dim();
	ChainQnInf mpsFU1(len, chi*len, qphys);
	mpsFU1.importMPS(wf_dir);

	std::vector<uni10::Qnum> qvirt;
	for (int i = 0; i < len; ++i) {
		// append lam[i]'s qlist to qvirt
		std::vector<uni10::Qnum> qi = mpsFU1.getLambda(i).bond()[0].Qlist();
		qvirt.insert(qvirt.end(), qi.begin(), qi.end());
	}

	std::vector<uni10::Bond> bdl, bdg;
	bdl = {uni10::Bond(uni10::BD_IN, qvirt), uni10::Bond(uni10::BD_OUT, qvirt)};
	bdg = {uni10::Bond(uni10::BD_IN, qvirt), uni10::Bond(uni10::BD_IN, qphys), uni10::Bond(uni10::BD_OUT, qvirt)};
	uni10::CUniTensor lam(bdl);
	uni10::CUniTensor gam(bdg);

	for (int i = 0; i < len; ++i) {
		std::map<uni10::Qnum, uni10::CMatrix> blks = mpsFU1.getLambda(i).getBlocks();
		for (std::map<uni10::Qnum, uni10::CMatrix>::iterator it = blks.begin(); it != blks.end(); ++it) {
			uni10::Qnum q = it->first;
			uni10::CMatrix blk = it->second;
			lam.putBlock( q, blk );
		}
	}
	lam *= std::sqrt(1./len);

	for (int i = 0; i < len; ++i) {
		std::map<uni10::Qnum, uni10::CMatrix> blks = mpsFU1.getGamma(i).getBlocks();
		for (std::map<uni10::Qnum, uni10::CMatrix>::iterator it = blks.begin(); it != blks.end(); ++it) {
			uni10::Qnum q = it->first;
			uni10::CMatrix blk = it->second;
			gam.putBlock( q, blk );
		}
	}
	gam *= std::sqrt((double)len);

	for (int i = 0; i < len; ++i) {
		mpsFU1.putLambda(lam, i);
		mpsFU1.putGamma(gam, i);
	}
	mpsFU1.exportMPS(df_dir);

	return 0;
}
