#include <iostream>
#include <sstream>

#include <uni10.hpp>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " <gam/lam tensor> <bond dimension>" << std::endl;
}

//======================================

uni10::CUniTensor initGamma(int chi1, int chi2, int d) {
	/// initialize a gamma tensor
	std::vector<uni10::Bond> bond_gam;
	bond_gam.push_back( uni10::Bond(uni10::BD_IN, chi1) );
	bond_gam.push_back( uni10::Bond(uni10::BD_IN, d) );
	bond_gam.push_back( uni10::Bond(uni10::BD_OUT, chi2) );

	uni10::CUniTensor gam(bond_gam);

	return gam;
}

//======================================

uni10::CUniTensor initLambda(int chi) {
	/// initialize a lambda tensor
	std::vector<uni10::Bond> bond_lam;
	bond_lam.push_back( uni10::Bond(uni10::BD_IN, chi) );
	bond_lam.push_back( uni10::Bond(uni10::BD_OUT, chi) );

	uni10::CUniTensor lam(bond_lam);

	return lam;
}

//======================================

uni10::CUniTensor resizeGamma( uni10::CUniTensor gam0, int chi_new ) {
	/// resize a gamma tensor 
	std::vector<uni10::Bond> bd = gam0.bond();
	int chi1 = bd[0].dim();
	int d    = bd[1].dim();
	int chi2 = bd[2].dim();

	uni10::CUniTensor gam;

	if ( chi_new == chi1 && chi_new == chi2 ) {
		gam = gam0;
	}
	else {
		int dim1 = std::min(chi1, chi_new);
		int dim2 = std::min(chi2, chi_new);
		gam = initGamma( chi_new, chi_new, d );

		uni10::CMatrix gam0_blk = gam0.getBlock();
		uni10::CMatrix gam_blk = gam.getBlock();

		for (int i = 0; i < dim1 * d; ++i)
			for (int j = 0; j < dim2; ++j)
				gam_blk.at(i, j) = gam0_blk.at(i, j);

		gam.putBlock( gam_blk );
	}

	return gam;
}

//======================================

uni10::CUniTensor resizeLambda( uni10::CUniTensor lam0, int chi_new ) {
	/// resize a lambda tensor
	int chi = lam0.bond()[0].dim();
	uni10::CUniTensor lam;

	if ( chi_new == chi ) {
		lam = lam0;
	}
	else {
		int dim = std::min(chi, chi_new);
		lam = initLambda( chi_new );

		uni10::CMatrix lam0_blk = lam0.getBlock(); 
		uni10::CMatrix lam_blk = lam.getBlock(); 

		for (int i = 0; i < dim; ++i)
			lam_blk.at(i, i) = lam0_blk.at(i, i);

		lam.putBlock( lam_blk );
	}

	return lam;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 3) {

		errMsg( argv[0] );
		return 1;
	}

	std::string fname;
	int bd_dim;

	fname = std::string(argv[1]);
	std::istringstream(argv[2]) >> bd_dim;

	uni10::CUniTensor uniT( fname );

	if ( uniT.bondNum() == 3 && uniT.inBondNum() == 2 )
		uniT = resizeGamma( uniT, bd_dim );	// gamma tensor
	else if ( uniT.bondNum() == 2 && uniT.inBondNum() == 1 )
		uniT = resizeLambda( uniT, bd_dim );	// lambda tensor
	else {
		errMsg( argv[0] );
		return 1;
	}

	uniT.save( fname );

	return 0;
}

