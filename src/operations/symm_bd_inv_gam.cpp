#include <sstream>

#include <tns-func/func_net.h>

//==============================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-u arg  load Uitary mtx from file [arg]" << std::endl;
}

//==============================================

int main(int argc, char* argv[]){

	std::string Umtx;

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-u") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				Umtx = std::string(argv[i+1]);
			}
			else {
				std::cerr << "usage: -u filename_of_U_mtx" << std::endl;
				return 1;
			}
		}
	}


	std::vector<uni10::CUniTensor> gamma;
	std::vector<uni10::CUniTensor> lambda;

	std::string wf_dir = "mps-inf";
	gamma.push_back( uni10::CUniTensor(wf_dir + "/gamma_0") );
	gamma.push_back( uni10::CUniTensor(wf_dir + "/gamma_1") );
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_0") );
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_1") );

	int D = gamma[0].bond()[0].dim();

	uni10::CUniTensor U;
	uni10::CUniTensor U_inv;

	U = uni10::CUniTensor(Umtx);
	U_inv = U;
	U_inv.transpose();
	U_inv.conj();

	uni10::CUniTensor gamAt = gamma[0];
	uni10::CUniTensor gamBt = gamma[1];

	int lab_old[] = {0, 100, 1};
	int lab_new[] = {1, 100, 0};

	gamAt.setLabel( lab_old );
	gamBt.setLabel( lab_old );
	gamAt.permute( lab_new, 2 );
	gamBt.permute( lab_new, 2 );

	gamAt = netLGL( U_inv, gamAt, U );
	gamBt = netLGL( U_inv, gamBt, U );

	uni10::CUniTensor gamAs = gamma[0] + gamAt;
	uni10::CUniTensor gamAas = gamma[0] + (-1.0) * gamAt;
	uni10::CUniTensor gamBs = gamma[1] + gamBt;
	uni10::CUniTensor gamBas = gamma[1] + (-1.0) * gamBt;

	gamAs.save(wf_dir + "/gamma_0");
	gamBs.save(wf_dir + "/gamma_1");
	//gamAas.save(wf_dir + "/gamma_0");
	//gamBas.save(wf_dir + "/gamma_1");

	return 0;
}

