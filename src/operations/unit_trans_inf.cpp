#include <sstream>

#include <tns-func/func_net.h>
#include <tns-func/func_convert.h>

#define _USE_MATH_DEFINES

//=====================

uni10::CUniTensor unitaryRot(int dim, double theta, int idx1, int idx2){

	std::vector<uni10::Bond> bonds;
	bonds.push_back( uni10::Bond( uni10::BD_IN, dim ) );
	bonds.push_back( uni10::Bond( uni10::BD_OUT, dim ) );

	uni10::UniTensor unitary_rot(bonds);

	double *rot_elem = new double[dim*dim];
	for (int i = 0; i < dim; ++i) {
		for (int j = 0; j < dim; ++j) {
			if (i == j)
				rot_elem[i * dim + j] = (i == idx1 || i == idx2)? cos(theta) : 1.0;
			else if (i == idx1 && j == idx2)
				rot_elem[i * dim + j] = -1.0 * sin(theta);
			else if (i == idx2 && j == idx1)
				rot_elem[i * dim + j] = sin(theta);
			else
				rot_elem[i * dim + j] = 0.0;
		}
	}

	unitary_rot.setRawElem(rot_elem);

	delete [] rot_elem;

	return real2Complex(unitary_rot);
}

//==============================================

int main(int argc, char* argv[]){

	int mode = 0;
	int idx1 = 0;
	int idx2 = 1;
	double theta = 0.5;
	std::string Umtx, Usav;
	bool save_u = false;

	if (argc < 2) {
		std::cerr << "Usage: " << argv[0] << " [options]" << std::endl;
		std::cerr << "Allowed options:" << std::endl;
		std::cerr << "-u arg  load Uitary mtx from file [arg]" << std::endl;
		std::cerr << "-rnd  transform with random unitary matrix" << std::endl;
		std::cerr << "-rot  (theta) (idx1) (idx2)  transform with rotational matrix" << std::endl;
		std::cerr << "-s arg  save Uitary mtx to file [arg]" << std::endl;
		return 1;
	}

	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-u") {
			mode = 2;
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				Umtx = std::string(argv[i+1]);
			}
			else {
				std::cerr << "usage: -u filename_of_U_mtx" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-rot") {
			mode = 1;
			if (i + 3 < argc) { // Make sure we aren't at the end of argv!
				std::stringstream(argv[i+1]) >> theta;
				std::istringstream(argv[i+2]) >> idx1;
				std::istringstream(argv[i+3]) >> idx2;
			}
			else if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> theta;
			}
			else {
				std::cerr << "usage: -rot (double)theta (int)idx1 (int)idx2" << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-rnd") {
			mode = 0;
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				save_u = true;
				Usav = std::string(argv[i+1]);
			}
			else {
				std::cerr << "usage: -s filename_of_U_mtx" << std::endl;
				return 1;
			}
		}
	}


	std::vector<uni10::CUniTensor> gamma;
	std::vector<uni10::CUniTensor> lambda;

	std::string wf_dir = "mps-inf";
	gamma.push_back( uni10::CUniTensor(wf_dir + "/gamma_0") );
	gamma.push_back( uni10::CUniTensor(wf_dir + "/gamma_1") );
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_0") );
	lambda.push_back( uni10::CUniTensor(wf_dir + "/lambda_1") );

	int D = gamma[0].bond()[0].dim();

	uni10::CUniTensor U;
	uni10::CUniTensor U_inv;

	if (mode == 2) {
		U = uni10::CUniTensor(Umtx);
		U_inv = U;
		U_inv.transpose();
		U_inv.conj();
	}

	else if (mode == 1) {
		U = unitaryRot(D, M_PI * theta, idx1, idx2);
		U_inv = unitaryRot(D, -1.0 * M_PI * theta, idx1, idx2);
	}

	else if (mode == 0) {
		uni10::CMatrix rnd_mtx(D, D);
		rnd_mtx.randomize();
		std::vector<uni10::CMatrix> rnd_mtx_svd = rnd_mtx.svd();

		std::vector<uni10::Bond> bdu;
		bdu.push_back( uni10::Bond( uni10::BD_IN, D ) );
		bdu.push_back( uni10::Bond( uni10::BD_OUT, D ) );

		U = uni10::CUniTensor(bdu);
		U_inv = uni10::CUniTensor(bdu);
		U.putBlock( rnd_mtx_svd[0] );
		U_inv.putBlock( rnd_mtx_svd[0].transpose() );
	}

	gamma[0] = netLGL( U_inv, gamma[0], U );
	gamma[1] = netLGL( U_inv, gamma[1], U );
	lambda[0] = netLLL( U_inv, lambda[0], U );
	lambda[1] = netLLL( U_inv, lambda[1], U );

	gamma[0].save(wf_dir + "/gamma_0");
	gamma[1].save(wf_dir + "/gamma_1");
	lambda[0].save(wf_dir + "/lambda_0");
	lambda[1].save(wf_dir + "/lambda_1");

	if (save_u)
		U.save( Usav );

	return 0;
}

