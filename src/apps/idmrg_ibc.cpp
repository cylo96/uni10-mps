#include <sstream>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H arg  load Hamiltonian mpo from folder arg" << std::endl;
	std::cerr << "-w arg  load Wavefunction (gamma and lambda tensors) from folder arg" << std::endl;
	std::cerr << "-r arg  Resume DMRG from folder arg" << std::endl;
	std::cerr << "-l arg  initial ibc lattice Length" << std::endl;
	std::cerr << "-sw arg  initial ibc dmrg SWeep" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of iDMRG lattice growing/iteration Steps" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {
	///
	if (argc < 3) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian mpo directory
	std::string mpo_dir;
	// wavefunction directory
	std::string wf_dir = "mps-inf";
	// load gamma lambda from file?
	bool load_file = false;
	// resume dmrg?
	bool resume = false;
	// initial length
	int len = 20;
	// initial dmrg sweep
	int sweep = 20;
	// bond dimension
	int bd_dim;
	// number of steps
	int steps_max = 10;
	// max iteration for Lanczos algorithm
	int iter_max = 200;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				mpo_dir = std::string(argv[i+1]);
			}
			else { // Uh-oh, there was no argument to the -H option.
				std::cerr << "-H option requires the directory of the Hamiltonian MPO." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-r") {
			resume = true;
			load_file = true;
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> len;
			}
			else { // Uh-oh, there was no argument to the -m option.
				std::cerr << "-l option requires a positive integer lattice length." << std::endl;
				return 1;
			}  
		}
		else if (std::string(argv[i]) == "-sw") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> sweep;
			}
			else { // Uh-oh, there was no argument to the -m option.
				std::cerr << "-sw option requires a positive integer number of sweeps." << std::endl;
				return 1;
			}  
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> steps_max;
			}
			else { // Uh-oh, there was no argument to the -s option.
				std::cerr << "-s option requires a positive integer number of iDMRG steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> iter_max;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::stringstream(argv[i+1]) >> tolerance;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
	}

	/// import H
	uni10::CUniTensor hl = uni10::CUniTensor("HL");
	uni10::CUniTensor hlw = uni10::CUniTensor("HLW");
	uni10::CUniTensor hw = uni10::CUniTensor("ham_xxz");	// <-- no good
	uni10::CUniTensor hwr = uni10::CUniTensor("HWR");
	uni10::CUniTensor hr = uni10::CUniTensor("HR");

	uni10::CUniTensor gam( wf_dir + "/gamma_0" );
	bd_dim = gam.bond()[0].dim();
	int d  = gam.bond()[1].dim();
	int mid = len/2 - 1;

	ChainIBC mps(len, d, bd_dim);
	mps.importMPS( wf_dir );

	if (resume) {
		mps = ChainIBC(2, d, bd_dim);
		mps.importMPS( wf_dir );
		std::cout << "#Step\tEnergy\tlam[0]\tlam[1]\tlam[2]\tlam[3]\n";
		mps.idmrgResume( mpo_dir, sweep, steps_max, iter_max, tolerance, wf_dir );
	}
	else {
		/// perform TEBD
		Complex dT(0.0, 0.1);
		mps.tebdImp( hl, hlw, hw, hwr, hr, hw, mid, dT, 20 );
		/// perform IBC::iDMRG
		std::cout << "#Step\tEnergy\tlam[0]\tlam[1]\tlam[2]\tlam[3]\n";
		mps.idmrg( mpo_dir, sweep, steps_max, iter_max, tolerance, wf_dir );
		//mps.exportMPS( "mps-ibc-xxz" );	// <-- no good
	}

	ChainInf imps( d, bd_dim );
	imps.putGamma( mps.getGamma(0) );
	imps.putGamma( mps.getGamma(1) );
	imps.putLambda( mps.getLambda(0) );
	imps.putLambda( mps.getLambda(1) );

	Complex dt (0.0, 1e-9);
	imps.itebd( hw, dt, 100, 4 );
	imps.exportMPS( wf_dir );	

	return 0;
}

