#include <iostream>
#include <sstream>

#include <mps/ChainInf.h>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H args  model Hamiltonian" << std::endl;
	std::cerr << "-w  load Wavefunction (gamma and lambda tensors) from current folder" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-it arg  evolution (Imaginary) Timestep" << std::endl;
	std::cerr << "-rt arg  evolution (Real) Timestep" << std::endl;
	std::cerr << "-s arg  number of evolution Steps" << std::endl;
	std::cerr << "-ord arg  the ORDer of Trotter-Suzuki expansion" << std::endl;
	std::cerr << "-mon arg (at) (arg)  set a quantity to MONitor during evolution" << std::endl;
	std::cerr << "-mint arg  set the Monitoring INTerval" << std::endl;
	std::cerr << "-gs  evolve to Ground State using default method" << std::endl;
}

//======================================

int main( int argc, char* argv[] ) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian
	std::string ham;
	// wavefunction directory
	std::string wf_dir = "mps-inf";
	// bond dimension
	int bd_dim = 5;
	// initial imag dt
	double dT_init = 0.1;
	// initial real dt
	double dt_init = 0.0;
	// number of steps
	int steps_max = -1;
	// order of Trotter-Suzuki expansion
	int ts_order = 2;
	// load gamma lambda from file?
	bool load_file = false;
	// monitor list
	bool prt_monitor = false;
	int mon_interval = 10;
	std::vector<monitor> monitors;
	// evolve to ground state?
	bool to_gs = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				ham = std::string(argv[i+1]);
			}
			else { // Uh-oh, there was no argument to the -H option.
				std::cerr << "-H option requires the filename of the Hamiltonian." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> bd_dim;
			}
			else { // Uh-oh, there was no argument to the -m option.
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}  
		}
		else if (std::string(argv[i]) == "-it") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::stringstream(argv[i+1]) >> dT_init;
				dt_init = 0.0;
			}
			else { // Uh-oh, there was no argument to the -it option.
				std::cerr << "-it option requires a positive dt." << std::endl;
				return 1;
			}
		} 
		else if (std::string(argv[i]) == "-rt") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::stringstream(argv[i+1]) >> dt_init;
				dT_init = 0.0;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-rt option requires a positive dt." << std::endl;
				return 1;
			}
		} 
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> steps_max;
			}
			else { // Uh-oh, there was no argument to the -s option.
				std::cerr << "-s option requires a positive integer number of evolution." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ord") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> ts_order;
			}
			else { // Uh-oh, there was no argument to the -ord option.
				std::cerr << "-ord option requires a positive integer order of Trotter-Suzuki expansion." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-mon") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				prt_monitor = true;
				if (i + 3 < argc && std::string(argv[i+2]) == "at" ) {
					int idx;
					std::istringstream(argv[i+3]) >> idx;
					monitors.push_back( monitor( std::string(argv[i+1]), idx ) );
				}
				else {
					monitors.push_back( monitor( std::string(argv[i+1]), 0 ) );
				}
			}
			else { // Uh-oh, there was no argument to the -mon option.
				std::cerr << "-mon option requires a string of the monitoring parameter." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-mint") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::stringstream(argv[i+1]) >> mon_interval;
			}
			else { // Uh-oh, there was no argument to the -mint option.
				std::cerr << "-mint option requires a positive integer interval for monitoring." << std::endl;
				return 1;
			}
		} 
		else if (std::string(argv[i]) == "-gs") {
			to_gs = true;
		}
	}


	/// main function body
	uni10::CUniTensor hamiltonian( ham );
	int phys_dim = hamiltonian.bond()[0].dim();

	ChainInf mps2site( phys_dim, bd_dim );

	if ( load_file ) {
		mps2site.importMPS( wf_dir );
	}
	else {
		mps2site.randomize();
	}

	std::cout << "\n# Starting iTEBD with: Bond Dimension = " << bd_dim << "\n";

	if ( to_gs ) {

		std::complex<double> dt (0.0, dT_init);	// dt in complex form

		double dt_min = 5e-9;
		double t = 0.0;
		int ckpt, Nevl;

		if ( steps_max != -1 )
			steps_max = 10 * (1 + steps_max / 10);

		double eng0 = 0.0; 
		double eng1, dE;

		while ( dt.imag() >= dt_min ) {

			ckpt = 0;
			Nevl = 0;

			while (ckpt < 1) {
				//====== evolve and update ======
				mps2site.itebd( hamiltonian, dt, 10, ts_order );

				//====== calculate energy ======
				eng1 = mps2site.expVal( hamiltonian )[0].real();
				dE = fabs( eng0 - eng1 );

				//====== output ======
				if ( Nevl == steps_max || ( Nevl > 0 && dE < ( std::min( 1e-14, 1e-8 * dt.imag()) ) ) ) {
					std::cout << "\ndt = " << dt << "\n# of evolution = " << Nevl << "\n";
					std::cout << "Energy = " << std::setprecision(10) << eng1 << "\n\n";
					ckpt = 1;
				}

				eng0 = eng1;
				Nevl += 10;
			}

			dt /= 2.0;
		}
	}

	else {

		std::complex<double> dt (dt_init, dT_init);	// dt in complex form

		if (prt_monitor) {
			mps2site.itebd( hamiltonian, dt, steps_max, ts_order, monitors, mon_interval );
		}
		else {
			mps2site.itebd( hamiltonian, dt, steps_max, ts_order );

			double eng = mps2site.expVal( hamiltonian )[0].real();
			std::cout << "\ndt = " << dt << "\n# of evolution = " << steps_max << "\n";
			std::cout << "Energy = " << std::setprecision(10) << eng << "\n\n";
		}
	}

	mps2site.exportMPS( wf_dir );
	mps2site.clear();

	return 0;
}


