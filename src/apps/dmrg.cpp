#include <sstream>
#include <math.h>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

void split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss;
	ss.str(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
}


std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H arg  load Hamiltonian mpo from folder arg" << std::endl;
	std::cerr << "-w arg  load Wavefunction (gamma and lambda tensors)" << std::endl;
	std::cerr << "-r arg  Resume dmrg using mps from folder arg" << std::endl;
	std::cerr << "-l arg  system Length" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of dmrg Sweeps" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-intm arg  save INTerMediate state every arg sweeps" << std::endl;
	std::cerr << "-u2  perform 2-site Update (default is 1-site)" << std::endl;
	std::cerr << "-te  max Truncation Error for 2-site update" << std::endl;
	std::cerr << "-V  Verbose. show information during dmrg update" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian mpo directory
	std::string mpo_dir;
	// wavefunction directory
	std::string wf_dir = "mps-inf";
	// load mps from file?
	bool load_file = false;
	bool resume = false;
	// save intermediate state?
	bool save_intm = false;
	int intm;
	// system length
	int len = 20;
	// bond dimension
	int bd_dim = 5;
	// number of sweeps
	int sweep = 10;
	// max iteration for Lanczos algorithm
	int iter_max = 100;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// max allowed truncation error
	double trunc_err_lim = 1.0;
	// show info during dmrg update
	bool verbose = false;
	// use 2-site update
	bool u2 = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				mpo_dir = std::string(argv[i+1]);
			}
			else { // Uh-oh, there was no argument to the -H option.
				std::cerr << "-H option requires the directory of the Hamiltonian MPO." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w" || std::string(argv[i]) == "-r") {
			load_file = true;
			resume = true;
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> len;
			}
			else { // Uh-oh, there was no argument to the -m option.
				std::cerr << "-l option requires a positive integer system length." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> bd_dim;
			}
			else { // Uh-oh, there was no argument to the -m option.
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> sweep;
			}
			else { // Uh-oh, there was no argument to the -s option.
				std::cerr << "-s option requires a positive integer number of iDMRG steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> iter_max;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::stringstream(argv[i+1]) >> tolerance;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-intm option requires a positive integer number of sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-te") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::stringstream(argv[i+1]) >> trunc_err_lim;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-te option requires a positive number of Truncation Error tolerance." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
		else if (std::string(argv[i]) == "-u2") {
			u2 = true;
		}
	}

	/// initialize OBC chain
	uni10::CUniTensor mpo_m( mpo_dir + "/mpo_m" );
	int phys_dim = mpo_m.bond()[1].dim();

	ChainOBC mpsOBC(len, phys_dim, bd_dim);

	std::vector<std::string> substrs = split( mpo_dir , '-');
	std::string save_dir = "mps-obc-" + substrs[ substrs.size()-1 ];


	if (load_file) {
		mpsOBC.importMPS( wf_dir );
	}
	else {
		mpsOBC.randomize();
		mpsOBC.exportMPS( save_dir );
	}

	/// perform DMRG
	if (verbose) {
		if (u2)
			std::cout << "#ParL\tParR\tTruncErr\tEntEnt\tTotEng\n";
		else
			std::cout << "#ParL\tParR\tEntEnt\tTotEng\n";
	}
	if (save_intm) {
		int s = 0;
		while ( s < sweep ) {
			if (u2)
				mpsOBC.dmrgU2( mpo_dir, intm, iter_max, tolerance, trunc_err_lim, verbose );
			else
				mpsOBC.dmrg( mpo_dir, intm, iter_max, tolerance, verbose );
			s += intm;
			mpsOBC.exportMPS( save_dir + "-s" + std::to_string((long long) s) );
		}
	}
	else {
		if (u2)
			mpsOBC.dmrgU2( mpo_dir, sweep, iter_max, tolerance, trunc_err_lim, verbose );
		else
			mpsOBC.dmrg( mpo_dir, sweep, iter_max, tolerance, verbose );
	}

	mpsOBC.exportMPS( save_dir );
	return 0;
}
