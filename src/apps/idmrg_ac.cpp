#include <sstream>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H arg  load Hamiltonian mpo from folder arg" << std::endl;
	std::cerr << "-J arg  set J value of XXZ hamiltonian" << std::endl;
	std::cerr << "-Jz arg  set Jz value of XXZ hamiltonian" << std::endl;
	std::cerr << "-w arg  load Wavefunction (gamma and lambda tensors) from folder arg" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of iDMRG lattice growing/iteration Steps" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {
	///
	if (argc < 3) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian mpo directory
	std::string mpo_dir;
	// boundary hamiltonian directory
	std::string hb_dir = ".";
	//
	double J = 1.0, Jz = 1.0;
	// wavefunction directory
	std::string wf_dir = "mps-inf";
	// load gamma lambda from file?
	bool load_file = false;
	// bond dimension
	int bd_dim;
	// number of steps
	int steps_max = 10;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				mpo_dir = std::string(argv[i+1]);
			}
			else { // Uh-oh, there was no argument to the -H option.
				std::cerr << "-H option requires the directory of the Hamiltonian MPO." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-J") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::stringstream(argv[i+1]) >> J;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-J option requires a float number of J." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-Jz") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::stringstream(argv[i+1]) >> Jz;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-Jz option requires a float number of Jz." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> bd_dim;
			}
			else { // Uh-oh, there was no argument to the -m option.
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}  
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> steps_max;
			}
			else { // Uh-oh, there was no argument to the -s option.
				std::cerr << "-s option requires a positive integer number of iDMRG steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> iter_max;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::stringstream(argv[i+1]) >> tolerance;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
	}

	/// import H
	uni10::CUniTensor hl = uni10::CUniTensor("HL");
	uni10::CUniTensor hlw = uni10::CUniTensor("HLW");
	uni10::CUniTensor hw = uni10::CUniTensor("ham_xxz");	// <-- no good
	uni10::CUniTensor hwr = uni10::CUniTensor("HWR");
	uni10::CUniTensor hr = uni10::CUniTensor("HR");

	int uc = 2;
	ChainIBC mps( uc, hw.bond()[0].dim(), bd_dim );
	mps.importMPS( wf_dir );
	mps.idmrgAC( J, Jz, mpo_dir, hb_dir, steps_max, iter_max, tolerance, wf_dir );

	ChainInf imps( hw.bond()[0].dim(), bd_dim );
	imps.putGamma( mps.getGamma(0) );
	imps.putGamma( mps.getGamma(1) );
	imps.putLambda( mps.getLambda(0) );
	imps.putLambda( mps.getLambda(1) );
	imps.exportMPS( wf_dir );	

	return 0;
}

