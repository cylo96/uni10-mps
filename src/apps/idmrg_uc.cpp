#include <iostream>
#include <sstream>

#include <mps.hpp>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H arg  load Hamiltonian mpo from folder arg" << std::endl;
	std::cerr << "-w arg  load Wavefunction (gamma and lambda tensors) from folder arg" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of iDMRG lattice growing/iteration Steps" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian mpo directory
	std::string mpo_dir;
	// wavefunction directory
	std::string wf_dir = "mps-inf";
	// load gamma lambda from file?
	bool load_file = false;
	// bond dimension
	int bd_dim = 5;
	// number of steps
	int steps_max = 10;
	// max iteration for Lanczos algorithm
	int iter_max = 100;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				mpo_dir = std::string(argv[i+1]);
			}
			else { // Uh-oh, there was no argument to the -H option.
				std::cerr << "-H option requires the directory of the Hamiltonian MPO." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> bd_dim;
			}
			else { // Uh-oh, there was no argument to the -m option.
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}  
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> steps_max;
			}
			else { // Uh-oh, there was no argument to the -s option.
				std::cerr << "-s option requires a positive integer number of iDMRG steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> iter_max;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::stringstream(argv[i+1]) >> tolerance;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
	}

	/// main function body
	uni10::CUniTensor mpo_l( mpo_dir + "/mpo_l" );
	uni10::CUniTensor mpo_r( mpo_dir + "/mpo_r" );

	uni10::CUniTensor hamiltonian = mpoLR( mpo_l, mpo_r );
	int phys_dim = hamiltonian.bond()[0].dim();

	ChainInf mps2site( phys_dim, bd_dim );
	mps2site.randomize();
	mps2site.idmrgUC( mpo_dir, steps_max, iter_max, tolerance );

	Complex dt (0.0, 1e-9);
	mps2site.itebd( hamiltonian, dt, 100, 4 );

	mps2site.exportMPS( wf_dir );

	return 0;
}

