#include <sstream>
#include <math.h>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H arg  load Hamiltonian mpo from folder arg" << std::endl;
	std::cerr << "-w arg  load Wavefunction (gamma and lambda tensors) from folder arg" << std::endl;
	std::cerr << "-b arg  load Boundary operators from folder arg" << std::endl;
	std::cerr << "-l arg  system Length" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of dmrg Sweeps" << std::endl;
	std::cerr << "-u2  dmrg performs 2-site Update" << std::endl;
	std::cerr << "-V  Verbose. show information during dmrg update" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-ramp arg  tolerance RaMPing factor for Lanczos algorithm" << std::endl;
	std::cerr << "-intm arg  save INTerMediate state every arg sweeps" << std::endl;
	std::cerr << "-df arg  Destination Folder arg" << std::endl;
	std::cerr << "-rev  REVerse left-right end (means, change fixed end to the right)" << std::endl;
	std::cerr << "-nq  save a copy of No Qnum state" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian mpo directory
	std::string mpo_dir = "mpo-ham-xxz";
	// initial wavefunction directory
	std::string wf_dir = "mps-inf";
	// resume wavefunction directory
	std::string resume_dir = "mps-semiinf-xxz";
	// final mps destination directory
	std::string mps_dir = "mps-semiinf-xxz";
	// load mps from file?
	bool load_file = false;
	bool resume = false;
	// save intermediate state?
	bool save_intm = false;
	int intm = 2;
	// system length
	int len = 100;
	// bond dimension
	int bd_dim = 5;
	// number of sweeps
	int sweep = 10;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// tolerance ramping
	int ramp_tol = 10;
	// 2-site update
	bool two_site_update = false;
	// show truncation error
	bool verbose = false;
	// reverse chain?
	bool reverse = false;
	// save a no qnum state?
	bool no_qn = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc)
				mpo_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-H option requires the directory of the Hamiltonian MPO." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				wf_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-w option requires the directory of the Wavefunction MPS." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-r") {
			resume = true;
			if (i + 1 < argc)
				resume_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-r option requires the directory name of MPS to resume." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-df option requires a directory name for final MPS." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l option requires a positive integer system length." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sweep;
			else {
				std::cerr << "-s option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iter_max;
			else {
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-rmp" || std::string(argv[i]) == "-ramp") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> ramp_tol;
			else {
				std::cerr << "-ramp option requires a positive integer number of tolerance ramping factor." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) {
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else {
				std::cerr << "-intm option requires a positive integer number of sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-u2") {
			two_site_update = true;
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
		else if (std::string(argv[i]) == "-rev") {
			reverse = true;
		}
		else if (std::string(argv[i]) == "-nq") {
			no_qn = true;
		}
	}


	/// main function body
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( len, mpo_dir, mpo_sy, mpo_pl, mpo_mi );

	uni10::CUniTensor hw = op2SiteFromMPOQn( mpo_sy[1], mpo_pl[1], mpo_mi[1] );
	std::vector<uni10::Qnum> phys_dim = hw.bond()[0].Qlist();

	/// initialize SemiInf Chain
	ChainQnSemiInf mpsSemiInf( len, bd_dim, phys_dim );
	if (reverse)
		mpsSemiInf.reverse();
	if (load_file)
		mpsSemiInf.importMPS( wf_dir );
	else
		mpsSemiInf.randomize();

	if (resume) {
		int n = (reverse)? 0 : len;
		int nn = (reverse)? 1 : len-1;
		uni10::CUniTensor lam = mpsSemiInf.getLambda(n);
		mpsSemiInf.importMPS( resume_dir );

		std::vector<uni10::Qnum> qnew = lam.bond()[0].Qlist();
		std::vector<uni10::Qnum> qL = mpsSemiInf.getGamma(len-1).bond()[0].Qlist();
		mpsSemiInf.putLambda( lam, n );
		mpsSemiInf.resizeGamma( nn, qL, qnew );
	}

	/// perform DMRG
	int s = 0;
	if (save_intm) {
		s = 0;
		while ( s < sweep ) {
			if (two_site_update)
				mpsSemiInf.dmrgU2( mpo_dir, intm, iter_max, tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			else
				mpsSemiInf.dmrg( mpo_dir, intm, iter_max, tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );

			s += intm;
			mpsSemiInf.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );
		}
	}
	else {
		if (two_site_update)
			mpsSemiInf.dmrgU2( mpo_dir, sweep, iter_max, tolerance, verbose );
		else
			mpsSemiInf.dmrg( mpo_dir, sweep, iter_max, tolerance, verbose );
	}

	mpsSemiInf.exportMPS( mps_dir );

	return 0;
}
