#include <iostream>
#include <sstream>

#include <mps.hpp>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H arg  load Hamiltonian mpo from folder arg" << std::endl;
	std::cerr << "-w arg  load Wavefunction (gamma and lambda tensors)" << std::endl;
	std::cerr << "-r arg  Resume DMRG using mps from folder arg" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of iDMRG lattice growing/iteration Steps" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-off  OFFset hb in boundary mpo when doing dmrg" << std::endl;
	std::cerr << "-svco arg  Singular Value CutOff" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian mpo directory
	std::string mpo_dir;
	// wavefunction directory
	std::string wf_dir = "mps-inf";
	// load mps from file?
	bool load_file = false;
	bool resume = false;
	// bond dimension
	int bd_dim = 5;
	// number of steps
	int steps_max = 10;
	// max iteration for Lanczos algorithm
	int iter_max = 100;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// error tolerance for Lanczos algorithm
	double sv_cutoff = 1e-10;
	// offset hb to traceless?
	bool offset_hb = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				mpo_dir = std::string(argv[i+1]);
			}
			else { // Uh-oh, there was no argument to the -H option.
				std::cerr << "-H option requires the directory of the Hamiltonian MPO." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-r") {
			load_file = true;
			resume = true;
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> bd_dim;
			}
			else { // Uh-oh, there was no argument to the -m option.
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> steps_max;
			}
			else { // Uh-oh, there was no argument to the -s option.
				std::cerr << "-s option requires a positive integer number of iDMRG steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::istringstream(argv[i+1]) >> iter_max;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::stringstream(argv[i+1]) >> tolerance;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-svco") {
			if (i + 1 < argc) { // Make sure we aren't at the end of argv!
				std::stringstream(argv[i+1]) >> sv_cutoff;
			}
			else { // Uh-oh, there was no argument to the -rt option.
				std::cerr << "-svco option requires a positive number of singular value cutoff." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-off") {
			offset_hb = true;
		}
	}

	/// main function body
	uni10::CUniTensor mpo_l( mpo_dir + "/mpo_l" );
	uni10::CUniTensor mpo_r( mpo_dir + "/mpo_r" );

	uni10::CUniTensor hamiltonian = mpoLR( mpo_l, mpo_r );
	int phys_dim = hamiltonian.bond()[0].dim();

	ChainInf mps2site( phys_dim, bd_dim );
	mps2site.setSchValCutoff(sv_cutoff);

	if (load_file) {
		mps2site.importMPS( wf_dir );
		if (resume)
			mps2site.idmrgResume( mpo_dir, steps_max, iter_max, tolerance, wf_dir, offset_hb );
		else
			mps2site.idmrg( mpo_dir, steps_max, iter_max, tolerance, wf_dir, offset_hb );
	}
	else {
		mps2site.randomize();
		mps2site.exportMPS( wf_dir );
		mps2site.idmrg( mpo_dir, steps_max, iter_max, tolerance, wf_dir, offset_hb );
		//mps2site.idmrgSweep( mpo_dir, steps_max, iter_max, tolerance );
	}

//	Complex dt (0.0, 1e-9);
//	mps2site.itebd( hamiltonian, dt, 100, 4 );

	mps2site.exportMPS( wf_dir );

	return 0;
}
