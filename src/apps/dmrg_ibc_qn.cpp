#include <sstream>
#include <math.h>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H arg  load Hamiltonian mpo from folder arg" << std::endl;
	std::cerr << "-w arg  load inf Wavefunction (gamma and lambda tensors) from folder arg" << std::endl;
	std::cerr << "-w2 arg1 arg2  load 2 semi-inf Wavefunctions from folders arg" << std::endl;
	std::cerr << "-r arg  Resume dmrg update and/or Resize mps from folder arg" << std::endl;
	std::cerr << "-b arg  load Boundary operators from folder arg" << std::endl;
	std::cerr << "-l arg  system Length" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-uc arg  Unit Cell size of the wavefunction to load" << std::endl;
	std::cerr << "-s arg  number of dmrg Sweeps" << std::endl;
	std::cerr << "-s2 arg  number of 2-site update dmrg Sweeps after 1-site update" << std::endl;
	std::cerr << "-l0 arg  first stage sub-system Length" << std::endl;
	std::cerr << "-ts arg  imaginary Time evol Steps for first stage" << std::endl;
	std::cerr << "-s0 arg  number of dmrg Sweeps for first stage" << std::endl;
	std::cerr << "-u2  dmrg performs 2-site Update" << std::endl;
	std::cerr << "-iloc arg  Initial Location for dmrg sweep" << std::endl;
	std::cerr << "-brk arg  BReaK link/entanglement at location arg" << std::endl;
	std::cerr << "-V  Verbose. show information during dmrg update" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-ramp arg  tolerance RaMPing factor for Lanczos algorithm" << std::endl;
	std::cerr << "-intm arg  save INTerMediate state every arg sweeps" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian mpo directory
	std::string mpo_dir = "mpo-ham-xxz";
	// inital wavefunction directory
	std::string wf_dir = "mps-inf";
	// resume wavefunction directory
	std::string mps_dir = "mps-ibc-xxz";
	// boundary hamiltonian directory
	std::string hb_dir = ".";
	// semi-inf wavefunction directory
	std::string semi_dir_l = "mps-inf";
	std::string semi_dir_r = "mps-inf";
	// load mps from file?
	bool load_file = true;
	bool resume = false;
	bool load_2semi = false;
	// save intermediate state?
	bool save_intm = false;
	int intm = 2;
	// system length
	int len = 100;
	int l0 = -1;
	// initial location for dmrg sweep
	int init_loc = 0;
	// broken link
	int brk_loc = -1;
	// bond dimension
	int bd_dim = 5;
	// unit cell size
	int unit_cell = 2;
	// number of sweeps
	int sweep = 10;
	int sw_u2 = 0;
	int s0 = -1;
	// imaginary tebd steps
	int tebd_st = 50;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// tolerance ramping
	int ramp_tol = 10;
	// 2-site update
	bool two_site_update = false;
	// show truncation error
	bool verbose = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc)
				mpo_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-H option requires the directory of the Hamiltonian MPO." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				wf_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-w option requires the directory of the Wavefunction MPS." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w2") {
			load_file = true;
			load_2semi = true;
			if (i + 2 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos ) {
				semi_dir_l = std::string(argv[i+1]);
				semi_dir_r = std::string(argv[i+2]);
			}
			else {
				std::cerr << "-w2 option requires 2 directories of semi-inf Wavefunction MPS." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-r") {
			resume = true;
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-r option requires the directory name of MPS to resume." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-b") {
			if (i + 1 < argc)
				hb_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-b option requires the directory of the Boundary Hamiltonians." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l option requires a positive integer system length." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-uc") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> unit_cell;
			else {
				std::cerr << "-uc option requires a positive integer unit cell size." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sweep;
			else {
				std::cerr << "-s option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s2") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sw_u2;
			else {
				std::cerr << "-s2 option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l0") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> l0;
			else {
				std::cerr << "-l0 option requires a positive integer sub-system length." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s0") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> s0;
			else {
				std::cerr << "-s0 option requires a positive integer number of iDMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-iloc") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> init_loc;
			else {
				std::cerr << "-iloc option requires a positive integer initial location for DMRG sweep." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-brk") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> brk_loc;
			else {
				std::cerr << "-brk option requires a positive integer location of the broken link." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ts") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> tebd_st;
			else {
				std::cerr << "-ts option requires a positive integer number of TEBD steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iter_max;
			else {
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
		else if ( (std::string(argv[i]) == "-ramp") || (std::string(argv[i]) == "-rmp") ) {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> ramp_tol;
			else {
				std::cerr << "-ramp option requires a positive integer number of tolerance ramping factor." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) {
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else {
				std::cerr << "-intm option requires a positive integer number of sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-u2") {
			two_site_update = true;
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
	}

	s0 = (s0 < 0)? 2*sweep : s0;
	l0 = (l0 > len)? len : l0;
	bool two_stage = (l0 > 0);

	/// main function body
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( len, mpo_dir, mpo_sy, mpo_pl, mpo_mi );

	uni10::CUniTensor hw = op2SiteFromMPOQn( mpo_sy[1], mpo_pl[1], mpo_mi[1] );
	std::vector<uni10::Qnum> phys_dim = hw.bond()[0].Qlist();
/*
	/// import H
	uni10::CUniTensor hl = uni10::CUniTensor( hb_dir + "/HL" );
	uni10::CUniTensor hlw = uni10::CUniTensor( hb_dir + "/HLW" );
	uni10::CUniTensor hw = uni10::CUniTensor( hb_dir + "/ham_xxz" );
	uni10::CUniTensor hwr = uni10::CUniTensor( hb_dir + "/HWR" );
	uni10::CUniTensor hr = uni10::CUniTensor( hb_dir + "/HR" );
	uni10::CUniTensor himp = uni10::CUniTensor( hb_dir + "/ham_imp" );
*/
	/// initialize IBC Chain
	ChainQnIBC mpsIBC( len, bd_dim, phys_dim );
	if (two_stage)
		mpsIBC = ChainQnIBC( l0, bd_dim, phys_dim );
	int mid = mpsIBC.getSize()/2 - 1;

	size_t found = wf_dir.find("semiinf");
	if (load_2semi)
		mpsIBC.import2Semi( semi_dir_l, semi_dir_r );
	else {
		if (found != std::string::npos)	// if found semiinf in wf_dir
			mpsIBC.import2Semi( wf_dir );
		else
			mpsIBC.importMPS( wf_dir, unit_cell );
	}

	if (resume) {
		uni10::CUniTensor lam = mpsIBC.getLambda(0);
		mpsIBC.importMPS( mps_dir );

		int len_init = mpsIBC.getSize();
		std::vector<uni10::Qnum> qnew = lam.bond()[0].Qlist();
		std::vector<uni10::Qnum> q0 = mpsIBC.getGamma(0).bond()[2].Qlist();
		std::vector<uni10::Qnum> qL = mpsIBC.getGamma(len_init-1).bond()[0].Qlist();
		mpsIBC.putLambda( lam, 0 );
		mpsIBC.putLambda( lam, len_init );
		mpsIBC.resizeGamma( 0, qnew, q0 );
		mpsIBC.resizeGamma( len_init-1, qL, qnew );
	}

	// break link
	if (brk_loc > 0)
		mpsIBC.breakLink(brk_loc);

/*
	/// perform TEBD
	Complex dT(0.0, 0.1);
	mpsIBC.tebdImp( hl, hlw, hw, hwr, hr, himp, mid, dT, tebd_st );
*/
	/// perform DMRG
	int s = 0;
	if (two_stage) {
		// stage 1: l0 <= len
		if (verbose)
			std::cout << "\nStage 1\n";
		while ( s < s0 ) {
			mpsIBC.dmrgImp( init_loc, mid, mpo_dir,
				2, iter_max, tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			s += 2;
		}
		mpsIBC.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );

		// stage 2: prepare full len
		if (found != std::string::npos)	// if found semiinf in wf_dir
			wf_dir = "mps-inf";
		mpsIBC.expand( (len-l0)/2, (len-l0)/2, wf_dir, 2 );
		mid = len/2 - 1;
		if (verbose)
			std::cout << "\nStage 2\n";
	}

	if (save_intm) {
		s = 0;
		while ( s < sweep ) {
			init_loc = (s == 0)? init_loc : 0;
			if (two_site_update)
				mpsIBC.dmrgImpU2( init_loc, mid, mpo_dir, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-2*(two_stage)-s)/2)), verbose );
			else {
				if ( s < sweep-sw_u2 )
					mpsIBC.dmrgImp( init_loc, mid, mpo_dir, intm, iter_max,
						tolerance * std::max(1.0, std::pow(10, (ramp_tol-2*(two_stage)-s)/2)), verbose );
				else
					mpsIBC.dmrgImpU2( init_loc, mid, mpo_dir, intm, iter_max,
						tolerance * std::max(1.0, std::pow(10, (ramp_tol-2*(two_stage)-s)/2)), verbose );
			}
			s += intm;
			mpsIBC.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );
		}
	}
	else {
		if (two_site_update)
			mpsIBC.dmrgImpU2( init_loc, mid, mpo_dir, sweep, iter_max, tolerance, verbose );
		else
			mpsIBC.dmrgImp( init_loc, mid, mpo_dir, sweep, iter_max, tolerance, verbose );
	}

	mpsIBC.exportMPS( mps_dir );
	return 0;
}
