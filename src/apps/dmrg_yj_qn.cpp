#include <iostream>
#include <sstream>
// #include <complex>
#include <mps.hpp>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H <(str) Hamiltonian mpo directory>" << std::endl;
	std::cerr << "-H3 <(str) Hamiltonian mpo1> <(str) Hamiltonian mpo2> <(str) Hamiltonian mpo3>" << std::endl;
	std::cerr << "-w <(str) Wavefunction mps directory>" << std::endl;
	std::cerr << "-w3 <(str) Wavefunction mps1> <(str) Wavefunction mps2> <(str) Wavefunction mps3>" << std::endl;
	std::cerr << "-ww <(str) replace Wavefunction mps on Wire>" << std::endl;
	std::cerr << "-ww3 <(str) Wavefunc Wire1> <(str) Wavefunc Wire2> <(str) Wavefunc Wire3>" << std::endl;
	std::cerr << "-l <(int) system Length>" << std::endl;
	std::cerr << "-uc <(int) Unit Cell size>" << std::endl;
	std::cerr << "-m <(int) Max bond dimension>" << std::endl;
	std::cerr << "-mj <(int) Max bond dimension at JT>" << std::endl;
	std::cerr << "-mr <(num) bond dimension Ramp factor>" << std::endl;
	std::cerr << "-s <(int) dmrg Sweeps>" << std::endl;
	std::cerr << "-u2  dmrg performs 2-site Update" << std::endl;
	std::cerr << "-iloc <(int) (int) Initial wire and LOCation for dmrg sweep>" << std::endl;
	std::cerr << "-df <(str) Destination Folder for final mps>" << std::endl;
	std::cerr << "-ite <(int) Lanczos max ITEration>" << std::endl;
	std::cerr << "-tol <(num) Lanczos error TOLerance>" << std::endl;
	std::cerr << "-ramp <(int) tolerance RaMPing factor>" << std::endl;
	std::cerr << "-intm <(int) save INTerMediate state every arg sweeps>" << std::endl;
	std::cerr << "-pert <(int) type of PERTurbation> <(double) parameter of PERTurbation>" << std::endl;
}

//======================================

int main( int argc, char* argv[] ) {

	if (argc < 2) {
		errMsg( argv[0] );
		return 1;
	}

	/// main function body
	// hamiltonian
	std::string ham_dir = "mpo-ham-hub";
	std::string h1_dir = "mpo-ham-hub";
	std::string h2_dir = "mpo-ham-hub";
	std::string h3_dir = "mpo-ham-hub";
	// wavefunction directory
	std::string wf_dir = "mps-inf";
	std::string w1_dir = "mps-inf";
	std::string w2_dir = "mps-inf";
	std::string w3_dir = "mps-inf";
	std::string ww_dir = "mps-inf";
	std::string ww1_dir = "mps-inf";
	std::string ww2_dir = "mps-inf";
	std::string ww3_dir = "mps-inf";
	bool replace_ww = false;
	// final mps directory
	std::string mps_dir = "mps-ibc-yj";
	// number of sweeps
	int sweep = 10;
	int sw_u2 = 4;
	// length of each wire
	int len = 16;
	// unit-cell size
	int uc = 4;
	// bond dimension
	int chi = 8;
	int chi_junc = chi;
	double chi_ramp = 2.;
	// initial location for dmrg sweep
	int init_wire = 1;
	int init_loc = 0;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// tolerance ramping
	int ramp_tol = 6;
	// save intermediate state?
	bool save_intm = false;
	int intm = 1;
	// 2-site update
	bool two_site_update = false;
	// separate junction gams
	bool sep_gj = false;
	// perturbation
	bool pert = false;
	int pert_type = 0;
	double pert_param = 0.;

	/// set parameters
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc)
				ham_dir = std::string(argv[i+1]);
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-H3") {
			if (i + 3 < argc) {
				ham_dir = "";
				h1_dir = std::string(argv[i+1]);
				h2_dir = std::string(argv[i+2]);
				h3_dir = std::string(argv[i+3]);
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			if (i + 1 < argc) {
				wf_dir = std::string(argv[i+1]);
				w1_dir = wf_dir;
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w3") {
			if (i + 3 < argc) {
				wf_dir = "";
				w1_dir = std::string(argv[i+1]);
				w2_dir = std::string(argv[i+2]);
				w3_dir = std::string(argv[i+3]);
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ww") {
			if (i + 1 < argc) {
				ww_dir = std::string(argv[i+1]);
				replace_ww = true;
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ww3") {
			if (i + 3 < argc) {
				ww_dir = "";
				ww1_dir = std::string(argv[i+1]);
				ww2_dir = std::string(argv[i+2]);
				ww3_dir = std::string(argv[i+3]);
				replace_ww = true;
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-uc") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> uc;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> chi;
				chi_junc = chi;
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-mj") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> chi_junc;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-mr") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> chi_ramp;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sweep;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s2") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sw_u2;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-iloc") {
			if (i + 2 < argc) {
				std::istringstream(argv[i+1]) >> init_wire;
				std::istringstream(argv[i+2]) >> init_loc;
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iter_max;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if ( (std::string(argv[i]) == "-ramp") || (std::string(argv[i]) == "-rmp") ) {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> ramp_tol;
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) {
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-u2") {
			two_site_update = true;
		}
		else if (std::string(argv[i]) == "-pert") {
			if (i + 2 < argc) {
				pert = true;
				std::istringstream(argv[i+1]) >> pert_type;
				std::stringstream(argv[i+2]) >> pert_param;
			}
			else {
				errMsg( argv[0] );
				return 1;
			}
		}
	}

	// initialize YJ
	uni10::CUniTensor gam0;
	try {
		gam0 = uni10::CUniTensor(w1_dir + "/wire1/gamma_0");
	}
	catch(const std::exception& e) {
		try {
			gam0 = uni10::CUniTensor(w1_dir + "/gamma_0");
		}
		catch(const std::exception& e) {
			errMsg(argv[0]);
			return 1;
		}
	}
	std::vector<uni10::Qnum> qphys = gam0.bond()[1].Qlist();
	std::vector<uni10::Qnum> qvirt = gam0.bond()[0].Qlist();
	chi = gam0.bond()[0].dim();
	YJuncQnIBC mpsYJ(len, chi, qphys, qvirt);

	// import MPS
	if (wf_dir == "")
		mpsYJ.importMPS(w1_dir, w2_dir, w3_dir, uc, sep_gj);
	else
		mpsYJ.importMPS(wf_dir, uc, sep_gj);
		
	if (chi_junc != chi) {
		mpsYJ.setBDJT(chi_junc);
		mpsYJ.setBDRamp(chi_ramp);
	}

	if (replace_ww) {
		if (ww_dir == "")
			mpsYJ.importWireMPS(ww1_dir, ww2_dir, ww3_dir, uc);
		else
			mpsYJ.importWireMPS(ww_dir, uc);
	}

	// import MPO
	if (ham_dir == "")
		mpsYJ.importMPOYJ(h1_dir, h2_dir, h3_dir);
	else
		mpsYJ.importMPOYJ(ham_dir);

	// perturbation
	if (pert)
		mpsYJ.perturbMPOYJ(pert_type, pert_param);

	// DMRG
	int s = 0;
	std::cout << "#wire\tpartL\tpartR\tentropy\t\teigVal" << '\n';
	while (s < sweep) {
		if (two_site_update) {
			mpsYJ.dmrgJTU2(init_wire, init_loc, intm, iter_max,
				tolerance * std::max(1.0, std::pow(10, (ramp_tol-s-1)*intm)), true, chi_junc);
		}
		else {
			if (s < sw_u2)
				mpsYJ.dmrgJTU2(init_wire, init_loc, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-s-1)*intm)), true, chi_junc);
			else
				mpsYJ.dmrgJT(1, 0, intm, iter_max,
					tolerance * std::max(1.0, std::pow(10, (ramp_tol-s-1)*intm)), true);
		}
		s += intm;
		if (save_intm)
			mpsYJ.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );
	}

	mpsYJ.exportMPS( mps_dir );

	return 0;
}
