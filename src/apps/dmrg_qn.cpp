#include <sstream>
#include <math.h>

#include <mps.hpp>

#define _USE_MATH_DEFINES

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H arg  load Hamiltonian mpo from folder arg" << std::endl;
	std::cerr << "-w arg  load Wavefunction (gamma and lambda tensors) from folder arg" << std::endl;
	std::cerr << "-b arg  load Boundary operators from folder arg" << std::endl;
	std::cerr << "-l arg  system Length" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of dmrg Sweeps" << std::endl;
	std::cerr << "-u2  dmrg performs 2-site Update" << std::endl;
	std::cerr << "-N arg  total quantum Number of the mps" << std::endl;
	std::cerr << "-V  Verbose. show information during dmrg update" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-ramp arg  tolerance RaMPing factor for Lanczos algorithm" << std::endl;
	std::cerr << "-intm arg  save INTerMediate state every arg sweeps" << std::endl;
	std::cerr << "-df arg  Destination Folder arg" << std::endl;
	std::cerr << "-nq  save a copy of No Qnum state" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian mpo directory
	std::string mpo_dir = "mpo-ham-xxz";
	// initial wavefunction directory
	std::string wf_dir = "mps-inf";
	// final mps destination directory
	std::string mps_dir = "mps-obc-xxz";
	// load mps from file?
	bool load_file = false;
	// save intermediate state?
	bool save_intm = false;
	int intm = 2;
	// system length
	int len = 100;
	// bond dimension
	int bd_dim = 5;
	// number of sweeps
	int sweep = 10;
	// max iteration for Lanczos algorithm
	int iter_max = 500;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// tolerance ramping
	int ramp_tol = 10;
	// 2-site update
	bool two_site_update = false;
	// show truncation error
	bool verbose = false;
	// save a no qnum state?
	bool no_qn = false;
	// total qnum
	int qn_tot = 0;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc)
				mpo_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-H option requires the directory of the Hamiltonian MPO." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc && ((std::string)argv[i+1]).find("-") != std::string::npos )
				wf_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-w option requires the directory of the Wavefunction MPS." << std::endl;
				return 1;
			}  
		}
		else if (std::string(argv[i]) == "-df") {
			if (i + 1 < argc)
				mps_dir = std::string(argv[i+1]);
			else {
				std::cerr << "-df option requires a directory name for final MPS." << std::endl;
				return 1;
			}  
		}
		else if (std::string(argv[i]) == "-l") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> len;
			else {
				std::cerr << "-l option requires a positive integer system length." << std::endl;
				return 1;
			}  
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> bd_dim;
			else {
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}  
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> sweep;
			else {
				std::cerr << "-s option requires a positive integer number of DMRG sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> iter_max;
			else {
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc)
				std::stringstream(argv[i+1]) >> tolerance;
			else {
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-rmp" || std::string(argv[i]) == "-ramp") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> ramp_tol;
			else {
				std::cerr << "-ramp option requires a positive integer number of tolerance ramping factor." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-intm") {
			if (i + 1 < argc) {
				save_intm = true;
				std::istringstream(argv[i+1]) >> intm;
			}
			else {
				std::cerr << "-intm option requires a positive integer number of sweeps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-N") {
			if (i + 1 < argc)
				std::istringstream(argv[i+1]) >> qn_tot;
			else {
				std::cerr << "-N option requires an integer quantum number." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-u2") {
			two_site_update = true;
		}
		else if (std::string(argv[i]) == "-V") {
			verbose = true;
		}
		else if (std::string(argv[i]) == "-nq") {
			no_qn = true;
		}
	}


	/// main function body
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( len, mpo_dir, mpo_sy, mpo_pl, mpo_mi );

	uni10::CUniTensor hw = op2SiteFromMPOQn( mpo_sy[1], mpo_pl[1], mpo_mi[1] );
	std::vector<uni10::Qnum> phys_dim = hw.bond()[0].Qlist();
	bool fermi = phys_dim[0].isFermionic();

	/// initialize OBC Chain
	ChainQnOBC mpsOBC( len, bd_dim, phys_dim );
	if (qn_tot != 0) {
		if (qn_tot%2 && fermi)
			mpsOBC = ChainQnOBC( len, bd_dim, phys_dim, uni10::Qnum(0), uni10::Qnum(uni10::PRTF_ODD, qn_tot, uni10::PRT_EVEN) );
		else
			mpsOBC = ChainQnOBC( len, bd_dim, phys_dim, uni10::Qnum(0), uni10::Qnum(qn_tot) );
	}
	if (load_file)
		mpsOBC.importMPS( wf_dir );
	else
		mpsOBC.randomize();

	/// perform DMRG
	int s = 0;
	if (save_intm) {
		s = 0;
		while ( s < sweep ) {
			if (two_site_update)
				mpsOBC.dmrgU2( mpo_dir, intm, iter_max, tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );
			else
				mpsOBC.dmrg( mpo_dir, intm, iter_max, tolerance * std::max(1.0, std::pow(10, (ramp_tol-s)/2)), verbose );

			s += intm;
			mpsOBC.exportMPS( mps_dir + "-s" + std::to_string((long long) s) );
		}
	}
	else {
		if (two_site_update)
			mpsOBC.dmrgU2( mpo_dir, sweep, iter_max, tolerance, verbose );
		else
			mpsOBC.dmrg( mpo_dir, sweep, iter_max, tolerance, verbose );
	}

	mpsOBC.exportMPS( mps_dir );

	if (no_qn) {
		ChainOBC mpsNQ = mpsOBC.toNoQ();
		mpsNQ.exportMPS( mps_dir + "-nq" );
	}

	return 0;
}
