#include <iostream>
#include <sstream>

#include <mps.hpp>

//======================================

void errMsg( char* arg ) {

	std::cerr << "Usage: " << arg << " [options]" << std::endl;
	std::cerr << "Allowed options:" << std::endl;
	std::cerr << "-H arg  load Hamiltonian mpo from folder arg" << std::endl;
	std::cerr << "-w arg  load Wavefunction (gamma and lambda tensors)" << std::endl;
	std::cerr << "-r arg  Resume DMRG using mps from folder arg" << std::endl;
	std::cerr << "-m arg  Max bond dimension" << std::endl;
	std::cerr << "-s arg  number of iDMRG lattice growing/iteration Steps" << std::endl;
	std::cerr << "-ite arg  max ITEration for Lanczos algorithm" << std::endl;
	std::cerr << "-tol arg  error TOLerance for Lanczos algorithm" << std::endl;
	std::cerr << "-off  OFFset hb in boundary mpo when doing dmrg" << std::endl;
	std::cerr << "-nq  save a copy of No Qnum state" << std::endl;
}

//======================================

int main(int argc, char* argv[]) {

	if (argc < 2) {

		errMsg( argv[0] );
		return 1;
	}

	// hamiltonian mpo directory
	std::string mpo_dir;
	// wavefunction directory
	std::string wf_dir = "mps-inf";
	// load mps from file?
	bool load_file = false;
	bool resume = false;
	// bond dimension
	int bd_dim = 5;
	// number of steps
	int steps_max = 10;
	// max iteration for Lanczos algorithm
	int iter_max = 100;
	// error tolerance for Lanczos algorithm
	double tolerance = 1e-15;
	// offset hb to traceless?
	bool offset_hb = false;
	// save a no qnum state?
	bool no_qn = false;

	/// set parameters
	for (int i = 1; i < argc; ++i) {

		if (std::string(argv[i]) == "-H") {
			if (i + 1 < argc) {
				mpo_dir = std::string(argv[i+1]);
			}
			else {
				std::cerr << "-H option requires the directory of the Hamiltonian MPO." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-w") {
			load_file = true;
			if (i + 1 < argc) {
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-r") {
			load_file = true;
			resume = true;
			if (i + 1 < argc) {
				wf_dir = std::string(argv[i+1]);
			}
		}
		else if (std::string(argv[i]) == "-m") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> bd_dim;
			}
			else {
				std::cerr << "-m option requires a positive integer bond dimension." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-s") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> steps_max;
			}
			else {
				std::cerr << "-s option requires a positive integer number of iDMRG steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-ite") {
			if (i + 1 < argc) {
				std::istringstream(argv[i+1]) >> iter_max;
			}
			else {
				std::cerr << "-ite option requires a positive integer number of Lanczos iteration steps." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-tol") {
			if (i + 1 < argc) {
				std::stringstream(argv[i+1]) >> tolerance;
			}
			else {
				std::cerr << "-tol option requires a positive number of Lanczos error tolerance." << std::endl;
				return 1;
			}
		}
		else if (std::string(argv[i]) == "-off") {
			offset_hb = true;
		}
		else if (std::string(argv[i]) == "-nq") {
			no_qn = true;
		}
	}

	/// main function body
	std::vector<uni10::CUniTensor> mpo_sy;
	std::vector<uni10::CUniTensor> mpo_pl;
	std::vector<uni10::CUniTensor> mpo_mi;
	importMPOQn( 2, mpo_dir, mpo_sy, mpo_pl, mpo_mi );

	uni10::CUniTensor hamiltonian = mpoLRQn( mpo_sy, mpo_pl, mpo_mi );
	std::vector<uni10::Qnum> qphys = hamiltonian.bond()[0].Qlist();
	std::vector<uni10::Qnum> qvirt;

	bool fermi = qphys[0].isFermionic();
	if (fermi) {
		qvirt = hamiltonian.bond()[0].Qlist();
		for (int i = 0; i < qvirt.size(); ++i)
			qvirt[i].assign( qvirt[i].prtF(), 0, qvirt[i].prt() );
	}
	else
		qvirt.push_back( uni10::Qnum(0) );

	ChainQnInf mps2site( bd_dim, qphys, qvirt );

	if (load_file) {
		mps2site.importMPS( wf_dir );
		if (resume)
			mps2site.idmrgResume( mpo_dir, steps_max, iter_max, tolerance, wf_dir, offset_hb );
		else
			mps2site.idmrg( mpo_dir, steps_max, iter_max, tolerance, wf_dir, offset_hb );
	}
	else {
		mps2site.randomize();
		mps2site.exportMPS( wf_dir );
		// steps_max+1 ? one more update to keep the blk struct of final lam0 similar to original
		mps2site.idmrg( mpo_dir, steps_max+1, iter_max, tolerance, wf_dir, offset_hb );
	}

//	Complex dt (0.0, 1e-9);
//	mps2site.itebd( hamiltonian, dt, 100, 1 );

	mps2site.exportMPS( wf_dir );

	if (no_qn) {
		ChainInf mpsNQ = mps2site.toNoQ();
		mpsNQ.exportMPS( wf_dir + "-nq" );
	}

	return 0;
}
